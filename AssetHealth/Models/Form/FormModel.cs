﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using System.Web.Mvc;
using FluentValidation.Attributes;
using AssetHealth.Validators;
using AssetHealth.Models.Form;

using AssetHealth.Common.DTO.Enums;
using Ares.Web.Extensions;
using Ares.Web.Models.Language;
using Ares.Web.Models.Parameter_item;
using AssetHealth.Models.User;
using AssetHealth.Models.Equipment;

namespace AssetHealth.Models.Form
{
    [Validator(typeof(FormModelValidator))]
    public partial class FormModel : BaseAresModel //, ILocalizedModel<TopicLocalizedModel>
    {
        public FormModel()
        {
            //Locales = new List<TopicLocalizedModel>();
            SelectedTab = "1";
            
            Filter_FormQuestionModel = new AssetHealth.Models.Form_question.FormQuestionModel();  
                       
        }
        
        public string SelectedTab { get; set; }

        
        [AresResourceDisplayName("FormModel.Fields.Id")]
        public int Id { get; set; }
        

        
        [AresResourceDisplayName("FormModel.Fields.Object_id")]
        public string Object_id { get; set; }
        
        
        
        [AresResourceDisplayName("FormModel.Fields.Category_id")]
        public int? Category_id { get; set; }
        
        
        
        [AresResourceDisplayName("FormModel.Fields.Derived_id")]
        public int? Derived_id { get; set; }
        
        
        
        [AresResourceDisplayName("FormModel.Fields.Language_id")]
        public int? Language_id { get; set; }
        
        
        
        [AresResourceDisplayName("FormModel.Fields.Period")]
        public int? Period { get; set; }
        
        
        
        [AresResourceDisplayName("FormModel.Fields.Equipment_id")]
        public int? Equipment_id { get; set; }
        
        
        
        [AresResourceDisplayName("FormModel.Fields.Role_id")]
        public int? Role_id { get; set; }
        
        
        
        [AresResourceDisplayName("FormModel.Fields.Server_id")]
        public int? Server_id { get; set; }
        
        
        
        [AresResourceDisplayName("FormModel.Fields.Code")]
        public string Code { get; set; }
        
        
        
        [AresResourceDisplayName("FormModel.Fields.Title")]
        public string Title { get; set; }
        
        
        
        [AresResourceDisplayName("FormModel.Fields.Description")]
        public string Description { get; set; }
        
        
        
        [AresResourceDisplayName("FormModel.Fields.Notes")]
        public string Notes { get; set; }
        
        
        
        [AresResourceDisplayName("FormModel.Fields.Version")]
        public int? Version { get; set; }
        
        
        
        [AresResourceDisplayName("FormModel.Fields.Image_path")]
        public string Image_path { get; set; }
        
        
        
        [AresResourceDisplayName("FormModel.Fields.Form_json")]
        public string Form_json { get; set; }
        
        
        
        [AresResourceDisplayName("FormModel.Fields.Form_json2")]
        public string Form_json2 { get; set; }
        
        
        
        [AresResourceDisplayName("FormModel.Fields.Active")]
        public bool Active { get; set; }
        
        
        
        [AresResourceDisplayName("FormModel.Fields.Deleted")]
        public bool Deleted { get; set; }
        
        
        
        [AresResourceDisplayName("FormModel.Fields.Display_order")]
        public int? Display_order { get; set; }
        
        
        
            public FormModel DERIVED { get; set; }  
            
            [AresResourceDisplayName("FormModel.Fields.Derived_id_Text")]
            public string Derived_id_Text { get; set; }   
            public SelectList Derived_id_Form_SelectList { get; set; }           
            
            public EquipmentModel EQUIPMENT { get; set; }  
            
            [AresResourceDisplayName("FormModel.Fields.Equipment_id_Text")]
            public string Equipment_id_Text { get; set; }   
            public SelectList Equipment_id_Form_SelectList { get; set; }           
            
            public LanguageModel LANGUAGE { get; set; }  
            
            [AresResourceDisplayName("FormModel.Fields.Language_id_Text")]
            public string Language_id_Text { get; set; }   
            public SelectList Language_id_Language_SelectList { get; set; }           
            
            public ParameterItemModel PARAMETER_ITEMS { get; set; }  
            
            [AresResourceDisplayName("FormModel.Fields.Category_id_Text")]
            public string Category_id_Text { get; set; }   
            public SelectList Category_id_ParameterItem_SelectList { get; set; }           
            
            public ParameterItemModel PERIOD_PARAMITEMS { get; set; }  
            
            [AresResourceDisplayName("FormModel.Fields.Period_Text")]
            public string Period_Text { get; set; }   
            public SelectList Period_ParameterItem_SelectList { get; set; }           
            
            public RoleModel ROLE { get; set; }  
            
            [AresResourceDisplayName("FormModel.Fields.Role_id_Text")]
            public string Role_id_Text { get; set; }   
            public SelectList Role_id_Role_SelectList { get; set; }           
            
            [AresResourceDisplayName("FormModel.Fields.Filter_FormAnswerRoundModel")]
            public AssetHealth.Models.Form_answer_round.FormAnswerRoundModel Filter_FormAnswerRoundModel { get; set; }   
                       
            [AresResourceDisplayName("FormModel.Fields.Filter_FormQuestionModel")]
            public AssetHealth.Models.Form_question.FormQuestionModel Filter_FormQuestionModel { get; set; }


        [AresResourceDisplayName("FormModel.Fields.Filter_EqLocation")]
        public string Filter_EqLocation_Text { get; set; }
        public SelectList Filter_EqLocation_SelectList { get; set; }



    }


}

