﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using System.Web.Mvc;
using FluentValidation.Attributes;
using AssetHealth.Validators;
using AssetHealth.Models.Eq_channel;
using AssetHealth.Common.DTO.Enums;
using Ares.Web.Extensions;
using Ares.Web.Models.Parameter_item;

namespace AssetHealth.Models.Eq_threshold
{
    [Validator(typeof(EqThresholdModelValidator))]
    public partial class EqThresholdModel : BaseAresModel //, ILocalizedModel<TopicLocalizedModel>
    {
        public EqThresholdModel()
        {
            //Locales = new List<TopicLocalizedModel>();
            SelectedTab = "1";
        }
        
        public string SelectedTab { get; set; }

        
        [AresResourceDisplayName("EqThresholdModel.Fields.Id")]
        public int Id { get; set; }
        

        
        [AresResourceDisplayName("EqThresholdModel.Fields.Object_id")]
        public string Object_id { get; set; }
        
        
        
        [AresResourceDisplayName("EqThresholdModel.Fields.Eq_channel_id")]
        public int Eq_channel_id { get; set; }
        
        
        
        [AresResourceDisplayName("EqThresholdModel.Fields.Name")]
        public string Name { get; set; }
        
        
        
        [AresResourceDisplayName("EqThresholdModel.Fields.Description")]
        public string Description { get; set; }
        
        
        
        [AresResourceDisplayName("EqThresholdModel.Fields.Lower_limit")]
        public double? Lower_limit { get; set; }
        
        
        
        [AresResourceDisplayName("EqThresholdModel.Fields.Upper_limit")]
        public double? Upper_limit { get; set; }

        [AresResourceDisplayName("EqThresholdModel.Fields.Upper_limit")]
        public string Upper_limit_Text { get; set; }



        [AresResourceDisplayName("EqThresholdModel.Fields.Warning_type")]
        public int? Warning_type { get; set; }
        
        
        
        [AresResourceDisplayName("EqThresholdModel.Fields.Strategy")]
        public int? Strategy { get; set; }
        
        
        
        [AresResourceDisplayName("EqThresholdModel.Fields.Level_increase_percentage")]
        public double? Level_increase_percentage { get; set; }
        
        
        
        [AresResourceDisplayName("EqThresholdModel.Fields.Level_decrease_percentage")]
        public double? Level_decrease_percentage { get; set; }
        
        
        
        [AresResourceDisplayName("EqThresholdModel.Fields.Level_increase_repeat_times")]
        public int? Level_increase_repeat_times { get; set; }
        
        
        
        [AresResourceDisplayName("EqThresholdModel.Fields.Level_decrease_repeat_times")]
        public int? Level_decrease_repeat_times { get; set; }
        
        
        
        [AresResourceDisplayName("EqThresholdModel.Fields.Active")]
        public bool Active { get; set; }
        
        
        
            public ParameterItemModel THRESHOLD_STRATEGY_PARAMITEMS { get; set; }  
            
            [AresResourceDisplayName("EqThresholdModel.Fields.Strategy_Text")]
            public string Strategy_Text { get; set; }   
            public SelectList Strategy_ParameterItem_SelectList { get; set; }           
            
            public ParameterItemModel THRESHOLD_WARNING_TYPE_PARAMITEMS { get; set; }  
            
            [AresResourceDisplayName("EqThresholdModel.Fields.Warning_type_Text")]
            public string Warning_type_Text { get; set; }   
            public SelectList Warning_type_ParameterItem_SelectList { get; set; }           
            
            public EqChannelModel threshold_eq_channel { get; set; }  
            
            [AresResourceDisplayName("EqThresholdModel.Fields.Eq_channel_id_Text")]
            public string Eq_channel_id_Text { get; set; }   
            public SelectList Eq_channel_id_EqChannel_SelectList { get; set; }

        [AresResourceDisplayName("EqThresholdModel.Fields.Filter_EqLocation_Text")]
        public string Filter_EqLocation_Text { get; set; }
        public SelectList Filter_EqLocation_SelectList { get; set; }


        [AresResourceDisplayName("EqThresholdModel.Fields.Upper_Limit_Option_YES_NO")]
        public string Upper_Limit_Option_YES_NO { get; set; }
        public SelectList Upper_Limit_Option_YES_NO_SelectList { get; set; }


    }


}

