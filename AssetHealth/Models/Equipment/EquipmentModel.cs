﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using System.Web.Mvc;
using FluentValidation.Attributes;
using AssetHealth.Validators;
using AssetHealth.Models.Eq_location;
using AssetHealth.Common.DTO.Enums;
using Ares.Web.Extensions;
using Ares.Web.Models.Parameter_item;

namespace AssetHealth.Models.Equipment
{
    [Validator(typeof(EquipmentModelValidator))]
    public partial class EquipmentModel : BaseAresModel //, ILocalizedModel<TopicLocalizedModel>
    {
        public EquipmentModel()
        {
            //Locales = new List<TopicLocalizedModel>();
            SelectedTab = "1";
            
            Filter_EqChannelModel = new AssetHealth.Models.Eq_channel.EqChannelModel();  
                       
            
            Filter_EqWarningModel = new AssetHealth.Models.Eq_warning.EqWarningModel();  
                       
        }
        
        public string SelectedTab { get; set; }

        
        [AresResourceDisplayName("EquipmentModel.Fields.Id")]
        public int Id { get; set; }
        

        
        [AresResourceDisplayName("EquipmentModel.Fields.Object_id")]
        public string Object_id { get; set; }
        
        
        
        [AresResourceDisplayName("EquipmentModel.Fields.Eq_location_id")]
        public int Eq_location_id { get; set; }
        
        
        
        [AresResourceDisplayName("EquipmentModel.Fields.Equipment_type")]
        public int? Equipment_type { get; set; }
        
        
        
        [AresResourceDisplayName("EquipmentModel.Fields.Ip_address")]
        public string Ip_address { get; set; }
        
        
        
        [AresResourceDisplayName("EquipmentModel.Fields.Name")]
        public string Name { get; set; }
        
        
        
        [AresResourceDisplayName("EquipmentModel.Fields.Number_of_channels")]
        public int? Number_of_channels { get; set; }
        
        
        
        [AresResourceDisplayName("EquipmentModel.Fields.Cid")]
        public string Cid { get; set; }
        
        
        
        [AresResourceDisplayName("EquipmentModel.Fields.Description")]
        public string Description { get; set; }
        
        
        
        [AresResourceDisplayName("EquipmentModel.Fields.Read_order")]
        public int? Read_order { get; set; }
        
        
        
        [AresResourceDisplayName("EquipmentModel.Fields.Active")]
        public bool Active { get; set; }
        
        
        
            public ParameterItemModel EQUIPMENT_TYPE_PARAMITEMS { get; set; }  
            
            [AresResourceDisplayName("EquipmentModel.Fields.Equipment_type_Text")]
            public string Equipment_type_Text { get; set; }   
            public SelectList Equipment_type_ParameterItem_SelectList { get; set; }           
            
            public EqLocationModel Location { get; set; }  
            
            [AresResourceDisplayName("EquipmentModel.Fields.Eq_location_id_Text")]
            public string Eq_location_id_Text { get; set; }   
            public SelectList Eq_location_id_EqLocation_SelectList { get; set; }           
            
            [AresResourceDisplayName("EquipmentModel.Fields.Filter_EqChannelModel")]
            public AssetHealth.Models.Eq_channel.EqChannelModel Filter_EqChannelModel { get; set; }   
                       
            [AresResourceDisplayName("EquipmentModel.Fields.Filter_EqWarningModel")]
            public AssetHealth.Models.Eq_warning.EqWarningModel Filter_EqWarningModel { get; set; }

        [AresResourceDisplayName("EquipmentModel.Fields.FormList")]
        public string[] FormList { get; set; }
        public SelectList Forms_SelectList { get; set; }


    }


}

