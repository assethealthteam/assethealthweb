﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetHealth.Models.Home
{
    public class MapPopupModel
    {
        public string LastReadDateTime { get; set; }
        public string LocationName { get; set; }
        public string SchemaPath { get; set; }
        public string CurrentWarnings { get; set; }
        public int AlarmCount { get; set; }
        public int WarningCount { get; set; }
    }
}