﻿using Ares.Core.Helpers;
using Ares.Web.Models.Notification;
using Ares.Web.Razor;
using AssetHealth.Common.DTO;
using AssetHealth.Common.DTO.Extensions;
using AssetHealth.Models.Common;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace AssetHealth.Models.Home
{
    //[Bind(Exclude = "AuthorSelectList")]
    public class DashboardModel
    {
        public DashboardModel()
        {
            StartDate = DateTime.Now.AddDays(-30);
            EndDate = DateTime.Now;
            GeneralStatModel = new List<AmChartsModel>();
            UserActivityModel = new List<AssetHealthPerformanceModel>();

            RelatedLocationIds = new List<int>();
        }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public long StartDateLong { get { return StringHelper.ConvertDateTimeToLong(StartDate); } }
        public long EndDateLong { get { return StringHelper.ConvertDateTimeToLong(EndDate); } }

        public string AssetHealthPerformance { get; set; }
        public string ChecklistPlan { get; set; }
        public string GeneralStats { get; set; }
        public string Activities { get; set; }
        public string EquipmentHealthPerformance { get; set; }
        public string LocationHealthPerformance { get; set; }
        public string CurrentWarnings { get; set; }
        public string QuestionGrid { get; set; }
        public List<AmChartsModel> GeneralStatModel { get; set; }
        public List<NotificationModel> NotificationModels { get; set; }
        public List<AssetHealthPerformanceModel> UserActivityModel { get; set; }

        public List<StatBoxModel> StatBoxModels { get; set; }

        [AresResourceDisplayName("DashboardModel.Fields.Filter_Location_Id")]
        public int Filter_Location_Id { get; set; }
        public SelectList Filter_Location_SelectList { get; set; }

        [JsonIgnore]
        [ScriptIgnore]
        public MySqlParameter[] SqlParams { get; set; }

        [JsonIgnore]
        [ScriptIgnore]
        public List<int> RelatedLocationIds { get; set; }
        [JsonIgnore]
        [ScriptIgnore]
        public List<EqLocation> Locations { get; set; }

        public void PrepareDashboardModel(Ares.UMS.DTO.Users.User currentUser)
        {
            StartDate = new DateTime(StartDate.Year, StartDate.Month, StartDate.Day, 0, 0, 0);
            EndDate = new DateTime(EndDate.Year, EndDate.Month, EndDate.Day, 23, 59, 59);

            Locations = currentUser.Location();

            SqlParams = new MySqlParameter[]
            {
              new MySqlParameter("exec_mode", 2) { Direction = ParameterDirection.Input },  // cache
              new MySqlParameter("tenant_id", currentUser.tenant_id) { Direction = ParameterDirection.Input },
              new MySqlParameter("locId", Filter_Location_Id) { Direction = ParameterDirection.Input },
              new MySqlParameter("roleId", currentUser.UserRoles.Min(r => r.Id) ) { Direction = ParameterDirection.Input },
              new MySqlParameter("startDate", StartDateLong) { Direction = ParameterDirection.Input },
              new MySqlParameter("endDate", EndDateLong) { Direction = ParameterDirection.Input },
              new MySqlParameter("groupBy", 4) { Direction = ParameterDirection.Input },
              new MySqlParameter("userId", currentUser.Id) { Direction = ParameterDirection.Input },

              new MySqlParameter("languageId", currentUser.WorkingLanguage_Id) { Direction = ParameterDirection.Input },
              new MySqlParameter("Id", Locations.FirstOrDefault().Id) { Direction = ParameterDirection.Input },
            };

            if (Filter_Location_Id <= 0 && currentUser.UserRoles.All(r => r.Id > 2))
                Filter_Location_Id = Locations.FirstOrDefault().Id;

            if (Filter_Location_Id > 0)
            {
                RelatedLocationIds.Add(Filter_Location_Id);
            }
            else
            {
                RelatedLocationIds = currentUser.RelatedLocations().Select(r => r.Id).ToList();
            }

        }
    }



    public class AssetHealthPerformanceModel
    {
        public string HEALTHY { get; set; }
        public string UNHEALTHY { get; set; }
        public string LOCATION_NAME { get; set; }
        public string EQUIPMENT_NAME { get; set; }
        public string FORMATTED_DAY { get; set; }
        [AresResourceDisplayName("AssetHealthPerformanceModel.PERFORMANCE")]
        public string PERFORMANCE { get; set; }
        public string CATEGORY_Id { get; set; }
        public string CATEGORY { get; set; }



        // user performance fields
        [AresResourceDisplayName("AssetHealthPerformanceModel.ROLENAME_TITLE")]
        public string ROLENAME_TITLE { get; set; }
        [AresResourceDisplayName("AssetHealthPerformanceModel.PERIOD_TITLE")]
        public string PERIOD_TITLE { get; set; }
        [AresResourceDisplayName("AssetHealthPerformanceModel.USERNAME")]
        public string USERNAME { get; set; }
        [AresResourceDisplayName("AssetHealthPerformanceModel.NUMBER_OF_QUESTIONS")]
        public string NUMBER_OF_QUESTIONS { get; set; }
        [AresResourceDisplayName("AssetHealthPerformanceModel.REQUIRED_ANSWER")]
        public string REQUIRED_ANSWER { get; set; }
        [AresResourceDisplayName("AssetHealthPerformanceModel.TOTAL_ANSWERED")]
        public string TOTAL_ANSWERED { get; set; }
        [AresResourceDisplayName("AssetHealthPerformanceModel.FILES_COUNT")]
        public string FILES_COUNT { get; set; }


    }



    public class CurrentWarningModel
    {
        public int Id { get; set; }
        public int LOC_ID { get; set; }
        public string LOCATION_NAME { get; set; }
        public int QUESTION_ID { get; set; }
        public string NAME { get; set; }
        public string TITLE { get; set; }
        public string CONVERTED_ANSWER { get; set; }
        public string WARNING_TYPE { get; set; }
        public string READ_DATETIME { get; set; }
        public string PERIOD_TITLE { get; set; }
        public string USERNAME { get; set; }
        


    }


    public class CalendarEventModel
    {
        public string eventID { get; set; }
        public string start { get; set; }
        public string end { get; set; }
        public string Title { get; set; }
        public string Desc { get; set; }
        
        public string backgroundColor { get; set; }
        public string url { get; set; }
        public string dow { get; set; }
        public string allDay { get; set; }

    }


    public class QuestionModel
    {
        public int Id { get; set; }
        public string LOCATION_NAME { get; set; }
        public string EQUIPMENT_NAME { get; set; }
        public string OPTION_GROUP_ID { get; set; }
        public string TITLE { get; set; }
        public string DISPLAY_ORDER { get; set; }
        

    }

    public class AmChartsModel
    {
        public int Id { get; set; }
        public string Date { get; set; }
        public string Value { get; set; }
        public string Label { get; set; }
        public string TITLE { get; set; }

    }
}