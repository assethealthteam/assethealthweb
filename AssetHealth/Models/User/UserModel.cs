using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using Ext.Net.MVC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AssetHealth.Models.User
{
    [FluentValidation.Attributes.Validator(typeof(Validators.UserModelValidator))]
    public class UserModel : BaseAresModel
    {
        public UserModel()
        {
            UserPicture = new Ares.Web.Models.Media.PictureModel();
            UserPasswordChangeModel = new UserPasswordChangeModel();
        }

        [Field(ReadOnly = true, AllowBlank = true, FieldType = typeof(Ext.Net.Hidden))]
        [AresResourceDisplayName("UserModel.Fields.Id")]
        public virtual int Id { get; set; }

        [AresResourceDisplayName("UserModel.Fields.User_guid")]
        public string User_guid { get; set; }

        [AresResourceDisplayName("UserModel.Fields.Username")]
        public string Username { get; set; }


        [AresResourceDisplayName("UserModel.Fields.Password")]
        public string Password { get; set; }


        [AresResourceDisplayName("UserModel.Fields.Password_format_id")]
        public int? Password_format_id { get; set; }


        [AresResourceDisplayName("UserModel.Fields.Password_salt")]
        public string Password_salt { get; set; }


        [AresResourceDisplayName("UserModel.Fields.Email")]
        public string Email { get; set; }

        [AresResourceDisplayName("UserModel.Fields.IdentityNumber")]
        public string IdentityNumber { get; set; }


        [AresResourceDisplayName("UserModel.Fields.System_name")]
        public string System_name { get; set; }


        [AresResourceDisplayName("UserModel.Fields.First_name")]
        public string First_name { get; set; }


        [AresResourceDisplayName("UserModel.Fields.Last_name")]
        public string Last_name { get; set; }


        [AresResourceDisplayName("UserModel.Fields.Birth_date")]
        public DateTime? Birth_date { get; set; }


        [AresResourceDisplayName("UserModel.Fields.Hire_date")]
        public DateTime? Hire_date { get; set; }

        [AresResourceDisplayName("UserModel.Fields.Extension")]
        public string Extension { get; set; }


        [AresResourceDisplayName("UserModel.Fields.Mobile_phone")]
        public string Mobile_phone { get; set; }


        [AresResourceDisplayName("UserModel.Fields.Notes")]
        public string Notes { get; set; }

        [AresResourceDisplayName("UserModel.Fields.Description")]
        public string Description { get; set; }


        [AresResourceDisplayName("UserModel.Fields.Active")]
        public bool Active { get; set; }


        [AresResourceDisplayName("UserModel.Fields.Deleted")]
        public bool Deleted { get; set; }
        [AresResourceDisplayName("UserModel.Fields.Is_system_account")]
        public bool Is_system_account { get; set; }


        [AresResourceDisplayName("UserModel.Fields.Last_ip_address")]
        public string Last_ip_address { get; set; }


        [AresResourceDisplayName("UserModel.Fields.Last_login_datetime")]
        public DateTime? Last_login_datetime { get; set; }


        [AresResourceDisplayName("UserModel.Fields.Last_password_changed_datetime")]
        public DateTime? Last_password_changed_datetime { get; set; }


        [AresResourceDisplayName("UserModel.Fields.Last_activity_datetime")]
        public DateTime? Last_activity_datetime { get; set; }


        [AresResourceDisplayName("UserModel.Fields.Failed_password_attempt_count")]
        public int? Failed_password_attempt_count { get; set; }
        [AresResourceDisplayName("UserModel.Fields.Orgunit_id")]
        public int? OrgUnit_id { get; set; }


        [AresResourceDisplayName("UserModel.Fields.Jobtitle_id")]
        public int? Jobtitle_id { get; set; }


        [AresResourceDisplayName("UserModel.Fields.Language_id")]
        public int? Language_id { get; set; }


        [AresResourceDisplayName("UserModel.Fields.Insert_user_id")]
        public string Insert_user_id { get; set; }


        [AresResourceDisplayName("UserModel.Fields.Update_user_id")]
        public string Update_user_id { get; set; }


        [AresResourceDisplayName("UserModel.Fields.Insert_datetime")]
        public string Insert_datetime { get; set; }


        [AresResourceDisplayName("UserModel.Fields.Update_datetime")]
        public string Update_datetime { get; set; }


        [AresResourceDisplayName("UserModel.Fields.Manager_id")]
        public int? Manager_id { get; set; }


        [AresResourceDisplayName("UserModel.Fields.Fullname")]
        public string Fullname { get; set; }

        [AresResourceDisplayName("UserModel.Fields.Nationality_Id")]
        public int? Nationality_id { get; set; }

        public SelectList Nationality_id_Nationality_SelectList { get; set; }


        public Ares.Web.Models.Media.PictureModel UserPicture { get; set; }

        /*
        private ICollection<Role> _userRoles;
        public virtual ICollection<Role> UserRoles
        {
            get { return _userRoles ?? (_userRoles = new HashSet<Role>()); }
            set { _userRoles = value; }
        }
        */

        public Ext.Net.ListItem[] RoleListItems { get; set; }

        public string ReqRoleListItems { get; set; }

        public List<UserModel> UserList { get; set; }
        public string SelectedUser { get; set; }


        public string SearchFirstName { get; set; }

        public MultiSelectListViewModel<Role> RoleViewModel { get; set; }


        public UserPasswordChangeModel UserPasswordChangeModel { get; set; }

        [AresResourceDisplayName("UserModel.Fields.Jobtitle_id_Text")]
        public string Jobtitle_id_Text { get; set; }
        public SelectList Jobtitle_id_JobTitle_SelectList { get; set; }

        [AresResourceDisplayName("UserModel.Fields.Orgunit_id_Text")]
        public string Orgunit_id_Text { get; set; }
        public SelectList Orgunit_id_OrgUnit_SelectList { get; set; }

        [AresResourceDisplayName("UserModel.Fields.Language_id_Text")]
        public string Language_id_Text { get; set; }
        public SelectList Language_id_Language_SelectList { get; set; }

        [AresResourceDisplayName("UserModel.Fields.Manager_id_Text")]
        public string Manager_id_Text { get; set; }
        public SelectList Manager_id_User_SelectList { get; set; }


        [AresResourceDisplayName("UserModel.Fields.WorkingLanguage_Id")]
        public virtual int? WorkingLanguage_Id { get; set; }

    }



    public class UserPasswordChangeModel
    {
        public virtual int Id { get; set; }

        [AresResourceDisplayName("UserPasswordChangeModel.Fields.Username")]
        public string Username { get; set; }

        [AresResourceDisplayName("UserPasswordChangeModel.Fields.CurrentPassword")]
        public string CurrentPassword { get; set; }
        [AresResourceDisplayName("UserPasswordChangeModel.Fields.NewPassword")]
        public string NewPassword { get; set; }
        [AresResourceDisplayName("UserPasswordChangeModel.Fields.NewPasswordAgain")]
        public string NewPasswordAgain { get; set; }



    }
}