using Ares.Web.Models;
using Ext.Net.MVC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetHealth.Models.User
{
    public class PermissionListModel : BaseAresModel
    {
        public PermissionListModel()
        {
            
        }

        public string SearchPermissionName { get; set; }
        public string SearchSystemname { get; set; }
        public string SearchCategory { get; set; }

        public IList<PermissionModel> PermissionModels { get; set; }
    }
}