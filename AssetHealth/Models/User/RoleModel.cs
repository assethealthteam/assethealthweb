using Ares.UMS.DTO.Security;
using Ares.Web.Models;
using Ares.Web.Razor;
using Ext.Net.MVC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetHealth.Models.User
{
    [FluentValidation.Attributes.Validator(typeof(Validators.RoleModelValidator))]
    public class RoleModel : BaseAresModel
    {
        public RoleModel()
        {
        }

        [Field(ReadOnly = true, AllowBlank = true, FieldType = typeof(Ext.Net.Hidden))]
        [AresResourceDisplayName("RoleModel.Fields.Id")]
        public virtual int Id { get; set; }
        [AresResourceDisplayName("RoleModel.Fields.Rolename")]
        public string Rolename { get; set; }
        [AresResourceDisplayName("RoleModel.Fields.Systemname")]
        public string Systemname { get; set; }


        public Ext.Net.ListItem[] PermissionListItems { get; set; }

        public string ReqPermissionListItems { get; set; }

        public Ares.Web.Models.MultiSelectListViewModel<Permission> PermissionViewModel { get; set; }



    }
}