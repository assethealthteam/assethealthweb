﻿using Ares.Web.Models;
using Ares.Web.Razor;
using AssetHealth.Validators;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AssetHealth.Models.User
{
    [Validator(typeof(PasswordRecoveryValidator))]
    public class PasswordRecoveryModel : BaseAresModel
    {
        [AllowHtml]
        [AresResourceDisplayName("User.PasswordRecovery.Email")]
        public string Email { get; set; }

        public string Result { get; set; }
    }
}