using Ares.Web.Models;
using Ares.Web.Razor;
using AssetHealth.Models.Configuration;
using AssetHealth.Validators;
using Ext.Net.MVC;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace AssetHealth.Models.User
{

    [FluentValidation.Attributes.Validator(typeof(PermissionModelValidator))]
    public class PermissionModel : BaseAresModel
    {
        public PermissionModel()
        {
            
        }

        [Field(ReadOnly = true, AllowBlank = true, FieldType = typeof(Ext.Net.Hidden))]
        [AresResourceDisplayName("PermissionModel.Fields.Id")]
        public virtual int Id { get; set; }

        [AresResourceDisplayName("PermissionModel.Fields.PermissionName")]
        public string PermissionName { get; set; }
        [AresResourceDisplayName("PermissionModel.Fields.Systemname")]
        public string Systemname { get; set; }
        [AresResourceDisplayName("PermissionModel.Fields.Category")]
        public string Category { get; set; }
        [AresResourceDisplayName("PermissionModel.Fields.Description")]
        public string Description { get; set; }

        [JsonIgnore]  // used in NewtonKing
        [ScriptIgnore]  // used in return Json statements in controller classses
        public List<MenuModel> Menus { get; set; }


        public Ext.Net.ListItem[] MenuListItems { get; set; }

        public string ReqMenuListItems { get; set; }


    }
}