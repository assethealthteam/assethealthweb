﻿using Ares.Web.Models;
using Ares.Web.Razor;
using AssetHealth.Validators;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AssetHealth.Models.User
{
    [Validator(typeof(PasswordRecoveryConfirmValidator))]
    public class PasswordRecoveryConfirmModel : BaseAresModel
    {
        public string Email { get; set; }
        public string Token { get; set; }

        [AllowHtml]
        [DataType(DataType.Password)]
        [AresResourceDisplayName("User.PasswordRecovery.NewPassword")]
        public string NewPassword { get; set; }

        [AllowHtml]
        [DataType(DataType.Password)]
        [AresResourceDisplayName("User.PasswordRecovery.ConfirmNewPassword")]
        public string ConfirmNewPassword { get; set; }

        public bool DisablePasswordChanging { get; set; }
        public string Result { get; set; }
    }
}
