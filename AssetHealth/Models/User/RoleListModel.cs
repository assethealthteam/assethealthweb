using Ares.Web.Models;
using Ext.Net.MVC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetHealth.Models.User
{
    public class RoleListModel : BaseAresModel
    {
        public RoleListModel()
        {
        }


        public string SearchRolename { get; set; }
        public string SearchSystemname { get; set; }


        public IList<RoleModel> RoleModels { get; set; }

    }
}