using Ares.Web.Models;
using Ext.Net.MVC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetHealth.Models.User
{
    public class UserListModel : BaseAresModel
    {
        public UserListModel()
        {
            
        }

        
        public string SearchUsername { get; set; }
        public string SearchEmail { get; set; }
        public string SearchFirstName { get; set; }
        public string SearchLastName { get; set; }
        public IList<UserModel> UserModels { get; set; }



    }
}