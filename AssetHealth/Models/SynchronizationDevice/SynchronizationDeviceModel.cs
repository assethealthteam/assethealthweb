﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using System.Web.Mvc;
using FluentValidation.Attributes;
using AssetHealth.Validators;
using AssetHealth.Models.Device;
using AssetHealth.Models.Synchronization;
using AssetHealth.Common.DTO.Enums;
using Ares.Web.Extensions;
using Ares.Sync.DTO.Enums;

namespace AssetHealth.Models.Synchronization_device
{
    [Validator(typeof(SynchronizationDeviceModelValidator))]
    public partial class SynchronizationDeviceModel : BaseAresModel //, ILocalizedModel<TopicLocalizedModel>
    {
        public SynchronizationDeviceModel()
        {
            //Locales = new List<TopicLocalizedModel>();
            SelectedTab = "1";
        }
        
        public string SelectedTab { get; set; }

        
        [AresResourceDisplayName("SynchronizationDeviceModel.Fields.Id")]
        public int Id { get; set; }
        

        
        [AresResourceDisplayName("SynchronizationDeviceModel.Fields.Synchronization_id")]
        public int? Synchronization_id { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationDeviceModel.Fields.Device_id")]
        public int Device_id { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationDeviceModel.Fields.Udid")]
        public string Udid { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationDeviceModel.Fields.Status")]
        public EnumSynchronizationDeviceStatus Status { get; set; }
        
        public virtual string EnumStatus_Text
		{
			get 
            { 
                return EnumExtensions.ToLocalizedtring(Status);
            }
		}
        
        
        [AresResourceDisplayName("SynchronizationDeviceModel.Fields.Start_datetime")]
        public DateTime? Start_datetime { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationDeviceModel.Fields.Finish_datetime")]
        public DateTime? Finish_datetime { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationDeviceModel.Fields.Description")]
        public string Description { get; set; }
        
        
        
            public DeviceModel SYNCHRONIZATION_DEVICE_DEVICE { get; set; }  
            
            [AresResourceDisplayName("SynchronizationDeviceModel.Fields.Device_id_Text")]
            public string Device_id_Text { get; set; }   
            public SelectList Device_id_Device_SelectList { get; set; }           
            
            public SynchronizationModel SYNCHRONIZATION_DEVICE_SYNCHRONIZATION { get; set; }  
            
            [AresResourceDisplayName("SynchronizationDeviceModel.Fields.Synchronization_id_Text")]
            public string Synchronization_id_Text { get; set; }   
            public SelectList Synchronization_id_Synchronization_SelectList { get; set; }           
            

    }


}

