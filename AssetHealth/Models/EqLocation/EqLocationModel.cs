﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using System.Web.Mvc;
using FluentValidation.Attributes;
using AssetHealth.Validators;
using AssetHealth.Models.Eq_location;
using AssetHealth.Common.DTO.Enums;
using Ares.Web.Extensions;

namespace AssetHealth.Models.Eq_location
{
    [Validator(typeof(EqLocationModelValidator))]
    public partial class EqLocationModel : BaseAresModel //, ILocalizedModel<TopicLocalizedModel>
    {
        public EqLocationModel()
        {
            //Locales = new List<TopicLocalizedModel>();
            SelectedTab = "1";
            
            Filter_EquipmentModel = new AssetHealth.Models.Equipment.EquipmentModel();  
                       
        }
        
        public string SelectedTab { get; set; }

        
        [AresResourceDisplayName("EqLocationModel.Fields.Id")]
        public int Id { get; set; }
        

        
        [AresResourceDisplayName("EqLocationModel.Fields.Object_id")]
        public string Object_id { get; set; }
        
        
        
        [AresResourceDisplayName("EqLocationModel.Fields.Location_name")]
        public string Location_name { get; set; }
        
        
        
        [AresResourceDisplayName("EqLocationModel.Fields.Cid")]
        public string Cid { get; set; }
        
        
        
        [AresResourceDisplayName("EqLocationModel.Fields.Description")]
        public string Description { get; set; }
        
        
        
        [AresResourceDisplayName("EqLocationModel.Fields.Display_order")]
        public int? Display_order { get; set; }
        
        
        
        [AresResourceDisplayName("EqLocationModel.Fields.Schema_path")]
        public string Schema_path { get; set; }
        
        
        
        [AresResourceDisplayName("EqLocationModel.Fields.Latitude")]
        public string Latitude { get; set; }
        
        
        
        [AresResourceDisplayName("EqLocationModel.Fields.Longitude")]
        public string Longitude { get; set; }
        
        
        
        [AresResourceDisplayName("EqLocationModel.Fields.Parent_id")]
        public int? Parent_id { get; set; }
        
        
        
            public EqLocationModel ParentLocation { get; set; }  
            
            [AresResourceDisplayName("EqLocationModel.Fields.Parent_id_Text")]
            public string Parent_id_Text { get; set; }   
            public SelectList Parent_id_EqLocation_SelectList { get; set; }           
            
            [AresResourceDisplayName("EqLocationModel.Fields.Filter_EquipmentModel")]
            public AssetHealth.Models.Equipment.EquipmentModel Filter_EquipmentModel { get; set; }


        [AresResourceDisplayName("EqLocationModel.Fields.UserList")]
        public string[] UserList { get; set; }
        public SelectList Users_SelectList { get; set; }

    }


}

