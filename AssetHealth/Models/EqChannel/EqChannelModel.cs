﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using System.Web.Mvc;
using FluentValidation.Attributes;
using AssetHealth.Validators;
using AssetHealth.Models.Equipment;
using AssetHealth.Common.DTO.Enums;
using Ares.Web.Extensions;
using Ares.Web.Models.Parameter_item;

namespace AssetHealth.Models.Eq_channel
{
    [Validator(typeof(EqChannelModelValidator))]
    public partial class EqChannelModel : BaseAresModel //, ILocalizedModel<TopicLocalizedModel>
    {
        public EqChannelModel()
        {
            //Locales = new List<TopicLocalizedModel>();
            SelectedTab = "1";
            
            Filter_EqThresholdModel = new AssetHealth.Models.Eq_threshold.EqThresholdModel();  
                       
            
            Filter_EqWarningModel = new AssetHealth.Models.Eq_warning.EqWarningModel();  
                       
        }
        
        public string SelectedTab { get; set; }

        
        [AresResourceDisplayName("EqChannelModel.Fields.Id")]
        public int Id { get; set; }
        

        
        [AresResourceDisplayName("EqChannelModel.Fields.Object_id")]
        public string Object_id { get; set; }
        
        
        
        [AresResourceDisplayName("EqChannelModel.Fields.Equipment_id")]
        public int Equipment_id { get; set; }
        
        
        
        [AresResourceDisplayName("EqChannelModel.Fields.Sensor_type")]
        public int? Sensor_type { get; set; }
        
        
        
        [AresResourceDisplayName("EqChannelModel.Fields.Name")]
        public string Name { get; set; }
        
        
        
        [AresResourceDisplayName("EqChannelModel.Fields.Channel_number")]
        public int? Channel_number { get; set; }
        
        
        
        [AresResourceDisplayName("EqChannelModel.Fields.Active")]
        public bool Active { get; set; }
        
        
        
        [AresResourceDisplayName("EqChannelModel.Fields.Cid")]
        public string Cid { get; set; }
        
        
        
        [AresResourceDisplayName("EqChannelModel.Fields.Description")]
        public string Description { get; set; }
        
        
        
            public ParameterItemModel CHANNEL_SENSOR_TYPE_PARAMITEMS { get; set; }  
            
            [AresResourceDisplayName("EqChannelModel.Fields.Sensor_type_Text")]
            public string Sensor_type_Text { get; set; }   
            public SelectList Sensor_type_ParameterItem_SelectList { get; set; }           
            
            public EquipmentModel channel_equipment { get; set; }  
            
            [AresResourceDisplayName("EqChannelModel.Fields.Equipment_id_Text")]
            public string Equipment_id_Text { get; set; }   
            public SelectList Equipment_id_Equipment_SelectList { get; set; }           
            
            [AresResourceDisplayName("EqChannelModel.Fields.Filter_EqThresholdModel")]
            public AssetHealth.Models.Eq_threshold.EqThresholdModel Filter_EqThresholdModel { get; set; }   
                       
            [AresResourceDisplayName("EqChannelModel.Fields.Filter_EqWarningModel")]
            public AssetHealth.Models.Eq_warning.EqWarningModel Filter_EqWarningModel { get; set; }

        [AresResourceDisplayName("EqChannelModel.Fields.QuestionList")]
        public string[] QuestionList { get; set; }
        public SelectList Questions_SelectList { get; set; }

        [AresResourceDisplayName("EqChannelModel.Fields.Filter_EqLocation")]
        public string Filter_EqLocation_Text { get; set; }
        public SelectList Filter_EqLocation_SelectList { get; set; }
    }


}

