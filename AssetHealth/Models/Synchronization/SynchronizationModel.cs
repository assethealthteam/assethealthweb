﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using System.Web.Mvc;
using FluentValidation.Attributes;
using AssetHealth.Validators;
using AssetHealth.Models.Device;
using AssetHealth.Common.DTO.Enums;
using Ares.Web.Extensions;
using Ares.Sync.DTO.Enums;

namespace AssetHealth.Models.Synchronization
{
    [Validator(typeof(SynchronizationModelValidator))]
    public partial class SynchronizationModel : BaseAresModel //, ILocalizedModel<TopicLocalizedModel>
    {
        public SynchronizationModel()
        {
            //Locales = new List<TopicLocalizedModel>();
            SelectedTab = "1";
            
            Filter_SynchronizationDetailModel = new AssetHealth.Models.Synchronization_detail.SynchronizationDetailModel();  
                       
            
            Filter_SynchronizationDeviceModel = new AssetHealth.Models.Synchronization_device.SynchronizationDeviceModel();  
                       
        }
        
        public string SelectedTab { get; set; }

        
        [AresResourceDisplayName("SynchronizationModel.Fields.Id")]
        public int Id { get; set; }
        

        

        
        
        /*
        [AresResourceDisplayName("SynchronizationModel.Fields.Object_id")]
        public string Object_id { get; set; }
        */
        
        
        [AresResourceDisplayName("SynchronizationModel.Fields.Device_id")]
        public int? Device_id { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationModel.Fields.Udid")]
        public string Udid { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationModel.Fields.Username")]
        public string Username { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationModel.Fields.Sync_type")]
        public EnumSynchronizationSync_type Sync_type { get; set; }
        
        public virtual string EnumSync_type_Text
		{
			get 
            { 
                return EnumExtensions.ToLocalizedtring(Sync_type);
            }
		}
        
        
        [AresResourceDisplayName("SynchronizationModel.Fields.Status")]
        public EnumSynchronizationStatus Status { get; set; }
        
        public virtual string EnumStatus_Text
		{
			get 
            { 
                return EnumExtensions.ToLocalizedtring(Status);
            }
		}
        
        
        [AresResourceDisplayName("SynchronizationModel.Fields.Start_datetime")]
        public DateTime? Start_datetime { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationModel.Fields.Finish_datetime")]
        public DateTime? Finish_datetime { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationModel.Fields.Server_id")]
        public int? Server_id { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationModel.Fields.Ip_address")]
        public string Ip_address { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationModel.Fields.Client_version")]
        public string Client_version { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationModel.Fields.Server_version")]
        public string Server_version { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationModel.Fields.Description")]
        public string Description { get; set; }
        
        
        
            public DeviceModel SYNCHRONIZATION_DEVICE { get; set; }  
            
            [AresResourceDisplayName("SynchronizationModel.Fields.Device_id_Text")]
            public string Device_id_Text { get; set; }   
            public SelectList Device_id_Device_SelectList { get; set; }           
            
            [AresResourceDisplayName("SynchronizationModel.Fields.Filter_SynchronizationDetailModel")]
            public AssetHealth.Models.Synchronization_detail.SynchronizationDetailModel Filter_SynchronizationDetailModel { get; set; }   
                       
            [AresResourceDisplayName("SynchronizationModel.Fields.Filter_SynchronizationDeviceModel")]
            public AssetHealth.Models.Synchronization_device.SynchronizationDeviceModel Filter_SynchronizationDeviceModel { get; set; }


        public string[] DeviceList { get; set; }
    }


}

