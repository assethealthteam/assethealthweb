﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using System.Web.Mvc;
using FluentValidation.Attributes;
using AssetHealth.Validators;
using AssetHealth.Models.Device;
using AssetHealth.Common.DTO.Enums;
using Ares.Web.Extensions;
using Ares.Sync.DTO.Enums;

namespace AssetHealth.Models.Synchronization
{
    //[Validator(typeof(SynchronizationModelValidator))]
    public partial class PrepareSynchronizationModel : BaseAresModel //, ILocalizedModel<TopicLocalizedModel>
    {
        public PrepareSynchronizationModel()
        {
            //Locales = new List<TopicLocalizedModel>();
            SelectedTab = "1";
            
            Filter_SynchronizationDetailModel = new AssetHealth.Models.Synchronization_detail.SynchronizationDetailModel();  
                       
            
            Filter_SynchronizationDeviceModel = new AssetHealth.Models.Synchronization_device.SynchronizationDeviceModel();  
        }
        
        public string SelectedTab { get; set; }

        
        [AresResourceDisplayName("SynchronizationModel.Fields.Id")]
        public int Id { get; set; }

            
        [AresResourceDisplayName("SynchronizationModel.Fields.Device_id_Text")]
        public string Device_id_Text { get; set; }   
        public SelectList Device_id_Device_SelectList { get; set; }

        public SelectList Location_id_Location_SelectList { get; set; }
        public SelectList Role_id_Role_SelectList { get; set; }
        public SelectList FormQuestion_id_FormQuestion_SelectList { get; set; }
        public SelectList FormQuestion_Version_SelectList { get; set; }
        public SelectList Form_id_Form_SelectList { get; set; }
        public SelectList Files_SelectList { get; set; }

        [AresResourceDisplayName("SynchronizationModel.Fields.Filter_SynchronizationDetailModel")]
        public AssetHealth.Models.Synchronization_detail.SynchronizationDetailModel Filter_SynchronizationDetailModel { get; set; }   
                       
        [AresResourceDisplayName("SynchronizationModel.Fields.Filter_SynchronizationDeviceModel")]
        public AssetHealth.Models.Synchronization_device.SynchronizationDeviceModel Filter_SynchronizationDeviceModel { get; set; }

        [AresResourceDisplayName("PrepareSynchronizationModel.Fields.LocationList")]
        public string[] LocationList { get; set; }
        [AresResourceDisplayName("PrepareSynchronizationModel.Fields.RoleList")]
        public string[] RoleList { get; set; }

        [AresResourceDisplayName("PrepareSynchronizationModel.Fields.DeviceList")]
        public string[] DeviceList { get; set; }

        [AresResourceDisplayName("PrepareSynchronizationModel.Fields.FormList")]
        public string[] FormList { get; set; }

        [AresResourceDisplayName("PrepareSynchronizationModel.Fields.FormQuestionList")]
        public string[] FormQuestionList { get; set; }
        [AresResourceDisplayName("PrepareSynchronizationModel.Fields.FormQuestion_VersionList")]
        public string[] FormQuestion_VersionList { get; set; }
        

        /*
        [AresResourceDisplayName("PrepareSynchronizationModel.Fields.PreferenceFileName")]
        public string PreferenceFileName { get; set; }
        */

        [AresResourceDisplayName("PrepareSynchronizationModel.Fields.FileList")]
        public string[] FileList { get; set; }

    }



}

