﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using System.Web.Mvc;
using FluentValidation.Attributes;
using AssetHealth.Validators;
using AssetHealth.Models.Device;
using AssetHealth.Common.DTO.Enums;
using Ares.Web.Extensions;
using Ares.Sync.DTO.Enums;
using AssetHealth.Models.Synchronization_detail;

namespace AssetHealth.Models.Synchronization_log
{
    [Validator(typeof(SynchronizationLogModelValidator))]
    public partial class SynchronizationLogModel : BaseAresModel //, ILocalizedModel<TopicLocalizedModel>
    {
        public SynchronizationLogModel()
        {
            //Locales = new List<TopicLocalizedModel>();
            SelectedTab = "1";
        }
        
        public string SelectedTab { get; set; }

        
        [AresResourceDisplayName("SynchronizationLogModel.Fields.Id")]
        public int Id { get; set; }
        

        
        [AresResourceDisplayName("SynchronizationLogModel.Fields.Synchronization_detail_id")]
        public int Synchronization_detail_id { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationLogModel.Fields.Status")]
        public EnumSynchronizationLogStatus? Status { get; set; }
        
        public virtual string EnumStatus_Text
		{
			get 
            { 
                return EnumExtensions.ToLocalizedtring(Status);
            }
		}
        
        
        [AresResourceDisplayName("SynchronizationLogModel.Fields.Device_id")]
        public int Device_id { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationLogModel.Fields.Client_id")]
        public int? Client_id { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationLogModel.Fields.Item")]
        public string Item { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationLogModel.Fields.Size")]
        public long? Size { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationLogModel.Fields.Message")]
        public string Message { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationLogModel.Fields.Page_index")]
        public int? Page_index { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationLogModel.Fields.Page_size")]
        public int? Page_size { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationLogModel.Fields.Size_limit")]
        public long? Size_limit { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationLogModel.Fields.Retry_count")]
        public int? Retry_count { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationLogModel.Fields.Version")]
        public int? Version { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationLogModel.Fields.Start_datetime")]
        public DateTime? Start_datetime { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationLogModel.Fields.End_datetime")]
        public DateTime? End_datetime { get; set; }
        
        
                [AresResourceDisplayName("SynchronizationLogModel.Fields.Description")]
        public string Description { get; set; }
            public DeviceModel SYNCHRONIZATION_LOG_DEVICE { get; set; }  
            
            [AresResourceDisplayName("SynchronizationLogModel.Fields.Device_id_Text")]
            public string Device_id_Text { get; set; }   
            public SelectList Device_id_Device_SelectList { get; set; }     
			
			public SynchronizationDetailModel SYNCHRONIZATION_LOG_DETAIL { get; set; }  
            
            [AresResourceDisplayName("SynchronizationLogModel.Fields.Synchronization_detail_id_Text")]
            public string Synchronization_detail_id_Text { get; set; }   
            public SelectList Synchronization_detail_id_SynchronizationDetail_SelectList { get; set; }           
                  
            

    }


}

