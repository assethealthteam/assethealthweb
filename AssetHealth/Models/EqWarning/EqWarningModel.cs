﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using System.Web.Mvc;
using FluentValidation.Attributes;
using AssetHealth.Validators;
using AssetHealth.Models.Equipment;
using AssetHealth.Models.Eq_channel;
using AssetHealth.Common.DTO.Enums;
using Ares.Web.Extensions;
using Ares.Web.Models.Parameter_item;

namespace AssetHealth.Models.Eq_warning
{
    [Validator(typeof(EqWarningModelValidator))]
    public partial class EqWarningModel : BaseAresModel //, ILocalizedModel<TopicLocalizedModel>
    {
        public EqWarningModel()
        {
            //Locales = new List<TopicLocalizedModel>();
            SelectedTab = "1";
        }
        
        public string SelectedTab { get; set; }

        
        [AresResourceDisplayName("EqWarningModel.Fields.Id")]
        public int Id { get; set; }
        

        
        [AresResourceDisplayName("EqWarningModel.Fields.Object_id")]
        public string Object_id { get; set; }
        
        
        
        [AresResourceDisplayName("EqWarningModel.Fields.Read_no")]
        public long? Read_no { get; set; }
        
        
        
        [AresResourceDisplayName("EqWarningModel.Fields.Equipment_id")]
        public int? Equipment_id { get; set; }
        
        
        
        [AresResourceDisplayName("EqWarningModel.Fields.Eq_channel_id")]
        public int? Eq_channel_id { get; set; }
        
        
        
        [AresResourceDisplayName("EqWarningModel.Fields.Real_value")]
        public string Real_value { get; set; }
        
        
        
        [AresResourceDisplayName("EqWarningModel.Fields.Io_real_value")]
        public double? Io_real_value { get; set; }
        
        
        
        [AresResourceDisplayName("EqWarningModel.Fields.Io_input_value")]
        public double? Io_input_value { get; set; }
        
        
        
        [AresResourceDisplayName("EqWarningModel.Fields.Lower_limit")]
        public double? Lower_limit { get; set; }
        
        
        
        [AresResourceDisplayName("EqWarningModel.Fields.Upper_limit")]
        public double? Upper_limit { get; set; }
        
        
        
        [AresResourceDisplayName("EqWarningModel.Fields.Level_increase_repeat_times")]
        public int? Level_increase_repeat_times { get; set; }
        
        
        
        [AresResourceDisplayName("EqWarningModel.Fields.Level_decrease_repeat_times")]
        public int? Level_decrease_repeat_times { get; set; }
        
        
        
        [AresResourceDisplayName("EqWarningModel.Fields.Strategy")]
        public int? Strategy { get; set; }
        
        
        
        [AresResourceDisplayName("EqWarningModel.Fields.Repeat_count")]
        public int? Repeat_count { get; set; }
        
        
        
        [AresResourceDisplayName("EqWarningModel.Fields.Pre_warning_type")]
        public int? Pre_warning_type { get; set; }
        
        
        
        [AresResourceDisplayName("EqWarningModel.Fields.Warning_type")]
        public int? Warning_type { get; set; }
        
        
        
        [AresResourceDisplayName("EqWarningModel.Fields.Active")]
        public bool Active { get; set; }
        
        
        
        [AresResourceDisplayName("EqWarningModel.Fields.Description")]
        public string Description { get; set; }
        
        
        
        [AresResourceDisplayName("EqWarningModel.Fields.Last_stop_datetime")]
        public string Last_stop_datetime { get; set; }
        
        
        
            public ParameterItemModel WARNING_PRE_WARNING_PARAMITEMS { get; set; }  
            
            [AresResourceDisplayName("EqWarningModel.Fields.Pre_warning_type_Text")]
            public string Pre_warning_type_Text { get; set; }   
            public SelectList Pre_warning_type_ParameterItem_SelectList { get; set; }           
            
            public ParameterItemModel WARNING_STRATEGY_PARAMITEMS { get; set; }  
            
            [AresResourceDisplayName("EqWarningModel.Fields.Strategy_Text")]
            public string Strategy_Text { get; set; }   
            public SelectList Strategy_ParameterItem_SelectList { get; set; }           
            
            public ParameterItemModel WARNING_WARNING_PARAMITEMS { get; set; }  
            
            [AresResourceDisplayName("EqWarningModel.Fields.Warning_type_Text")]
            public string Warning_type_Text { get; set; }   
            public SelectList Warning_type_ParameterItem_SelectList { get; set; }           
            
            public EquipmentModel Warning_EQUIPMENT { get; set; }  
            
            [AresResourceDisplayName("EqWarningModel.Fields.Equipment_id_Text")]
            public string Equipment_id_Text { get; set; }   
            public SelectList Equipment_id_Equipment_SelectList { get; set; }           
            
            public EqChannelModel Warning_EQ_CHANNEL { get; set; }  
            
            [AresResourceDisplayName("EqWarningModel.Fields.Eq_channel_id_Text")]
            public string Eq_channel_id_Text { get; set; }   
            public SelectList Eq_channel_id_EqChannel_SelectList { get; set; }


        public string LocationName { get; set; }
        public int? SensorType { get; set; }
        public int? LocationParentId { get; set; }


        [AresResourceDisplayName("EqWarningModel.Fields.Eq_location_id_Text")]
        public string Eq_location_id_Text { get; set; }

        public SelectList Eq_location_id_EqLocation_SelectList { get; set; }


    }


}

