﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using System.Web.Mvc;
using FluentValidation.Attributes;
using AssetHealth.Validators;
using AssetHealth.Models.Form_question;
using AssetHealth.Models.Form;

using AssetHealth.Common.DTO.Enums;
using Ares.Web.Extensions;
using Ares.Web.Models.Parameter_item;
using Ares.Web.Models.Parameter_group;
using AssetHealth.Models.Eq_channel;
using Ares.Web.Svc.Media;

namespace AssetHealth.Models.Form_question
{
    [Validator(typeof(FormQuestionModelValidator))]
    public partial class FormQuestionModel : BaseAresModel //, ILocalizedModel<TopicLocalizedModel>
    {
        public FormQuestionModel()
        {
            //Locales = new List<TopicLocalizedModel>();
            SelectedTab = "1";
            
            Filter_FormAnswerModel = new AssetHealth.Models.Form_answer.FormAnswerModel();  
                       
            
            Filter_FormQuestionOptionModel = new AssetHealth.Models.Form_question_option.FormQuestionOptionModel();
            Filter_EqThresholdModel = new AssetHealth.Models.Eq_threshold.EqThresholdModel();

        }
        
        public string SelectedTab { get; set; }

        
        [AresResourceDisplayName("FormQuestionModel.Fields.Id")]
        public int Id { get; set; }
        

        
        [AresResourceDisplayName("FormQuestionModel.Fields.Object_id")]
        public string Object_id { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionModel.Fields.Category_id")]
        public int? Category_id { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionModel.Fields.Type_id")]
        public int? Type_id { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionModel.Fields.Form_id")]
        public int Form_id { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionModel.Fields.Derived_id")]
        public int? Derived_id { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionModel.Fields.Eq_channel_id")]
        public int? Eq_channel_id { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionModel.Fields.Code")]
        public string Code { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionModel.Fields.Title")]
        public string Title { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionModel.Fields.Sublocation_name")]
        public string Sublocation_name { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionModel.Fields.Criticality")]
        public int? Criticality { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionModel.Fields.Hint")]
        public string Hint { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionModel.Fields.Unhealthy_cause")]
        public string Unhealthy_cause { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionModel.Fields.Last_answer_id")]
        public int? Last_answer_id { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionModel.Fields.Last_answer_datetime")]
        public long? Last_answer_datetime { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionModel.Fields.Server_id")]
        public int? Server_id { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionModel.Fields.Description")]
        public string Description { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionModel.Fields.Version")]
        public int? Version { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionModel.Fields.Image_path")]
        public string Image_path { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionModel.Fields.Device_image_path")]
        public string Device_image_path { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionModel.Fields.Estimated_time")]
        public int? Estimated_time { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionModel.Fields.Method")]
        public int? Method { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionModel.Fields.Help_text")]
        [AllowHtml]
        public string Help_text { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionModel.Fields.Json")]
        public string Json { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionModel.Fields.Display_order")]
        public int? Display_order { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionModel.Fields.Step")]
        public int? Step { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionModel.Fields.Period")]
        public int? Period { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionModel.Fields.Option_group_id")]
        public int? Option_group_id { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionModel.Fields.V_required")]
        public bool V_required { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionModel.Fields.V_email")]
        public string V_email { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionModel.Fields.V_min_length")]
        public int? V_min_length { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionModel.Fields.V_max_length")]
        public int? V_max_length { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionModel.Fields.V_regex")]
        public string V_regex { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionModel.Fields.Active")]
        public bool Active { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionModel.Fields.Deleted")]
        public bool Deleted { get; set; }
        
        
        
            public EqChannelModel EQ_CHANNEL { get; set; }  
            
            [AresResourceDisplayName("FormQuestionModel.Fields.Eq_channel_id_Text")]
            public string Eq_channel_id_Text { get; set; }   
            public SelectList Eq_channel_id_EqChannel_SelectList { get; set; }           
            
            public ParameterItemModel QUESTION_CATEGORY_PARAMITEMS { get; set; }  
            
            [AresResourceDisplayName("FormQuestionModel.Fields.Category_id_Text")]
            public string Category_id_Text { get; set; }   
            public SelectList Category_id_ParameterItem_SelectList { get; set; }           
            
            public FormQuestionModel QUESTION_DERIVED { get; set; }  
            
            [AresResourceDisplayName("FormQuestionModel.Fields.Derived_id_Text")]
            public string Derived_id_Text { get; set; }   
            public SelectList Derived_id_FormQuestion_SelectList { get; set; }           
            
            public FormModel QUESTION_FORM { get; set; }  
            
            [AresResourceDisplayName("FormQuestionModel.Fields.Form_id_Text")]
            public string Form_id_Text { get; set; }   
            public SelectList Form_id_Form_SelectList { get; set; }           
            
            public ParameterGroupModel QUESTION_OPTION_PARAM_GROUP { get; set; }  
            
            [AresResourceDisplayName("FormQuestionModel.Fields.Option_group_id_Text")]
            public string Option_group_id_Text { get; set; }   
            public SelectList Option_group_id_ParameterGroup_SelectList { get; set; }           
            
            public ParameterItemModel QUESTION_PERIOD_CATEGORY_PARAMITEMS { get; set; }  
            
            [AresResourceDisplayName("FormQuestionModel.Fields.Period_Text")]
            public string Period_Text { get; set; }   
            public SelectList Period_ParameterItem_SelectList { get; set; }           
            
            public ParameterItemModel QUESTION_TYPE_CATEGORY_PARAMITEMS { get; set; }  
            
            [AresResourceDisplayName("FormQuestionModel.Fields.Type_id_Text")]
            public string Type_id_Text { get; set; }   
            public SelectList Type_id_ParameterItem_SelectList { get; set; }

        [AresResourceDisplayName("FormQuestionModel.Fields.Filter_FormAnswerModel")]
            public AssetHealth.Models.Form_answer.FormAnswerModel Filter_FormAnswerModel { get; set; }   
                       
            [AresResourceDisplayName("FormQuestionModel.Fields.Filter_FormQuestionOptionModel")]
            public AssetHealth.Models.Form_question_option.FormQuestionOptionModel Filter_FormQuestionOptionModel { get; set; }   
                       
        public ImageGallery ImageGallery { get; set; }

        public SelectList YesNo_SelectList { get; set; }

        [AresResourceDisplayName("FormQuestionModel.Fields.Eq_location_id_Text")]
        public string Eq_location_id_Text { get; set; }
        
        public SelectList Eq_location_id_EqLocation_SelectList { get; set; }


        [AresResourceDisplayName("EqChannelModel.Fields.Filter_EqThresholdModel")]
        public AssetHealth.Models.Eq_threshold.EqThresholdModel Filter_EqThresholdModel { get; set; }


    }


}

