using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using Ext.Net.MVC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetHealth.Models.Language
{
    public class LocaleResourceModel : BaseAresModel
    {
        public LocaleResourceModel()
        {
        }

        [Field(ReadOnly = true, AllowBlank = true, FieldType = typeof(Ext.Net.Hidden))]
        public virtual int Id { get; set; }

        
        public int LanguageId { get; set; }

        [AresResourceDisplayName("ResourceModel.ResourceName")]
        public string ResourceName { get; set; }

        [AresResourceDisplayName("ResourceModel.ResourceValue")]
        public string ResourceValue { get; set; }

        


    }
}