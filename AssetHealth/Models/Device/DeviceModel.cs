﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using System.Web.Mvc;
using FluentValidation.Attributes;
using AssetHealth.Validators;
using AssetHealth.Common.DTO.Enums;
using Ares.Web.Extensions;
using Ares.Sync.DTO.Enums;
using Ares.Sync.Controllers;
using Ares.Web.JSON;
using Newtonsoft.Json;

namespace AssetHealth.Models.Device
{
    [Validator(typeof(DeviceModelValidator))]
    public partial class DeviceModel : BaseAresModel //, ILocalizedModel<TopicLocalizedModel>
    {
        public DeviceModel()
        {
            //Locales = new List<TopicLocalizedModel>();
            SelectedTab = "1";
            
            Filter_FormAnswerModel = new AssetHealth.Models.Form_answer.FormAnswerModel();  
                       
            
            Filter_FormAnswerRoundModel = new AssetHealth.Models.Form_answer_round.FormAnswerRoundModel();  
                       
            
            Filter_SynchronizationModel = new AssetHealth.Models.Synchronization.SynchronizationModel();  
                       
            
            Filter_SynchronizationDeviceModel = new AssetHealth.Models.Synchronization_device.SynchronizationDeviceModel();  
                       
            
            Filter_SynchronizationLogModel = new AssetHealth.Models.Synchronization_log.SynchronizationLogModel();  
                       
        }
        
        public string SelectedTab { get; set; }

        
        [AresResourceDisplayName("DeviceModel.Fields.Id")]
        public int Id { get; set; }
        

        
        [AresResourceDisplayName("DeviceModel.Fields.Device_type")]
        public EnumDeviceDevice_type? Device_type { get; set; }
        
        public virtual string EnumDevice_type_Text
		{
			get 
            { 
                return EnumExtensions.ToLocalizedtring(Device_type);
            }
		}
        
        
        [AresResourceDisplayName("DeviceModel.Fields.Udid")]
        public string Udid { get; set; }

        [AresResourceDisplayName("DeviceModel.Fields.Imei_number")]
        public string Imei_number { get; set; }



        [AresResourceDisplayName("DeviceModel.Fields.Device_name")]
        public string Device_name { get; set; }
        
        
        
        [AresResourceDisplayName("DeviceModel.Fields.Device_model")]
        public string Device_model { get; set; }

        [AresResourceDisplayName("DeviceModel.Fields.Package_info")]
        public string Package_info { get; set; }

        public List<AutoUpdateEntity> AutoUpdateEntities { get; set; }

        [AresResourceDisplayName("DeviceModel.Fields.AssetHealth_Version")]
        public string AssetHealth_Version
        {
            get
            {
                AutoUpdateEntities = AutoUpdateEntities == null && !string.IsNullOrEmpty(Package_info) ? JsonConvert.DeserializeObject<Ares.Web.Mvc.Entities.BaseRequest<List<AutoUpdateEntity>>>(Package_info, new AresJsonSerializerSettings()).Data : AutoUpdateEntities;

                if (AutoUpdateEntities != null && AutoUpdateEntities.Exists(e => !string.IsNullOrEmpty(e.appname) && e.appname.Contains("com.improvative.assethealth")))
                    return AutoUpdateEntities.FirstOrDefault(e => !string.IsNullOrEmpty(e.appname) && e.appname.Contains("com.improvative.assethealth")).version;

                return "";
            }
        }

        [AresResourceDisplayName("DeviceModel.Fields.TechService_Version")]
        public string TechService_Version
        {
            get
            {
                AutoUpdateEntities = AutoUpdateEntities == null && !string.IsNullOrEmpty(Package_info) ? JsonConvert.DeserializeObject<Ares.Web.Mvc.Entities.BaseRequest<List<AutoUpdateEntity>>>(Package_info, new AresJsonSerializerSettings()).Data : AutoUpdateEntities;

                if (AutoUpdateEntities != null && AutoUpdateEntities.Exists(e => !string.IsNullOrEmpty(e.appname) && e.appname.Contains("com.improvative.techservice")))
                    return AutoUpdateEntities.FirstOrDefault(e => !string.IsNullOrEmpty(e.appname) && e.appname.Contains("com.improvative.techservice")).version;

                return "";
            }
        }
        



        [AresResourceDisplayName("DeviceModel.Fields.Platform_type")]
        public EnumDevicePlatform_type Platform_type { get; set; }
        
        public virtual string EnumPlatform_type_Text
		{
			get 
            { 
                return EnumExtensions.ToLocalizedtring(Platform_type);
            }
		}
        
        
        [AresResourceDisplayName("DeviceModel.Fields.Os_version")]
        public string Os_version { get; set; }
        
        
        
        [AresResourceDisplayName("DeviceModel.Fields.Current_carrier_network")]
        public string Current_carrier_network { get; set; }
        
        
        
        [AresResourceDisplayName("DeviceModel.Fields.Last_contact_time")]
        public DateTime? Last_contact_time { get; set; }
        
        
        
        [AresResourceDisplayName("DeviceModel.Fields.Battery_level")]
        public string Battery_level { get; set; }
        
        
        
        [AresResourceDisplayName("DeviceModel.Fields.Scan_status")]
        public EnumDeviceScan_status? Scan_status { get; set; }
        
        public virtual string EnumScan_status_Text
		{
			get 
            { 
                return EnumExtensions.ToLocalizedtring(Scan_status);
            }
		}
        
        
        [AresResourceDisplayName("DeviceModel.Fields.Agent_type")]
        public EnumDeviceAgent_type? Agent_type { get; set; }
        
        public virtual string EnumAgent_type_Text
		{
			get 
            { 
                return EnumExtensions.ToLocalizedtring(Agent_type);
            }
		}
        
        
        [AresResourceDisplayName("DeviceModel.Fields.Username")]
        public string Username { get; set; }
        
        
        
        [AresResourceDisplayName("DeviceModel.Fields.Mobile_number")]
        public string Mobile_number { get; set; }
        
        
        
        [AresResourceDisplayName("DeviceModel.Fields.Image_path")]
        public string Image_path { get; set; }
        
        
        
            [AresResourceDisplayName("DeviceModel.Fields.Filter_FormAnswerModel")]
            public AssetHealth.Models.Form_answer.FormAnswerModel Filter_FormAnswerModel { get; set; }   
                       
            [AresResourceDisplayName("DeviceModel.Fields.Filter_FormAnswerRoundModel")]
            public AssetHealth.Models.Form_answer_round.FormAnswerRoundModel Filter_FormAnswerRoundModel { get; set; }   
                       
            [AresResourceDisplayName("DeviceModel.Fields.Filter_SynchronizationModel")]
            public AssetHealth.Models.Synchronization.SynchronizationModel Filter_SynchronizationModel { get; set; }   
                       
            [AresResourceDisplayName("DeviceModel.Fields.Filter_SynchronizationDeviceModel")]
            public AssetHealth.Models.Synchronization_device.SynchronizationDeviceModel Filter_SynchronizationDeviceModel { get; set; }   
                       
            [AresResourceDisplayName("DeviceModel.Fields.Filter_SynchronizationLogModel")]
            public AssetHealth.Models.Synchronization_log.SynchronizationLogModel Filter_SynchronizationLogModel { get; set; }


        [AresResourceDisplayName("DeviceModel.Fields.UserList")]
        public string[] UserList { get; set; }
        public SelectList Users_SelectList { get; set; }


    }


}

