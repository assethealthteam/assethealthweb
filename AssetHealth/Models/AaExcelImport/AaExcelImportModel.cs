﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using System.Web.Mvc;
using FluentValidation.Attributes;
using AssetHealth.Validators;
using Ares.Sync.Common.DTO.Enums;
using Ares.Web.Extensions;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace AssetHealth.Models.Aa_excel_import
{
    [Validator(typeof(AaExcelImportModelValidator))]
    public partial class AaExcelImportModel : BaseAresModel //, ILocalizedModel<TopicLocalizedModel>
    {
        public AaExcelImportModel()
        {
            //Locales = new List<TopicLocalizedModel>();
            SelectedTab = "1";
        }

        [ScriptIgnore]
        [JsonIgnore]
        public string SelectedTab { get; set; }

        
        [AresResourceDisplayName("AaExcelImportModel.Fields.Id")]
        public int Id { get; set; }
        

        
        [AresResourceDisplayName("AaExcelImportModel.Fields.Location_name")]
        public string Location_name { get; set; }
        
        
        
        [AresResourceDisplayName("AaExcelImportModel.Fields.Sublocation_name")]
        public string Sublocation_name { get; set; }
        
        
        
        [AresResourceDisplayName("AaExcelImportModel.Fields.Equipment_name")]
        public string Equipment_name { get; set; }
        
        
        
        [AresResourceDisplayName("AaExcelImportModel.Fields.Object_id")]
        public string Object_id { get; set; }
        
        
        
        [AresResourceDisplayName("AaExcelImportModel.Fields.Period")]
        public string Period { get; set; }
        
        
        
        [AresResourceDisplayName("AaExcelImportModel.Fields.Category")]
        public string Category { get; set; }
        
        
        
        [AresResourceDisplayName("AaExcelImportModel.Fields.Question_type")]
        public string Question_type { get; set; }
        
        
        
        [AresResourceDisplayName("AaExcelImportModel.Fields.Code")]
        public string Code { get; set; }
        
        
        
        [AresResourceDisplayName("AaExcelImportModel.Fields.Title")]
        public string Title { get; set; }
        
        
        
        [AresResourceDisplayName("AaExcelImportModel.Fields.Hint")]
        [ScriptIgnore]
        [JsonIgnore]
        public string Hint { get; set; }
        
        
        
        [AresResourceDisplayName("AaExcelImportModel.Fields.Display_order")]
        public string Display_order { get; set; }
        
        
        
        [AresResourceDisplayName("AaExcelImportModel.Fields.Unhealthy_cause")]
        public string Unhealthy_cause { get; set; }
        
        
        
        [AresResourceDisplayName("AaExcelImportModel.Fields.Threshold_repetition_count")]
        public string Threshold_repetition_count { get; set; }
        
        
        
        [AresResourceDisplayName("AaExcelImportModel.Fields.Answer_deadline_day_count")]
        public string Answer_deadline_day_count { get; set; }
        
        
        
        [AresResourceDisplayName("AaExcelImportModel.Fields.Answer_deadline_hour_count")]
        public string Answer_deadline_hour_count { get; set; }
        
        
        
        [AresResourceDisplayName("AaExcelImportModel.Fields.Criticality")]
        public string Criticality { get; set; }
        
        
        
        [AresResourceDisplayName("AaExcelImportModel.Fields.Is_deleted")]
        public string Is_deleted { get; set; }
        
        
        
        [AresResourceDisplayName("AaExcelImportModel.Fields.Warning_level")]
        public string Warning_level { get; set; }
        
        
        
        [AresResourceDisplayName("AaExcelImportModel.Fields.Alarm_level")]
        public string Alarm_level { get; set; }
        
        
        
        [AresResourceDisplayName("AaExcelImportModel.Fields.Location_id")]
        [ScriptIgnore]
        [JsonIgnore]
        public int? Location_id { get; set; }
        
        
        
        [AresResourceDisplayName("AaExcelImportModel.Fields.Equipment_id")]
        [ScriptIgnore]
        [JsonIgnore]
        public int? Equipment_id { get; set; }
        
        
        
        [AresResourceDisplayName("AaExcelImportModel.Fields.Period_id")]
        [ScriptIgnore]
        [JsonIgnore]
        public int? Period_id { get; set; }
        
        
        
        [AresResourceDisplayName("AaExcelImportModel.Fields.Category_id")]
        [ScriptIgnore]
        [JsonIgnore]
        public int? Category_id { get; set; }
        
        
        
        [AresResourceDisplayName("AaExcelImportModel.Fields.Questiontype_id")]
        [ScriptIgnore]
        [JsonIgnore]
        public int? Questiontype_id { get; set; }
        

        
        
        [AresResourceDisplayName("AaExcelImportModel.Fields.Form_id")]
        [ScriptIgnore]
        [JsonIgnore]
        public int? Form_id { get; set; }
        
        
        
        [AresResourceDisplayName("AaExcelImportModel.Fields.Formquestion_id")]
        [ScriptIgnore]
        [JsonIgnore]
        public int? Formquestion_id { get; set; }
        
        
        
        [AresResourceDisplayName("AaExcelImportModel.Fields.Eq_channel_id")]
        [ScriptIgnore]
        [JsonIgnore]
        public int? Eq_channel_id { get; set; }
        
        
        

    }


    public class SpreadsheetSubmitViewModel
    {
        public IList<AaExcelImportModel> Created { get; set; }

        public IList<AaExcelImportModel> Destroyed { get; set; }

        public IList<AaExcelImportModel> Updated { get; set; }
    }

}
