﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using System.Web.Mvc;
using FluentValidation.Attributes;
using AssetHealth.Validators;
using AssetHealth.Models.Form_question_option;
using AssetHealth.Models.Form_question;
using AssetHealth.Common.DTO.Enums;
using Ares.Web.Extensions;

namespace AssetHealth.Models.Form_question_option
{
    [Validator(typeof(FormQuestionOptionModelValidator))]
    public partial class FormQuestionOptionModel : BaseAresModel //, ILocalizedModel<TopicLocalizedModel>
    {
        public FormQuestionOptionModel()
        {
            //Locales = new List<TopicLocalizedModel>();
            SelectedTab = "1";
        }
        
        public string SelectedTab { get; set; }

        
        [AresResourceDisplayName("FormQuestionOptionModel.Fields.Id")]
        public int Id { get; set; }
        

        
        [AresResourceDisplayName("FormQuestionOptionModel.Fields.Object_id")]
        public string Object_id { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionOptionModel.Fields.Form_question_id")]
        public int Form_question_id { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionOptionModel.Fields.Derived_id")]
        public int? Derived_id { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionOptionModel.Fields.Code")]
        public string Code { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionOptionModel.Fields.Title")]
        public string Title { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionOptionModel.Fields.Hint")]
        public string Hint { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionOptionModel.Fields.Description")]
        public string Description { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionOptionModel.Fields.Version")]
        public int? Version { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionOptionModel.Fields.Image_path")]
        public string Image_path { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionOptionModel.Fields.Device_image_path")]
        public string Device_image_path { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionOptionModel.Fields.Estimated_time")]
        public int? Estimated_time { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionOptionModel.Fields.Video_path")]
        public string Video_path { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionOptionModel.Fields.Device_video_path")]
        public string Device_video_path { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionOptionModel.Fields.Method")]
        public int? Method { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionOptionModel.Fields.Display_order")]
        public int? Display_order { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionOptionModel.Fields.Period")]
        public string Period { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionOptionModel.Fields.Active")]
        public bool Active { get; set; }
        
        
        
        [AresResourceDisplayName("FormQuestionOptionModel.Fields.Deleted")]
        public bool Deleted { get; set; }
        
        
        
            public FormQuestionOptionModel QUESTION_OPTION_DERIVED { get; set; }  
            
            [AresResourceDisplayName("FormQuestionOptionModel.Fields.Derived_id_Text")]
            public string Derived_id_Text { get; set; }   
            public SelectList Derived_id_FormQuestionOption_SelectList { get; set; }           
            
            public FormQuestionModel QUESTION_OPTION_QUESTION { get; set; }  
            
            [AresResourceDisplayName("FormQuestionOptionModel.Fields.Form_question_id_Text")]
            public string Form_question_id_Text { get; set; }   
            public SelectList Form_question_id_FormQuestion_SelectList { get; set; }           
            

    }


}

