﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetHealth.Models
{
    public class BootstrapDropdownItemModel
    {
        public string link { get; set; }
        public string icon { get; set; }
        public string text { get; set; }
        public bool selected { get; set; }


    }
}