﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using System.Web.Mvc;
using FluentValidation.Attributes;
using AssetHealth.Validators;
using AssetHealth.Models.Synchronization;
using AssetHealth.Common.DTO.Enums;
using Ares.Web.Extensions;
using Ares.Sync.DTO.Enums;
using Ares.Sync.Common.DTO.Enums;

namespace AssetHealth.Models.Synchronization_detail
{
    [Validator(typeof(SynchronizationDetailModelValidator))]
    public partial class SynchronizationDetailModel : BaseAresModel //, ILocalizedModel<TopicLocalizedModel>
    {
        public SynchronizationDetailModel()
        {
            //Locales = new List<TopicLocalizedModel>();
            SelectedTab = "1";
            
            Filter_SynchronizationLogModel = new AssetHealth.Models.Synchronization_log.SynchronizationLogModel();  
                       
        }
        
        public string SelectedTab { get; set; }

        
        [AresResourceDisplayName("SynchronizationDetailModel.Fields.Id")]
        public int Id { get; set; }
        

        
        
        /*
        
        [AresResourceDisplayName("SynchronizationDetailModel.Fields.Object_id")]
        public string Object_id { get; set; }
        */
        
        
        [AresResourceDisplayName("SynchronizationDetailModel.Fields.Synchronization_id")]
        public int Synchronization_id { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationDetailModel.Fields.Status")]
        public EnumSynchronizationDetailStatus? Status { get; set; }
        
        public virtual string EnumStatus_Text
		{
			get 
            { 
                return EnumExtensions.ToLocalizedtring(Status);
            }
		}
        
        
        [AresResourceDisplayName("SynchronizationDetailModel.Fields.Table_name")]
        public string Table_name { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationDetailModel.Fields.Server_id")]
        public int? Server_id { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationDetailModel.Fields.Client_id")]
        public int? Client_id { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationDetailModel.Fields.Transaction_id")]
        public int? Transaction_id { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationDetailModel.Fields.Operation_type")]
        public EnumSynchronizationDetailOperation_type? Operation_type { get; set; }
        
        public virtual string EnumOperation_type_Text
		{
			get 
            { 
                return EnumExtensions.ToLocalizedtring(Operation_type);
            }
		}
        
        
        [AresResourceDisplayName("SynchronizationDetailModel.Fields.Result")]
        public int? Result { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationDetailModel.Fields.Message")]
        public string Message { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationDetailModel.Fields.Item")]
        public string Item { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationDetailModel.Fields.Size")]
        public long? Size { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationDetailModel.Fields.Order")]
        public int? Order { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationDetailModel.Fields.Start_datetime")]
        public DateTime? Start_datetime { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationDetailModel.Fields.End_datetime")]
        public DateTime? End_datetime { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationDetailModel.Fields.Last_exec_datetime")]
        public DateTime? Last_exec_datetime { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationDetailModel.Fields.Retry_count")]
        public int? Retry_count { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationDetailModel.Fields.Page_index")]
        public int? Page_index { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationDetailModel.Fields.Page_size")]
        public int? Page_size { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationDetailModel.Fields.Page_count")]
        public int? Page_count { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationDetailModel.Fields.Total_item_count")]
        public long? Total_item_count { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationDetailModel.Fields.Size_limit")]
        public long? Size_limit { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationDetailModel.Fields.Description")]
        public string Description { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationDetailModel.Fields.Sync_detail_server_id")]
        public int? Sync_detail_server_id { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationDetailModel.Fields.Notes")]
        public string Notes { get; set; }
        
        
        
        [AresResourceDisplayName("SynchronizationDetailModel.Fields.Active")]
        public bool Active { get; set; }
        
        
        
            public SynchronizationModel SYNCHRONIZATION_DETAIL { get; set; }  
            
            [AresResourceDisplayName("SynchronizationDetailModel.Fields.Synchronization_id_Text")]
            public string Synchronization_id_Text { get; set; }   
            public SelectList Synchronization_id_Synchronization_SelectList { get; set; }           
            
            [AresResourceDisplayName("SynchronizationDetailModel.Fields.Filter_SynchronizationLogModel")]
            public AssetHealth.Models.Synchronization_log.SynchronizationLogModel Filter_SynchronizationLogModel { get; set; }   
                       

    }


}

