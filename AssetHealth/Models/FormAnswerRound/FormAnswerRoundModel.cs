﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using System.Web.Mvc;
using FluentValidation.Attributes;

using AssetHealth.Common.Common.DTO.Enums;
using Ares.Web.Extensions;
using Ares.Web.Models.Parameter_item;
using AssetHealth.Validators;
using AssetHealth.Models.Form;
using AssetHealth.Models.User;

namespace AssetHealth.Models.Form_answer_round
{
    [Validator(typeof(FormAnswerRoundModelValidator))]
    public partial class FormAnswerRoundModel : BaseAresModel //, ILocalizedModel<TopicLocalizedModel>
    {
        public FormAnswerRoundModel()
        {
            //Locales = new List<TopicLocalizedModel>();
            SelectedTab = "1";
            
            Filter_FormAnswerModel = new AssetHealth.Models.Form_answer.FormAnswerModel();  
                       
        }
        
        public string SelectedTab { get; set; }

        
        [AresResourceDisplayName("FormAnswerRoundModel.Fields.Id")]
        public int Id { get; set; }
        

        
        [AresResourceDisplayName("FormAnswerRoundModel.Fields.Object_id")]
        public string Object_id { get; set; }
        
        
        
        [AresResourceDisplayName("FormAnswerRoundModel.Fields.Device_id")]
        public int? Device_id { get; set; }
        
        
        
        [AresResourceDisplayName("FormAnswerRoundModel.Fields.Form_id")]
        public int? Form_id { get; set; }
        
        
        
        [AresResourceDisplayName("FormAnswerRoundModel.Fields.Location_id")]
        public int? Location_id { get; set; }
        
        
        
        [AresResourceDisplayName("FormAnswerRoundModel.Fields.Equipment_id")]
        public int? Equipment_id { get; set; }
        
        
        
        [AresResourceDisplayName("FormAnswerRoundModel.Fields.Role_id")]
        public int? Role_id { get; set; }
        
        
        
        [AresResourceDisplayName("FormAnswerRoundModel.Fields.Username")]
        public string Username { get; set; }
        
        
        
        [AresResourceDisplayName("FormAnswerRoundModel.Fields.Read_no")]
        public long? Read_no { get; set; }
        
        
        
        [AresResourceDisplayName("FormAnswerRoundModel.Fields.Start_read_datetime")]
        public DateTime? Start_read_datetime { get; set; }
        
        
        
        [AresResourceDisplayName("FormAnswerRoundModel.Fields.End_read_datetime")]
        public DateTime? End_read_datetime { get; set; }
        
        
        
        [AresResourceDisplayName("FormAnswerRoundModel.Fields.Last_read_datetime")]
        public DateTime? Last_read_datetime { get; set; }
        
        
        
        [AresResourceDisplayName("FormAnswerRoundModel.Fields.Warning_type")]
        public int? Warning_type { get; set; }
        
        
        
        [AresResourceDisplayName("FormAnswerRoundModel.Fields.Period")]
        public int? Period { get; set; }
        
        
        
        [AresResourceDisplayName("FormAnswerRoundModel.Fields.Json")]
        public string Json { get; set; }
        
        
        
        [AresResourceDisplayName("FormAnswerRoundModel.Fields.Start_count")]
        public int? Start_count { get; set; }
        
        
        
        [AresResourceDisplayName("FormAnswerRoundModel.Fields.File_count")]
        public int? File_count { get; set; }
        
        
        
        [AresResourceDisplayName("FormAnswerRoundModel.Fields.Required_read_datetime")]
        public DateTime? Required_read_datetime { get; set; }
        
        
        
        [AresResourceDisplayName("FormAnswerRoundModel.Fields.Description")]
        public string Description { get; set; }
        
        
        
        [AresResourceDisplayName("FormAnswerRoundModel.Fields.Notes")]
        public string Notes { get; set; }
        
        
        /*
            public DeviceModel ANSWER_ROUND_DEVICE { get; set; }  
            
            [AresResourceDisplayName("FormAnswerRoundModel.Fields.Device_id_Text")]
            public string Device_id_Text { get; set; }   
            public SelectList Device_id_Device_SelectList { get; set; }           
            */
			public RoleModel ANSWER_ROUND_ROLE { get; set; }  
            
            [AresResourceDisplayName("FormAnswerRoundModel.Fields.Role_id_Text")]
            public string Role_id_Text { get; set; }   
            public SelectList Role_id_Role_SelectList { get; set; } 
			
            public FormModel ANSWER_ROUND_FORM { get; set; }  
            
            [AresResourceDisplayName("FormAnswerRoundModel.Fields.Form_id_Text")]
            public string Form_id_Text { get; set; }   
            public SelectList Form_id_Form_SelectList { get; set; }           
            
            public ParameterItemModel ANSWER_ROUND_PERIOD_PARAMITEMS { get; set; }  
            
            [AresResourceDisplayName("FormAnswerRoundModel.Fields.Period_Text")]
            public string Period_Text { get; set; }   
            public SelectList Period_ParameterItem_SelectList { get; set; }           
            
            public ParameterItemModel ANSWER_ROUND_WARNING_PARAMITEMS { get; set; }  
            
            [AresResourceDisplayName("FormAnswerRoundModel.Fields.Warning_type_Text")]
            public string Warning_type_Text { get; set; }   
            public SelectList Warning_type_ParameterItem_SelectList { get; set; }           
            
            [AresResourceDisplayName("FormAnswerRoundModel.Fields.Filter_FormAnswerModel")]
            public AssetHealth.Models.Form_answer.FormAnswerModel Filter_FormAnswerModel { get; set; }   
                       

    }


}

