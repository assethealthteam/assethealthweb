using Ares.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetHealth.Models.Common
{
    public class UploadExcelModel : BaseAresModel
    {
        public UploadExcelModel()
        {
            SelectedTab = "1";
        }
        public string SelectedTab { get; set; }
        public string SheetName { get; set; }


    }
}