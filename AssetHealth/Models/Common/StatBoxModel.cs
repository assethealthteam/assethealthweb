using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;

namespace AssetHealth.Models.Common
{
    public class StatBoxModel
    {
        public StatBoxModel()
        {
            
        }

        public string StatTitle { get; set; }
        public string StatSmallTitle { get; set; }
        public string ProgressTitle { get; set; }
        public int Percentage { get; set; }
        public string StatValue { get; set; }
        /// <summary>
        /// green-sharp, red-haze, blue-sharp, purple-soft
        /// </summary>
        public string CssColorName{ get; set; }
        public string CssIcon { get; set; }

    }
}