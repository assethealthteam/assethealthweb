using Ares.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetHealth.Models.Common
{
    public class LanguageSelectorModel : BaseAresModel
    {
        public LanguageSelectorModel()
        {
            AvailableLanguages = new List<LanguageModel>();
        }

        public IList<LanguageModel> AvailableLanguages { get; set; }

        public int CurrentLanguageId { get; set; }
        public LanguageModel CurrentLanguage { get; set; }

        public string UserFullName { get; set; }
        public string UserJobTitle { get; set; }
        public string UserThumbPictureUrl { get; set; }

    }
}