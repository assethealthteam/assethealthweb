﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AssetHealth.Models.Common
{
    public class InstitutionLayoutBoxModel
    {
        public int FilterLocationId { get; set; }
        public SelectList FilterLocations { get; set; }
        public string HomePagePartialView { get; set; }
    }
}