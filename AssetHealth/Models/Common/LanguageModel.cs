using Ares.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetHealth.Models.Common
{
    public class LanguageModel : BaseAresEntityModel
    {
        public string Name { get; set; }

        public string FlagImageFileName { get; set; }
    }
}