using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetHealth.Models.Common
{
    public class ImageUploadModel
    {
        public string EntityName { get; set; }
        public int EntityId { get; set; }
        public string ImageUrl { get; set; }
        public string MimeType { get; set; }
        public string UploadFilename { get; set; }
        public string GuidFilename { get; set; }

    }
}