using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetHealth.Models.Common
{
    public class BreadcrumbModel
    {
        public BreadcrumbModel()
        {
            Breadcrumb = new List<string>();
        }

        public IList<string> Breadcrumb { get; set; }

    }
}