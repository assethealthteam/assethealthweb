using Ares.Web.Models;
using Ext.Net.MVC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using AssetHealth.Models.User;
using Newtonsoft.Json;
using Ares.Web.Razor;

namespace AssetHealth.Models.Configuration
{
    [FluentValidation.Attributes.Validator(typeof(Validators.MenuModelValidator))]
    public class MenuModel : BaseAresModel
    {
        public MenuModel()
        {

        }

        [Field(ReadOnly = true, AllowBlank = true, FieldType = typeof(Ext.Net.Hidden))]
        [AresResourceDisplayName("MenuModel.Fields.Id")]
        public virtual int Id { get; set; }

        [AresResourceDisplayName("MenuModel.Fields.Menuname")]
        public string Menuname { get; set; }
        [AresResourceDisplayName("MenuModel.Fields.Areaname")]
        public string Areaname { get; set; }
        [AresResourceDisplayName("MenuModel.Fields.Title")]
        public string Title { get; set; }
        [AresResourceDisplayName("MenuModel.Fields.Resourcename")]
        public string Resourcename { get; set; }
        [AresResourceDisplayName("MenuModel.Fields.Url")]
        public string Url { get; set; }
        [AresResourceDisplayName("MenuModel.Fields.Controller")]
        public string Controller { get; set; }
        [AresResourceDisplayName("MenuModel.Fields.Actionname")]
        public string Actionname { get; set; }
        [AresResourceDisplayName("MenuModel.Fields.Imageurl")]
        public string Imageurl { get; set; }
        [AresResourceDisplayName("MenuModel.Fields.Active")]
        public bool Active { get; set; }
        [AresResourceDisplayName("MenuModel.Fields.Deleted")]
        public bool Deleted { get; set; }
        [AresResourceDisplayName("MenuModel.Fields.Displayorder")]
        public int Displayorder { get; set; }

        [JsonIgnore]  // used in NewtonKing
        [ScriptIgnore]  // used in return Json statements in controller classses
        public List<MenuModel> SubMenus { get; set; }
        public string SelectedParentMenu { get; set; }

        public virtual int? TopParent_Id { get; set; }
        public virtual MenuModel TopParent { get; set; }
        [AresResourceDisplayName("MenuModel.Fields.Parent_Id")]
        public int? Parent_Id { get; set; }
        public MenuModel Parent { get; set; }
        public PermissionModel Permission { get; set; }


        [AresResourceDisplayName("MenuModel.Fields.ParentMenuId")]
        public int? ParentMenuId { get; set; }
        public SelectList Menus_SelectList { get; set; }

        [AresResourceDisplayName("MenuModel.Fields.PermissionId")]
        public int? Permission_Id { get; set; }
        public SelectList Permissions { get; set; }


    }
}