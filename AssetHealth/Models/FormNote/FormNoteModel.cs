﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using System.Web.Mvc;
using FluentValidation.Attributes;
using AssetHealth.Validators;
using AssetHealth.Models.Device;
using AssetHealth.Models.Form_answer_round;
using AssetHealth.Common.DTO.Enums;
using Ares.Web.Extensions;
using Ares.Web.Models.Parameter_item;

namespace AssetHealth.Models.Form_notes
{
    [Validator(typeof(FormNoteModelValidator))]
    public partial class FormNoteModel : BaseAresModel //, ILocalizedModel<TopicLocalizedModel>
    {
        public FormNoteModel()
        {
            //Locales = new List<TopicLocalizedModel>();
            SelectedTab = "1";
        }
        
        public string SelectedTab { get; set; }

        
        [AresResourceDisplayName("FormNoteModel.Fields.Id")]
        public int Id { get; set; }
        

        
        [AresResourceDisplayName("FormNoteModel.Fields.Object_id")]
        public string Object_id { get; set; }
        
        
        
        [AresResourceDisplayName("FormNoteModel.Fields.Device_id")]
        public int? Device_id { get; set; }
        
        
        
        [AresResourceDisplayName("FormNoteModel.Fields.Form_answer_round_id")]
        public int Form_answer_round_id { get; set; }
        
        
        
        [AresResourceDisplayName("FormNoteModel.Fields.Username")]
        public string Username { get; set; }
        
        
        
        [AresResourceDisplayName("FormNoteModel.Fields.Read_no")]
        public long? Read_no { get; set; }
        
        
        
        [AresResourceDisplayName("FormNoteModel.Fields.Warning_type")]
        public int? Warning_type { get; set; }
        
        
        
        [AresResourceDisplayName("FormNoteModel.Fields.Server_id")]
        public int Server_id { get; set; }
        
        
        
        [AresResourceDisplayName("FormNoteModel.Fields.Title")]
        public string Title { get; set; }
        
        
        
        [AresResourceDisplayName("FormNoteModel.Fields.Description")]
        public string Description { get; set; }
        
        
        
        [AresResourceDisplayName("FormNoteModel.Fields.Notes")]
        public string Notes { get; set; }
        
        
        
        [AresResourceDisplayName("FormNoteModel.Fields.Actual_read_datetime")]
        public DateTime? Actual_read_datetime { get; set; }
        
        
        
            public DeviceModel NOTES_DEVICE { get; set; }  
            
            [AresResourceDisplayName("FormNoteModel.Fields.Device_id_Text")]
            public string Device_id_Text { get; set; }   
            public SelectList Device_id_Device_SelectList { get; set; }           
            
            public FormAnswerRoundModel NOTES_ROUND { get; set; }  
            
            [AresResourceDisplayName("FormNoteModel.Fields.Form_answer_round_id_Text")]
            public string Form_answer_round_id_Text { get; set; }   
            public SelectList Form_answer_round_id_FormAnswerRound_SelectList { get; set; }           
            
            public ParameterItemModel NOTES_WARNING_PARAMITEMS { get; set; }  
            
            [AresResourceDisplayName("FormNoteModel.Fields.Warning_type_Text")]
            public string Warning_type_Text { get; set; }   
            public SelectList Warning_type_ParameterItem_SelectList { get; set; }           
            

    }


}

