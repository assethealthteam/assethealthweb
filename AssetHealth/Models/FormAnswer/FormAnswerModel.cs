﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using System.Web.Mvc;
using FluentValidation.Attributes;
using AssetHealth.Validators;
using AssetHealth.Models.Form_question;
using AssetHealth.Common.DTO.Enums;
using Ares.Web.Extensions;
using AssetHealth.Common.Common.DTO.Enums;
using AssetHealth.Models.Device;
using AssetHealth.Models.Form_answer_round;
using Ares.Web.Models.Parameter_item;
using Ares.Web.Models.Media;
using Ares.Web.Svc.Media;

namespace AssetHealth.Models.Form_answer
{
    [Validator(typeof(FormAnswerModelValidator))]
    public partial class FormAnswerModel : BaseAresModel //, ILocalizedModel<TopicLocalizedModel>
    {
        public FormAnswerModel()
        {
            //Locales = new List<TopicLocalizedModel>();
            SelectedTab = "1";
        }
        
        public string SelectedTab { get; set; }

        
        [AresResourceDisplayName("FormAnswerModel.Fields.Id")]
        public int Id { get; set; }
        

        
        [AresResourceDisplayName("FormAnswerModel.Fields.Object_id")]
        public string Object_id { get; set; }
        
        
        
        [AresResourceDisplayName("FormAnswerModel.Fields.Form_question_id")]
        public int Form_question_id { get; set; }
        
        
        
        [AresResourceDisplayName("FormAnswerModel.Fields.Device_id")]
        public int? Device_id { get; set; }
        
        
        
        [AresResourceDisplayName("FormAnswerModel.Fields.Form_answer_round_id")]
        public int Form_answer_round_id { get; set; }
        
        
        
        [AresResourceDisplayName("FormAnswerModel.Fields.Previous_answer")]
        public int? Previous_answer { get; set; }
        
        
        
        [AresResourceDisplayName("FormAnswerModel.Fields.Username")]
        public string Username { get; set; }
        
        
        
        [AresResourceDisplayName("FormAnswerModel.Fields.Read_no")]
        public long? Read_no { get; set; }
        
        
        
        [AresResourceDisplayName("FormAnswerModel.Fields.Warning_type")]
        public int? Warning_type { get; set; }
        
        
        
        [AresResourceDisplayName("FormAnswerModel.Fields.Answer")]
        public string Answer { get; set; }
        
        
        
        [AresResourceDisplayName("FormAnswerModel.Fields.Long_answer")]
        public string Long_answer { get; set; }
        
        
        
        [AresResourceDisplayName("FormAnswerModel.Fields.Converted_answer")]
        public string Converted_answer { get; set; }
        
        
        
        [AresResourceDisplayName("FormAnswerModel.Fields.Json")]
        public string Json { get; set; }
        
        
        
        [AresResourceDisplayName("FormAnswerModel.Fields.Required_read_datetime")]
        public DateTime? Required_read_datetime { get; set; }
        
        
        
        [AresResourceDisplayName("FormAnswerModel.Fields.Actual_read_datetime")]
        public DateTime? Actual_read_datetime { get; set; }
        
        
        
        [AresResourceDisplayName("FormAnswerModel.Fields.Description")]
        public string Description { get; set; }
        
        
        
        [AresResourceDisplayName("FormAnswerModel.Fields.Notes")]
        public string Notes { get; set; }
        
        
        
        [AresResourceDisplayName("FormAnswerModel.Fields.Udid")]
        public string Udid { get; set; }
        
        
        
            public DeviceModel ANSWER_DEVICE { get; set; }  
            
            [AresResourceDisplayName("FormAnswerModel.Fields.Device_id_Text")]
            public string Device_id_Text { get; set; }   
            public SelectList Device_id_Device_SelectList { get; set; }           
            
            public FormAnswerModel ANSWER_PREVIOUS_ANSWER { get; set; }  
            
            [AresResourceDisplayName("FormAnswerModel.Fields.Previous_answer_Text")]
            public string Previous_answer_Text { get; set; }   
            public SelectList Previous_answer_FormAnswer_SelectList { get; set; }           
            
            public FormQuestionModel ANSWER_QUESTION { get; set; }  
            
            [AresResourceDisplayName("FormAnswerModel.Fields.Form_question_id_Text")]
            public string Form_question_id_Text { get; set; }   
            public SelectList Form_question_id_FormQuestion_SelectList { get; set; }           
            
            public FormAnswerRoundModel ANSWER_ROUND { get; set; }  
            
            [AresResourceDisplayName("FormAnswerModel.Fields.Form_answer_round_id_Text")]
            public string Form_answer_round_id_Text { get; set; }   
            public SelectList Form_answer_round_id_FormAnswerRound_SelectList { get; set; }           
            
            public ParameterItemModel ANSWER_WARNING_PARAMITEMS { get; set; }  
            
            [AresResourceDisplayName("FormAnswerModel.Fields.Warning_type_Text")]
            public string Warning_type_Text { get; set; }   
            public SelectList Warning_type_ParameterItem_SelectList { get; set; }


        [AresResourceDisplayName("FormAnswerModel.Fields.Filter_StartDate")]
        public DateTime? Filter_StartDate { get; set; }

        public List<FilesModel> Files { get; set; }
        public ImageGallery ImageGallery { get; set; }

        [AresResourceDisplayName("FormAnswerModel.Fields.Filter_OnlyWithMedias")]
        public int? Filter_OnlyWithMedias { get; set; }
        public SelectList YesNo_SelectList { get; set; }
        public bool HasMedia { get; set; }

        [AresResourceDisplayName("FormAnswerModel.Fields.Filter_EqLocation")]
        public string Filter_EqLocation_Text { get; set; }
        public SelectList Filter_EqLocation_SelectList { get; set; }


    }

    public class GraphPopupModel
    {
        public int QuestionId { get; set; }
        public string QuestionTitle { get; set; }
        public int? OptionGroupId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

    }

}

