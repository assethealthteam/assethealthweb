﻿using Ares.Core.Framework;
using Ares.Core.Framework.DI;
using Ares.Core.Helpers;
using Ares.DTO;
using Ares.Svc.Event;
using Ares.Svc.Event.Entity;
using Ares.Sync.DTO;
using Ares.Sync.DTO.Enums;
using Ares.Sync.Event;
using Ares.Sync.Svc;
using Ares.UMS.Svc;
using Ares.Web.DI;
using Ares.Web.JSON;
using AssetHealth.Common.DAL;
using AssetHealth.Common.DTO;
using AssetHealth.Common.DTO.Extensions;
using AssetHealth.Models.Home;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace AssetHealth.EventConsumer
{
    public class SynchronizationEventConsumer : 
        IConsumer<EntityInserted<FormQuestion>>,
        IConsumer<EntityUpdated<FormQuestion>>,
        IConsumer<EntityInserted<Form>>,
        IConsumer<EntityUpdated<Form>>,

        IConsumer<SyncSendClientChangesFinished>
    {
        private readonly ISynchronizationDetailSvc _iSynchronizationDetailSvc;
        private readonly ISynchronizationSvc _iSynchronizationSvc;
        private readonly ISynchronizationDeviceSvc _iSynchronizationDeviceSvc;
        private readonly ISynchronizationLogSvc _iSynchronizationLogSvc;
        private readonly IDeviceSvc _iDeviceSvc;
        private readonly IWorkContext _iWorkContext;
        private readonly IAsyncRunner _asyncRunner;

        public SynchronizationEventConsumer(ISynchronizationSvc iSynchronizationSvc, ISynchronizationDetailSvc iSynchronizationDetailSvc, IAsyncRunner asyncRunner,
            ISynchronizationDeviceSvc iSynchronizationDeviceSvc, ISynchronizationLogSvc iSynchronizationLogSvc, IDeviceSvc iDeviceSvc, IWorkContext iWorkContext)
        {
            this._iSynchronizationSvc = iSynchronizationSvc;
            this._iSynchronizationDetailSvc = iSynchronizationDetailSvc;
            this._iSynchronizationDeviceSvc = iSynchronizationDeviceSvc;
            this._iSynchronizationLogSvc = iSynchronizationLogSvc;
            this._iDeviceSvc = iDeviceSvc;
            this._iWorkContext = iWorkContext;
            this._asyncRunner = asyncRunner;

        }

        public void HandleEvent(EntityInserted<FormQuestion> eventMessage)
        {
            _iSynchronizationSvc.checkSyncData(eventMessage.Entity, "form_question", EnumSynchronizationDetailOperation_type.Created);
        }

        public void HandleEvent(EntityUpdated<FormQuestion> eventMessage)
        {
            _iSynchronizationSvc.checkSyncData(eventMessage.Entity, "form_question", EnumSynchronizationDetailOperation_type.Updated);
        }

        public void HandleEvent(EntityInserted<Form> eventMessage)
        {
            _iSynchronizationSvc.checkSyncData(eventMessage.Entity, "form", EnumSynchronizationDetailOperation_type.Created);
        }

        public void HandleEvent(EntityUpdated<Form> eventMessage)
        {
            _iSynchronizationSvc.checkSyncData(eventMessage.Entity, "form", EnumSynchronizationDetailOperation_type.Updated);
        }



        public void HandleEvent(SyncSendClientChangesFinished eventMessage)
        {
            DashboardModel model = new DashboardModel();
            model.StartDate = new DateTime(model.StartDate.Year, model.StartDate.Month, model.StartDate.Day, 0, 0, 0);
            model.EndDate = new DateTime(model.EndDate.Year, model.EndDate.Month, model.EndDate.Day, 23, 59, 59);

            MySqlParameter[] sqlParams = new MySqlParameter[]
            {
              new MySqlParameter("exec_mode", 1) { Direction = ParameterDirection.Input },  // recreate
              new MySqlParameter("tenant_id", _iWorkContext.CurrentUser.tenant_id) { Direction = ParameterDirection.Input },
              new MySqlParameter("locId", model.Filter_Location_Id) { Direction = ParameterDirection.Input },
              new MySqlParameter("roleId", _iWorkContext.CurrentUser.UserRoles.Min(r => r.Id) ) { Direction = ParameterDirection.Input },
              new MySqlParameter("startDate", model.StartDateLong) { Direction = ParameterDirection.Input },
              new MySqlParameter("endDate", model.EndDateLong) { Direction = ParameterDirection.Input },
              new MySqlParameter("groupBy", 4) { Direction = ParameterDirection.Input },
              new MySqlParameter("userId", _iWorkContext.CurrentUser.Id) { Direction = ParameterDirection.Input },

              new MySqlParameter("languageId", _iWorkContext.CurrentUser.WorkingLanguage_Id) { Direction = ParameterDirection.Input },
             // new MySqlParameter("Id", locations.FirstOrDefault().Id) { Direction = ParameterDirection.Input },
            };

            foreach (EqLocation loc in _iWorkContext.CurrentUser.Location())
            {
                sqlParams[2].Value = loc.Id;
                _asyncRunner.Run<ISynchronizationSvc>(listingService => listingService.ExecuteSqlCommand(HardCodedSQL.DASHBOARD_SP_STATISTICS_CALCULATE, sqlParams));
                
            }

            
        }

    }
}