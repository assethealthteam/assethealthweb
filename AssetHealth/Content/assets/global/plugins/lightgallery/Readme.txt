JQUery Version
lightGallery-1.6.11


No-Dependency Version / Only javascript
lightgallery.js-1.1.1




scriptFiles.Add("https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.11/js/lightgallery-all.js");
    scriptFiles.Add("~/Content/assets/global/plugins/lightgallery/js/lightgallery.js");
    scriptFiles.Add("~/Content/assets/global/plugins/lightgallery/modules/lg-pager.js");
    scriptFiles.Add("~/Content/assets/global/plugins/lightgallery/modules/lg-autoplay.min.js");
    scriptFiles.Add("~/Content/assets/global/plugins/lightgallery/modules/lg-fullscreen.js");
    scriptFiles.Add("~/Content/assets/global/plugins/lightgallery/modules/lg-zoom.js");
    scriptFiles.Add("~/Content/assets/global/plugins/lightgallery/modules/lg-share.js");
    scriptFiles.Add("~/Content/assets/global/plugins/lightgallery/modules/lg-thumbnail.js");
    scriptFiles.Add("~/Content/assets/global/plugins/lightgallery/modules/lg-video.js");