﻿



using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.IO;
using System.Web.Http;
using System.Net;
using System.Web.Http.ModelBinding;
using System.Web.Security;
using Ext.Net.MVC;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using System.Web.Mvc;
using System.Linq;
using Ares.UMS.Svc.Directory;
using Ares.UMS.DTO.Users;
using Ares.UMS.Svc.Users;
using Ares.Web.Svc.Authentication;
using Ares.Web.Svc.Web;
using Ares.UMS.Svc.Common;
using Ares.UMS.DTO.Messages;
using Ares.UMS.Svc.Messages;
using Ares.Core.Helpers;
using Ares.UMS.DTO.Localization;
using Ares.UMS.Svc.Localization;
using Ares.UMS.DTO.Media;
using Ares.UMS.Svc.Media;
using Ares.Web.Models.Role;
using Ares.UMS.Svc.Security;
using Ares.UMS.DTO.Security;
using Ares.Web.Helpers;
using Ares.Web.Attributes;
using Ares.Web.Controllers;
using Ares.UMS.Svc.Configuration;
using Ares.UMS.DTO.Configuration;
using Ares.UMS.Svc;
using Ares.Web.Razor.Extensions.Datatable.Model;
using AssetHealth.Extensions;
using AssetHealth.Models.User;
using AssetHealth.Common.Svc;
using AssetHealth.Common.DTO;
using AssetHealth.Models.Configuration;
using AssetHealth.Models.Form;
using AssetHealth.Models.Synchronization_detail;
using AssetHealth.Models.Synchronization;
using AssetHealth.Models.Eq_channel;
using AssetHealth.Models.Eq_threshold;
using AssetHealth.Models.Eq_warning;
using AssetHealth.Models.Eq_location;
using AssetHealth.Models.Equipment;
using AssetHealth.Models.Form_question;
using AssetHealth.Models.Form_answer;
using AssetHealth.Models.Form_question_option;
using AssetHealth.Common.DTO.Extensions;
using System.Web.Routing;
using Ares.Web.Svc.Media;
using AssetHealth.Common.DTO.Enums;
using AssetHealth.Common.Svc.JSONGenerator;
using Ares.Core.Framework;

namespace AssetHealth.Contollers
{
	
    public partial class FormController : BaseAresController
	{
		#region Fields

        private readonly IFormSvc _iFormsvc;
        private readonly FileUploadHelper _iFileUploadHelper;

        private readonly IParameterItemSvc _iParameterItemsvc;
        private readonly IFormQuestionSvc _iFormQuestionsvc;
        private readonly IUserSvc _iUsersvc;
        private readonly IEqChannelSvc _iEqChannelsvc;
        private readonly IEqThresholdSvc _iEqThresholdsvc;
        private readonly IEqWarningSvc _iEqWarningsvc;
        private readonly IEqLocationSvc _iEqLocationsvc;
        private readonly IEquipmentSvc _iEquipmentsvc;
        private readonly IParameterGroupSvc _iParameterGroupsvc;
        private readonly IFormAnswerSvc _iFormAnswersvc;
        private readonly IFormQuestionOptionSvc _iFormQuestionOptionsvc;
        private readonly IFormAnswerRoundSvc _iFormAnswerRoundsvc;
        private readonly IRoleSvc _iRoleSvc;

        private readonly IStateProvinceSvc _stateProvinceSvc;
        private readonly ISettingSvc _settingSvc;
        private readonly FileSettings _fileSettings;
        
        private readonly IGenericAttributeSvc _genericAttributeSvc;
        private readonly ILanguageSvc _iLanguageSvc;
        private readonly ILocalizationSvc _localizationSvc;
        private readonly IPictureSvc _pictureSvc;
        private readonly IUserSvc _userSvc;
        private readonly IRoleSvc _roleSvc;
        private readonly IPermissionSvc _permissionSvc;
        private readonly IMenuSvc _menuSvc;
        private readonly IWorkContext _workContext;
        private readonly IUserRegistrationSvc _userRegistrationSvc;
        private readonly IAuthenticationSvc _authenticationSvc;
        private readonly IWorkflowMessageSvc _workflowMessageSvc;
        private readonly INewsLetterSubscriptionSvc _newsLetterSubscriptionSvc;
        private readonly IWebHelper _webHelper;
        private readonly UserSettings _userSettings;
        private readonly LocalizationSettings _localizationSettings;
        private readonly MediaSettings _mediaSettings;

	    #endregion

		#region Constructors

        public FormController(IFormSvc Formsvc,
            FileUploadHelper iFileUploadHelper,
        IParameterItemSvc ParameterItemsvc,
        IFormQuestionSvc FormQuestionsvc,
        IUserSvc Usersvc,
        IEqChannelSvc EqChannelsvc,
        IEqThresholdSvc EqThresholdsvc,
        IEqWarningSvc EqWarningsvc,
        IEqLocationSvc EqLocationsvc,
        IEquipmentSvc Equipmentsvc,
        ILanguageSvc iLanguageSvc, IRoleSvc iRoleSvc,
        IParameterGroupSvc ParameterGroupsvc,
        IFormAnswerSvc FormAnswersvc, IFormAnswerRoundSvc iFormAnswerRoundsvc,
        IFormQuestionOptionSvc FormQuestionOptionsvc,
            IStateProvinceSvc stateProvinceSvc, IGenericAttributeSvc genericAttributeSvc, ILocalizationSvc localizationSvc,  
            UserSettings userSettings, IPictureSvc pictureSvc, ISettingSvc settingSvc, FileSettings fileSettings,
            IUserSvc userSvc, IRoleSvc roleSvc, IPermissionSvc permissionSvc, IMenuSvc menuSvc, IWorkContext workContext, IUserRegistrationSvc userRegistrationSvc, IAuthenticationSvc authenticationSvc, IWorkflowMessageSvc workflowMessageSvc,
            INewsLetterSubscriptionSvc newsLetterSubscriptionSvc, IWebHelper webHelper, LocalizationSettings localizationSettings, MediaSettings mediaSettings)
		{
            this._iFormsvc = Formsvc;
            this._iFileUploadHelper = iFileUploadHelper;
            
            this._iParameterItemsvc = ParameterItemsvc;
            this._iFormQuestionsvc = FormQuestionsvc;
            this._iUsersvc = Usersvc;
            this._iEqChannelsvc = EqChannelsvc;
            this._iEqThresholdsvc = EqThresholdsvc;
            this._iEqWarningsvc = EqWarningsvc;
            this._iEqLocationsvc = EqLocationsvc;
            this._iEquipmentsvc = Equipmentsvc;
            this._iParameterGroupsvc = ParameterGroupsvc;
            this._iFormAnswersvc = FormAnswersvc;
            this._iFormQuestionOptionsvc = FormQuestionOptionsvc;
            this._settingSvc = settingSvc;
            this._fileSettings = fileSettings;
            this._iFormAnswerRoundsvc = iFormAnswerRoundsvc;
            this._iRoleSvc = iRoleSvc;

            this._stateProvinceSvc = stateProvinceSvc;
            this._genericAttributeSvc = genericAttributeSvc;
            this._iLanguageSvc = iLanguageSvc;
            this._pictureSvc = pictureSvc;
            this._localizationSvc = localizationSvc;
            this._userSettings = userSettings;
            this._userSvc = userSvc;
            this._roleSvc = roleSvc;
            this._permissionSvc = permissionSvc;
            this._menuSvc = menuSvc;
            this._workContext = workContext;
            this._userRegistrationSvc = userRegistrationSvc;
            this._authenticationSvc = authenticationSvc;
            this._workflowMessageSvc = workflowMessageSvc;
            this._newsLetterSubscriptionSvc = newsLetterSubscriptionSvc;
            this._webHelper = webHelper;
            this._localizationSettings = localizationSettings;
            this._mediaSettings = mediaSettings;
		}

		#endregion 

        #region Utilities
        
        
        #endregion

        #region Form methods

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        [AresAuthorize(ResourceKey = "Admin.Form.Read")]
        public ActionResult List()
        {
            FormModel model = new FormModel();
            //model. = _roleSvc.GetAll().ToList().Select(x => x.ToModel()).ToList();
            FillDropDowns(model);
            
            
            FillDropDownFormQuestion(model.Filter_FormQuestionModel);

            model.Active = true;
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.Form.Read")]
        public ActionResult ListFormDatatable(Ares.Web.Razor.Extensions.Datatable.Model.DTParameterModel model)
        {
            var models = _iFormsvc.GetAll();

            if (!_workContext.CurrentUser.IsAdmin())
            {
                List<int> relatedLocationIds = _workContext.CurrentUser.RelatedLocations().Select(r => r.Id).ToList();
                models = models.Where(e => relatedLocationIds.Contains(e.EQUIPMENT.Location.Id));
            }


            if (model.Search != null && !string.IsNullOrEmpty(model.Search.Value))
            {
                models = models.Where(m => m.Object_id.Contains(model.Search.Value));
            }
            if (model.IsSearchValueExist("Filter_EqLocation_Text"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Filter_EqLocation_Text"));
                models = models.Where(m => m.EQUIPMENT.Eq_location_id == tmpValue);
            }
            if (model.IsSearchValueExist("Role_id_Text"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Role_id_Text"));
                models = models.Where(m => m.Role_id == tmpValue);
            }


            if (model.IsSearchValueExist("Category_id_Text"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Category_id_Text"));
                models = models.Where(m => m.Category_id == tmpValue);
            }
            if(model.IsSearchValueExist("Derived_id_Text"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Derived_id_Text"));
                models = models.Where(m => m.Derived_id == tmpValue);
            }
            if(model.IsSearchValueExist("Language_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Language_id"));
                models = models.Where(m => m.Language_id == tmpValue);
            }
            if(model.IsSearchValueExist("Period"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Period"));
                models = models.Where(m => m.Period == tmpValue);
            }
            if(model.IsSearchValueExist("Code"))
            {
                string tmpValue = model.GetSearchValue("Code");
                models = models.Where(m => m.Code.Contains(tmpValue));
            }
            if(model.IsSearchValueExist("Title"))
            {
                string tmpValue = model.GetSearchValue("Title");
                models = models.Where(m => m.Title.Contains(tmpValue));
            }
            if(model.IsSearchValueExist("Description"))
            {
                string tmpValue = model.GetSearchValue("Description");
                models = models.Where(m => m.Description.Contains(tmpValue));
            }
            if(model.IsSearchValueExist("Version"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Version"));
                models = models.Where(m => m.Version == tmpValue);
            }
            if(model.IsSearchValueExist("Image_path"))
            {
                string tmpValue = model.GetSearchValue("Image_path");
                models = models.Where(m => m.Image_path.Contains(tmpValue));
            }
            if(model.IsSearchValueExist("Form_json"))
            {
            }
            if(model.IsSearchValueExist("Active"))
            {
                bool tmpValue = bool.Parse(model.GetSearchValue("Active"));
                models = models.Where(m => m.Active == tmpValue);
            }
            if(model.IsSearchValueExist("Deleted"))
            {
                bool tmpValue = bool.Parse(model.GetSearchValue("Deleted"));
                models = models.Where(m => m.Deleted == tmpValue);
            }
            if(model.IsSearchValueExist("Display_order"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Display_order"));
                models = models.Where(m => m.Display_order == tmpValue);
            }
            
            
            DTOrder order = null;
            if (model.Order != null && model.Order.Count() > 0)
            {
                order = model.Order.First();
                var pi = typeof(Form).GetProperty(model.Columns.ToList()[order.Column].Data);
                if(order.Dir == "asc")
                    models = models.OrderBy(x => pi.GetValue(x, null));
                else
                    models = models.OrderByDescending(x => pi.GetValue(x, null));
                
            }

            var totalRecords = models.Count();
            
            models = models.OrderBy(e => e.Id).Skip(model.Length * ((int)(Math.Ceiling((double)model.Start / model.Length)) + 1 - 1)).Take(model.Length);

            JsonResult jsonRes = Json(new
            {
                sEcho = model.Draw,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                //aaData = result
                aaData = models.ToList().Select(x => { var m = x.ToModel(); m.Derived_id_Text = x.DERIVED == null ? "" : x.DERIVED.Code; m.Category_id_Text = x.PARAMETER_ITEMS == null ? "" : x.PARAMETER_ITEMS.GetLocalized(p => p.Title); m.Period_Text = x.PERIOD_PARAMITEMS == null ? "" : x.PERIOD_PARAMITEMS.GetLocalized(p => p.Title); m.Filter_FormQuestionModel = null; m.Filter_FormAnswerRoundModel = null; m.Role_id_Text = x.ROLE != null ? x.ROLE.Rolename : ""; m.Filter_EqLocation_Text = x.EQUIPMENT.Location != null ? x.EQUIPMENT.Location.Location_name : ""; return m;})
            }, JsonRequestBehavior.AllowGet);

            return jsonRes;
        }
        
        [AresAuthorize(ResourceKey = "Admin.Form.Create")]
        public ActionResult Create()
        {
            FormModel model = new FormModel();
            FillDropDowns(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.Form.Create")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Create([Bind(Exclude = "Filter_FormQuestionModel")] FormModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {                
                var record = model.ToEntity();
                
                record.Object_id = Guid.NewGuid().ToString();
                record.Insert_user_id = _workContext.CurrentUser.Id.ToString();
                record.Insert_datetime = StringHelper.ConvertDateTimeToString(DateTime.Now);
                
            
                record = _iFormsvc.Insert(record);

                SuccessNotification(_localizationSvc.GetResource("Notifications.Form.Created"));
                
                if (continueEditing)
                    return RedirectToAction("Edit", new { Id = record.Id });

                return RedirectToAction("List");
            }
            else
            {
                FillDropDowns(model);
                return View(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.Form.Update")]
        [FormAuthorize]
        public ActionResult Edit(int id, string SelectedTab)
        {
            Form record = _iFormsvc.GetById(id);
            var model = record.ToModel();
            model.SelectedTab = SelectedTab;

            
                        
            FillDropDowns(model);
            
            FillDropDownFormQuestion(model.Filter_FormQuestionModel);

            model.Filter_FormQuestionModel.Active = true;


            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.Form.Update")]
        [FormAuthorize]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Edit([Bind(Exclude = "Filter_FormQuestionModel")] FormModel model, bool continueEditing)
        {
            var item = _iFormsvc.GetById(model.Id);
            Form originalItem = (Form)item.Clone();
            if (item == null)
                //No country found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                item = model.ToEntity(item);

                JsonFormWizard jsonWizard = new JsonFormWizard();
                item.Form_json = jsonWizard.GenerateJSONFromDTOForm(item);
                foreach(FormQuestion question in item.FORMForm_question)
                {
                    question.Json = jsonWizard.GenerateJSONFromDTOFormQuestion(question);
                }

                item.Object_id = originalItem.Object_id;
                item.Insert_user_id = originalItem.Insert_user_id;
                item.Insert_datetime = originalItem.Insert_datetime;



                item.Update_user_id = _workContext.CurrentUser.Id.ToString();
                item.Update_datetime = StringHelper.ConvertDateTimeToString(DateTime.Now);

                _iFormsvc.Update(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.Form.Updated"));
                
                if(continueEditing)
                    return RedirectToAction("Edit", new { Id = item.Id });

                return RedirectToAction("List");
            }
            else
            {
                FillDropDowns(model);
            
            FillDropDownFormQuestion(model.Filter_FormQuestionModel);
                       
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.Form.Delete")]
        [FormAuthorize]
        public ActionResult Delete(int id)
        {

            var item = _iFormsvc.GetById(id);
            if (item == null)
                return RedirectToAction("List");

            try
            {
                item.FORMForm_question.ToList().ForEach(d => _iFormQuestionsvc.Delete(d));
                
                _iFormsvc.Delete(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.Form.Deleted"));
                return RedirectToAction("List");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("Edit", new { id = item.Id });
            }
        }
        
        
        public ActionResult CreateOrUpdate(string itemId, string imageUrl, string mimeType, string uploadFilename)
        {
            FormModel response = null;
            

            int nItemId = -1;
            int.TryParse(itemId, out nItemId);

            if (nItemId != -1)
            {
                Form record = _iFormsvc.GetById(nItemId);
                response = record.ToModel();
                if (record != null) // update
                {

                   

                }
                else // insert mode (after image upload popup close)
                {
                    response = new FormModel();
                    //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() { Text = "" } };
                }
            }
            else // insert
            {
                response = new FormModel();
                //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() {Text = "" }  };
                
            }

 
            ViewBag.itemId = nItemId;

            return View(response);
        }
        
        
        public void FillDropDowns(FormModel model)
        {
            model.Filter_EqLocation_SelectList = new SelectList(_iEqLocationsvc.GetAuthorizedLocations(_workContext.CurrentUser.Id), "Id", "Location_name");

            //model.Derived_id_Form_SelectList = new SelectList(_iFormsvc.GetAllFromCache().ToList() , "Id", "Title");
            model.Role_id_Role_SelectList = new SelectList(_iRoleSvc.GetAllFromCache().Where(r => r.Systemname != EnumRoleNames.Administrators).ToList(), "Id", "Rolename");

            //model.Equipment_id_Form_SelectList = new SelectList(_iEquipmentsvc.GetAll().Where(e => e.Location.ParentLocation != null && e.Location.ParentLocation.User_From_EqLocationUserMapping.Any(u => u.Id == _workContext.CurrentUser.Id)).ToList(), "Id", "Name");
            model.Equipment_id_Form_SelectList = new SelectList(_iEquipmentsvc.GetAuthorizedEquipments(_workContext.CurrentUser.Id), "Id", "Name");

            model.Category_id_ParameterItem_SelectList = new SelectList(_iParameterItemsvc.GetAllFromCache().Where(p => p.Group.System_name == "FORM_CATEGORY_ID").ToList().Select(p => { return new { Id = p.Id, Title = p.GetLocalized(l => l.Title) }; }), "Id", "Title");

            model.Language_id_Language_SelectList = new SelectList(_iLanguageSvc.GetAllLanguages(), "Id", "Name");

            model.Period_ParameterItem_SelectList = new SelectList(_iParameterItemsvc.GetAllFromCache().Where(p => p.Group.System_name == "FORMQUESTIONPERIOD").ToList().Select(p => { return new { Id = p.Id, Title = p.GetLocalized(l => l.Title) }; }), "Id", "Title");


        }
        
        #endregion
                         



            
        #region  FormQuestion methods
        
        public void FillDropDownFormQuestion(FormQuestionModel model)
        {
            List<int> authorizedEquipments = _iEquipmentsvc.GetAuthorizedEquipments(_workContext.CurrentUser.Id).Select(e => e.Id).ToList();


            model.Category_id_ParameterItem_SelectList = new SelectList(_iParameterItemsvc.GetAllFromCache().Where(p => p.Group.System_name == "FORMQUESTIONCATEGORY").ToList().Select(p => { return new { Id = p.Id, Title = p.GetLocalized(l => l.Title) }; }), "Id", "Title");

            //model.Derived_id_FormQuestion_SelectList = new SelectList(_iFormQuestionsvc.GetAllFromCache().ToList(), "Id", "Title");

            //model.Eq_channel_id_EqChannel_SelectList = new SelectList(_iEqChannelsvc.GetAll().Where(c => c.channel_equipment.Location.User_From_EqLocationUserMapping.Any(u => u.Id == _workContext.CurrentUser.Id) ).ToList(), "Id", "Name");
            if(model.Id > 0 && model.Form_id > 0)
            {
                Form form = _iFormsvc.GetById(model.Form_id);
                model.Eq_channel_id_EqChannel_SelectList = new SelectList(_iEqChannelsvc.GetAuthorizedChannels(_workContext.CurrentUser.Id).Where(c => c.Equipment_id == form.Equipment_id), "Id", "Name");
            }

            List<SelectListItem> yesNoSelectList = new List<SelectListItem>();
            yesNoSelectList.Add(new SelectListItem() { Text = _localizationSvc.GetResource("select") });
            yesNoSelectList.Add(new SelectListItem() { Value = "1", Text = _localizationSvc.GetResource("Admin.Common.Yes") });
            yesNoSelectList.Add(new SelectListItem() { Value = "2", Text = _localizationSvc.GetResource("Admin.Common.No") });

            model.YesNo_SelectList = new SelectList(yesNoSelectList, "Value", "Text");


            model.Form_id_Form_SelectList = new SelectList(_iFormsvc.GetAllFromCache().Where(f => authorizedEquipments.Contains(f.EQUIPMENT.Id)).ToList(), "Id", "Title");

            model.Option_group_id_ParameterGroup_SelectList = new SelectList(_iParameterGroupsvc.GetAllFromCache().Where(p => p.System_name != null && p.System_name.ToUpper().StartsWith("OPTION")).ToList().Select(p => new { Id = p.Id, Name = p.GetLocalized(l => l.Name)}), "Id", "Name");

            model.Type_id_ParameterItem_SelectList = new SelectList(_iParameterItemsvc.GetAllFromCache().Where(p => p.Group.System_name == "FORMQUESTIONTYPE").ToList().Select(p => { return new { Id = p.Id, Title = p.GetLocalized(l => l.Title) }; }), "Id", "Title");

            model.Period_ParameterItem_SelectList = new SelectList(_iParameterItemsvc.GetAllFromCache().Where(p => p.Group.System_name == "FORMQUESTIONPERIOD").ToList().Select(p => { return new { Id = p.Id, Title = p.GetLocalized(l => l.Title) }; }) , "Id", "Title");
        }
        
        public ActionResult CreateOrUpdateFormQuestion(string itemId, string imageUrl, string mimeType, string uploadFilename)
        {
            FormQuestionModel response = null;


            int nItemId = -1;
            int.TryParse(itemId, out nItemId);

            if (nItemId != -1)
            {
                FormQuestion record = _iFormQuestionsvc.GetById(nItemId);
                response = record.ToModel();
                if (record != null) // update
                {



                    //response.RoleListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem { Text = "Administrators" }, new Ext.Net.ListItem { Text = "Guests" } };
                    //response.PermissionListItems = permListItems.ToArray();

                }
                else // insert mode (after image upload popup close)
                {
                    response = new FormQuestionModel();
                    //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() { Text = "" } };
                }
            }
            else // insert
            {
                response = new FormQuestionModel();
                //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() {Text = "" }  };

            }


            ViewBag.itemId = nItemId;

            return View(response);
        }
        
        [AresAuthorize(ResourceKey = "Admin.FormQuestion.Read")]
        public ActionResult ListFormQuestionDatatable(Ares.Web.Razor.Extensions.Datatable.Model.DTParameterModel model, int? FormModelId)
        {
            var models = _iFormQuestionsvc.GetAll();

            if (!_workContext.CurrentUser.IsAdmin())
            {
                List<int> relatedLocationIds = _workContext.CurrentUser.RelatedLocations().Select(r => r.Id).ToList();
                models = models.Where(e => relatedLocationIds.Contains(e.QUESTION_FORM.EQUIPMENT.Location.Id));
            }

            models = models.Where(m => m.Form_id == FormModelId);
            if (model.Search != null && !string.IsNullOrEmpty(model.Search.Value))
            {
                models = models.Where(m => m.Object_id.Contains(model.Search.Value));
            }
            
            if (model.IsSearchValueExist("Category_id_Text"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Category_id_Text"));
                models = models.Where(m => m.Category_id == tmpValue);
            }
            if (model.IsSearchValueExist("Type_id_Text"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Type_id_Text"));
                models = models.Where(m => m.Type_id == tmpValue);
            }
            if (model.IsSearchValueExist("Form_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Form_id"));
                models = models.Where(m => m.Form_id == tmpValue);
            }
            if (model.IsSearchValueExist("Derived_id_Text"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Derived_id_Text"));
                models = models.Where(m => m.Derived_id == tmpValue);
            }
            if (model.IsSearchValueExist("Code"))
            {
                string tmpValue = model.GetSearchValue("Code");
                models = models.Where(m => m.Code.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Title"))
            {
                string tmpValue = model.GetSearchValue("Title");
                models = models.Where(m => m.Title.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Hint"))
            {
                string tmpValue = model.GetSearchValue("Hint");
                models = models.Where(m => m.Hint.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Description"))
            {
                string tmpValue = model.GetSearchValue("Description");
                models = models.Where(m => m.Description.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Version"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Version"));
                models = models.Where(m => m.Version == tmpValue);
            }
            if (model.IsSearchValueExist("Image_path"))
            {
                string tmpValue = model.GetSearchValue("Image_path");
                models = models.Where(m => m.Image_path.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Device_image_path"))
            {
                string tmpValue = model.GetSearchValue("Device_image_path");
                models = models.Where(m => m.Device_image_path.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Estimated_time"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Estimated_time"));
                models = models.Where(m => m.Estimated_time == tmpValue);
            }
            if (model.IsSearchValueExist("Method"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Method"));
                models = models.Where(m => m.Method == tmpValue);
            }
            if (model.IsSearchValueExist("Help_text"))
            {
            }
            if (model.IsSearchValueExist("Json"))
            {
            }
            if (model.IsSearchValueExist("Display_order"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Display_order"));
                models = models.Where(m => m.Display_order == tmpValue);
            }
            if (model.IsSearchValueExist("Step"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Step"));
                models = models.Where(m => m.Step == tmpValue);
            }
            if (model.IsSearchValueExist("Period"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Period"));
                models = models.Where(m => m.Period == tmpValue);
            }
            if (model.IsSearchValueExist("Option_group_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Option_group_id"));
                models = models.Where(m => m.Option_group_id == tmpValue);
            }
            if (model.IsSearchValueExist("V_required"))
            {
                bool tmpValue = bool.Parse(model.GetSearchValue("V_required"));
                models = models.Where(m => m.V_required == tmpValue);
            }
            if (model.IsSearchValueExist("V_email"))
            {
                string tmpValue = model.GetSearchValue("V_email");
                models = models.Where(m => m.V_email.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("V_min_length"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("V_min_length"));
                models = models.Where(m => m.V_min_length == tmpValue);
            }
            if (model.IsSearchValueExist("V_max_length"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("V_max_length"));
                models = models.Where(m => m.V_max_length == tmpValue);
            }
            if (model.IsSearchValueExist("V_regex"))
            {
                string tmpValue = model.GetSearchValue("V_regex");
                models = models.Where(m => m.V_regex.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Active"))
            {
                bool tmpValue = bool.Parse(model.GetSearchValue("Active"));
                models = models.Where(m => m.Active == tmpValue);
            }
            if (model.IsSearchValueExist("Deleted"))
            {
                bool tmpValue = bool.Parse(model.GetSearchValue("Deleted"));
                models = models.Where(m => m.Deleted == tmpValue);
            }
            
            DTOrder order = null;
            if (model.Order != null && model.Order.Count() > 0)
            {
                order = model.Order.First();
                var pi = typeof(FormQuestion).GetProperty(model.Columns.ToList()[order.Column].Data);
                if(order.Dir == "asc")
                    models = models.OrderBy(x => pi.GetValue(x, null));
                else
                    models = models.OrderByDescending(x => pi.GetValue(x, null));
                
            }

            var totalRecords = models.Count();
            
            models = models.OrderBy(e => e.Id).Skip(model.Length * ((int)(Math.Ceiling((double)model.Start / model.Length)) + 1 - 1)).Take(model.Length);

            List<FormQuestionModel> aaData = models.ToList().Select(x => { var m = x.ToModel(); m.Period_Text = m.QUESTION_PERIOD_CATEGORY_PARAMITEMS == null ? "" : x.QUESTION_PERIOD_CATEGORY_PARAMITEMS.GetLocalized(p => p.Title);  m.Derived_id_Text = x.QUESTION_DERIVED == null ? "" : x.QUESTION_DERIVED.Code; m.Form_id_Text = x.QUESTION_FORM == null ? "" : x.QUESTION_FORM.Code; m.Category_id_Text = x.QUESTION_CATEGORY_PARAMITEMS == null ? "" : x.QUESTION_CATEGORY_PARAMITEMS.GetLocalized(p => p.Title); m.Type_id_Text = x.QUESTION_TYPE_CATEGORY_PARAMITEMS == null ? "" : x.QUESTION_TYPE_CATEGORY_PARAMITEMS.GetLocalized(p => p.Title); return m; }).ToList();
            List<ImageGallery> galleries = _iFileUploadHelper.GenerateImageGalleryJSON("Formquestion", aaData.Select(a => a.Id).ToList(), false);
            if (galleries != null && galleries.Count > 0)
                aaData.ForEach(a => a.ImageGallery = galleries.FirstOrDefault(g => g.entityId == a.Id));

            JsonResult jsonRes = Json(new
            {
                sEcho = model.Draw,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                //aaData = result
                aaData = aaData
            }, JsonRequestBehavior.AllowGet);
            jsonRes.MaxJsonLength = 50000000;
            return jsonRes;
        }
        
        [AresAuthorize(ResourceKey = "Admin.FormQuestion.Create")]
        public ActionResult CreateFormQuestion(int FormModelId)
        {
            FormQuestionModel model = new FormQuestionModel();
            model.Form_id = FormModelId;
            
            
            FillDropDownFormQuestion(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormQuestion.Create")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult CreateFormQuestion([Bind(Exclude = "Filter_FormAnswerModel,Filter_FormQuestionOptionModel")] FormQuestionModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {                
                var record = model.ToEntity();

                record.Object_id = Guid.NewGuid().ToString();
                record.Insert_user_id = _workContext.CurrentUser.Id.ToString();
                record.Insert_datetime = StringHelper.ConvertDateTimeToString(DateTime.Now);

                JsonFormWizard jsonWizard = new JsonFormWizard();

                //record.Json = jsonWizard.GenerateJSONFromDTOFormQuestion(record);
                record = _iFormQuestionsvc.Insert(record);

                record = _iFormQuestionsvc.GetById(record.Id);

                record.Json = jsonWizard.GenerateJSONFromDTOFormQuestion(record);
                _iFormQuestionsvc.Update(record);

                Form form = record.QUESTION_FORM;
                form.Form_json = jsonWizard.GenerateJSONFromDTOForm(form);
                _iFormsvc.Update(form);

                _iFormQuestionsvc.insertOrUpdateChannelAndThreshold(record);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormQuestion.Created"));
                
                if (continueEditing)
                    return RedirectToAction("EditFormQuestion", new { id = record.Id });

                return RedirectToAction("Edit", new  { id = model.Form_id, SelectedTab = "2" });
            }
            else
            {
                FillDropDownFormQuestion(model);
                return View(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        [FormQuestionAuthorize]
        [AresAuthorize(ResourceKey = "Admin.FormQuestion.Update")]
        public ActionResult EditFormQuestion(int id, string SelectedTab)
        {
            FormQuestion record = _iFormQuestionsvc.GetById(id);
            var model = record.ToModel();
            model.SelectedTab = SelectedTab;
            
            
            
            FillDropDownFormQuestion(model);
            
            FillDropDownFormAnswer(model.Filter_FormAnswerModel);
                       
            
            FillDropDownFormQuestionOption(model.Filter_FormQuestionOptionModel);
                       

            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormQuestion.Update")]
        [FormQuestionAuthorize]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult EditFormQuestion([Bind(Exclude = "Filter_FormAnswerModel,Filter_FormQuestionOptionModel")] FormQuestionModel model, bool continueEditing)
        {
            var item = _iFormQuestionsvc.GetById(model.Id);
            FormQuestion originalItem = (FormQuestion)item.Clone();
            if (item == null)
                //No country found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                item = model.ToEntity(item);

                item.Object_id = originalItem.Object_id;
                item.Insert_user_id = originalItem.Insert_user_id;
                item.Insert_datetime = originalItem.Insert_datetime;



                item.Update_user_id = _workContext.CurrentUser.Id.ToString();
                item.Update_datetime = StringHelper.ConvertDateTimeToString(DateTime.Now);

                _iFormQuestionsvc.Update(item);
                _iFormQuestionsvc.insertOrUpdateChannelAndThreshold(item);

                JsonFormWizard jsonWizard = new JsonFormWizard();
                item = _iFormQuestionsvc.GetById(model.Id);
                item.Json = jsonWizard.GenerateJSONFromDTOFormQuestion(item);
                _iFormQuestionsvc.Update(item);

                Form form = item.QUESTION_FORM;
                form.Form_json = jsonWizard.GenerateJSONFromDTOForm(form);
                _iFormsvc.Update(form);





                SuccessNotification(_localizationSvc.GetResource("Notifications.FormQuestion.Updated"));
                
                if(continueEditing)
                    return RedirectToAction("EditFormQuestion", new { id = item.Id });
                
                return RedirectToAction("Edit", new  { id = model.Form_id, SelectedTab = "2" });
            }

            //If we got this far, something failed, redisplay form
            FillDropDownFormQuestion(model);
            
            FillDropDownFormAnswer(model.Filter_FormAnswerModel);
                       
            
            FillDropDownFormQuestionOption(model.Filter_FormQuestionOptionModel);
                       
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormQuestion.Delete")]
        [FormQuestionAuthorize]
        public ActionResult DeleteFormQuestion(int id)
        {

            var item = _iFormQuestionsvc.GetById(id);
            if (item == null)
                return RedirectToAction("List");

            try
            {
                item.FORM_QUESTIONForm_answer.ToList().ForEach(d => _iFormAnswersvc.Delete(d));
                item.FORM_QUESTIONForm_question_option.ToList().ForEach(d => _iFormQuestionOptionsvc.Delete(d));
                
                _iFormQuestionsvc.Delete(item);

                Form form = item.QUESTION_FORM;
                form.Form_json = new JsonFormWizard().GenerateJSONFromDTOForm(form);
                _iFormsvc.Update(form);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormQuestion.Deleted"));
                
                return RedirectToAction("Edit", new  { id = item.Form_id, SelectedTab = "2" });
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("EditFormQuestion", new { id = item.Id });
            }
        }
        #endregion 
          



        





            
        #region  FormAnswer methods
        
        public void FillDropDownFormAnswer(FormAnswerModel model)
        {

            model.Previous_answer_FormAnswer_SelectList = new SelectList(_iFormAnswersvc.GetAll().ToList(), "Id", "Object_id");

            model.Form_question_id_FormQuestion_SelectList = new SelectList(_iFormQuestionsvc.GetAll().ToList(), "Id", "Object_id");

            model.Form_answer_round_id_FormAnswerRound_SelectList = new SelectList(_iFormAnswerRoundsvc.GetAll().ToList(), "Id", "Object_id");

            model.Warning_type_ParameterItem_SelectList = new SelectList(_iParameterItemsvc.GetAll().Where(p => p.Group.System_name == "FORM_ANSWER_WARNING_TYPE").ToList().Select(p => { return new { Id = p.Id, Title = p.GetLocalized(l => l.Title) }; }), "Id", "Title");
        }
        
        public ActionResult CreateOrUpdateFormAnswer(string itemId, string imageUrl, string mimeType, string uploadFilename)
        {
            FormAnswerModel response = null;


            int nItemId = -1;
            int.TryParse(itemId, out nItemId);

            if (nItemId != -1)
            {
                FormAnswer record = _iFormAnswersvc.GetById(nItemId);
                response = record.ToModel();
                if (record != null) // update
                {



                    //response.RoleListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem { Text = "Administrators" }, new Ext.Net.ListItem { Text = "Guests" } };
                    //response.PermissionListItems = permListItems.ToArray();

                }
                else // insert mode (after image upload popup close)
                {
                    response = new FormAnswerModel();
                    //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() { Text = "" } };
                }
            }
            else // insert
            {
                response = new FormAnswerModel();
                //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() {Text = "" }  };

            }


            ViewBag.itemId = nItemId;

            return View(response);
        }
        
        [AresAuthorize(ResourceKey = "Admin.FormAnswer.Read")]
        public ActionResult ListFormAnswerDatatable(Ares.Web.Razor.Extensions.Datatable.Model.DTParameterModel model, int? FormQuestionModelId)
        {
            var models = _iFormAnswersvc.GetAll();
            if (!_workContext.CurrentUser.IsAdmin())
            {
                List<int> relatedLocationIds = _workContext.CurrentUser.RelatedLocations().Select(r => r.Id).ToList();
                models = models.Where(e => relatedLocationIds.Contains(e.ANSWER_QUESTION.QUESTION_FORM.EQUIPMENT.Location.Id));
            }

            models = models.Where(m => m.Form_question_id == FormQuestionModelId);
            if (model.Search != null && !string.IsNullOrEmpty(model.Search.Value))
            {
                models = models.Where(m => m.Object_id.Contains(model.Search.Value));
            }
            
            if (model.IsSearchValueExist("Form_question_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Form_question_id"));
                models = models.Where(m => m.Form_question_id == tmpValue);
            }
            if (model.IsSearchValueExist("Device_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Device_id"));
                models = models.Where(m => m.Device_id == tmpValue);
            }
            if (model.IsSearchValueExist("Form_answer_round_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Form_answer_round_id"));
                models = models.Where(m => m.Form_answer_round_id == tmpValue);
            }
            if (model.IsSearchValueExist("Previous_answer"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Previous_answer"));
                models = models.Where(m => m.Previous_answer == tmpValue);
            }
            if (model.IsSearchValueExist("Username"))
            {
                string tmpValue = model.GetSearchValue("Username");
                models = models.Where(m => m.Username.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Read_no"))
            {
                long tmpValue = long.Parse(model.GetSearchValue("Read_no"));
                models = models.Where(m => m.Read_no == tmpValue);
            }
            if (model.IsSearchValueExist("Warning_type"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Warning_type"));
                models = models.Where(m => m.Warning_type == tmpValue);
            }
            if (model.IsSearchValueExist("Answer"))
            {
                string tmpValue = model.GetSearchValue("Answer");
                models = models.Where(m => m.Answer.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Long_answer"))
            {
            }
            if (model.IsSearchValueExist("Converted_answer"))
            {
                string tmpValue = model.GetSearchValue("Converted_answer");
                models = models.Where(m => m.Converted_answer.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Json"))
            {
            }
            if (model.IsSearchValueExist("Required_read_datetime"))
            {
                
                
            }
            if (model.IsSearchValueExist("Actual_read_datetime"))
            {
                
                
            }
            if (model.IsSearchValueExist("Description"))
            {
                string tmpValue = model.GetSearchValue("Description");
                models = models.Where(m => m.Description.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Notes"))
            {
            }
            if (model.IsSearchValueExist("Udid"))
            {
                string tmpValue = model.GetSearchValue("Udid");
                models = models.Where(m => m.Udid.Contains(tmpValue));
            }
            
            DTOrder order = null;
            if (model.Order != null && model.Order.Count() > 0)
            {
                order = model.Order.First();
                var pi = typeof(FormAnswer).GetProperty(model.Columns.ToList()[order.Column].Data);
                if(order.Dir == "asc")
                    models = models.OrderBy(x => pi.GetValue(x, null));
                else
                    models = models.OrderByDescending(x => pi.GetValue(x, null));
                
            }

            var totalRecords = models.Count();
            
            models = models.OrderBy(e => e.Id).Skip(model.Length * ((int)(Math.Ceiling((double)model.Start / model.Length)) + 1 - 1)).Take(model.Length);

            JsonResult jsonRes = Json(new
            {
                sEcho = model.Draw,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                //aaData = result
                aaData = models.ToList().Select(x => { var m = x.ToModel(); m.Previous_answer_Text = x.ANSWER_PREVIOUS_ANSWER == null ? "" : x.ANSWER_PREVIOUS_ANSWER.Object_id; m.Form_question_id_Text = x.ANSWER_QUESTION == null ? "" : x.ANSWER_QUESTION.Object_id; m.Form_answer_round_id_Text = x.ANSWER_ROUND == null ? "" : x.ANSWER_ROUND.Object_id; m.Warning_type_Text = x.ANSWER_WARNING_PARAMITEMS == null ? "" : x.ANSWER_WARNING_PARAMITEMS.Code;  return m;})
            }, JsonRequestBehavior.AllowGet);
            return jsonRes;
        }
        
        [AresAuthorize(ResourceKey = "Admin.FormAnswer.Create")]
        public ActionResult CreateFormAnswer(int FormQuestionModelId)
        {
            FormAnswerModel model = new FormAnswerModel();
            model.Form_question_id = FormQuestionModelId;
            
            
            FillDropDownFormAnswer(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormAnswer.Create")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult CreateFormAnswer( FormAnswerModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {                
                var record = model.ToEntity();
                
                record.Object_id = Guid.NewGuid().ToString();
                record.Insert_user_id = _workContext.CurrentUser.Id.ToString();
                record.Insert_datetime = StringHelper.ConvertDateTimeToString(DateTime.Now);
                
                
                record = _iFormAnswersvc.Insert(record);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormAnswer.Created"));
                
                if (continueEditing)
                    return RedirectToAction("EditFormAnswer", new { id = record.Id });

                return RedirectToAction("EditFormQuestion", new  { id = model.Form_question_id, SelectedTab = "2" });
            }
            else
            {
                FillDropDownFormAnswer(model);
                return View(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormAnswer.Update")]
        [FormAnswerAuthorize]
        public ActionResult EditFormAnswer(int id, string SelectedTab)
        {
            FormAnswer record = _iFormAnswersvc.GetById(id);
            var model = record.ToModel();
            model.SelectedTab = SelectedTab;
            
            
            
            FillDropDownFormAnswer(model);
            
            
                       

            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormAnswer.Update")]
        [FormAnswerAuthorize]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult EditFormAnswer( FormAnswerModel model, bool continueEditing)
        {
            var item = _iFormAnswersvc.GetById(model.Id);
            FormAnswer originalItem = (FormAnswer)item.Clone();
            if (item == null)
                //No country found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                item = model.ToEntity(item);
                item.Object_id = originalItem.Object_id;                
                item.Insert_user_id = originalItem.Insert_user_id;                
                item.Insert_datetime = originalItem.Insert_datetime;                
                
                
                
                item.Update_user_id = _workContext.CurrentUser.Id.ToString();
                item.Update_datetime = StringHelper.ConvertDateTimeToString(DateTime.Now);

                _iFormAnswersvc.Update(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormAnswer.Updated"));
                
                if(continueEditing)
                    return RedirectToAction("EditFormAnswer", new { id = item.Id });
                
                return RedirectToAction("EditFormQuestion", new  { id = model.Form_question_id, SelectedTab = "2" });
            }

            //If we got this far, something failed, redisplay form
            FillDropDownFormAnswer(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormAnswer.Delete")]
        [FormAnswerAuthorize]
        public ActionResult DeleteFormAnswer(int id)
        {

            var item = _iFormAnswersvc.GetById(id);
            if (item == null)
                return RedirectToAction("List");

            try
            {
                
                _iFormAnswersvc.Delete(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormAnswer.Deleted"));
                
                return RedirectToAction("EditFormQuestion", new  { id = item.Form_question_id, SelectedTab = "2" });
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("EditFormAnswer", new { id = item.Id });
            }
        }
        #endregion 
          



        








            
        #region  FormQuestionOption methods
        
        public void FillDropDownFormQuestionOption(FormQuestionOptionModel model)
        {

            model.Derived_id_FormQuestionOption_SelectList = new SelectList(_iFormQuestionOptionsvc.GetAll().ToList(), "Id", "Object_id");

            model.Form_question_id_FormQuestion_SelectList = new SelectList(_iFormQuestionsvc.GetAll().ToList(), "Id", "Object_id");
        }
        
        public ActionResult CreateOrUpdateFormQuestionOption(string itemId, string imageUrl, string mimeType, string uploadFilename)
        {
            FormQuestionOptionModel response = null;


            int nItemId = -1;
            int.TryParse(itemId, out nItemId);

            if (nItemId != -1)
            {
                FormQuestionOption record = _iFormQuestionOptionsvc.GetById(nItemId);
                response = record.ToModel();
                if (record != null) // update
                {



                    //response.RoleListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem { Text = "Administrators" }, new Ext.Net.ListItem { Text = "Guests" } };
                    //response.PermissionListItems = permListItems.ToArray();

                }
                else // insert mode (after image upload popup close)
                {
                    response = new FormQuestionOptionModel();
                    //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() { Text = "" } };
                }
            }
            else // insert
            {
                response = new FormQuestionOptionModel();
                //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() {Text = "" }  };

            }


            ViewBag.itemId = nItemId;

            return View(response);
        }
        
        [AresAuthorize(ResourceKey = "Admin.FormQuestionOption.Read")]
        public ActionResult ListFormQuestionOptionDatatable(Ares.Web.Razor.Extensions.Datatable.Model.DTParameterModel model, int? FormQuestionModelId)
        {
            var models = _iFormQuestionOptionsvc.GetAll();
            models = models.Where(m => m.Form_question_id == FormQuestionModelId);
            if (model.Search != null && !string.IsNullOrEmpty(model.Search.Value))
            {
                models = models.Where(m => m.Object_id.Contains(model.Search.Value));
            }
            
            if (model.IsSearchValueExist("Form_question_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Form_question_id"));
                models = models.Where(m => m.Form_question_id == tmpValue);
            }
            if (model.IsSearchValueExist("Derived_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Derived_id"));
                models = models.Where(m => m.Derived_id == tmpValue);
            }
            if (model.IsSearchValueExist("Code"))
            {
                string tmpValue = model.GetSearchValue("Code");
                models = models.Where(m => m.Code.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Title"))
            {
                string tmpValue = model.GetSearchValue("Title");
                models = models.Where(m => m.Title.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Hint"))
            {
                string tmpValue = model.GetSearchValue("Hint");
                models = models.Where(m => m.Hint.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Description"))
            {
                string tmpValue = model.GetSearchValue("Description");
                models = models.Where(m => m.Description.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Version"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Version"));
                models = models.Where(m => m.Version == tmpValue);
            }
            if (model.IsSearchValueExist("Image_path"))
            {
                string tmpValue = model.GetSearchValue("Image_path");
                models = models.Where(m => m.Image_path.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Device_image_path"))
            {
                string tmpValue = model.GetSearchValue("Device_image_path");
                models = models.Where(m => m.Device_image_path.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Estimated_time"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Estimated_time"));
                models = models.Where(m => m.Estimated_time == tmpValue);
            }
            if (model.IsSearchValueExist("Video_path"))
            {
                string tmpValue = model.GetSearchValue("Video_path");
                models = models.Where(m => m.Video_path.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Device_video_path"))
            {
                string tmpValue = model.GetSearchValue("Device_video_path");
                models = models.Where(m => m.Device_video_path.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Method"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Method"));
                models = models.Where(m => m.Method == tmpValue);
            }
            if (model.IsSearchValueExist("Display_order"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Display_order"));
                models = models.Where(m => m.Display_order == tmpValue);
            }
            if (model.IsSearchValueExist("Period"))
            {
                string tmpValue = model.GetSearchValue("Period");
                models = models.Where(m => m.Period.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Active"))
            {
                bool tmpValue = bool.Parse(model.GetSearchValue("Active"));
                models = models.Where(m => m.Active == tmpValue);
            }
            if (model.IsSearchValueExist("Deleted"))
            {
                bool tmpValue = bool.Parse(model.GetSearchValue("Deleted"));
                models = models.Where(m => m.Deleted == tmpValue);
            }
            
            DTOrder order = null;
            if (model.Order != null && model.Order.Count() > 0)
            {
                order = model.Order.First();
                var pi = typeof(FormQuestionOption).GetProperty(model.Columns.ToList()[order.Column].Data);
                if(order.Dir == "asc")
                    models = models.OrderBy(x => pi.GetValue(x, null));
                else
                    models = models.OrderByDescending(x => pi.GetValue(x, null));
                
            }

            var totalRecords = models.Count();
            
            models = models.OrderBy(e => e.Id).Skip(model.Length * ((int)(Math.Ceiling((double)model.Start / model.Length)) + 1 - 1)).Take(model.Length);

            JsonResult jsonRes = Json(new
            {
                sEcho = model.Draw,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                //aaData = result
                aaData = models.ToList().Select(x => { var m = x.ToModel(); m.Derived_id_Text = x.QUESTION_OPTION_DERIVED == null ? "" : x.QUESTION_OPTION_DERIVED.Object_id; m.Form_question_id_Text = x.QUESTION_OPTION_QUESTION == null ? "" : x.QUESTION_OPTION_QUESTION.Object_id;  return m;})
            }, JsonRequestBehavior.AllowGet);
            return jsonRes;
        }
        
        [AresAuthorize(ResourceKey = "Admin.FormQuestionOption.Create")]
        public ActionResult CreateFormQuestionOption(int FormQuestionModelId)
        {
            FormQuestionOptionModel model = new FormQuestionOptionModel();
            model.Form_question_id = FormQuestionModelId;
            
            
            FillDropDownFormQuestionOption(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormQuestionOption.Create")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult CreateFormQuestionOption( FormQuestionOptionModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {                
                var record = model.ToEntity();
                record.Object_id = Guid.NewGuid().ToString();
                record.Insert_user_id = _workContext.CurrentUser.Id.ToString();
                record.Insert_datetime = StringHelper.ConvertDateTimeToString(DateTime.Now);

                record = _iFormQuestionOptionsvc.Insert(record);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormQuestionOption.Created"));
                
                if (continueEditing)
                    return RedirectToAction("EditFormQuestionOption", new { id = record.Id });

                return RedirectToAction("EditFormQuestion", new  { id = model.Form_question_id, SelectedTab = "4" });
            }
            else
            {
                FillDropDownFormQuestionOption(model);
                return View(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }
        
        public ActionResult EditFormQuestionOption(int id, string SelectedTab)
        {
            FormQuestionOption record = _iFormQuestionOptionsvc.GetById(id);
            var model = record.ToModel();
            model.SelectedTab = SelectedTab;
            
            
            
            FillDropDownFormQuestionOption(model);

            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormQuestionOption.Update")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult EditFormQuestionOption( FormQuestionOptionModel model, bool continueEditing)
        {
            var item = _iFormQuestionOptionsvc.GetById(model.Id);
            FormQuestionOption originalItem = (FormQuestionOption)item.Clone();
            if (item == null)
                //No country found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                item = model.ToEntity(item);
                item.Object_id = originalItem.Object_id;                
                item.Insert_user_id = originalItem.Insert_user_id;                
                item.Insert_datetime = originalItem.Insert_datetime;                
                
                
                
                item.Update_user_id = _workContext.CurrentUser.Id.ToString();
                item.Update_datetime = StringHelper.ConvertDateTimeToString(DateTime.Now);

                _iFormQuestionOptionsvc.Update(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormQuestionOption.Updated"));
                
                if(continueEditing)
                    return RedirectToAction("EditFormQuestionOption", new { id = item.Id });
                
                return RedirectToAction("EditFormQuestion", new  { id = model.Form_question_id, SelectedTab = "4" });
            }

            //If we got this far, something failed, redisplay form
            FillDropDownFormQuestionOption(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormQuestionOption.Delete")]
        public ActionResult DeleteFormQuestionOption(int id)
        {

            var item = _iFormQuestionOptionsvc.GetById(id);
            if (item == null)
                return RedirectToAction("List");

            try
            {
                
                _iFormQuestionOptionsvc.Delete(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormQuestionOption.Deleted"));
                
                return RedirectToAction("EditFormQuestion", new  { id = item.Form_question_id, SelectedTab = "4" });
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("EditFormQuestionOption", new { id = item.Id });
            }
        }
        #endregion




        public ActionResult FillForms(List<int> locIds, List<int> roleIds)
        {
            var query = _iFormsvc.GetAll().Where(d => locIds.Contains(d.EQUIPMENT.Eq_location_id));
            if (roleIds != null && roleIds.Count > 0 && roleIds.All(r => r > 0))
                query = query.Where(d => roleIds.Contains(d.Role_id ?? -1));

            List<Form> forms = query.ToList();
            var items = forms.Select(d => new { Id = d.Id, Title = (d.Code != null ? d.Code + " " : "") + d.Title }).ToList();

            //items.Insert(0, new FormQuestionModel() { Title = _localizationSvc.GetResource("Select") });

            return Json(items, JsonRequestBehavior.AllowGet);
        }



        public ActionResult FillQuestions(List<int> formIds, List<int> versions)
        {
            List<Form> forms = _iFormsvc.GetAll().Where(d => formIds.Contains(d.Id)).ToList();
            var distinctVersions = forms.SelectMany(f => f.FORMForm_question.Select(q => q.Version ?? -1).Distinct()).Distinct().ToList();
            var questionVersions = distinctVersions.Select(d => new { Id = d, Title = d }).ToList();

            var questionQuery = _iFormQuestionsvc.GetAll().Where(d => formIds.Contains(d.Form_id));
            if (versions != null && versions.Where(v => v != 0).Count() > 0)
                questionQuery = questionQuery.Where(q => versions.Contains(q.Version ?? -1));

            var questions = questionQuery.ToList().Select(d => new { Id = d.Id, Title = (d.Code != null ? d.Code + " " : "") + d.Title   }).ToList();
            var result = new { questions = questions, versions = questionVersions };
            //items.Insert(0, new FormQuestionModel() { Title = _localizationSvc.GetResource("Select") });

            return Json(result, JsonRequestBehavior.AllowGet);
        }




        public ActionResult fillEmptyJSONValues()
        {
            IFormQuestionSvc iFormQuestionSvc = FrameworkCtx.Current.Resolve<IFormQuestionSvc>();

            iFormQuestionSvc.fillEmptyJSONValues();

            return Redirect(Request.UrlReferrer.ToString());
        }


    }


    public class FormAuthorizeAttribute : ActionFilterAttribute
    {
        public IWorkContext WorkContext { get; set; }
        public IFormSvc FormSvc { get; set; }
        //public string IdParamName { get; set; }
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (WorkContext.CurrentUser.IsAdmin())
            {
                base.OnActionExecuting(context);
                return;
            }

            var httpContext = context.HttpContext;

            List<EqLocation> relatedLocations = WorkContext.CurrentUser.RelatedLocations().ToList();

            if (!string.IsNullOrEmpty(context.RouteData.Values["id"].ToString()))
            {
                int entityID = int.Parse(context.RouteData.Values["id"].ToString());

                Form tmpEntity = FormSvc.GetById(entityID);

                if (tmpEntity != null && relatedLocations != null && relatedLocations.Count > 0 && relatedLocations.Any(l => l.Id == tmpEntity.EQUIPMENT.Location.Id))
                {
                    base.OnActionExecuting(context);
                    return;
                }

            }

            context.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "User", action = "NotAuthorized" }));
        }
    }

    public class FormQuestionAuthorizeAttribute : ActionFilterAttribute
    {
        public IWorkContext WorkContext { get; set; }
        public IFormQuestionSvc FormQuestionSvc { get; set; }
        //public string IdParamName { get; set; }
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (WorkContext.CurrentUser.IsAdmin())
            {
                base.OnActionExecuting(context);
                return;
            }

            var httpContext = context.HttpContext;

            List<EqLocation> relatedLocations = WorkContext.CurrentUser.RelatedLocations().ToList();

            if (!string.IsNullOrEmpty(context.RouteData.Values["id"].ToString()))
            {
                int entityID = int.Parse(context.RouteData.Values["id"].ToString());

                FormQuestion tmpEntity = FormQuestionSvc.GetById(entityID);

                if (tmpEntity != null && relatedLocations != null && relatedLocations.Count > 0 && relatedLocations.Any(l => l.Id == tmpEntity.QUESTION_FORM.EQUIPMENT.Location.Id))
                {
                    base.OnActionExecuting(context);
                    return;
                }

            }

            context.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "User", action = "NotAuthorized" }));
        }

    }


    public class FormAnswerAuthorizeAttribute : ActionFilterAttribute
    {
        public IWorkContext WorkContext { get; set; }
        public IFormAnswerSvc FormAnswerSvc { get; set; }
        //public string IdParamName { get; set; }
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (WorkContext.CurrentUser.IsAdmin())
            {
                base.OnActionExecuting(context);
                return;
            }

            var httpContext = context.HttpContext;

            List<EqLocation> relatedLocations = WorkContext.CurrentUser.RelatedLocations().ToList();

            if (!string.IsNullOrEmpty(context.RouteData.Values["id"].ToString()))
            {
                int entityID = int.Parse(context.RouteData.Values["id"].ToString());

                FormAnswer tmpEntity = FormAnswerSvc.GetById(entityID);

                if (tmpEntity != null && relatedLocations != null && relatedLocations.Count > 0 && relatedLocations.Any(l => l.Id == tmpEntity.ANSWER_QUESTION.QUESTION_FORM.EQUIPMENT.Location.Id))
                {
                    base.OnActionExecuting(context);
                    return;
                }

            }

            context.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "User", action = "NotAuthorized" }));
        }

    }
}

