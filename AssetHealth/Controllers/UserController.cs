using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Ares.UMS.Svc.Directory;
using System.Linq;
using Ares.UMS.DTO.Users;
using Ares.UMS.Svc.Users;
using Ares.UMS.Svc.Common;
using Ares.UMS.DTO.Messages;
using Ares.UMS.Svc.Messages;
using Ares.Core.Helpers;
using Ares.UMS.DTO.Localization;
using Ares.UMS.Svc.Localization;
using System.Net;
using System.Web.Security;
using Ext.Net.MVC;
using Ares.UMS.DTO.Media;
using Ares.UMS.Svc.Media;
using System.IO;
using Ext.Net;
using Ares.Web.Helpers;
using Ares.Core.Framework.Logging.Business;
using Ares.Core.Framework.Logging;
using Ares.UMS.Svc;
using Ares.Web.Svc.Authentication;
using Ares.Web.Svc.Media;
using AssetHealth.Extensions;
using AssetHealth.Models.User;
using System.Text.RegularExpressions;
using Ares.Core;
using Ares.UMS.Svc.Security;
using AssetHealth.Common.Svc;
using Ares.Web.Attributes;
using Ares.Web.Controllers;
using AssetHealth.Common.DTO.Extensions;
using System.Web.Routing;
using AssetHealth.Common.DTO;

namespace AssetHealth.Controllers
{
    public class UserController : BaseAresController
    {
        private readonly IStateProvinceSvc _stateProvinceSvc;
        private readonly IGenericAttributeSvc _genericAttributeSvc;
        private readonly ILocalizationSvc _localizationSvc;
        private readonly IPictureSvc _pictureSvc;
        private readonly IUserSvc _userSvc;
        private readonly IRoleSvc _roleSvc;
        private readonly IWorkContext _workContext;
        private readonly IUserRegistrationSvc _userRegistrationSvc;
        private readonly IAuthenticationSvc _authenticationSvc;
        private readonly IWorkflowMessageSvc _workflowMessageSvc;
        private readonly INewsLetterSubscriptionSvc _newsLetterSubscriptionSvc;
        private readonly IWebHelper _webHelper;
        private readonly UserSettings _userSettings;
        private readonly LocalizationSettings _localizationSettings;
        private readonly MediaSettings _mediaSettings;
        private readonly IBusinessLogger _businessLogger;
        private readonly IEncryptionSvc _iEncryptionSvc;
        private readonly ICountrySvc _iCountrySvc;
        //private readonly IParameterItemSvc _iParameteritemssvc;

        private readonly ILogger _iLogger;
        private readonly IJobTitleSvc _iJobTitlesvc;
        private readonly IOrgUnitSvc _iOrgUnitsvc;
        private readonly ILanguageSvc _iLanguagesvc;
        private readonly ISiteContext _siteContext;


        private readonly IEqLocationSvc _iEqLocationSvc;

        public UserController(IStateProvinceSvc stateProvinceSvc, IGenericAttributeSvc genericAttributeSvc, ILocalizationSvc localizationSvc,  
            UserSettings userSettings, IPictureSvc pictureSvc,
            //IParameterItemSvc iParameteritemssvc,
            IEncryptionSvc iEncryptionSvc,
            IJobTitleSvc JobTitlesvc,
            IOrgUnitSvc OrgUnitsvc, ILogger iLogger,
            ILanguageSvc Languagesvc,
             ISiteContext siteContext,
             ICountrySvc iCountrySvc, 
            IUserSvc userSvc, IRoleSvc roleSvc, IWorkContext workContext, IUserRegistrationSvc userRegistrationSvc, IAuthenticationSvc authenticationSvc, IWorkflowMessageSvc workflowMessageSvc,
            INewsLetterSubscriptionSvc newsLetterSubscriptionSvc, IWebHelper webHelper, LocalizationSettings localizationSettings, MediaSettings mediaSettings, IBusinessLogger businessLogger
            ,IEqLocationSvc iEqLocationSvc
            )
        {
            this._stateProvinceSvc = stateProvinceSvc;
            this._genericAttributeSvc = genericAttributeSvc;
            this._pictureSvc = pictureSvc;
            this._localizationSvc = localizationSvc;
            this._userSettings = userSettings;
            this._userSvc = userSvc;
            this._roleSvc = roleSvc;
            this._workContext = workContext;
            this._userRegistrationSvc = userRegistrationSvc;
            this._authenticationSvc = authenticationSvc;
            this._workflowMessageSvc = workflowMessageSvc;
            this._newsLetterSubscriptionSvc = newsLetterSubscriptionSvc;
            this._webHelper = webHelper;
            this._localizationSettings = localizationSettings;
            this._mediaSettings = mediaSettings;
            this._businessLogger = businessLogger;

            this._siteContext = siteContext;
            //this._iParameteritemssvc = iParameteritemssvc;
            this._iLogger = iLogger;
            this._iJobTitlesvc = JobTitlesvc;
            this._iOrgUnitsvc = OrgUnitsvc;
            this._iLanguagesvc = Languagesvc;
            this._iEncryptionSvc = iEncryptionSvc;
            this._iCountrySvc = iCountrySvc;

            this._iEqLocationSvc = iEqLocationSvc;

        }


        [ChildActionOnly]
        public ActionResult _LoginBox()
        {
            var model = new Ares.Web.Models.User.LoginModel();
            model.UsernamesEnabled = _userSettings.UsernamesEnabled;

            return PartialView(model);
        }
        
        public ActionResult LogOut()
        {
            _authenticationSvc.SignOut();
            return RedirectToAction("Index", "Home");
        }


        public ActionResult Login()
        {
            var model = new Ares.Web.Models.User.LoginModel();
            model.UsernamesEnabled = _userSettings.UsernamesEnabled;

            return View(model);
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult Login(Ares.Web.Models.User.LoginModel model, string returnUrl)
        {


            if (ModelState.IsValid)
            {
                if (_userSettings.UsernamesEnabled && model.Username != null)
                {
                    model.Username = model.Username.Trim();
                }

                if (_userSvc.ValidateUser(_userSettings.UsernamesEnabled ? model.Username : model.Email, model.Password))
                {
                    var user = _userSettings.UsernamesEnabled ? _userSvc.GetUserByUsername(model.Username) : _userSvc.GetUserByEmail(model.Email);

                    //sign in new user
                    //_authenticationSvc.SignIn(user, model.RememberMe);
                    _authenticationSvc.SignIn(user, true);


                    if (!string.IsNullOrEmpty(returnUrl) && Url.IsLocalUrl(returnUrl))
                        return Redirect(returnUrl);
                    else
                        //return RedirectToRoute("Default");
                        return Redirect("/");
                }
                else
                {
                    User tmpUser = _userSvc.GetUserByUsername(model.Username);
                    if (tmpUser != null && !tmpUser.Active.GetValueOrDefault(false))
                    {
                        ModelState.AddModelError("", _localizationSvc.GetResource("Login.Username.Is.InActive"));
                    }
                    else
                    {
                        _businessLogger.Log(new LogItem() { Message = (_userSettings.UsernamesEnabled ? model.Username : model.Email) + " Kullanıcı Adı veya şifre yanlış" });
                        //ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials"));
                        ModelState.AddModelError("", _localizationSvc.GetResource("Login.Username.or.Password.Invalid"));
                    }
                }
            }
            else
            {
                ModelState.AddModelError("", "Model Is not Valid");
            }



            model.UsernamesEnabled = _userSettings.UsernamesEnabled;

            //return View(model);
            return View("Login", model);
            //return Redirect(ControllerContext.HttpContext.Request.UrlReferrer.ToString());
        }

        [System.Web.Mvc.HttpPost]
        public string AjaxLogin(Ares.Web.Models.User.LoginModel model, string returnUrl)
        {


            if (ModelState.IsValid)
            {
                if (_userSettings.UsernamesEnabled && model.Username != null)
                {
                    model.Username = model.Username.Trim();
                }

                if (_userSvc.ValidateUser(_userSettings.UsernamesEnabled ? model.Username : model.Email, model.Password))
                {
                    var user = _userSettings.UsernamesEnabled ? _userSvc.GetUserByUsername(model.Username) : _userSvc.GetUserByEmail(model.Email);

                    //sign in new user
                    _authenticationSvc.SignIn(user, model.RememberMe);


                    if (!string.IsNullOrEmpty(returnUrl) && Url.IsLocalUrl(returnUrl))
                        return "";
                    else
                        //return RedirectToRoute("Default");
                        return "";
                }
                else
                {
                    //ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials"));
                    ModelState.AddModelError(string.Empty, "Kullanıcı Adı veya şifre yanlış");
                }
            }


            model.UsernamesEnabled = _userSettings.UsernamesEnabled;


            var errorList = ModelState.Values.SelectMany(m => m.Errors)
                                 .Select(e => e.ErrorMessage)
                                 .ToList();

            return String.Join("<br>", errorList);
        }


        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }



        [ChildActionOnly]
        public ActionResult Register()
        {
            //var model = new Ares.Web.Models.User.RegisterModel();
            var model = new AssetHealth.Models.User.RegisterModel();
            //model.LoginType = loginType;

            //model.UserModel.Nationality_id_Nationality_SelectList = new SelectList(_iParameteritemssvc.GetAll().Where(p => p.ParameterGroup.ReserveName == "NATIONALITY").ToList(), "Id", _workContext.WorkingLanguage.Id == 1 ? "Title_TR" : "Title_EN");

            return View(model);
        }

        [System.Web.Mvc.HttpPost]
        //[MultipleButton(Name = "action", Argument = "register")]
        public ActionResult Register(AssetHealth.Models.User.RegisterModel model)
        {
            /*
            if (!Request.Url.ToString().ToLowerInvariant().Contains("register"))
                return Register(model.LoginType);
            */

            //_iLogger.Log(new LogItem() { Message = "Register geldi"  + model.UserModel.Username });
            userValidation(model.UserModel, "UserModel.");


            if (ModelState.IsValid)
            {


                model.UserModel.Password_format_id = _userSettings.DefaultPasswordFormatId;
                //model.UserModel.Active = true;
                model.UserModel.User_guid = Guid.NewGuid().ToString();

                /*
                switch ((PasswordFormat)model.UserModel.Password_format_id)
                {
                    case PasswordFormat.Clear:
                        {
                            model.UserModel.Password = model.UserPasswordChangeModel.NewPassword;
                        }
                        break;
                    case PasswordFormat.Encrypted:
                        {
                            model.UserModel.Password = _iEncryptionSvc.EncryptText(model.UserPasswordChangeModel.NewPassword);
                        }
                        break;
                    case PasswordFormat.Hashed:
                        {
                            string saltKey = _iEncryptionSvc.CreateSaltKey(5);
                            model.UserModel.Password_salt = saltKey;
                            model.UserModel.Password = _iEncryptionSvc.CreatePasswordHash(model.UserPasswordChangeModel.NewPassword, saltKey, _userSettings.HashedPasswordFormat);
                        }
                        break;
                    default:
                        break;
                }
                */
                User user = model.UserModel.ToEntity();
                user.INSERT_DATETIME = StringHelper.ConvertDateTimeToString(DateTime.Now);


                if (model.UserModel.Id == 0)
                    user = _userSvc.Insert(user);
                else
                    _userSvc.Update(user);


                _genericAttributeSvc.SaveAttribute(user, "Nationality_id", model.UserModel.Nationality_id, _siteContext.CurrentSite.Id);

                /*
                if (model.LoginType == EnumLoginType.GrantBeneficiary)
                {
                    Potgrantbeneficiary pgb = new Potgrantbeneficiary()
                    { Applicantname = "",  Postaladdress = "", Phonenumber = "", Faxnumber = "", Contactperson = "", Contactmailaddress = "", City_id = 6, Createdby = 0, Createddate = DateTime.Now };
                    pgb.Institutiontype_id = _iParameteritemssvc.GetAll().Where(p => p.ParameterGroup.ReserveName == "BENEFICIARYINSTITUTIONTYPE").FirstOrDefault().Id;

                    pgb = _iPotgrantbeneficiarySvc.Insert(pgb);
                    Userdetail ud = new Userdetail();
                    ud.User_id = user.Id;
                    ud.Grantbeneficiary_id = pgb.Id;

                    _iUserdetailssvc.Insert(ud);
                }
                */

                if (model.UserModel.UserPasswordChangeModel != null && !string.IsNullOrEmpty(model.UserModel.UserPasswordChangeModel.NewPassword) && !string.IsNullOrEmpty(model.UserModel.UserPasswordChangeModel.NewPasswordAgain))
                    _userSvc.PasswordChange(model.UserModel.Username, model.UserModel.UserPasswordChangeModel.CurrentPassword, model.UserModel.UserPasswordChangeModel.NewPasswordAgain);


                if (_userSettings.NotifyNewUserRegistration)
                    _workflowMessageSvc.SendUserRegisteredNotificationMessage(user, _localizationSettings.DefaultAdminLanguageId);

                switch (_userSettings.EnumUserRegistrationType)
                {
                    case EnumUserRegistrationType.EmailValidation:
                        {
                            //email validation message
                            _genericAttributeSvc.SaveAttribute(user, SystemUserAttributeNames.AccountActivationToken, Guid.NewGuid().ToString(), _siteContext.CurrentSite.Id);
                            _workflowMessageSvc.SendUserEmailValidationMessage(user, _workContext.WorkingLanguage.Id);

                            //result
                            //return RedirectToRoute("RegisterResult", new { resultId = (int)EnumUserRegistrationType.EmailValidation });
                            break;
                        }
                    case EnumUserRegistrationType.AdminApproval:
                        {
                            //return RedirectToRoute("RegisterResult", new { resultId = (int)EnumUserRegistrationType.AdminApproval });
                            break;
                        }
                    case EnumUserRegistrationType.Standard:
                        {
                            //send user welcome message
                            _workflowMessageSvc.SendUserWelcomeMessage(user, _workContext.WorkingLanguage.Id);

                            var redirectUrl = Url.RouteUrl("RegisterResult", new { resultId = (int)EnumUserRegistrationType.Standard });

                            //return Redirect(redirectUrl);
                            break;
                        }
                    default:
                        {
                            //return RedirectToRoute("HomePage");
                            break;
                        }
                }


            }


            var errorList = ModelState.Values.SelectMany(m => m.Errors).Select(e => e.ErrorMessage).ToList();

            if (errorList != null && errorList.Count > 0)
            {
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                Response.TrySkipIisCustomErrors = true;
                return Json(new { success = false, responseText = String.Join("<br>", errorList) }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Response.StatusCode = (int)HttpStatusCode.OK;
                return Json(new { success = true, responseText = "Başarılı" }, JsonRequestBehavior.AllowGet);
            }

            //return RedirectToAction("Login", "User");
        }

        public void userValidation(UserModel model, string validatePrefix = "")
        {
            bool fromAdminPage = string.IsNullOrEmpty(validatePrefix);

            if (_userSettings.UsernamesEnabled && model.Username != null)
            {
                model.Username = model.Username.Trim();
            }
            model.Email = model.Email != null ? model.Email.Trim() : null;

            if (String.IsNullOrEmpty(model.Email))
            {
                ModelState.AddModelError(validatePrefix + "Email", _localizationSvc.GetResource("Account.Register.Errors.EmailIsNotProvided"));
            }
            else if (model.Nationality_id == null)
            {
                ModelState.AddModelError(validatePrefix + "Nationality_id", _localizationSvc.GetResource("Account.Register.Errors.NationalityRequired"));
            }
            else if (string.IsNullOrEmpty(model.IdentityNumber))
            {
                ModelState.AddModelError(validatePrefix + "IdentityNumber", _localizationSvc.GetResource("Account.Register.Errors.IdentityNumberRequired"));
            }
            /*
            else if (model.Nationality_id == 270)
            {
                if (!TCKimlikValidator.Validate(model.IdentityNumber))
                {
                    ModelState.AddModelError(validatePrefix + "IdentityNumber", _localizationSvc.GetResource("User.WrongIdentityNumber"));
                }
                else
                {
                    MernisClient client = new MernisClient();
                    MERNISSonuc sonuc = client.MERNISSorgula(model.IdentityNumber);

                    if (sonuc != null && !string.IsNullOrEmpty(sonuc.Adi) && !string.IsNullOrEmpty(sonuc.Soyadi) && (model.First_name.ToLower(new System.Globalization.CultureInfo("tr-TR", false)).Trim() != sonuc.Adi.ToLower(new System.Globalization.CultureInfo("tr-TR", false)).Trim() || model.Last_name.ToLower(new System.Globalization.CultureInfo("tr-TR", false)).Trim() != sonuc.Soyadi.ToLower(new System.Globalization.CultureInfo("tr-TR", false)).Trim()))
                        ModelState.AddModelError(validatePrefix + "Nationality_id", _localizationSvc.GetResource("Account.Register.Errors.MERNISIncompatible"));
                }


            }
            */
            else if (!CommonHelper.IsValidEmail(model.Email))
            {
                ModelState.AddModelError(validatePrefix + "Email", _localizationSvc.GetResource("Common.WrongEmail"));
            }
            else if (String.IsNullOrWhiteSpace(model.Username))
            {
                ModelState.AddModelError(validatePrefix + "Username", _localizationSvc.GetResource("Account.Register.Errors.UsernameIsRequired"));
            }
            else if (model.Id <= 0)  // create
            {
                if (/*!model.IsADUser &&*/ String.IsNullOrWhiteSpace(model.UserPasswordChangeModel.NewPassword))
                {
                    ModelState.AddModelError(validatePrefix + "UserPasswordChangeModel.NewPassword", _localizationSvc.GetResource("Account.Register.Errors.PasswordIsNotProvided"));
                }
                else if (/*!model.IsADUser &&*/ PasswordAdvisor.CheckStrength(model.UserPasswordChangeModel.NewPassword) < PasswordScore.Medium)
                {
                    ModelState.AddModelError(validatePrefix + "UserPasswordChangeModel.NewPassword", _localizationSvc.GetResource("Account.Register.Errors.PasswordIsNotStrong"));
                }

                if (_userSvc.GetUserByEmail(model.Email) != null)  //validate unique user
                    ModelState.AddModelError(validatePrefix + "Email", _localizationSvc.GetResource("Account.Register.Errors.EmailAlreadyExists"));

                if (_userSettings.UsernamesEnabled)
                {
                    if (String.IsNullOrEmpty(model.Username))
                    {
                        ModelState.AddModelError(validatePrefix + "Username", _localizationSvc.GetResource("Account.Register.Errors.UsernameIsNotProvided"));
                    }
                    if (_userSvc.GetUserByUsername(model.Username) != null)
                    {
                        ModelState.AddModelError(validatePrefix + "Username", _localizationSvc.GetResource("Account.Register.Errors.UsernameAlreadyExists"));
                    }
                }

                // TCKN unique olma kontrolü kaldırıldı
                /*
                if (!string.IsNullOrEmpty(model.IdentityNumber) && _userSvc.GetAll().Any(u => u.IdentityNumber == model.IdentityNumber))
                {
                    ModelState.AddModelError(validatePrefix + "IdentityNumber", _localizationSvc.GetResource("Account.Register.Errors.IdentityNumberMustBeUnique"));
                }
                */
            }
            else if (model.Id > 0)  // update
            {
                if (!fromAdminPage)
                {
                    if (/*!model.IsADUser &&*/ String.IsNullOrWhiteSpace(model.UserPasswordChangeModel.NewPassword))
                    {
                        ModelState.AddModelError(validatePrefix + "UserPasswordChangeModel.NewPassword", _localizationSvc.GetResource("Account.Register.Errors.PasswordIsNotProvided"));
                    }
                    else if (/*!model.IsADUser &&*/ PasswordAdvisor.CheckStrength(model.UserPasswordChangeModel.NewPassword) < PasswordScore.Medium)
                    {
                        ModelState.AddModelError(validatePrefix + "UserPasswordChangeModel.NewPassword", _localizationSvc.GetResource("Account.Register.Errors.PasswordIsNotStrong"));
                    }
                }

                User tmpUser = _userSvc.GetUserByUsername(model.Username);
                /*
                
                if (tmpUser != null && tmpUser.Id != model.Id)                  //validate unique user
                    ModelState.AddModelError(validatePrefix + "Email", _localizationSvc.GetResource("Account.Register.Errors.EmailAlreadyExists"));
                */
                if (_userSettings.UsernamesEnabled)
                {
                    if (String.IsNullOrEmpty(model.Username))
                    {
                        ModelState.AddModelError(validatePrefix + "Username", _localizationSvc.GetResource("Account.Register.Errors.UsernameIsNotProvided"));
                    }

                    tmpUser = _userSvc.GetUserByUsername(model.Username);
                    if (tmpUser != null && tmpUser.Id != model.Id)
                    {
                        ModelState.AddModelError(validatePrefix + "Username", _localizationSvc.GetResource("Account.Register.Errors.UsernameAlreadyExists"));
                    }
                }

                // TCKN unique olma kontrolü kaldırıldı
                /*
                if (!string.IsNullOrEmpty(model.IdentityNumber) && _userSvc.GetAll().Any(u => u.IdentityNumber == model.IdentityNumber && u.Id != model.Id))
                {
                    ModelState.AddModelError(validatePrefix + "IdentityNumber", _localizationSvc.GetResource("Account.Register.Errors.IdentityNumberMustBeUnique"));
                }
                */
            }

            if (model.Username == model.UserPasswordChangeModel.NewPassword)
            {
                ModelState.AddModelError(validatePrefix + "Password", _localizationSvc.GetResource("Account.Register.Errors.UsernameAndPassword.MustBeDifferent"));
            }



        }


        [System.Web.Mvc.HttpPost]
        public ActionResult AjaxRegister(Ares.Web.Models.User.RegisterModel model, string returnUrl)
        {


            //check whether registration is allowed
            if (_userSettings.EnumUserRegistrationType == EnumUserRegistrationType.Disabled)
                //return RedirectToRoute("RegisterResult", new { resultId = (int)EnumUserRegistrationType.Disabled });
                return Json(new { success = false, responseText = EnumUserRegistrationType.Disabled.ToString() }, JsonRequestBehavior.AllowGet);

            if (_workContext.CurrentUser != null && _workContext.CurrentUser.IsRegistered())
            {
                //Already registered user. 
                _authenticationSvc.SignOut();

                //Save a new record
                _workContext.CurrentUser = _userSvc.InsertGuestUser();
            }
            var user = _workContext.CurrentUser;
            if (user == null)
            {
                user = new User();
            }
            /*
            //validate CAPTCHA
            if (_captchaSettings.Enabled && _captchaSettings.ShowOnRegistrationPage && !captchaValid)
            {
                ModelState.AddModelError("", _localizationSvc.GetResource("Common.WrongCaptcha"));
            }
            */
            if (ModelState.IsValid)
            {
                if (_userSettings.UsernamesEnabled && model.Username != null)
                {
                    model.Username = model.Username.Trim();
                }

                bool isApproved = _userSettings.EnumUserRegistrationType == EnumUserRegistrationType.Standard;
                var registrationRequest = new UserRegistrationRequest(user, model.Email,
                    _userSettings.UsernamesEnabled ? model.Username : model.Email, model.Password, (PasswordFormat)_userSettings.DefaultPasswordFormatId, isApproved);
                var registrationResult = _userRegistrationSvc.RegisterUser(registrationRequest);
                if (registrationResult.Success)
                {
                    /*
                    //properties
                    if (_dateTimeSettings.AllowUsersToSetTimeZone)
                    {
                        _genericAttributeSvc.SaveAttribute(user, SystemUserAttributeNames.TimeZoneId, model.TimeZoneId);
                    }

                    
                    //form fields
                    if (_userSettings.GenderEnabled)
                        _genericAttributeSvc.SaveAttribute(user, SystemUserAttributeNames.Gender, model.Gender);
                    _genericAttributeSvc.SaveAttribute(user, SystemUserAttributeNames.FirstName, model.FirstName);
                    _genericAttributeSvc.SaveAttribute(user, SystemUserAttributeNames.LastName, model.LastName);
                    if (_userSettings.DateOfBirthEnabled)
                    {
                        DateTime? dateOfBirth = null;
                        try
                        {
                            dateOfBirth = new DateTime(model.DateOfBirthYear.Value, model.DateOfBirthMonth.Value, model.DateOfBirthDay.Value);
                        }
                        catch { }
                        _genericAttributeSvc.SaveAttribute(user, SystemUserAttributeNames.DateOfBirth, dateOfBirth);
                    }
                    
                    if (_userSettings.CompanyEnabled)
                        _genericAttributeSvc.SaveAttribute(user, SystemUserAttributeNames.Company, model.Company);
                    if (_userSettings.StreetAddressEnabled)
                        _genericAttributeSvc.SaveAttribute(user, SystemUserAttributeNames.StreetAddress, model.StreetAddress);
                    if (_userSettings.StreetAddress2Enabled)
                        _genericAttributeSvc.SaveAttribute(user, SystemUserAttributeNames.StreetAddress2, model.StreetAddress2);
                    if (_userSettings.ZipPostalCodeEnabled)
                        _genericAttributeSvc.SaveAttribute(user, SystemUserAttributeNames.ZipPostalCode, model.ZipPostalCode);
                    if (_userSettings.CityEnabled)
                        _genericAttributeSvc.SaveAttribute(user, SystemUserAttributeNames.City, model.City);
                    if (_userSettings.CountryEnabled)
                        _genericAttributeSvc.SaveAttribute(user, SystemUserAttributeNames.CountryId, model.CountryId);
                    if (_userSettings.CountryEnabled && _userSettings.StateProvinceEnabled)
                        _genericAttributeSvc.SaveAttribute(user, SystemUserAttributeNames.StateProvinceId, model.StateProvinceId);
                    if (_userSettings.PhoneEnabled)
                        _genericAttributeSvc.SaveAttribute(user, SystemUserAttributeNames.Phone, model.Phone);
                    if (_userSettings.FaxEnabled)
                        _genericAttributeSvc.SaveAttribute(user, SystemUserAttributeNames.Fax, model.Fax);
                     */

                    //newsletter
                    if (_userSettings.NewsletterEnabled)
                    {
                        //save newsletter value
                        var newsletter = _newsLetterSubscriptionSvc.GetNewsLetterSubscriptionByEmail(model.Email);
                        if (newsletter != null)
                        {
                            //if (model.Newsletter)
                            {
                                newsletter.Active = true;
                                _newsLetterSubscriptionSvc.UpdateNewsLetterSubscription(newsletter);
                            }
                            //else
                            //{
                            //When registering, not checking the newsletter check box should not take an existing email address off of the subscription list.
                            //_newsLetterSubscriptionService.DeleteNewsLetterSubscription(newsletter);
                            //}
                        }
                        else
                        {
                            //if (model.Newsletter)
                            {
                                _newsLetterSubscriptionSvc.InsertNewsLetterSubscription(new NewsLetterSubscription()
                                {
                                    NewsLetterSubscriptionGuid = Guid.NewGuid(),
                                    Email = model.Email,
                                    Active = true,
                                    CreatedOnUtc = DateTime.UtcNow
                                });
                            }
                        }
                    }

                    //login user now
                    if (isApproved)
                        _authenticationSvc.SignIn(user, true);

                    /*
                    //associated with external account (if possible)
                    TryAssociateAccountWithExternalAccount(user);

                    //insert default address (if possible)
                    var defaultAddress = new Address()
                    {
                        FirstName = user.GetAttribute<string>(SystemUserAttributeNames.FirstName),
                        LastName = user.GetAttribute<string>(SystemUserAttributeNames.LastName),
                        Email = user.Email,
                        Company = user.GetAttribute<string>(SystemUserAttributeNames.Company),
                        CountryId = user.GetAttribute<int>(SystemUserAttributeNames.CountryId) > 0 ?
                            (int?)user.GetAttribute<int>(SystemUserAttributeNames.CountryId) : null,
                        StateProvinceId = user.GetAttribute<int>(SystemUserAttributeNames.StateProvinceId) > 0 ?
                            (int?)user.GetAttribute<int>(SystemUserAttributeNames.StateProvinceId) : null,
                        City = user.GetAttribute<string>(SystemUserAttributeNames.City),
                        Address1 = user.GetAttribute<string>(SystemUserAttributeNames.StreetAddress),
                        Address2 = user.GetAttribute<string>(SystemUserAttributeNames.StreetAddress2),
                        ZipPostalCode = user.GetAttribute<string>(SystemUserAttributeNames.ZipPostalCode),
                        PhoneNumber = user.GetAttribute<string>(SystemUserAttributeNames.Phone),
                        FaxNumber = user.GetAttribute<string>(SystemUserAttributeNames.Fax),
                        CreatedOnUtc = user.CreatedOnUtc
                    };

                    if (this._addressService.IsAddressValid(defaultAddress))
                    {
                        //some validation
                        if (defaultAddress.CountryId == 0)
                            defaultAddress.CountryId = null;
                        if (defaultAddress.StateProvinceId == 0)
                            defaultAddress.StateProvinceId = null;
                        //set default address
                        user.Addresses.Add(defaultAddress);
                        user.BillingAddress = defaultAddress;
                        user.ShippingAddress = defaultAddress;
                        _userService.UpdateUser(user);
                    }
                    */
                    //notifications
                    if (_userSettings.NotifyNewUserRegistration)
                        _workflowMessageSvc.SendUserRegisteredNotificationMessage(user, _localizationSettings.DefaultAdminLanguageId);

                    switch (_userSettings.EnumUserRegistrationType)
                    {
                        case EnumUserRegistrationType.EmailValidation:
                            {
                                //email validation message
                                _genericAttributeSvc.SaveAttribute(user, SystemUserAttributeNames.AccountActivationToken, Guid.NewGuid().ToString());
                                _workflowMessageSvc.SendUserEmailValidationMessage(user, _workContext.WorkingLanguage.Id);

                                //result
                                //return RedirectToRoute("RegisterResult", new { resultId = (int)EnumUserRegistrationType.EmailValidation });
                                break;
                            }
                        case EnumUserRegistrationType.AdminApproval:
                            {
                                //return RedirectToRoute("RegisterResult", new { resultId = (int)EnumUserRegistrationType.AdminApproval });
                                break;
                            }
                        case EnumUserRegistrationType.Standard:
                            {
                                //send user welcome message
                                _workflowMessageSvc.SendUserWelcomeMessage(user, _workContext.WorkingLanguage.Id);

                                var redirectUrl = Url.RouteUrl("RegisterResult", new { resultId = (int)EnumUserRegistrationType.Standard });
                                if (!String.IsNullOrEmpty(returnUrl))
                                    redirectUrl = _webHelper.ModifyQueryString(redirectUrl, "returnurl=" + HttpUtility.UrlEncode(returnUrl), null);
                                //return Redirect(redirectUrl);
                                break;
                            }
                        default:
                            {
                                //return RedirectToRoute("HomePage");
                                break;
                            }
                    }
                }
                else
                {
                    foreach (var error in registrationResult.Errors)
                        ModelState.AddModelError("", error);
                }
            }
            /*
            //If we got this far, something failed, redisplay form
            model.AllowUsersToSetTimeZone = _dateTimeSettings.AllowUsersToSetTimeZone;
            foreach (var tzi in _dateTimeHelper.GetSystemTimeZones())
                model.AvailableTimeZones.Add(new SelectListItem() { Text = tzi.DisplayName, Value = tzi.Id, Selected = (tzi.Id == _dateTimeHelper.DefaultStoreTimeZone.Id) });

            
            //form fields
            model.GenderEnabled = _userSettings.GenderEnabled;
            model.DateOfBirthEnabled = _userSettings.DateOfBirthEnabled;
            model.CompanyEnabled = _userSettings.CompanyEnabled;
            model.CompanyRequired = _userSettings.CompanyRequired;
            model.StreetAddressEnabled = _userSettings.StreetAddressEnabled;
            model.StreetAddressRequired = _userSettings.StreetAddressRequired;
            model.StreetAddress2Enabled = _userSettings.StreetAddress2Enabled;
            model.StreetAddress2Required = _userSettings.StreetAddress2Required;
            model.ZipPostalCodeEnabled = _userSettings.ZipPostalCodeEnabled;
            model.ZipPostalCodeRequired = _userSettings.ZipPostalCodeRequired;
            model.CityEnabled = _userSettings.CityEnabled;
            model.CityRequired = _userSettings.CityRequired;
            model.CountryEnabled = _userSettings.CountryEnabled;
            model.StateProvinceEnabled = _userSettings.StateProvinceEnabled;
            model.PhoneEnabled = _userSettings.PhoneEnabled;
            model.PhoneRequired = _userSettings.PhoneRequired;
            model.FaxEnabled = _userSettings.FaxEnabled;
            model.FaxRequired = _userSettings.FaxRequired;
            model.NewsletterEnabled = _userSettings.NewsletterEnabled;
            model.AcceptPrivacyPolicyEnabled = _userSettings.AcceptPrivacyPolicyEnabled;
            model.UsernamesEnabled = _userSettings.UsernamesEnabled;
            model.CheckUsernameAvailabilityEnabled = _userSettings.CheckUsernameAvailabilityEnabled;
            model.DisplayCaptcha = _captchaSettings.Enabled && _captchaSettings.ShowOnRegistrationPage;
            
            if (_userSettings.CountryEnabled)
            {
                model.AvailableCountries.Add(new SelectListItem() { Text = _localizationService.GetResource("Address.SelectCountry"), Value = "0" });
                foreach (var c in _countryService.GetAllCountries())
                {
                    model.AvailableCountries.Add(new SelectListItem() { Text = c.GetLocalized(x => x.Name), Value = c.Id.ToString(), Selected = (c.Id == model.CountryId) });
                }


                if (_userSettings.StateProvinceEnabled)
                {
                    //states
                    var states = _stateProvinceService.GetStateProvincesByCountryId(model.CountryId).ToList();
                    if (states.Count > 0)
                    {
                        foreach (var s in states)
                            model.AvailableStates.Add(new SelectListItem() { Text = s.GetLocalized(x => x.Name), Value = s.Id.ToString(), Selected = (s.Id == model.StateProvinceId) });
                    }
                    else
                        model.AvailableStates.Add(new SelectListItem() { Text = _localizationService.GetResource("Address.OtherNonUS"), Value = "0" });

                }
            }
            */

            var errorList = ModelState.Values.SelectMany(m => m.Errors)
                                 .Select(e => e.ErrorMessage)
                                 .ToList();
            if (errorList != null && errorList.Count > 0)
            {
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return Json(new { success = false, responseText = String.Join("<br>", errorList) }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Response.StatusCode = (int)HttpStatusCode.OK;
                return Json(new { success = true, responseText = "Başarılı" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AccountActivation(string token, string email)
        {
            var user = _userSvc.GetUserByEmail(email);
            if (user == null)
                return RedirectToRoute("HomePage");

            var cToken = user.GetAttribute<string>(SystemUserAttributeNames.AccountActivationToken, _siteContext.CurrentSite.Id);
            if (String.IsNullOrEmpty(cToken))
                return RedirectToRoute("HomePage");

            if (!cToken.Equals(token, StringComparison.InvariantCultureIgnoreCase))
                return RedirectToRoute("HomePage");

            //activate user account
            user.Active = true;
            _userSvc.Update(user);
            _genericAttributeSvc.SaveAttribute(user, SystemUserAttributeNames.AccountActivationToken, "", _siteContext.CurrentSite.Id);
            //send welcome message
            _workflowMessageSvc.SendUserWelcomeMessage(user, _workContext.WorkingLanguage.Id);

            
            string result = _localizationSvc.GetResource("Account.AccountActivation.Activated");
            //return View(model: result);
            return RedirectToAction("Login", "User");
        }

        [AresAuthorize(ResourceKey = "Admin.User.Read")]
        public ActionResult List()
        {
            UserListModel model = new UserListModel();
            model.UserModels = _userSvc.GetAll().ToList().Select(x => x.ToModel()).ToList();

            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.User.Read")]
        public ActionResult ListUsers(UserListModel model, StoreRequestParameters parameters)
        {
            string jsonString = JsonHelper.JsonSerializer<IList<UserModel>>(
                _userSvc.GetAll().Where(u=>
                ((model.SearchFirstName ?? "") == "" || u.FirstName == model.SearchFirstName) &&
                ((model.SearchLastName ?? "") == "" || u.LastName == model.SearchLastName)  &&
                ((model.SearchUsername ?? "") == "" || u.Username == model.SearchUsername) &&
                ((model.SearchEmail ?? "") == "" || u.Email == model.SearchEmail) 
                ).ToList().Select(x => x.ToModel()).ToList());

            var result = new { success = true, data = jsonString, searchModel = JsonHelper.JsonSerializer<UserListModel>(model), Message = "" };
            JsonResult jsonRes = Json(result, JsonRequestBehavior.AllowGet);

            return jsonRes;
        }


        public ActionResult CreateOrUpdate(string itemId, string imageUrl, string mimeType, string uploadFilename)
        {
            UserModel response = null;
            

            int nItemId = -1;
            int.TryParse(itemId, out nItemId);
            //nUserId = nUserId == 0 ? -1 : nUserId;

            if (nItemId != -1)
            {
                User record = _userSvc.GetById(nItemId);
                response = record.ToModel();
                //response.JobTitle = record.GetLocalized(x => x.JobTitle);
                if (record != null) // update
                {
                    response.UserPicture = record.GetPictureModel();

                    if (string.IsNullOrEmpty(response.UserPicture.ImageUrl))
                        response.UserPicture.ImageUrl = _mediaSettings.BlankProfileImagePath;

                    var roleListItems = from ur in record.UserRoles
                                        select new Ext.Net.ListItem { Text = ur.Rolename };

                    //response.RoleListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem { Text = "Administrators" }, new Ext.Net.ListItem { Text = "Guests" } };
                    response.RoleListItems = roleListItems.ToArray();

                    response.UserList = _userSvc.GetAll().ToList().Select(u => u.ToModel()).ToList();
                }
                else // insert mode (after image upload popup close)
                {
                    response = new UserModel();


                    string uploadPath = _mediaSettings.ImageUploadPath;

                    var fileName1 = Path.GetFileName(imageUrl);

                    bool isExists = System.IO.Directory.Exists(uploadPath);

                    if (!isExists)
                        System.IO.Directory.CreateDirectory(uploadPath);

                    var path = string.Format("{0}\\{1}", uploadPath, imageUrl);

                    response.UserPicture.ImageUrl = path;
                    response.UserPicture.GuidFilename = imageUrl;
                    response.UserPicture.MimeType = mimeType;
                    response.UserPicture.UploadFilename = uploadFilename;
                    response.RoleListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() { Text = "" } };
                    response.UserList = _userSvc.GetAll().ToList().Select(u => u.ToModel()).ToList();
                }
            }
            else // insert
            {
                response = new UserModel();
                response.UserPicture.ImageUrl = _mediaSettings.BlankProfileImagePath;
                response.RoleListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() {Text = "" }  };
                response.UserList = _userSvc.GetAll().ToList().Select(u => u.ToModel()).ToList();
            }

 
            ViewBag.itemId = nItemId;

            return View(response);
        }

        public ActionResult ListRoles()
        {
            string jsonString = JsonHelper.JsonSerializer<IList<Models.User.RoleModel>>(_roleSvc.GetAll().ToList().Select(x => x.ToModel()).ToList());
            var result = new { success = true, data = jsonString, Message = "" };
            JsonResult jsonRes = Json(result, JsonRequestBehavior.AllowGet);
            return jsonRes;

            //return Json(new { success = true, data = _roleSvc.GetAll().ToList(), Message = "" }, JsonRequestBehavior.AllowGet);
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult CreateOrUpdate(UserModel model)
        {


            if (model.Id == 0) // insert mode
            {
                User record = model.ToEntity();
                

                if (model.ReqRoleListItems != null)
                {
                    var urFromModel = model.ReqRoleListItems.Split(',');


                    foreach (string item in urFromModel)
                    {
                        Role role = _roleSvc.GetById(Int32.Parse(item));
                        record.UserRoles.Add(role);
                    }
                }

                _userSvc.Insert(record);


                Picture pict = new Picture();

                pict.PictureBinary = System.IO.File.ReadAllBytes(Server.MapPath(model.UserPicture.ImageUrl));
                pict.MimeType = model.UserPicture.MimeType;
                pict.UploadFilename = model.UserPicture.UploadFilename;
                pict.GuidFilename = model.UserPicture.GuidFilename;

                _pictureSvc.SetPictureForEntity(record.Id, "User", pict);

                System.IO.File.Delete(Server.MapPath(model.UserPicture.ImageUrl));
            }
            else  // update mode
            {
                User record = _userSvc.GetById(model.Id);

                var urStringArrayFromDb = (from ur in record.UserRoles
                                           select ur.Id + "");




                if (model.ReqRoleListItems != null)
                {
                    var urFromModel = model.ReqRoleListItems.Split(',');

                    // Items in first array but not on second array (to be added to db)
                    IEnumerable<string> onlyInModelNotInDb = urFromModel.Except(urStringArrayFromDb);

                    foreach (string item in onlyInModelNotInDb)
                    {
                        Role role = _roleSvc.GetById(Int32.Parse(item));
                        record.UserRoles.Add(role);
                    }

                    // Items in first array but not second array (to be deleted from db)
                    IEnumerable<string> onlyInDbNotInModel = new List<string>(urStringArrayFromDb.Except(urFromModel));

                    foreach (string item in onlyInDbNotInModel)
                    {
                        Role role = record.UserRoles.Where(ur => ur.Id + "" == item).FirstOrDefault();
                        record.UserRoles.Remove(role);
                    }
                }
                else
                {
                    if(record.UserRoles != null)
                        record.UserRoles.Clear();
                }





                record = model.ToEntity(record);

                _userSvc.Update(record);
            }

            /*
            X.Js.AddScript("parentAutoLoadControl.hide();");
            MessageBus.Default.Publish("refresh", "Click OK to refresh the grid.");
            */
            return this.FormPanel();
        }

        [AresAuthorize(ResourceKey = "Admin.User.Read")]
        public ActionResult ListUsers2(Ares.Web.Razor.Extensions.Datatable.Model.DTParameterModel model)
        {
            var userModels = _userSvc.GetAll();


            /*string jsonString = JsonHelper.JsonSerializer<IList<Models.User.UserModel>>(userModels);
            var result = from c in userModels.ToList()
                         select new[] { Convert.ToString(c.Id), c.Username,
                          c.Email, c.First_name };
            */
            //int locId = _workContext.CurrentUser.Location().Id;

            
            if (!_workContext.CurrentUser.IsAdmin())
            {
                //userModels = (from l in _iEqLocationSvc.GetAll() from u in userModels where  l.User_From_EqLocationUserMapping.Any(lu => lu.Id == u.Id) && l.Id == locId select u);
                //userModels = userModels.Where(u => u.Location().Id == locId);
                //userModels = from u in userModels join l in _iEqLocationSvc.GetAll() on 1 equals 1 where l.User_From_EqLocationUserMapping.Any(lu => lu.Id == u.Id) && l.Id == locId select u;
                userModels = from u in userModels where u.UserRoles.All(r => r.Rolename != EnumRoleNames.Administrators) select u;
            }
            
            if (model.Search != null && !string.IsNullOrEmpty(model.Search.Value))
            {
                userModels = userModels.Where(m => m.Username.Contains(model.Search.Value));
            }

            var totalRecords = userModels.Count();

            userModels = userModels.OrderBy(e => e.Id).Skip(model.Length * ((int)(Math.Ceiling((double)model.Start / model.Length)) + 1 - 1)).Take(model.Length);



            JsonResult jsonRes = Json(new
            {
                sEcho = model.Draw,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                //aaData = result
                aaData = userModels.ToList().Select(x => x.ToModel()).ToList()
            }, JsonRequestBehavior.AllowGet);
            return jsonRes;

            //return Json(new { success = true, data = _roleSvc.GetAll().ToList(), Message = "" }, JsonRequestBehavior.AllowGet);
        }


        [AresAuthorize(ResourceKey = "Admin.User.Create")]
        public ActionResult Create()
        {
            var model = new UserModel();
            model.RoleViewModel = GetMultiSelectListViewInitialModel<Role>(_roleSvc.GetAll().ToList(), null);

            FillDropDowns(model);

            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.User.Create")]
        [System.Web.Mvc.HttpPost]
        public ActionResult Create(UserModel model)
        {

            userValidation(model);

            if (ModelState.IsValid)
            {
                var user = model.ToEntity();

                user.INSERT_USER_ID = _workContext.CurrentUser.Id.ToString();
                user.INSERT_DATETIME = StringHelper.ConvertDateTimeToString(DateTime.Now);
                user.UserGuid = Guid.NewGuid().ToString();


                if (model.RoleViewModel != null && model.RoleViewModel.PostedItems.ItemIds != null)
                {
                    model.RoleViewModel = GetMultiSelectListViewModel<Role>(model.RoleViewModel.PostedItems, _roleSvc.GetAll().ToList());
                    var urFromModel = model.RoleViewModel.PostedItems.ItemIds;


                    foreach (string item in urFromModel)
                    {
                        Role role = _roleSvc.GetById(Int32.Parse(item));
                        user.UserRoles.Add(role);
                    }


                }

                User returnedUSer = _userSvc.Insert(user);

                User dbUser = _userSvc.GetById(returnedUSer.Id);
                /*
                EqLocation loc = _workContext.CurrentUser.Location();
                loc.User_From_EqLocationUserMapping.Add(dbUser);
                _iEqLocationSvc.Update(loc);
                */
                _genericAttributeSvc.SaveAttribute(user, "Nationality_id", model.Nationality_id, _siteContext.CurrentSite.Id);


                if (model.UserPasswordChangeModel != null && !string.IsNullOrEmpty(model.UserPasswordChangeModel.NewPassword) && !string.IsNullOrEmpty(model.UserPasswordChangeModel.NewPasswordAgain))
                {
                    if (model.UserPasswordChangeModel.CurrentPassword != null && model.UserPasswordChangeModel.CurrentPassword == "")
                        model.UserPasswordChangeModel.CurrentPassword = null;

                    _userSvc.PasswordChange(model.Username, model.UserPasswordChangeModel.CurrentPassword, model.UserPasswordChangeModel.NewPasswordAgain);
                }
                    

                SuccessNotification(_localizationSvc.GetResource("Notifications.User.Created"));

                return RedirectToAction("List");
            }
            else
            {
                model.RoleViewModel = GetMultiSelectListViewInitialModel<Role>(_roleSvc.GetAll().ToList(), null);


                FillDropDowns(model);
                return View(model);
            }
        }

        [AresAuthorize(ResourceKey = "Admin.User.Update")]
        [UserLocationAuthorize]
        public ActionResult Edit(int id)
        {
            User user = _userSvc.GetById(id);
            var model = user.ToModel();


            model.UserPasswordChangeModel = new UserPasswordChangeModel();
            /*
            if (user.Password == null && user.PasswordSalt == null)
                model.IsADUser = true;
                */

            model.Nationality_id = user.GetAttribute<int>(model.Id, "Nationality_id", _genericAttributeSvc, _siteContext.CurrentSite.Id);

            FillDropDowns(model);
            List<Role> allRoles = _roleSvc.GetAll().ToList();
            if (!_workContext.CurrentUser.IsAdmin())
            {
                allRoles.RemoveAt(0);
            }

            model.RoleViewModel = GetMultiSelectListViewInitialModel<Role>(allRoles, user.UserRoles.ToList());

            return View(model);
        }


        [AresAuthorize(ResourceKey = "Admin.User.Update")]
        [UserLocationAuthorize]
        [System.Web.Mvc.HttpPost]
        public ActionResult Edit(UserModel model)
        {
            userValidation(model);

            if (ModelState.IsValid)
            {
                var user = _userSvc.GetById(model.Id);
                var urStringArrayFromDb = (from ur in user.UserRoles select ur.Id + "");

                if (model.RoleViewModel != null && model.RoleViewModel.PostedItems.ItemIds != null)
                {
                    model.RoleViewModel = GetMultiSelectListViewModel<Role>(model.RoleViewModel.PostedItems, _roleSvc.GetAll().ToList());
                    var urFromModel = model.RoleViewModel.PostedItems.ItemIds;

                    // Items in first array but not on second array (to be added to db)
                    IEnumerable<string> onlyInModelNotInDb = urFromModel.Except(urStringArrayFromDb);

                    foreach (string item in onlyInModelNotInDb)
                    {
                        Role role = _roleSvc.GetById(Int32.Parse(item));
                        user.UserRoles.Add(role);
                    }

                    // Items in first array but not second array (to be deleted from db)
                    IEnumerable<string> onlyInDbNotInModel = new List<string>(urStringArrayFromDb.Except(urFromModel));

                    foreach (string item in onlyInDbNotInModel)
                    {
                        Role role = user.UserRoles.Where(ur => ur.Id + "" == item).FirstOrDefault();
                        user.UserRoles.Remove(role);
                    }
                }
                else
                {
                    if (user.UserRoles != null)
                        user.UserRoles.Clear();
                }


                user = model.ToEntity(user);
                user.UPDATE_USER_ID = _workContext.CurrentUser.Id.ToString();
                user.UPDATE_DATETIME = StringHelper.ConvertDateTimeToString(DateTime.Now);

                /*
                if (model.IsADUser)
                {
                    user.Password = null;
                    user.PasswordSalt = null;
                    user.PasswordFormatId = 0;
                }
                */

                _userSvc.Update(user);

                _genericAttributeSvc.SaveAttribute(user, "Nationality_id", model.Nationality_id, _siteContext.CurrentSite.Id);



                if (model.UserPasswordChangeModel != null && !string.IsNullOrEmpty(model.UserPasswordChangeModel.NewPassword) && !string.IsNullOrEmpty(model.UserPasswordChangeModel.NewPasswordAgain))
                    //_userSvc.PasswordChange(model.Username, model.UserPasswordChangeModel.CurrentPassword, model.UserPasswordChangeModel.NewPasswordAgain);
                    _userSvc.PasswordChange(model.Username, model.UserPasswordChangeModel.NewPasswordAgain);

                SuccessNotification(_localizationSvc.GetResource("Notifications.User.Updated"));

                return RedirectToAction("List");
            }
            else
            {
                User user = _userSvc.GetById(model.Id);
                model = user.ToModel();


                model.UserPasswordChangeModel = new UserPasswordChangeModel();
                /*
                if (user.Password == null && user.PasswordSalt == null)
                    model.IsADUser = true;
                    */

                FillDropDowns(model);
                model.RoleViewModel = GetMultiSelectListViewInitialModel<Role>(_roleSvc.GetAll().ToList(), user.UserRoles.ToList());

                return View(model);
            }
        }


        [AresAuthorize(ResourceKey = "Admin.User.Delete")]
        [UserLocationAuthorize]
        public ActionResult Delete(string itemId)
        {
            int nItemId = -1;
            int.TryParse(itemId, out nItemId);

            if (nItemId != -1)
            {
                User user = _userSvc.GetById(nItemId);

                _pictureSvc.DeletePictureFromEntity(user.Id, "User", user.GetPicture());
                _userSvc.Delete(user);
            }
            else
            {

            }


            return this.Direct();
        }
        
        
        public ActionResult NotAuthorized()
        {

            return View();
        }

        public void FillDropDowns(UserModel model)
        {
            //model.Nationality_id_Nationality_SelectList = new SelectList(_iParameteritemssvc.GetAll().Where(p => p.ParameterGroup.ReserveName == "NATIONALITY").ToList(), "Id", _workContext.WorkingLanguage.Id == 1 ? "Title_TR" : "Title_EN");

            model.Nationality_id_Nationality_SelectList = new SelectList(_iCountrySvc.GetAll().OrderBy(m => m.DisplayOrder).ToList(), "Id", "Name");

            model.Jobtitle_id_JobTitle_SelectList = new SelectList(_iJobTitlesvc.GetAll().ToList(), "Id", "Name");

            model.Manager_id_User_SelectList = new SelectList(_userSvc.GetAll().ToList(), "Id", "Fullname");

            model.Orgunit_id_OrgUnit_SelectList = new SelectList(_iOrgUnitsvc.GetAll().ToList(), "Id", "Name");

            model.Language_id_Language_SelectList = new SelectList(_iLanguagesvc.GetAllLanguages(), "Id", "Name");


        }


        public ActionResult PasswordChange()
        {



            return View();
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult PasswordChange(UserPasswordChangeModel modelIn)
        {
            _userSvc.PasswordChange(modelIn.Username, modelIn.CurrentPassword, modelIn.NewPasswordAgain);


            return View();
        }


        public ActionResult PasswordRecovery()
        {


            return View(new PasswordRecoveryModel());
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult PasswordRecovery(PasswordRecoveryModel model)
        {

            if (!Request.Url.ToString().ToLowerInvariant().Contains("password"))
                return PasswordRecovery();

            if (ModelState.IsValid)
            {
                var user = _userSvc.GetUserByEmail(model.Email);
                if (user != null && user.Active.GetValueOrDefault(false) && !user.Deleted.GetValueOrDefault(false))
                {
                    //save token and current date
                    var passwordRecoveryToken = Guid.NewGuid();
                    _genericAttributeSvc.SaveAttribute(user, SystemUserAttributeNames.PasswordRecoveryToken, passwordRecoveryToken.ToString(), _siteContext.CurrentSite.Id);
                    /*
                    DateTime? generatedDateTime = DateTime.UtcNow;
                    _genericAttributeSvc.SaveAttribute(user, SystemUserAttributeNames.PasswordRecoveryTokenDateGenerated, generatedDateTime);
                    */
                    //send email
                    _workflowMessageSvc.SendUserPasswordRecoveryMessage(user, _workContext.WorkingLanguage.Id);

                    model.Result = _localizationSvc.GetResource("User.PasswordRecovery.EmailHasBeenSent");
                }
                else
                {
                    model.Result = _localizationSvc.GetResource("User.PasswordRecovery.EmailNotFound");
                    ModelState.AddModelError("", model.Result);
                }

            }


            var errorList = ModelState.Values.SelectMany(m => m.Errors).Select(e => e.ErrorMessage).ToList();

            if (errorList != null && errorList.Count > 0)
            {
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                Response.TrySkipIisCustomErrors = true;
                return Json(new { success = false, responseText = String.Join("<br>", errorList) }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Response.StatusCode = (int)HttpStatusCode.OK;
                return Json(new { success = true, responseText = "Başarılı" }, JsonRequestBehavior.AllowGet);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }


        public ActionResult PasswordRecoveryConfirm(string token, string email)
        {
            var user = _userSvc.GetUserByEmail(email);
            if (user == null)
                return RedirectToRoute("HomePage");

            var model = new PasswordRecoveryConfirmModel();
            model.Email = email;
            model.Token = token;

            /*
            //validate token
            if (!user.IsPasswordRecoveryTokenValid(token))
            {
                model.DisablePasswordChanging = true;
                model.Result = _localizationSvc.GetResource("User.PasswordRecovery.WrongToken");
            }

            //validate token expiration date
            if (user.IsPasswordRecoveryLinkExpired(_userSettings))
            {
                model.DisablePasswordChanging = true;
                model.Result = _localizationSvc.GetResource("User.PasswordRecovery.LinkExpired");
            }
            */
            return View(model);
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult PasswordRecoveryConfirm(string token, string email, PasswordRecoveryConfirmModel model)
        {
            var user = _userSvc.GetUserByEmail(email);
            if (user == null)
                return RedirectToRoute("HomePage");

            /*
            //validate token
            if (!user.IsPasswordRecoveryTokenValid(token))
            {
                model.DisablePasswordChanging = true;
                model.Result = _localizationSvc.GetResource("User.PasswordRecovery.WrongToken");
            }

            //validate token expiration date
            if (user.IsPasswordRecoveryLinkExpired(_userSettings))
            {
                model.DisablePasswordChanging = true;
                model.Result = _localizationSvc.GetResource("User.PasswordRecovery.LinkExpired");
                return View(model);
            }
            */

            if (PasswordAdvisor.CheckStrength(model.NewPassword) < PasswordScore.Medium)
            {
                ModelState.AddModelError("", _localizationSvc.GetResource("Account.Register.Errors.PasswordIsNotStrong"));
                model.Result = _localizationSvc.GetResource("Account.Register.Errors.PasswordIsNotStrong");
            }

            if (ModelState.IsValid)
            {
                bool success = _userSvc.PasswordChange(user.Username, model.NewPassword);

                if (success)
                {
                    _genericAttributeSvc.SaveAttribute(user, SystemUserAttributeNames.PasswordRecoveryToken, "", _siteContext.CurrentSite.Id);

                    model.DisablePasswordChanging = true;
                    model.Result = _localizationSvc.GetResource("User.PasswordRecovery.PasswordHasBeenChanged");
                }
                else
                {
                    //model.Result = response.Errors.FirstOrDefault();
                }

                return View(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }


    }


    public enum PasswordScore
    {
        Blank = 0,
        VeryWeak = 1,
        Weak = 2,
        Medium = 3,
        Strong = 4,
        VeryStrong = 5
    }

    public class PasswordAdvisor
    {
        public static PasswordScore CheckStrength(string password)
        {
            int score = 0;

            if (password.Length < 1)
                return PasswordScore.Blank;
            if (password.Length < 4)
                return PasswordScore.VeryWeak;

            if (password.Length >= 6)
                score++;
            if (password.Length >= 12)
                score++;
            if (Regex.Match(password, "\\d+", RegexOptions.ECMAScript).Success)
                score++;
            if (Regex.Match(password, "[a-z]", RegexOptions.ECMAScript).Success &&
              Regex.Match(password, "[A-Z]", RegexOptions.ECMAScript).Success)
                score++;
            if (Regex.Match(password, ".[!,@,#,$,%,^,&,*,?,_,~,-,£,(,)]", RegexOptions.ECMAScript).Success)
                score++;

            return (PasswordScore)score;
        }
    }


    public class UserLocationAuthorizeAttribute : ActionFilterAttribute
    {
        public IWorkContext WorkContext { get; set; }
        public IUserSvc UserSvc { get; set; }
        //public string IdParamName { get; set; }
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (WorkContext.CurrentUser.IsAdmin())
            {
                base.OnActionExecuting(context);
                return;
            }

            var httpContext = context.HttpContext;

            List<int> locId = WorkContext.CurrentUser.Location().Select(l => l.Id).ToList();

            if (!string.IsNullOrEmpty(context.RouteData.Values["id"].ToString()))
            {
                //var id = httpContext.ActionParameters[IdParamName] as Int32?;
                //int userID = int.Parse(context.ActionParameters[IdParamName].ToString());
                int userID = int.Parse(context.RouteData.Values["id"].ToString());

                User tmpUser = UserSvc.GetById(userID);

                if (tmpUser != null && tmpUser.Location().Any( l=> locId.Contains(l.Id)) )
                {
                    base.OnActionExecuting(context);
                    return;
                }

            }
            
            context.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "User", action = "NotAuthorized" }));
        }
    }

    /*
    [AttributeUsageAttribute(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class UserLocationAuthorizeAttribute : AuthorizeAttribute
    {

        public IWorkContext WorkContext { get; set; }
        public IUserSvc UserSvc { get; set; }

        public string IdParamName { get; set; }

        //Called when access is denied
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "User", action = "NotAuthorized" }));
        }

        //Core authentication, called before each action
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (WorkContext.CurrentUser.IsAdmin())
                return true;


            int locId = WorkContext.CurrentUser.Location().Id;
            if (!string.IsNullOrEmpty(httpContext.Request.Params[IdParamName]))
            {
                //var id = httpContext.ActionParameters[IdParamName] as Int32?;
                int userID = int.Parse(httpContext.Request.Params[IdParamName].ToString());

                User tmpUser = UserSvc.GetById(userID);

                if (tmpUser != null)
                    return UserSvc.GetById(userID).Location().Id == locId;
            }

            return false;
        }
    }
    */
}
