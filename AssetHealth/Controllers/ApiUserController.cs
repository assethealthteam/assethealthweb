﻿using Ares.Core.Framework;
using Ares.Core.Framework.Logging;
using Ares.Core.Framework.Logging.Business;
using Ares.Core.Helpers;
using Ares.Sync.Controllers;
using Ares.Sync.DTO;
using Ares.Sync.Services;
using Ares.Sync.Svc;
using Ares.UMS.DTO.Localization;
using Ares.UMS.DTO.Media;
using Ares.UMS.DTO.Users;
using Ares.UMS.Svc;
using Ares.UMS.Svc.Common;
using Ares.UMS.Svc.Directory;
using Ares.UMS.Svc.Localization;
using Ares.UMS.Svc.Media;
using Ares.UMS.Svc.Messages;
using Ares.UMS.Svc.Security;
using Ares.UMS.Svc.Users;
using Ares.Web.Helpers;
using Ares.Web.JSON;
using Ares.Web.Models.User;
using Ares.Web.Mvc.Entities;
using Ares.Web.Mvc.Result;
using Ares.Web.Svc.Authentication;
using Ares.Web.Validators.Users;
using AssetHealth.Common.DTO;
using AssetHealth.Common.Svc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Mvc;

namespace AssetHealth.Controllers
{
    public class ApiUserController : ApiController
    {
        private readonly IStateProvinceSvc _stateProvinceSvc;
        private readonly IGenericAttributeSvc _genericAttributeSvc;
        private readonly ILocalizationSvc _localizationSvc;
        private readonly IPictureSvc _pictureSvc;
        private readonly IUserSvc _userSvc;
        private readonly IRoleSvc _roleSvc;
        private readonly IWorkContext _workContext;
        private readonly IUserRegistrationSvc _userRegistrationSvc;
        private readonly IAuthenticationSvc _authenticationSvc;
        private readonly IWorkflowMessageSvc _workflowMessageSvc;
        private readonly INewsLetterSubscriptionSvc _newsLetterSubscriptionSvc;
        private readonly IWebHelper _webHelper;
        private readonly UserSettings _userSettings;
        private readonly LocalizationSettings _localizationSettings;
        private readonly MediaSettings _mediaSettings;
        private readonly IBusinessLogger _businessLogger;
        private readonly IEncryptionSvc _iEncryptionSvc;
        private readonly ICountrySvc _iCountrySvc;
        //private readonly IParameterItemSvc _iParameteritemssvc;

        private readonly ILogger _iLogger;
        private readonly IJobTitleSvc _iJobTitlesvc;
        private readonly IOrgUnitSvc _iOrgUnitsvc;
        private readonly ILanguageSvc _iLanguagesvc;
        private readonly ISiteContext _siteContext;


        private readonly IDeviceSvc _iDeviceSvc;
        private readonly IEqLocationSvc _iEqLocationSvc;


        public ApiUserController(IStateProvinceSvc stateProvinceSvc, IGenericAttributeSvc genericAttributeSvc, ILocalizationSvc localizationSvc,
    UserSettings userSettings, IPictureSvc pictureSvc, 
    //IParameterItemSvc iParameteritemssvc,
    IEncryptionSvc iEncryptionSvc,
    IJobTitleSvc JobTitlesvc,
    IOrgUnitSvc OrgUnitsvc, ILogger iLogger,
    ILanguageSvc Languagesvc,
     ISiteContext siteContext,
     ICountrySvc iCountrySvc,
    IUserSvc userSvc, IRoleSvc roleSvc, IWorkContext workContext, IUserRegistrationSvc userRegistrationSvc, IAuthenticationSvc authenticationSvc, IWorkflowMessageSvc workflowMessageSvc,
    INewsLetterSubscriptionSvc newsLetterSubscriptionSvc, IWebHelper webHelper, LocalizationSettings localizationSettings, MediaSettings mediaSettings, IBusinessLogger businessLogger,
            IEqLocationSvc iEqLocationSvc, IDeviceSvc iDeviceSvc
            )
        {
            this._stateProvinceSvc = stateProvinceSvc;
            this._genericAttributeSvc = genericAttributeSvc;
            this._pictureSvc = pictureSvc;
            this._localizationSvc = localizationSvc;
            this._userSettings = userSettings;
            this._userSvc = userSvc;
            this._roleSvc = roleSvc;
            this._workContext = workContext;
            this._userRegistrationSvc = userRegistrationSvc;
            this._authenticationSvc = authenticationSvc;
            this._workflowMessageSvc = workflowMessageSvc;
            this._newsLetterSubscriptionSvc = newsLetterSubscriptionSvc;
            this._webHelper = webHelper;
            this._localizationSettings = localizationSettings;
            this._mediaSettings = mediaSettings;
            this._businessLogger = businessLogger;

            this._siteContext = siteContext;
            //this._iParameteritemssvc = iParameteritemssvc;
            this._iLogger = iLogger;
            this._iJobTitlesvc = JobTitlesvc;
            this._iOrgUnitsvc = OrgUnitsvc;
            this._iLanguagesvc = Languagesvc;
            this._iEncryptionSvc = iEncryptionSvc;
            this._iCountrySvc = iCountrySvc;

            this._iEqLocationSvc = iEqLocationSvc;
            this._iDeviceSvc = iDeviceSvc;

        }




        [System.Web.Http.Route("api/ApiUser/Login")]
        public IHttpActionResult Login(Ares.Web.Models.User.LoginModel model)
        {
            ApiTransferObject<int> result = new ApiTransferObject<int>();
            if(model != null)
            {
                if (_userSettings.UsernamesEnabled && model.Username != null)
                {
                    model.Username = model.Username.Trim();
                }

                if (_userSvc.ValidateUser(_userSettings.UsernamesEnabled ? model.Username : model.Email, model.Password))
                {
                    var user = _userSettings.UsernamesEnabled ? _userSvc.GetUserByUsername(model.Username) : _userSvc.GetUserByEmail(model.Email);

                    //sign in new user
                    _authenticationSvc.SignIn(user, model.RememberMe);

                    result.SetInformation("SUCCESS");

                    if(user != null && user.UserRoles != null && user.UserRoles.Count > 0)
                        result.Data = user.UserRoles.FirstOrDefault().Id;
                }
                else
                {
                    result.SetError("Kullanıcı Adı veya şifre yanlış");
                }

            }
            
            else
            {
                result.SetError("Kullanıcı Adı veya şifre yanlış");
            }

            

            return new RawJsonActionResult(JsonConvert.SerializeObject(result, new AresJsonSerializerSettings()));
        }


        [System.Web.Http.Route("api/ApiUser/Register")]
        public IHttpActionResult Register(BaseRequest<UserModel> model)
        {
            ApiTransferObject result = new ApiTransferObject();

            if (string.IsNullOrEmpty(model.Data.CompanyCode))
            {
                result.SetError("Şirket Kodu Alanı Zorunludur");
                return new RawJsonActionResult(JsonConvert.SerializeObject(result, new AresJsonSerializerSettings()));
            }

            EqLocation location = _iEqLocationSvc.GetAll().FirstOrDefault(l => l.Cid == model.Data.CompanyCode);
            if (location == null)
            {
                result.SetError("Şirket Kodu hatallıdır.Lütfen kontrol ediniz");
                return new RawJsonActionResult(JsonConvert.SerializeObject(result, new AresJsonSerializerSettings()));
            }


            var instance = (UserModelValidator)FrameworkCtx.Current.ContainerManager.ResolveUnregistered(typeof(UserModelValidator));
            Dictionary<string,string> validationResult = instance.userValidation(model.Data, "");

            if (validationResult != null && validationResult.Count > 0)
            {
                result.SetError(validationResult.FirstOrDefault().Value);

                return new RawJsonActionResult(JsonConvert.SerializeObject(result, new AresJsonSerializerSettings()));
            }

            
            //model.UserModel.Active = true;
            

            //User user = model.UserModel.ToEntity();
            User user = new User();
            //user.PasswordFormatId = _userSettings.DefaultPasswordFormatId;
            user.UserGuid = Guid.NewGuid().ToString();
            //user.Active = true;
            user.Email = model.Data.Email;
            user.FirstName = model.Data.First_name;
            user.LastName = model.Data.Last_name;
            user.Username = model.Data.Username;
            user.MobilePhone = model.Data.Mobile_phone;
            user.INSERT_DATETIME = StringHelper.ConvertDateTimeToString(DateTime.Now);


            if (model.Data.Id == 0)
                user = _userSvc.Insert(user);
            else
                _userSvc.Update(user);


            _genericAttributeSvc.SaveAttribute(user, "Nationality_id", model.Data.Nationality_id, _siteContext.CurrentSite.Id);

            if (model.Data.UserPasswordChangeModel != null && !string.IsNullOrEmpty(model.Data.UserPasswordChangeModel.NewPassword) && !string.IsNullOrEmpty(model.Data.UserPasswordChangeModel.NewPasswordAgain))
            {
                if (model.Data.UserPasswordChangeModel.CurrentPassword != null && model.Data.UserPasswordChangeModel.CurrentPassword == "")
                    model.Data.UserPasswordChangeModel.CurrentPassword = null;

                _userSvc.PasswordChange(model.Data.Username, model.Data.UserPasswordChangeModel.CurrentPassword, model.Data.UserPasswordChangeModel.NewPasswordAgain);
            }
                


            if (_userSettings.NotifyNewUserRegistration)
                _workflowMessageSvc.SendUserRegisteredNotificationMessage(user, _localizationSettings.DefaultAdminLanguageId);


            switch (_userSettings.EnumUserRegistrationType)
            {
                case EnumUserRegistrationType.EmailValidation:
                    {
                        //email validation message
                        _genericAttributeSvc.SaveAttribute(user, SystemUserAttributeNames.AccountActivationToken, Guid.NewGuid().ToString(), _siteContext.CurrentSite.Id);
                        _workflowMessageSvc.SendUserEmailValidationMessage(user, _workContext.WorkingLanguage.Id);

                        //result
                        //return RedirectToRoute("RegisterResult", new { resultId = (int)EnumUserRegistrationType.EmailValidation });
                        break;
                    }
                case EnumUserRegistrationType.AdminApproval:
                    {
                        //return RedirectToRoute("RegisterResult", new { resultId = (int)EnumUserRegistrationType.AdminApproval });
                        break;
                    }
                case EnumUserRegistrationType.Standard:
                    {
                        //send user welcome message
                        _workflowMessageSvc.SendUserWelcomeMessage(user, _workContext.WorkingLanguage.Id);

                        //var redirectUrl = Url.RouteUrl("RegisterResult", new { resultId = (int)EnumUserRegistrationType.Standard });

                        //return Redirect(redirectUrl);
                        break;
                    }
                default:
                    {
                        //return RedirectToRoute("HomePage");
                        break;
                    }
            }

            // End of normal user registration

            if (!string.IsNullOrEmpty(model.Data.CompanyCode))
            {
                location.User_From_EqLocationUserMapping.Add(user);
                _iEqLocationSvc.Update(location);
            }

            if (!string.IsNullOrEmpty(model.AndroidId))
            {
                Device device = _iDeviceSvc.GetAll().FirstOrDefault(d => d.Udid == model.AndroidId);

                if (device == null)
                {
                    device = new Device() { Udid = model.AndroidId,
                        Platform_type = Ares.Sync.DTO.Enums.EnumDevicePlatform_type.Android,
                        Device_model = model.DeviceName
                    };
                    _iDeviceSvc.Insert(device);
                }
            }


            foreach (var validationItem in validationResult)
            {
                ModelState.AddModelError(validationItem.Key, validationItem.Value);
            }

            result.SetInformation("Başarıyla kaydedildi");

            return new RawJsonActionResult(JsonConvert.SerializeObject(result, new AresJsonSerializerSettings()));
        }

        private void authenticateDevice(Device device)
        {
            if (device.User_From_DeviceUserMapping != null && device.User_From_DeviceUserMapping.Count > 0)
            {
                _authenticationSvc.SignIn(device.User_From_DeviceUserMapping[0], true);
            }
        }


        [System.Web.Http.Route("api/ApiUser/TechServiceLogin")]
        //public IHttpActionResult TechServiceLogin(TechServiceInput input)
        public IHttpActionResult TechServiceLogin(BaseRequest<List<AutoUpdateEntity>> input)
        {
            ApiTransferObject<int> result = new ApiTransferObject<int>();
            string json = JsonConvert.SerializeObject(input, new JsonSerializerSettings() { Formatting = Formatting.Indented });
            if (!string.IsNullOrEmpty(input.AndroidId) && !string.IsNullOrEmpty(input.MobileNumber)) // login 
            {

                Device device = _iDeviceSvc.GetAll().FirstOrDefault(d => d.Mobile_number == input.MobileNumber && d.Udid == input.AndroidId && (d.Active ?? false));
                

                if (device != null && !string.IsNullOrEmpty(device.Udid))
                {
                    authenticateDevice(device);

                    device.Last_contact_time = DateTime.Now;
                    device.Update_datetime = StringHelper.ConvertDateTimeToString(DateTime.Now);
                    device.Last_ip_address = _webHelper.GetCurrentIpAddress();
                    device.Package_info = json;
                    _iDeviceSvc.Update(device);

                    
                    _businessLogger.Log(new LogItem() { Message = input.AndroidId + " device has logged in " + input.MobileNumber, LongMessage = json });

                    result.SetInformation("SUCCESS");
                    result.Data = 1;

                }
                else
                { 
                    _businessLogger.Log(new LogItem() { Message = input.AndroidId + " device could not log in "  + input.MobileNumber , LongMessage = json });
                    result.SetError("Lütfen Telefon Numaranızın kayıtlı olduğu cihazla işlem yapınız.Sorun devam ederse lütfen sistem yöneticinizle görüşün");
                }

            }
            else
            {
                _businessLogger.Log(new LogItem() { Message = input.AndroidId + " Telefon mumarası boş olamaz " + input.MobileNumber, LongMessage = json });

                result.SetError("Telefon mumarası boş olamaz");
            }


            return new RawJsonActionResult(JsonConvert.SerializeObject(result, new AresJsonSerializerSettings()));
        }


        [System.Web.Http.Route("api/ApiUser/Ping")]
        public IHttpActionResult Ping(BaseRequest<List<AutoUpdateEntity>> input)
        {
            ApiTransferObject<int> result = new ApiTransferObject<int>();
            if (!string.IsNullOrEmpty(input.AndroidId) )
            {
                Device device = _iDeviceSvc.GetAll().OrderByDescending(d => d.Id).FirstOrDefault(d => d.Udid == input.AndroidId || d.Imei_number == input.ImeiNumber);

                string json = JsonConvert.SerializeObject(input, new JsonSerializerSettings() { Formatting = Formatting.Indented });

                if (device != null && !string.IsNullOrEmpty(device.Imei_number))
                {
                    device.Last_contact_time = DateTime.Now;
                    device.Update_datetime = StringHelper.ConvertDateTimeToString(DateTime.Now);
                    device.Last_ip_address = _webHelper.GetCurrentIpAddress();
                    device.Package_info = json;
                    if (string.IsNullOrEmpty(device.Udid) && !string.IsNullOrEmpty(input.AndroidId))
                        device.Udid = input.AndroidId;

                    _iDeviceSvc.Update(device);

                    _businessLogger.Log(new LogItem() { Message = input.AndroidId + " device is Alive" , LongMessage = json });

                    result.SetInformation("SUCCESS");
                    result.Data = 1;

                }
                else
                {
                    Device deviceTmp = new Device() {
                        Object_id = Guid.NewGuid().ToString(),
                        Udid = input.AndroidId,
                        Platform_type = Ares.Sync.DTO.Enums.EnumDevicePlatform_type.Android,
                        Device_name = input.DeviceName,
                        Last_contact_time = DateTime.Now,
                        Imei_number = input.ImeiNumber,
                        Package_info = json,
                        Last_ip_address = _webHelper.GetCurrentIpAddress(),
                         Insert_datetime = StringHelper.ConvertDateTimeToString(DateTime.Now)
                    };

                    _iDeviceSvc.Insert(deviceTmp);

                    
                    _businessLogger.Log(new LogItem() { Message = input.AndroidId + " device could not log in " + input.MobileNumber, LongMessage = json });
                    result.SetInformation("SUCCESS");
                }

            }
            else
            {
                string json = JsonConvert.SerializeObject(input, new JsonSerializerSettings() { Formatting = Formatting.Indented });
                _businessLogger.Log(new LogItem() { Message = input.AndroidId + " Device can not be recognized " + input.MobileNumber, LongMessage = json });

                result.SetError("Device can not be recognized");
            }


            return new RawJsonActionResult(JsonConvert.SerializeObject(result, new AresJsonSerializerSettings()));
        }

    }





}
