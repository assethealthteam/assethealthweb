﻿



using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.IO;
using System.Web.Http;
using System.Net;
using System.Web.Http.ModelBinding;
using System.Web.Security;
using Ext.Net.MVC;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using System.Web.Mvc;
using System.Linq;
using Ares.UMS.Svc.Directory;
using Ares.UMS.DTO.Users;
using Ares.UMS.Svc.Users;
using Ares.Web.Svc.Authentication;
using Ares.Web.Svc.Web;
using Ares.UMS.Svc.Common;
using Ares.UMS.DTO.Messages;
using Ares.UMS.Svc.Messages;
using Ares.Core.Helpers;
using Ares.UMS.DTO.Localization;
using Ares.UMS.Svc.Localization;
using Ares.UMS.DTO.Media;
using Ares.UMS.Svc.Media;
using Ares.Web.Models.Role;
using Ares.UMS.Svc.Security;
using Ares.UMS.DTO.Security;
using Ares.Web.Helpers;
using Ares.Web.Attributes;
using Ares.Web.Controllers;
using Ares.UMS.Svc.Configuration;
using Ares.UMS.DTO.Configuration;
using Ares.UMS.Svc;
using Ares.Web.Razor.Extensions.Datatable.Model;
using AssetHealth.Extensions;
using AssetHealth.Models.User;
using AssetHealth.Common.Svc;
using AssetHealth.Common.DTO;
using AssetHealth.Models.Configuration;
using AssetHealth.Models.Synchronization;
using AssetHealth.Models.Synchronization_detail;
using AssetHealth.Models.User;
using AssetHealth.Models.Eq_channel;
using AssetHealth.Models.Eq_threshold;
using AssetHealth.Models.Eq_warning;
using AssetHealth.Models.Eq_location;
using AssetHealth.Models.Equipment;
using AssetHealth.Models.Form_answer;
using AssetHealth.Models.Form;
using AssetHealth.Models.Form_question;
using AssetHealth.Models.Form_question_option;
using AssetHealth.Models.Device;
using Ares.Sync.DTO;
using Ares.Sync.Svc;
using Ares.Core.Framework;
using Ares.Sync.DTO.Enums;
using Newtonsoft.Json;
using Ares.Sync.Common.DTO.Enums;
using Ares.Web.JSON;
using AssetHealth.Common.DTO.Extensions;
using System.Text;
using AssetHealth.Common.DAL;
using AssetHealth.Models.Synchronization_device;

namespace AssetHealth.Contollers
{
	
    public partial class SynchronizationController : BaseAresController
	{
		#region Fields

        private readonly ISynchronizationSvc _iSynchronizationsvc;
        
        private readonly ISynchronizationDetailSvc _iSynchronizationDetailsvc;
        private readonly ISynchronizationDeviceSvc _iSynchronizationDevicesvc;
        private readonly IUserSvc _iUsersvc;
        private readonly IEqChannelSvc _iEqChannelsvc;
        private readonly IEqThresholdSvc _iEqThresholdsvc;
        private readonly IEqWarningSvc _iEqWarningsvc;
        private readonly IEqLocationSvc _iEqLocationsvc;
        private readonly IEquipmentSvc _iEquipmentsvc;
        private readonly IFormAnswerSvc _iFormAnswersvc;
        private readonly IFormSvc _iFormsvc;
        private readonly IFormQuestionSvc _iFormQuestionsvc;
        private readonly IFormQuestionOptionSvc _iFormQuestionOptionsvc;
        private readonly IParameterGroupSvc _iParameterGroupsvc;
        private readonly IDeviceSvc _iDevicesvc;
        
        private readonly IFilesSvc _iFilesSvc;

        private readonly IStateProvinceSvc _stateProvinceSvc;
        private readonly ISettingSvc _settingSvc;
        private readonly FileSettings _fileSettings;
        
        private readonly IGenericAttributeSvc _genericAttributeSvc;
        private readonly ILocalizationSvc _localizationSvc;
        private readonly IPictureSvc _pictureSvc;
        private readonly IUserSvc _userSvc;
        private readonly IRoleSvc _roleSvc;
        private readonly IPermissionSvc _permissionSvc;
        private readonly IMenuSvc _menuSvc;
        private readonly IWorkContext _workContext;
        private readonly IUserRegistrationSvc _userRegistrationSvc;
        private readonly IAuthenticationSvc _authenticationSvc;
        private readonly IWorkflowMessageSvc _workflowMessageSvc;
        private readonly INewsLetterSubscriptionSvc _newsLetterSubscriptionSvc;
        private readonly IWebHelper _webHelper;
        private readonly UserSettings _userSettings;
        private readonly LocalizationSettings _localizationSettings;
        private readonly MediaSettings _mediaSettings;

	    #endregion

		#region Constructors

        public SynchronizationController(ISynchronizationSvc Synchronizationsvc,
        ISynchronizationDetailSvc SynchronizationDetailsvc,
        IUserSvc Usersvc,
        IFilesSvc iFilesSvc,
        IEqChannelSvc EqChannelsvc,
        IEqThresholdSvc EqThresholdsvc,
        IEqWarningSvc EqWarningsvc,
        IEqLocationSvc EqLocationsvc,
        IEquipmentSvc Equipmentsvc,
        IFormAnswerSvc FormAnswersvc,
        IFormSvc Formsvc,
        IFormQuestionSvc FormQuestionsvc,
        IFormQuestionOptionSvc FormQuestionOptionsvc,
        IParameterGroupSvc ParameterGroupsvc,
        IDeviceSvc Devicesvc,
        ISynchronizationDeviceSvc iSynchronizationDeviceSvc,
            IStateProvinceSvc stateProvinceSvc, IGenericAttributeSvc genericAttributeSvc, ILocalizationSvc localizationSvc,  
            UserSettings userSettings, IPictureSvc pictureSvc, ISettingSvc settingSvc, FileSettings fileSettings,
            IUserSvc userSvc, IRoleSvc roleSvc, IPermissionSvc permissionSvc, IMenuSvc menuSvc, IWorkContext workContext, IUserRegistrationSvc userRegistrationSvc, IAuthenticationSvc authenticationSvc, IWorkflowMessageSvc workflowMessageSvc,
            INewsLetterSubscriptionSvc newsLetterSubscriptionSvc, IWebHelper webHelper, LocalizationSettings localizationSettings, MediaSettings mediaSettings)
		{
            this._iSynchronizationsvc = Synchronizationsvc;
            
            this._iSynchronizationDetailsvc = SynchronizationDetailsvc;
            this._iSynchronizationDevicesvc = iSynchronizationDeviceSvc;
            this._iUsersvc = Usersvc;
            this._iFilesSvc = iFilesSvc;
            this._iEqChannelsvc = EqChannelsvc;
            this._iEqThresholdsvc = EqThresholdsvc;
            this._iEqWarningsvc = EqWarningsvc;
            this._iEqLocationsvc = EqLocationsvc;
            this._iEquipmentsvc = Equipmentsvc;
            this._iFormAnswersvc = FormAnswersvc;
            this._iFormsvc = Formsvc;
            this._iFormQuestionsvc = FormQuestionsvc;
            this._iFormQuestionOptionsvc = FormQuestionOptionsvc;
            this._iParameterGroupsvc = ParameterGroupsvc;
            this._iDevicesvc = Devicesvc;
            this._settingSvc = settingSvc;
            this._fileSettings = fileSettings;
            
            this._stateProvinceSvc = stateProvinceSvc;
            this._genericAttributeSvc = genericAttributeSvc;
            this._pictureSvc = pictureSvc;
            this._localizationSvc = localizationSvc;
            this._userSettings = userSettings;
            this._userSvc = userSvc;
            this._roleSvc = roleSvc;
            this._permissionSvc = permissionSvc;
            this._menuSvc = menuSvc;
            this._workContext = workContext;
            this._userRegistrationSvc = userRegistrationSvc;
            this._authenticationSvc = authenticationSvc;
            this._workflowMessageSvc = workflowMessageSvc;
            this._newsLetterSubscriptionSvc = newsLetterSubscriptionSvc;
            this._webHelper = webHelper;
            this._localizationSettings = localizationSettings;
            this._mediaSettings = mediaSettings;
		}

		#endregion 

        #region Utilities
        
        
        #endregion

        #region Synchronization methods

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        [AresAuthorize(ResourceKey = "Admin.Synchronization.Read")]
        public ActionResult List()
        {
            SynchronizationModel model = new SynchronizationModel();
            //model. = _roleSvc.GetAll().ToList().Select(x => x.ToModel()).ToList();
            FillDropDowns(model);
            
            
            FillDropDownSynchronizationDetail(model.Filter_SynchronizationDetailModel);
                       
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.Synchronization.Read")]
        public ActionResult ListSynchronizationDatatable(Ares.Web.Razor.Extensions.Datatable.Model.DTParameterModel model)
        {
            var models = _iSynchronizationsvc.GetAll();
            
            if (model.Search != null && !string.IsNullOrEmpty(model.Search.Value))
            {
                models = models.Where(m => m.Udid.Contains(model.Search.Value));
            }
            
            if(model.IsSearchValueExist("Device_id_Text"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Device_id_Text"));
                models = models.Where(m => m.Device_id == tmpValue);
            }
            if(model.IsSearchValueExist("Udid"))
            {
                string tmpValue = model.GetSearchValue("Udid");
                models = models.Where(m => m.Udid.Contains(tmpValue));
            }
            if(model.IsSearchValueExist("Username"))
            {
                string tmpValue = model.GetSearchValue("Username");
                models = models.Where(m => m.Username.Contains(tmpValue));
            }
            if(model.IsSearchValueExist("EnumSync_type_Text"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("EnumSync_type_Text"));
                models = models.Where(m => (int)m.Sync_type == tmpValue);
            }
            if(model.IsSearchValueExist("EnumStatus_Text"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("EnumStatus_Text"));
                models = models.Where(m => (int)m.Status == tmpValue);
            }
            if(model.IsSearchValueExist("Start_datetime"))
            {
                
                
            }
            if(model.IsSearchValueExist("Finish_datetime"))
            {
                
                
            }
            if(model.IsSearchValueExist("Client_version"))
            {
                string tmpValue = model.GetSearchValue("Client_version");
                models = models.Where(m => m.Client_version.Contains(tmpValue));
            }
            if(model.IsSearchValueExist("Server_version"))
            {
                string tmpValue = model.GetSearchValue("Server_version");
                models = models.Where(m => m.Server_version.Contains(tmpValue));
            }
            if(model.IsSearchValueExist("Description"))
            {
                string tmpValue = model.GetSearchValue("Description");
                models = models.Where(m => m.Description.Contains(tmpValue));
            }
            
            
            DTOrder order = null;
            if (model.Order != null && model.Order.Count() > 0)
            {
                order = model.Order.First();
                var pi = typeof(Synchronization).GetProperty(model.Columns.ToList()[order.Column].Data);
                if(order.Dir == "asc")
                    models = models.OrderBy(x => pi.GetValue(x, null));
                else
                    models = models.OrderByDescending(x => pi.GetValue(x, null));
                
            }

            var totalRecords = models.Count();
            
            models = models.OrderBy(e => e.Id).Skip(model.Length * ((int)(Math.Ceiling((double)model.Start / model.Length)) + 1 - 1)).Take(model.Length);

            JsonResult jsonRes = Json(new
            {
                sEcho = model.Draw,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                //aaData = result
                aaData = models.ToList().Select(x => { var m = x.ToModel(); m.Device_id_Text = x.SYNCHRONIZATION_DEVICE == null ? "" : x.SYNCHRONIZATION_DEVICE.Device_name;  return m;})
            }, JsonRequestBehavior.AllowGet);
            return jsonRes;
        }
        
        [AresAuthorize(ResourceKey = "Admin.Synchronization.Create")]
        public ActionResult Create()
        {
            SynchronizationModel model = new SynchronizationModel();
            FillDropDowns(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.Synchronization.Create")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Create([Bind(Exclude = "Filter_SynchronizationDetailModel")] SynchronizationModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {                
                var record = model.ToEntity();
                
            
                record = _iSynchronizationsvc.Insert(record);

                SuccessNotification(_localizationSvc.GetResource("Notifications.Synchronization.Created"));
                
                if (continueEditing)
                    return RedirectToAction("Edit", new { Id = record.Id });

                return RedirectToAction("List");
            }
            else
            {
                FillDropDowns(model);
                return View(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.Synchronization.Update")]
        public ActionResult Edit(int id, string SelectedTab)
        {
            Synchronization record = _iSynchronizationsvc.GetById(id);
            var model = record.ToModel();
            model.SelectedTab = SelectedTab;
                        
            FillDropDowns(model);
            
            FillDropDownSynchronizationDetail(model.Filter_SynchronizationDetailModel);
                       
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.Synchronization.Update")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Edit([Bind(Exclude = "Filter_SynchronizationDetailModel")] SynchronizationModel model, bool continueEditing)
        {
            var item = _iSynchronizationsvc.GetById(model.Id);
            Synchronization originalItem = (Synchronization)item.Clone();
            if (item == null)
                //No country found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                item = model.ToEntity(item);

                item.Object_id = originalItem.Object_id;
                item.Insert_user_id = originalItem.Insert_user_id;
                item.Insert_datetime = originalItem.Insert_datetime;

                _iSynchronizationsvc.Update(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.Synchronization.Updated"));
                
                if(continueEditing)
                    return RedirectToAction("Edit", new { Id = item.Id });

                return RedirectToAction("List");
            }
            else
            {
                FillDropDowns(model);
            
            FillDropDownSynchronizationDetail(model.Filter_SynchronizationDetailModel);
                       
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.Synchronization.Delete")]
        public ActionResult Delete(int id)
        {

            var item = _iSynchronizationsvc.GetById(id);
            if (item == null)
                return RedirectToAction("List");

            try
            {
                item.SYNCHRONIZATIONSynchronization_detail.ToList().ForEach(d => _iSynchronizationDetailsvc.Delete(d));
                
                _iSynchronizationsvc.Delete(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.Synchronization.Deleted"));
                return RedirectToAction("List");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("Edit", new { id = item.Id });
            }
        }
        
        
        public ActionResult CreateOrUpdate(string itemId, string imageUrl, string mimeType, string uploadFilename)
        {
            SynchronizationModel response = null;
            

            int nItemId = -1;
            int.TryParse(itemId, out nItemId);

            if (nItemId != -1)
            {
                Synchronization record = _iSynchronizationsvc.GetById(nItemId);
                response = record.ToModel();
                if (record != null) // update
                {

                   

                }
                else // insert mode (after image upload popup close)
                {
                    response = new SynchronizationModel();
                    //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() { Text = "" } };
                }
            }
            else // insert
            {
                response = new SynchronizationModel();
                //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() {Text = "" }  };
                
            }

 
            ViewBag.itemId = nItemId;

            return View(response);
        }
        
        
        public void FillDropDowns(SynchronizationModel model)
        {
            

            model.Device_id_Device_SelectList = new SelectList(_iDevicesvc.GetAll().ToList(), "Id", "Device_name");
            model.Filter_SynchronizationDeviceModel.Device_id_Device_SelectList = new SelectList(_iDevicesvc.GetAll().ToList(), "Id", "Device_name");


        }
        
        #endregion
                         



            
        #region  SynchronizationDetail methods
        
        public void FillDropDownSynchronizationDetail(SynchronizationDetailModel model)
        {

            model.Synchronization_id_Synchronization_SelectList = new SelectList(_iSynchronizationsvc.GetAll().ToList(), "Id", "Udid");
        }
        
        public ActionResult CreateOrUpdateSynchronizationDetail(string itemId, string imageUrl, string mimeType, string uploadFilename)
        {
            SynchronizationDetailModel response = null;


            int nItemId = -1;
            int.TryParse(itemId, out nItemId);

            if (nItemId != -1)
            {
                SynchronizationDetail record = _iSynchronizationDetailsvc.GetById(nItemId);
                response = record.ToModel();
                if (record != null) // update
                {



                    //response.RoleListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem { Text = "Administrators" }, new Ext.Net.ListItem { Text = "Guests" } };
                    //response.PermissionListItems = permListItems.ToArray();

                }
                else // insert mode (after image upload popup close)
                {
                    response = new SynchronizationDetailModel();
                    //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() { Text = "" } };
                }
            }
            else // insert
            {
                response = new SynchronizationDetailModel();
                //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() {Text = "" }  };

            }


            ViewBag.itemId = nItemId;

            return View(response);
        }
        
        [AresAuthorize(ResourceKey = "Admin.SynchronizationDetail.Read")]
        public ActionResult ListSynchronizationDetailDatatable(Ares.Web.Razor.Extensions.Datatable.Model.DTParameterModel model, int? SynchronizationModelId)
        {
            var models = _iSynchronizationDetailsvc.GetAll();
            models = models.Where(m => m.Synchronization_id == SynchronizationModelId);
            if (model.Search != null && !string.IsNullOrEmpty(model.Search.Value))
            {
                models = models.Where(m => m.Table_name.Contains(model.Search.Value));
            }
            
            if (model.IsSearchValueExist("Synchronization_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Synchronization_id"));
                models = models.Where(m => m.Synchronization_id == tmpValue);
            }
            if (model.IsSearchValueExist("Table_name"))
            {
                string tmpValue = model.GetSearchValue("Table_name");
                models = models.Where(m => m.Table_name.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Server_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Server_id"));
                models = models.Where(m => m.Server_id == tmpValue);
            }
            if (model.IsSearchValueExist("Client_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Client_id"));
                models = models.Where(m => m.Client_id == tmpValue);
            }
            if (model.IsSearchValueExist("EnumStatus_Text"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("EnumStatus_Text"));
                models = models.Where(m => (int)m.Status == tmpValue);
            }

            if (model.IsSearchValueExist("Transaction_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Transaction_id"));
                models = models.Where(m => m.Transaction_id == tmpValue);
            }
            if (model.IsSearchValueExist("Operation_type"))
            {
                
            }
            if (model.IsSearchValueExist("Result"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Result"));
                models = models.Where(m => m.Result == tmpValue);
            }
            if (model.IsSearchValueExist("Message"))
            {
            }
            if (model.IsSearchValueExist("Item"))
            {
            }
            if (model.IsSearchValueExist("Order"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Order"));
                models = models.Where(m => m.Order == tmpValue);
            }
            if (model.IsSearchValueExist("Start_datetime"))
            {
                
                
            }
            if (model.IsSearchValueExist("Description"))
            {
                string tmpValue = model.GetSearchValue("Description");
                models = models.Where(m => m.Description.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Active"))
            {
                bool tmpValue = bool.Parse(model.GetSearchValue("Active"));
                models = models.Where(m => m.Active == tmpValue);
            }
            
            DTOrder order = null;
            if (model.Order != null && model.Order.Count() > 0)
            {
                order = model.Order.First();
                var pi = typeof(SynchronizationDetail).GetProperty(model.Columns.ToList()[order.Column].Data);
                if(order.Dir == "asc")
                    models = models.OrderBy(x => pi.GetValue(x, null));
                else
                    models = models.OrderByDescending(x => pi.GetValue(x, null));
                
            }

            var totalRecords = models.Count();
            
            models = models.OrderBy(e => e.Id).Skip(model.Length * ((int)(Math.Ceiling((double)model.Start / model.Length)) + 1 - 1)).Take(model.Length);

            JsonResult jsonRes = Json(new
            {
                sEcho = model.Draw,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                //aaData = result
                aaData = models.ToList().Select(x => { var m = x.ToModel(); m.Synchronization_id_Text = x.SYNCHRONIZATION_DETAIL == null ? "" : x.SYNCHRONIZATION_DETAIL.Udid;  return m;})
            }, JsonRequestBehavior.AllowGet);
            return jsonRes;
        }
        
        [AresAuthorize(ResourceKey = "Admin.SynchronizationDetail.Create")]
        public ActionResult CreateSynchronizationDetail(int SynchronizationModelId)
        {
            SynchronizationDetailModel model = new SynchronizationDetailModel();
            model.Synchronization_id = SynchronizationModelId;
            
            
            FillDropDownSynchronizationDetail(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.SynchronizationDetail.Create")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult CreateSynchronizationDetail( SynchronizationDetailModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {                
                var record = model.ToEntity();
                
                
                record = _iSynchronizationDetailsvc.Insert(record);

                SuccessNotification(_localizationSvc.GetResource("Notifications.SynchronizationDetail.Created"));
                
                if (continueEditing)
                    return RedirectToAction("EditSynchronizationDetail", new { id = record.Id });

                return RedirectToAction("Edit", new  { id = model.Synchronization_id, SelectedTab = "2" });
            }
            else
            {
                FillDropDownSynchronizationDetail(model);
                return View(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }
        
        public ActionResult EditSynchronizationDetail(int id, string SelectedTab)
        {
            SynchronizationDetail record = _iSynchronizationDetailsvc.GetById(id);
            var model = record.ToModel();
            model.SelectedTab = SelectedTab;
            
            
            
            FillDropDownSynchronizationDetail(model);

            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.SynchronizationDetail.Update")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult EditSynchronizationDetail( SynchronizationDetailModel model, bool continueEditing)
        {
            var item = _iSynchronizationDetailsvc.GetById(model.Id);
            SynchronizationDetail originalItem = (SynchronizationDetail)item.Clone();
            if (item == null)
                //No country found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                item = model.ToEntity(item);

                item.Object_id = originalItem.Object_id;
                item.Insert_user_id = originalItem.Insert_user_id;
                item.Insert_datetime = originalItem.Insert_datetime;

                _iSynchronizationDetailsvc.Update(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.SynchronizationDetail.Updated"));
                
                if(continueEditing)
                    return RedirectToAction("EditSynchronizationDetail", new { id = item.Id });
                
                return RedirectToAction("Edit", new  { id = model.Synchronization_id, SelectedTab = "2" });
            }

            //If we got this far, something failed, redisplay form
            FillDropDownSynchronizationDetail(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.SynchronizationDetail.Delete")]
        public ActionResult DeleteSynchronizationDetail(int id)
        {

            var item = _iSynchronizationDetailsvc.GetById(id);
            if (item == null)
                return RedirectToAction("List");

            try
            {
                
                _iSynchronizationDetailsvc.Delete(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.SynchronizationDetail.Deleted"));
                
                return RedirectToAction("Edit", new  { id = item.Synchronization_id, SelectedTab = "2" });
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("EditSynchronizationDetail", new { id = item.Id });
            }
        }
        #endregion



        #region  SynchronizationDevice methods

        public void FillDropDownSynchronizationDevice(SynchronizationDeviceModel model)
        {

            model.Synchronization_id_Synchronization_SelectList = new SelectList(_iSynchronizationsvc.GetAll().ToList(), "Id", "Id");
            model.Device_id_Device_SelectList = new SelectList(_iDevicesvc.GetAll().ToList(), "Id", "Device_name");
        }

        public ActionResult CreateOrUpdateSynchronizationDevice(string itemId, string imageUrl, string mimeType, string uploadFilename)
        {
            SynchronizationDeviceModel response = null;


            int nItemId = -1;
            int.TryParse(itemId, out nItemId);

            if (nItemId != -1)
            {
                SynchronizationDevice record = _iSynchronizationDevicesvc.GetById(nItemId);
                response = record.ToModel();
                if (record != null) // update
                {



                    //response.RoleListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem { Text = "Administrators" }, new Ext.Net.ListItem { Text = "Guests" } };
                    //response.PermissionListItems = permListItems.ToArray();

                }
                else // insert mode (after image upload popup close)
                {
                    response = new SynchronizationDeviceModel();
                    //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() { Text = "" } };
                }
            }
            else // insert
            {
                response = new SynchronizationDeviceModel();
                //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() {Text = "" }  };

            }


            ViewBag.itemId = nItemId;

            return View(response);
        }

        [AresAuthorize(ResourceKey = "Admin.SynchronizationDevice.Read")]
        public ActionResult ListSynchronizationDeviceDatatable(Ares.Web.Razor.Extensions.Datatable.Model.DTParameterModel model, int? SynchronizationModelId)
        {
            var models = _iSynchronizationDevicesvc.GetAll();
            models = models.Where(m => m.Synchronization_id == SynchronizationModelId);
            if (model.Search != null && !string.IsNullOrEmpty(model.Search.Value))
            {
                models = models.Where(m => m.Udid.Contains(model.Search.Value));
            }

            
            if (model.IsSearchValueExist("Synchronization_id_Text"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Synchronization_id_Text"));
                models = models.Where(m => m.Synchronization_id == tmpValue);
            }
            if (model.IsSearchValueExist("Device_id_Text"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Device_id_Text"));
                models = models.Where(m => m.Device_id == tmpValue);
            }
            if (model.IsSearchValueExist("Udid"))
            {
                string tmpValue = model.GetSearchValue("Udid");
                models = models.Where(m => m.Udid.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Status"))
            {

            }
            if (model.IsSearchValueExist("Start_datetime"))
            {


            }
            if (model.IsSearchValueExist("Finish_datetime"))
            {


            }
            if (model.IsSearchValueExist("Description"))
            {
                string tmpValue = model.GetSearchValue("Description");
                models = models.Where(m => m.Description.Contains(tmpValue));
            }

            DTOrder order = null;
            if (model.Order != null && model.Order.Count() > 0)
            {
                order = model.Order.First();
                var pi = typeof(SynchronizationDevice).GetProperty(model.Columns.ToList()[order.Column].Data);
                if (order.Dir == "asc")
                    models = models.OrderBy(x => pi.GetValue(x, null));
                else
                    models = models.OrderByDescending(x => pi.GetValue(x, null));

            }

            var totalRecords = models.Count();

            models = models.OrderBy(e => e.Id).Skip(model.Length * ((int)(Math.Ceiling((double)model.Start / model.Length)) + 1 - 1)).Take(model.Length);

            JsonResult jsonRes = Json(new
            {
                sEcho = model.Draw,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                //aaData = result
                aaData = models.ToList().Select(x => { var m = x.ToModel(); m.Synchronization_id_Text = x.SYNCHRONIZATION_DEVICE_SYNCHRONIZATION == null ? "" : x.SYNCHRONIZATION_DEVICE_SYNCHRONIZATION.Id + ""; return m; })
            }, JsonRequestBehavior.AllowGet);
            return jsonRes;
        }

        [AresAuthorize(ResourceKey = "Admin.SynchronizationDevice.Create")]
        public ActionResult CreateSynchronizationDevice(int SynchronizationModelId)
        {
            SynchronizationDeviceModel model = new SynchronizationDeviceModel();
            model.Synchronization_id = SynchronizationModelId;


            FillDropDownSynchronizationDevice(model);

            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.SynchronizationDevice.Create")]
        [System.Web.Mvc.HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult CreateSynchronizationDevice(SynchronizationDeviceModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {
                var record = model.ToEntity();



                record = _iSynchronizationDevicesvc.Insert(record);

                SuccessNotification(_localizationSvc.GetResource("Notifications.SynchronizationDevice.Created"));

                if (continueEditing)
                    return RedirectToAction("EditSynchronizationDevice", new { id = record.Id });

                return RedirectToAction("Edit", new { id = model.Synchronization_id, SelectedTab = "3" });
            }
            else
            {
                FillDropDownSynchronizationDevice(model);
                return View(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.SynchronizationDevice.Update")]
        public ActionResult EditSynchronizationDevice(int id, string SelectedTab)
        {
            SynchronizationDevice record = _iSynchronizationDevicesvc.GetById(id);
            var model = record.ToModel();
            model.SelectedTab = SelectedTab;



            FillDropDownSynchronizationDevice(model);

            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.SynchronizationDevice.Update")]
        [System.Web.Mvc.HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult EditSynchronizationDevice(SynchronizationDeviceModel model, bool continueEditing)
        {
            var item = _iSynchronizationDevicesvc.GetById(model.Id);
            SynchronizationDevice originalItem = (SynchronizationDevice)item.Clone();
            if (item == null)
                //No country found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                item = model.ToEntity(item);

                

                _iSynchronizationDevicesvc.Update(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.SynchronizationDevice.Updated"));

                if (continueEditing)
                    return RedirectToAction("EditSynchronizationDevice", new { id = item.Id });

                return RedirectToAction("Edit", new { id = model.Synchronization_id, SelectedTab = "3" });
            }

            //If we got this far, something failed, redisplay form
            FillDropDownSynchronizationDevice(model);

            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.SynchronizationDevice.Delete")]
        public ActionResult DeleteSynchronizationDevice(int id)
        {

            var item = _iSynchronizationDevicesvc.GetById(id);
            if (item == null)
                return RedirectToAction("List");

            try
            {

                _iSynchronizationDevicesvc.Delete(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.SynchronizationDevice.Deleted"));

                return RedirectToAction("Edit", new { id = item.Synchronization_id, SelectedTab = "3" });
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("EditSynchronizationDevice", new { id = item.Id });
            }
        }
        #endregion



        public ActionResult SyncPopup(int id)
        {
            PrepareSynchronizationModel model = new PrepareSynchronizationModel();
            model.CustomProperties.Add("id", id);
            model.Id = id;

            model.Device_id_Device_SelectList = new SelectList(_iDevicesvc.GetAll().Where(d => 1 == 1).ToList().Select(u => new { Id = u.Id, Title = u.Device_name}), "Id", "Title");
            model.DeviceList = new List<string>().ToArray();

            model.Location_id_Location_SelectList = new SelectList(_iEqLocationsvc.GetAll().ToList().Select(u => new { Id = u.Id, Title = u.Location_name }), "Id", "Title");
            model.LocationList = new List<string>().ToArray();

            IRoleSvc roleSvc = FrameworkCtx.Current.Resolve<IRoleSvc>();

            model.Role_id_Role_SelectList = new SelectList(roleSvc.GetAll().ToList().Select(u => new { Id = u.Id, Title = u.Rolename }), "Id", "Title");
            model.RoleList = new List<string>().ToArray();


            var models = _iFormsvc.GetAll();

            if (!_workContext.CurrentUser.IsAdmin())
            {
                List<int> relatedLocationIds = _workContext.CurrentUser.RelatedLocations().Select(r => r.Id).ToList();
                models = models.Where(e => relatedLocationIds.Contains(e.EQUIPMENT.Location.Id));
            }

            model.Form_id_Form_SelectList = new SelectList(models.ToList().Select(u => new { Id = u.Id, Title = u.Title }), "Id", "Title");
            model.FormList = new List<string>().ToArray();

            model.FormQuestion_id_FormQuestion_SelectList = new SelectList(Enumerable.Empty<SelectListItem>());
            model.FormQuestionList = new List<string>().ToArray();

            model.FormQuestion_Version_SelectList = new SelectList(Enumerable.Empty<SelectListItem>());
            model.FormQuestion_VersionList = new List<string>().ToArray();



            var files = Directory.EnumerateFiles(HttpContext.Server.MapPath(_fileSettings.DefaultUploadDir), "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith("_update.sql") || s.EndsWith("_preferences.json") || s.EndsWith(".cmd"));
            if (files != null && files.Count() > 0)
                model.Files_SelectList = new SelectList(files.Select(f => f.Substring(f.LastIndexOf("\\") + 1)).Select(f => new { Id = f, Text = f }), "Id", "Text");
            else
                model.Files_SelectList = new SelectList(Enumerable.Empty<SelectListItem>());

            model.FileList = new List<string>().ToArray();

            return View(model);
        }


        public ActionResult PrepareServerChangeChangeSet(PrepareSynchronizationModel model)
        {
            ISynchronizationSvc iSynchronizationSvc = FrameworkCtx.Current.Resolve<ISynchronizationSvc>();
            IFormSvc formSvc = FrameworkCtx.Current.Resolve<IFormSvc>();
            IDeviceSvc deviceSvc = FrameworkCtx.Current.Resolve<IDeviceSvc>();

            Synchronization sync = new Synchronization();
            sync.Object_id = Guid.NewGuid().ToString();
            sync.Sync_type = EnumSynchronizationSync_type.From_Server;
            sync.Status = EnumSynchronizationStatus.Started;
            sync.Start_datetime = DateTime.Now;

            sync.SYNCHRONIZATIONSynchronization_device = new List<SynchronizationDevice>();
            sync.SYNCHRONIZATIONSynchronization_device = FillOneToManyValues<SynchronizationDevice, Device>(sync.SYNCHRONIZATIONSynchronization_device, model.DeviceList, _iDevicesvc, c => c.Device_id);
            if (sync.SYNCHRONIZATIONSynchronization_device != null && sync.SYNCHRONIZATIONSynchronization_device.Count > 0)
                sync.SYNCHRONIZATIONSynchronization_device.ToList().ForEach(sd => sd.Status = EnumSynchronizationDeviceStatus.None);


            List<SynchronizationDetail> details = new List<SynchronizationDetail>();

            if (model.FileList != null && model.FileList.Length > 0)
            {
                sync.Status = EnumSynchronizationStatus.Resume;
                foreach (string fileName in model.FileList)
                {
                    Files tmpFile = new Files();
                    tmpFile.File_reference = fileName.Trim();
                    details.Add(new SynchronizationDetail()
                    {
                        Object_id = Guid.NewGuid().ToString(),
                        Table_name = "files",
                        Server_id = null,
                        Status = EnumSynchronizationDetailStatus.Started,
                        Operation_type = EnumSynchronizationDetailOperation_type.Created,
                        Active = true,
                        Item = JsonConvert.SerializeObject(tmpFile, new AresJsonSerializerSettings())
                    });
                }
                

            }
            else
            {
                foreach (string formId in model.FormList)
                {
                    //Form form = formSvc.GetById(model.Id);
                    Form form = formSvc.GetById(int.Parse(formId));

                    details.Add(new SynchronizationDetail()
                    {
                        Object_id = Guid.NewGuid().ToString(),
                        Table_name = "form",
                        Server_id = form.Id,
                        Status = EnumSynchronizationDetailStatus.Started,
                        Operation_type = EnumSynchronizationDetailOperation_type.Created,
                        Active = true,
                        Item = JsonConvert.SerializeObject(form, new AresJsonSerializerSettings())
                    });

                    foreach (FormQuestion question in form.FORMForm_question)
                    {
                        details.Add(new SynchronizationDetail()
                        {
                            Object_id = Guid.NewGuid().ToString(),
                            Table_name = "form_question",
                            Server_id = question.Id,
                            Status = EnumSynchronizationDetailStatus.Started,
                            Operation_type = EnumSynchronizationDetailOperation_type.Created,
                            Active = true,
                            Item = JsonConvert.SerializeObject(question, new AresJsonSerializerSettings())
                        });
                    }


                    foreach (FormQuestion question in form.FORMForm_question)
                    {
                        List<Files> files = _iFilesSvc.GetAll().Where(f => f.Entity_name == "Formquestion" && f.Entity_id == question.Id && f.Entity_property == "Files" && f.Deleted == 0).ToList();
                        if (files != null && files.Count > 0)
                        {
                            foreach (Files tmpFile in files)
                            {
                                details.Add(new SynchronizationDetail()
                                {
                                    Object_id = Guid.NewGuid().ToString(),
                                    Table_name = "files",
                                    Server_id = tmpFile.Id,
                                    Status = EnumSynchronizationDetailStatus.Started,
                                    Operation_type = EnumSynchronizationDetailOperation_type.Created,
                                    Active = true,
                                    Item = JsonConvert.SerializeObject(tmpFile, new AresJsonSerializerSettings())
                                });
                            }

                        }

                    }

                }

            }

            

            sync.SYNCHRONIZATIONSynchronization_detail = details;
            sync = iSynchronizationSvc.Insert(sync);

            return Redirect(Request.UrlReferrer.ToString());


        }

        public ActionResult PrepareServerChangeChangeSetYeni(PrepareSynchronizationModel model)
        { 
            ISynchronizationSvc iSynchronizationSvc = FrameworkCtx.Current.Resolve<ISynchronizationSvc>();
            IFormSvc formSvc = FrameworkCtx.Current.Resolve<IFormSvc>();
            IFormQuestionSvc iFormQuestionSvc = FrameworkCtx.Current.Resolve<IFormQuestionSvc>();
            IDeviceSvc deviceSvc = FrameworkCtx.Current.Resolve<IDeviceSvc>();
            

            List<Device> devices = deviceSvc.GetAll().Where(d => model.DeviceList.Contains(d.Id + "")).ToList();

            List<Ares.Sync.Models.Synchronization.DeviceSynchronizationListItemModel> items = iSynchronizationSvc.IsNewSynchronizationNeeded(devices);

            foreach (IGrouping<Synchronization, Ares.Sync.Models.Synchronization.DeviceSynchronizationListItemModel> groupedItem in items.GroupBy(d => d.Synchronization))
            {
                foreach (Ares.Sync.Models.Synchronization.DeviceSynchronizationListItemModel item in groupedItem)
                {
                    Synchronization sync = item.Synchronization;
                    if (item.IsNew) // new Sync
                    {
                        /*
                        sync.SYNCHRONIZATIONSynchronization_device = FillOneToManyValues<SynchronizationDevice, Device>(sync.SYNCHRONIZATIONSynchronization_device, model.DeviceList, _iDevicesvc, c => c.Device_id);
                        if (sync.SYNCHRONIZATIONSynchronization_device != null && sync.SYNCHRONIZATIONSynchronization_device.Count > 0)
                            sync.SYNCHRONIZATIONSynchronization_device.ToList().ForEach(sd => sd.Status = EnumSynchronizationDeviceStatus.None);
                        */
                        sync.SYNCHRONIZATIONSynchronization_device = new List<SynchronizationDevice>();
                        //items.Where(i => i.IsNew).ToList().ForEach(i => { sync.SYNCHRONIZATIONSynchronization_device.Add(new SynchronizationDevice() { Device_id = i.DeviceId, Status = EnumSynchronizationDeviceStatus.None }); });
                        sync.SYNCHRONIZATIONSynchronization_device.Add(new SynchronizationDevice() { Device_id = item.DeviceId, Status = EnumSynchronizationDeviceStatus.None });


                    }

                    SynchronizationDetail tmpDetail = _iSynchronizationDetailsvc.GetAll().FirstOrDefault(d => d.SYNCHRONIZATION_DETAILSynchronization_log.Any(sl => sl.Device_id == item.DeviceId ));
                    bool deviceNew = tmpDetail == null;


                    if (model.FileList != null && model.FileList.Length > 0)
                    {
                        sync.Status = EnumSynchronizationStatus.Resume;

                        foreach (string fileName in model.FileList)
                        {
                            Files tmpFile = new Files();
                            tmpFile.File_reference = fileName.Trim();

                            /*
                            using (StreamReader sr = System.IO.File.OpenText(HttpContext.Server.MapPath(_fileSettings.DefaultUploadDir) + "/" + fileName))
                            {
                                string s = sr.ReadToEnd();
                                tmpFile.Notes = s; // File_data json olarak byte arrat serialize yapılmadığından notes'a yazıyoruz
                            }
                            */
                            string content = System.IO.File.ReadAllText(HttpContext.Server.MapPath(_fileSettings.DefaultUploadDir) + "/" + fileName, Encoding.Default);
                            tmpFile.Notes = content;

                            iSynchronizationSvc.addSynchronizationDetail(ref sync, item.IsNew, item.DeviceId, tmpFile, "files", EnumSynchronizationDetailOperation_type.Created, deviceNew);
                        }

                        

                    }
                    else
                    {

                        foreach (string formId in model.FormList)
                        {
                            Form form = formSvc.GetById(int.Parse(formId));
                            iSynchronizationSvc.addSynchronizationDetail(ref sync, item.IsNew, item.DeviceId, form, "form", EnumSynchronizationDetailOperation_type.Created, deviceNew);

                            //List<FormQuestion> formQuestions = form.FORMForm_question;
                            //List<FormQuestion> formQuestions = iFormQuestionSvc.GetAll().Where(q => model.FormQuestionList.Contains(q.Id + "")).ToList();



                        }

                        List<FormQuestion> formQuestions = iFormQuestionSvc.SqlQuery<FormQuestion>(string.Format(HardCodedSQL.FORM_QUESTION_GET_IDS_FOR_BATCH_UPDATE, string.Join<string>(", ", model.FormQuestionList) )).ToList();

                        foreach (FormQuestion question in formQuestions)
                        {
                            iSynchronizationSvc.addSynchronizationDetail(ref sync, item.IsNew, item.DeviceId, question, "form_question", EnumSynchronizationDetailOperation_type.Created, deviceNew);
                        }



                        /*
                        foreach (FormQuestion question in formQuestions)
                        {
                            //List<Files> files = _iFilesSvc.GetAll().Where(f => f.Entity_name == "Formquestion" && questionIds.Contains(f.Entity_id) && f.Entity_property == "Files" && f.Deleted == 0).ToList();
                            List<Files> files = _iFilesSvc.GetAll().Where(f => f.Entity_name == "Formquestion" && f.Entity_id == question.Id).ToList();
                            if (files != null && files.Count > 0)
                            {
                                foreach (Files tmpFile in files)
                                {
                                    iSynchronizationSvc.addSynchronizationDetail(ref sync, item.IsNew, item.DeviceId, tmpFile, "files", EnumSynchronizationDetailOperation_type.Created, deviceNew);
                                }
                            }
                        }
                        */
                        //List<Files> files = _iFilesSvc.GetAll().Where(f => f.Entity_name == "Formquestion" && questionIds.Contains(f.Entity_id) && f.Entity_property == "Files" && f.Deleted == 0).ToList();
                        List<Files> files = _iFilesSvc.SqlQuery<Files>(string.Format(HardCodedSQL.SYNCHRONIZATION_GET_FILES_OF_QUESTIONS,  string.Join<string>(", ", model.FormQuestionList) )).ToList();

                        if (files != null && files.Count > 0)
                        {
                            foreach (Files tmpFile in files)
                            {
                                iSynchronizationSvc.addSynchronizationDetail(ref sync, item.IsNew, item.DeviceId, tmpFile, "files", EnumSynchronizationDetailOperation_type.Created, deviceNew);
                            }
                        }
                    }

                    if (item.IsNew) 
                        iSynchronizationSvc.Insert(sync);
                    else
                        iSynchronizationSvc.Update(sync);

                }
            }
            return Redirect(Request.UrlReferrer.ToString());
        }





    }
}

