﻿



using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.IO;
using System.Web.Http;
using System.Net;
using System.Web.Http.ModelBinding;
using System.Web.Security;
using Ext.Net.MVC;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using System.Web.Mvc;
using System.Linq;
using Ares.UMS.Svc.Directory;
using Ares.UMS.DTO.Users;
using Ares.UMS.Svc.Users;
using Ares.Web.Svc.Authentication;
using Ares.Web.Svc.Web;
using Ares.UMS.Svc.Common;
using Ares.UMS.DTO.Messages;
using Ares.UMS.Svc.Messages;
using Ares.Core.Helpers;
using Ares.UMS.DTO.Localization;
using Ares.UMS.Svc.Localization;
using Ares.UMS.DTO.Media;
using Ares.UMS.Svc.Media;
using Ares.Web.Models.Role;
using Ares.UMS.Svc.Security;
using Ares.UMS.DTO.Security;
using Ares.Web.Helpers;
using Ares.Web.Attributes;
using Ares.Web.Controllers;
using Ares.UMS.Svc.Configuration;
using Ares.UMS.DTO.Configuration;
using Ares.UMS.Svc;
using Ares.Web.Razor.Extensions.Datatable.Model;
using AssetHealth.Extensions;
using AssetHealth.Models.User;
using AssetHealth.Common.Svc;
using AssetHealth.Common.DTO;
using AssetHealth.Models.Configuration;
using AssetHealth.Models.Form_answer_round;
using AssetHealth.Models.Form_question;
using AssetHealth.Models.Synchronization_detail;
using AssetHealth.Models.Synchronization_device;
using AssetHealth.Models.Synchronization;
using AssetHealth.Models.Synchronization_log;
using AssetHealth.Models.Eq_channel;
using AssetHealth.Models.Eq_threshold;
using AssetHealth.Models.Eq_warning;
using AssetHealth.Models.Eq_location;
using AssetHealth.Models.Form_question_option;
using AssetHealth.Models.Device;
using AssetHealth.Models.Form_answer;
using Ares.Sync.Svc;

namespace AssetHealth.Contollers
{
	
    public partial class FormAnswerRoundController : BaseAresController
	{
		#region Fields

        private readonly IFormAnswerRoundSvc _iFormAnswerRoundsvc;
        
        private readonly IFormQuestionSvc _iFormQuestionsvc;
        private readonly IParameterItemSvc _iParameterItemsvc;
        private readonly ISynchronizationDetailSvc _iSynchronizationDetailsvc;
        private readonly ISynchronizationDeviceSvc _iSynchronizationDevicesvc;
        private readonly ISynchronizationSvc _iSynchronizationsvc;
        private readonly ISynchronizationLogSvc _iSynchronizationLogsvc;
        private readonly IEqChannelSvc _iEqChannelsvc;
        private readonly IEqThresholdSvc _iEqThresholdsvc;
        private readonly IEqWarningSvc _iEqWarningsvc;
        private readonly IEqLocationSvc _iEqLocationsvc;
        private readonly IFormQuestionOptionSvc _iFormQuestionOptionsvc;
        private readonly IDeviceSvc _iDevicesvc;
        private readonly IFormAnswerSvc _iFormAnswersvc;
        
        private readonly IStateProvinceSvc _stateProvinceSvc;
        private readonly ISettingSvc _settingSvc;
        private readonly FileSettings _fileSettings;
        
        private readonly IGenericAttributeSvc _genericAttributeSvc;
        private readonly ILocalizationSvc _localizationSvc;
        private readonly IPictureSvc _pictureSvc;
        private readonly IUserSvc _userSvc;
        private readonly IRoleSvc _roleSvc;
        private readonly IPermissionSvc _permissionSvc;
        private readonly IMenuSvc _menuSvc;
        private readonly IWorkContext _workContext;
        private readonly IUserRegistrationSvc _userRegistrationSvc;
        private readonly IAuthenticationSvc _authenticationSvc;
        private readonly IWorkflowMessageSvc _workflowMessageSvc;
        private readonly INewsLetterSubscriptionSvc _newsLetterSubscriptionSvc;
        private readonly IWebHelper _webHelper;
        private readonly UserSettings _userSettings;
        private readonly LocalizationSettings _localizationSettings;
        private readonly MediaSettings _mediaSettings;

	    #endregion

		#region Constructors

        public FormAnswerRoundController(IFormAnswerRoundSvc FormAnswerRoundsvc,
        IFormQuestionSvc FormQuestionsvc,
        IParameterItemSvc ParameterItemsvc,
        ISynchronizationDetailSvc SynchronizationDetailsvc,
        ISynchronizationDeviceSvc SynchronizationDevicesvc,
        ISynchronizationSvc Synchronizationsvc,
        ISynchronizationLogSvc SynchronizationLogsvc,
        IEqChannelSvc EqChannelsvc,
        IEqThresholdSvc EqThresholdsvc,
        IEqWarningSvc EqWarningsvc,
        IEqLocationSvc EqLocationsvc,
        IFormQuestionOptionSvc FormQuestionOptionsvc,
        IDeviceSvc Devicesvc,
        IFormAnswerSvc FormAnswersvc,
            IStateProvinceSvc stateProvinceSvc, IGenericAttributeSvc genericAttributeSvc, ILocalizationSvc localizationSvc,  
            UserSettings userSettings, IPictureSvc pictureSvc, ISettingSvc settingSvc, FileSettings fileSettings,
            IUserSvc userSvc, IRoleSvc roleSvc, IPermissionSvc permissionSvc, IMenuSvc menuSvc, IWorkContext workContext, IUserRegistrationSvc userRegistrationSvc, IAuthenticationSvc authenticationSvc, IWorkflowMessageSvc workflowMessageSvc,
            INewsLetterSubscriptionSvc newsLetterSubscriptionSvc, IWebHelper webHelper, LocalizationSettings localizationSettings, MediaSettings mediaSettings)
		{
            this._iFormAnswerRoundsvc = FormAnswerRoundsvc;
            
            this._iFormQuestionsvc = FormQuestionsvc;
            this._iParameterItemsvc = ParameterItemsvc;
            this._iSynchronizationDetailsvc = SynchronizationDetailsvc;
            this._iSynchronizationDevicesvc = SynchronizationDevicesvc;
            this._iSynchronizationsvc = Synchronizationsvc;
            this._iSynchronizationLogsvc = SynchronizationLogsvc;
            this._iEqChannelsvc = EqChannelsvc;
            this._iEqThresholdsvc = EqThresholdsvc;
            this._iEqWarningsvc = EqWarningsvc;
            this._iEqLocationsvc = EqLocationsvc;
            this._iFormQuestionOptionsvc = FormQuestionOptionsvc;
            this._iDevicesvc = Devicesvc;
            this._iFormAnswersvc = FormAnswersvc;
            this._settingSvc = settingSvc;
            this._fileSettings = fileSettings;
            
            this._stateProvinceSvc = stateProvinceSvc;
            this._genericAttributeSvc = genericAttributeSvc;
            this._pictureSvc = pictureSvc;
            this._localizationSvc = localizationSvc;
            this._userSettings = userSettings;
            this._userSvc = userSvc;
            this._roleSvc = roleSvc;
            this._permissionSvc = permissionSvc;
            this._menuSvc = menuSvc;
            this._workContext = workContext;
            this._userRegistrationSvc = userRegistrationSvc;
            this._authenticationSvc = authenticationSvc;
            this._workflowMessageSvc = workflowMessageSvc;
            this._newsLetterSubscriptionSvc = newsLetterSubscriptionSvc;
            this._webHelper = webHelper;
            this._localizationSettings = localizationSettings;
            this._mediaSettings = mediaSettings;
		}

		#endregion 

        #region Utilities
        
        
        #endregion

        #region FormAnswerRound methods

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        [AresAuthorize(ResourceKey = "Admin.FormAnswerRound.Read")]
        public ActionResult List()
        {
            FormAnswerRoundModel model = new FormAnswerRoundModel();
            //model. = _roleSvc.GetAll().ToList().Select(x => x.ToModel()).ToList();
            FillDropDowns(model);
            
            
            FillDropDownFormAnswer(model.Filter_FormAnswerModel);
                       
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormAnswerRound.Read")]
        public ActionResult ListFormAnswerRoundDatatable(Ares.Web.Razor.Extensions.Datatable.Model.DTParameterModel model)
        {
            var models = _iFormAnswerRoundsvc.GetAll();
            
            if (model.Search != null && !string.IsNullOrEmpty(model.Search.Value))
            {
                models = models.Where(m => m.Object_id.Contains(model.Search.Value));
            }
            
            if(model.IsSearchValueExist("Object_id"))
            {
                string tmpValue = model.GetSearchValue("Object_id");
                models = models.Where(m => m.Object_id.Contains(tmpValue));
            }
            if(model.IsSearchValueExist("Device_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Device_id"));
                models = models.Where(m => m.Device_id == tmpValue);
            }
            if(model.IsSearchValueExist("Username"))
            {
                string tmpValue = model.GetSearchValue("Username");
                models = models.Where(m => m.Username.Contains(tmpValue));
            }
            if(model.IsSearchValueExist("Read_no"))
            {
                long tmpValue = long.Parse(model.GetSearchValue("Read_no"));
                models = models.Where(m => m.Read_no == tmpValue);
            }
            if(model.IsSearchValueExist("Start_read_datetime"))
            {
                
                
            }
            if(model.IsSearchValueExist("End_read_datetime"))
            {
                
                
            }
            if(model.IsSearchValueExist("Last_read_datetime"))
            {
                
                
            }
            if(model.IsSearchValueExist("Warning_type"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Warning_type"));
                models = models.Where(m => m.Warning_type == tmpValue);
            }
            if(model.IsSearchValueExist("Period"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Period"));
                models = models.Where(m => m.Period == tmpValue);
            }
            if(model.IsSearchValueExist("Json"))
            {
            }
            if(model.IsSearchValueExist("Start_count"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Start_count"));
                models = models.Where(m => m.Start_count == tmpValue);
            }
            if(model.IsSearchValueExist("File_count"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("File_count"));
                models = models.Where(m => m.File_count == tmpValue);
            }
            if(model.IsSearchValueExist("Required_read_datetime"))
            {
                
                
            }
            if(model.IsSearchValueExist("Description"))
            {
                string tmpValue = model.GetSearchValue("Description");
                models = models.Where(m => m.Description.Contains(tmpValue));
            }
            if(model.IsSearchValueExist("Notes"))
            {
            }
            
            
            DTOrder order = null;
            if (model.Order != null && model.Order.Count() > 0)
            {
                order = model.Order.First();
                var pi = typeof(FormAnswerRound).GetProperty(model.Columns.ToList()[order.Column].Data);
                if(order.Dir == "asc")
                    models = models.OrderBy(x => pi.GetValue(x, null));
                else
                    models = models.OrderByDescending(x => pi.GetValue(x, null));
                
            }

            var totalRecords = models.Count();
            
            models = models.OrderBy(e => e.Id).Skip(model.Length * ((int)(Math.Ceiling((double)model.Start / model.Length)) + 1 - 1)).Take(model.Length);

            JsonResult jsonRes = Json(new
            {
                sEcho = model.Draw,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                //aaData = result
                aaData = models.ToList().Select(x => { var m = x.ToModel(); /*m.Device_id_Text = x.ANSWER_ROUND_DEVICE == null ? "" : x.ANSWER_ROUND_DEVICE.Udid;*/ m.Period_Text = x.ANSWER_ROUND_PERIOD_PARAMITEMS == null ? "" : x.ANSWER_ROUND_PERIOD_PARAMITEMS.Code;  return m;})
            }, JsonRequestBehavior.AllowGet);
            return jsonRes;
        }
        
        [AresAuthorize(ResourceKey = "Admin.FormAnswerRound.Create")]
        public ActionResult Create()
        {
            FormAnswerRoundModel model = new FormAnswerRoundModel();
            FillDropDowns(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormAnswerRound.Create")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Create([Bind(Exclude = "Filter_FormAnswerModel")] FormAnswerRoundModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {                
                var record = model.ToEntity();
                
            
                record = _iFormAnswerRoundsvc.Insert(record);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormAnswerRound.Created"));
                
                if (continueEditing)
                    return RedirectToAction("Edit", new { Id = record.Id });

                return RedirectToAction("List");
            }
            else
            {
                FillDropDowns(model);
                return View(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormAnswerRound.Update")]
        public ActionResult Edit(int id, string SelectedTab)
        {
            FormAnswerRound record = _iFormAnswerRoundsvc.GetById(id);
            var model = record.ToModel();
            model.SelectedTab = SelectedTab;
                        
            FillDropDowns(model);
            
            FillDropDownFormAnswer(model.Filter_FormAnswerModel);
                       
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormAnswerRound.Update")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Edit([Bind(Exclude = "Filter_FormAnswerModel")] FormAnswerRoundModel model, bool continueEditing)
        {
            var item = _iFormAnswerRoundsvc.GetById(model.Id);
            FormAnswerRound originalItem = (FormAnswerRound)item.Clone();
            if (item == null)
                //No country found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                item = model.ToEntity(item);
                item.Object_id = originalItem.Object_id;
                item.Insert_user_id = originalItem.Insert_user_id;
                item.Insert_datetime = originalItem.Insert_datetime;


                _iFormAnswerRoundsvc.Update(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormAnswerRound.Updated"));
                
                if(continueEditing)
                    return RedirectToAction("Edit", new { Id = item.Id });

                return RedirectToAction("List");
            }
            else
            {
                FillDropDowns(model);
            
            FillDropDownFormAnswer(model.Filter_FormAnswerModel);
                       
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormAnswerRound.Delete")]
        public ActionResult Delete(int id)
        {

            var item = _iFormAnswerRoundsvc.GetById(id);
            if (item == null)
                return RedirectToAction("List");

            try
            {
                item.FORM_ANSWER_ROUNDForm_answer.ToList().ForEach(d => _iFormAnswersvc.Delete(d));
                
                _iFormAnswerRoundsvc.Delete(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormAnswerRound.Deleted"));
                return RedirectToAction("List");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("Edit", new { id = item.Id });
            }
        }
        
        
        public ActionResult CreateOrUpdate(string itemId, string imageUrl, string mimeType, string uploadFilename)
        {
            FormAnswerRoundModel response = null;
            

            int nItemId = -1;
            int.TryParse(itemId, out nItemId);

            if (nItemId != -1)
            {
                FormAnswerRound record = _iFormAnswerRoundsvc.GetById(nItemId);
                response = record.ToModel();
                if (record != null) // update
                {

                   

                }
                else // insert mode (after image upload popup close)
                {
                    response = new FormAnswerRoundModel();
                    //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() { Text = "" } };
                }
            }
            else // insert
            {
                response = new FormAnswerRoundModel();
                //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() {Text = "" }  };
                
            }

 
            ViewBag.itemId = nItemId;

            return View(response);
        }
        
        
        public void FillDropDowns(FormAnswerRoundModel model)
        {
            

            //model.Device_id_Device_SelectList = new SelectList(_iDevicesvc.GetAll().ToList(), "Id", "Udid");

            model.Period_ParameterItem_SelectList = new SelectList(_iParameterItemsvc.GetAllFromCache().ToList(), "Id", "Code");

            model.Warning_type_ParameterItem_SelectList = new SelectList(_iParameterItemsvc.GetAllFromCache().ToList(), "Id", "Code");
            
            
        }
        
        #endregion
                         



            
        #region  FormAnswer methods
        
        public void FillDropDownFormAnswer(FormAnswerModel model)
        {

            model.Device_id_Device_SelectList = new SelectList(_iDevicesvc.GetAll().ToList(), "Id", "Udid");

            model.Previous_answer_FormAnswer_SelectList = new SelectList(_iFormAnswersvc.GetAll().ToList(), "Id", "Object_id");

            model.Form_question_id_FormQuestion_SelectList = new SelectList(_iFormQuestionsvc.GetAll().ToList(), "Id", "Object_id");

            model.Form_answer_round_id_FormAnswerRound_SelectList = new SelectList(_iFormAnswerRoundsvc.GetAll().ToList(), "Id", "Object_id");

            model.Warning_type_ParameterItem_SelectList = new SelectList(_iParameterItemsvc.GetAll().ToList(), "Id", "Code");
        }
        
        public ActionResult CreateOrUpdateFormAnswer(string itemId, string imageUrl, string mimeType, string uploadFilename)
        {
            FormAnswerModel response = null;


            int nItemId = -1;
            int.TryParse(itemId, out nItemId);

            if (nItemId != -1)
            {
                FormAnswer record = _iFormAnswersvc.GetById(nItemId);
                response = record.ToModel();
                if (record != null) // update
                {



                    //response.RoleListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem { Text = "Administrators" }, new Ext.Net.ListItem { Text = "Guests" } };
                    //response.PermissionListItems = permListItems.ToArray();

                }
                else // insert mode (after image upload popup close)
                {
                    response = new FormAnswerModel();
                    //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() { Text = "" } };
                }
            }
            else // insert
            {
                response = new FormAnswerModel();
                //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() {Text = "" }  };

            }


            ViewBag.itemId = nItemId;

            return View(response);
        }
        
        [AresAuthorize(ResourceKey = "Admin.FormAnswer.Read")]
        public ActionResult ListFormAnswerDatatable(Ares.Web.Razor.Extensions.Datatable.Model.DTParameterModel model, int? FormAnswerRoundModelId)
        {
            var models = _iFormAnswersvc.GetAll();
            models = models.Where(m => m.Form_answer_round_id == FormAnswerRoundModelId);
            if (model.Search != null && !string.IsNullOrEmpty(model.Search.Value))
            {
                models = models.Where(m => m.Object_id.Contains(model.Search.Value));
            }
            
            if (model.IsSearchValueExist("Object_id"))
            {
                string tmpValue = model.GetSearchValue("Object_id");
                models = models.Where(m => m.Object_id.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Form_question_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Form_question_id"));
                models = models.Where(m => m.Form_question_id == tmpValue);
            }
            if (model.IsSearchValueExist("Device_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Device_id"));
                models = models.Where(m => m.Device_id == tmpValue);
            }
            if (model.IsSearchValueExist("Form_answer_round_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Form_answer_round_id"));
                models = models.Where(m => m.Form_answer_round_id == tmpValue);
            }
            if (model.IsSearchValueExist("Previous_answer"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Previous_answer"));
                models = models.Where(m => m.Previous_answer == tmpValue);
            }
            if (model.IsSearchValueExist("Username"))
            {
                string tmpValue = model.GetSearchValue("Username");
                models = models.Where(m => m.Username.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Read_no"))
            {
                long tmpValue = long.Parse(model.GetSearchValue("Read_no"));
                models = models.Where(m => m.Read_no == tmpValue);
            }
            if (model.IsSearchValueExist("Warning_type"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Warning_type"));
                models = models.Where(m => m.Warning_type == tmpValue);
            }
            if (model.IsSearchValueExist("Answer"))
            {
                string tmpValue = model.GetSearchValue("Answer");
                models = models.Where(m => m.Answer.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Long_answer"))
            {
            }
            if (model.IsSearchValueExist("Converted_answer"))
            {
                string tmpValue = model.GetSearchValue("Converted_answer");
                models = models.Where(m => m.Converted_answer.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Json"))
            {
            }
            if (model.IsSearchValueExist("Required_read_datetime"))
            {
                
                
            }
            if (model.IsSearchValueExist("Actual_read_datetime"))
            {
                
                
            }
            if (model.IsSearchValueExist("Description"))
            {
                string tmpValue = model.GetSearchValue("Description");
                models = models.Where(m => m.Description.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Notes"))
            {
            }
            if (model.IsSearchValueExist("Udid"))
            {
                string tmpValue = model.GetSearchValue("Udid");
                models = models.Where(m => m.Udid.Contains(tmpValue));
            }
            
            DTOrder order = null;
            if (model.Order != null && model.Order.Count() > 0)
            {
                order = model.Order.First();
                var pi = typeof(FormAnswer).GetProperty(model.Columns.ToList()[order.Column].Data);
                if(order.Dir == "asc")
                    models = models.OrderBy(x => pi.GetValue(x, null));
                else
                    models = models.OrderByDescending(x => pi.GetValue(x, null));
                
            }

            var totalRecords = models.Count();
            
            models = models.OrderBy(e => e.Id).Skip(model.Length * ((int)(Math.Ceiling((double)model.Start / model.Length)) + 1 - 1)).Take(model.Length);

            JsonResult jsonRes = Json(new
            {
                sEcho = model.Draw,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                //aaData = result
                aaData = models.ToList().Select(x => { var m = x.ToModel(); /*m.Device_id_Text = x.ANSWER_DEVICE == null ? "" : x.ANSWER_DEVICE.Udid;*/ m.Previous_answer_Text = x.ANSWER_PREVIOUS_ANSWER == null ? "" : x.ANSWER_PREVIOUS_ANSWER.Object_id; m.Form_question_id_Text = x.ANSWER_QUESTION == null ? "" : x.ANSWER_QUESTION.Object_id; m.Form_answer_round_id_Text = x.ANSWER_ROUND == null ? "" : x.ANSWER_ROUND.Object_id;  return m;})
            }, JsonRequestBehavior.AllowGet);
            return jsonRes;
        }
        
        [AresAuthorize(ResourceKey = "Admin.FormAnswer.Create")]
        public ActionResult CreateFormAnswer(int FormAnswerRoundModelId)
        {
            FormAnswerModel model = new FormAnswerModel();
            model.Form_answer_round_id = FormAnswerRoundModelId;
            
            
            FillDropDownFormAnswer(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormAnswer.Create")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult CreateFormAnswer( FormAnswerModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {                
                var record = model.ToEntity();
                
                
                record = _iFormAnswersvc.Insert(record);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormAnswer.Created"));
                
                if (continueEditing)
                    return RedirectToAction("EditFormAnswer", new { id = record.Id });

                return RedirectToAction("Edit", new  { id = model.Form_answer_round_id, SelectedTab = "2" });
            }
            else
            {
                FillDropDownFormAnswer(model);
                return View(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }
        
        public ActionResult EditFormAnswer(int id, string SelectedTab)
        {
            FormAnswer record = _iFormAnswersvc.GetById(id);
            var model = record.ToModel();
            model.SelectedTab = SelectedTab;
            
            
            
            FillDropDownFormAnswer(model);

            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormAnswer.Update")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult EditFormAnswer( FormAnswerModel model, bool continueEditing)
        {
            var item = _iFormAnswersvc.GetById(model.Id);
            if (item == null)
                //No country found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                item = model.ToEntity(item);
                
                

                _iFormAnswersvc.Update(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormAnswer.Updated"));
                
                if(continueEditing)
                    return RedirectToAction("EditFormAnswer", new { id = item.Id });
                
                return RedirectToAction("Edit", new  { id = model.Form_answer_round_id, SelectedTab = "2" });
            }

            //If we got this far, something failed, redisplay form
            FillDropDownFormAnswer(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormAnswer.Delete")]
        public ActionResult DeleteFormAnswer(int id)
        {

            var item = _iFormAnswersvc.GetById(id);
            if (item == null)
                return RedirectToAction("List");

            try
            {
                
                _iFormAnswersvc.Delete(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormAnswer.Deleted"));
                
                return RedirectToAction("Edit", new  { id = item.Form_answer_round_id, SelectedTab = "2" });
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("EditFormAnswer", new { id = item.Id });
            }
        }
        #endregion 
          



        


        

    }
}

