﻿



using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.IO;
using System.Web.Http;
using System.Net;
using System.Web.Http.ModelBinding;
using System.Web.Security;
using Ext.Net.MVC;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using System.Web.Mvc;
using System.Linq;
using Ares.UMS.Svc.Directory;
using Ares.UMS.DTO.Users;
using Ares.UMS.Svc.Users;
using Ares.Web.Svc.Authentication;
using Ares.Web.Svc.Web;
using Ares.UMS.Svc.Common;
using Ares.UMS.DTO.Messages;
using Ares.UMS.Svc.Messages;
using Ares.Core.Helpers;
using Ares.UMS.DTO.Localization;
using Ares.UMS.Svc.Localization;
using Ares.UMS.DTO.Media;
using Ares.UMS.Svc.Media;
using Ares.Web.Models.Role;
using Ares.UMS.Svc.Security;
using Ares.UMS.DTO.Security;
using Ares.Web.Helpers;
using Ares.Web.Attributes;
using Ares.Web.Controllers;
using Ares.UMS.Svc.Configuration;
using Ares.UMS.DTO.Configuration;
using Ares.UMS.Svc;
using Ares.Web.Razor.Extensions.Datatable.Model;
using AssetHealth.Extensions;
using AssetHealth.Models.User;
using AssetHealth.Common.Svc;
using AssetHealth.Common.DTO;
using AssetHealth.Models.Configuration;
using AssetHealth.Models.Eq_threshold;
using AssetHealth.Models.User;
using AssetHealth.Models.Form_answer;
using AssetHealth.Models.Form_question;
using AssetHealth.Models.Form_answer_round;
using AssetHealth.Models.Synchronization_detail;
using AssetHealth.Models.Synchronization_device;
using AssetHealth.Models.Synchronization;
using AssetHealth.Models.Synchronization_log;
using AssetHealth.Models.Equipment;
using AssetHealth.Models.Eq_warning;
using AssetHealth.Models.Eq_channel;
using Ares.Sync.Svc;
using AssetHealth.Common.DTO.Extensions;

namespace AssetHealth.Contollers
{
	
    public partial class EqThresholdController : BaseAresController
	{
		#region Fields

        private readonly IEqThresholdSvc _iEqThresholdsvc;
        
        private readonly IUserSvc _iUsersvc;
        private readonly IFormAnswerSvc _iFormAnswersvc;
        private readonly IFormQuestionSvc _iFormQuestionsvc;
        private readonly IFormAnswerRoundSvc _iFormAnswerRoundsvc;
        private readonly IParameterItemSvc _iParameterItemsvc;
        private readonly ISynchronizationDetailSvc _iSynchronizationDetailsvc;
        private readonly ISynchronizationDeviceSvc _iSynchronizationDevicesvc;
        private readonly ISynchronizationSvc _iSynchronizationsvc;
        private readonly ISynchronizationLogSvc _iSynchronizationLogsvc;
        private readonly IEquipmentSvc _iEquipmentsvc;
        private readonly IEqWarningSvc _iEqWarningsvc;
        private readonly IEqChannelSvc _iEqChannelsvc;
        private readonly IEqLocationSvc _iEqLocationSvc;

        private readonly IStateProvinceSvc _stateProvinceSvc;
        private readonly ISettingSvc _settingSvc;
        private readonly FileSettings _fileSettings;
        
        private readonly IGenericAttributeSvc _genericAttributeSvc;
        private readonly ILocalizationSvc _localizationSvc;
        private readonly IPictureSvc _pictureSvc;
        private readonly IUserSvc _userSvc;
        private readonly IRoleSvc _roleSvc;
        private readonly IPermissionSvc _permissionSvc;
        private readonly IMenuSvc _menuSvc;
        private readonly IWorkContext _workContext;
        private readonly IUserRegistrationSvc _userRegistrationSvc;
        private readonly IAuthenticationSvc _authenticationSvc;
        private readonly IWorkflowMessageSvc _workflowMessageSvc;
        private readonly INewsLetterSubscriptionSvc _newsLetterSubscriptionSvc;
        private readonly IWebHelper _webHelper;
        private readonly UserSettings _userSettings;
        private readonly LocalizationSettings _localizationSettings;
        private readonly MediaSettings _mediaSettings;

	    #endregion

		#region Constructors

        public EqThresholdController(IEqThresholdSvc EqThresholdsvc,
        IUserSvc Usersvc, IEqLocationSvc iEqLocationSvc,
        IFormAnswerSvc FormAnswersvc,
        IFormQuestionSvc FormQuestionsvc,
        IFormAnswerRoundSvc FormAnswerRoundsvc,
        IParameterItemSvc ParameterItemsvc,
        ISynchronizationDetailSvc SynchronizationDetailsvc,
        ISynchronizationDeviceSvc SynchronizationDevicesvc,
        ISynchronizationSvc Synchronizationsvc,
        ISynchronizationLogSvc SynchronizationLogsvc,
        IEquipmentSvc Equipmentsvc,
        IEqWarningSvc EqWarningsvc,
        IEqChannelSvc EqChannelsvc,
            IStateProvinceSvc stateProvinceSvc, IGenericAttributeSvc genericAttributeSvc, ILocalizationSvc localizationSvc,  
            UserSettings userSettings, IPictureSvc pictureSvc, ISettingSvc settingSvc, FileSettings fileSettings,
            IUserSvc userSvc, IRoleSvc roleSvc, IPermissionSvc permissionSvc, IMenuSvc menuSvc, IWorkContext workContext, IUserRegistrationSvc userRegistrationSvc, IAuthenticationSvc authenticationSvc, IWorkflowMessageSvc workflowMessageSvc,
            INewsLetterSubscriptionSvc newsLetterSubscriptionSvc, IWebHelper webHelper, LocalizationSettings localizationSettings, MediaSettings mediaSettings)
		{
            this._iEqThresholdsvc = EqThresholdsvc;
            
            this._iUsersvc = Usersvc;
            this._iFormAnswersvc = FormAnswersvc;
            this._iFormQuestionsvc = FormQuestionsvc;
            this._iFormAnswerRoundsvc = FormAnswerRoundsvc;
            this._iParameterItemsvc = ParameterItemsvc;
            this._iSynchronizationDetailsvc = SynchronizationDetailsvc;
            this._iSynchronizationDevicesvc = SynchronizationDevicesvc;
            this._iSynchronizationsvc = Synchronizationsvc;
            this._iSynchronizationLogsvc = SynchronizationLogsvc;
            this._iEquipmentsvc = Equipmentsvc;
            this._iEqWarningsvc = EqWarningsvc;
            this._iEqChannelsvc = EqChannelsvc;
            this._settingSvc = settingSvc;
            this._fileSettings = fileSettings;
            this._iEqLocationSvc = iEqLocationSvc;

            this._stateProvinceSvc = stateProvinceSvc;
            this._genericAttributeSvc = genericAttributeSvc;
            this._pictureSvc = pictureSvc;
            this._localizationSvc = localizationSvc;
            this._userSettings = userSettings;
            this._userSvc = userSvc;
            this._roleSvc = roleSvc;
            this._permissionSvc = permissionSvc;
            this._menuSvc = menuSvc;
            this._workContext = workContext;
            this._userRegistrationSvc = userRegistrationSvc;
            this._authenticationSvc = authenticationSvc;
            this._workflowMessageSvc = workflowMessageSvc;
            this._newsLetterSubscriptionSvc = newsLetterSubscriptionSvc;
            this._webHelper = webHelper;
            this._localizationSettings = localizationSettings;
            this._mediaSettings = mediaSettings;
		}

		#endregion 

        #region Utilities
        
        
        #endregion

        #region EqThreshold methods

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        [AresAuthorize(ResourceKey = "Admin.EqThreshold.Read")]
        public ActionResult List()
        {
            EqThresholdModel model = new EqThresholdModel();
            //model. = _roleSvc.GetAll().ToList().Select(x => x.ToModel()).ToList();
            FillDropDowns(model);
            model.Active = true;
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.EqThreshold.Read")]
        public ActionResult ListEqThresholdDatatable(Ares.Web.Razor.Extensions.Datatable.Model.DTParameterModel model)
        {
            var models = _iEqThresholdsvc.GetAll();

            if (!_workContext.CurrentUser.IsAdmin())
            {
                List<int> relatedLocationIds = _workContext.CurrentUser.RelatedLocations().Select(r => r.Id).ToList();
                models = models.Where(e => relatedLocationIds.Contains(e.threshold_eq_channel.channel_equipment.Location.Id));
            }

            if (model.Search != null && !string.IsNullOrEmpty(model.Search.Value))
            {
                models = models.Where(m => m.Object_id.Contains(model.Search.Value));
            }

            if (model.IsSearchValueExist("Filter_EqLocation_Text"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Filter_EqLocation_Text"));
                models = models.Where(m => m.threshold_eq_channel.channel_equipment.Eq_location_id == tmpValue);
            }

            if (model.IsSearchValueExist("Eq_channel_id_Text"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Eq_channel_id_Text"));
                models = models.Where(m => m.Eq_channel_id == tmpValue);
            }
            if(model.IsSearchValueExist("Name"))
            {
                string tmpValue = model.GetSearchValue("Name");
                models = models.Where(m => m.Name.Contains(tmpValue));
            }
            if(model.IsSearchValueExist("Description"))
            {
            }
            if(model.IsSearchValueExist("Lower_limit"))
            {
            }
            if(model.IsSearchValueExist("Upper_limit"))
            {
            }
            if(model.IsSearchValueExist("Warning_type_Text"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Warning_type_Text"));
                models = models.Where(m => m.Warning_type == tmpValue);
            }
            if(model.IsSearchValueExist("Strategy"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Strategy"));
                models = models.Where(m => m.Strategy == tmpValue);
            }
            if(model.IsSearchValueExist("Level_increase_percentage"))
            {
            }
            if(model.IsSearchValueExist("Level_decrease_percentage"))
            {
            }
            if(model.IsSearchValueExist("Level_increase_repeat_times"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Level_increase_repeat_times"));
                models = models.Where(m => m.Level_increase_repeat_times == tmpValue);
            }
            if(model.IsSearchValueExist("Level_decrease_repeat_times"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Level_decrease_repeat_times"));
                models = models.Where(m => m.Level_decrease_repeat_times == tmpValue);
            }
            if(model.IsSearchValueExist("Active"))
            {
                bool tmpValue = bool.Parse(model.GetSearchValue("Active"));
                models = models.Where(m => m.Active == tmpValue);
            }
            
            
            DTOrder order = null;
            if (model.Order != null && model.Order.Count() > 0)
            {
                order = model.Order.First();
                var pi = typeof(EqThreshold).GetProperty(model.Columns.ToList()[order.Column].Data);
                if(order.Dir == "asc")
                    models = models.OrderBy(x => pi.GetValue(x, null));
                else
                    models = models.OrderByDescending(x => pi.GetValue(x, null));
                
            }

            var totalRecords = models.Count();
            
            models = models.OrderBy(e => e.Id).Skip(model.Length * ((int)(Math.Ceiling((double)model.Start / model.Length)) + 1 - 1)).Take(model.Length);

            JsonResult jsonRes = Json(new
            {
                sEcho = model.Draw,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                //aaData = result
                aaData = models.ToList().Select(x => { var m = x.ToModel(); m.Filter_EqLocation_Text =  x.threshold_eq_channel != null ? x.threshold_eq_channel.channel_equipment.Location.Location_name : "" ; m.Strategy_Text = x.THRESHOLD_STRATEGY_PARAMITEMS == null ? "" : x.THRESHOLD_STRATEGY_PARAMITEMS.GetLocalized(p=> p.Title); m.Warning_type_Text = x.THRESHOLD_WARNING_TYPE_PARAMITEMS == null ? "" : x.THRESHOLD_WARNING_TYPE_PARAMITEMS.GetLocalized(p => p.Title); m.Eq_channel_id_Text = x.threshold_eq_channel == null ? "" : x.threshold_eq_channel.Name;  return m;})
            }, JsonRequestBehavior.AllowGet);
            return jsonRes;
        }
        
        [AresAuthorize(ResourceKey = "Admin.EqThreshold.Create")]
        public ActionResult Create()
        {
            EqThresholdModel model = new EqThresholdModel();
            FillDropDowns(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.EqThreshold.Create")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Create( EqThresholdModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {                
                var record = model.ToEntity();
                
                record.Object_id = Guid.NewGuid().ToString();
                record.Insert_user_id = _workContext.CurrentUser.Id.ToString();
                record.Insert_datetime = StringHelper.ConvertDateTimeToString(DateTime.Now);
                
            
                record = _iEqThresholdsvc.Insert(record);

                SuccessNotification(_localizationSvc.GetResource("Notifications.EqThreshold.Created"));
                
                if (continueEditing)
                    return RedirectToAction("Edit", new { Id = record.Id });

                return RedirectToAction("List");
            }
            else
            {
                FillDropDowns(model);
                return View(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.EqThreshold.Update")]
        [EqThresholdAuthorize]
        public ActionResult Edit(int id, string SelectedTab)
        {
            EqThreshold record = _iEqThresholdsvc.GetById(id);
            var model = record.ToModel();
            model.SelectedTab = SelectedTab;
                        
            FillDropDowns(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.EqThreshold.Update")]
        [EqThresholdAuthorize]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Edit( EqThresholdModel model, bool continueEditing)
        {
            var item = _iEqThresholdsvc.GetById(model.Id);
            EqThreshold originalItem = (EqThreshold)item.Clone();
            if (item == null)
                //No country found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                item = model.ToEntity(item);
                item.Object_id = originalItem.Object_id;                
                item.Insert_user_id = originalItem.Insert_user_id;                
                item.Insert_datetime = originalItem.Insert_datetime;                
                
                
                
                item.Update_user_id = _workContext.CurrentUser.Id.ToString();
                item.Update_datetime = StringHelper.ConvertDateTimeToString(DateTime.Now);

                _iEqThresholdsvc.Update(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.EqThreshold.Updated"));
                
                if(continueEditing)
                    return RedirectToAction("Edit", new { Id = item.Id });

                return RedirectToAction("List");
            }
            else
            {
                FillDropDowns(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.EqThreshold.Delete")]
        [EqThresholdAuthorize]
        public ActionResult Delete(int id)
        {

            var item = _iEqThresholdsvc.GetById(id);
            if (item == null)
                return RedirectToAction("List");

            try
            {
                
                _iEqThresholdsvc.Delete(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.EqThreshold.Deleted"));
                return RedirectToAction("List");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("Edit", new { id = item.Id });
            }
        }
        
        
        public ActionResult CreateOrUpdate(string itemId, string imageUrl, string mimeType, string uploadFilename)
        {
            EqThresholdModel response = null;
            

            int nItemId = -1;
            int.TryParse(itemId, out nItemId);

            if (nItemId != -1)
            {
                EqThreshold record = _iEqThresholdsvc.GetById(nItemId);
                response = record.ToModel();
                if (record != null) // update
                {

                   

                }
                else // insert mode (after image upload popup close)
                {
                    response = new EqThresholdModel();
                    //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() { Text = "" } };
                }
            }
            else // insert
            {
                response = new EqThresholdModel();
                //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() {Text = "" }  };
                
            }

 
            ViewBag.itemId = nItemId;

            return View(response);
        }
        
        
        public void FillDropDowns(EqThresholdModel model)
        {

            model.Filter_EqLocation_SelectList = new SelectList(_iEqLocationSvc.GetAuthorizedLocations(_workContext.CurrentUser.Id), "Id", "Location_name");
            model.Strategy_ParameterItem_SelectList = new SelectList(_iParameterItemsvc.GetAllFromCache().Where(p => p.Group.System_name == "EQ_THRESHOLD_STRATEGY").ToList().Select(p => { return new { Id = p.Id, Title = p.GetLocalized(l => l.Title) }; }), "Id", "Title");
            model.Warning_type_ParameterItem_SelectList = new SelectList(_iParameterItemsvc.GetAllFromCache().Where(p => p.Group.System_name == "EQ_WARNING_WARNING_TYPE").ToList().Select(p => { return new { Id = p.Id, Title = p.GetLocalized(l => l.Title) }; }), "Id", "Title");

            //model.Eq_channel_id_EqChannel_SelectList = new SelectList(_iEqChannelsvc.GetAll().ToList(), "Id", "Name");
            model.Eq_channel_id_EqChannel_SelectList = new SelectList(_iEqChannelsvc.GetAuthorizedChannels(_workContext.CurrentUser.Id), "Id", "Name");


        }
        
        #endregion
                         
        

    }
}

