using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using System.Web.Mvc;
using Ares.UMS.Svc.Directory;
using System.Linq;
using Ares.UMS.DTO.Users;
using Ares.UMS.Svc.Users;

using Ares.UMS.Svc.Common;
using Ares.UMS.DTO.Messages;
using Ares.UMS.Svc.Messages;
using Ares.Core.Helpers;
using Ares.UMS.DTO.Localization;
using Ares.UMS.Svc.Localization;
using System.Net;
using System.Web.Security;
using Ext.Net.MVC;
using Ares.UMS.DTO.Media;
using Ares.UMS.Svc.Media;
using Ares.Web.Models.Role;
using System.IO;
using Ares.UMS.Svc.Security;
using Ares.UMS.DTO.Security;
using Ares.Web.Helpers;

using Ares.Web.Attributes;
using Ares.Web.Controllers;
using Ares.UMS.Svc.Configuration;

using Ares.UMS.DTO.Configuration;
using Ares.UMS.Svc;
using Ares.Web.Svc.Authentication;
using AssetHealth.Models.User;
using AssetHealth.Extensions;

namespace AssetHealth.Controllers
{
    public class PermissionController : BaseAresController
    {
        private readonly IStateProvinceSvc _stateProvinceSvc;
        private readonly IGenericAttributeSvc _genericAttributeSvc;
        private readonly ILocalizationSvc _localizationSvc;
        private readonly IPictureSvc _pictureSvc;
        private readonly IUserSvc _userSvc;
        private readonly IRoleSvc _roleSvc;
        private readonly IPermissionSvc _permissionSvc;
        private readonly IMenuSvc _menuSvc;
        private readonly IWorkContext _workContext;
        private readonly IUserRegistrationSvc _userRegistrationSvc;
        private readonly IAuthenticationSvc _authenticationSvc;
        private readonly IWorkflowMessageSvc _workflowMessageSvc;
        private readonly INewsLetterSubscriptionSvc _newsLetterSubscriptionSvc;
        private readonly IWebHelper _webHelper;
        private readonly UserSettings _userSettings;
        private readonly LocalizationSettings _localizationSettings;
        private readonly MediaSettings _mediaSettings;

        public PermissionController(IStateProvinceSvc stateProvinceSvc, IGenericAttributeSvc genericAttributeSvc, ILocalizationSvc localizationSvc,  
            UserSettings userSettings, IPictureSvc pictureSvc,
            IUserSvc userSvc, IRoleSvc roleSvc, IPermissionSvc permissionSvc, IMenuSvc menuSvc, IWorkContext workContext, IUserRegistrationSvc userRegistrationSvc, IAuthenticationSvc authenticationSvc, IWorkflowMessageSvc workflowMessageSvc,
            INewsLetterSubscriptionSvc newsLetterSubscriptionSvc, IWebHelper webHelper, LocalizationSettings localizationSettings, MediaSettings mediaSettings)
        {
            this._stateProvinceSvc = stateProvinceSvc;
            this._genericAttributeSvc = genericAttributeSvc;
            this._pictureSvc = pictureSvc;
            this._localizationSvc = localizationSvc;
            this._userSettings = userSettings;
            this._userSvc = userSvc;
            this._roleSvc = roleSvc;
            this._permissionSvc = permissionSvc;
            this._menuSvc = menuSvc;
            this._workContext = workContext;
            this._userRegistrationSvc = userRegistrationSvc;
            this._authenticationSvc = authenticationSvc;
            this._workflowMessageSvc = workflowMessageSvc;
            this._newsLetterSubscriptionSvc = newsLetterSubscriptionSvc;
            this._webHelper = webHelper;
            this._localizationSettings = localizationSettings;
            this._mediaSettings = mediaSettings;

        }




        public ActionResult List()
        {
            PermissionModel model = new PermissionModel();
            //model. = _roleSvc.GetAll().ToList().Select(x => x.ToModel()).ToList();

            return View(model);

        }


        public ActionResult ListPermissionDatatable(Ares.Web.Razor.Extensions.Datatable.Model.DTParameterModel model)
        {
            var models = _permissionSvc.GetAll();
            
            if (model.Search != null && !string.IsNullOrEmpty(model.Search.Value))
            {
                models = models.Where(m => m.PermissionName.Contains(model.Search.Value));
            }
            var totalRecords = models.Count();

            models = models.OrderBy(e => e.Id).Skip(model.Length * ((int)(Math.Ceiling((double)model.Start / model.Length)) + 1 - 1)).Take(model.Length);

            

            JsonResult jsonRes = Json(new
            {
                sEcho = model.Draw,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                //aaData = result
                aaData = models.ToList().Select(x => x.ToModel())
            }, JsonRequestBehavior.AllowGet);
            return jsonRes;

        }

        public ActionResult Create()
        {
            var model = new PermissionModel();
            

            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.Permission.Create")]
        [System.Web.Mvc.HttpPost]
        public ActionResult Create(PermissionModel model)
        {
            if (!ModelState.IsValid)
            { // re-render the view when validation failed.
                return View("Create", model);
            }

            var permission = model.ToEntity();
            permission.Active = true;

            _permissionSvc.Insert(permission);

            SuccessNotification(_localizationSvc.GetResource("Notifications.Permission.Created"));

            return RedirectToAction("List");
        }

        public ActionResult Edit(int id)
        {
            Permission record = _permissionSvc.GetById(id);
            var model = record.ToModel();


            return View(model);
        }


        [AresAuthorize(ResourceKey = "Admin.Permission.Update")]
        [System.Web.Mvc.HttpPost]
        public ActionResult Edit(PermissionModel model)
        {
            
            var record = _permissionSvc.GetById(model.Id);

            record = model.ToEntity(record);

            _permissionSvc.Update(record);

            SuccessNotification(_localizationSvc.GetResource("Notifications.Permission.Updated"));

            return RedirectToAction("List");
        }


        public ActionResult CreateOrUpdate(string itemId, string imageUrl, string mimeType, string uploadFilename)
        {
            PermissionModel response = null;
            

            int nItemId = -1;
            int.TryParse(itemId, out nItemId);

            if (nItemId != -1)
            {
                Permission record = _permissionSvc.GetById(nItemId);
                response = record.ToModel();
               
            }
            else // insert
            {
                response = new PermissionModel();
                
            }

 
            ViewBag.itemId = nItemId;

            return View(response);
        }

        public ActionResult ListPermissions()
        {
            string jsonString = JsonHelper.JsonSerializer<IList<Models.User.PermissionModel>>(_permissionSvc.GetAll().ToList().Select(x => x.ToModel()).ToList());
            var result = new { success = true, data = jsonString, Message = "" };
            JsonResult jsonRes = Json(result, JsonRequestBehavior.AllowGet);
            return jsonRes;

            //return Json(new { success = true, data = _roleSvc.GetAll().ToList(), Message = "" }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Delete(int id)
        {
            if (id != -1)
            {
                Permission record = _permissionSvc.GetById(id);

                _permissionSvc.Delete(record);
                SuccessNotification(_localizationSvc.GetResource("Notifications.Permission.Deleted"));
            }
            else
            {

            }


            return RedirectToAction("List");
        }


    }
}
