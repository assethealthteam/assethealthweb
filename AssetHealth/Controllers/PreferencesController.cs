﻿using Ares.Core.Framework.Logging;
using Ares.UMS.DAL.Repository;
using Ares.UMS.DTO.Users;
using Ares.UMS.Svc;
using Ares.UMS.Svc.Common;
using Ares.UMS.Svc.Configuration;
using Ares.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Mvc;

namespace AssetHealth.Controllers
{
    public class PreferencesController : BaseAresController
    {
        private readonly IUserRepository _userRepository;
        private readonly IRoleRepository _roleRepository;
        
        private readonly UserSettings _userSettings;
        private readonly ILogger _logger;
        private readonly IMenuRepository _menuRepository;
        private readonly IMenuSvc _menuSvc;

        private readonly IGenericAttributeSvc _genericAttributeService;
        private readonly IWorkContext _workContext;

        public PreferencesController(IUserRepository userRepository, UserSettings userSettings, IMenuRepository menuRepository, IRoleRepository roleRepository, 
            ILogger logger, IMenuSvc menuSvc, 
            IGenericAttributeSvc genericAttributeService,
            IWorkContext workContext)
        {
            this._userRepository = userRepository;
            this._userSettings = userSettings;
            this._roleRepository = roleRepository;

            this._menuRepository = menuRepository;
            this._logger = logger;
            this._menuSvc = menuSvc;

            this._genericAttributeService = genericAttributeService;
            this._workContext = workContext;

        }

        [System.Web.Mvc.HttpPost]
        public virtual ActionResult SavePreference(string name, bool value)
        {
            //permission validation is not required here
            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException("name");

            _genericAttributeService.SaveAttribute(_workContext.CurrentUser, name, value);

            return Json(new
            {
                Result = true
            }, JsonRequestBehavior.AllowGet);
        }


    }
}