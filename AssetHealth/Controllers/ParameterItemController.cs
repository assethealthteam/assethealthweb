﻿



using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.IO;
using System.Web.Http;
using System.Net;
using System.Web.Http.ModelBinding;
using System.Web.Security;
using Ext.Net.MVC;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using System.Web.Mvc;
using System.Linq;
using Ares.UMS.Svc.Directory;
using Ares.UMS.DTO.Users;
using Ares.UMS.Svc.Users;
using Ares.Web.Svc.Authentication;
using Ares.Web.Svc.Web;
using Ares.UMS.Svc.Common;
using Ares.UMS.DTO.Messages;
using Ares.UMS.Svc.Messages;
using Ares.Core.Helpers;
using Ares.UMS.DTO.Localization;
using Ares.UMS.Svc.Localization;
using Ares.UMS.DTO.Media;
using Ares.UMS.Svc.Media;
using Ares.Web.Models.Role;
using Ares.UMS.Svc.Security;
using Ares.UMS.DTO.Security;
using Ares.Web.Helpers;
using Ares.Web.Attributes;
using Ares.Web.Controllers;
using Ares.UMS.Svc.Configuration;
using Ares.UMS.DTO.Configuration;
using Ares.UMS.Svc;
using Ares.Web.Razor.Extensions.Datatable.Model;
using AssetHealth.Extensions;
using AssetHealth.Models.User;
using AssetHealth.Common.Svc;
using AssetHealth.Common.DTO;
using AssetHealth.Models.Configuration;
using AssetHealth.Models.Synchronization_detail;
using AssetHealth.Models.Synchronization;
using AssetHealth.Models.User;
using AssetHealth.Models.Eq_channel;
using AssetHealth.Models.Eq_threshold;
using AssetHealth.Models.Eq_warning;
using AssetHealth.Models.Eq_location;
using AssetHealth.Models.Equipment;
using AssetHealth.Models.Form_answer;
using AssetHealth.Models.Form;
using AssetHealth.Models.Form_question;
using AssetHealth.Models.Form_question_option;
using Ares.Web.Models.Parameter_item;

namespace AssetHealth.Contollers
{
	
    public partial class ParameterItemController : BaseAresController
	{
		#region Fields

        private readonly IParameterItemSvc _iParameterItemsvc;
        
        private readonly IUserSvc _iUsersvc;
        private readonly IEqChannelSvc _iEqChannelsvc;
        private readonly IEqThresholdSvc _iEqThresholdsvc;
        private readonly IEqWarningSvc _iEqWarningsvc;
        private readonly IEqLocationSvc _iEqLocationsvc;
        private readonly IEquipmentSvc _iEquipmentsvc;
        private readonly IFormAnswerSvc _iFormAnswersvc;
        private readonly IFormSvc _iFormsvc;
        private readonly IFormQuestionSvc _iFormQuestionsvc;
        private readonly IFormQuestionOptionSvc _iFormQuestionOptionsvc;
        private readonly IParameterGroupSvc _iParameterGroupsvc;
        
        private readonly IStateProvinceSvc _stateProvinceSvc;
        private readonly ISettingSvc _settingSvc;
        private readonly FileSettings _fileSettings;
        
        private readonly IGenericAttributeSvc _genericAttributeSvc;
        private readonly ILocalizationSvc _localizationSvc;
        private readonly IPictureSvc _pictureSvc;
        private readonly IUserSvc _userSvc;
        private readonly IRoleSvc _roleSvc;
        private readonly IPermissionSvc _permissionSvc;
        private readonly IMenuSvc _menuSvc;
        private readonly IWorkContext _workContext;
        private readonly IUserRegistrationSvc _userRegistrationSvc;
        private readonly IAuthenticationSvc _authenticationSvc;
        private readonly IWorkflowMessageSvc _workflowMessageSvc;
        private readonly INewsLetterSubscriptionSvc _newsLetterSubscriptionSvc;
        private readonly IWebHelper _webHelper;
        private readonly UserSettings _userSettings;
        private readonly LocalizationSettings _localizationSettings;
        private readonly MediaSettings _mediaSettings;

	    #endregion

		#region Constructors

        public ParameterItemController(IParameterItemSvc ParameterItemsvc,
        IUserSvc Usersvc,
        IEqChannelSvc EqChannelsvc,
        IEqThresholdSvc EqThresholdsvc,
        IEqWarningSvc EqWarningsvc,
        IEqLocationSvc EqLocationsvc,
        IEquipmentSvc Equipmentsvc,
        IFormAnswerSvc FormAnswersvc,
        IFormSvc Formsvc,
        IFormQuestionSvc FormQuestionsvc,
        IFormQuestionOptionSvc FormQuestionOptionsvc,
        IParameterGroupSvc ParameterGroupsvc,
            IStateProvinceSvc stateProvinceSvc, IGenericAttributeSvc genericAttributeSvc, ILocalizationSvc localizationSvc,  
            UserSettings userSettings, IPictureSvc pictureSvc, ISettingSvc settingSvc, FileSettings fileSettings,
            IUserSvc userSvc, IRoleSvc roleSvc, IPermissionSvc permissionSvc, IMenuSvc menuSvc, IWorkContext workContext, IUserRegistrationSvc userRegistrationSvc, IAuthenticationSvc authenticationSvc, IWorkflowMessageSvc workflowMessageSvc,
            INewsLetterSubscriptionSvc newsLetterSubscriptionSvc, IWebHelper webHelper, LocalizationSettings localizationSettings, MediaSettings mediaSettings)
		{
            this._iParameterItemsvc = ParameterItemsvc;
            
            this._iUsersvc = Usersvc;
            this._iEqChannelsvc = EqChannelsvc;
            this._iEqThresholdsvc = EqThresholdsvc;
            this._iEqWarningsvc = EqWarningsvc;
            this._iEqLocationsvc = EqLocationsvc;
            this._iEquipmentsvc = Equipmentsvc;
            this._iFormAnswersvc = FormAnswersvc;
            this._iFormsvc = Formsvc;
            this._iFormQuestionsvc = FormQuestionsvc;
            this._iFormQuestionOptionsvc = FormQuestionOptionsvc;
            this._iParameterGroupsvc = ParameterGroupsvc;
            this._settingSvc = settingSvc;
            this._fileSettings = fileSettings;
            
            this._stateProvinceSvc = stateProvinceSvc;
            this._genericAttributeSvc = genericAttributeSvc;
            this._pictureSvc = pictureSvc;
            this._localizationSvc = localizationSvc;
            this._userSettings = userSettings;
            this._userSvc = userSvc;
            this._roleSvc = roleSvc;
            this._permissionSvc = permissionSvc;
            this._menuSvc = menuSvc;
            this._workContext = workContext;
            this._userRegistrationSvc = userRegistrationSvc;
            this._authenticationSvc = authenticationSvc;
            this._workflowMessageSvc = workflowMessageSvc;
            this._newsLetterSubscriptionSvc = newsLetterSubscriptionSvc;
            this._webHelper = webHelper;
            this._localizationSettings = localizationSettings;
            this._mediaSettings = mediaSettings;
		}

		#endregion 

        #region Utilities
        
        
        #endregion

        #region ParameterItem methods

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        [AresAuthorize(ResourceKey = "Admin.ParameterItem.Read")]
        public ActionResult List()
        {
            ParameterItemModel model = new ParameterItemModel();
            //model. = _roleSvc.GetAll().ToList().Select(x => x.ToModel()).ToList();
            FillDropDowns(model);
            
            /*
            FillDropDownForm(model.Filter_FormModel);
                       
            
            FillDropDownFormQuestion(model.Filter_FormQuestionModel);
            */           
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.ParameterItem.Read")]
        public ActionResult ListParameterItemDatatable(Ares.Web.Razor.Extensions.Datatable.Model.DTParameterModel model)
        {
            var models = _iParameterItemsvc.GetAll();
            
            if (model.Search != null && !string.IsNullOrEmpty(model.Search.Value))
            {
                models = models.Where(m => m.Code.Contains(model.Search.Value));
            }
            
            if(model.IsSearchValueExist("Parametergroup_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Parametergroup_id"));
                models = models.Where(m => m.Parametergroup_id == tmpValue);
            }
            if(model.IsSearchValueExist("Code"))
            {
                string tmpValue = model.GetSearchValue("Code");
                models = models.Where(m => m.Code.Contains(tmpValue));
            }
            if(model.IsSearchValueExist("Title"))
            {
                string tmpValue = model.GetSearchValue("Title");
                models = models.Where(m => m.Title.Contains(tmpValue));
            }
            if(model.IsSearchValueExist("Description"))
            {
                string tmpValue = model.GetSearchValue("Description");
                models = models.Where(m => m.Description.Contains(tmpValue));
            }
            if(model.IsSearchValueExist("Active"))
            {
                bool tmpValue = bool.Parse(model.GetSearchValue("Active"));
                models = models.Where(m => m.Active == tmpValue);
            }
            
            
            DTOrder order = null;
            if (model.Order != null && model.Order.Count() > 0)
            {
                order = model.Order.First();
                var pi = typeof(ParameterItem).GetProperty(model.Columns.ToList()[order.Column].Data);
                if(order.Dir == "asc")
                    models = models.OrderBy(x => pi.GetValue(x, null));
                else
                    models = models.OrderByDescending(x => pi.GetValue(x, null));
                
            }

            var totalRecords = models.Count();
            
            models = models.OrderBy(e => e.Id).Skip(model.Length * ((int)(Math.Ceiling((double)model.Start / model.Length)) + 1 - 1)).Take(model.Length);

            JsonResult jsonRes = Json(new
            {
                sEcho = model.Draw,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                //aaData = result
                aaData = models.ToList().Select(x => { var m = x.ToModel(); m.Parametergroup_id_Text = x.Group == null ? "" : x.Group.Name;  return m;})
            }, JsonRequestBehavior.AllowGet);
            return jsonRes;
        }
        
        [AresAuthorize(ResourceKey = "Admin.ParameterItem.Create")]
        public ActionResult Create()
        {
            ParameterItemModel model = new ParameterItemModel();
            FillDropDowns(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.ParameterItem.Create")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Create([Bind(Exclude = "Filter_FormModel,Filter_FormQuestionModel")] ParameterItemModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {                
                var record = model.ToEntity();
                
            
                record = _iParameterItemsvc.Insert(record);

                SuccessNotification(_localizationSvc.GetResource("Notifications.ParameterItem.Created"));
                
                if (continueEditing)
                    return RedirectToAction("Edit", new { Id = record.Id });

                return RedirectToAction("List");
            }
            else
            {
                FillDropDowns(model);
                return View(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.ParameterItem.Update")]
        public ActionResult Edit(int id, string SelectedTab)
        {
            ParameterItem record = _iParameterItemsvc.GetById(id);
            var model = record.ToModel();
            model.SelectedTab = SelectedTab;
                        
            FillDropDowns(model);
            /*
            FillDropDownForm(model.Filter_FormModel);
                       
            
            FillDropDownFormQuestion(model.Filter_FormQuestionModel);
                       
            */
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.ParameterItem.Update")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Edit([Bind(Exclude = "Filter_FormModel,Filter_FormQuestionModel")] ParameterItemModel model, bool continueEditing)
        {
            var item = _iParameterItemsvc.GetById(model.Id);
            ParameterItem originalItem = (ParameterItem)item.Clone();
            if (item == null)
                //No country found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                item = model.ToEntity(item);

                item.Insert_user_id = originalItem.Insert_user_id;
                item.Insert_datetime = originalItem.Insert_datetime;


                _iParameterItemsvc.Update(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.ParameterItem.Updated"));
                
                if(continueEditing)
                    return RedirectToAction("Edit", new { Id = item.Id });

                return RedirectToAction("List");
            }
            else
            {
                FillDropDowns(model);
            
                /*
            FillDropDownForm(model.Filter_FormModel);
                       
            
            FillDropDownFormQuestion(model.Filter_FormQuestionModel);
                    */   
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.ParameterItem.Delete")]
        public ActionResult Delete(int id)
        {

            var item = _iParameterItemsvc.GetById(id);
            if (item == null)
                return RedirectToAction("List");

            try
            {
                /*
                item.CATEGORYForm.ToList().ForEach(d => _iFormsvc.Delete(d));
                item.CATEGORYForm_question.ToList().ForEach(d => _iFormQuestionsvc.Delete(d));
                item.PERIODForm_question.ToList().ForEach(d => _iFormQuestionsvc.Delete(d));
                item.TYPEForm_question.ToList().ForEach(d => _iFormQuestionsvc.Delete(d));
                */
                _iParameterItemsvc.Delete(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.ParameterItem.Deleted"));
                return RedirectToAction("List");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("Edit", new { id = item.Id });
            }
        }
        
        
        public ActionResult CreateOrUpdate(string itemId, string imageUrl, string mimeType, string uploadFilename)
        {
            ParameterItemModel response = null;
            

            int nItemId = -1;
            int.TryParse(itemId, out nItemId);

            if (nItemId != -1)
            {
                ParameterItem record = _iParameterItemsvc.GetById(nItemId);
                response = record.ToModel();
                if (record != null) // update
                {

                   

                }
                else // insert mode (after image upload popup close)
                {
                    response = new ParameterItemModel();
                    //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() { Text = "" } };
                }
            }
            else // insert
            {
                response = new ParameterItemModel();
                //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() {Text = "" }  };
                
            }

 
            ViewBag.itemId = nItemId;

            return View(response);
        }
        
        
        public void FillDropDowns(ParameterItemModel model)
        {
            

            model.Parametergroup_id_ParameterGroup_SelectList = new SelectList(_iParameterGroupsvc.GetAll().ToList(), "Id", "Name");
            
            
        }
        
        #endregion
                         



            
        #region  Form methods
        
        public void FillDropDownForm(FormModel model)
        {

            model.Derived_id_Form_SelectList = new SelectList(_iFormsvc.GetAll().ToList(), "Id", "Code");

            model.Category_id_ParameterItem_SelectList = new SelectList(_iParameterItemsvc.GetAll().ToList(), "Id", "Code");
        }
        
        public ActionResult CreateOrUpdateForm(string itemId, string imageUrl, string mimeType, string uploadFilename)
        {
            FormModel response = null;


            int nItemId = -1;
            int.TryParse(itemId, out nItemId);

            if (nItemId != -1)
            {
                Form record = _iFormsvc.GetById(nItemId);
                response = record.ToModel();
                if (record != null) // update
                {



                    //response.RoleListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem { Text = "Administrators" }, new Ext.Net.ListItem { Text = "Guests" } };
                    //response.PermissionListItems = permListItems.ToArray();

                }
                else // insert mode (after image upload popup close)
                {
                    response = new FormModel();
                    //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() { Text = "" } };
                }
            }
            else // insert
            {
                response = new FormModel();
                //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() {Text = "" }  };

            }


            ViewBag.itemId = nItemId;

            return View(response);
        }
        
        [AresAuthorize(ResourceKey = "Admin.Form.Read")]
        public ActionResult ListFormDatatable(Ares.Web.Razor.Extensions.Datatable.Model.DTParameterModel model, int? ParameterItemModelId)
        {
            var models = _iFormsvc.GetAll();
            models = models.Where(m => m.Category_id == ParameterItemModelId);
            if (model.Search != null && !string.IsNullOrEmpty(model.Search.Value))
            {
                models = models.Where(m => m.Code.Contains(model.Search.Value));
            }
            
            if (model.IsSearchValueExist("Category_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Category_id"));
                models = models.Where(m => m.Category_id == tmpValue);
            }
            if (model.IsSearchValueExist("Derived_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Derived_id"));
                models = models.Where(m => m.Derived_id == tmpValue);
            }
            if (model.IsSearchValueExist("Code"))
            {
                string tmpValue = model.GetSearchValue("Code");
                models = models.Where(m => m.Code.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Title"))
            {
                string tmpValue = model.GetSearchValue("Title");
                models = models.Where(m => m.Title.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Description"))
            {
                string tmpValue = model.GetSearchValue("Description");
                models = models.Where(m => m.Description.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Version"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Version"));
                models = models.Where(m => m.Version == tmpValue);
            }
            if (model.IsSearchValueExist("Image_path"))
            {
                string tmpValue = model.GetSearchValue("Image_path");
                models = models.Where(m => m.Image_path.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Form_json"))
            {
            }
            if (model.IsSearchValueExist("Active"))
            {
                bool tmpValue = bool.Parse(model.GetSearchValue("Active"));
                models = models.Where(m => m.Active == tmpValue);
            }
            if (model.IsSearchValueExist("Deleted"))
            {
                bool tmpValue = bool.Parse(model.GetSearchValue("Deleted"));
                models = models.Where(m => m.Deleted == tmpValue);
            }
            if (model.IsSearchValueExist("Display_order"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Display_order"));
                models = models.Where(m => m.Display_order == tmpValue);
            }
            
            DTOrder order = null;
            if (model.Order != null && model.Order.Count() > 0)
            {
                order = model.Order.First();
                var pi = typeof(Form).GetProperty(model.Columns.ToList()[order.Column].Data);
                if(order.Dir == "asc")
                    models = models.OrderBy(x => pi.GetValue(x, null));
                else
                    models = models.OrderByDescending(x => pi.GetValue(x, null));
                
            }

            var totalRecords = models.Count();
            
            models = models.OrderBy(e => e.Id).Skip(model.Length * ((int)(Math.Ceiling((double)model.Start / model.Length)) + 1 - 1)).Take(model.Length);

            JsonResult jsonRes = Json(new
            {
                sEcho = model.Draw,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                //aaData = result
                aaData = models.ToList().Select(x => { var m = x.ToModel(); m.Derived_id_Text = x.DERIVED == null ? "" : x.DERIVED.Code; m.Category_id_Text = x.PARAMETER_ITEMS == null ? "" : x.PARAMETER_ITEMS.Code;  return m;})
            }, JsonRequestBehavior.AllowGet);
            return jsonRes;
        }
        
        [AresAuthorize(ResourceKey = "Admin.Form.Create")]
        public ActionResult CreateForm(int ParameterItemModelId)
        {
            FormModel model = new FormModel();
            model.Category_id = ParameterItemModelId;
            
            
            FillDropDownForm(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.Form.Create")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult CreateForm([Bind(Exclude = "Filter_FormQuestionModel")] FormModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {                
                var record = model.ToEntity();
                
                
                record = _iFormsvc.Insert(record);

                SuccessNotification(_localizationSvc.GetResource("Notifications.Form.Created"));
                
                if (continueEditing)
                    return RedirectToAction("EditForm", new { id = record.Id });

                return RedirectToAction("Edit", new  { id = model.Category_id, SelectedTab = "2" });
            }
            else
            {
                FillDropDownForm(model);
                return View(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }
        
        public ActionResult EditForm(int id, string SelectedTab)
        {
            Form record = _iFormsvc.GetById(id);
            var model = record.ToModel();
            model.SelectedTab = SelectedTab;
            
            
            
            FillDropDownForm(model);
            
            FillDropDownFormQuestion(model.Filter_FormQuestionModel);
                       

            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.Form.Update")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult EditForm([Bind(Exclude = "Filter_FormQuestionModel")] FormModel model, bool continueEditing)
        {
            var item = _iFormsvc.GetById(model.Id);
            if (item == null)
                //No country found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                item = model.ToEntity(item);
                
                

                _iFormsvc.Update(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.Form.Updated"));
                
                if(continueEditing)
                    return RedirectToAction("EditForm", new { id = item.Id });
                
                return RedirectToAction("Edit", new  { id = model.Category_id, SelectedTab = "2" });
            }

            //If we got this far, something failed, redisplay form
            FillDropDownForm(model);
            
            FillDropDownFormQuestion(model.Filter_FormQuestionModel);
                       
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.Form.Delete")]
        public ActionResult DeleteForm(int id)
        {

            var item = _iFormsvc.GetById(id);
            if (item == null)
                return RedirectToAction("List");

            try
            {
                item.FORMForm_question.ToList().ForEach(d => _iFormQuestionsvc.Delete(d));
                
                _iFormsvc.Delete(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.Form.Deleted"));
                
                return RedirectToAction("Edit", new  { id = item.Category_id, SelectedTab = "2" });
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("EditForm", new { id = item.Id });
            }
        }
        #endregion 
          



        





            
        #region  FormQuestion methods
        
        public void FillDropDownFormQuestion(FormQuestionModel model)
        {

            model.Category_id_ParameterItem_SelectList = new SelectList(_iParameterItemsvc.GetAll().ToList(), "Id", "Code");

            model.Derived_id_FormQuestion_SelectList = new SelectList(_iFormQuestionsvc.GetAll().ToList(), "Id", "Code");

            model.Form_id_Form_SelectList = new SelectList(_iFormsvc.GetAll().ToList(), "Id", "Code");

            model.Period_ParameterItem_SelectList = new SelectList(_iParameterItemsvc.GetAll().ToList(), "Id", "Code");

            model.Type_id_ParameterItem_SelectList = new SelectList(_iParameterItemsvc.GetAll().ToList(), "Id", "Code");
        }
        
        public ActionResult CreateOrUpdateFormQuestion(string itemId, string imageUrl, string mimeType, string uploadFilename)
        {
            FormQuestionModel response = null;


            int nItemId = -1;
            int.TryParse(itemId, out nItemId);

            if (nItemId != -1)
            {
                FormQuestion record = _iFormQuestionsvc.GetById(nItemId);
                response = record.ToModel();
                if (record != null) // update
                {



                    //response.RoleListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem { Text = "Administrators" }, new Ext.Net.ListItem { Text = "Guests" } };
                    //response.PermissionListItems = permListItems.ToArray();

                }
                else // insert mode (after image upload popup close)
                {
                    response = new FormQuestionModel();
                    //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() { Text = "" } };
                }
            }
            else // insert
            {
                response = new FormQuestionModel();
                //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() {Text = "" }  };

            }


            ViewBag.itemId = nItemId;

            return View(response);
        }
        
        [AresAuthorize(ResourceKey = "Admin.FormQuestion.Read")]
        public ActionResult ListFormQuestionDatatable(Ares.Web.Razor.Extensions.Datatable.Model.DTParameterModel model, int? FormModelId)
        {
            var models = _iFormQuestionsvc.GetAll();
            models = models.Where(m => m.Form_id == FormModelId);
            if (model.Search != null && !string.IsNullOrEmpty(model.Search.Value))
            {
                models = models.Where(m => m.Code.Contains(model.Search.Value));
            }
            
            if (model.IsSearchValueExist("Category_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Category_id"));
                models = models.Where(m => m.Category_id == tmpValue);
            }
            if (model.IsSearchValueExist("Type_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Type_id"));
                models = models.Where(m => m.Type_id == tmpValue);
            }
            if (model.IsSearchValueExist("Form_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Form_id"));
                models = models.Where(m => m.Form_id == tmpValue);
            }
            if (model.IsSearchValueExist("Derived_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Derived_id"));
                models = models.Where(m => m.Derived_id == tmpValue);
            }
            if (model.IsSearchValueExist("Code"))
            {
                string tmpValue = model.GetSearchValue("Code");
                models = models.Where(m => m.Code.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Title"))
            {
                string tmpValue = model.GetSearchValue("Title");
                models = models.Where(m => m.Title.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Hint"))
            {
                string tmpValue = model.GetSearchValue("Hint");
                models = models.Where(m => m.Hint.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Description"))
            {
                string tmpValue = model.GetSearchValue("Description");
                models = models.Where(m => m.Description.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Version"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Version"));
                models = models.Where(m => m.Version == tmpValue);
            }
            if (model.IsSearchValueExist("Image_path"))
            {
                string tmpValue = model.GetSearchValue("Image_path");
                models = models.Where(m => m.Image_path.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Device_image_path"))
            {
                string tmpValue = model.GetSearchValue("Device_image_path");
                models = models.Where(m => m.Device_image_path.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Estimated_time"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Estimated_time"));
                models = models.Where(m => m.Estimated_time == tmpValue);
            }
            if (model.IsSearchValueExist("Method"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Method"));
                models = models.Where(m => m.Method == tmpValue);
            }
            if (model.IsSearchValueExist("Display_order"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Display_order"));
                models = models.Where(m => m.Display_order == tmpValue);
            }
            if (model.IsSearchValueExist("Step"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Step"));
                models = models.Where(m => m.Step == tmpValue);
            }
            if (model.IsSearchValueExist("Period"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Period"));
                models = models.Where(m => m.Period == tmpValue);
            }
            if (model.IsSearchValueExist("V_required"))
            {
                bool tmpValue = bool.Parse(model.GetSearchValue("V_required"));
                models = models.Where(m => m.V_required == tmpValue);
            }
            if (model.IsSearchValueExist("V_email"))
            {
                string tmpValue = model.GetSearchValue("V_email");
                models = models.Where(m => m.V_email.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("V_min_length"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("V_min_length"));
                models = models.Where(m => m.V_min_length == tmpValue);
            }
            if (model.IsSearchValueExist("V_max_length"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("V_max_length"));
                models = models.Where(m => m.V_max_length == tmpValue);
            }
            if (model.IsSearchValueExist("V_regex"))
            {
                string tmpValue = model.GetSearchValue("V_regex");
                models = models.Where(m => m.V_regex.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Active"))
            {
                bool tmpValue = bool.Parse(model.GetSearchValue("Active"));
                models = models.Where(m => m.Active == tmpValue);
            }
            if (model.IsSearchValueExist("Deleted"))
            {
                bool tmpValue = bool.Parse(model.GetSearchValue("Deleted"));
                models = models.Where(m => m.Deleted == tmpValue);
            }
            
            DTOrder order = null;
            if (model.Order != null && model.Order.Count() > 0)
            {
                order = model.Order.First();
                var pi = typeof(FormQuestion).GetProperty(model.Columns.ToList()[order.Column].Data);
                if(order.Dir == "asc")
                    models = models.OrderBy(x => pi.GetValue(x, null));
                else
                    models = models.OrderByDescending(x => pi.GetValue(x, null));
                
            }

            var totalRecords = models.Count();
            
            models = models.OrderBy(e => e.Id).Skip(model.Length * ((int)(Math.Ceiling((double)model.Start / model.Length)) + 1 - 1)).Take(model.Length);

            JsonResult jsonRes = Json(new
            {
                sEcho = model.Draw,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                //aaData = result
                aaData = models.ToList().Select(x => { var m = x.ToModel(); m.Category_id_Text = x.QUESTION_CATEGORY_PARAMITEMS == null ? "" : x.QUESTION_CATEGORY_PARAMITEMS.Code; m.Derived_id_Text = x.QUESTION_DERIVED == null ? "" : x.QUESTION_DERIVED.Code; m.Form_id_Text = x.QUESTION_FORM == null ? "" : x.QUESTION_FORM.Code; m.Period_Text = x.QUESTION_PERIOD_CATEGORY_PARAMITEMS == null ? "" : x.QUESTION_PERIOD_CATEGORY_PARAMITEMS.Code; m.Type_id_Text = x.QUESTION_TYPE_CATEGORY_PARAMITEMS == null ? "" : x.QUESTION_TYPE_CATEGORY_PARAMITEMS.Code;  return m;})
            }, JsonRequestBehavior.AllowGet);
            return jsonRes;
        }
        
        [AresAuthorize(ResourceKey = "Admin.FormQuestion.Create")]
        public ActionResult CreateFormQuestion(int FormModelId)
        {
            FormQuestionModel model = new FormQuestionModel();
            model.Form_id = FormModelId;
            
            
            FillDropDownFormQuestion(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormQuestion.Create")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult CreateFormQuestion([Bind(Exclude = "Filter_FormAnswerModel,Filter_FormQuestionOptionModel")] FormQuestionModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {                
                var record = model.ToEntity();
                
                
                record = _iFormQuestionsvc.Insert(record);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormQuestion.Created"));
                
                if (continueEditing)
                    return RedirectToAction("EditFormQuestion", new { id = record.Id });

                return RedirectToAction("EditForm", new  { id = model.Form_id, SelectedTab = "2" });
            }
            else
            {
                FillDropDownFormQuestion(model);
                return View(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }
        
        public ActionResult EditFormQuestion(int id, string SelectedTab)
        {
            FormQuestion record = _iFormQuestionsvc.GetById(id);
            var model = record.ToModel();
            model.SelectedTab = SelectedTab;
            
            
            
            FillDropDownFormQuestion(model);
            
            FillDropDownFormAnswer(model.Filter_FormAnswerModel);
                       
            
            FillDropDownFormQuestionOption(model.Filter_FormQuestionOptionModel);
                       

            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormQuestion.Update")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult EditFormQuestion([Bind(Exclude = "Filter_FormAnswerModel,Filter_FormQuestionOptionModel")] FormQuestionModel model, bool continueEditing)
        {
            var item = _iFormQuestionsvc.GetById(model.Id);
            if (item == null)
                //No country found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                item = model.ToEntity(item);
                
                

                _iFormQuestionsvc.Update(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormQuestion.Updated"));
                
                if(continueEditing)
                    return RedirectToAction("EditFormQuestion", new { id = item.Id });
                
                return RedirectToAction("EditForm", new  { id = model.Form_id, SelectedTab = "2" });
            }

            //If we got this far, something failed, redisplay form
            FillDropDownFormQuestion(model);
            
            FillDropDownFormAnswer(model.Filter_FormAnswerModel);
                       
            
            FillDropDownFormQuestionOption(model.Filter_FormQuestionOptionModel);
                       
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormQuestion.Delete")]
        public ActionResult DeleteFormQuestion(int id)
        {

            var item = _iFormQuestionsvc.GetById(id);
            if (item == null)
                return RedirectToAction("List");

            try
            {
                item.FORM_QUESTIONForm_answer.ToList().ForEach(d => _iFormAnswersvc.Delete(d));
                item.FORM_QUESTIONForm_question_option.ToList().ForEach(d => _iFormQuestionOptionsvc.Delete(d));
                item.FORM_QUESTIONForm_answer.ToList().ForEach(d => _iFormAnswersvc.Delete(d));
                item.FORM_QUESTIONForm_question_option.ToList().ForEach(d => _iFormQuestionOptionsvc.Delete(d));
                
                _iFormQuestionsvc.Delete(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormQuestion.Deleted"));
                
                return RedirectToAction("EditForm", new  { id = item.Form_id, SelectedTab = "2" });
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("EditFormQuestion", new { id = item.Id });
            }
        }
        #endregion 
          



        





            
        #region  FormAnswer methods
        
        public void FillDropDownFormAnswer(FormAnswerModel model)
        {

            model.Form_question_id_FormQuestion_SelectList = new SelectList(_iFormQuestionsvc.GetAll().ToList(), "Id", "Code");
        }
        
        public ActionResult CreateOrUpdateFormAnswer(string itemId, string imageUrl, string mimeType, string uploadFilename)
        {
            FormAnswerModel response = null;


            int nItemId = -1;
            int.TryParse(itemId, out nItemId);

            if (nItemId != -1)
            {
                FormAnswer record = _iFormAnswersvc.GetById(nItemId);
                response = record.ToModel();
                if (record != null) // update
                {



                    //response.RoleListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem { Text = "Administrators" }, new Ext.Net.ListItem { Text = "Guests" } };
                    //response.PermissionListItems = permListItems.ToArray();

                }
                else // insert mode (after image upload popup close)
                {
                    response = new FormAnswerModel();
                    //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() { Text = "" } };
                }
            }
            else // insert
            {
                response = new FormAnswerModel();
                //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() {Text = "" }  };

            }


            ViewBag.itemId = nItemId;

            return View(response);
        }
        
        [AresAuthorize(ResourceKey = "Admin.FormAnswer.Read")]
        public ActionResult ListFormAnswerDatatable(Ares.Web.Razor.Extensions.Datatable.Model.DTParameterModel model, int? FormQuestionModelId)
        {
            var models = _iFormAnswersvc.GetAll();
            models = models.Where(m => m.Form_question_id == FormQuestionModelId);
            if (model.Search != null && !string.IsNullOrEmpty(model.Search.Value))
            {
                models = models.Where(m => m.Udid.Contains(model.Search.Value));
            }
            
            if (model.IsSearchValueExist("Form_question_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Form_question_id"));
                models = models.Where(m => m.Form_question_id == tmpValue);
            }
            if (model.IsSearchValueExist("Device_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Device_id"));
                models = models.Where(m => m.Device_id == tmpValue);
            }
            if (model.IsSearchValueExist("Udid"))
            {
                string tmpValue = model.GetSearchValue("Udid");
                models = models.Where(m => m.Udid.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Username"))
            {
                string tmpValue = model.GetSearchValue("Username");
                models = models.Where(m => m.Username.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Read_no"))
            {
                long tmpValue = long.Parse(model.GetSearchValue("Read_no"));
                models = models.Where(m => m.Read_no == tmpValue);
            }
            if (model.IsSearchValueExist("Answer"))
            {
                string tmpValue = model.GetSearchValue("Answer");
                models = models.Where(m => m.Answer.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Long_answer"))
            {
            }
            if (model.IsSearchValueExist("Converted_answer"))
            {
                string tmpValue = model.GetSearchValue("Converted_answer");
                models = models.Where(m => m.Converted_answer.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Description"))
            {
                string tmpValue = model.GetSearchValue("Description");
                models = models.Where(m => m.Description.Contains(tmpValue));
            }
            
            DTOrder order = null;
            if (model.Order != null && model.Order.Count() > 0)
            {
                order = model.Order.First();
                var pi = typeof(FormAnswer).GetProperty(model.Columns.ToList()[order.Column].Data);
                if(order.Dir == "asc")
                    models = models.OrderBy(x => pi.GetValue(x, null));
                else
                    models = models.OrderByDescending(x => pi.GetValue(x, null));
                
            }

            var totalRecords = models.Count();
            
            models = models.OrderBy(e => e.Id).Skip(model.Length * ((int)(Math.Ceiling((double)model.Start / model.Length)) + 1 - 1)).Take(model.Length);

            JsonResult jsonRes = Json(new
            {
                sEcho = model.Draw,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                //aaData = result
                aaData = models.ToList().Select(x => { var m = x.ToModel(); m.Form_question_id_Text = x.ANSWER_QUESTION == null ? "" : x.ANSWER_QUESTION.Code;  return m;})
            }, JsonRequestBehavior.AllowGet);
            return jsonRes;
        }
        
        [AresAuthorize(ResourceKey = "Admin.FormAnswer.Create")]
        public ActionResult CreateFormAnswer(int FormQuestionModelId)
        {
            FormAnswerModel model = new FormAnswerModel();
            model.Form_question_id = FormQuestionModelId;
            
            
            FillDropDownFormAnswer(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormAnswer.Create")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult CreateFormAnswer( FormAnswerModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {                
                var record = model.ToEntity();
                
                
                record = _iFormAnswersvc.Insert(record);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormAnswer.Created"));
                
                if (continueEditing)
                    return RedirectToAction("EditFormAnswer", new { id = record.Id });

                return RedirectToAction("EditFormQuestion", new  { id = model.Form_question_id, SelectedTab = "2" });
            }
            else
            {
                FillDropDownFormAnswer(model);
                return View(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }
        
        public ActionResult EditFormAnswer(int id, string SelectedTab)
        {
            FormAnswer record = _iFormAnswersvc.GetById(id);
            var model = record.ToModel();
            model.SelectedTab = SelectedTab;
            
            
            
            FillDropDownFormAnswer(model);

            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormAnswer.Update")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult EditFormAnswer( FormAnswerModel model, bool continueEditing)
        {
            var item = _iFormAnswersvc.GetById(model.Id);
            if (item == null)
                //No country found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                item = model.ToEntity(item);
                
                

                _iFormAnswersvc.Update(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormAnswer.Updated"));
                
                if(continueEditing)
                    return RedirectToAction("EditFormAnswer", new { id = item.Id });
                
                return RedirectToAction("EditFormQuestion", new  { id = model.Form_question_id, SelectedTab = "2" });
            }

            //If we got this far, something failed, redisplay form
            FillDropDownFormAnswer(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormAnswer.Delete")]
        public ActionResult DeleteFormAnswer(int id)
        {

            var item = _iFormAnswersvc.GetById(id);
            if (item == null)
                return RedirectToAction("List");

            try
            {
                
                _iFormAnswersvc.Delete(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormAnswer.Deleted"));
                
                return RedirectToAction("EditFormQuestion", new  { id = item.Form_question_id, SelectedTab = "2" });
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("EditFormAnswer", new { id = item.Id });
            }
        }
        #endregion 
          



        





            
        #region  FormQuestionOption methods
        
        public void FillDropDownFormQuestionOption(FormQuestionOptionModel model)
        {

            model.Derived_id_FormQuestionOption_SelectList = new SelectList(_iFormQuestionOptionsvc.GetAll().ToList(), "Id", "Code");

            model.Form_question_id_FormQuestion_SelectList = new SelectList(_iFormQuestionsvc.GetAll().ToList(), "Id", "Code");
        }
        
        public ActionResult CreateOrUpdateFormQuestionOption(string itemId, string imageUrl, string mimeType, string uploadFilename)
        {
            FormQuestionOptionModel response = null;


            int nItemId = -1;
            int.TryParse(itemId, out nItemId);

            if (nItemId != -1)
            {
                FormQuestionOption record = _iFormQuestionOptionsvc.GetById(nItemId);
                response = record.ToModel();
                if (record != null) // update
                {



                    //response.RoleListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem { Text = "Administrators" }, new Ext.Net.ListItem { Text = "Guests" } };
                    //response.PermissionListItems = permListItems.ToArray();

                }
                else // insert mode (after image upload popup close)
                {
                    response = new FormQuestionOptionModel();
                    //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() { Text = "" } };
                }
            }
            else // insert
            {
                response = new FormQuestionOptionModel();
                //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() {Text = "" }  };

            }


            ViewBag.itemId = nItemId;

            return View(response);
        }
        
        [AresAuthorize(ResourceKey = "Admin.FormQuestionOption.Read")]
        public ActionResult ListFormQuestionOptionDatatable(Ares.Web.Razor.Extensions.Datatable.Model.DTParameterModel model, int? FormQuestionModelId)
        {
            var models = _iFormQuestionOptionsvc.GetAll();
            models = models.Where(m => m.Form_question_id == FormQuestionModelId);
            if (model.Search != null && !string.IsNullOrEmpty(model.Search.Value))
            {
                models = models.Where(m => m.Code.Contains(model.Search.Value));
            }
            
            if (model.IsSearchValueExist("Form_question_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Form_question_id"));
                models = models.Where(m => m.Form_question_id == tmpValue);
            }
            if (model.IsSearchValueExist("Derived_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Derived_id"));
                models = models.Where(m => m.Derived_id == tmpValue);
            }
            if (model.IsSearchValueExist("Code"))
            {
                string tmpValue = model.GetSearchValue("Code");
                models = models.Where(m => m.Code.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Title"))
            {
                string tmpValue = model.GetSearchValue("Title");
                models = models.Where(m => m.Title.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Hint"))
            {
                string tmpValue = model.GetSearchValue("Hint");
                models = models.Where(m => m.Hint.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Description"))
            {
                string tmpValue = model.GetSearchValue("Description");
                models = models.Where(m => m.Description.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Version"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Version"));
                models = models.Where(m => m.Version == tmpValue);
            }
            if (model.IsSearchValueExist("Image_path"))
            {
                string tmpValue = model.GetSearchValue("Image_path");
                models = models.Where(m => m.Image_path.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Device_image_path"))
            {
                string tmpValue = model.GetSearchValue("Device_image_path");
                models = models.Where(m => m.Device_image_path.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Estimated_time"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Estimated_time"));
                models = models.Where(m => m.Estimated_time == tmpValue);
            }
            if (model.IsSearchValueExist("Video_path"))
            {
                string tmpValue = model.GetSearchValue("Video_path");
                models = models.Where(m => m.Video_path.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Device_video_path"))
            {
                string tmpValue = model.GetSearchValue("Device_video_path");
                models = models.Where(m => m.Device_video_path.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Method"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Method"));
                models = models.Where(m => m.Method == tmpValue);
            }
            if (model.IsSearchValueExist("Display_order"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Display_order"));
                models = models.Where(m => m.Display_order == tmpValue);
            }
            if (model.IsSearchValueExist("Period"))
            {
                string tmpValue = model.GetSearchValue("Period");
                models = models.Where(m => m.Period.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Active"))
            {
                bool tmpValue = bool.Parse(model.GetSearchValue("Active"));
                models = models.Where(m => m.Active == tmpValue);
            }
            if (model.IsSearchValueExist("Deleted"))
            {
                bool tmpValue = bool.Parse(model.GetSearchValue("Deleted"));
                models = models.Where(m => m.Deleted == tmpValue);
            }
            
            DTOrder order = null;
            if (model.Order != null && model.Order.Count() > 0)
            {
                order = model.Order.First();
                var pi = typeof(FormQuestionOption).GetProperty(model.Columns.ToList()[order.Column].Data);
                if(order.Dir == "asc")
                    models = models.OrderBy(x => pi.GetValue(x, null));
                else
                    models = models.OrderByDescending(x => pi.GetValue(x, null));
                
            }

            var totalRecords = models.Count();
            
            models = models.OrderBy(e => e.Id).Skip(model.Length * ((int)(Math.Ceiling((double)model.Start / model.Length)) + 1 - 1)).Take(model.Length);

            JsonResult jsonRes = Json(new
            {
                sEcho = model.Draw,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                //aaData = result
                aaData = models.ToList().Select(x => { var m = x.ToModel(); m.Derived_id_Text = x.QUESTION_OPTION_DERIVED == null ? "" : x.QUESTION_OPTION_DERIVED.Code; m.Form_question_id_Text = x.QUESTION_OPTION_QUESTION == null ? "" : x.QUESTION_OPTION_QUESTION.Code;  return m;})
            }, JsonRequestBehavior.AllowGet);
            return jsonRes;
        }
        
        [AresAuthorize(ResourceKey = "Admin.FormQuestionOption.Create")]
        public ActionResult CreateFormQuestionOption(int FormQuestionModelId)
        {
            FormQuestionOptionModel model = new FormQuestionOptionModel();
            model.Form_question_id = FormQuestionModelId;
            
            
            FillDropDownFormQuestionOption(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormQuestionOption.Create")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult CreateFormQuestionOption( FormQuestionOptionModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {                
                var record = model.ToEntity();
                
                
                record = _iFormQuestionOptionsvc.Insert(record);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormQuestionOption.Created"));
                
                if (continueEditing)
                    return RedirectToAction("EditFormQuestionOption", new { id = record.Id });

                return RedirectToAction("EditFormQuestion", new  { id = model.Form_question_id, SelectedTab = "3" });
            }
            else
            {
                FillDropDownFormQuestionOption(model);
                return View(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }
        
        public ActionResult EditFormQuestionOption(int id, string SelectedTab)
        {
            FormQuestionOption record = _iFormQuestionOptionsvc.GetById(id);
            var model = record.ToModel();
            model.SelectedTab = SelectedTab;
            
            
            
            FillDropDownFormQuestionOption(model);

            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormQuestionOption.Update")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult EditFormQuestionOption( FormQuestionOptionModel model, bool continueEditing)
        {
            var item = _iFormQuestionOptionsvc.GetById(model.Id);
            if (item == null)
                //No country found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                item = model.ToEntity(item);
                
                

                _iFormQuestionOptionsvc.Update(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormQuestionOption.Updated"));
                
                if(continueEditing)
                    return RedirectToAction("EditFormQuestionOption", new { id = item.Id });
                
                return RedirectToAction("EditFormQuestion", new  { id = model.Form_question_id, SelectedTab = "3" });
            }

            //If we got this far, something failed, redisplay form
            FillDropDownFormQuestionOption(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormQuestionOption.Delete")]
        public ActionResult DeleteFormQuestionOption(int id)
        {

            var item = _iFormQuestionOptionsvc.GetById(id);
            if (item == null)
                return RedirectToAction("List");

            try
            {
                
                _iFormQuestionOptionsvc.Delete(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormQuestionOption.Deleted"));
                
                return RedirectToAction("EditFormQuestion", new  { id = item.Form_question_id, SelectedTab = "3" });
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("EditFormQuestionOption", new { id = item.Id });
            }
        }
        #endregion 
          



        


        

    }
}

