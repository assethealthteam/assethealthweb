﻿



using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.IO;
using System.Web.Http;
using System.Net;
using System.Web.Http.ModelBinding;
using System.Web.Security;
using Ext.Net.MVC;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using System.Web.Mvc;
using System.Linq;
using Ares.UMS.Svc.Directory;
using Ares.UMS.DTO.Users;
using Ares.UMS.Svc.Users;
using Ares.Web.Svc.Authentication;
using Ares.Web.Svc.Web;
using Ares.UMS.Svc.Common;
using Ares.UMS.DTO.Messages;
using Ares.UMS.Svc.Messages;
using Ares.Core.Helpers;
using Ares.UMS.DTO.Localization;
using Ares.UMS.Svc.Localization;
using Ares.UMS.DTO.Media;
using Ares.UMS.Svc.Media;
using Ares.Web.Models.Role;
using Ares.UMS.Svc.Security;
using Ares.UMS.DTO.Security;
using Ares.Web.Helpers;
using Ares.Web.Attributes;
using Ares.Web.Controllers;
using Ares.UMS.Svc.Configuration;
using Ares.UMS.DTO.Configuration;
using Ares.UMS.Svc;
using Ares.Web.Razor.Extensions.Datatable.Model;
using AssetHealth.Extensions;
using AssetHealth.Models.User;
using AssetHealth.Common.Svc;
using AssetHealth.Common.DTO;
using AssetHealth.Models.Configuration;
using AssetHealth.Models.Form_answer;
using AssetHealth.Models.Synchronization_detail;
using AssetHealth.Models.Synchronization;
using AssetHealth.Models.User;
using AssetHealth.Models.Eq_channel;
using AssetHealth.Models.Eq_threshold;
using AssetHealth.Models.Eq_warning;
using AssetHealth.Models.Eq_location;
using AssetHealth.Models.Equipment;
using AssetHealth.Models.Form_question;
using AssetHealth.Models.Form_question_option;
using Ares.Sync.Svc;
using System.Linq.Expressions;
using AssetHealth.Common.DTO.Extensions;
using Ares.Web.Svc.Media;
using Ares.Sync.DTO;
using AssetHealth.Mapper;

namespace AssetHealth.Contollers
{
	
    public partial class FormAnswerController : BaseAresController
	{
		#region Fields

        private readonly IFormAnswerSvc _iFormAnswersvc;
        private readonly FileUploadHelper _iFileUploadHelper;

        private readonly IFilesSvc _iFilesSvc;
        private readonly IUserSvc _iUsersvc;
        private readonly IDeviceSvc _iDeviceSvc;
        private readonly IEqChannelSvc _iEqChannelsvc;
        private readonly IEqThresholdSvc _iEqThresholdsvc;
        private readonly IEqWarningSvc _iEqWarningsvc;
        private readonly IEqLocationSvc _iEqLocationsvc;
        private readonly IEquipmentSvc _iEquipmentsvc;
        private readonly IParameterItemSvc _iParameterItemsvc;
        private readonly IFormQuestionSvc _iFormQuestionsvc;
        private readonly IFormQuestionOptionSvc _iFormQuestionOptionsvc;
        
        private readonly IStateProvinceSvc _stateProvinceSvc;
        private readonly ISettingSvc _settingSvc;
        private readonly FileSettings _fileSettings;
        
        private readonly IGenericAttributeSvc _genericAttributeSvc;
        private readonly ILocalizationSvc _localizationSvc;
        private readonly IPictureSvc _pictureSvc;
        private readonly IUserSvc _userSvc;
        private readonly IRoleSvc _roleSvc;
        private readonly IPermissionSvc _permissionSvc;
        private readonly IMenuSvc _menuSvc;
        private readonly IWorkContext _workContext;
        private readonly IUserRegistrationSvc _userRegistrationSvc;
        private readonly IAuthenticationSvc _authenticationSvc;
        private readonly IWorkflowMessageSvc _workflowMessageSvc;
        private readonly INewsLetterSubscriptionSvc _newsLetterSubscriptionSvc;
        private readonly IWebHelper _webHelper;
        private readonly UserSettings _userSettings;
        private readonly LocalizationSettings _localizationSettings;
        private readonly MediaSettings _mediaSettings;

	    #endregion

		#region Constructors

        public FormAnswerController(IFormAnswerSvc FormAnswersvc, IFilesSvc iFilesSvc,
            FileUploadHelper iFileUploadHelper,
        IUserSvc Usersvc,
        IEqChannelSvc EqChannelsvc,
        IEqThresholdSvc EqThresholdsvc,
        IEqWarningSvc EqWarningsvc,
        IEqLocationSvc EqLocationsvc,
        IEquipmentSvc Equipmentsvc,
        IParameterItemSvc ParameterItemsvc,
        IFormQuestionSvc FormQuestionsvc,
        IDeviceSvc iDeviceSvc,
        IFormQuestionOptionSvc FormQuestionOptionsvc,
            IStateProvinceSvc stateProvinceSvc, IGenericAttributeSvc genericAttributeSvc, ILocalizationSvc localizationSvc,  
            UserSettings userSettings, IPictureSvc pictureSvc, ISettingSvc settingSvc, FileSettings fileSettings,
            IUserSvc userSvc, IRoleSvc roleSvc, IPermissionSvc permissionSvc, IMenuSvc menuSvc, IWorkContext workContext, IUserRegistrationSvc userRegistrationSvc, IAuthenticationSvc authenticationSvc, IWorkflowMessageSvc workflowMessageSvc,
            INewsLetterSubscriptionSvc newsLetterSubscriptionSvc, IWebHelper webHelper, LocalizationSettings localizationSettings, MediaSettings mediaSettings)
		{
            this._iFormAnswersvc = FormAnswersvc;
            this._iFileUploadHelper = iFileUploadHelper;

            this._iFilesSvc = iFilesSvc;
            this._iUsersvc = Usersvc;
            this._iEqChannelsvc = EqChannelsvc;
            this._iEqThresholdsvc = EqThresholdsvc;
            this._iEqWarningsvc = EqWarningsvc;
            this._iEqLocationsvc = EqLocationsvc;
            this._iEquipmentsvc = Equipmentsvc;
            this._iParameterItemsvc = ParameterItemsvc;
            this._iFormQuestionsvc = FormQuestionsvc;
            this._iDeviceSvc = iDeviceSvc;
            this._iFormQuestionOptionsvc = FormQuestionOptionsvc;
            this._settingSvc = settingSvc;
            this._fileSettings = fileSettings;
            
            this._stateProvinceSvc = stateProvinceSvc;
            this._genericAttributeSvc = genericAttributeSvc;
            this._pictureSvc = pictureSvc;
            this._localizationSvc = localizationSvc;
            this._userSettings = userSettings;
            this._userSvc = userSvc;
            this._roleSvc = roleSvc;
            this._permissionSvc = permissionSvc;
            this._menuSvc = menuSvc;
            this._workContext = workContext;
            this._userRegistrationSvc = userRegistrationSvc;
            this._authenticationSvc = authenticationSvc;
            this._workflowMessageSvc = workflowMessageSvc;
            this._newsLetterSubscriptionSvc = newsLetterSubscriptionSvc;
            this._webHelper = webHelper;
            this._localizationSettings = localizationSettings;
            this._mediaSettings = mediaSettings;
		}

		#endregion 

        #region Utilities
        
        
        #endregion

        #region FormAnswer methods

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        [AresAuthorize(ResourceKey = "Admin.FormAnswer.Read")]
        public ActionResult List()
        {
            FormAnswerModel model = new FormAnswerModel();
            //model. = _roleSvc.GetAll().ToList().Select(x => x.ToModel()).ToList();
            FillDropDowns(model);


            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormAnswer.Read")]
        public ActionResult ListFormAnswerDatatable(Ares.Web.Razor.Extensions.Datatable.Model.DTParameterModel model)
        {
            var models = _iFormAnswersvc.GetAll();

            if (!_workContext.CurrentUser.IsAdmin())
            {
                List<int> relatedLocationIds = _workContext.CurrentUser.RelatedLocations().Select(r => r.Id).ToList();
                models = models.Where(e => relatedLocationIds.Contains(e.ANSWER_QUESTION.QUESTION_FORM.EQUIPMENT.Location.Id));
            }

            if (model.Search != null && !string.IsNullOrEmpty(model.Search.Value))
            {
                models = models.Where(m => m.Udid.Contains(model.Search.Value));
            }

            if (model.IsSearchValueExist("Filter_EqLocation_Text"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Filter_EqLocation_Text"));
                models = models.Where(m => m.ANSWER_QUESTION.QUESTION_FORM.EQUIPMENT.Eq_location_id == tmpValue);
            }
            if (model.IsSearchValueExist("Role_id_Text"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Role_id_Text"));
                models = models.Where(m => m.ANSWER_QUESTION.QUESTION_FORM.Role_id == tmpValue);
            }

            if (model.IsSearchValueExist("Form_question_id_Text"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Form_question_id_Text"));
                models = models.Where(m => m.Form_question_id == tmpValue);
            }
            if (model.IsSearchValueExist("Device_id_Text"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Device_id_Text"));
                models = models.Where(m => m.Device_id == tmpValue);
            }
            if (model.IsSearchValueExist("Udid"))
            {
                string tmpValue = model.GetSearchValue("Udid");
                models = models.Where(m => m.Udid.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Username"))
            {
                string tmpValue = model.GetSearchValue("Username");
                models = models.Where(m => m.Username.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Read_no"))
            {
                /*
                long tmpValue = long.Parse(model.GetSearchValue("Read_no"));
                models = models.Where(m => m.Read_no == tmpValue);
                */
                DateTime dtStart = DateTime.Now;
                DateTime.TryParseExact(model.GetSearchValue("Read_no"), "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtStart);
                long startDateTime = StringHelper.ConvertDateTimeToLong(dtStart);
                DateTime dtEnd = new DateTime(dtStart.Year, dtStart.Month, dtStart.Day, 23, 59, 59);
                long endDateTime = StringHelper.ConvertDateTimeToLong(dtEnd);

                models = models.Where(m => m.Read_no >= startDateTime && m.Read_no <= endDateTime);
            }
            if (model.IsSearchValueExist("Answer"))
            {
                string tmpValue = model.GetSearchValue("Answer");
                models = models.Where(m => m.Answer.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Long_answer"))
            {
            }
            if (model.IsSearchValueExist("Converted_answer"))
            {
                string tmpValue = model.GetSearchValue("Converted_answer");
                models = models.Where(m => m.Converted_answer.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Description"))
            {
                string tmpValue = model.GetSearchValue("Description");
                models = models.Where(m => m.Description.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Warning_type_Text"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Warning_type_Text"));
                models = models.Where(m => m.Warning_type == tmpValue);
            }


            /*
             // Aşağıdaki kısım left outer join'e örnek olduğundan silme
            var models2 = models.GroupJoin(_iFilesSvc.GetAll().Where(f => f.Entity_name == "Formanswer"),
                      a => a.Id,
                     d => d.Entity_id,
                     (a, d) => new { Answer = a, FileCol = d.DefaultIfEmpty(new Files()) })
                     //.SelectMany(a => a.Files , (a, f) => new { Answer = a.Answer, Question = "", Device = "", HasMedia = f.Id > 0 } );
                     .SelectMany(a => a.FileCol.Select(f => new { Answer = a.Answer, Question = "", Device = "", HasMedia = f.Id > 0 }));
           
            
                        var models2 = from a in models
                                      join f in _iFilesSvc.GetAll().Where(f => f.Entity_name == "Formanswer") on a.Id equals f.Entity_id into joinedT
                                      from pd in joinedT.DefaultIfEmpty()
                                      select new { Answer = a, Question = "", Device = "", HasMedia = pd != null }; // left outer join
                

            //Func<FormAnswer, Files, FormAnswer> UpdateHasMedia = ((a, b) => { if (b != null && b.Id > 0) { a.HasMedia = true; } return a; });  select UpdateHasMedia(a, pd); 
            */



            var models2 = models.Join(_iFormQuestionsvc.GetAll(),
                                a => a.Form_question_id,
                                q => q.Id,
                                (a, q) => new { Answer = a, Question = (q.Code != null ? q.Code + " " : "") + q.Title })
                                .Join(_iDeviceSvc.GetAll(),
                                 a => a.Answer.Device_id,
                                d => d.Id,
                                (a, d) => new { Answer = a.Answer, Question = a.Question, Device = d.Device_name });




            if (model.IsSearchValueExist("Filter_OnlyWithMedias"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Filter_OnlyWithMedias"));

                if(tmpValue == 1)
                {
                    models2 = models2.Join(_iFilesSvc.GetAll().Where(f => f.Entity_name == "Formanswer"),
                   a => a.Answer.Id,
                   q => q.Entity_id,
                   (a, q) => new { Answer = a.Answer, Question = a.Question, Device = a.Device });

                    models2 = models2.Distinct();
                }
                
            }

            DTOrder order = null;
            if (model.Order != null && model.Order.Count() > 0)
            {
                order = model.Order.First();
                if (order.Dir == "asc")
                    models2 = models2.OrderBy("Answer." + model.Columns.ToList()[order.Column].Name ,"asc");
                else
                    models2 = models2.OrderBy("Answer." + model.Columns.ToList()[order.Column].Name, "desc");

            }
            else
            {
                models2 = models2.OrderByDescending(i => i.Answer.Id);
            }

            var totalRecords = models2.Count();
            models2 = models2.Skip(model.Length * ((int)(Math.Ceiling((double)model.Start / model.Length)) + 1 - 1)).Take(model.Length);



            List<FormAnswerModel> aaData = models2.ToList().Select(m => { var item = m.Answer.ToModel(ListMapperConfiguration.CONFIGURATION_NAME); item.Form_question_id_Text = m.Question; item.Device_id_Text = m.Device; return item; }).ToList();

            //List <FormAnswerModel> aaData = models.ToList().Select(x => { var m = x.ToModel(); m.Form_question_id_Text = x.ANSWER_QUESTION == null ? "" : (x.ANSWER_QUESTION.Code != null ? x.ANSWER_QUESTION.Code + " " : "") + x.ANSWER_QUESTION.Title; /* m.Device_id_Text = x.Device_id == null ? "" : _iDeviceSvc.GetById(x.Device_id).Device_name*/; return m; }).ToList();

            List<ImageGallery> galleries = _iFileUploadHelper.GenerateImageGalleryJSON("Formanswer", aaData.Select(a => a.Id).ToList(), false);

            if (galleries != null && galleries.Count > 0)
                aaData.ForEach(a => a.ImageGallery = galleries.FirstOrDefault(g => g.entityId == a.Id));
                


            JsonResult jsonRes = Json(new
            {
                sEcho = model.Draw,
                iTotalRecords = totalRecords,
                //iTotalDisplayRecords = model.Length > 0 ? model.Length : totalRecords,
                iTotalDisplayRecords = totalRecords,
                //aaData = result
                aaData = aaData
            }, JsonRequestBehavior.AllowGet);

            jsonRes.MaxJsonLength = 500000000;
            return jsonRes;
        }
        
        [AresAuthorize(ResourceKey = "Admin.FormAnswer.Create")]
        public ActionResult Create()
        {
            FormAnswerModel model = new FormAnswerModel();
            FillDropDowns(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormAnswer.Create")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Create( FormAnswerModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {                
                var record = model.ToEntity();
                
            
                record = _iFormAnswersvc.Insert(record);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormAnswer.Created"));
                
                if (continueEditing)
                    return RedirectToAction("Edit", new { Id = record.Id });

                return RedirectToAction("List");
            }
            else
            {
                FillDropDowns(model);
                return View(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormAnswer.Update")]
        [FormAnswerAuthorize]
        public ActionResult Edit(int id, string SelectedTab)
        {
            FormAnswer record = _iFormAnswersvc.GetById(id);
            var model = record.ToModel();
            model.SelectedTab = SelectedTab;
                        
            FillDropDowns(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormAnswer.Update")]
        [FormAnswerAuthorize]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Edit( FormAnswerModel model, bool continueEditing)
        {
            var item = _iFormAnswersvc.GetById(model.Id);
            FormAnswer originalItem = (FormAnswer)item.Clone();
            if (item == null)
                //No country found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                item = model.ToEntity(item);
                item.Object_id = originalItem.Object_id;
                item.Insert_user_id = originalItem.Insert_user_id;
                item.Insert_datetime = originalItem.Insert_datetime;


                _iFormAnswersvc.Update(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormAnswer.Updated"));
                
                if(continueEditing)
                    return RedirectToAction("Edit", new { Id = item.Id });

                return RedirectToAction("List");
            }
            else
            {
                FillDropDowns(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormAnswer.Delete")]
        [FormAnswerAuthorize]
        public ActionResult Delete(int id)
        {

            var item = _iFormAnswersvc.GetById(id);
            if (item == null)
                return RedirectToAction("List");

            try
            {
                
                _iFormAnswersvc.Delete(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormAnswer.Deleted"));
                return RedirectToAction("List");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("Edit", new { id = item.Id });
            }
        }
        
        
        public ActionResult CreateOrUpdate(string itemId, string imageUrl, string mimeType, string uploadFilename)
        {
            FormAnswerModel response = null;
            

            int nItemId = -1;
            int.TryParse(itemId, out nItemId);

            if (nItemId != -1)
            {
                FormAnswer record = _iFormAnswersvc.GetById(nItemId);
                response = record.ToModel();
                if (record != null) // update
                {

                   

                }
                else // insert mode (after image upload popup close)
                {
                    response = new FormAnswerModel();
                    //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() { Text = "" } };
                }
            }
            else // insert
            {
                response = new FormAnswerModel();
                //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() {Text = "" }  };
                
            }

 
            ViewBag.itemId = nItemId;

            return View(response);
        }
        
        
        public void FillDropDowns(FormAnswerModel model)
        {
            IQueryable<FormQuestion> relatedQuestions = _iFormQuestionsvc.GetAll();
            if (!_workContext.CurrentUser.IsAdmin())
            {
                List<int> relatedLocationIds = _workContext.CurrentUser.RelatedLocations().Select(r => r.Id).ToList();
                relatedQuestions = relatedQuestions.Where(e => relatedLocationIds.Contains(e.QUESTION_FORM.EQUIPMENT.Location.Id));
            }

            model.Filter_EqLocation_SelectList = new SelectList(_iEqLocationsvc.GetAuthorizedLocations(_workContext.CurrentUser.Id), "Id", "Location_name");

            model.Device_id_Device_SelectList = new SelectList(_iDeviceSvc.GetAllFromCache().ToList(), "Id", "Device_name");
            model.Form_question_id_FormQuestion_SelectList = new SelectList(relatedQuestions.ToList().Select(p => { return new { Id = p.Id, Title = (!string.IsNullOrEmpty(p.Code) ? p.Code + " - " : "" ) + p.Title }; }), "Id", "Title");
            model.Warning_type_ParameterItem_SelectList = new SelectList(_iParameterItemsvc.GetAllFromCache().Where(p => p.Group.System_name == "EQ_WARNING_WARNING_TYPE").ToList().Select(p => { return new { Id = p.Id, Title = p.GetLocalized(l => l.Title) }; }), "Id", "Title");

            List<SelectListItem> yesNoSelectList = new List<SelectListItem>();
            yesNoSelectList.Add(new SelectListItem() {  Text = _localizationSvc.GetResource("select") });
            yesNoSelectList.Add(new SelectListItem() { Value = "1", Text = _localizationSvc.GetResource("Admin.Common.Yes") });
            yesNoSelectList.Add(new SelectListItem() { Value = "2", Text = _localizationSvc.GetResource("Admin.Common.No") });

            model.YesNo_SelectList = new SelectList(yesNoSelectList, "Value", "Text");

        }
        
        #endregion
                         
        

    }


        public static class LinqExtensions
        {

            public static IOrderedQueryable<TSource> OrderBy<TSource>(this IQueryable<TSource> source, string field, string dir = "asc")
            {

                // parametro => expressão
                var parametro = Expression.Parameter(typeof(TSource), "r");
                try
                {
                    field = field.EndsWith("_Text") ? field.Substring(0, field.IndexOf("_Text")) : field;
                    // Aşağıdaki kısım . ile ayrılan nested obje order'ları için
                    var expressao = field.Split('.').Aggregate((Expression)parametro, Expression.Property);
                    //var expressao = Expression.Property(parametro, field);
                    if (expressao.Type == typeof(int))
                    {
                        var lambda = Expression.Lambda<Func<TSource, int>>(expressao, parametro); // r => r.AlgumaCoisa
                        if (string.Equals(dir, "desc", StringComparison.InvariantCultureIgnoreCase))
                        {
                            return source.OrderByDescending(lambda);
                        }
                        return source.OrderBy(lambda);
                    }
                else if (expressao.Type == typeof(int?))
                {
                    var lambda = Expression.Lambda<Func<TSource, int?>>(expressao, parametro); // r => r.AlgumaCoisa
                    if (string.Equals(dir, "desc", StringComparison.InvariantCultureIgnoreCase))
                    {
                        return source.OrderByDescending(lambda);
                    }
                    return source.OrderBy(lambda);
                }
                else if (expressao.Type == typeof(long))
                    {
                    Type temp = expressao.Type;
                        var lambda = Expression.Lambda<Func<TSource, long>>(expressao, parametro); // r => r.AlgumaCoisa
                        if (string.Equals(dir, "desc", StringComparison.InvariantCultureIgnoreCase))
                        {
                            return source.OrderByDescending(lambda);
                        }
                        return source.OrderBy(lambda);
                    }
                else if (expressao.Type == typeof(long?) )
                {
                    Type temp = expressao.Type;
                    var lambda = Expression.Lambda<Func<TSource, long?>>(expressao, parametro); // r => r.AlgumaCoisa
                    if (string.Equals(dir, "desc", StringComparison.InvariantCultureIgnoreCase))
                    {
                        return source.OrderByDescending(lambda);
                    }
                    return source.OrderBy(lambda);
                }
                else if (expressao.Type == typeof(DateTime))
                {
                    Type temp = expressao.Type;
                    var lambda = Expression.Lambda<Func<TSource, DateTime>>(expressao, parametro); // r => r.AlgumaCoisa
                    if (string.Equals(dir, "desc", StringComparison.InvariantCultureIgnoreCase))
                    {
                        return source.OrderByDescending(lambda);
                    }
                    return source.OrderBy(lambda);
                }
                else if (expressao.Type == typeof(DateTime?))
                {
                    Type temp = expressao.Type;
                    var lambda = Expression.Lambda<Func<TSource, DateTime?>>(expressao, parametro); // r => r.AlgumaCoisa
                    if (string.Equals(dir, "desc", StringComparison.InvariantCultureIgnoreCase))
                    {
                        return source.OrderByDescending(lambda);
                    }
                    return source.OrderBy(lambda);
                }
                else
                    {
                        var lambda = Expression.Lambda<Func<TSource, string>>(expressao, parametro); // r => r.AlgumaCoisa
                        if (string.Equals(dir, "desc", StringComparison.InvariantCultureIgnoreCase))
                        {
                            return source.OrderByDescending(lambda);
                        }
                        return source.OrderBy(lambda);
                    }
                }
                catch(Exception exp)
                {

                }
                var expressao2 = Expression.Property(parametro, "Id");
                var lambda2 = Expression.Lambda<Func<TSource, int>>(expressao2, parametro); // r => r.AlgumaCoisa
                return source.OrderBy(lambda2);
            }

            public static IOrderedQueryable<TSource> ThenBy<TSource>(this IOrderedQueryable<TSource> source, string field, string dir = "asc")
            {
                var parametro = Expression.Parameter(typeof(TSource), "r");
                var expressao = Expression.Property(parametro, field);
                var lambda = Expression.Lambda<Func<TSource, string>>(expressao, parametro); // r => r.AlgumaCoisa
                if (string.Equals(dir, "desc", StringComparison.InvariantCultureIgnoreCase))
                {
                    return source.ThenByDescending(lambda);
                }
                return source.ThenBy(lambda);
            }


        public static Expression<Func<TEntity, TResult>> GetExpression<TEntity, TResult>(string prop)
        {
            var param = Expression.Parameter(typeof(TEntity), "p");
            var parts = prop.Split('.');

            Expression parent = parts.Aggregate<string, Expression>(param, Expression.Property);
            Expression conversion = Expression.Convert(parent, typeof(object));

            var tryExpression = Expression.TryCatch(Expression.Block(typeof(object), conversion),
                                                    Expression.Catch(typeof(object), Expression.Constant(null)));

            return Expression.Lambda<Func<TEntity, TResult>>(tryExpression, param);
        }

    }




}

