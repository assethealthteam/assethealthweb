﻿



using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.IO;
using System.Web.Http;
using System.Net;
using System.Web.Http.ModelBinding;
using System.Web.Security;
using Ext.Net.MVC;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using System.Web.Mvc;
using System.Linq;
using Ares.UMS.Svc.Directory;
using Ares.UMS.DTO.Users;
using Ares.UMS.Svc.Users;
using Ares.Web.Svc.Authentication;
using Ares.Web.Svc.Web;
using Ares.UMS.Svc.Common;
using Ares.UMS.DTO.Messages;
using Ares.UMS.Svc.Messages;
using Ares.Core.Helpers;
using Ares.UMS.DTO.Localization;
using Ares.UMS.Svc.Localization;
using Ares.UMS.DTO.Media;
using Ares.UMS.Svc.Media;
using Ares.Web.Models.Role;
using Ares.UMS.Svc.Security;
using Ares.UMS.DTO.Security;
using Ares.Web.Helpers;
using Ares.Web.Attributes;
using Ares.Web.Controllers;
using Ares.UMS.Svc.Configuration;
using Ares.UMS.DTO.Configuration;
using Ares.UMS.Svc;
using Ares.Web.Razor.Extensions.Datatable.Model;
using AssetHealth.Extensions;
using AssetHealth.Models.User;
using AssetHealth.Common.Svc;
using AssetHealth.Common.DTO;
using AssetHealth.Models.Configuration;
using AssetHealth.Models.Synchronization_detail;
using AssetHealth.Models.User;
using AssetHealth.Models.Eq_channel;
using AssetHealth.Models.Eq_threshold;
using AssetHealth.Models.Eq_warning;
using AssetHealth.Models.Eq_location;
using AssetHealth.Models.Equipment;
using AssetHealth.Models.Form_answer;
using AssetHealth.Models.Form;
using AssetHealth.Models.Form_question;
using AssetHealth.Models.Form_question_option;
using AssetHealth.Models.Device;
using AssetHealth.Models.Synchronization;
using Ares.Sync.DTO;
using Ares.Sync.Svc;

namespace AssetHealth.Contollers
{
	
    public partial class SynchronizationDetailController : BaseAresController
	{
		#region Fields

        private readonly ISynchronizationDetailSvc _iSynchronizationDetailsvc;
        
        private readonly IUserSvc _iUsersvc;
        private readonly IEqChannelSvc _iEqChannelsvc;
        private readonly IEqThresholdSvc _iEqThresholdsvc;
        private readonly IEqWarningSvc _iEqWarningsvc;
        private readonly IEqLocationSvc _iEqLocationsvc;
        private readonly IEquipmentSvc _iEquipmentsvc;
        private readonly IFormAnswerSvc _iFormAnswersvc;
        private readonly IFormSvc _iFormsvc;
        private readonly IFormQuestionSvc _iFormQuestionsvc;
        private readonly IFormQuestionOptionSvc _iFormQuestionOptionsvc;
        private readonly IParameterGroupSvc _iParameterGroupsvc;
        private readonly IDeviceSvc _iDevicesvc;
        private readonly ISynchronizationSvc _iSynchronizationsvc;
        
        private readonly IStateProvinceSvc _stateProvinceSvc;
        private readonly ISettingSvc _settingSvc;
        private readonly FileSettings _fileSettings;
        
        private readonly IGenericAttributeSvc _genericAttributeSvc;
        private readonly ILocalizationSvc _localizationSvc;
        private readonly IPictureSvc _pictureSvc;
        private readonly IUserSvc _userSvc;
        private readonly IRoleSvc _roleSvc;
        private readonly IPermissionSvc _permissionSvc;
        private readonly IMenuSvc _menuSvc;
        private readonly IWorkContext _workContext;
        private readonly IUserRegistrationSvc _userRegistrationSvc;
        private readonly IAuthenticationSvc _authenticationSvc;
        private readonly IWorkflowMessageSvc _workflowMessageSvc;
        private readonly INewsLetterSubscriptionSvc _newsLetterSubscriptionSvc;
        private readonly IWebHelper _webHelper;
        private readonly UserSettings _userSettings;
        private readonly LocalizationSettings _localizationSettings;
        private readonly MediaSettings _mediaSettings;

	    #endregion

		#region Constructors

        public SynchronizationDetailController(ISynchronizationDetailSvc SynchronizationDetailsvc,
        IUserSvc Usersvc,
        IEqChannelSvc EqChannelsvc,
        IEqThresholdSvc EqThresholdsvc,
        IEqWarningSvc EqWarningsvc,
        IEqLocationSvc EqLocationsvc,
        IEquipmentSvc Equipmentsvc,
        IFormAnswerSvc FormAnswersvc,
        IFormSvc Formsvc,
        IFormQuestionSvc FormQuestionsvc,
        IFormQuestionOptionSvc FormQuestionOptionsvc,
        IParameterGroupSvc ParameterGroupsvc,
        IDeviceSvc Devicesvc,
        ISynchronizationSvc Synchronizationsvc,
            IStateProvinceSvc stateProvinceSvc, IGenericAttributeSvc genericAttributeSvc, ILocalizationSvc localizationSvc,  
            UserSettings userSettings, IPictureSvc pictureSvc, ISettingSvc settingSvc, FileSettings fileSettings,
            IUserSvc userSvc, IRoleSvc roleSvc, IPermissionSvc permissionSvc, IMenuSvc menuSvc, IWorkContext workContext, IUserRegistrationSvc userRegistrationSvc, IAuthenticationSvc authenticationSvc, IWorkflowMessageSvc workflowMessageSvc,
            INewsLetterSubscriptionSvc newsLetterSubscriptionSvc, IWebHelper webHelper, LocalizationSettings localizationSettings, MediaSettings mediaSettings)
		{
            this._iSynchronizationDetailsvc = SynchronizationDetailsvc;
            
            this._iUsersvc = Usersvc;
            this._iEqChannelsvc = EqChannelsvc;
            this._iEqThresholdsvc = EqThresholdsvc;
            this._iEqWarningsvc = EqWarningsvc;
            this._iEqLocationsvc = EqLocationsvc;
            this._iEquipmentsvc = Equipmentsvc;
            this._iFormAnswersvc = FormAnswersvc;
            this._iFormsvc = Formsvc;
            this._iFormQuestionsvc = FormQuestionsvc;
            this._iFormQuestionOptionsvc = FormQuestionOptionsvc;
            this._iParameterGroupsvc = ParameterGroupsvc;
            this._iDevicesvc = Devicesvc;
            this._iSynchronizationsvc = Synchronizationsvc;
            this._settingSvc = settingSvc;
            this._fileSettings = fileSettings;
            
            this._stateProvinceSvc = stateProvinceSvc;
            this._genericAttributeSvc = genericAttributeSvc;
            this._pictureSvc = pictureSvc;
            this._localizationSvc = localizationSvc;
            this._userSettings = userSettings;
            this._userSvc = userSvc;
            this._roleSvc = roleSvc;
            this._permissionSvc = permissionSvc;
            this._menuSvc = menuSvc;
            this._workContext = workContext;
            this._userRegistrationSvc = userRegistrationSvc;
            this._authenticationSvc = authenticationSvc;
            this._workflowMessageSvc = workflowMessageSvc;
            this._newsLetterSubscriptionSvc = newsLetterSubscriptionSvc;
            this._webHelper = webHelper;
            this._localizationSettings = localizationSettings;
            this._mediaSettings = mediaSettings;
		}

		#endregion 

        #region Utilities
        
        
        #endregion

        #region SynchronizationDetail methods

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        [AresAuthorize(ResourceKey = "Admin.SynchronizationDetail.Read")]
        public ActionResult List()
        {
            SynchronizationDetailModel model = new SynchronizationDetailModel();
            //model. = _roleSvc.GetAll().ToList().Select(x => x.ToModel()).ToList();
            FillDropDowns(model);
            
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.SynchronizationDetail.Read")]
        public ActionResult ListSynchronizationDetailDatatable(Ares.Web.Razor.Extensions.Datatable.Model.DTParameterModel model)
        {
            var models = _iSynchronizationDetailsvc.GetAll();
            
            if (model.Search != null && !string.IsNullOrEmpty(model.Search.Value))
            {
                models = models.Where(m => m.Table_name.Contains(model.Search.Value));
            }
            
            if(model.IsSearchValueExist("Synchronization_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Synchronization_id"));
                models = models.Where(m => m.Synchronization_id == tmpValue);
            }
            if(model.IsSearchValueExist("Table_name"))
            {
                string tmpValue = model.GetSearchValue("Table_name");
                models = models.Where(m => m.Table_name.Contains(tmpValue));
            }
            if(model.IsSearchValueExist("Server_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Server_id"));
                models = models.Where(m => m.Server_id == tmpValue);
            }
            if(model.IsSearchValueExist("Client_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Client_id"));
                models = models.Where(m => m.Client_id == tmpValue);
            }
            if(model.IsSearchValueExist("Transaction_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Transaction_id"));
                models = models.Where(m => m.Transaction_id == tmpValue);
            }
            if(model.IsSearchValueExist("Operation_type"))
            {
                
            }
            if(model.IsSearchValueExist("Result"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Result"));
                models = models.Where(m => m.Result == tmpValue);
            }
            if(model.IsSearchValueExist("Message"))
            {
            }
            if(model.IsSearchValueExist("Item"))
            {
            }
            if(model.IsSearchValueExist("Order"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Order"));
                models = models.Where(m => m.Order == tmpValue);
            }
            if(model.IsSearchValueExist("Start_datetime"))
            {
                
                
            }
            if(model.IsSearchValueExist("Description"))
            {
                string tmpValue = model.GetSearchValue("Description");
                models = models.Where(m => m.Description.Contains(tmpValue));
            }
            if(model.IsSearchValueExist("Active"))
            {
                bool tmpValue = bool.Parse(model.GetSearchValue("Active"));
                models = models.Where(m => m.Active == tmpValue);
            }
            
            
            DTOrder order = null;
            if (model.Order != null && model.Order.Count() > 0)
            {
                order = model.Order.First();
                var pi = typeof(SynchronizationDetail).GetProperty(model.Columns.ToList()[order.Column].Data);
                if(order.Dir == "asc")
                    models = models.OrderBy(x => pi.GetValue(x, null));
                else
                    models = models.OrderByDescending(x => pi.GetValue(x, null));
                
            }

            var totalRecords = models.Count();
            
            models = models.OrderBy(e => e.Id).Skip(model.Length * ((int)(Math.Ceiling((double)model.Start / model.Length)) + 1 - 1)).Take(model.Length);

            JsonResult jsonRes = Json(new
            {
                sEcho = model.Draw,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                //aaData = result
                aaData = models.ToList().Select(x => { var m = x.ToModel(); m.Synchronization_id_Text = x.SYNCHRONIZATION_DETAIL == null ? "" : x.SYNCHRONIZATION_DETAIL.Udid;  return m;})
            }, JsonRequestBehavior.AllowGet);
            return jsonRes;
        }
        
        [AresAuthorize(ResourceKey = "Admin.SynchronizationDetail.Create")]
        public ActionResult Create()
        {
            SynchronizationDetailModel model = new SynchronizationDetailModel();
            FillDropDowns(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.SynchronizationDetail.Create")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Create( SynchronizationDetailModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {                
                var record = model.ToEntity();
                
            
                record = _iSynchronizationDetailsvc.Insert(record);

                SuccessNotification(_localizationSvc.GetResource("Notifications.SynchronizationDetail.Created"));
                
                if (continueEditing)
                    return RedirectToAction("Edit", new { Id = record.Id });

                return RedirectToAction("List");
            }
            else
            {
                FillDropDowns(model);
                return View(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.SynchronizationDetail.Update")]
        public ActionResult Edit(int id, string SelectedTab)
        {
            SynchronizationDetail record = _iSynchronizationDetailsvc.GetById(id);
            var model = record.ToModel();
            model.SelectedTab = SelectedTab;
                        
            FillDropDowns(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.SynchronizationDetail.Update")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Edit( SynchronizationDetailModel model, bool continueEditing)
        {
            var item = _iSynchronizationDetailsvc.GetById(model.Id);
            if (item == null)
                //No country found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                item = model.ToEntity(item);
                
                

                _iSynchronizationDetailsvc.Update(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.SynchronizationDetail.Updated"));
                
                if(continueEditing)
                    return RedirectToAction("Edit", new { Id = item.Id });

                return RedirectToAction("List");
            }
            else
            {
                FillDropDowns(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.SynchronizationDetail.Delete")]
        public ActionResult Delete(int id)
        {

            var item = _iSynchronizationDetailsvc.GetById(id);
            if (item == null)
                return RedirectToAction("List");

            try
            {
                
                _iSynchronizationDetailsvc.Delete(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.SynchronizationDetail.Deleted"));
                return RedirectToAction("List");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("Edit", new { id = item.Id });
            }
        }
        
        
        public ActionResult CreateOrUpdate(string itemId, string imageUrl, string mimeType, string uploadFilename)
        {
            SynchronizationDetailModel response = null;
            

            int nItemId = -1;
            int.TryParse(itemId, out nItemId);

            if (nItemId != -1)
            {
                SynchronizationDetail record = _iSynchronizationDetailsvc.GetById(nItemId);
                response = record.ToModel();
                if (record != null) // update
                {

                   

                }
                else // insert mode (after image upload popup close)
                {
                    response = new SynchronizationDetailModel();
                    //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() { Text = "" } };
                }
            }
            else // insert
            {
                response = new SynchronizationDetailModel();
                //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() {Text = "" }  };
                
            }

 
            ViewBag.itemId = nItemId;

            return View(response);
        }
        
        
        public void FillDropDowns(SynchronizationDetailModel model)
        {
            

            model.Synchronization_id_Synchronization_SelectList = new SelectList(_iSynchronizationsvc.GetAll().ToList(), "Id", "Udid");
            
            
        }
        
        #endregion
                         
        

    }
}

