using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using System.Web.Mvc;
using Ares.UMS.Svc.Directory;
using System.Linq;
using Ares.UMS.DTO.Users;
using Ares.UMS.Svc.Users;
using Ares.Web.Svc.Authentication;
using Ares.Web.Svc.Web;
using Ares.UMS.Svc.Common;
using Ares.UMS.DTO.Messages;
using Ares.UMS.Svc.Messages;
using Ares.Core.Helpers;
using Ares.UMS.DTO.Localization;
using Ares.UMS.Svc.Localization;
using System.Net;
using System.Web.Security;
using Ext.Net.MVC;
using Ares.UMS.DTO.Media;
using Ares.UMS.Svc.Media;
using Ares.Web.Models.Role;
using System.IO;
using Ares.UMS.Svc.Security;
using Ares.UMS.DTO.Security;
using Ares.Web.Helpers;
using Ares.UMS.Svc;
using AssetHealth.Extensions;
using AssetHealth.Models.User;
using Ares.Web.Controllers;
using Ares.Web.Attributes;

namespace AssetHealth.Controllers
{
    public class RoleController : BaseAresController
    {
        private readonly IStateProvinceSvc _stateProvinceSvc;
        private readonly IGenericAttributeSvc _genericAttributeSvc;
        private readonly ILocalizationSvc _localizationSvc;
        private readonly IPictureSvc _pictureSvc;
        private readonly IUserSvc _userSvc;
        private readonly IRoleSvc _roleSvc;
        private readonly IPermissionSvc _permissionSvc;
        private readonly IWorkContext _workContext;
        private readonly IUserRegistrationSvc _userRegistrationSvc;
        private readonly IAuthenticationSvc _authenticationSvc;
        private readonly IWorkflowMessageSvc _workflowMessageSvc;
        private readonly INewsLetterSubscriptionSvc _newsLetterSubscriptionSvc;
        private readonly IWebHelper _webHelper;
        private readonly UserSettings _userSettings;
        private readonly LocalizationSettings _localizationSettings;
        private readonly MediaSettings _mediaSettings;

        public RoleController(IStateProvinceSvc stateProvinceSvc, IGenericAttributeSvc genericAttributeSvc, ILocalizationSvc localizationSvc,  
            UserSettings userSettings, IPictureSvc pictureSvc,
            IUserSvc userSvc, IRoleSvc roleSvc, IPermissionSvc permissionSvc, IWorkContext workContext, IUserRegistrationSvc userRegistrationSvc, IAuthenticationSvc authenticationSvc, IWorkflowMessageSvc workflowMessageSvc,
            INewsLetterSubscriptionSvc newsLetterSubscriptionSvc, IWebHelper webHelper, LocalizationSettings localizationSettings, MediaSettings mediaSettings)
        {
            this._stateProvinceSvc = stateProvinceSvc;
            this._genericAttributeSvc = genericAttributeSvc;
            this._pictureSvc = pictureSvc;
            this._localizationSvc = localizationSvc;
            this._userSettings = userSettings;
            this._userSvc = userSvc;
            this._roleSvc = roleSvc;
            this._permissionSvc = permissionSvc;
            this._workContext = workContext;
            this._userRegistrationSvc = userRegistrationSvc;
            this._authenticationSvc = authenticationSvc;
            this._workflowMessageSvc = workflowMessageSvc;
            this._newsLetterSubscriptionSvc = newsLetterSubscriptionSvc;
            this._webHelper = webHelper;
            this._localizationSettings = localizationSettings;
            this._mediaSettings = mediaSettings;

        }




        public ActionResult List()
        {
            RoleListModel model = new RoleListModel();
            model.RoleModels = _roleSvc.GetAll().ToList().Select(x => x.ToModel()).ToList();

            return View(model);

        }

        public ActionResult ListRoles(RoleListModel model)
        {
            string jsonString = JsonHelper.JsonSerializer<IList<Models.User.RoleModel>>(
            _roleSvc.GetAll().Where(u =>
            ((model.SearchRolename ?? "") == "" || u.Rolename == model.SearchRolename) &&
            ((model.SearchSystemname ?? "") == "" || u.Systemname == model.SearchSystemname)
            ).ToList().Select(x => x.ToModel()).ToList());

            var result = new { success = true, data = jsonString, Message = "" };
            JsonResult jsonRes = Json(result, JsonRequestBehavior.AllowGet);
            return jsonRes;
        }

        public ActionResult ListRolesDatatable(Ares.Web.Razor.Extensions.Datatable.Model.DTParameterModel model)
        {
            var roleModels = _roleSvc.GetAll().OrderBy(e => e.Id).Skip(model.Length * ((int)(Math.Ceiling((double)model.Start / model.Length)) + 1 - 1)).Take(model.Length).ToList().Select(x => x.ToModel()).ToList();
            string jsonString = JsonHelper.JsonSerializer<IList<Models.User.RoleModel>>(roleModels);

            var totalRecords = _roleSvc.GetAll().Count();
            var result = from c in roleModels
                         select new[] { Convert.ToString(c.Id), c.Rolename, c.Systemname };



            JsonResult jsonRes = Json(new
            {
                sEcho = model.Draw,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                //aaData = result
                aaData = roleModels
            }, JsonRequestBehavior.AllowGet);
            return jsonRes;

            //return Json(new { success = true, data = _roleSvc.GetAll().ToList(), Message = "" }, JsonRequestBehavior.AllowGet);
        }

        [AresAuthorize(ResourceKey = "Admin.Role.Create")]
        public ActionResult Create()
        {
            var model = new Models.User.RoleModel();
            model.PermissionViewModel = GetMultiSelectListViewInitialModel<Permission>(_permissionSvc.GetAll().ToList(), null);

            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.Role.Create")]
        [System.Web.Mvc.HttpPost]
        public ActionResult Create(Models.User.RoleModel model)
        {
            if (!ModelState.IsValid)
            { // re-render the view when validation failed.
                model.PermissionViewModel = GetMultiSelectListViewInitialModel<Permission>(_permissionSvc.GetAll().ToList(), null);
                return View("Create", model);
            }

            var role = model.ToEntity();
            role.Active = true;

            _roleSvc.Insert(role);

            SuccessNotification(_localizationSvc.GetResource("Notifications.Role.Created"));

            return RedirectToAction("List");
        }

        [AresAuthorize(ResourceKey = "Admin.Role.Update")]
        public ActionResult Edit(int id)
        {
            Role role = _roleSvc.GetById(id);
            var model = role.ToModel();
            model.PermissionViewModel = GetMultiSelectListViewInitialModel<Permission>(_permissionSvc.GetAll().ToList(), role.Permissions.ToList()); 

            return View(model);
        }


        [AresAuthorize(ResourceKey = "Admin.Role.Update")]
        [System.Web.Mvc.HttpPost]
        public ActionResult Edit(Models.User.RoleModel model)
        {
            
            var role = _roleSvc.GetById(model.Id);


            var urStringArrayFromDb = (from ur in role.Permissions
                                       select ur.Id + "");

            if (model.PermissionViewModel != null)
            {
                model.PermissionViewModel = GetMultiSelectListViewModel<Permission>(model.PermissionViewModel.PostedItems, _permissionSvc.GetAll().ToList());

                var urFromModel = model.PermissionViewModel.PostedItems.ItemIds;

                // Items in first array but not on second array (to be added to db)
                IEnumerable<string> onlyInModelNotInDb = urFromModel.Except(urStringArrayFromDb);

                foreach (string item in onlyInModelNotInDb)
                {
                    Permission record = _permissionSvc.GetById(Int32.Parse(item));
                    role.Permissions.Add(record);
                }

                // Items in first array but not second array (to be deleted from db)
                IEnumerable<string> onlyInDbNotInModel = new List<string>(urStringArrayFromDb.Except(urFromModel));

                foreach (string item in onlyInDbNotInModel)
                {
                    Permission record = role.Permissions.Where(ur => ur.Id + "" == item).FirstOrDefault();
                    role.Permissions.Remove(record);
                }
            }
            else
            {
                if (role.Permissions != null)
                    role.Permissions.Clear();
            }
            role = model.ToEntity(role);


            _roleSvc.Update(role);

            SuccessNotification(_localizationSvc.GetResource("Notifications.Role.Updated"));

            return RedirectToAction("List");
        }


        public ActionResult CreateOrUpdate(string itemId, string imageUrl, string mimeType, string uploadFilename)
        {
            Models.User.RoleModel response = null;
            

            int nItemId = -1;
            int.TryParse(itemId, out nItemId);

            if (nItemId != -1)
            {
                Role record = _roleSvc.GetById(nItemId);
                response = record.ToModel();
                if (record != null) // update
                {

                    var permListItems = from ur in record.Permissions
                                        select new Ext.Net.ListItem { Text = ur.PermissionName };

                    //response.RoleListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem { Text = "Administrators" }, new Ext.Net.ListItem { Text = "Guests" } };
                    response.PermissionListItems = permListItems.ToArray();

                }
                else // insert mode (after image upload popup close)
                {
                    response = new Models.User.RoleModel();
                    response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() { Text = "" } };
                }
            }
            else // insert
            {
                response = new Models.User.RoleModel();
                response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() {Text = "" }  };
                
            }

 
            ViewBag.itemId = nItemId;

            return View(response);
        }

        public ActionResult ListPermissions()
        {
            string jsonString = JsonHelper.JsonSerializer<IList<Models.User.PermissionModel>>(_permissionSvc.GetAll().ToList().Select(x => x.ToModel()).ToList());
            var result = new { success = true, data = jsonString, Message = "" };
            JsonResult jsonRes = Json(result, JsonRequestBehavior.AllowGet);
            return jsonRes;

            //return Json(new { success = true, data = _roleSvc.GetAll().ToList(), Message = "" }, JsonRequestBehavior.AllowGet);
        }

        [System.Web.Mvc.HttpPost]
        public ActionResult CreateOrUpdate(Models.User.RoleModel model)
        {


            if (model.Id == 0)  // insert mode
            {
                Role record = model.ToEntity();
                

                if (model.ReqPermissionListItems != null)
                {
                    var urFromModel = model.ReqPermissionListItems.Split(',');


                    foreach (string item in urFromModel)
                    {
                        Permission perm = _permissionSvc.GetById(Int32.Parse(item));
                        record.Permissions.Add(perm);
                    }
                }

                _roleSvc.Insert(record);


            }
            else // update mode
            {
                Role record = _roleSvc.GetById(model.Id);

                var urStringArrayFromDb = (from ur in record.Permissions
                                           select ur.Id + "");


                if (model.ReqPermissionListItems != null)
                {
                    var urFromModel = model.ReqPermissionListItems.Split(',');

                    // Items in first array but not on second array (to be added to db)
                    IEnumerable<string> onlyInModelNotInDb = urFromModel.Except(urStringArrayFromDb);

                    foreach (string item in onlyInModelNotInDb)
                    {
                        Permission perm = _permissionSvc.GetById(Int32.Parse(item));
                        record.Permissions.Add(perm);
                    }

                    // Items in first array but not second array (to be deleted from db)
                    IEnumerable<string> onlyInDbNotInModel = new List<string>(urStringArrayFromDb.Except(urFromModel));

                    foreach (string item in onlyInDbNotInModel)
                    {
                        Permission perm = record.Permissions.Where(ur => ur.Id + "" == item).FirstOrDefault();
                        record.Permissions.Remove(perm);
                    }
                }
                else
                {
                    if(record.Permissions != null)
                        record.Permissions.Clear();
                }

                record = model.ToEntity(record);

                _roleSvc.Update(record);
            }


            return this.FormPanel();
        }

        [AresAuthorize(ResourceKey = "Admin.Role.Delete")]
        public ActionResult Delete(int id)
        {

            if (id != -1)
            {
                Role record = _roleSvc.GetById(id);

                _roleSvc.Delete(record);

                SuccessNotification(_localizationSvc.GetResource("Notifications.Role.Deleted"));
            }
            else
            {

            }


            return RedirectToAction("List");
        }


    }
}
