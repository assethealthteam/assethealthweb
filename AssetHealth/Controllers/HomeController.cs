using Ares.Core.Framework.Logging;
using Ares.Core.Helpers;
using Ares.UMS.DAL.Repository;
using Ares.UMS.DTO.Log;
using Ares.UMS.DTO.Users;
using Ares.UMS.Svc;
using Ares.UMS.Svc.Configuration;
using Ares.UMS.Svc.Localization;
using Ares.UMS.Svc.Log;
using Ares.UMS.Svc.Security;
using Ares.Web.Attributes;
using Ares.Web.Extensions;
using Ares.Web.JSON;
using AssetHealth.Common.DAL;
using AssetHealth.Common.DTO;
using AssetHealth.Common.DTO.Enums;
using AssetHealth.Common.DTO.Extensions;
using AssetHealth.Common.Svc;
using AssetHealth.Common.Svc.Analyzer;
using AssetHealth.Extensions;
using AssetHealth.Models.Common;
using AssetHealth.Models.Eq_warning;
using AssetHealth.Models.Form_answer;
using AssetHealth.Models.Home;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace AssetHealth.Controllers
{
    [AresAuthorize(ResourceKey = "Admin.Home")]
    public class HomeController : Controller
    {
        private readonly IUserRepository _userRepository;
        private readonly IRoleRepository _roleRepository;
        private readonly IEncryptionSvc _encryptionService;
        private readonly ILocalizationSvc _localizationSvc;
        private readonly UserSettings _userSettings;
        private readonly ILogger _logger;
        private readonly IMenuRepository _menuRepository;
        private readonly IMenuSvc _menuSvc;
        private readonly IEqLocationSvc _eqLocationSvc;
        private readonly IEqWarningSvc _iEqWarningSvc;
        private readonly IFormAnswerSvc _iFormAnswerSvc;
        private readonly IFormQuestionSvc _iFormQuestionSvc;
        private readonly INotificationSvc _iNotificationSvc;
        private readonly IParameterItemSvc _iParameterItemSvc;
        private readonly IWorkContext _workContext;

        public HomeController(IUserRepository userRepository, IEncryptionSvc encryptionService, UserSettings userSettings, 
            IMenuRepository menuRepository, IRoleRepository roleRepository, ILogger logger, IMenuSvc menuSvc, ILocalizationSvc localizationSvc,
            IEqLocationSvc eqLocationSvc, IEqWarningSvc iEqWarningSvc, IFormAnswerSvc iFormAnswerSvc, IFormQuestionSvc iFormQuestionSvc,
            INotificationSvc iNotificationSvc, IParameterItemSvc iParameterItemSvc, IWorkContext workContext
            )
        {
            this._userRepository = userRepository;
            this._encryptionService = encryptionService;
            this._userSettings = userSettings;
            this._roleRepository = roleRepository;
            this._localizationSvc = localizationSvc;

            this._menuRepository = menuRepository;
            this._logger = logger;
            this._menuSvc = menuSvc;

            this._iEqWarningSvc = iEqWarningSvc;
            this._iFormAnswerSvc = iFormAnswerSvc;
            this._iFormQuestionSvc = iFormQuestionSvc;

            this._eqLocationSvc = eqLocationSvc;
            this._iNotificationSvc = iNotificationSvc;
            this._iParameterItemSvc = iParameterItemSvc;
            this._workContext = workContext;

        }

        [AresAuthorize(ResourceKey = "Admin.System.Pilot")]
        public ActionResult Pilot(DashboardModel model)
        {

            if (model == null)
                model = new DashboardModel();

            //return View(prepareDashboard(model));

            return View(model);
        }


        public ActionResult Index(DashboardModel model)
        {

            //return View(prepareDashboard(model));

            if (model == null)
                model = new DashboardModel();

            List<EqLocation> locations = _workContext.CurrentUser.Location();

            if (model.Filter_Location_Id <= 0 && _workContext.CurrentUser.UserRoles.All(r => r.Id > 2))
                model.Filter_Location_Id = locations.FirstOrDefault().Id;

            //return View(prepareDashboard(model));

            return View(model);
        }

        public ActionResult GetQuestionGrid(DashboardModel model)
        {
            //JsonResult jsonRes = Json(prepareDashboard(model), JsonRequestBehavior.AllowGet);

            //DashboardModel model = new DashboardModel();
            if (model == null)
                model = new DashboardModel();

            model.PrepareDashboardModel(_workContext.CurrentUser);

            AresJsonSerializerSettings settings = new AresJsonSerializerSettings();
            settings.Formatting = Formatting.None;

            

            var questionList = _iFormQuestionSvc.SqlQuery<QuestionModel>(string.Format(HardCodedSQL.DASHBOARD_VQUESTION_GRID, string.Join<int>(",", model.RelatedLocationIds)), model.SqlParams).ToList();
            model.QuestionGrid = JsonConvert.SerializeObject(questionList, settings);


            JsonResult jsonRes = Json(model, JsonRequestBehavior.AllowGet);
            jsonRes.MaxJsonLength = int.MaxValue;
            return jsonRes;
        }

        public ActionResult GetNotifications(DashboardModel model)
        {
            //JsonResult jsonRes = Json(prepareDashboard(model), JsonRequestBehavior.AllowGet);

            //DashboardModel model = new DashboardModel();
            if (model == null)
                model = new DashboardModel();

            model.PrepareDashboardModel(_workContext.CurrentUser);

            AresJsonSerializerSettings settings = new AresJsonSerializerSettings();
            settings.Formatting = Formatting.None;

            //model.NotificationModels = _iNotificationSvc.GetAll().OrderByDescending(n => n.Id).Take(100).ToList().Select(x => { var nModel = x.ToModel(); nModel.Notes = CalculateDateDifferenceFeed(nModel.Start_datetime);  return nModel; }).ToList();
            IQueryable<Notification> qNotification = _iNotificationSvc.GetAll();
            //qNotification = from n in qNotification join l in _eqLocationSvc.GetAll() on 1 equals 1 where l.User_From_EqLocationUserMapping.Any(lu => lu.Id == n.Notified_user_id) && l.Id == locId select n;
            qNotification = qNotification.Where(n => n.Notified_entity_name == "eqlocation" && model.RelatedLocationIds.Contains(n.Notified_entity_id ?? 0));
            model.NotificationModels = qNotification.OrderByDescending(n => n.Id).Take(800).ToList().Select(x => { var nModel = x.ToModel(); nModel.Notes = CalculateDateDifferenceFeed(nModel.Start_datetime.GetValueOrDefault()); return nModel; }).ToList();



            JsonResult jsonRes = Json(model, JsonRequestBehavior.AllowGet);
            jsonRes.MaxJsonLength = int.MaxValue;
            return jsonRes;
        }

        public ActionResult GetUserStatistics(DashboardModel model)
        {
            //JsonResult jsonRes = Json(prepareDashboard(model), JsonRequestBehavior.AllowGet);

            //DashboardModel model = new DashboardModel();
            if (model == null)
                model = new DashboardModel();

            model.PrepareDashboardModel(_workContext.CurrentUser);

            AresJsonSerializerSettings settings = new AresJsonSerializerSettings();
            settings.Formatting = Formatting.None;

            int idxGroupBy = 6;

            model.SqlParams[idxGroupBy].Value = 5;
            var tmpList = _iFormAnswerSvc.SqlQuery<AssetHealthPerformanceModel>(HardCodedSQL.DASHBOARD_USER_STATISTICS, model.SqlParams).ToList();
            if (tmpList != null && tmpList.Count > 0)
            {
                var groupUserPerf = tmpList.GroupBy(u => u.ROLENAME_TITLE).Select(u => new AssetHealthPerformanceModel()
                {
                    ROLENAME_TITLE = u.Key,
                    NUMBER_OF_QUESTIONS = u.Sum(t => int.Parse(t.NUMBER_OF_QUESTIONS)).ToString(),
                    TOTAL_ANSWERED = u.Sum(t => int.Parse(t.TOTAL_ANSWERED)).ToString(),
                    REQUIRED_ANSWER = u.Sum(t => int.Parse(t.REQUIRED_ANSWER)).ToString(),
                    PERFORMANCE = (u.Sum(t => int.Parse(t.TOTAL_ANSWERED)) * 100 / u.Sum(t => int.Parse(t.REQUIRED_ANSWER))).ToString(),

                });

                model.UserActivityModel = groupUserPerf.ToList();
            }

            model.UserActivityModel.ForEach(p => p.PERFORMANCE = int.Parse(p.PERFORMANCE) > 100 ? "100" : p.PERFORMANCE);



            JsonResult jsonRes = Json(model, JsonRequestBehavior.AllowGet);
            jsonRes.MaxJsonLength = int.MaxValue;
            return jsonRes;
        }

        public ActionResult PrepareDashboard(DashboardModel model)
        {
            //JsonResult jsonRes = Json(prepareDashboard(model), JsonRequestBehavior.AllowGet);

            //DashboardModel model = new DashboardModel();
            if (model == null)
                model = new DashboardModel();

            model.PrepareDashboardModel(_workContext.CurrentUser);

            AresJsonSerializerSettings settings = new AresJsonSerializerSettings();
            settings.Formatting = Formatting.None;

            int idxGroupBy = 6;



            model.SqlParams[idxGroupBy].Value = 2;
            var tmpList = _iFormAnswerSvc.SqlQuery<AssetHealthPerformanceModel>(HardCodedSQL.DASHBOARD_SP_STATISTICS, model.SqlParams).ToList();
            //tmpList = _iFormAnswerSvc.SqlQuery<AssetHealthPerformanceModel>(@"SELECT NAME, sum(HEALTHY) HEALTHY, sum(UNHEALTHY) UNHEALTHY, TRUNCATE(sum(HEALTHY) * 100 / (sum(HEALTHY)  + sum(UNHEALTHY)) , 2) PERFORMANCE FROM VLOCATION_PERF WHERE READ_NO between @startDate AND @endDate group by Id", sqlParams).ToList();
            model.LocationHealthPerformance = JsonConvert.SerializeObject(tmpList, settings);


            model.SqlParams[idxGroupBy].Value = 3;
            tmpList = _iFormAnswerSvc.SqlQuery<AssetHealthPerformanceModel>(HardCodedSQL.DASHBOARD_SP_STATISTICS, model.SqlParams).ToList();

            //tmpList = _iFormAnswerSvc.SqlQuery<AssetHealthPerformanceModel>(@"SELECT MIN(CATEGORY) CATEGORY,CATEGORY_ID, MIN(NAME) NAME, sum(HEALTHY) HEALTHY, sum(UNHEALTHY) UNHEALTHY, TRUNCATE(sum(HEALTHY) * 100 / (sum(HEALTHY)  + sum(UNHEALTHY)) , 2) PERFORMANCE FROM VLOCATION_PERF_CATEGORY WHERE READ_NO between @startDate AND @endDate group by CATEGORY_Id", sqlParams).ToList();
            model.GeneralStatModel = tmpList.Select(s =>
            {
                AmChartsModel pModel = new AmChartsModel();
                pModel.Value = s.PERFORMANCE;
                //pModel.TITLE = _iParameterItemSvc.GetById(s.CATEGORY_Id).GetLocalized(p => p.Title);
                pModel.TITLE = s.CATEGORY;
                return pModel;
            }).ToList();



            JsonResult jsonRes = Json(model, JsonRequestBehavior.AllowGet);
            jsonRes.MaxJsonLength = int.MaxValue;
            return jsonRes;
        }

        public ActionResult PrepareGraphs(DashboardModel model)
        {
            //DashboardModel model = new DashboardModel();
            if (model == null)
                model = new DashboardModel();

            model.PrepareDashboardModel(_workContext.CurrentUser);

            AresJsonSerializerSettings settings = new AresJsonSerializerSettings();
            settings.Formatting = Formatting.None;

            int idxGroupBy = 6;

            // jquery flot plugin
            var tmpList = _iFormAnswerSvc.SqlQuery<AssetHealthPerformanceModel>(HardCodedSQL.DASHBOARD_SP_STATISTICS, model.SqlParams).ToList();
            model.AssetHealthPerformance = JsonConvert.SerializeObject(tmpList.Select(l => { var x = new List<string>(); x.Add(l.FORMATTED_DAY); x.Add(l.PERFORMANCE.Replace(",", ".")); return x; }), settings);

            // datatable
            var guncelAnormallikler = _iFormAnswerSvc.SqlQuery<CurrentWarningModel>(string.Format(HardCodedSQL.DASHBOARD_VCURRENT_WARNINGS, string.Join<int>(",", model.RelatedLocationIds), _workContext.WorkingLanguage.Id), model.SqlParams).ToList();
            model.CurrentWarnings = JsonConvert.SerializeObject(guncelAnormallikler.Select(l => { var x = new List<string>(); x.Add(l.Id.ToString()); x.Add(l.LOCATION_NAME + " - " + l.NAME); x.Add(l.TITLE); x.Add(l.PERIOD_TITLE); x.Add(l.CONVERTED_ANSWER); x.Add(l.WARNING_TYPE); x.Add(l.USERNAME); x.Add(l.READ_DATETIME); x.Add("&nbsp;&nbsp;<a href='/FormQuestion/Edit/" + l.QUESTION_ID + "' ><i class='lnkEdit fa fa-pencil' style='font-size: 22px;' title='" + _localizationSvc.GetResource("Dashboard.Grid.UnhealthySituations.GotoQuestionDetail") + "'></i></a>"); return x; }), settings);

            // amcharts
            model.SqlParams[idxGroupBy].Value = 1;
            tmpList = _iFormAnswerSvc.SqlQuery<AssetHealthPerformanceModel>(HardCodedSQL.DASHBOARD_SP_STATISTICS, model.SqlParams).ToList();

            if (model.Locations.FirstOrDefault().Id == 1 && tmpList.Count > 0 && tmpList.Count < 18)
            {
                Random random = new Random();
                int arraySize = 18 - tmpList.Count;
                for (int i = 0; i < arraySize; i++)
                    tmpList.Add(new AssetHealthPerformanceModel() { EQUIPMENT_NAME = "EKIPMAN " + i, HEALTHY = random.Next(0, 4000).ToString(), UNHEALTHY = random.Next(0, 4000).ToString() });
            }

            tmpList = tmpList.OrderByDescending(t => int.Parse(t.UNHEALTHY)).Take(12).Select(t => { t.UNHEALTHY = "-" + t.UNHEALTHY; return t; }).ToList();

            model.EquipmentHealthPerformance = JsonConvert.SerializeObject(tmpList, settings);

            JsonResult jsonRes = Json(model, JsonRequestBehavior.AllowGet);

            jsonRes.MaxJsonLength = int.MaxValue;
            return jsonRes;
        }

        private DashboardModel prepareDashboard(DashboardModel model)
        {
            //DashboardModel model = new DashboardModel();
            if (model == null)
                model = new DashboardModel();

            model.PrepareDashboardModel(_workContext.CurrentUser);

            AresJsonSerializerSettings settings = new AresJsonSerializerSettings();
            settings.Formatting = Formatting.None;


            int idxGroupBy = 6;

            // jquery flot plugin
            var tmpList = _iFormAnswerSvc.SqlQuery<AssetHealthPerformanceModel>(HardCodedSQL.DASHBOARD_SP_STATISTICS, model.SqlParams).ToList();
            model.AssetHealthPerformance = JsonConvert.SerializeObject(tmpList.Select(l => { var x = new List<string>(); x.Add(l.FORMATTED_DAY); x.Add(l.PERFORMANCE.Replace(",", ".")); return x; }), settings);

            // datatable
            var guncelAnormallikler = _iFormAnswerSvc.SqlQuery<CurrentWarningModel>(string.Format(HardCodedSQL.DASHBOARD_VCURRENT_WARNINGS, string.Join<int>(",", model.RelatedLocationIds), _workContext.WorkingLanguage.Id), model.SqlParams).ToList();
            model.CurrentWarnings = JsonConvert.SerializeObject(guncelAnormallikler.Select(l => { var x = new List<string>(); x.Add(l.Id.ToString()); x.Add(l.LOCATION_NAME + " - " + l.NAME); x.Add(l.TITLE); x.Add(l.PERIOD_TITLE); x.Add(l.CONVERTED_ANSWER); x.Add(l.WARNING_TYPE); x.Add(l.USERNAME); x.Add(l.READ_DATETIME); x.Add("&nbsp;&nbsp;<a href='/FormQuestion/Edit/" + l.QUESTION_ID+ "' ><i class='lnkEdit fa fa-pencil' style='font-size: 22px;' title='" + _localizationSvc.GetResource("Dashboard.Grid.UnhealthySituations.GotoQuestionDetail") + "'></i></a>"); return x; }), settings);

            // amcharts
            model.SqlParams[idxGroupBy].Value = 1;
            tmpList = _iFormAnswerSvc.SqlQuery<AssetHealthPerformanceModel>(HardCodedSQL.DASHBOARD_SP_STATISTICS, model.SqlParams).ToList();

            if (model.Locations.FirstOrDefault().Id == 1 && tmpList.Count > 0 && tmpList.Count < 18)
            {
                Random random = new Random();
                int arraySize = 18 - tmpList.Count;
                for (int i = 0; i < arraySize; i++)
                    tmpList.Add(new AssetHealthPerformanceModel() { EQUIPMENT_NAME = "EKIPMAN " + i, HEALTHY = random.Next(0, 4000).ToString(), UNHEALTHY = random.Next(0, 4000).ToString() });
            }

            tmpList = tmpList.OrderByDescending(t => int.Parse(t.UNHEALTHY)).Take(12).Select(t => { t.UNHEALTHY = "-" + t.UNHEALTHY; return t; }).ToList();

            model.EquipmentHealthPerformance = JsonConvert.SerializeObject(tmpList, settings);

            model.SqlParams[idxGroupBy].Value = 2;
            tmpList = _iFormAnswerSvc.SqlQuery<AssetHealthPerformanceModel>(HardCodedSQL.DASHBOARD_SP_STATISTICS, model.SqlParams).ToList();
            //tmpList = _iFormAnswerSvc.SqlQuery<AssetHealthPerformanceModel>(@"SELECT NAME, sum(HEALTHY) HEALTHY, sum(UNHEALTHY) UNHEALTHY, TRUNCATE(sum(HEALTHY) * 100 / (sum(HEALTHY)  + sum(UNHEALTHY)) , 2) PERFORMANCE FROM VLOCATION_PERF WHERE READ_NO between @startDate AND @endDate group by Id", sqlParams).ToList();
            model.LocationHealthPerformance = JsonConvert.SerializeObject(tmpList, settings);

            var questionList = _iFormQuestionSvc.SqlQuery<QuestionModel>(string.Format(HardCodedSQL.DASHBOARD_VQUESTION_GRID, string.Join<int>(",", model.RelatedLocationIds)), model.SqlParams).ToList();
            model.QuestionGrid = JsonConvert.SerializeObject(questionList, settings);

            //model.NotificationModels = _iNotificationSvc.GetAll().OrderByDescending(n => n.Id).Take(100).ToList().Select(x => { var nModel = x.ToModel(); nModel.Notes = CalculateDateDifferenceFeed(nModel.Start_datetime);  return nModel; }).ToList();
            IQueryable<Notification> qNotification = _iNotificationSvc.GetAll();
            //qNotification = from n in qNotification join l in _eqLocationSvc.GetAll() on 1 equals 1 where l.User_From_EqLocationUserMapping.Any(lu => lu.Id == n.Notified_user_id) && l.Id == locId select n;
            qNotification = qNotification.Where(n => n.Notified_entity_name == "eqlocation" && model.RelatedLocationIds.Contains(n.Notified_entity_id ?? 0));
            model.NotificationModels = qNotification.OrderByDescending(n => n.Id).Take(800).ToList().Select(x => { var nModel = x.ToModel(); nModel.Notes = CalculateDateDifferenceFeed(nModel.Start_datetime.GetValueOrDefault()); return nModel; }).ToList();


            model.SqlParams[idxGroupBy].Value = 3;
            tmpList = _iFormAnswerSvc.SqlQuery<AssetHealthPerformanceModel>(HardCodedSQL.DASHBOARD_SP_STATISTICS, model.SqlParams).ToList();

            //tmpList = _iFormAnswerSvc.SqlQuery<AssetHealthPerformanceModel>(@"SELECT MIN(CATEGORY) CATEGORY,CATEGORY_ID, MIN(NAME) NAME, sum(HEALTHY) HEALTHY, sum(UNHEALTHY) UNHEALTHY, TRUNCATE(sum(HEALTHY) * 100 / (sum(HEALTHY)  + sum(UNHEALTHY)) , 2) PERFORMANCE FROM VLOCATION_PERF_CATEGORY WHERE READ_NO between @startDate AND @endDate group by CATEGORY_Id", sqlParams).ToList();
            model.GeneralStatModel = tmpList.Select(s =>
            {
                AmChartsModel pModel = new AmChartsModel();
                pModel.Value = s.PERFORMANCE;
                //pModel.TITLE = _iParameterItemSvc.GetById(s.CATEGORY_Id).GetLocalized(p => p.Title);
                pModel.TITLE = s.CATEGORY;
                return pModel;
            }).ToList();

            model.SqlParams[idxGroupBy].Value = 5;
            tmpList = _iFormAnswerSvc.SqlQuery<AssetHealthPerformanceModel>(HardCodedSQL.DASHBOARD_USER_STATISTICS, model.SqlParams).ToList();
            if(tmpList != null && tmpList.Count > 0)
            {
                var groupUserPerf = tmpList.GroupBy(u => u.ROLENAME_TITLE).Select(u => new AssetHealthPerformanceModel()
                {
                    ROLENAME_TITLE = u.Key,
                    NUMBER_OF_QUESTIONS = u.Sum(t => int.Parse(t.NUMBER_OF_QUESTIONS)).ToString(),
                    TOTAL_ANSWERED = u.Sum(t => int.Parse(t.TOTAL_ANSWERED)).ToString(),
                    REQUIRED_ANSWER = u.Sum(t => int.Parse(t.REQUIRED_ANSWER)).ToString(),
                    PERFORMANCE = (u.Sum(t => int.Parse(t.TOTAL_ANSWERED)) * 100 / u.Sum(t => int.Parse(t.REQUIRED_ANSWER))).ToString(),

                });

                model.UserActivityModel = groupUserPerf.ToList();
            }
            
            model.UserActivityModel.ForEach(p => p.PERFORMANCE = int.Parse(p.PERFORMANCE) > 100 ? "100" : p.PERFORMANCE);

            return model;
        }

        public ActionResult UserStatisticsPopup(DashboardModel model)
        {
            if (model == null)
                model = new DashboardModel();

            model.EndDate = new DateTime(model.EndDate.Year, model.EndDate.Month, model.EndDate.Day, 23, 59, 59);

            AresJsonSerializerSettings settings = new AresJsonSerializerSettings();
            settings.Formatting = Formatting.None;

            MySqlParameter[] sqlParams = new MySqlParameter[]
            {
              new MySqlParameter("exec_mode", 2) { Direction = ParameterDirection.Input },  // cache
              new MySqlParameter("tenant_id", _workContext.CurrentUser.tenant_id) { Direction = ParameterDirection.Input },
              new MySqlParameter("locId", model.Filter_Location_Id) { Direction = ParameterDirection.Input },
              new MySqlParameter("roleId", _workContext.CurrentUser.UserRoles.Min(r => r.Id) ) { Direction = ParameterDirection.Input },
              new MySqlParameter("startDate", model.StartDateLong) { Direction = ParameterDirection.Input },
              new MySqlParameter("endDate", model.EndDateLong) { Direction = ParameterDirection.Input },
              new MySqlParameter("groupBy", 5) { Direction = ParameterDirection.Input },
              //new MySqlParameter("userId", _workContext.CurrentUser.Id) { Direction = ParameterDirection.Input },
              new MySqlParameter("languageId", _workContext.CurrentUser.WorkingLanguage_Id) { Direction = ParameterDirection.Input },
              
              //new MySqlParameter("Id", location.Id) { Direction = ParameterDirection.Input },
            };

            var tmpList = _iFormAnswerSvc.SqlQuery<AssetHealthPerformanceModel>(HardCodedSQL.DASHBOARD_USER_STATISTICS, sqlParams).ToList();
            var resultModel = JsonConvert.SerializeObject(tmpList.Select(l => { var x = new List<string>();  x.Add(l.LOCATION_NAME + " - " + l.ROLENAME_TITLE);  x.Add(l.PERIOD_TITLE); x.Add(l.NUMBER_OF_QUESTIONS); x.Add(l.REQUIRED_ANSWER); x.Add(l.TOTAL_ANSWERED); x.Add(l.FILES_COUNT); var perfo = int.Parse(l.TOTAL_ANSWERED) * 100 / int.Parse(l.REQUIRED_ANSWER); x.Add((perfo > 100 ? 100 : perfo) + " %"); return x; }), settings);
            model.Activities = resultModel;
            return View("../Common/UserActivityBoxDetail", model);
        }


        public string CalculateDateDifferenceFeed(long dateTime)
        {
            StringBuilder sb = new StringBuilder();
            TimeSpan difference = (DateTime.Now - StringHelper.ConvertLongToDateTime(dateTime)).Duration();
            
            if (difference.Days > 0)
                sb.Append(difference.Days + " " + _localizationSvc.GetResource("Admin.Common.Day") + " ");
            else if (difference.Hours > 0)
                sb.Append(difference.Hours + " " + _localizationSvc.GetResource("Admin.Common.Hour") + " ");
            else if (difference.Minutes > 0)
                sb.Append(difference.Minutes + " " + _localizationSvc.GetResource("Admin.Common.Minute") + " ");
            else if (difference.Seconds > 0)
                sb.Append(difference.Seconds + " " + _localizationSvc.GetResource("Admin.Common.Seconds") + " ");

            sb.Append(_localizationSvc.GetResource("Admin.Common.Ago"));

            return sb.ToString();
        }

        /// <summary>  
        /// GET: /Home/GetCalendarData  
        /// </summary>  
        /// <returns>Return data</returns>  
        public ActionResult GetCalendarData(DashboardModel model)
        {
            // Initialization.  
            JsonResult result = new JsonResult();

            List<int> relatedLocationIds = new List<int>();
            if (model.Filter_Location_Id > 0)
            {
                relatedLocationIds.Add(model.Filter_Location_Id);
            }
            else
            {
                relatedLocationIds = _workContext.CurrentUser.RelatedLocations().Select(r => r.Id).ToList();
            }

            var firstDayOfMonth = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

            try
            {
                // Loading.  
                List<CalendarEventModel> data = new List<CalendarEventModel>();

                List<FormQuestion> formQuestions = _iFormQuestionSvc.GetAll().Where(fq => relatedLocationIds.Contains(fq.QUESTION_FORM.EQUIPMENT.Location.Id)).ToList();
                data = formQuestions.GroupBy(q => q.Period).Select(q =>
                {
                    CalendarEventModel resultModel = null;
                    switch (q.Key)
                    {
                        case 14:
                            resultModel = new CalendarEventModel();

                            resultModel.dow = "[0, 1, 2, 3, 4, 5, 6]";
                            resultModel.Title = "Otonom Bakim " + " g�nl�k kontrol (" + q.Count() + " adet)";
                            resultModel.Desc = "Otonom Bakim " + " g�nl�k kontrol (" + q.Count() + " adet)";
                            resultModel.start = firstDayOfMonth.ToString("yyyyMMddHHmmss");
                            resultModel.end = lastDayOfMonth.ToString("yyyyMMddHHmmss");

                            /*
                            model.start = firstDayOfMonth.ToString();
                            model.end = lastDayOfMonth.ToString();
                            */
                            resultModel.allDay = "false";
                            resultModel.backgroundColor = "blue";
                            break;

                    }

                    return resultModel;
                }).Where(q => q != null).ToList();


                // Processing.  
                //result = this.Json(data, JsonRequestBehavior.AllowGet);
                return Content(JsonConvert.SerializeObject(data, new AresJsonSerializerSettings()), "application/json", Encoding.UTF8);

            }
            catch (Exception ex)
            {
                // Info  
                Console.Write(ex);
            }

            // Return info.  
            return result;
        }


        public ActionResult GetFormAnswerValuesForChart(int questionId, DashboardModel model)
        {
            if(model == null)
                model = new DashboardModel();

            model.EndDate = new DateTime(model.EndDate.Year, model.EndDate.Month, model.EndDate.Day, 23, 59, 59);

            List<FormAnswer> formAnswerList = _iFormAnswerSvc.GetAll().Where(a => a.Form_question_id == questionId && a.Read_no >= model.StartDateLong && a.Read_no <= model.EndDateLong).ToList();

            List<AmChartsModel> data = formAnswerList.Select(a => {
                return new AmChartsModel()
                {
                    Label = a.ANSWER_QUESTION.Option_group_id == null ? null :
                    (a.ANSWER_QUESTION.Option_group_id.GetValueOrDefault(0) == (int)EnumParameterGroup.GROUP_YES_NO_CORRECTED) ? "Evet" : null,
                    Value = a.Answer,
                    //Date = StringHelper.ConvertLongToDateTime(a.Read_no.GetValueOrDefault(0)).ToString("yyyy-MM-dd")
                    Date = a.Actual_read_datetime.GetValueOrDefault(StringHelper.ConvertLongToDateTime(a.Read_no.GetValueOrDefault(0))).ToString("yyyy-MM-dd")

                };
            }).ToList();

            return Content(JsonConvert.SerializeObject(data, new AresJsonSerializerSettings()), "application/json", Encoding.UTF8);
        }

        public ActionResult InstitutionLayoutBox()
        {
            InstitutionLayoutBoxModel model = new InstitutionLayoutBoxModel();
            EqLocation location = _workContext.CurrentUser.Location().FirstOrDefault();
            int locId = location.Id;
            model.FilterLocations = new SelectList(_eqLocationSvc.GetAll().Where(l => l.Parent_id == locId).ToList(), "Id", "Location_name");

            if (location != null && !string.IsNullOrEmpty(location.Schema_path))
                return View(location.Schema_path, model);
            else                    
                return View("../Common/InstitutionLayoutBox", model);
        }



        public ActionResult MapPopup(string locationName, DashboardModel dashBoardModel)
        {
            MapPopupModel model = new MapPopupModel();
            model.SchemaPath = "/Content/images/147_001.png";

            if(dashBoardModel == null)
                dashBoardModel = new DashboardModel();

            dashBoardModel.PrepareDashboardModel(_workContext.CurrentUser);

            EqLocation location = _eqLocationSvc.GetAll().Where(l => l.Cid == locationName).FirstOrDefault();

            AresJsonSerializerSettings settings = new AresJsonSerializerSettings();
            settings.Formatting = Formatting.None;

            if (location != null)
            {
                MySqlParameter[] sqlParams = new MySqlParameter[]
                {
                  new MySqlParameter("startDate", dashBoardModel.StartDateLong) { Direction = ParameterDirection.Input },
                  new MySqlParameter("endDate", dashBoardModel.EndDateLong) { Direction = ParameterDirection.Input },
                  //new MySqlParameter("Id",  location.Id) { Direction = ParameterDirection.Input },
                };

                var guncelAnormallikler = _iFormAnswerSvc.SqlQuery<CurrentWarningModel>(string.Format(HardCodedSQL.DASHBOARD_VCURRENT_WARNINGS, location.Id, _workContext.CurrentUser.WorkingLanguage_Id), sqlParams).ToList();
                //model.CurrentWarnings = JsonConvert.SerializeObject(guncelAnormallikler.Select(l => { var x = new List<string>(); x.Add(l.NAME); x.Add(l.TITLE); x.Add(l.CONVERTED_ANSWER); x.Add(l.WARNING_TYPE); x.Add(l.READ_DATETIME); return x; }), settings);
                model.CurrentWarnings = JsonConvert.SerializeObject(guncelAnormallikler.Select(l => { var x = new List<string>(); x.Add(l.Id.ToString()); x.Add(l.LOCATION_NAME + " - " + l.NAME); x.Add(l.TITLE); x.Add(l.PERIOD_TITLE); x.Add(l.CONVERTED_ANSWER); x.Add(l.WARNING_TYPE); x.Add(l.USERNAME); x.Add(l.READ_DATETIME); x.Add("&nbsp;&nbsp;<a href='/FormQuestion/Edit/" + l.QUESTION_ID + "' ><i class='lnkEdit fa fa-pencil' style='font-size: 22px;' title='" + _localizationSvc.GetResource("Dashboard.Grid.UnhealthySituations.GotoQuestionDetail") + "'></i></a>"); return x; }), settings);

            }


            int dayDif = (int)(dashBoardModel.EndDate - dashBoardModel.StartDate).TotalDays + 1; // bug�n� de ekleyece�iz daha gelmesini bekliyoruz bgn i�in de

            int warningCount = location != null ? _iEqWarningSvc.GetAll().Count(w => w.Active.HasValue && w.Active.Value && w.Warning_type == (int)EnumParameterItem.PARAM_ITEM_WARNING && w.Warning_EQUIPMENT.Location.Id == location.Id && w.Read_no >= dashBoardModel.StartDateLong && w.Read_no <= dashBoardModel.EndDateLong) : 0;
            int alarmCount = location != null ? _iEqWarningSvc.GetAll().Count(w => w.Active.HasValue && w.Active.Value && w.Warning_type == (int)EnumParameterItem.PARAM_ITEM_ALARM && w.Warning_EQUIPMENT.Location.Id == location.Id && w.Read_no >= dashBoardModel.StartDateLong && w.Read_no <= dashBoardModel.EndDateLong) : 0;

            model.AlarmCount = alarmCount;
            model.WarningCount = warningCount;
            model.LocationName = location.Location_name;



            return View(model);
        }

        public JsonResult MapPoll(DashboardModel model)
        {
            //DashboardModel model = new DashboardModel();
            if (model == null)
                model = new DashboardModel();

            model.EndDate = new DateTime(model.EndDate.Year, model.EndDate.Month, model.EndDate.Day, 23, 59, 59);

            //int locId = _workContext.CurrentUser.Location().Id;



            object[] sqlParams = new object[] { new MySqlParameter("startDate", model.StartDateLong) { Direction = ParameterDirection.Input },
            new MySqlParameter("endDate", model.EndDateLong) { Direction = ParameterDirection.Input },
            new MySqlParameter("userId", _workContext.CurrentUser.Id) { Direction = ParameterDirection.Input }};

            var warningsInDb = _iEqWarningSvc.SqlQuery<EqWarningModel>(HardCodedSQL.DASHBOARD_SP_MAP_POLL, sqlParams).ToList();

            return Json(warningsInDb);
        }

        public ActionResult Demo()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        public ActionResult Empty()
        {
            

            return View();
        }

        public ActionResult DashboardFilter(DashboardModel model)
        {
            model.Filter_Location_SelectList = new SelectList(_eqLocationSvc.GetAuthorizedLocations(_workContext.CurrentUser.Id), "Id", "Location_name");
            //model.Filter_Location_SelectList = new SelectList(_eqLocationSvc.GetAll().Where(l => l.Parent_id != null).ToList(), "Object_id", "Location_name");

            return View(model);
        }
        public ActionResult StatBoxGroup(DashboardModel model)
        {
            if (model == null)
                model = new DashboardModel();

            model.PrepareDashboardModel(_workContext.CurrentUser);

            int dayDif = (int)(model.EndDate - model.StartDate).TotalDays + 1; // bug�n� de ekleyece�iz daha gelmesini bekliyoruz bgn i�in de

            List<int> relatedLocationIds = new List<int>();
            if (model.Filter_Location_Id > 0)
            {
                relatedLocationIds.Add(model.Filter_Location_Id);
            }
            else
            {
                relatedLocationIds = _workContext.CurrentUser.RelatedLocations().Select(r => r.Id).ToList();
            }

            int warningCount = _iEqWarningSvc.GetAll().Count(w => w.Active.HasValue && w.Active.Value && w.Warning_type == (int)EnumParameterItem.PARAM_ITEM_WARNING && w.Read_no >= model.StartDateLong && w.Read_no <= model.EndDateLong && relatedLocationIds.Contains(w.Warning_EQUIPMENT.Location.Id));
            int alarmCount = _iEqWarningSvc.GetAll().Count(w => w.Active.HasValue && w.Active.Value && w.Warning_type == (int)EnumParameterItem.PARAM_ITEM_ALARM && w.Read_no >= model.StartDateLong && w.Read_no <= model.EndDateLong && relatedLocationIds.Contains(w.Warning_EQUIPMENT.Location.Id));
            int totalCollectedDataCount = _iFormAnswerSvc.GetAll().Count(a => a.Read_no >= model.StartDateLong && a.Read_no <= model.EndDateLong && relatedLocationIds.Contains(a.ANSWER_QUESTION.QUESTION_FORM.EQUIPMENT.Location.Id));
            int totalRequiredDataCount = _iFormQuestionSvc.GetAll().Count(q => relatedLocationIds.Contains(q.QUESTION_FORM.EQUIPMENT.Location.Id)) * dayDif;

            int allAlarmsCounts = _iEqWarningSvc.GetAll().Count(w => w.Warning_type == (int)EnumParameterItem.PARAM_ITEM_ALARM && w.Read_no >= model.StartDateLong && w.Read_no <= model.EndDateLong && relatedLocationIds.Contains(w.Warning_EQUIPMENT.Location.Id));
            int correctedAbnormals = _iEqWarningSvc.GetAll().Count(w => w.Active.HasValue && !w.Active.Value && w.Warning_type == (int)EnumParameterItem.PARAM_ITEM_ALARM && w.Read_no >= model.StartDateLong && w.Read_no <= model.EndDateLong && relatedLocationIds.Contains(w.Warning_EQUIPMENT.Location.Id));


            List<StatBoxModel> result = new List<StatBoxModel>();
            result.Add(new StatBoxModel() { StatTitle = _localizationSvc.GetResource("Dashboard.Stats.NumberOf.Alarms"), StatSmallTitle = "", CssColorName = "red-haze", ProgressTitle = _localizationSvc.GetResource("Dashboard.Stats.NumberOf.Alarms.ProgressTitle"), CssIcon = "icon-ban", Percentage = totalCollectedDataCount > 0 ? alarmCount * 100 / totalCollectedDataCount : 0, StatValue = alarmCount.ToString() }); // Ge�ti�imiz aya g�re alarmlarda de�i�im oran�
            result.Add(new StatBoxModel() { StatTitle = _localizationSvc.GetResource("Dashboard.Stats.NumberOf.Warnings"), StatSmallTitle = "", CssColorName = "blue-sharp", ProgressTitle = _localizationSvc.GetResource("Dashboard.Stats.NumberOf.Warnings.ProgressTitle"), CssIcon = "icon-bar-chart", Percentage = totalCollectedDataCount > 0 ? warningCount * 100 / totalCollectedDataCount : 0, StatValue = warningCount.ToString() }); // Ge�ti�imiz aya g�re uyar�larda de�i�im oran�
            result.Add(new StatBoxModel() { StatTitle = _localizationSvc.GetResource("Dashboard.Stats.Collected.Equipment.Data"), StatSmallTitle = "", CssColorName = "purple-soft", ProgressTitle = _localizationSvc.GetResource("Dashboard.Stats.Collected.Equipment.Data.ProgressTitle"), CssIcon = "icon-speedometer", Percentage = totalRequiredDataCount > 0 ? totalCollectedDataCount * 100 / totalRequiredDataCount : 0, StatValue = totalCollectedDataCount.ToString() }); // Toplanmas� gereken toplam verinin hen�z toplanm��lara oran�
            result.Add(new StatBoxModel() { StatTitle = _localizationSvc.GetResource("Dashboard.Stats.Corrected.Anormal.Status") , StatSmallTitle = "", CssColorName = "green-sharp", ProgressTitle = _localizationSvc.GetResource("Dashboard.Stats.Stabil.Equipments.ProgressTitle"), CssIcon = "icon-like", Percentage = allAlarmsCounts > 0 ? (100 * correctedAbnormals / allAlarmsCounts) : 0 , StatValue = correctedAbnormals.ToString() }); // D�zeltilen anormallikler

            model.StatBoxModels = result;

            return View("../Common/StatBoxGroup", model);
        }


        public ActionResult GraphPopup(int id, DashboardModel model)
        {
            GraphPopupModel result = new GraphPopupModel();
            FormAnswer answer = _iFormAnswerSvc.GetById(id);
            FormQuestion question = answer.ANSWER_QUESTION;
            result.QuestionTitle = (!string.IsNullOrEmpty(question.Code) ? question.Code + " " : "") + question.Title;
            result.QuestionId = question.Id;
            result.OptionGroupId = question.Option_group_id != null ? question.Option_group_id : -1;

            result.EndDate = StringHelper.ConvertLongToDateTime(answer.Read_no.GetValueOrDefault());
            result.StartDate = result.EndDate.AddDays(-30);
            /*
            if (model.EndDate != null)
                result.EndDate = model.EndDate;
            else
                result.EndDate = StringHelper.ConvertLongToDateTime(answer.Read_no.GetValueOrDefault());

            if (model.StartDate != null)
                result.StartDate = model.StartDate;
            else
                result.StartDate = result.EndDate.AddDays(-30);
                */
            return View("GraphPopup", result);
        }

        public ActionResult GraphPopupFromWarning(int id)
        {
            EqWarning warning = _iEqWarningSvc.GetById(id);
            FormAnswer answer = _iFormAnswerSvc.GetAll().Where(a => /*a.Read_no == warning.Read_no &&*/ a.ANSWER_QUESTION.Eq_channel_id == warning.Eq_channel_id).OrderByDescending(a => a.Read_no).FirstOrDefault();
            if (answer != null && answer.Actual_read_datetime != null)
            {
                GraphPopupModel model = new GraphPopupModel();
                FormQuestion question = answer.ANSWER_QUESTION;
                model.QuestionTitle = (!string.IsNullOrEmpty(question.Code) ? question.Code + " " : "") + question.Title;
                model.QuestionId = question.Id;
                model.OptionGroupId = question.Option_group_id != null ? question.Option_group_id : -1;

                model.EndDate = StringHelper.ConvertLongToDateTime(answer.Read_no.GetValueOrDefault());
                model.StartDate = model.EndDate.AddDays(-30);
                /*
                model.EndDate = DateTime.Now;
                model.StartDate = DateTime.Now.AddDays(-30);
                */
                return View("GraphPopup", model);
            }
            return null;
        }
            

        public ActionResult TotalProfitStatBox()
        {
            StatBoxModel model = new StatBoxModel();
            model.StatTitle = "Total Profit";
            model.CssColorName = "green-sharp";
            model.ProgressTitle = "Progress";
            model.CssIcon = "icon-pie-chart";
            model.Percentage = 76;
            model.StatValue = "7800";

            //return RedirectToAction("StatBox", "Common", model);
            return View("../Common/StatBox", model);
        }



    }
}
