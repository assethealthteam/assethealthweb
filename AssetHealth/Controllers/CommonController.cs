using Ares.Core.Caching;
using Ares.Core.Helpers;
using Ares.UMS.DTO.Localization;
using Ares.UMS.DTO.Media;
using Ares.UMS.DTO.Users;
using Ares.UMS.Svc.Localization;
using Ares.UMS.Svc.Media;
using Ares.UMS.Svc.Users;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Ares.Web.Helpers;
using AssetHealth.Models;
using AssetHealth.Models.Common;
using Ares.UMS.Svc;
using Ares.Web.Svc.Media;
using Ares.Web.Localization;
using AssetHealth.Extensions;
using AssetHealth.Models.Configuration;
using Ares.UMS.DAL.Repository;
using Ares.Web.Controllers;
using Ares.UMS.Svc.Configuration;
using Ares.Web.Attributes;
using System.Data.SqlClient;
using Ares.Core.Framework.Logging;
using System.Data;
using System.Reflection;
using Newtonsoft.Json;
using AssetHealth.Models.Synchronization;
using AssetHealth.Mapper;
using System.Data.OleDb;
using System.Data.Entity.Validation;
using LinqToExcel;
using AssetHealth.Common.DTO;
using AssetHealth.Common.Svc;
using Ares.Core.Framework;
using AssetHealth.Common.DTO.Extensions;
using Ares.UMS.DTO;
using Ares.Core;
using AssetHealth.Models.Home;
using AssetHealth.Common.DAL;

namespace AssetHealth.Controllers
{
    public class CommonController : BaseAresController
    {
        private readonly IWorkContext _workContext;
        private readonly IPictureSvc _pictureSvc;
        private readonly IUserSvc _userSvc;
        private readonly ILanguageSvc _languageSvc;
        private readonly ILanguageRepository _languageRepository;
        private readonly IMenuSvc _menuSvc;
        private readonly ITenantSvc _iTenantSvc;
        private readonly IEqLocationSvc _iEqLocationSvc;
        private readonly IWebHelper _webHelper;
        private readonly LocalizationSettings _localizationSettings;
        private readonly MediaSettings _mediaSettings;
        private readonly ICacheManager _cacheManager;
        private readonly ISiteContext _siteContext;
        private readonly HttpContextBase _httpContextBase;

        public CommonController(IWorkContext workContext, IUserSvc userSvc, IPictureSvc pictureSvc, ILanguageSvc languageSvc, ILanguageRepository languageRepository, IWebHelper webHelper, 
            LocalizationSettings localizationSettings, MediaSettings mediaSettings, ICacheManager cacheManager, ISiteContext siteContext, IMenuSvc menuSvc, ITenantSvc iTenantSvc, IEqLocationSvc iEqLocationSvc, HttpContextBase httpContextBase)
        {
            this._workContext = workContext;
            this._pictureSvc = pictureSvc;
            this._userSvc = userSvc;
            this._languageSvc = languageSvc;
            this._languageRepository = languageRepository;
            this._webHelper = webHelper;
            this._localizationSettings = localizationSettings;
            this._mediaSettings = mediaSettings;
            this._cacheManager = cacheManager;
            this._siteContext = siteContext;
            this._menuSvc = menuSvc;
            this._iTenantSvc = iTenantSvc;
            this._iEqLocationSvc = iEqLocationSvc;
            this._httpContextBase = httpContextBase;
        }

        public ActionResult ImageUpload(int EntityId)
        {
            ImageUploadModel model = new ImageUploadModel();
            model.EntityId = EntityId;

            return View(model);
        }

        [HttpPost]
        public ActionResult ImageUpload(ImageUploadModel model)
        {
            bool isSavedSuccessfully = true;
            string fName = "";
            string uploadFilename = "";
            string fMimeType = "";
            try
            {
                foreach (string fileName in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[fileName];
                    //Save file content goes here
                    uploadFilename = file.FileName;
                    fName = Guid.NewGuid() + Path.GetExtension(file.FileName);
                    fMimeType = file.ContentType;
                    if (file != null && file.ContentLength > 0)
                    {
                        /*
                        var originalDirectory = new DirectoryInfo(string.Format("{0}Content\\Images", Server.MapPath(@"\")));

                        string pathString = System.IO.Path.Combine(originalDirectory.ToString(), "upload");
                        */
                        string uploadPath = new DirectoryInfo(string.Format("{0}{1}", Server.MapPath(@"\"), _mediaSettings.ImageUploadPath)).ToString();

                        var fileName1 = Path.GetFileName(file.FileName);

                        bool isExists = System.IO.Directory.Exists(uploadPath);

                        if (!isExists)
                            System.IO.Directory.CreateDirectory(uploadPath);

                        //var path = string.Format("{0}\\{1}", uploadPath, file.FileName);
                        var path = string.Format("{0}{1}", uploadPath, fName);
                        file.SaveAs(path);

                    }

                }

            }
            catch (Exception ex)
            {
                isSavedSuccessfully = false;
            }


            if (isSavedSuccessfully)
            {
                return Json(new { ImageUrl = fName, MimeType = fMimeType, UploadFilename = uploadFilename });
                //model.ImageUrl = fName;
                //return View(model);
            }
            else
            {
                return Json(new { Message = "Error in saving file" });
            }
        }


        [HttpPost]
        public ActionResult ImageUploadConfirm(ImageUploadModel model)
        {
            if (model.EntityId != 0)
            {
                User user = _userSvc.GetById(model.EntityId);
                /*
                var originalDirectory = new DirectoryInfo(string.Format("{0}Content\\Images", Server.MapPath(@"\")));

                string pathString = System.IO.Path.Combine(originalDirectory.ToString(), "upload");
                */
                string uploadPath = new DirectoryInfo(string.Format("{0}{1}", Server.MapPath(@"\"), _mediaSettings.ImageUploadPath)).ToString();

                Picture pict = new Picture();

                pict.PictureBinary = System.IO.File.ReadAllBytes(uploadPath + "/" + model.ImageUrl);
                pict.MimeType = model.MimeType;
                pict.UploadFilename = model.UploadFilename;
                pict.GuidFilename = model.ImageUrl;

                _pictureSvc.SetPictureForEntity(model.EntityId, "User", pict);

                System.IO.File.Delete(uploadPath + "/" + model.ImageUrl);

            }
            //return View("ImageUpload", model.EntityId);
            return Json(new { Message = "Success" });
        }

        [HttpPost]
        public ActionResult ImageDelete(ImageUploadModel model)
        {
            User user = _userSvc.GetById(model.EntityId);

            _pictureSvc.DeletePictureFromEntity(model.EntityId, "User", user.GetPicture());

            //return View("ImageUpload", model.EntityId);
            return Json(new { Message = "Success" });
        }


        [ChildActionOnly]
        public ActionResult LanguageSelector()
        {
            LanguageSelectorModel model = new LanguageSelectorModel();
            model = PrepareLanguageSelectorModel();
            if(_workContext.CurrentUser != null)
            {
                model.UserFullName = _workContext.CurrentUser.GetFullName();

                IList<Picture> pictures = _workContext.CurrentUser.GetPictures();
                if (pictures != null)
                {
                    model.UserThumbPictureUrl = _pictureSvc.GetPictureUrl(pictures.FirstOrDefault());
                }
            }


            return PartialView(model);
        }

        public ActionResult ChangeLanguage(int langid, string returnUrl = "")
        {
            var language = _languageSvc.GetLanguageById(langid);
            if (language != null && language.Published)
            {
                _workContext.WorkingLanguage = language;
            }

            //url referrer
            if (String.IsNullOrEmpty(returnUrl))
                returnUrl = _webHelper.GetUrlReferrer();
            //home page
            if (String.IsNullOrEmpty(returnUrl) || !Url.IsLocalUrl(returnUrl))
                returnUrl = Url.RouteUrl("Home");
            if (_localizationSettings.SeoFriendlyUrlsForLanguagesEnabled)
            {
                string applicationPath = HttpContext.Request.ApplicationPath;
                if (returnUrl.IsLocalizedUrl(applicationPath, true))
                {
                    //already localized URL
                    returnUrl = returnUrl.RemoveLanguageSeoCodeFromRawUrl(applicationPath);
                }
                returnUrl = returnUrl.AddLanguageSeoCodeToRawUrl(applicationPath, _workContext.WorkingLanguage);
            }

            return Redirect(returnUrl);
        }

        [NonAction]
        protected LanguageSelectorModel PrepareLanguageSelectorModel()
        {
            //var availableLanguages = _cacheManager.Get(string.Format(ModelCacheEventConsumer.AVAILABLE_LANGUAGES_MODEL_KEY, _siteContext.CurrentSite.Id), () =>
            var availableLanguages = _cacheManager.Get(string.Format("Ares.pres.languages.all-{0}", _siteContext.CurrentSite.Id), () =>
            {
                var result = _languageSvc
                    //.GetAllLanguages(siteId: _siteContext.CurrentSite.Id)
                    .GetAllLanguages()
                    .Select(x => new LanguageModel()
                    {
                        Id = x.Id,
                        Name = x.Name,
                        FlagImageFileName = x.FlagImageFileName,
                    })
                    .ToList();
                return result;
            });

            var model = new LanguageSelectorModel()
            {
                CurrentLanguageId = _workContext.WorkingLanguage.Id,
                AvailableLanguages = availableLanguages,
                CurrentLanguage = new LanguageModel()
                {
                    Id = _workContext.WorkingLanguage.Id,
                    FlagImageFileName = _workContext.WorkingLanguage.FlagImageFileName,
                    Name = _workContext.WorkingLanguage.Name
                }
                //UseImages = _localizationSettings.UseImagesForLanguageSelection
            };
            return model;
        }


        [ChildActionOnly]
        public ActionResult Breadcrumb(string breadcrumb)
        {
            BreadcrumbModel model = new BreadcrumbModel();

            model.Breadcrumb.Add(breadcrumb);

            return PartialView(model);
        }

        [ChildActionOnly]
        public ActionResult InfoBlocks(string breadcrumb)
        {
            BreadcrumbModel model = new BreadcrumbModel();

            model.Breadcrumb.Add(breadcrumb);

            return PartialView(model);
        }

        [ChildActionOnly]
        public ActionResult Alert(string breadcrumb)
        {
            BreadcrumbModel model = new BreadcrumbModel();

            model.Breadcrumb.Add(breadcrumb);

            return PartialView(model);
        }

        [ChildActionOnly]
        public ActionResult Sidebar()
        {
            /*
            List<MenuModel> model = new List<MenuModel>();
            model.Add(new MenuModel() {Title = "Kullanıcı Listesi" });
            */
            //List<MenuModel> model = _userSvc.GetMenuForUser(HttpContext.User.Identity.Name).Select(m => m.ToModel()).ToList();
            //List<MenuModel> model = _menuSvc.GetAll().Where(m=> m.Parent != null).ToList().Select(m => m.ToModel()).ToList();
            //List<MenuModel> model = _menuSvc.GetAll().Where(m => m.Parent != null).ToList().Select(m => m.ToModel()).ToList();

            /*
            var models = _menuSvc.GetAll();
            models = models.Where(m => m.TopParent_Id == 1 && (m.Active ?? false)).OrderBy(m => m.DisplayOrder);

            var x = models.ToList();
            List<MenuModel> model = x.Select(m => { var item = m.ToModel(ListMapperConfiguration.CONFIGURATION_NAME); return item; }).ToList();
            */
            // Select'ten önce tolist çağırmak MultipleActiveResultSet parametresi Mysql'de olmadığı için gereklidir
            List<MenuModel> model = _userSvc.GetMenuForUser(HttpContext.User.Identity.Name, 1).OrderBy(m => m.DisplayOrder).ToList().Select(m => m.ToModel(ListMapperConfiguration.CONFIGURATION_NAME)).ToList();

            return PartialView(model);
        }

        [ChildActionOnly]
        public ActionResult Footer(string breadcrumb)
        {
            BreadcrumbModel model = new BreadcrumbModel();

            model.Breadcrumb.Add(breadcrumb);

            return PartialView(model);
        }

        public ActionResult ChartBoxChangeStatistics(DashboardModel model)
        {
            return View(model);
        }
        public ActionResult ChartBoxYesNo(DashboardModel model)
        {
            return View(model);
        }
        public ActionResult GeneralStatBox(DashboardModel model)
        {
            return View(model);
        }
        public ActionResult UserActivityBox(DashboardModel model)
        {
            return View(model);
        }
        public ActionResult ChartBoxPieChart(DashboardModel model)
        {
            return View(model);
        }

        public ActionResult FeedsListBox(DashboardModel model)
        {
            return View(model);
        }
        public ActionResult FeedsCalendarBox(DashboardModel model)
        {
            return View(model);
        }
        public ActionResult GridBoxQuestionGraph(DashboardModel model)
        {
            return View(model);
        }
        



        public ActionResult _Notifications()
        {

            return PartialView();
        }

        public ActionResult ModalDeleteConfirm(string deleteURL)
        {
            ViewData["deleteURL"] = deleteURL;
            if (!string.IsNullOrEmpty(deleteURL))
                ViewData["listURL"] = deleteURL.Substring(0, deleteURL.ToLowerInvariant().IndexOf("/delete")) + "/List";

            return PartialView();
        }

        public ActionResult GlobalModal()
        {

            return PartialView();
        }

        public ActionResult StatBox(StatBoxModel model)
        {

            return PartialView(model);
        }

        public ActionResult StatBoxGroup(DashboardModel model)
        {

            return PartialView(model);
        }


        public ActionResult InstitutionLayoutBox(InstitutionLayoutBoxModel model)
        {

            return PartialView(model);
        }

        public ActionResult UploadExcel()
        {
            UploadExcelModel model = new UploadExcelModel();
            model.SheetName = "Sheet1";

            return View("UploadExcel", model : model);
        }


        [HttpPost]
        public JsonResult UploadExcel(string txtSheetName, AaExcelImport entities, HttpPostedFileBase FileUpload)
        {
            IAaExcelImportSvc _iAaExcelImportSvc = FrameworkCtx.Current.Resolve<IAaExcelImportSvc>();
            FileSettings _fileSettings = FrameworkCtx.Current.Resolve<FileSettings>();
            List<string> data = new List<string>();
            int count = 0;

            if (FileUpload != null)
            {
                // tdata.ExecuteCommand("truncate table OtherCompanyAssets");  
                if (FileUpload.ContentType == "application/vnd.ms-excel" || FileUpload.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                {
                    string filename = FileUpload.FileName;
                    string targetpath = _httpContextBase.Server.MapPath(_fileSettings.DefaultUploadDir);
                    FileUpload.SaveAs(targetpath + "/" + filename);
                    string pathToExcelFile = targetpath + filename;
                    var connectionString = "";
                    if (filename.EndsWith(".xls"))
                    {
                        connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=Excel 8.0;", pathToExcelFile);
                    }
                    else if (filename.EndsWith(".xlsx"))
                    {
                        connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";", pathToExcelFile);
                    }
                    string sheetName = txtSheetName;

                    var adapter = new OleDbDataAdapter("SELECT * FROM [" + sheetName + "$]", connectionString);
                    var ds = new DataSet();

                    adapter.Fill(ds, "ExcelTable");

                    DataTable dtable = ds.Tables["ExcelTable"];

                    

                    var excelFile = new ExcelQueryFactory(pathToExcelFile);
                    var questions = from a in excelFile.Worksheet<AaExcelImport>(sheetName) select a;
                    

                    foreach (var a in questions)
                    {
                        try
                        {
                            if (!string.IsNullOrEmpty(a.Title) && !string.IsNullOrEmpty(a.Location_name) && !string.IsNullOrEmpty(a.Equipment_name))
                            {
                                AaExcelImport entity = new AaExcelImport();
                                
                                entity.Location_name = a.Location_name;
                                entity.Sublocation_name = a.Sublocation_name;
                                entity.Equipment_name = a.Equipment_name;
                                entity.Object_id = a.Object_id;
                                entity.Period = a.Period;
                                entity.Category = a.Category;
                                entity.Question_type = a.Question_type;
                                entity.Code = a.Code;
                                entity.Title = a.Title;
                                entity.Display_order = a.Display_order;
                                entity.Unhealthy_cause = a.Unhealthy_cause;
                                entity.Threshold_repetition_count = a.Threshold_repetition_count;
                                entity.Answer_deadline_day_count = a.Answer_deadline_day_count;
                                entity.Answer_deadline_hour_count = a.Answer_deadline_hour_count;
                                entity.Warning_level = a.Warning_level;
                                entity.Alarm_level = a.Alarm_level;
                                entity.Hint = a.Hint;
                                entity.Criticality = a.Criticality;



                                _iAaExcelImportSvc.Insert(entity);

                                count++;

                            }
                            /*
                            else
                            {
                                data.Add("<ul>");
                                if (a.Title == "" || a.Title == null) data.Add("<li> Title is required</li>");
                                if (a.Location_name == "" || a.Location_name == null) data.Add("<li> Location_name is required</li>");
                                if (a.Equipment_name == "" || a.Equipment_name == null) data.Add("<li>Equipment_name is required</li>");

                                data.Add("</ul>");
                                data.ToArray();
                                return Json(data, JsonRequestBehavior.AllowGet);
                            }
                            */
                        }

                        catch (DbEntityValidationException ex)
                        {
                            foreach (var entityValidationErrors in ex.EntityValidationErrors)
                            {

                                foreach (var validationError in entityValidationErrors.ValidationErrors)
                                {

                                    Response.Write("Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage);

                                }

                            }
                        }
                    }
                    //deleting excel file from folder  
                    if ((System.IO.File.Exists(pathToExcelFile)))
                    {
                        System.IO.File.Delete(pathToExcelFile);
                    }
                    return Json(count + " items were successfully imported", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    //alert message for invalid file format  
                    data.Add("<ul>");
                    data.Add("<li>Only Excel file format is allowed</li>");
                    data.Add("</ul>");
                    data.ToArray();
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                data.Add("<ul>");
                if (FileUpload == null) data.Add("<li>Please choose Excel file</li>");
                data.Add("</ul>");
                data.ToArray();
                return Json(data, JsonRequestBehavior.AllowGet);
            }
        }


        [ChildActionOnly]
        [AresAuthorize(ResourceKey = "Menu.Admin.System.ClearCache")]
        public ActionResult HeaderActions()
        {
            User currentUser = _workContext.CurrentUser;
            List<BootstrapDropdownItemModel> model = _iTenantSvc.GetAll().ToList().Select(t => new BootstrapDropdownItemModel()
            { selected = currentUser.tenant_id == t.Object_id,  text = t.Tenant_name,
              link = Request.Url.GetLeftPart(UriPartial.Authority) + HttpRuntime.AppDomainAppVirtualPath + "Common/HeaderActionsChangeTenant/" + t.Id
                
            }
            ).ToList();

            model.Add(new BootstrapDropdownItemModel()
            {
                selected = false,
                text = "Disable Tenant Filter",
                link = Request.Url.GetLeftPart(UriPartial.Authority) + HttpRuntime.AppDomainAppVirtualPath + "Common/HeaderActionsChangeTenant/" + 999

            });

            return PartialView(model);
        }

        [AresAuthorize(ResourceKey = "Menu.Admin.System.ClearCache")]
        public ActionResult HeaderActionsChangeTenant(int id)
        {

            if (id > 0)
            {
                if (id == 999)
                {
                    ModuleDataTransferDictionary dict = FrameworkCtx.Current.Resolve<ModuleDataTransferDictionary>();
                    dict.DisableMultiTenant = !dict.DisableMultiTenant;

                    return Redirect(Request.UrlReferrer.ToString());
                }
                User currentUser = _workContext.CurrentUser;
                EqLocation oldLoc = currentUser.Location().FirstOrDefault();
                oldLoc.User_From_EqLocationUserMapping.Remove(_workContext.CurrentUser);
                _iEqLocationSvc.Update(oldLoc);

                Tenant tenant = _iTenantSvc.GetById(id);
                string query = string.Format(HardCodedSQL.HEADER_ACTION_UPDATE_TENANT_ID, tenant.Object_id ,currentUser.Id);
                query += string.Format(HardCodedSQL.HEADER_ACTION_INSERT_LOC_USER_MAP, currentUser.Id , tenant.Object_id);

                _iEqLocationSvc.ExecuteSqlCommand(query);


                //return RedirectToAction("Index", "Home");
                return Redirect(Request.UrlReferrer.ToString());
            }

            return PartialView(id);
        }


        /*
        public JsonResult DeleteAdditionalInfo(int id)
        {
            Ares.DTO.Entity<int> entity = _iFormsvc.GetById(id);

            Dictionary<string, int> count = new Dictionary<string, int>();


            List<PropertyInfo> infos = entity.GetType().GetProperties().Where(p => p.PropertyType.IsGenericType && (p.PropertyType.GetGenericTypeDefinition() == typeof(IList<>))).ToList();
            foreach (PropertyInfo info in infos)
            {
                int c = ((System.Collections.ICollection)info.GetValue(entity)).Count;
                count.Add(info.Name, c);
            }
            return Json(JsonConvert.SerializeObject(count), JsonRequestBehavior.AllowGet);
        }
        */


        /*
        [AresAuthorize(ResourceKey = "Common.Language.Migrate_Ref_Data")]
        public ActionResult Migrate_Ref_Data()
        {
            object[] sqlParams = new object[] { new SqlParameter("@Id", "20150201") { Direction = ParameterDirection.Input } };

            var res = _languageRepository.SqlQuery<int>("EXEC [ares].[MIGRATE_REF_DATA] @Id;", sqlParams).ToList();
            if(res != null && res.Count > 0)
                Logger.Log(new LogItem() { Severity = LogLevel.Debug, Message = "MIGRATE_REF_DATA Executed result = " + res[0] });
            else
                Logger.Log(new LogItem() { Severity = LogLevel.Debug, Message = "MIGRATE_REF_DATA Executed result = null" });


            string returnUrl = "";
            //url referrer
            if (String.IsNullOrEmpty(returnUrl))
                returnUrl = _webHelper.GetUrlReferrer();
            //home page
            if (String.IsNullOrEmpty(returnUrl))
                returnUrl = Url.RouteUrl("HomePage");
            if (_localizationSettings.SeoFriendlyUrlsForLanguagesEnabled)
            {
                string applicationPath = HttpContext.Request.ApplicationPath;
                if (returnUrl.IsLocalizedUrl(applicationPath, true))
                {
                    //already localized URL
                    returnUrl = returnUrl.RemoveLanguageSeoCodeFromRawUrl(applicationPath);
                }
                returnUrl = returnUrl.AddLanguageSeoCodeToRawUrl(applicationPath, _workContext.WorkingLanguage);
            }
            return Redirect(returnUrl);
        }


        [AresAuthorize(ResourceKey = "Common.Language.Migrate_Ref_Data")]
        public ActionResult RestoreDemoDB()
        {
            object[] sqlParams = new object[] { new SqlParameter("@Id", "20150201") { Direction = ParameterDirection.Input } };

            var res = _languageRepository.SqlQuery<int>("Use [master]; EXEC [RESTORE_DEMO_DB] @Id;", sqlParams).ToList();
            if (res != null && res.Count > 0)
                Logger.Log(new LogItem() { Severity = LogLevel.Debug, Message = "RESTORE_DEMO_DB Executed result = " + res[0] });
            else
                Logger.Log(new LogItem() { Severity = LogLevel.Debug, Message = "RESTORE_DEMO_DB Executed result = null" });


            string returnUrl = "";
            //url referrer
            if (String.IsNullOrEmpty(returnUrl))
                returnUrl = _webHelper.GetUrlReferrer();
            //home page
            if (String.IsNullOrEmpty(returnUrl))
                returnUrl = Url.RouteUrl("HomePage");
            if (_localizationSettings.SeoFriendlyUrlsForLanguagesEnabled)
            {
                string applicationPath = HttpContext.Request.ApplicationPath;
                if (returnUrl.IsLocalizedUrl(applicationPath, true))
                {
                    //already localized URL
                    returnUrl = returnUrl.RemoveLanguageSeoCodeFromRawUrl(applicationPath);
                }
                returnUrl = returnUrl.AddLanguageSeoCodeToRawUrl(applicationPath, _workContext.WorkingLanguage);
            }
            return Redirect(returnUrl);
        }
        */

    }
}
