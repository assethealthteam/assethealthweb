using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using System.Web.Mvc;
using Ares.UMS.Svc.Directory;
using System.Linq;
using Ares.UMS.DTO.Users;
using Ares.UMS.Svc.Users;
using Ares.UMS.Svc.Common;
using Ares.UMS.DTO.Messages;
using Ares.UMS.Svc.Messages;
using Ares.Core.Helpers;
using Ares.UMS.DTO.Localization;
using Ares.UMS.Svc.Localization;
using System.Net;
using System.Web.Security;
using Ext.Net.MVC;
using Ares.UMS.DTO.Media;
using Ares.UMS.Svc.Media;
using System.IO;
using Ares.UMS.Svc.Security;
using Ares.UMS.DTO.Security;
using Ares.Web.Helpers;

using Ares.UMS.Svc.Configuration;
using Ares.UMS.DTO.Configuration;
using Ares.Web.Controllers;
using Ares.UMS.Svc;
using Ares.Web.Svc.Authentication;
using Ares.Web.Attributes;
using AssetHealth.Models.Configuration;
using AssetHealth.Extensions;
using Ares.Cloud.Amazon;
using Ares.Core.Framework;
using Ares.Web.Svc.Media;
using AssetHealth.Common.DAL;

namespace AssetHealth.Controllers
{
    public class RemoteAdminController : BaseAresController
    {
        private readonly IFilesSvc _iFilesSvc;

        private readonly IStateProvinceSvc _stateProvinceSvc;
        private readonly IGenericAttributeSvc _genericAttributeSvc;
        private readonly ILocalizationSvc _localizationSvc;
        private readonly IPictureSvc _pictureSvc;
        private readonly IUserSvc _userSvc;
        private readonly IRoleSvc _roleSvc;
        private readonly IPermissionSvc _permissionSvc;
        private readonly IMenuSvc _menuSvc;
        private readonly IWorkContext _workContext;
        private readonly IUserRegistrationSvc _userRegistrationSvc;
        private readonly IAuthenticationSvc _authenticationSvc;
        private readonly IWorkflowMessageSvc _workflowMessageSvc;
        private readonly INewsLetterSubscriptionSvc _newsLetterSubscriptionSvc;
        private readonly IWebHelper _webHelper;
        private readonly UserSettings _userSettings;
        private readonly LocalizationSettings _localizationSettings;
        private readonly MediaSettings _mediaSettings;

        public RemoteAdminController(IFilesSvc iFilesSvc, IStateProvinceSvc stateProvinceSvc, IGenericAttributeSvc genericAttributeSvc, ILocalizationSvc localizationSvc,  
            UserSettings userSettings, IPictureSvc pictureSvc,
            IUserSvc userSvc, IRoleSvc roleSvc, IPermissionSvc permissionSvc, IMenuSvc menuSvc, IWorkContext workContext, IUserRegistrationSvc userRegistrationSvc, IAuthenticationSvc authenticationSvc, IWorkflowMessageSvc workflowMessageSvc,
            INewsLetterSubscriptionSvc newsLetterSubscriptionSvc, IWebHelper webHelper, LocalizationSettings localizationSettings, MediaSettings mediaSettings)
        {
            this._iFilesSvc = iFilesSvc;
            this._stateProvinceSvc = stateProvinceSvc;
            this._genericAttributeSvc = genericAttributeSvc;
            this._pictureSvc = pictureSvc;
            this._localizationSvc = localizationSvc;
            this._userSettings = userSettings;
            this._userSvc = userSvc;
            this._roleSvc = roleSvc;
            this._permissionSvc = permissionSvc;
            this._menuSvc = menuSvc;
            this._workContext = workContext;
            this._userRegistrationSvc = userRegistrationSvc;
            this._authenticationSvc = authenticationSvc;
            this._workflowMessageSvc = workflowMessageSvc;
            this._newsLetterSubscriptionSvc = newsLetterSubscriptionSvc;
            this._webHelper = webHelper;
            this._localizationSettings = localizationSettings;
            this._mediaSettings = mediaSettings;

        }




        public ActionResult List()
        {
            MenuModel model = new MenuModel();
            //model. = _roleSvc.GetAll().ToList().Select(x => x.ToModel()).ToList();

            return View(model);

        }

        [AresAuthorize(ResourceKey = "RemoteAdmin.Remote")]
        public ActionResult Remote()
        {
            

            return View();

        }


        [AresAuthorize(ResourceKey = "RemoteAdmin.ListAmazonS3Files")]
        public ActionResult ListFiles()
        {

            
            return View();

        }


        [AresAuthorize(ResourceKey = "RemoteAdmin.ListAmazonS3Files")]
        public JsonResult ListAmazonS3FilesContent()
        {

            AmazonS3FileRepository repository = (AmazonS3FileRepository)FrameworkCtx.Current.ResolveNamed<IFileUploadManager>(AmazonS3FileRepository.AUTOFAC_REGISTER_NAME);
            var items = repository.ListFiles();
            items.ForEach(i => i.File_reference = "https://assethealth.s3.amazonaws.com/demo/" + i.Title);

            //List<Files> filesInDb = _iFilesSvc.GetAll().Where(f => 1 == 1 || 1 == 1).ToList();
            object[] sqlParams = new object[1];
            List<Files> filesInDb = _iFilesSvc.SelectCustomQuery<Files>(HardCodedSQL.REMOTE_ADMIN_GET_FILES, sqlParams).ToList(); // Tenant_id filtresinden admin olarak ka�mak i�in

            var result = items.Select(i => new { Files = i, SizeHumanReadable = StringHelper.GetBytesReadable(i.Size), IsInDb = filesInDb.Any(f => i.Title == f.Title ) });

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [AresAuthorize(ResourceKey = "RemoteAdmin.DeleteAmazonS3File")]
        public ActionResult DeleteAmazonS3File(string fileName)
        {
            string AWSBucketName = System.Configuration.ConfigurationManager.AppSettings["AWSBucketName"];
            string AWSBucketRegion = System.Configuration.ConfigurationManager.AppSettings["AWSBucketRegion"];
            string AWSBucketSubDir = System.Configuration.ConfigurationManager.AppSettings["AWSBucketSubDir"];


            AmazonS3FileRepository repository = (AmazonS3FileRepository)FrameworkCtx.Current.ResolveNamed<IFileUploadManager>(AmazonS3FileRepository.AUTOFAC_REGISTER_NAME);
            repository.DeleteObjectNonVersionedBucketAsync(AWSBucketName, AWSBucketSubDir + "/" + fileName, AWSBucketRegion);

            return RedirectToAction("ListAmazonS3FilesContent");
        }



    }
}
