﻿



using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.IO;
using System.Web.Http;
using System.Net;
using System.Web.Http.ModelBinding;
using System.Web.Security;
using Ext.Net.MVC;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using System.Web.Mvc;
using System.Linq;
using Ares.UMS.Svc.Directory;
using Ares.UMS.DTO.Users;
using Ares.UMS.Svc.Users;
using Ares.Web.Svc.Authentication;
using Ares.Web.Svc.Web;
using Ares.UMS.Svc.Common;
using Ares.UMS.DTO.Messages;
using Ares.UMS.Svc.Messages;
using Ares.Core.Helpers;
using Ares.UMS.DTO.Localization;
using Ares.UMS.Svc.Localization;
using Ares.UMS.DTO.Media;
using Ares.UMS.Svc.Media;
using Ares.Web.Models.Role;
using Ares.UMS.Svc.Security;
using Ares.UMS.DTO.Security;
using Ares.Web.Helpers;
using Ares.Web.Attributes;
using Ares.Web.Controllers;
using Ares.UMS.Svc.Configuration;
using Ares.UMS.DTO.Configuration;
using Ares.UMS.Svc;
using Ares.Web.Razor.Extensions.Datatable.Model;
using AssetHealth.Extensions;
using AssetHealth.Models.User;
using AssetHealth.Common.Svc;
using AssetHealth.Common.DTO;
using AssetHealth.Models.Configuration;
using AssetHealth.Models.Device;
using AssetHealth.Models.User;
using AssetHealth.Models.Form_answer;
using AssetHealth.Models.Form_question;
using AssetHealth.Models.Form_answer_round;
using AssetHealth.Models.Synchronization_detail;
using AssetHealth.Models.Synchronization;
using AssetHealth.Models.Synchronization_log;
using Ares.Sync.Svc;
using Ares.Sync.DTO;
using System.Collections.ObjectModel;
using Ares.DTO;
using Ares.Svc;
using AssetHealth.Common.DTO.Extensions;
using System.Web.Routing;
using AssetHealth.Common.DAL;
using AssetHealth.Models.Synchronization_device;
using Newtonsoft.Json;
using Ares.Sync.Controllers;
using Ares.Web.JSON;
using AssetHealth.Mapper;

namespace AssetHealth.Contollers
{
	
    public partial class DeviceController : BaseAresController
	{
		#region Fields

        private readonly IDeviceSvc _iDevicesvc;
        
        private readonly IUserSvc _iUsersvc;
        private readonly IFormAnswerSvc _iFormAnswersvc;
        private readonly IFormQuestionSvc _iFormQuestionsvc;
        private readonly IFormAnswerRoundSvc _iFormAnswerRoundsvc;
        private readonly IParameterItemSvc _iParameterItemsvc;
        private readonly ISynchronizationDetailSvc _iSynchronizationDetailsvc;
        private readonly ISynchronizationDeviceSvc _iSynchronizationDevicesvc;
        private readonly ISynchronizationSvc _iSynchronizationsvc;
        private readonly ISynchronizationLogSvc _iSynchronizationLogsvc;
        
        private readonly IStateProvinceSvc _stateProvinceSvc;
        private readonly ISettingSvc _settingSvc;
        private readonly FileSettings _fileSettings;
        
        private readonly IGenericAttributeSvc _genericAttributeSvc;
        private readonly ILocalizationSvc _localizationSvc;
        private readonly IPictureSvc _pictureSvc;
        private readonly IUserSvc _userSvc;
        private readonly IRoleSvc _roleSvc;
        private readonly IPermissionSvc _permissionSvc;
        private readonly IMenuSvc _menuSvc;
        private readonly IWorkContext _workContext;
        private readonly IUserRegistrationSvc _userRegistrationSvc;
        private readonly IAuthenticationSvc _authenticationSvc;
        private readonly IWorkflowMessageSvc _workflowMessageSvc;
        private readonly INewsLetterSubscriptionSvc _newsLetterSubscriptionSvc;
        private readonly IWebHelper _webHelper;
        private readonly UserSettings _userSettings;
        private readonly LocalizationSettings _localizationSettings;
        private readonly MediaSettings _mediaSettings;

	    #endregion

		#region Constructors

        public DeviceController(IDeviceSvc Devicesvc,
        IUserSvc Usersvc,
        IFormAnswerSvc FormAnswersvc,
        IFormQuestionSvc FormQuestionsvc,
        IFormAnswerRoundSvc FormAnswerRoundsvc,
        IParameterItemSvc ParameterItemsvc,
        ISynchronizationDetailSvc SynchronizationDetailsvc,
        ISynchronizationDeviceSvc SynchronizationDevicesvc,
        ISynchronizationSvc Synchronizationsvc,
        ISynchronizationLogSvc SynchronizationLogsvc,
            IStateProvinceSvc stateProvinceSvc, IGenericAttributeSvc genericAttributeSvc, ILocalizationSvc localizationSvc,  
            UserSettings userSettings, IPictureSvc pictureSvc, ISettingSvc settingSvc, FileSettings fileSettings,
            IUserSvc userSvc, IRoleSvc roleSvc, IPermissionSvc permissionSvc, IMenuSvc menuSvc, IWorkContext workContext, IUserRegistrationSvc userRegistrationSvc, IAuthenticationSvc authenticationSvc, IWorkflowMessageSvc workflowMessageSvc,
            INewsLetterSubscriptionSvc newsLetterSubscriptionSvc, IWebHelper webHelper, LocalizationSettings localizationSettings, MediaSettings mediaSettings)
		{
            this._iDevicesvc = Devicesvc;
            
            this._iUsersvc = Usersvc;
            this._iFormAnswersvc = FormAnswersvc;
            this._iFormQuestionsvc = FormQuestionsvc;
            this._iFormAnswerRoundsvc = FormAnswerRoundsvc;
            this._iParameterItemsvc = ParameterItemsvc;
            this._iSynchronizationDetailsvc = SynchronizationDetailsvc;
            this._iSynchronizationDevicesvc = SynchronizationDevicesvc;
            this._iSynchronizationsvc = Synchronizationsvc;
            this._iSynchronizationLogsvc = SynchronizationLogsvc;
            this._settingSvc = settingSvc;
            this._fileSettings = fileSettings;
            
            this._stateProvinceSvc = stateProvinceSvc;
            this._genericAttributeSvc = genericAttributeSvc;
            this._pictureSvc = pictureSvc;
            this._localizationSvc = localizationSvc;
            this._userSettings = userSettings;
            this._userSvc = userSvc;
            this._roleSvc = roleSvc;
            this._permissionSvc = permissionSvc;
            this._menuSvc = menuSvc;
            this._workContext = workContext;
            this._userRegistrationSvc = userRegistrationSvc;
            this._authenticationSvc = authenticationSvc;
            this._workflowMessageSvc = workflowMessageSvc;
            this._newsLetterSubscriptionSvc = newsLetterSubscriptionSvc;
            this._webHelper = webHelper;
            this._localizationSettings = localizationSettings;
            this._mediaSettings = mediaSettings;
		}

		#endregion 

        #region Utilities
        
        
        #endregion

        #region Device methods

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        [AresAuthorize(ResourceKey = "Admin.Device.Read")]
        public ActionResult List()
        {
            DeviceModel model = new DeviceModel();
            //model. = _roleSvc.GetAll().ToList().Select(x => x.ToModel()).ToList();
            FillDropDowns(model);
            
            
            //FillDropDownFormAnswer(model.Filter_FormAnswerModel);
                       
            
            //FillDropDownFormAnswerRound(model.Filter_FormAnswerRoundModel);
                       
            
            //FillDropDownSynchronization(model.Filter_SynchronizationModel);
                       
            
            //FillDropDownSynchronizationDevice(model.Filter_SynchronizationDeviceModel);
                       
            
            //FillDropDownSynchronizationLog(model.Filter_SynchronizationLogModel);
                       
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.Device.Read")]
        public ActionResult ListDeviceDatatable(Ares.Web.Razor.Extensions.Datatable.Model.DTParameterModel model)
        {
            var models = _iDevicesvc.GetAll();

            /*
            if (!_workContext.CurrentUser.IsAdmin())
            {
                int userId = _workContext.CurrentUser.Id;
                models = models.Where(d => d.User_From_DeviceUserMapping.Any(dum => dum.Id == userId));
            }
              */  

            if (model.Search != null && !string.IsNullOrEmpty(model.Search.Value))
            {
                models = models.Where(m => m.Device_name.Contains(model.Search.Value));
            }
            
            if(model.IsSearchValueExist("EnumDevice_type_Text"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("EnumDevice_type_Text"));
                models = models.Where(m => m.Device_type.HasValue && (int)m.Device_type == tmpValue);
            }
            if(model.IsSearchValueExist("Udid"))
            {
                string tmpValue = model.GetSearchValue("Udid");
                models = models.Where(m => m.Udid.Contains(tmpValue));
            }
            if(model.IsSearchValueExist("Device_name"))
            {
                string tmpValue = model.GetSearchValue("Device_name");
                models = models.Where(m => m.Device_name.Contains(tmpValue));
            }
            if(model.IsSearchValueExist("Device_model"))
            {
                string tmpValue = model.GetSearchValue("Device_model");
                models = models.Where(m => m.Device_model.Contains(tmpValue));
            }
            if(model.IsSearchValueExist("Platform_type_Text"))
            {
                
            }
            if(model.IsSearchValueExist("Os_version"))
            {
                string tmpValue = model.GetSearchValue("Os_version");
                models = models.Where(m => m.Os_version.Contains(tmpValue));
            }
            if(model.IsSearchValueExist("Current_carrier_network"))
            {
                string tmpValue = model.GetSearchValue("Current_carrier_network");
                models = models.Where(m => m.Current_carrier_network.Contains(tmpValue));
            }
            if(model.IsSearchValueExist("Last_contact_time"))
            {
                
                
            }
            if(model.IsSearchValueExist("Battery_level"))
            {
                string tmpValue = model.GetSearchValue("Battery_level");
                models = models.Where(m => m.Battery_level.Contains(tmpValue));
            }
            if(model.IsSearchValueExist("Scan_status"))
            {
                
            }
            if(model.IsSearchValueExist("Agent_type"))
            {
                
            }
            if(model.IsSearchValueExist("Username"))
            {
                string tmpValue = model.GetSearchValue("Username");
                models = models.Where(m => m.Username.Contains(tmpValue));
            }
            if(model.IsSearchValueExist("Mobile_number"))
            {
                string tmpValue = model.GetSearchValue("Mobile_number");
                models = models.Where(m => m.Mobile_number.Contains(tmpValue));
            }
            if(model.IsSearchValueExist("Image_path"))
            {
                string tmpValue = model.GetSearchValue("Image_path");
                models = models.Where(m => m.Image_path.Contains(tmpValue));
            }
            
            
            DTOrder order = null;
            if (model.Order != null && model.Order.Count() > 0)
            {
                order = model.Order.First();
                var pi = typeof(Device).GetProperty(model.Columns.ToList()[order.Column].Data);
                if(order.Dir == "asc")
                    models = models.OrderBy(x => pi.GetValue(x, null));
                else
                    models = models.OrderByDescending(x => pi.GetValue(x, null));
                
            }

            var totalRecords = models.Count();
            
            models = models.OrderBy(e => e.Id).Skip(model.Length * ((int)(Math.Ceiling((double)model.Start / model.Length)) + 1 - 1)).Take(model.Length);

            JsonResult jsonRes = Json(new
            {
                sEcho = model.Draw,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                //aaData = result
                aaData = models.ToList().Select(x => { var m = x.ToModel(ListMapperConfiguration.CONFIGURATION_NAME);  return m;})
            }, JsonRequestBehavior.AllowGet);
            return jsonRes;
        }
        
        [AresAuthorize(ResourceKey = "Admin.Device.Create")]
        public ActionResult Create()
        {
            DeviceModel model = new DeviceModel();
            FillDropDowns(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.Device.Create")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Create([Bind(Exclude = "Filter_FormAnswerModel,Filter_FormAnswerRoundModel,Filter_SynchronizationModel,Filter_SynchronizationDeviceModel,Filter_SynchronizationLogModel")] DeviceModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {                
                var record = model.ToEntity();

                record.Insert_user_id = _workContext.CurrentUser.Id.ToString();
                record.Insert_datetime = StringHelper.ConvertDateTimeToString(DateTime.Now);
                if (string.IsNullOrEmpty(record.Udid))
                    record.Udid = "";

                record.Object_id = Guid.NewGuid().ToString();
                record.Active = true;
                record.User_From_DeviceUserMapping = FillManyToManyValues<User>(record.User_From_DeviceUserMapping, model.UserList, _iUsersvc);

                record = _iDevicesvc.Insert(record);

                SuccessNotification(_localizationSvc.GetResource("Notifications.Device.Created"));
                
                if (continueEditing)
                    return RedirectToAction("Edit", new { Id = record.Id });

                return RedirectToAction("List");
            }
            else
            {
                FillDropDowns(model);
                return View(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.Device.Update")]
        //[DeviceAuthorize]
        public ActionResult Edit(int id, string SelectedTab)
        {
            Device record = _iDevicesvc.GetById(id);
            var model = record.ToModel();
            model.SelectedTab = SelectedTab;

            model.UserList = GetSelectedItemsStringArray<User>(record.User_From_DeviceUserMapping);

            FillDropDowns(model);
            
            //FillDropDownFormAnswer(model.Filter_FormAnswerModel);
                       
            
            //FillDropDownFormAnswerRound(model.Filter_FormAnswerRoundModel);
                       
            
            //FillDropDownSynchronization(model.Filter_SynchronizationModel);
                       
            
            //FillDropDownSynchronizationDevice(model.Filter_SynchronizationDeviceModel);
                       
            
            //FillDropDownSynchronizationLog(model.Filter_SynchronizationLogModel);
                       
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.Device.Update")]
        //[DeviceAuthorize]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Edit([Bind(Exclude = "Filter_FormAnswerModel,Filter_FormAnswerRoundModel,Filter_SynchronizationModel,Filter_SynchronizationDeviceModel,Filter_SynchronizationLogModel")] DeviceModel model, bool continueEditing)
        {
            var item = _iDevicesvc.GetById(model.Id);
            Device originalItem = (Device)item.Clone();
            if (item == null)
                //No country found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                item = model.ToEntity(item);

                var newUserList = FillManyToManyValues<User>(item.User_From_DeviceUserMapping, model.UserList, _iUsersvc);
                string newTenantId = null;
                if (newUserList != null && newUserList.Count > 0 && _workContext.IsAdmin && item.User_From_DeviceUserMapping != newUserList)
                {
                    newTenantId = newUserList[0].tenant_id;
                }

                item.User_From_DeviceUserMapping = newUserList;
                

                item.Update_user_id = _workContext.CurrentUser.Id.ToString();
                item.Update_datetime = StringHelper.ConvertDateTimeToString(DateTime.Now);

                _iDevicesvc.Update(item);

                if (!string.IsNullOrEmpty(newTenantId))
                {
                    _iDevicesvc.ExecuteSqlCommand(string.Format(HardCodedSQL.DEVICE_UPDATE_TENANT_ID, newTenantId, item.Id ));
                }

                SuccessNotification(_localizationSvc.GetResource("Notifications.Device.Updated"));
                
                if(continueEditing)
                    return RedirectToAction("Edit", new { Id = item.Id });

                return RedirectToAction("List");
            }
            else
            {
                FillDropDowns(model);
            
            //FillDropDownFormAnswer(model.Filter_FormAnswerModel);
                       
            
            //FillDropDownFormAnswerRound(model.Filter_FormAnswerRoundModel);
                       
            
            //FillDropDownSynchronization(model.Filter_SynchronizationModel);
                       
            
            //FillDropDownSynchronizationDevice(model.Filter_SynchronizationDeviceModel);
                       
            
            //FillDropDownSynchronizationLog(model.Filter_SynchronizationLogModel);
                       
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }
       
        
        

        [AresAuthorize(ResourceKey = "Admin.Device.Delete")]
        [DeviceAuthorize]
        public ActionResult Delete(int id)
        {

            var item = _iDevicesvc.GetById(id);
            if (item == null)
                return RedirectToAction("List");

            try
            {
                //item.DEVICEForm_answer.ToList().ForEach(d => _iFormAnswersvc.Delete(d));
                //item.DEVICEForm_answer_round.ToList().ForEach(d => _iFormAnswerRoundsvc.Delete(d));
                item.DEVICESynchronization.ToList().ForEach(d => _iSynchronizationsvc.Delete(d));
                item.DEVICESynchronization_device.ToList().ForEach(d => _iSynchronizationDevicesvc.Delete(d));
                item.DEVICESynchronization_log.ToList().ForEach(d => _iSynchronizationLogsvc.Delete(d));
                
                _iDevicesvc.Delete(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.Device.Deleted"));
                return RedirectToAction("List");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("Edit", new { id = item.Id });
            }
        }
        
        
        public ActionResult CreateOrUpdate(string itemId, string imageUrl, string mimeType, string uploadFilename)
        {
            DeviceModel response = null;
            

            int nItemId = -1;
            int.TryParse(itemId, out nItemId);

            if (nItemId != -1)
            {
                Device record = _iDevicesvc.GetById(nItemId);
                response = record.ToModel();
                if (record != null) // update
                {

                   

                }
                else // insert mode (after image upload popup close)
                {
                    response = new DeviceModel();
                    //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() { Text = "" } };
                }
            }
            else // insert
            {
                response = new DeviceModel();
                //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() {Text = "" }  };
                
            }

 
            ViewBag.itemId = nItemId;

            return View(response);
        }
        
        
        public void FillDropDowns(DeviceModel model)
        {
            if(_workContext.IsAdmin)
                model.Users_SelectList = new SelectList(_iUsersvc.SelectCustomQuery<User>(HardCodedSQL.DEVICE_SQLECT_ALL_USERS).ToList().Select(u => new { Id = u.Id, Title = u.FirstName + " " + u.LastName + " - " + u.Username }), "Id", "Title");
            else
                model.Users_SelectList = new SelectList(_iUsersvc.GetAll().ToList().Select(u => new { Id = u.Id, Title = u.FirstName + " " + u.LastName + " - " + u.Username }), "Id", "Title");
        }
        
        #endregion
                         



            
        #region  FormAnswer methods
        
        public void FillDropDownFormAnswer(FormAnswerModel model)
        {

            model.Device_id_Device_SelectList = new SelectList(_iDevicesvc.GetAllFromCache().ToList(), "Id", "Udid");

            model.Previous_answer_FormAnswer_SelectList = new SelectList(_iFormAnswersvc.GetAll().ToList(), "Id", "Object_id");

            model.Form_question_id_FormQuestion_SelectList = new SelectList(_iFormQuestionsvc.GetAll().ToList(), "Id", "Object_id");

            model.Form_answer_round_id_FormAnswerRound_SelectList = new SelectList(_iFormAnswerRoundsvc.GetAll().ToList(), "Id", "Object_id");

            model.Warning_type_ParameterItem_SelectList = new SelectList(_iParameterItemsvc.GetAllFromCache().ToList(), "Id", "Code");
        }
        
        public ActionResult CreateOrUpdateFormAnswer(string itemId, string imageUrl, string mimeType, string uploadFilename)
        {
            FormAnswerModel response = null;


            int nItemId = -1;
            int.TryParse(itemId, out nItemId);

            if (nItemId != -1)
            {
                FormAnswer record = _iFormAnswersvc.GetById(nItemId);
                response = record.ToModel();
                if (record != null) // update
                {



                    //response.RoleListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem { Text = "Administrators" }, new Ext.Net.ListItem { Text = "Guests" } };
                    //response.PermissionListItems = permListItems.ToArray();

                }
                else // insert mode (after image upload popup close)
                {
                    response = new FormAnswerModel();
                    //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() { Text = "" } };
                }
            }
            else // insert
            {
                response = new FormAnswerModel();
                //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() {Text = "" }  };

            }


            ViewBag.itemId = nItemId;

            return View(response);
        }
        
        [AresAuthorize(ResourceKey = "Admin.FormAnswer.Read")]
        public ActionResult ListFormAnswerDatatable(Ares.Web.Razor.Extensions.Datatable.Model.DTParameterModel model, int? DeviceModelId)
        {
            var models = _iFormAnswersvc.GetAll();
            models = models.Where(m => m.Device_id == DeviceModelId);
            if (model.Search != null && !string.IsNullOrEmpty(model.Search.Value))
            {
                models = models.Where(m => m.Object_id.Contains(model.Search.Value));
            }
            
            if (model.IsSearchValueExist("Form_question_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Form_question_id"));
                models = models.Where(m => m.Form_question_id == tmpValue);
            }
            if (model.IsSearchValueExist("Device_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Device_id"));
                models = models.Where(m => m.Device_id == tmpValue);
            }
            if (model.IsSearchValueExist("Form_answer_round_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Form_answer_round_id"));
                models = models.Where(m => m.Form_answer_round_id == tmpValue);
            }
            if (model.IsSearchValueExist("Previous_answer"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Previous_answer"));
                models = models.Where(m => m.Previous_answer == tmpValue);
            }
            if (model.IsSearchValueExist("Username"))
            {
                string tmpValue = model.GetSearchValue("Username");
                models = models.Where(m => m.Username.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Read_no"))
            {
                long tmpValue = long.Parse(model.GetSearchValue("Read_no"));
                models = models.Where(m => m.Read_no == tmpValue);
            }
            if (model.IsSearchValueExist("Warning_type"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Warning_type"));
                models = models.Where(m => m.Warning_type == tmpValue);
            }
            if (model.IsSearchValueExist("Answer"))
            {
                string tmpValue = model.GetSearchValue("Answer");
                models = models.Where(m => m.Answer.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Long_answer"))
            {
            }
            if (model.IsSearchValueExist("Converted_answer"))
            {
                string tmpValue = model.GetSearchValue("Converted_answer");
                models = models.Where(m => m.Converted_answer.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Json"))
            {
            }
            if (model.IsSearchValueExist("Required_read_datetime"))
            {
                
                
            }
            if (model.IsSearchValueExist("Actual_read_datetime"))
            {
                
                
            }
            if (model.IsSearchValueExist("Description"))
            {
                string tmpValue = model.GetSearchValue("Description");
                models = models.Where(m => m.Description.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Notes"))
            {
            }
            if (model.IsSearchValueExist("Udid"))
            {
                string tmpValue = model.GetSearchValue("Udid");
                models = models.Where(m => m.Udid.Contains(tmpValue));
            }
            
            DTOrder order = null;
            if (model.Order != null && model.Order.Count() > 0)
            {
                order = model.Order.First();
                var pi = typeof(FormAnswer).GetProperty(model.Columns.ToList()[order.Column].Data);
                if(order.Dir == "asc")
                    models = models.OrderBy(x => pi.GetValue(x, null));
                else
                    models = models.OrderByDescending(x => pi.GetValue(x, null));
                
            }

            var totalRecords = models.Count();
            
            models = models.OrderBy(e => e.Id).Skip(model.Length * ((int)(Math.Ceiling((double)model.Start / model.Length)) + 1 - 1)).Take(model.Length);

            JsonResult jsonRes = Json(new
            {
                sEcho = model.Draw,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                //aaData = result
                aaData = models.ToList().Select(x => { var m = x.ToModel(); /*m.Device_id_Text = x.ANSWER_DEVICE == null ? "" : x.ANSWER_DEVICE.Udid;*/ m.Previous_answer_Text = x.ANSWER_PREVIOUS_ANSWER == null ? "" : x.ANSWER_PREVIOUS_ANSWER.Object_id; m.Form_question_id_Text = x.ANSWER_QUESTION == null ? "" : x.ANSWER_QUESTION.Object_id; m.Form_answer_round_id_Text = x.ANSWER_ROUND == null ? "" : x.ANSWER_ROUND.Object_id; m.Warning_type_Text = x.ANSWER_WARNING_PARAMITEMS == null ? "" : x.ANSWER_WARNING_PARAMITEMS.Code;  return m;})
            }, JsonRequestBehavior.AllowGet);
            return jsonRes;
        }
        
        [AresAuthorize(ResourceKey = "Admin.FormAnswer.Create")]
        public ActionResult CreateFormAnswer(int DeviceModelId)
        {
            FormAnswerModel model = new FormAnswerModel();
            model.Device_id = DeviceModelId;
            
            
            FillDropDownFormAnswer(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormAnswer.Create")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult CreateFormAnswer( FormAnswerModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {                
                var record = model.ToEntity();
                
                record.Object_id = Guid.NewGuid().ToString();
                record.Insert_user_id = _workContext.CurrentUser.Id.ToString();
                record.Insert_datetime = StringHelper.ConvertDateTimeToString(DateTime.Now);
                
                
                record = _iFormAnswersvc.Insert(record);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormAnswer.Created"));
                
                if (continueEditing)
                    return RedirectToAction("EditFormAnswer", new { id = record.Id });

                return RedirectToAction("Edit", new  { id = model.Device_id, SelectedTab = "2" });
            }
            else
            {
                FillDropDownFormAnswer(model);
                return View(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }
        
        public ActionResult EditFormAnswer(int id, string SelectedTab)
        {
            FormAnswer record = _iFormAnswersvc.GetById(id);
            var model = record.ToModel();
            model.SelectedTab = SelectedTab;
            
            
            
            FillDropDownFormAnswer(model);

            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormAnswer.Update")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult EditFormAnswer( FormAnswerModel model, bool continueEditing)
        {
            var item = _iFormAnswersvc.GetById(model.Id);
            FormAnswer originalItem = (FormAnswer)item.Clone();
            if (item == null)
                //No country found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                item = model.ToEntity(item);
                item.Object_id = originalItem.Object_id;                
                item.Insert_user_id = originalItem.Insert_user_id;                
                item.Insert_datetime = originalItem.Insert_datetime;                
                
                
                
                item.Update_user_id = _workContext.CurrentUser.Id.ToString();
                item.Update_datetime = StringHelper.ConvertDateTimeToString(DateTime.Now);

                _iFormAnswersvc.Update(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormAnswer.Updated"));
                
                if(continueEditing)
                    return RedirectToAction("EditFormAnswer", new { id = item.Id });
                
                return RedirectToAction("Edit", new  { id = model.Device_id, SelectedTab = "2" });
            }

            //If we got this far, something failed, redisplay form
            FillDropDownFormAnswer(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormAnswer.Delete")]
        public ActionResult DeleteFormAnswer(int id)
        {

            var item = _iFormAnswersvc.GetById(id);
            if (item == null)
                return RedirectToAction("List");

            try
            {
                
                _iFormAnswersvc.Delete(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormAnswer.Deleted"));
                
                return RedirectToAction("Edit", new  { id = item.Device_id, SelectedTab = "2" });
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("EditFormAnswer", new { id = item.Id });
            }
        }
        #endregion 
          



        





            
        #region  FormAnswerRound methods
        
        public void FillDropDownFormAnswerRound(FormAnswerRoundModel model)
        {

            //model.Device_id_Device_SelectList = new SelectList(_iDevicesvc.GetAll().ToList(), "Id", "Udid");

            model.Period_ParameterItem_SelectList = new SelectList(_iParameterItemsvc.GetAllFromCache().ToList(), "Id", "Code");

            model.Warning_type_ParameterItem_SelectList = new SelectList(_iParameterItemsvc.GetAllFromCache().ToList(), "Id", "Code");
        }
        
        public ActionResult CreateOrUpdateFormAnswerRound(string itemId, string imageUrl, string mimeType, string uploadFilename)
        {
            FormAnswerRoundModel response = null;


            int nItemId = -1;
            int.TryParse(itemId, out nItemId);

            if (nItemId != -1)
            {
                FormAnswerRound record = _iFormAnswerRoundsvc.GetById(nItemId);
                response = record.ToModel();
                if (record != null) // update
                {



                    //response.RoleListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem { Text = "Administrators" }, new Ext.Net.ListItem { Text = "Guests" } };
                    //response.PermissionListItems = permListItems.ToArray();

                }
                else // insert mode (after image upload popup close)
                {
                    response = new FormAnswerRoundModel();
                    //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() { Text = "" } };
                }
            }
            else // insert
            {
                response = new FormAnswerRoundModel();
                //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() {Text = "" }  };

            }


            ViewBag.itemId = nItemId;

            return View(response);
        }
        
        [AresAuthorize(ResourceKey = "Admin.FormAnswerRound.Read")]
        public ActionResult ListFormAnswerRoundDatatable(Ares.Web.Razor.Extensions.Datatable.Model.DTParameterModel model, int? DeviceModelId)
        {
            var models = _iFormAnswerRoundsvc.GetAll();
            models = models.Where(m => m.Device_id == DeviceModelId);
            if (model.Search != null && !string.IsNullOrEmpty(model.Search.Value))
            {
                models = models.Where(m => m.Object_id.Contains(model.Search.Value));
            }
            
            if (model.IsSearchValueExist("Device_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Device_id"));
                models = models.Where(m => m.Device_id == tmpValue);
            }
            if (model.IsSearchValueExist("Username"))
            {
                string tmpValue = model.GetSearchValue("Username");
                models = models.Where(m => m.Username.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Read_no"))
            {
                long tmpValue = long.Parse(model.GetSearchValue("Read_no"));
                models = models.Where(m => m.Read_no == tmpValue);
            }
            if (model.IsSearchValueExist("Start_read_datetime"))
            {
                
                
            }
            if (model.IsSearchValueExist("End_read_datetime"))
            {
                
                
            }
            if (model.IsSearchValueExist("Last_read_datetime"))
            {
                
                
            }
            if (model.IsSearchValueExist("Warning_type"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Warning_type"));
                models = models.Where(m => m.Warning_type == tmpValue);
            }
            if (model.IsSearchValueExist("Period"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Period"));
                models = models.Where(m => m.Period == tmpValue);
            }
            if (model.IsSearchValueExist("Json"))
            {
            }
            if (model.IsSearchValueExist("Start_count"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Start_count"));
                models = models.Where(m => m.Start_count == tmpValue);
            }
            if (model.IsSearchValueExist("File_count"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("File_count"));
                models = models.Where(m => m.File_count == tmpValue);
            }
            if (model.IsSearchValueExist("Required_read_datetime"))
            {
                
                
            }
            if (model.IsSearchValueExist("Description"))
            {
                string tmpValue = model.GetSearchValue("Description");
                models = models.Where(m => m.Description.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Notes"))
            {
            }
            
            DTOrder order = null;
            if (model.Order != null && model.Order.Count() > 0)
            {
                order = model.Order.First();
                var pi = typeof(FormAnswerRound).GetProperty(model.Columns.ToList()[order.Column].Data);
                if(order.Dir == "asc")
                    models = models.OrderBy(x => pi.GetValue(x, null));
                else
                    models = models.OrderByDescending(x => pi.GetValue(x, null));
                
            }

            var totalRecords = models.Count();
            
            models = models.OrderBy(e => e.Id).Skip(model.Length * ((int)(Math.Ceiling((double)model.Start / model.Length)) + 1 - 1)).Take(model.Length);

            JsonResult jsonRes = Json(new
            {
                sEcho = model.Draw,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                //aaData = result
                aaData = models.ToList().Select(x => { var m = x.ToModel(); /*m.Device_id_Text = x.ANSWER_ROUND_DEVICE == null ? "" : x.ANSWER_ROUND_DEVICE.Udid;*/ m.Period_Text = x.ANSWER_ROUND_PERIOD_PARAMITEMS == null ? "" : x.ANSWER_ROUND_PERIOD_PARAMITEMS.Code; m.Warning_type_Text = x.ANSWER_ROUND_WARNING_PARAMITEMS == null ? "" : x.ANSWER_ROUND_WARNING_PARAMITEMS.Code;  return m;})
            }, JsonRequestBehavior.AllowGet);
            return jsonRes;
        }
        
        [AresAuthorize(ResourceKey = "Admin.FormAnswerRound.Create")]
        public ActionResult CreateFormAnswerRound(int DeviceModelId)
        {
            FormAnswerRoundModel model = new FormAnswerRoundModel();
            model.Device_id = DeviceModelId;
            
            
            FillDropDownFormAnswerRound(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormAnswerRound.Create")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult CreateFormAnswerRound([Bind(Exclude = "Filter_FormAnswerModel")] FormAnswerRoundModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {                
                var record = model.ToEntity();
                
                record.Object_id = Guid.NewGuid().ToString();
                
                
                record = _iFormAnswerRoundsvc.Insert(record);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormAnswerRound.Created"));
                
                if (continueEditing)
                    return RedirectToAction("EditFormAnswerRound", new { id = record.Id });

                return RedirectToAction("Edit", new  { id = model.Device_id, SelectedTab = "3" });
            }
            else
            {
                FillDropDownFormAnswerRound(model);
                return View(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }
        
        public ActionResult EditFormAnswerRound(int id, string SelectedTab)
        {
            FormAnswerRound record = _iFormAnswerRoundsvc.GetById(id);
            var model = record.ToModel();
            model.SelectedTab = SelectedTab;
            
            
            
            FillDropDownFormAnswerRound(model);
            
            FillDropDownFormAnswer(model.Filter_FormAnswerModel);
                       

            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormAnswerRound.Update")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult EditFormAnswerRound([Bind(Exclude = "Filter_FormAnswerModel")] FormAnswerRoundModel model, bool continueEditing)
        {
            var item = _iFormAnswerRoundsvc.GetById(model.Id);
            FormAnswerRound originalItem = (FormAnswerRound)item.Clone();
            if (item == null)
                //No country found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                item = model.ToEntity(item);
                item.Object_id = originalItem.Object_id;                
                
                
                

                _iFormAnswerRoundsvc.Update(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormAnswerRound.Updated"));
                
                if(continueEditing)
                    return RedirectToAction("EditFormAnswerRound", new { id = item.Id });
                
                return RedirectToAction("Edit", new  { id = model.Device_id, SelectedTab = "3" });
            }

            //If we got this far, something failed, redisplay form
            FillDropDownFormAnswerRound(model);
            
            FillDropDownFormAnswer(model.Filter_FormAnswerModel);
                       
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormAnswerRound.Delete")]
        public ActionResult DeleteFormAnswerRound(int id)
        {

            var item = _iFormAnswerRoundsvc.GetById(id);
            if (item == null)
                return RedirectToAction("List");

            try
            {
                item.FORM_ANSWER_ROUNDForm_answer.ToList().ForEach(d => _iFormAnswersvc.Delete(d));
                item.FORM_ANSWER_ROUNDForm_answer.ToList().ForEach(d => _iFormAnswersvc.Delete(d));
                
                _iFormAnswerRoundsvc.Delete(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormAnswerRound.Deleted"));
                
                return RedirectToAction("Edit", new  { id = item.Device_id, SelectedTab = "3" });
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("EditFormAnswerRound", new { id = item.Id });
            }
        }
        #endregion 
          



        





            
        #region  Synchronization methods
        
        public void FillDropDownSynchronization(SynchronizationModel model)
        {

            model.Device_id_Device_SelectList = new SelectList(_iDevicesvc.GetAll().ToList(), "Id", "Udid");
        }
        
        public ActionResult CreateOrUpdateSynchronization(string itemId, string imageUrl, string mimeType, string uploadFilename)
        {
            SynchronizationModel response = null;


            int nItemId = -1;
            int.TryParse(itemId, out nItemId);

            if (nItemId != -1)
            {
                Synchronization record = _iSynchronizationsvc.GetById(nItemId);
                response = record.ToModel();
                if (record != null) // update
                {



                    //response.RoleListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem { Text = "Administrators" }, new Ext.Net.ListItem { Text = "Guests" } };
                    //response.PermissionListItems = permListItems.ToArray();

                }
                else // insert mode (after image upload popup close)
                {
                    response = new SynchronizationModel();
                    //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() { Text = "" } };
                }
            }
            else // insert
            {
                response = new SynchronizationModel();
                //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() {Text = "" }  };

            }


            ViewBag.itemId = nItemId;

            return View(response);
        }
        
        [AresAuthorize(ResourceKey = "Admin.Synchronization.Read")]
        public ActionResult ListSynchronizationDatatable(Ares.Web.Razor.Extensions.Datatable.Model.DTParameterModel model, int? DeviceModelId)
        {
            var models = _iSynchronizationsvc.GetAll();
            models = models.Where(m => m.Device_id == DeviceModelId);
            if (model.Search != null && !string.IsNullOrEmpty(model.Search.Value))
            {
                models = models.Where(m => m.Object_id.Contains(model.Search.Value));
            }
            
            if (model.IsSearchValueExist("Device_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Device_id"));
                models = models.Where(m => m.Device_id == tmpValue);
            }
            if (model.IsSearchValueExist("Udid"))
            {
                string tmpValue = model.GetSearchValue("Udid");
                models = models.Where(m => m.Udid.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Username"))
            {
                string tmpValue = model.GetSearchValue("Username");
                models = models.Where(m => m.Username.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Sync_type"))
            {
                
            }
            if (model.IsSearchValueExist("Status"))
            {
                
            }
            if (model.IsSearchValueExist("Start_datetime"))
            {
                
                
            }
            if (model.IsSearchValueExist("Finish_datetime"))
            {
                
                
            }
            if (model.IsSearchValueExist("Client_version"))
            {
                string tmpValue = model.GetSearchValue("Client_version");
                models = models.Where(m => m.Client_version.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Server_version"))
            {
                string tmpValue = model.GetSearchValue("Server_version");
                models = models.Where(m => m.Server_version.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Description"))
            {
                string tmpValue = model.GetSearchValue("Description");
                models = models.Where(m => m.Description.Contains(tmpValue));
            }
            
            DTOrder order = null;
            if (model.Order != null && model.Order.Count() > 0)
            {
                order = model.Order.First();
                var pi = typeof(Synchronization).GetProperty(model.Columns.ToList()[order.Column].Data);
                if(order.Dir == "asc")
                    models = models.OrderBy(x => pi.GetValue(x, null));
                else
                    models = models.OrderByDescending(x => pi.GetValue(x, null));
                
            }

            var totalRecords = models.Count();
            
            models = models.OrderBy(e => e.Id).Skip(model.Length * ((int)(Math.Ceiling((double)model.Start / model.Length)) + 1 - 1)).Take(model.Length);

            JsonResult jsonRes = Json(new
            {
                sEcho = model.Draw,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                //aaData = result
                aaData = models.ToList().Select(x => { var m = x.ToModel(); m.Device_id_Text = x.SYNCHRONIZATION_DEVICE == null ? "" : x.SYNCHRONIZATION_DEVICE.Udid;  return m;})
            }, JsonRequestBehavior.AllowGet);
            return jsonRes;
        }
        
        [AresAuthorize(ResourceKey = "Admin.Synchronization.Create")]
        public ActionResult CreateSynchronization(int DeviceModelId)
        {
            SynchronizationModel model = new SynchronizationModel();
            model.Device_id = DeviceModelId;
            
            
            FillDropDownSynchronization(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.Synchronization.Create")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult CreateSynchronization([Bind(Exclude = "Filter_SynchronizationDetailModel,Filter_SynchronizationDeviceModel")] SynchronizationModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {                
                var record = model.ToEntity();
                
                record.Object_id = Guid.NewGuid().ToString();
                record.Insert_user_id = _workContext.CurrentUser.Id.ToString();
                record.Insert_datetime = StringHelper.ConvertDateTimeToString(DateTime.Now);
                
                
                record = _iSynchronizationsvc.Insert(record);

                SuccessNotification(_localizationSvc.GetResource("Notifications.Synchronization.Created"));
                
                if (continueEditing)
                    return RedirectToAction("EditSynchronization", new { id = record.Id });

                return RedirectToAction("Edit", new  { id = model.Device_id, SelectedTab = "4" });
            }
            else
            {
                FillDropDownSynchronization(model);
                return View(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }
        
        public ActionResult EditSynchronization(int id, string SelectedTab)
        {
            Synchronization record = _iSynchronizationsvc.GetById(id);
            var model = record.ToModel();
            model.SelectedTab = SelectedTab;
            
            
            
            FillDropDownSynchronization(model);
            
            FillDropDownSynchronizationDetail(model.Filter_SynchronizationDetailModel);
                       
            
            FillDropDownSynchronizationDevice(model.Filter_SynchronizationDeviceModel);
                       

            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.Synchronization.Update")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult EditSynchronization([Bind(Exclude = "Filter_SynchronizationDetailModel,Filter_SynchronizationDeviceModel")] SynchronizationModel model, bool continueEditing)
        {
            var item = _iSynchronizationsvc.GetById(model.Id);
            Synchronization originalItem = (Synchronization)item.Clone();
            if (item == null)
                //No country found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                item = model.ToEntity(item);
                item.Object_id = originalItem.Object_id;                
                item.Insert_user_id = originalItem.Insert_user_id;                
                item.Insert_datetime = originalItem.Insert_datetime;                
                
                
                
                item.Update_user_id = _workContext.CurrentUser.Id.ToString();
                item.Update_datetime = StringHelper.ConvertDateTimeToString(DateTime.Now);

                _iSynchronizationsvc.Update(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.Synchronization.Updated"));
                
                if(continueEditing)
                    return RedirectToAction("EditSynchronization", new { id = item.Id });
                
                return RedirectToAction("Edit", new  { id = model.Device_id, SelectedTab = "4" });
            }

            //If we got this far, something failed, redisplay form
            FillDropDownSynchronization(model);
            
            FillDropDownSynchronizationDetail(model.Filter_SynchronizationDetailModel);
                       
            
            FillDropDownSynchronizationDevice(model.Filter_SynchronizationDeviceModel);
                       
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.Synchronization.Delete")]
        public ActionResult DeleteSynchronization(int id)
        {

            var item = _iSynchronizationsvc.GetById(id);
            if (item == null)
                return RedirectToAction("List");

            try
            {
                item.SYNCHRONIZATIONSynchronization_detail.ToList().ForEach(d => _iSynchronizationDetailsvc.Delete(d));
                item.SYNCHRONIZATIONSynchronization_device.ToList().ForEach(d => _iSynchronizationDevicesvc.Delete(d));
                item.SYNCHRONIZATIONSynchronization_detail.ToList().ForEach(d => _iSynchronizationDetailsvc.Delete(d));
                item.SYNCHRONIZATIONSynchronization_device.ToList().ForEach(d => _iSynchronizationDevicesvc.Delete(d));
                item.SYNCHRONIZATIONSynchronization_detail.ToList().ForEach(d => _iSynchronizationDetailsvc.Delete(d));
                item.SYNCHRONIZATIONSynchronization_device.ToList().ForEach(d => _iSynchronizationDevicesvc.Delete(d));
                
                _iSynchronizationsvc.Delete(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.Synchronization.Deleted"));
                
                return RedirectToAction("Edit", new  { id = item.Device_id, SelectedTab = "4" });
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("EditSynchronization", new { id = item.Id });
            }
        }
        #endregion 
          



        





            
        #region  SynchronizationDetail methods
        
        public void FillDropDownSynchronizationDetail(SynchronizationDetailModel model)
        {

            model.Synchronization_id_Synchronization_SelectList = new SelectList(_iSynchronizationsvc.GetAll().ToList(), "Id", "Object_id");
        }
        
        public ActionResult CreateOrUpdateSynchronizationDetail(string itemId, string imageUrl, string mimeType, string uploadFilename)
        {
            SynchronizationDetailModel response = null;


            int nItemId = -1;
            int.TryParse(itemId, out nItemId);

            if (nItemId != -1)
            {
                SynchronizationDetail record = _iSynchronizationDetailsvc.GetById(nItemId);
                response = record.ToModel();
                if (record != null) // update
                {



                    //response.RoleListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem { Text = "Administrators" }, new Ext.Net.ListItem { Text = "Guests" } };
                    //response.PermissionListItems = permListItems.ToArray();

                }
                else // insert mode (after image upload popup close)
                {
                    response = new SynchronizationDetailModel();
                    //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() { Text = "" } };
                }
            }
            else // insert
            {
                response = new SynchronizationDetailModel();
                //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() {Text = "" }  };

            }


            ViewBag.itemId = nItemId;

            return View(response);
        }
        
        [AresAuthorize(ResourceKey = "Admin.SynchronizationDetail.Read")]
        public ActionResult ListSynchronizationDetailDatatable(Ares.Web.Razor.Extensions.Datatable.Model.DTParameterModel model, int? SynchronizationModelId)
        {
            var models = _iSynchronizationDetailsvc.GetAll();
            models = models.Where(m => m.Synchronization_id == SynchronizationModelId);
            if (model.Search != null && !string.IsNullOrEmpty(model.Search.Value))
            {
                models = models.Where(m => m.Object_id.Contains(model.Search.Value));
            }
            
            if (model.IsSearchValueExist("Synchronization_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Synchronization_id"));
                models = models.Where(m => m.Synchronization_id == tmpValue);
            }
            if (model.IsSearchValueExist("Sync_detail_server_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Sync_detail_server_id"));
                models = models.Where(m => m.Sync_detail_server_id == tmpValue);
            }
            if (model.IsSearchValueExist("Status"))
            {
                
            }
            if (model.IsSearchValueExist("Table_name"))
            {
                string tmpValue = model.GetSearchValue("Table_name");
                models = models.Where(m => m.Table_name.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Server_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Server_id"));
                models = models.Where(m => m.Server_id == tmpValue);
            }
            if (model.IsSearchValueExist("Client_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Client_id"));
                models = models.Where(m => m.Client_id == tmpValue);
            }
            if (model.IsSearchValueExist("Transaction_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Transaction_id"));
                models = models.Where(m => m.Transaction_id == tmpValue);
            }
            if (model.IsSearchValueExist("Operation_type"))
            {
                
            }
            if (model.IsSearchValueExist("Result"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Result"));
                models = models.Where(m => m.Result == tmpValue);
            }
            if (model.IsSearchValueExist("Message"))
            {
            }
            if (model.IsSearchValueExist("Item"))
            {
            }
            if (model.IsSearchValueExist("Size"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Size"));
                models = models.Where(m => m.Size == tmpValue);
            }
            if (model.IsSearchValueExist("Order"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Order"));
                models = models.Where(m => m.Order == tmpValue);
            }
            if (model.IsSearchValueExist("Start_datetime"))
            {
                
                
            }
            if (model.IsSearchValueExist("End_datetime"))
            {
                
                
            }
            if (model.IsSearchValueExist("Last_exec_datetime"))
            {
                
                
            }
            if (model.IsSearchValueExist("Retry_count"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Retry_count"));
                models = models.Where(m => m.Retry_count == tmpValue);
            }
            if (model.IsSearchValueExist("Page_index"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Page_index"));
                models = models.Where(m => m.Page_index == tmpValue);
            }
            if (model.IsSearchValueExist("Page_size"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Page_size"));
                models = models.Where(m => m.Page_size == tmpValue);
            }
            if (model.IsSearchValueExist("Page_count"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Page_count"));
                models = models.Where(m => m.Page_count == tmpValue);
            }
            if (model.IsSearchValueExist("Total_item_count"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Total_item_count"));
                models = models.Where(m => m.Total_item_count == tmpValue);
            }
            if (model.IsSearchValueExist("Size_limit"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Size_limit"));
                models = models.Where(m => m.Size_limit == tmpValue);
            }
            if (model.IsSearchValueExist("Description"))
            {
            }
            if (model.IsSearchValueExist("Notes"))
            {
            }
            if (model.IsSearchValueExist("Active"))
            {
                bool tmpValue = bool.Parse(model.GetSearchValue("Active"));
                models = models.Where(m => m.Active == tmpValue);
            }
            
            DTOrder order = null;
            if (model.Order != null && model.Order.Count() > 0)
            {
                order = model.Order.First();
                var pi = typeof(SynchronizationDetail).GetProperty(model.Columns.ToList()[order.Column].Data);
                if(order.Dir == "asc")
                    models = models.OrderBy(x => pi.GetValue(x, null));
                else
                    models = models.OrderByDescending(x => pi.GetValue(x, null));
                
            }

            var totalRecords = models.Count();
            
            models = models.OrderBy(e => e.Id).Skip(model.Length * ((int)(Math.Ceiling((double)model.Start / model.Length)) + 1 - 1)).Take(model.Length);

            JsonResult jsonRes = Json(new
            {
                sEcho = model.Draw,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                //aaData = result
                aaData = models.ToList().Select(x => { var m = x.ToModel(); m.Synchronization_id_Text = x.SYNCHRONIZATION_DETAIL == null ? "" : x.SYNCHRONIZATION_DETAIL.Object_id;  return m;})
            }, JsonRequestBehavior.AllowGet);
            return jsonRes;
        }
        
        [AresAuthorize(ResourceKey = "Admin.SynchronizationDetail.Create")]
        public ActionResult CreateSynchronizationDetail(int SynchronizationModelId)
        {
            SynchronizationDetailModel model = new SynchronizationDetailModel();
            model.Synchronization_id = SynchronizationModelId;
            
            
            FillDropDownSynchronizationDetail(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.SynchronizationDetail.Create")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult CreateSynchronizationDetail([Bind(Exclude = "Filter_SynchronizationLogModel")] SynchronizationDetailModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {                
                var record = model.ToEntity();
                
                record.Object_id = Guid.NewGuid().ToString();
                
                
                record = _iSynchronizationDetailsvc.Insert(record);

                SuccessNotification(_localizationSvc.GetResource("Notifications.SynchronizationDetail.Created"));
                
                if (continueEditing)
                    return RedirectToAction("EditSynchronizationDetail", new { id = record.Id });

                return RedirectToAction("EditSynchronization", new  { id = model.Synchronization_id, SelectedTab = "2" });
            }
            else
            {
                FillDropDownSynchronizationDetail(model);
                return View(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }
        
        public ActionResult EditSynchronizationDetail(int id, string SelectedTab)
        {
            SynchronizationDetail record = _iSynchronizationDetailsvc.GetById(id);
            var model = record.ToModel();
            model.SelectedTab = SelectedTab;
            
            
            
            FillDropDownSynchronizationDetail(model);
            
            FillDropDownSynchronizationLog(model.Filter_SynchronizationLogModel);
                       

            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.SynchronizationDetail.Update")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult EditSynchronizationDetail([Bind(Exclude = "Filter_SynchronizationLogModel")] SynchronizationDetailModel model, bool continueEditing)
        {
            var item = _iSynchronizationDetailsvc.GetById(model.Id);
            SynchronizationDetail originalItem = (SynchronizationDetail)item.Clone();
            if (item == null)
                //No country found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                item = model.ToEntity(item);
                item.Object_id = originalItem.Object_id;                
                
                
                

                _iSynchronizationDetailsvc.Update(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.SynchronizationDetail.Updated"));
                
                if(continueEditing)
                    return RedirectToAction("EditSynchronizationDetail", new { id = item.Id });
                
                return RedirectToAction("EditSynchronization", new  { id = model.Synchronization_id, SelectedTab = "2" });
            }

            //If we got this far, something failed, redisplay form
            FillDropDownSynchronizationDetail(model);
            
            FillDropDownSynchronizationLog(model.Filter_SynchronizationLogModel);
                       
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.SynchronizationDetail.Delete")]
        public ActionResult DeleteSynchronizationDetail(int id)
        {

            var item = _iSynchronizationDetailsvc.GetById(id);
            if (item == null)
                return RedirectToAction("List");

            try
            {
                item.SYNCHRONIZATION_DETAILSynchronization_log.ToList().ForEach(d => _iSynchronizationLogsvc.Delete(d));
                item.SYNCHRONIZATION_DETAILSynchronization_log.ToList().ForEach(d => _iSynchronizationLogsvc.Delete(d));
                item.SYNCHRONIZATION_DETAILSynchronization_log.ToList().ForEach(d => _iSynchronizationLogsvc.Delete(d));
                item.SYNCHRONIZATION_DETAILSynchronization_log.ToList().ForEach(d => _iSynchronizationLogsvc.Delete(d));
                
                _iSynchronizationDetailsvc.Delete(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.SynchronizationDetail.Deleted"));
                
                return RedirectToAction("EditSynchronization", new  { id = item.Synchronization_id, SelectedTab = "2" });
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("EditSynchronizationDetail", new { id = item.Id });
            }
        }
        #endregion 
          



        





            
        #region  SynchronizationLog methods
        
        public void FillDropDownSynchronizationLog(SynchronizationLogModel model)
        {

            model.Synchronization_detail_id_SynchronizationDetail_SelectList = new SelectList(_iSynchronizationDetailsvc.GetAll().ToList(), "Id", "Object_id");

            model.Device_id_Device_SelectList = new SelectList(_iDevicesvc.GetAll().ToList(), "Id", "Udid");
        }
        
        public ActionResult CreateOrUpdateSynchronizationLog(string itemId, string imageUrl, string mimeType, string uploadFilename)
        {
            SynchronizationLogModel response = null;


            int nItemId = -1;
            int.TryParse(itemId, out nItemId);

            if (nItemId != -1)
            {
                SynchronizationLog record = _iSynchronizationLogsvc.GetById(nItemId);
                response = record.ToModel();
                if (record != null) // update
                {



                    //response.RoleListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem { Text = "Administrators" }, new Ext.Net.ListItem { Text = "Guests" } };
                    //response.PermissionListItems = permListItems.ToArray();

                }
                else // insert mode (after image upload popup close)
                {
                    response = new SynchronizationLogModel();
                    //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() { Text = "" } };
                }
            }
            else // insert
            {
                response = new SynchronizationLogModel();
                //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() {Text = "" }  };

            }


            ViewBag.itemId = nItemId;

            return View(response);
        }
        
        [AresAuthorize(ResourceKey = "Admin.SynchronizationLog.Read")]
        public ActionResult ListSynchronizationLogDatatable(Ares.Web.Razor.Extensions.Datatable.Model.DTParameterModel model, int? SynchronizationDetailModelId)
        {
            var models = _iSynchronizationLogsvc.GetAll();
            models = models.Where(m => m.Synchronization_detail_id == SynchronizationDetailModelId);
            if (model.Search != null && !string.IsNullOrEmpty(model.Search.Value))
            {
                models = models.Where(m => m.Item.Contains(model.Search.Value));
            }
            
            if (model.IsSearchValueExist("Synchronization_detail_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Synchronization_detail_id"));
                models = models.Where(m => m.Synchronization_detail_id == tmpValue);
            }
            if (model.IsSearchValueExist("Status"))
            {
                
            }
            if (model.IsSearchValueExist("Device_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Device_id"));
                models = models.Where(m => m.Device_id == tmpValue);
            }
            if (model.IsSearchValueExist("Client_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Client_id"));
                models = models.Where(m => m.Client_id == tmpValue);
            }
            if (model.IsSearchValueExist("Item"))
            {
            }
            if (model.IsSearchValueExist("Size"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Size"));
                models = models.Where(m => m.Size == tmpValue);
            }
            if (model.IsSearchValueExist("Message"))
            {
            }
            if (model.IsSearchValueExist("Page_index"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Page_index"));
                models = models.Where(m => m.Page_index == tmpValue);
            }
            if (model.IsSearchValueExist("Page_size"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Page_size"));
                models = models.Where(m => m.Page_size == tmpValue);
            }
            if (model.IsSearchValueExist("Size_limit"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Size_limit"));
                models = models.Where(m => m.Size_limit == tmpValue);
            }
            if (model.IsSearchValueExist("Retry_count"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Retry_count"));
                models = models.Where(m => m.Retry_count == tmpValue);
            }
            if (model.IsSearchValueExist("Version"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Version"));
                models = models.Where(m => m.Version == tmpValue);
            }
            if (model.IsSearchValueExist("Start_datetime"))
            {
                
                
            }
            if (model.IsSearchValueExist("End_datetime"))
            {
                
                
            }
            if (model.IsSearchValueExist("Description"))
            {
            }
            
            DTOrder order = null;
            if (model.Order != null && model.Order.Count() > 0)
            {
                order = model.Order.First();
                var pi = typeof(SynchronizationLog).GetProperty(model.Columns.ToList()[order.Column].Data);
                if(order.Dir == "asc")
                    models = models.OrderBy(x => pi.GetValue(x, null));
                else
                    models = models.OrderByDescending(x => pi.GetValue(x, null));
                
            }

            var totalRecords = models.Count();
            
            models = models.OrderBy(e => e.Id).Skip(model.Length * ((int)(Math.Ceiling((double)model.Start / model.Length)) + 1 - 1)).Take(model.Length);

            JsonResult jsonRes = Json(new
            {
                sEcho = model.Draw,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                //aaData = result
                aaData = models.ToList().Select(x => { var m = x.ToModel(); m.Synchronization_detail_id_Text = x.SYNCHRONIZATION_LOG_DETAIL == null ? "" : x.SYNCHRONIZATION_LOG_DETAIL.Object_id; m.Device_id_Text = x.SYNCHRONIZATION_LOG_DEVICE == null ? "" : x.SYNCHRONIZATION_LOG_DEVICE.Udid;  return m;})
            }, JsonRequestBehavior.AllowGet);
            return jsonRes;
        }
        
        [AresAuthorize(ResourceKey = "Admin.SynchronizationLog.Create")]
        public ActionResult CreateSynchronizationLog(int SynchronizationDetailModelId)
        {
            SynchronizationLogModel model = new SynchronizationLogModel();
            model.Synchronization_detail_id = SynchronizationDetailModelId;
            
            
            FillDropDownSynchronizationLog(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.SynchronizationLog.Create")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult CreateSynchronizationLog( SynchronizationLogModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {                
                var record = model.ToEntity();
                
                
                
                record = _iSynchronizationLogsvc.Insert(record);

                SuccessNotification(_localizationSvc.GetResource("Notifications.SynchronizationLog.Created"));
                
                if (continueEditing)
                    return RedirectToAction("EditSynchronizationLog", new { id = record.Id });

                return RedirectToAction("EditSynchronizationDetail", new  { id = model.Synchronization_detail_id, SelectedTab = "2" });
            }
            else
            {
                FillDropDownSynchronizationLog(model);
                return View(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }
        
        public ActionResult EditSynchronizationLog(int id, string SelectedTab)
        {
            SynchronizationLog record = _iSynchronizationLogsvc.GetById(id);
            var model = record.ToModel();
            model.SelectedTab = SelectedTab;
            
            
            
            FillDropDownSynchronizationLog(model);

            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.SynchronizationLog.Update")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult EditSynchronizationLog( SynchronizationLogModel model, bool continueEditing)
        {
            var item = _iSynchronizationLogsvc.GetById(model.Id);
            SynchronizationLog originalItem = (SynchronizationLog)item.Clone();
            if (item == null)
                //No country found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                item = model.ToEntity(item);
                
                
                

                _iSynchronizationLogsvc.Update(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.SynchronizationLog.Updated"));
                
                if(continueEditing)
                    return RedirectToAction("EditSynchronizationLog", new { id = item.Id });
                
                return RedirectToAction("EditSynchronizationDetail", new  { id = model.Synchronization_detail_id, SelectedTab = "2" });
            }

            //If we got this far, something failed, redisplay form
            FillDropDownSynchronizationLog(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.SynchronizationLog.Delete")]
        public ActionResult DeleteSynchronizationLog(int id)
        {

            var item = _iSynchronizationLogsvc.GetById(id);
            if (item == null)
                return RedirectToAction("List");

            try
            {
                
                _iSynchronizationLogsvc.Delete(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.SynchronizationLog.Deleted"));
                
                return RedirectToAction("EditSynchronizationDetail", new  { id = item.Synchronization_detail_id, SelectedTab = "2" });
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("EditSynchronizationLog", new { id = item.Id });
            }
        }
        #endregion 
          



        





            
        #region  SynchronizationDevice methods
        
        public void FillDropDownSynchronizationDevice(SynchronizationDeviceModel model)
        {

            model.Device_id_Device_SelectList = new SelectList(_iDevicesvc.GetAll().ToList(), "Id", "Udid");

            model.Synchronization_id_Synchronization_SelectList = new SelectList(_iSynchronizationsvc.GetAll().ToList(), "Id", "Object_id");
        }
        
        public ActionResult CreateOrUpdateSynchronizationDevice(string itemId, string imageUrl, string mimeType, string uploadFilename)
        {
            SynchronizationDeviceModel response = null;


            int nItemId = -1;
            int.TryParse(itemId, out nItemId);

            if (nItemId != -1)
            {
                SynchronizationDevice record = _iSynchronizationDevicesvc.GetById(nItemId);
                response = record.ToModel();
                if (record != null) // update
                {



                    //response.RoleListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem { Text = "Administrators" }, new Ext.Net.ListItem { Text = "Guests" } };
                    //response.PermissionListItems = permListItems.ToArray();

                }
                else // insert mode (after image upload popup close)
                {
                    response = new SynchronizationDeviceModel();
                    //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() { Text = "" } };
                }
            }
            else // insert
            {
                response = new SynchronizationDeviceModel();
                //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() {Text = "" }  };

            }


            ViewBag.itemId = nItemId;

            return View(response);
        }
        
        [AresAuthorize(ResourceKey = "Admin.SynchronizationDevice.Read")]
        public ActionResult ListSynchronizationDeviceDatatable(Ares.Web.Razor.Extensions.Datatable.Model.DTParameterModel model, int? SynchronizationModelId)
        {
            var models = _iSynchronizationDevicesvc.GetAll();
            models = models.Where(m => m.Synchronization_id == SynchronizationModelId);
            if (model.Search != null && !string.IsNullOrEmpty(model.Search.Value))
            {
                models = models.Where(m => m.Udid.Contains(model.Search.Value));
            }
            
            if (model.IsSearchValueExist("Synchronization_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Synchronization_id"));
                models = models.Where(m => m.Synchronization_id == tmpValue);
            }
            if (model.IsSearchValueExist("Device_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Device_id"));
                models = models.Where(m => m.Device_id == tmpValue);
            }
            if (model.IsSearchValueExist("Udid"))
            {
                string tmpValue = model.GetSearchValue("Udid");
                models = models.Where(m => m.Udid.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Status"))
            {
                
            }
            if (model.IsSearchValueExist("Start_datetime"))
            {
                
                
            }
            if (model.IsSearchValueExist("Finish_datetime"))
            {
                
                
            }
            if (model.IsSearchValueExist("Description"))
            {
                string tmpValue = model.GetSearchValue("Description");
                models = models.Where(m => m.Description.Contains(tmpValue));
            }
            
            DTOrder order = null;
            if (model.Order != null && model.Order.Count() > 0)
            {
                order = model.Order.First();
                var pi = typeof(SynchronizationDevice).GetProperty(model.Columns.ToList()[order.Column].Data);
                if(order.Dir == "asc")
                    models = models.OrderBy(x => pi.GetValue(x, null));
                else
                    models = models.OrderByDescending(x => pi.GetValue(x, null));
                
            }

            var totalRecords = models.Count();
            
            models = models.OrderBy(e => e.Id).Skip(model.Length * ((int)(Math.Ceiling((double)model.Start / model.Length)) + 1 - 1)).Take(model.Length);

            JsonResult jsonRes = Json(new
            {
                sEcho = model.Draw,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                //aaData = result
                aaData = models.ToList().Select(x => { var m = x.ToModel(); m.Device_id_Text = x.SYNCHRONIZATION_DEVICE_DEVICE == null ? "" : x.SYNCHRONIZATION_DEVICE_DEVICE.Udid; m.Synchronization_id_Text = x.SYNCHRONIZATION_DEVICE_SYNCHRONIZATION == null ? "" : x.SYNCHRONIZATION_DEVICE_SYNCHRONIZATION.Object_id;  return m;})
            }, JsonRequestBehavior.AllowGet);
            return jsonRes;
        }
        
        [AresAuthorize(ResourceKey = "Admin.SynchronizationDevice.Create")]
        public ActionResult CreateSynchronizationDevice(int SynchronizationModelId)
        {
            SynchronizationDeviceModel model = new SynchronizationDeviceModel();
            model.Synchronization_id = SynchronizationModelId;
            
            
            FillDropDownSynchronizationDevice(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.SynchronizationDevice.Create")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult CreateSynchronizationDevice( SynchronizationDeviceModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {                
                var record = model.ToEntity();
                
                
                
                record = _iSynchronizationDevicesvc.Insert(record);

                SuccessNotification(_localizationSvc.GetResource("Notifications.SynchronizationDevice.Created"));
                
                if (continueEditing)
                    return RedirectToAction("EditSynchronizationDevice", new { id = record.Id });

                return RedirectToAction("EditSynchronization", new  { id = model.Synchronization_id, SelectedTab = "3" });
            }
            else
            {
                FillDropDownSynchronizationDevice(model);
                return View(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }
        
        public ActionResult EditSynchronizationDevice(int id, string SelectedTab)
        {
            SynchronizationDevice record = _iSynchronizationDevicesvc.GetById(id);
            var model = record.ToModel();
            model.SelectedTab = SelectedTab;
            
            
            
            FillDropDownSynchronizationDevice(model);

            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.SynchronizationDevice.Update")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult EditSynchronizationDevice( SynchronizationDeviceModel model, bool continueEditing)
        {
            var item = _iSynchronizationDevicesvc.GetById(model.Id);
            SynchronizationDevice originalItem = (SynchronizationDevice)item.Clone();
            if (item == null)
                //No country found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                item = model.ToEntity(item);
                
                
                

                _iSynchronizationDevicesvc.Update(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.SynchronizationDevice.Updated"));
                
                if(continueEditing)
                    return RedirectToAction("EditSynchronizationDevice", new { id = item.Id });
                
                return RedirectToAction("EditSynchronization", new  { id = model.Synchronization_id, SelectedTab = "3" });
            }

            //If we got this far, something failed, redisplay form
            FillDropDownSynchronizationDevice(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.SynchronizationDevice.Delete")]
        public ActionResult DeleteSynchronizationDevice(int id)
        {

            var item = _iSynchronizationDevicesvc.GetById(id);
            if (item == null)
                return RedirectToAction("List");

            try
            {
                
                _iSynchronizationDevicesvc.Delete(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.SynchronizationDevice.Deleted"));
                
                return RedirectToAction("EditSynchronization", new  { id = item.Synchronization_id, SelectedTab = "3" });
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("EditSynchronizationDevice", new { id = item.Id });
            }
        }
        #endregion 
          



        


        

    }


    public class DeviceAuthorizeAttribute : ActionFilterAttribute
    {
        public IWorkContext WorkContext { get; set; }
        public IDeviceSvc DeviceSvc { get; set; }
        //public string IdParamName { get; set; }
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (WorkContext.CurrentUser.IsAdmin())
            {
                base.OnActionExecuting(context);
                return;
            }

            var httpContext = context.HttpContext;

            if (!string.IsNullOrEmpty(context.RouteData.Values["id"].ToString()))
            {
                int entityID = int.Parse(context.RouteData.Values["id"].ToString());

                Device tmpEntity = DeviceSvc.GetById(entityID);

                if (tmpEntity != null && tmpEntity.User_From_DeviceUserMapping.Any(d => d.Id == WorkContext.CurrentUser.Id))
                {
                    base.OnActionExecuting(context);
                    return;
                }

            }

            context.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "User", action = "NotAuthorized" }));
        }
    }
}

