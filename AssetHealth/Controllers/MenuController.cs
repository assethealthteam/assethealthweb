using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using System.Web.Mvc;
using Ares.UMS.Svc.Directory;
using System.Linq;
using Ares.UMS.DTO.Users;
using Ares.UMS.Svc.Users;
using Ares.UMS.Svc.Common;
using Ares.UMS.DTO.Messages;
using Ares.UMS.Svc.Messages;
using Ares.Core.Helpers;
using Ares.UMS.DTO.Localization;
using Ares.UMS.Svc.Localization;
using System.Net;
using System.Web.Security;
using Ext.Net.MVC;
using Ares.UMS.DTO.Media;
using Ares.UMS.Svc.Media;
using System.IO;
using Ares.UMS.Svc.Security;
using Ares.UMS.DTO.Security;
using Ares.Web.Helpers;

using Ares.UMS.Svc.Configuration;
using Ares.UMS.DTO.Configuration;
using Ares.Web.Controllers;
using Ares.UMS.Svc;
using Ares.Web.Svc.Authentication;
using Ares.Web.Attributes;
using AssetHealth.Models.Configuration;
using AssetHealth.Extensions;

namespace AssetHealth.Controllers
{
    public class MenuController : BaseAresController
    {
        private readonly IStateProvinceSvc _stateProvinceSvc;
        private readonly IGenericAttributeSvc _genericAttributeSvc;
        private readonly ILocalizationSvc _localizationSvc;
        private readonly IPictureSvc _pictureSvc;
        private readonly IUserSvc _userSvc;
        private readonly IRoleSvc _roleSvc;
        private readonly IPermissionSvc _permissionSvc;
        private readonly IMenuSvc _menuSvc;
        private readonly IWorkContext _workContext;
        private readonly IUserRegistrationSvc _userRegistrationSvc;
        private readonly IAuthenticationSvc _authenticationSvc;
        private readonly IWorkflowMessageSvc _workflowMessageSvc;
        private readonly INewsLetterSubscriptionSvc _newsLetterSubscriptionSvc;
        private readonly IWebHelper _webHelper;
        private readonly UserSettings _userSettings;
        private readonly LocalizationSettings _localizationSettings;
        private readonly MediaSettings _mediaSettings;

        public MenuController(IStateProvinceSvc stateProvinceSvc, IGenericAttributeSvc genericAttributeSvc, ILocalizationSvc localizationSvc,  
            UserSettings userSettings, IPictureSvc pictureSvc,
            IUserSvc userSvc, IRoleSvc roleSvc, IPermissionSvc permissionSvc, IMenuSvc menuSvc, IWorkContext workContext, IUserRegistrationSvc userRegistrationSvc, IAuthenticationSvc authenticationSvc, IWorkflowMessageSvc workflowMessageSvc,
            INewsLetterSubscriptionSvc newsLetterSubscriptionSvc, IWebHelper webHelper, LocalizationSettings localizationSettings, MediaSettings mediaSettings)
        {
            this._stateProvinceSvc = stateProvinceSvc;
            this._genericAttributeSvc = genericAttributeSvc;
            this._pictureSvc = pictureSvc;
            this._localizationSvc = localizationSvc;
            this._userSettings = userSettings;
            this._userSvc = userSvc;
            this._roleSvc = roleSvc;
            this._permissionSvc = permissionSvc;
            this._menuSvc = menuSvc;
            this._workContext = workContext;
            this._userRegistrationSvc = userRegistrationSvc;
            this._authenticationSvc = authenticationSvc;
            this._workflowMessageSvc = workflowMessageSvc;
            this._newsLetterSubscriptionSvc = newsLetterSubscriptionSvc;
            this._webHelper = webHelper;
            this._localizationSettings = localizationSettings;
            this._mediaSettings = mediaSettings;

        }




        public ActionResult List()
        {
            MenuModel model = new MenuModel();
            //model. = _roleSvc.GetAll().ToList().Select(x => x.ToModel()).ToList();

            return View(model);

        }


        public ActionResult ListMenuDatatable(Ares.Web.Razor.Extensions.Datatable.Model.DTParameterModel model)
        {
            var menuModels = _menuSvc.GetAll();
            var totalRecords = 0;
             if (model.Search != null && !string.IsNullOrEmpty(model.Search.Value))
            {
                menuModels = menuModels.Where(m => m.Menuname.Contains(model.Search.Value));
            }

            totalRecords = menuModels.Count();

            menuModels = menuModels.OrderBy(e => e.Id).Skip(model.Length * ((int)(Math.Ceiling((double)model.Start / model.Length)) + 1 - 1)).Take(model.Length);
            //string jsonString = JsonHelper.JsonSerializer<IList<Models.Configuration.MenuModel>>(menuModels);

            
            //var result = from c in menuModels select new[] { Convert.ToString(c.Id), c.Menuname, c.Title, c.Controller, c.Actionname};



            JsonResult jsonRes = Json(new
            {
                sEcho = model.Draw,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                //aaData = result
                aaData = menuModels.ToList().Select(x => x.ToModel()).ToList()
            }, JsonRequestBehavior.AllowGet);
            return jsonRes;

            //return Json(new { success = true, data = _roleSvc.GetAll().ToList(), Message = "" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var model = new MenuModel();

            //model.PermissionViewModel = GetMultiSelectListViewInitialModel<Permission>(_permissionSvc.GetAll().ToList(), null);
            setDropDowns(model);

            return View(model);
        }

        public void setDropDowns(MenuModel model)
        {
            var menus = from m in _menuSvc.GetAll().ToList()
                        select new
                        {
                            Id = m.Id,
                            Title = _localizationSvc.GetResource(m.Title)
                        };

            model.Menus_SelectList = new SelectList(menus, "Id", "Title");
            model.Permissions = new SelectList(_permissionSvc.GetAll().ToList().ToList(), "Id", "PermissionName");
        }


        [AresAuthorize(ResourceKey = "Permissions.Menu.Create")]
        [System.Web.Mvc.HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(MenuModel model)
        {
            if (!ModelState.IsValid)
            { // re-render the view when validation failed.
                setDropDowns(model);
                return View("Create", model);
            }

            var menu = model.ToEntity();

            menu = model.ToEntity(menu);
            Menu parentMenu = _menuSvc.GetById(model.ParentMenuId);
            menu.Parent = parentMenu;
            menu.TopParent = findTopParent(parentMenu);

            Permission permission = _permissionSvc.GetById(model.Permission_Id);
            menu.Permission = permission;

            _menuSvc.Insert(menu);

            SuccessNotification(_localizationSvc.GetResource("Notifications.Menu.Created"));

            return RedirectToAction("List");
        }

        private Menu findTopParent(Menu menu)
        {
            int i = 0;
            Menu tmpMenu = menu.Parent;
            if (tmpMenu == null)
                return tmpMenu;

            while (tmpMenu.Parent != null && i < 10)
            {
                tmpMenu = tmpMenu.Parent;
                i++;
            }

            return tmpMenu;
        }

        
        public ActionResult Edit(int id)
        {
            Menu menu = _menuSvc.GetById(id);
            var model = menu.ToModel();


            var menus = from m in _menuSvc.GetAll().ToList() 
                        select new
                        {
                            Id = m.Id,
                            Title = _localizationSvc.GetResource(m.Title)
                        };

            model.Menus_SelectList = new SelectList(menus, "Id", "Title");

            if(menu.Parent != null)
                model.ParentMenuId = menu.Parent.Id;

            var permissions = from m in _permissionSvc.GetAll().ToList()
                        select new
                        {
                            Id = m.Id,
                            Title = m.PermissionName
                        };

            model.Permissions = new SelectList(permissions, "Id", "Title");

            if (menu.Permission != null)
                model.Permission_Id = menu.Permission.Id;

            return View(model);
        }


        [AresAuthorize(ResourceKey = "Permissions.Menu.Edit")]
        [System.Web.Mvc.HttpPost, ValidateInput(false)]
        public ActionResult Edit(MenuModel model)
        {
            
            var menu = _menuSvc.GetById(model.Id);

            /*
            var urStringArrayFromDb = (from ur in menu.MenuPermissions
                                       select ur.Id + "");
            
            if (model.PermissionViewModel != null)
            {
                model.PermissionViewModel = GetMultiSelectListViewModel<Permission>(model.PermissionViewModel.PostedItems, _permissionSvc.GetAll().ToList());

                var urFromModel = model.PermissionViewModel.PostedItems.ItemIds;

                // Items in first array but not on second array (to be added to db)
                IEnumerable<string> onlyInModelNotInDb = urFromModel.Except(urStringArrayFromDb);

                foreach (string item in onlyInModelNotInDb)
                {
                    Permission record = _permissionSvc.GetById(Int32.Parse(item));
                    menu.MenuPermissions.Add(record);
                }

                // Items in first array but not second array (to be deleted from db)
                IEnumerable<string> onlyInDbNotInModel = new List<string>(urStringArrayFromDb.Except(urFromModel));

                foreach (string item in onlyInDbNotInModel)
                {
                    Permission record = menu.MenuPermissions.Where(ur => ur.Id + "" == item).FirstOrDefault();
                    menu.MenuPermissions.Remove(record);
                }
            }
            else
            {
                if (menu.MenuPermissions != null)
                    menu.MenuPermissions.Clear();
            }

    */

            menu = model.ToEntity(menu);
            Menu parentMenu = _menuSvc.GetById(model.ParentMenuId);
            menu.Parent = parentMenu;
            menu.TopParent = findTopParent(parentMenu);

            Permission permission = _permissionSvc.GetById(model.Permission_Id);
            menu.Permission = permission;

            _menuSvc.Update(menu);

            SuccessNotification(_localizationSvc.GetResource("Notifications.Menu.Updated"));

            return RedirectToAction("List");
        }


        public ActionResult CreateOrUpdate(string itemId, string imageUrl, string mimeType, string uploadFilename)
        {
            MenuModel response = null;
            

            int nItemId = -1;
            int.TryParse(itemId, out nItemId);

            if (nItemId != -1)
            {
                Menu record = _menuSvc.GetById(nItemId);
                response = record.ToModel();
                if (record != null) // update
                {

                   

                }
                else // insert mode (after image upload popup close)
                {
                    response = new MenuModel();
                    //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() { Text = "" } };
                }
            }
            else // insert
            {
                response = new MenuModel();
                //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() {Text = "" }  };
                
            }

 
            ViewBag.itemId = nItemId;

            return View(response);
        }

        public ActionResult ListPermissions()
        {
            string jsonString = JsonHelper.JsonSerializer<IList<Models.User.PermissionModel>>(_permissionSvc.GetAll().ToList().Select(x => x.ToModel()).ToList());
            var result = new { success = true, data = jsonString, Message = "" };
            JsonResult jsonRes = Json(result, JsonRequestBehavior.AllowGet);
            return jsonRes;

            //return Json(new { success = true, data = _roleSvc.GetAll().ToList(), Message = "" }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Delete(int id)
        {
            if (id != -1)
            {
                Menu record = _menuSvc.GetById(id);

                _menuSvc.Delete(record);

                SuccessNotification(_localizationSvc.GetResource("Notifications.Menu.Deleted"));
            }
            else
            {

            }


            return RedirectToAction("List");
        }


    }
}
