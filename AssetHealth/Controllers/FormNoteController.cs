﻿



using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.IO;
using System.Web.Http;
using System.Net;
using System.Web.Http.ModelBinding;
using System.Web.Security;
using Ext.Net.MVC;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using System.Web.Mvc;
using System.Linq;
using Ares.UMS.Svc.Directory;
using Ares.UMS.DTO.Users;
using Ares.UMS.Svc.Users;
using Ares.Web.Svc.Authentication;
using Ares.Web.Svc.Web;
using Ares.UMS.Svc.Common;
using Ares.UMS.DTO.Messages;
using Ares.UMS.Svc.Messages;
using Ares.Core.Helpers;
using Ares.UMS.DTO.Localization;
using Ares.UMS.Svc.Localization;
using Ares.UMS.DTO.Media;
using Ares.UMS.Svc.Media;
using Ares.Web.Models.Role;
using Ares.UMS.Svc.Security;
using Ares.UMS.DTO.Security;
using Ares.Web.Helpers;
using Ares.Web.Attributes;
using Ares.Web.Controllers;
using Ares.UMS.Svc.Configuration;
using Ares.UMS.DTO.Configuration;
using Ares.UMS.Svc;
using Ares.Web.Razor.Extensions.Datatable.Model;
using AssetHealth.Extensions;
using AssetHealth.Models.User;
using AssetHealth.Common.Svc;
using AssetHealth.Common.DTO;
using AssetHealth.Models.Configuration;
using AssetHealth.Models.Form_notes;
using AssetHealth.Models.Device;
using AssetHealth.Models.Form_answer_round;
using Ares.Sync.Svc;

namespace AssetHealth.Contollers
{
	
    public partial class FormNoteController : BaseAresController
	{
		#region Fields

        private readonly IFormNoteSvc _iFormNotessvc;
        
        private readonly IParameterItemSvc _iParameterItemsvc;
        private readonly IDeviceSvc _iDevicesvc;
        private readonly IFormAnswerRoundSvc _iFormAnswerRoundsvc;
        
        private readonly IStateProvinceSvc _stateProvinceSvc;
        private readonly ISettingSvc _settingSvc;
        private readonly FileSettings _fileSettings;
        
        private readonly IGenericAttributeSvc _genericAttributeSvc;
        private readonly ILocalizationSvc _localizationSvc;
        private readonly IPictureSvc _pictureSvc;
        private readonly IUserSvc _userSvc;
        private readonly IRoleSvc _roleSvc;
        private readonly IPermissionSvc _permissionSvc;
        private readonly IMenuSvc _menuSvc;
        private readonly IWorkContext _workContext;
        private readonly IUserRegistrationSvc _userRegistrationSvc;
        private readonly IAuthenticationSvc _authenticationSvc;
        private readonly IWorkflowMessageSvc _workflowMessageSvc;
        private readonly INewsLetterSubscriptionSvc _newsLetterSubscriptionSvc;
        private readonly IWebHelper _webHelper;
        private readonly UserSettings _userSettings;
        private readonly LocalizationSettings _localizationSettings;
        private readonly MediaSettings _mediaSettings;

	    #endregion

		#region Constructors

        public FormNoteController(IFormNoteSvc FormNotessvc,
        IParameterItemSvc ParameterItemsvc,
        IDeviceSvc Devicesvc,
        IFormAnswerRoundSvc FormAnswerRoundsvc,
            IStateProvinceSvc stateProvinceSvc, IGenericAttributeSvc genericAttributeSvc, ILocalizationSvc localizationSvc,  
            UserSettings userSettings, IPictureSvc pictureSvc, ISettingSvc settingSvc, FileSettings fileSettings,
            IUserSvc userSvc, IRoleSvc roleSvc, IPermissionSvc permissionSvc, IMenuSvc menuSvc, IWorkContext workContext, IUserRegistrationSvc userRegistrationSvc, IAuthenticationSvc authenticationSvc, IWorkflowMessageSvc workflowMessageSvc,
            INewsLetterSubscriptionSvc newsLetterSubscriptionSvc, IWebHelper webHelper, LocalizationSettings localizationSettings, MediaSettings mediaSettings)
		{
            this._iFormNotessvc = FormNotessvc;
            
            this._iParameterItemsvc = ParameterItemsvc;
            this._iDevicesvc = Devicesvc;
            this._iFormAnswerRoundsvc = FormAnswerRoundsvc;
            this._settingSvc = settingSvc;
            this._fileSettings = fileSettings;
            
            this._stateProvinceSvc = stateProvinceSvc;
            this._genericAttributeSvc = genericAttributeSvc;
            this._pictureSvc = pictureSvc;
            this._localizationSvc = localizationSvc;
            this._userSettings = userSettings;
            this._userSvc = userSvc;
            this._roleSvc = roleSvc;
            this._permissionSvc = permissionSvc;
            this._menuSvc = menuSvc;
            this._workContext = workContext;
            this._userRegistrationSvc = userRegistrationSvc;
            this._authenticationSvc = authenticationSvc;
            this._workflowMessageSvc = workflowMessageSvc;
            this._newsLetterSubscriptionSvc = newsLetterSubscriptionSvc;
            this._webHelper = webHelper;
            this._localizationSettings = localizationSettings;
            this._mediaSettings = mediaSettings;
		}

		#endregion 

        #region Utilities
        
        
        #endregion

        #region FormNote methods

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        [AresAuthorize(ResourceKey = "Admin.FormNote.Read")]
        public ActionResult List()
        {
            FormNoteModel model = new FormNoteModel();
            //model. = _roleSvc.GetAll().ToList().Select(x => x.ToModel()).ToList();
            FillDropDowns(model);
            
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormNote.Read")]
        public ActionResult ListFormNoteDatatable(Ares.Web.Razor.Extensions.Datatable.Model.DTParameterModel model)
        {
            var models = _iFormNotessvc.GetAll();
            
            if (model.Search != null && !string.IsNullOrEmpty(model.Search.Value))
            {
                models = models.Where(m => m.Object_id.Contains(model.Search.Value));
            }
            
            if(model.IsSearchValueExist("Device_id_Text"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Device_id_Text"));
                models = models.Where(m => m.Device_id == tmpValue);
            }
            if(model.IsSearchValueExist("Form_answer_round_id_Text"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Form_answer_round_id_Text"));
                models = models.Where(m => m.Form_answer_round_id == tmpValue);
            }
            if(model.IsSearchValueExist("Username"))
            {
                string tmpValue = model.GetSearchValue("Username");
                models = models.Where(m => m.Username.Contains(tmpValue));
            }
            if(model.IsSearchValueExist("Read_no"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Read_no"));
                models = models.Where(m => m.Read_no == tmpValue);
            }
            if(model.IsSearchValueExist("Warning_type_Text"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Warning_type_Text"));
                models = models.Where(m => m.Warning_type == tmpValue);
            }
            if(model.IsSearchValueExist("Title"))
            {
                string tmpValue = model.GetSearchValue("Title");
                models = models.Where(m => m.Title.Contains(tmpValue));
            }
            if(model.IsSearchValueExist("Description"))
            {
                string tmpValue = model.GetSearchValue("Description");
                models = models.Where(m => m.Description.Contains(tmpValue));
            }
            if(model.IsSearchValueExist("Notes"))
            {
            }
            if(model.IsSearchValueExist("Actual_read_datetime"))
            {
                
                
            }
            
            
            DTOrder order = null;
            if (model.Order != null && model.Order.Count() > 0)
            {
                order = model.Order.First();
                var pi = typeof(FormNote).GetProperty(model.Columns.ToList()[order.Column].Data);
                if(order.Dir == "asc")
                    models = models.OrderBy(x => pi.GetValue(x, null));
                else
                    models = models.OrderByDescending(x => pi.GetValue(x, null));
                
            }

            var totalRecords = models.Count();
            
            models = models.OrderBy(e => e.Id).Skip(model.Length * ((int)(Math.Ceiling((double)model.Start / model.Length)) + 1 - 1)).Take(model.Length);

            JsonResult jsonRes = Json(new
            {
                sEcho = model.Draw,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                //aaData = result
                aaData = models.ToList().Select(x => { var m = x.ToModel();/* m.Device_id_Text = x.NOTES_DEVICE == null ? "" : x.NOTES_DEVICE.Udid;*/ m.Form_answer_round_id_Text = x.NOTES_ROUND == null ? "" : x.NOTES_ROUND.Object_id; m.Warning_type_Text = x.NOTES_WARNING_PARAMITEMS == null ? "" : x.NOTES_WARNING_PARAMITEMS.Code;  return m;})
            }, JsonRequestBehavior.AllowGet);
            return jsonRes;
        }
        
        [AresAuthorize(ResourceKey = "Admin.FormNote.Create")]
        public ActionResult Create()
        {
            FormNoteModel model = new FormNoteModel();
            FillDropDowns(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormNote.Create")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Create( FormNoteModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {                
                var record = model.ToEntity();
                
                record.Object_id = Guid.NewGuid().ToString();
                record.Insert_user_id = _workContext.CurrentUser.Id.ToString();
                record.Insert_datetime = StringHelper.ConvertDateTimeToString(DateTime.Now);
                
            
                record = _iFormNotessvc.Insert(record);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormNote.Created"));
                
                if (continueEditing)
                    return RedirectToAction("Edit", new { Id = record.Id });

                return RedirectToAction("List");
            }
            else
            {
                FillDropDowns(model);
                return View(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormNote.Update")]
        public ActionResult Edit(int id, string SelectedTab)
        {
            FormNote record = _iFormNotessvc.GetById(id);
            var model = record.ToModel();
            model.SelectedTab = SelectedTab;
                        
            FillDropDowns(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormNote.Update")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Edit( FormNoteModel model, bool continueEditing)
        {
            var item = _iFormNotessvc.GetById(model.Id);
            FormNote originalItem = (FormNote)item.Clone();
            if (item == null)
                //No country found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                item = model.ToEntity(item);
                item.Object_id = originalItem.Object_id;                
                item.Insert_user_id = originalItem.Insert_user_id;                
                item.Insert_datetime = originalItem.Insert_datetime;                
                
                
                
                item.Update_user_id = _workContext.CurrentUser.Id.ToString();
                item.Update_datetime = StringHelper.ConvertDateTimeToString(DateTime.Now);

                _iFormNotessvc.Update(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormNote.Updated"));
                
                if(continueEditing)
                    return RedirectToAction("Edit", new { Id = item.Id });

                return RedirectToAction("List");
            }
            else
            {
                FillDropDowns(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormNote.Delete")]
        public ActionResult Delete(int id)
        {

            var item = _iFormNotessvc.GetById(id);
            if (item == null)
                return RedirectToAction("List");

            try
            {
                
                _iFormNotessvc.Delete(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormNote.Deleted"));
                return RedirectToAction("List");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("Edit", new { id = item.Id });
            }
        }
        
        
        public ActionResult CreateOrUpdate(string itemId, string imageUrl, string mimeType, string uploadFilename)
        {
            FormNoteModel response = null;
            

            int nItemId = -1;
            int.TryParse(itemId, out nItemId);

            if (nItemId != -1)
            {
                FormNote record = _iFormNotessvc.GetById(nItemId);
                response = record.ToModel();
                if (record != null) // update
                {

                   

                }
                else // insert mode (after image upload popup close)
                {
                    response = new FormNoteModel();
                    //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() { Text = "" } };
                }
            }
            else // insert
            {
                response = new FormNoteModel();
                //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() {Text = "" }  };
                
            }

 
            ViewBag.itemId = nItemId;

            return View(response);
        }
        
        
        public void FillDropDowns(FormNoteModel model)
        {
            

            model.Device_id_Device_SelectList = new SelectList(_iDevicesvc.GetAll().ToList(), "Id", "Udid");

            model.Form_answer_round_id_FormAnswerRound_SelectList = new SelectList(_iFormAnswerRoundsvc.GetAll().ToList(), "Id", "Object_id");

            model.Warning_type_ParameterItem_SelectList = new SelectList(_iParameterItemsvc.GetAll().Where(p => p.Group.System_name == "FORM_NOTES_WARNING_TYPE").ToList().Select(p => { return new { Id = p.Id, Title = p.GetLocalized(l => l.Title) }; }), "Id", "Title");
            
            
        }
        
        #endregion
                         
        

    }
}

