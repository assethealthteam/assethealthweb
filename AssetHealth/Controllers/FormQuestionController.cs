﻿



using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.IO;
using System.Web.Http;
using System.Net;
using System.Web.Http.ModelBinding;
using System.Web.Security;
using Ext.Net.MVC;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using System.Web.Mvc;
using System.Linq;
using Ares.UMS.Svc.Directory;
using Ares.UMS.DTO.Users;
using Ares.UMS.Svc.Users;
using Ares.Web.Svc.Authentication;
using Ares.Web.Svc.Web;
using Ares.UMS.Svc.Common;
using Ares.UMS.DTO.Messages;
using Ares.UMS.Svc.Messages;
using Ares.Core.Helpers;
using Ares.UMS.DTO.Localization;
using Ares.UMS.Svc.Localization;
using Ares.UMS.DTO.Media;
using Ares.UMS.Svc.Media;
using Ares.Web.Models.Role;
using Ares.UMS.Svc.Security;
using Ares.UMS.DTO.Security;
using Ares.Web.Helpers;
using Ares.Web.Attributes;
using Ares.Web.Controllers;
using Ares.UMS.Svc.Configuration;
using Ares.UMS.DTO.Configuration;
using Ares.UMS.Svc;
using Ares.Web.Razor.Extensions.Datatable.Model;
using AssetHealth.Extensions;
using AssetHealth.Models.User;
using AssetHealth.Common.Svc;
using AssetHealth.Common.DTO;
using AssetHealth.Models.Configuration;
using AssetHealth.Models.Form_question;
using AssetHealth.Models.Synchronization_detail;
using AssetHealth.Models.Synchronization;
using AssetHealth.Models.User;
using AssetHealth.Models.Eq_channel;
using AssetHealth.Models.Eq_threshold;
using AssetHealth.Models.Eq_warning;
using AssetHealth.Models.Eq_location;
using AssetHealth.Models.Equipment;
using AssetHealth.Models.Form_question_option;
using AssetHealth.Models.Form_answer;
using AssetHealth.Models.Form;
using AssetHealth.Common.DTO.Extensions;
using Ares.Core.Framework;
using AssetHealth.Common.DAL;
using AssetHealth.Common.DTO.Enums;
using AssetHealth.Mapper;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using AssetHealth.Models.Aa_excel_import;
using Newtonsoft.Json;
using AssetHealth.KendoSerializer;
using Ares.Sync.Controllers;
using Ares.Web.JSON;
using Ares.Web.Mvc.Results;
using AssetHealth.Common.Svc.JSONGenerator;

namespace AssetHealth.Contollers
{
	
    public partial class FormQuestionController : BaseAresController
	{
		#region Fields

        private readonly IFormQuestionSvc _iFormQuestionsvc;
        

        private readonly IUserSvc _iUsersvc;
        private readonly IEqChannelSvc _iEqChannelsvc;
        private readonly IEqThresholdSvc _iEqThresholdsvc;
        private readonly IEqWarningSvc _iEqWarningsvc;
        private readonly IEqLocationSvc _iEqLocationsvc;
        private readonly IEquipmentSvc _iEquipmentsvc;
        private readonly IParameterGroupSvc _iParameterGroupsvc;
        private readonly IParameterItemSvc _iParameterItemsvc;
        private readonly IFormQuestionOptionSvc _iFormQuestionOptionsvc;
        private readonly IFormAnswerSvc _iFormAnswersvc;
        private readonly IFormAnswerRoundSvc _iFormAnswerRoundsvc;
        private readonly IFormSvc _iFormsvc;
        
        private readonly IStateProvinceSvc _stateProvinceSvc;
        private readonly ISettingSvc _settingSvc;
        private readonly FileSettings _fileSettings;
        
        private readonly IGenericAttributeSvc _genericAttributeSvc;
        private readonly ILocalizationSvc _localizationSvc;
        private readonly IPictureSvc _pictureSvc;
        private readonly IUserSvc _userSvc;
        private readonly IRoleSvc _roleSvc;
        private readonly IPermissionSvc _permissionSvc;
        private readonly IMenuSvc _menuSvc;
        private readonly IWorkContext _workContext;
        private readonly IUserRegistrationSvc _userRegistrationSvc;
        private readonly IAuthenticationSvc _authenticationSvc;
        private readonly IWorkflowMessageSvc _workflowMessageSvc;
        private readonly INewsLetterSubscriptionSvc _newsLetterSubscriptionSvc;
        private readonly IWebHelper _webHelper;
        private readonly UserSettings _userSettings;
        private readonly LocalizationSettings _localizationSettings;
        private readonly MediaSettings _mediaSettings;

	    #endregion

		#region Constructors

        public FormQuestionController(IFormQuestionSvc FormQuestionsvc,
        IUserSvc Usersvc,
        IEqChannelSvc EqChannelsvc,
        IEqThresholdSvc EqThresholdsvc,
        IEqWarningSvc EqWarningsvc,
        IEqLocationSvc EqLocationsvc,
        IEquipmentSvc Equipmentsvc,
        IParameterItemSvc ParameterItemsvc,
        IFormQuestionOptionSvc FormQuestionOptionsvc,
        IFormAnswerSvc FormAnswersvc,
        IFormAnswerRoundSvc FormAnswerRoundsvc,
        IFormSvc Formsvc, IParameterGroupSvc iParameterGroupsvc,
            IStateProvinceSvc stateProvinceSvc, IGenericAttributeSvc genericAttributeSvc, ILocalizationSvc localizationSvc,  
            UserSettings userSettings, IPictureSvc pictureSvc, ISettingSvc settingSvc, FileSettings fileSettings,
            IUserSvc userSvc, IRoleSvc roleSvc, IPermissionSvc permissionSvc, IMenuSvc menuSvc, IWorkContext workContext, IUserRegistrationSvc userRegistrationSvc, IAuthenticationSvc authenticationSvc, IWorkflowMessageSvc workflowMessageSvc,
            INewsLetterSubscriptionSvc newsLetterSubscriptionSvc, IWebHelper webHelper, LocalizationSettings localizationSettings, MediaSettings mediaSettings)
		{
            this._iFormQuestionsvc = FormQuestionsvc;
            

            this._iUsersvc = Usersvc;
            this._iEqChannelsvc = EqChannelsvc;
            this._iEqThresholdsvc = EqThresholdsvc;
            this._iEqWarningsvc = EqWarningsvc;
            this._iEqLocationsvc = EqLocationsvc;
            this._iEquipmentsvc = Equipmentsvc;
            this._iParameterGroupsvc = iParameterGroupsvc;
            this._iParameterItemsvc = ParameterItemsvc;
            this._iFormQuestionOptionsvc = FormQuestionOptionsvc;
            this._iFormAnswersvc = FormAnswersvc;
            this._iFormAnswerRoundsvc = FormAnswerRoundsvc;
            this._iFormsvc = Formsvc;
            this._settingSvc = settingSvc;
            this._fileSettings = fileSettings;
            
            this._stateProvinceSvc = stateProvinceSvc;
            this._genericAttributeSvc = genericAttributeSvc;
            this._pictureSvc = pictureSvc;
            this._localizationSvc = localizationSvc;
            this._userSettings = userSettings;
            this._userSvc = userSvc;
            this._roleSvc = roleSvc;
            this._permissionSvc = permissionSvc;
            this._menuSvc = menuSvc;
            this._workContext = workContext;
            this._userRegistrationSvc = userRegistrationSvc;
            this._authenticationSvc = authenticationSvc;
            this._workflowMessageSvc = workflowMessageSvc;
            this._newsLetterSubscriptionSvc = newsLetterSubscriptionSvc;
            this._webHelper = webHelper;
            this._localizationSettings = localizationSettings;
            this._mediaSettings = mediaSettings;
		}

		#endregion 

        #region Utilities
        
        
        #endregion

        #region FormQuestion methods

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        [AresAuthorize(ResourceKey = "Admin.FormQuestion.Read")]
        public ActionResult List()
        {
            FormQuestionModel model = new FormQuestionModel();
            //model. = _roleSvc.GetAll().ToList().Select(x => x.ToModel()).ToList();
            FillDropDowns(model);
            
            
            FillDropDownFormAnswer(model.Filter_FormAnswerModel);
                       
            
            //FillDropDownFormQuestionOption(model.Filter_FormQuestionOptionModel);
                       
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormQuestion.Read")]
        public ActionResult ListFormQuestionDatatable(Ares.Web.Razor.Extensions.Datatable.Model.DTParameterModel model)
        {
            var models = _iFormQuestionsvc.GetAll();
            
            if (!_workContext.CurrentUser.IsAdmin())
            {
                List<int> relatedLocationIds = _workContext.CurrentUser.RelatedLocations().Select(r => r.Id).ToList();
                models = models.Where(e => relatedLocationIds.Contains(e.QUESTION_FORM.EQUIPMENT.Location.Id));
            }
            
            if (model.Search != null && !string.IsNullOrEmpty(model.Search.Value))
            {
                models = models.Where(m => m.Code.Contains(model.Search.Value));
            }

            if (model.IsSearchValueExist("Eq_location_id_Text"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Eq_location_id_Text"));
                models = models.Where(m => m.QUESTION_FORM.EQUIPMENT.Eq_location_id == tmpValue);
            }

            if (model.IsSearchValueExist("Category_id_Text"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Category_id_Text"));
                models = models.Where(m => m.Category_id == tmpValue);
            }
            if(model.IsSearchValueExist("Type_id_Text"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Type_id_Text"));
                models = models.Where(m => m.Type_id == tmpValue);
            }
            if(model.IsSearchValueExist("Form_id_Text"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Form_id_Text"));
                models = models.Where(m => m.Form_id == tmpValue);
            }
            if(model.IsSearchValueExist("Derived_id_Text"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Derived_id_Text"));
                models = models.Where(m => m.Derived_id == tmpValue);
            }
            if(model.IsSearchValueExist("Code"))
            {
                string tmpValue = model.GetSearchValue("Code");
                models = models.Where(m => m.Code.Contains(tmpValue));
            }
            if(model.IsSearchValueExist("Title"))
            {
                string tmpValue = model.GetSearchValue("Title");
                models = models.Where(m => m.Title.Contains(tmpValue));
            }
            if(model.IsSearchValueExist("Hint"))
            {
                string tmpValue = model.GetSearchValue("Hint");
                models = models.Where(m => m.Hint.Contains(tmpValue));
            }
            if(model.IsSearchValueExist("Description"))
            {
                string tmpValue = model.GetSearchValue("Description");
                models = models.Where(m => m.Description.Contains(tmpValue));
            }
            if(model.IsSearchValueExist("Version"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Version"));
                models = models.Where(m => m.Version == tmpValue);
            }
            if(model.IsSearchValueExist("Image_path"))
            {
                string tmpValue = model.GetSearchValue("Image_path");
                models = models.Where(m => m.Image_path.Contains(tmpValue));
            }
            if(model.IsSearchValueExist("Device_image_path"))
            {
                string tmpValue = model.GetSearchValue("Device_image_path");
                models = models.Where(m => m.Device_image_path.Contains(tmpValue));
            }
            if(model.IsSearchValueExist("Estimated_time"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Estimated_time"));
                models = models.Where(m => m.Estimated_time == tmpValue);
            }
            if(model.IsSearchValueExist("Method"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Method"));
                models = models.Where(m => m.Method == tmpValue);
            }
            if(model.IsSearchValueExist("Help_text"))
            {
            }
            if(model.IsSearchValueExist("Json"))
            {
            }
            if(model.IsSearchValueExist("Display_order"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Display_order"));
                models = models.Where(m => m.Display_order == tmpValue);
            }
            if(model.IsSearchValueExist("Step"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Step"));
                models = models.Where(m => m.Step == tmpValue);
            }
            if(model.IsSearchValueExist("Period_Text"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Period_Text"));
                models = models.Where(m => m.Period == tmpValue);
            }
            if(model.IsSearchValueExist("Option_group_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Option_group_id"));
                models = models.Where(m => m.Option_group_id == tmpValue);
            }
            if(model.IsSearchValueExist("V_required"))
            {
                bool tmpValue = bool.Parse(model.GetSearchValue("V_required"));
                models = models.Where(m => m.V_required == tmpValue);
            }
            if(model.IsSearchValueExist("V_email"))
            {
                string tmpValue = model.GetSearchValue("V_email");
                models = models.Where(m => m.V_email.Contains(tmpValue));
            }
            if(model.IsSearchValueExist("V_min_length"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("V_min_length"));
                models = models.Where(m => m.V_min_length == tmpValue);
            }
            if(model.IsSearchValueExist("V_max_length"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("V_max_length"));
                models = models.Where(m => m.V_max_length == tmpValue);
            }
            if(model.IsSearchValueExist("V_regex"))
            {
                string tmpValue = model.GetSearchValue("V_regex");
                models = models.Where(m => m.V_regex.Contains(tmpValue));
            }
            if(model.IsSearchValueExist("Active"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Active"));

                if (tmpValue == 1)
                {
                    models = models.Where(m => m.Active == true);
                }
                else if (tmpValue == 2)
                {
                    models = models.Where(m => m.Active == false);
                }

            }
            if(model.IsSearchValueExist("Deleted"))
            {
                bool tmpValue = bool.Parse(model.GetSearchValue("Deleted"));
                models = models.Where(m => m.Deleted == tmpValue);
            }

            var totalRecords = models.Count();

            DTOrder order = null;
            if (model.Order != null && model.Order.Count() > 0)
            {
                order = model.Order.First();
                var pi = typeof(FormQuestion).GetProperty(model.Columns.ToList()[order.Column].Data);
                if (order.Dir == "asc")
                    models = models.OrderBy(x => pi.GetValue(x, null));
                else
                    models = models.OrderByDescending(x => pi.GetValue(x, null));

            }

            if (model.Length != -1)
                models = models.OrderBy(e => e.Id).Skip(model.Length * ((int)(Math.Ceiling((double)model.Start / model.Length)) + 1 - 1)).Take(model.Length);


            var models2 = models.Join(_iFormsvc.GetAll(),
                    a => a.Form_id,
                    q => q.Id,
                    (a, q) => new { FormQuestion = a, Form = q })
                    .Join(_iEquipmentsvc.GetAll(),
                     a => a.Form.Equipment_id,
                    d => d.Id,
                    (a, d) => new { FormQuestion = a.FormQuestion , Form = a.Form, Equipment = d })
                    .Join(_iEqLocationsvc.GetAll(),
                     a => a.Equipment.Eq_location_id,
                    d => d.Id,
                    (a, d) => new { FormQuestion = a.FormQuestion, Form = a.Form, Equipment = a.Equipment, Location = d })
                    .Join(_iParameterItemsvc.GetAll(),
                     a => a.FormQuestion.Category_id,
                    d => d.Id,
                    (a, d) => new { FormQuestion = a.FormQuestion, Form = a.Form, Equipment = a.Equipment, Location = a.Location, pItemCategory = d })
                    .Join(_iParameterItemsvc.GetAll(),
                     a => a.FormQuestion.Type_id,
                    d => d.Id,
                    (a, d) => new { FormQuestion = a.FormQuestion, Form = a.Form, Equipment = a.Equipment, Location = a.Location, pItemCategory = a.pItemCategory, pType = d  })
                    .Join(_iParameterItemsvc.GetAll(),
                     a => a.FormQuestion.Period,
                    d => d.Id,
                    (a, d) => new { FormQuestion = a.FormQuestion, Form = a.Form, Equipment = a.Equipment, Location = a.Location, pItemCategory = a.pItemCategory, pType = a.pType, pPeriod = d })

            ;

            /*
            DTOrder order = null;
            if (model.Order != null && model.Order.Count() > 0)
            {
                order = model.Order.First();
                var pi = typeof(FormQuestion).GetProperty(model.Columns.ToList()[order.Column].Data);
                if(order.Dir == "asc")
                    models = models.OrderBy(x => pi.GetValue(x, null));
                else
                    models = models.OrderByDescending(x => pi.GetValue(x, null));
                
            }

            var totalRecords = models.Count();

            if(model.Length != -1)
                models = models.OrderBy(e => e.Id).Skip(model.Length * ((int)(Math.Ceiling((double)model.Start / model.Length)) + 1 - 1)).Take(model.Length);

            */





            List<FormQuestionModel> aaData = models2.ToList().Select(m => { var item = m.FormQuestion.ToModel(ListMapperConfiguration.CONFIGURATION_NAME); item.Form_id_Text = m.Form.Title; item.Eq_location_id_Text = m.Location.Location_name; item.Category_id_Text = m.pItemCategory.GetLocalized(p => p.Title); item.Period_Text = m.pPeriod.GetLocalized(p => p.Title); item.Type_id_Text = m.pType.GetLocalized(p => p.Title);  return item; }).ToList();
            //List<FormQuestionModel> aaData = models2.ToList().Select(m => { var item = m.FormQuestion.ToModel(ListMapperConfiguration.CONFIGURATION_NAME); return item; }).ToList();

            JsonResult jsonRes = Json(new
            {
                sEcho = model.Draw,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                //aaData = result
                //aaData = models.ToList().Select(x => { var m = x.ToModel(); m.Period_Text = x.QUESTION_PERIOD_CATEGORY_PARAMITEMS == null ? "" : x.QUESTION_PERIOD_CATEGORY_PARAMITEMS.GetLocalized(p => p.Title); m.Category_id_Text = x.QUESTION_CATEGORY_PARAMITEMS == null ? "" : x.QUESTION_CATEGORY_PARAMITEMS.GetLocalized(p => p.Title); m.Derived_id_Text = x.QUESTION_DERIVED == null ? "" : x.QUESTION_DERIVED.Code; m.Form_id_Text = x.QUESTION_FORM == null ? "" : x.QUESTION_FORM.Title; m.Period_Text = x.QUESTION_PERIOD_CATEGORY_PARAMITEMS == null ? "" : x.QUESTION_PERIOD_CATEGORY_PARAMITEMS.GetLocalized(p => p.Title); m.Type_id_Text = x.QUESTION_TYPE_CATEGORY_PARAMITEMS == null ? "" : x.QUESTION_TYPE_CATEGORY_PARAMITEMS.GetLocalized(p => p.Title); m.Eq_location_id_Text = x.QUESTION_FORM == null ? "" : x.QUESTION_FORM.EQUIPMENT.Location.Location_name; return m;})
                aaData = aaData
            }, JsonRequestBehavior.AllowGet);
            jsonRes.MaxJsonLength = 50000000;

            return jsonRes;
        }

        [AresAuthorize(ResourceKey = "Admin.FormQuestion.Read")]
        public ActionResult ListKendo()
        {
            FormQuestionModel model = new FormQuestionModel();
            //model. = _roleSvc.GetAll().ToList().Select(x => x.ToModel()).ToList();
            FillDropDowns(model);


            FillDropDownFormAnswer(model.Filter_FormAnswerModel);


            //FillDropDownFormQuestionOption(model.Filter_FormQuestionOptionModel);


            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormQuestion.Read")]
        public ActionResult ListKendoQuestions([DataSourceRequest] DataSourceRequest request)
        {
            /*
            Ares.Web.Razor.Extensions.Datatable.Model.DTParameterModel model = new DTParameterModel();
            var result = ListFormQuestionDatatable(model);
            */
            var models = _iFormQuestionsvc.GetAll();
            

            var models2 = models.Join(_iFormsvc.GetAll(),
        a => a.Form_id,
        q => q.Id,
        (a, q) => new { FormQuestion = a, Form = q })
        .Join(_iEquipmentsvc.GetAll(),
         a => a.Form.Equipment_id,
        d => d.Id,
        (a, d) => new { FormQuestion = a.FormQuestion, Form = a.Form, Equipment = d })
        .Join(_iEqLocationsvc.GetAll(),
         a => a.Equipment.Eq_location_id,
        d => d.Id,
        (a, d) => new { FormQuestion = a.FormQuestion, Form = a.Form, Equipment = a.Equipment, Location = d })
        .Join(_iParameterItemsvc.GetAll(),
         a => a.FormQuestion.Category_id,
        d => d.Id,
        (a, d) => new { FormQuestion = a.FormQuestion, Form = a.Form, Equipment = a.Equipment, Location = a.Location, pItemCategory = d })
        .Join(_iParameterItemsvc.GetAll(),
         a => a.FormQuestion.Type_id,
        d => d.Id,
        (a, d) => new { FormQuestion = a.FormQuestion, Form = a.Form, Equipment = a.Equipment, Location = a.Location, pItemCategory = a.pItemCategory, pType = d })
        .Join(_iParameterItemsvc.GetAll(),
         a => a.FormQuestion.Period,
        d => d.Id,
        (a, d) => new { FormQuestion = a.FormQuestion, Form = a.Form, Equipment = a.Equipment, Location = a.Location, pItemCategory = a.pItemCategory, pType = a.pType, pPeriod = d })

;

            var resultQuery = models2.OrderBy(q => q.FormQuestion.Id).Skip(1000).Take(10).ToList().Select(q =>
            new AaExcelImportModel() { Id = q.FormQuestion.Id, Location_name = q.Location.Location_name,
                Equipment_name = q.Equipment.Name, Object_id = q.FormQuestion.Object_id,
                Period = q.pPeriod.Title, Category = q.pItemCategory.Title, Question_type = q.pType.Title, Code = q.FormQuestion.Code, Title = q.FormQuestion.Title,
                Display_order = q.FormQuestion.Display_order == null ? "" : q.FormQuestion.Display_order.ToString(),
                Unhealthy_cause = q.FormQuestion.Unhealthy_cause }).ToList();

            var result = resultQuery as IEnumerable<AaExcelImportModel>;

            
            var jsonResolver = new PropertyRenameAndIgnoreSerializerContractResolver();
            jsonResolver.IgnoreProperty(typeof(AaExcelImportModel), "Threshold_repetition_count");
            jsonResolver.IgnoreProperty(typeof(AaExcelImportModel), "Answer_deadline_day_count");
            jsonResolver.IgnoreProperty(typeof(AaExcelImportModel), "Answer_deadline_hour_count");
            jsonResolver.IgnoreProperty(typeof(AaExcelImportModel), "Criticality");
            jsonResolver.IgnoreProperty(typeof(AaExcelImportModel), "Sublocation_name");
            jsonResolver.IgnoreProperty(typeof(AaExcelImportModel), "Is_deleted"); 
            //jsonResolver.RenameProperty(typeof(AaExcelImportModel), "FirstName", "firstName");



            AresJSONNetResult jsonResult = new AresJSONNetResult();
            //var serializerSettings = new JsonSerializerSettings();
            jsonResult.Settings.ContractResolver = jsonResolver;

            jsonResult.Data = result.ToDataSourceResult(request);
            jsonResult.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            /*
            var deneme = Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);

            
            var json = JsonConvert.SerializeObject(result, serializerSettings);

            return Content(json, "application/json");
            */


            //return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            //return new LargeJsonResult() { Data = result.ToDataSourceResult(request) };
            return jsonResult;
        }

        [AresAuthorize(ResourceKey = "Admin.FormQuestion.Read")]
        public ActionResult ListExcel()
        {
            FormQuestionModel model = new FormQuestionModel();
            //model. = _roleSvc.GetAll().ToList().Select(x => x.ToModel()).ToList();
            FillDropDowns(model);


            FillDropDownFormAnswer(model.Filter_FormAnswerModel);


            //FillDropDownFormQuestionOption(model.Filter_FormQuestionOptionModel);


            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormQuestion.Create")]
        public ActionResult Spreadsheet_Submit(SpreadsheetSubmitViewModel model)
        {
            var result = new SpreadsheetSubmitViewModel()
            {
                Created = new List<AaExcelImportModel>(),
                Updated = new List<AaExcelImportModel>(),
                Destroyed = new List<AaExcelImportModel>()
            };

            if ((model.Created != null || model.Updated != null || model.Destroyed != null) && ModelState.IsValid)
            {
                if (model.Created != null)
                {
                    foreach (var created in model.Created)
                    {
                        //productService.Create(created);
                        result.Created.Add(created);
                    }
                }

                if (model.Updated != null)
                {
                    foreach (var updated in model.Updated)
                    {
                        //productService.Update(updated);
                        result.Updated.Add(updated);
                    }
                }

                if (model.Destroyed != null)
                {
                    foreach (var destroyed in model.Destroyed)
                    {
                        //productService.Destroy(destroyed);
                        result.Destroyed.Add(destroyed);
                    }
                }

                return Json(result);
            }
            else
            {
                return new HttpStatusCodeResult(400, "The models contain invalid property values.");
            }
        }

        [AresAuthorize(ResourceKey = "Admin.FormQuestion.Create")]
        public ActionResult Create()
        {
            FormQuestionModel model = new FormQuestionModel();
            FillDropDowns(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormQuestion.Create")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Create([Bind(Exclude = "Filter_FormAnswerModel,Filter_FormQuestionOptionModel")] FormQuestionModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {                
                var record = model.ToEntity();
                
                record.Object_id = Guid.NewGuid().ToString();
                record.Insert_user_id = _workContext.CurrentUser.Id.ToString();
                record.Insert_datetime = StringHelper.ConvertDateTimeToString(DateTime.Now);
                
            
                record = _iFormQuestionsvc.Insert(record);

                _iFormQuestionsvc.insertOrUpdateChannelAndThreshold(record);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormQuestion.Created"));
                
                if (continueEditing)
                    return RedirectToAction("Edit", new { Id = record.Id });

                return RedirectToAction("List");
            }
            else
            {
                FillDropDowns(model);
                return View(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormQuestion.Update")]
        //[FormQuestionAuthorize]
        public ActionResult Edit(int id, string SelectedTab)
        {
            FormQuestion record = _iFormQuestionsvc.GetById(id);
            var model = record.ToModel();
            model.SelectedTab = SelectedTab;
                        
            FillDropDowns(model);
            
            FillDropDownFormAnswer(model.Filter_FormAnswerModel);
                       
            
            //FillDropDownFormQuestionOption(model.Filter_FormQuestionOptionModel);
            FillDropDownEqThreshold(model.Filter_EqThresholdModel);


            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormQuestion.Update")]
        [FormQuestionAuthorize]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Edit([Bind(Exclude = "Filter_FormAnswerModel,Filter_FormQuestionOptionModel")] FormQuestionModel model, bool continueEditing)
        {
            var item = _iFormQuestionsvc.GetById(model.Id);
            FormQuestion originalItem = (FormQuestion)item.Clone();
            if (item == null)
                //No country found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                item = model.ToEntity(item);
                item.Object_id = originalItem.Object_id;                
                item.Insert_user_id = originalItem.Insert_user_id;                
                item.Insert_datetime = originalItem.Insert_datetime;

                JsonFormWizard wizard = new JsonFormWizard();
                item.Json = wizard.GenerateJSONFromDTOFormQuestion(item);

                item.Update_user_id = _workContext.CurrentUser.Id.ToString();
                item.Update_datetime = StringHelper.ConvertDateTimeToString(DateTime.Now);

                _iFormQuestionsvc.Update(item);

                _iFormQuestionsvc.insertOrUpdateChannelAndThreshold(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormQuestion.Updated"));
                
                if(continueEditing)
                    return RedirectToAction("Edit", new { Id = item.Id });

                return RedirectToAction("List");
            }
            else
            {
                FillDropDowns(model);
            
            FillDropDownFormAnswer(model.Filter_FormAnswerModel);
                       
            
            FillDropDownFormQuestionOption(model.Filter_FormQuestionOptionModel);
                       
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormQuestion.Delete")]
        [FormQuestionAuthorize]
        public ActionResult Delete(int id)
        {

            var item = _iFormQuestionsvc.GetById(id);
            if (item == null)
                return RedirectToAction("List");

            try
            {
                item.FORM_QUESTIONForm_answer.ToList().ForEach(d => _iFormAnswersvc.Delete(d));
                item.FORM_QUESTIONForm_question_option.ToList().ForEach(d => _iFormQuestionOptionsvc.Delete(d));
                
                _iFormQuestionsvc.Delete(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormQuestion.Deleted"));
                return RedirectToAction("List");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("Edit", new { id = item.Id });
            }
        }
        
        
        public ActionResult CreateOrUpdate(string itemId, string imageUrl, string mimeType, string uploadFilename)
        {
            FormQuestionModel response = null;
            

            int nItemId = -1;
            int.TryParse(itemId, out nItemId);

            if (nItemId != -1)
            {
                FormQuestion record = _iFormQuestionsvc.GetById(nItemId);
                response = record.ToModel();
                if (record != null) // update
                {

                   

                }
                else // insert mode (after image upload popup close)
                {
                    response = new FormQuestionModel();
                    //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() { Text = "" } };
                }
            }
            else // insert
            {
                response = new FormQuestionModel();
                //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() {Text = "" }  };
                
            }

 
            ViewBag.itemId = nItemId;

            return View(response);
        }
        
        
        public void FillDropDowns(FormQuestionModel model)
        {

            int userId = _workContext.CurrentUser.Id;
            model.Category_id_ParameterItem_SelectList = new SelectList(_iParameterItemsvc.GetAllFromCache().Where(p => p.Group.System_name == "FORMQUESTIONCATEGORY").ToList().Select(p => { return new { Id = p.Id, Title = p.GetLocalized(l => l.Title) }; }), "Id", "Title");

            List<int> authorizedEquipments = _iEquipmentsvc.GetAuthorizedEquipments(userId).Select(e => e.Id).ToList();

            //model.Derived_id_FormQuestion_SelectList = new SelectList( _iFormQuestionsvc.GetAll().ToList(), "Id", "Code");

            model.Form_id_Form_SelectList = new SelectList(_iFormsvc.GetAll().Where(f => authorizedEquipments.Contains(f.EQUIPMENT.Id)).ToList(), "Id", "Title");


            model.Period_ParameterItem_SelectList = new SelectList(_iParameterItemsvc.GetAllFromCache().Where(p => p.Group.System_name == "FORMQUESTIONPERIOD").ToList().Select(p => { return new { Id = p.Id, Title = p.GetLocalized(l => l.Title) }; }), "Id", "Title");

            model.Type_id_ParameterItem_SelectList = new SelectList(_iParameterItemsvc.GetAllFromCache().Where(p => p.Group.System_name == "FORMQUESTIONTYPE").ToList().Select(p => { return new { Id = p.Id, Title = p.GetLocalized(l => l.Title) }; }), "Id", "Title");

            List<SelectListItem> yesNoSelectList = new List<SelectListItem>();
            yesNoSelectList.Add(new SelectListItem() { Text = _localizationSvc.GetResource("select") });
            yesNoSelectList.Add(new SelectListItem() { Value = "1", Text = _localizationSvc.GetResource("Admin.Common.Yes") });
            yesNoSelectList.Add(new SelectListItem() { Value = "2", Text = _localizationSvc.GetResource("Admin.Common.No") });

            model.YesNo_SelectList = new SelectList(yesNoSelectList, "Value", "Text");

            model.Eq_location_id_EqLocation_SelectList = new SelectList(_iEqLocationsvc.GetAuthorizedLocations(userId), "Id", "Location_Name");

            if (model.Id > 0 && model.Form_id > 0)
            {
                Form form = _iFormsvc.GetById(model.Form_id);
                model.Eq_channel_id_EqChannel_SelectList = new SelectList(_iEqChannelsvc.GetAuthorizedChannels(_workContext.CurrentUser.Id).Where(c => c.Equipment_id == form.Equipment_id), "Id", "Name");
            }

            model.Option_group_id_ParameterGroup_SelectList = new SelectList(_iParameterGroupsvc.GetAllFromCache().Where(p => p.System_name != null && p.System_name.ToUpper().StartsWith("OPTION")).ToList().Select(p => new { Id = p.Id, Name = p.GetLocalized(l => l.Name) }), "Id", "Name");


        }
        
        #endregion
                         



            
        #region  FormAnswer methods
        
        public void FillDropDownFormAnswer(FormAnswerModel model)
        {

            //model.Previous_answer_FormAnswer_SelectList = new SelectList(_iFormAnswersvc.GetAll().ToList(), "Id", "Object_id");

            //model.Form_question_id_FormQuestion_SelectList = new SelectList(_iFormQuestionsvc.GetAll().ToList(), "Id", "Object_id");

            //model.Form_answer_round_id_FormAnswerRound_SelectList = new SelectList(_iFormAnswerRoundsvc.GetAll().ToList(), "Id", "Object_id");

            model.Warning_type_ParameterItem_SelectList = new SelectList(_iParameterItemsvc.GetAllFromCache().Where(p => p.Group.System_name == "FORM_ANSWER_WARNING_TYPE").ToList().Select(p => { return new { Id = p.Id, Title = p.GetLocalized(l => l.Title) }; }), "Id", "Title");
        }
        
        public ActionResult CreateOrUpdateFormAnswer(string itemId, string imageUrl, string mimeType, string uploadFilename)
        {
            FormAnswerModel response = null;


            int nItemId = -1;
            int.TryParse(itemId, out nItemId);

            if (nItemId != -1)
            {
                FormAnswer record = _iFormAnswersvc.GetById(nItemId);
                response = record.ToModel();
                if (record != null) // update
                {



                    //response.RoleListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem { Text = "Administrators" }, new Ext.Net.ListItem { Text = "Guests" } };
                    //response.PermissionListItems = permListItems.ToArray();

                }
                else // insert mode (after image upload popup close)
                {
                    response = new FormAnswerModel();
                    //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() { Text = "" } };
                }
            }
            else // insert
            {
                response = new FormAnswerModel();
                //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() {Text = "" }  };

            }


            ViewBag.itemId = nItemId;

            return View(response);
        }
        
        [AresAuthorize(ResourceKey = "Admin.FormAnswer.Read")]
        public ActionResult ListFormAnswerDatatable(Ares.Web.Razor.Extensions.Datatable.Model.DTParameterModel model, int? FormQuestionModelId)
        {
            var models = _iFormAnswersvc.GetAll();
            models = models.Where(m => m.Form_question_id == FormQuestionModelId);
            if (model.Search != null && !string.IsNullOrEmpty(model.Search.Value))
            {
                models = models.Where(m => m.Udid.Contains(model.Search.Value));
            }
            
            if (model.IsSearchValueExist("Form_question_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Form_question_id"));
                models = models.Where(m => m.Form_question_id == tmpValue);
            }
            if (model.IsSearchValueExist("Device_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Device_id"));
                models = models.Where(m => m.Device_id == tmpValue);
            }
            if (model.IsSearchValueExist("Udid"))
            {
                string tmpValue = model.GetSearchValue("Udid");
                models = models.Where(m => m.Udid.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Username"))
            {
                string tmpValue = model.GetSearchValue("Username");
                models = models.Where(m => m.Username.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Read_no"))
            {
                long tmpValue = long.Parse(model.GetSearchValue("Read_no"));
                models = models.Where(m => m.Read_no == tmpValue);
            }
            if (model.IsSearchValueExist("Answer"))
            {
                string tmpValue = model.GetSearchValue("Answer");
                models = models.Where(m => m.Answer.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Long_answer"))
            {
            }
            if (model.IsSearchValueExist("Converted_answer"))
            {
                string tmpValue = model.GetSearchValue("Converted_answer");
                models = models.Where(m => m.Converted_answer.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Description"))
            {
                string tmpValue = model.GetSearchValue("Description");
                models = models.Where(m => m.Description.Contains(tmpValue));
            }
            
            DTOrder order = null;
            if (model.Order != null && model.Order.Count() > 0)
            {
                order = model.Order.First();
                var pi = typeof(FormAnswer).GetProperty(model.Columns.ToList()[order.Column].Data);
                if(order.Dir == "asc")
                    models = models.OrderBy(x => pi.GetValue(x, null));
                else
                    models = models.OrderByDescending(x => pi.GetValue(x, null));
                
            }

            var totalRecords = models.Count();
            
            models = models.OrderBy(e => e.Id).Skip(model.Length * ((int)(Math.Ceiling((double)model.Start / model.Length)) + 1 - 1)).Take(model.Length);

            JsonResult jsonRes = Json(new
            {
                sEcho = model.Draw,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                //aaData = result
                aaData = models.ToList().Select(x => { var m = x.ToModel(); m.Form_question_id_Text = x.ANSWER_QUESTION == null ? "" : x.ANSWER_QUESTION.Code;  return m;})
            }, JsonRequestBehavior.AllowGet);
            return jsonRes;
        }
        
        [AresAuthorize(ResourceKey = "Admin.FormAnswer.Create")]
        public ActionResult CreateFormAnswer(int FormQuestionModelId)
        {
            FormAnswerModel model = new FormAnswerModel();
            model.Form_question_id = FormQuestionModelId;
            
            
            FillDropDownFormAnswer(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormAnswer.Create")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult CreateFormAnswer( FormAnswerModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {                
                var record = model.ToEntity();
                
                
                record = _iFormAnswersvc.Insert(record);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormAnswer.Created"));
                
                if (continueEditing)
                    return RedirectToAction("EditFormAnswer", new { id = record.Id });

                return RedirectToAction("Edit", new  { id = model.Form_question_id, SelectedTab = "2" });
            }
            else
            {
                FillDropDownFormAnswer(model);
                return View(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }
        
        public ActionResult EditFormAnswer(int id, string SelectedTab)
        {
            FormAnswer record = _iFormAnswersvc.GetById(id);
            var model = record.ToModel();
            model.SelectedTab = SelectedTab;
            
            
            
            FillDropDownFormAnswer(model);

            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormAnswer.Update")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult EditFormAnswer( FormAnswerModel model, bool continueEditing)
        {
            var item = _iFormAnswersvc.GetById(model.Id);
            FormAnswer originalItem = (FormAnswer)item.Clone();
            if (item == null)
                //No country found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                item = model.ToEntity(item);
                item.Object_id = originalItem.Object_id;
                item.Insert_user_id = originalItem.Insert_user_id;
                item.Insert_datetime = originalItem.Insert_datetime;


                _iFormAnswersvc.Update(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormAnswer.Updated"));
                
                if(continueEditing)
                    return RedirectToAction("EditFormAnswer", new { id = item.Id });
                
                return RedirectToAction("Edit", new  { id = model.Form_question_id, SelectedTab = "2" });
            }

            //If we got this far, something failed, redisplay form
            FillDropDownFormAnswer(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormAnswer.Delete")]
        public ActionResult DeleteFormAnswer(int id)
        {

            var item = _iFormAnswersvc.GetById(id);
            if (item == null)
                return RedirectToAction("List");

            try
            {
                
                _iFormAnswersvc.Delete(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormAnswer.Deleted"));
                
                return RedirectToAction("Edit", new  { id = item.Form_question_id, SelectedTab = "2" });
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("EditFormAnswer", new { id = item.Id });
            }
        }
        #endregion 
          



        





            
        #region  FormQuestionOption methods
        
        public void FillDropDownFormQuestionOption(FormQuestionOptionModel model)
        {

            model.Derived_id_FormQuestionOption_SelectList = new SelectList(_iFormQuestionOptionsvc.GetAll().ToList(), "Id", "Code");

            model.Form_question_id_FormQuestion_SelectList = new SelectList(_iFormQuestionsvc.GetAll().ToList(), "Id", "Code");
        }
        
        public ActionResult CreateOrUpdateFormQuestionOption(string itemId, string imageUrl, string mimeType, string uploadFilename)
        {
            FormQuestionOptionModel response = null;


            int nItemId = -1;
            int.TryParse(itemId, out nItemId);

            if (nItemId != -1)
            {
                FormQuestionOption record = _iFormQuestionOptionsvc.GetById(nItemId);
                response = record.ToModel();
                if (record != null) // update
                {



                    //response.RoleListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem { Text = "Administrators" }, new Ext.Net.ListItem { Text = "Guests" } };
                    //response.PermissionListItems = permListItems.ToArray();

                }
                else // insert mode (after image upload popup close)
                {
                    response = new FormQuestionOptionModel();
                    //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() { Text = "" } };
                }
            }
            else // insert
            {
                response = new FormQuestionOptionModel();
                //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() {Text = "" }  };

            }


            ViewBag.itemId = nItemId;

            return View(response);
        }
        
        [AresAuthorize(ResourceKey = "Admin.FormQuestionOption.Read")]
        public ActionResult ListFormQuestionOptionDatatable(Ares.Web.Razor.Extensions.Datatable.Model.DTParameterModel model, int? FormQuestionModelId)
        {
            var models = _iFormQuestionOptionsvc.GetAll();
            models = models.Where(m => m.Form_question_id == FormQuestionModelId);
            if (model.Search != null && !string.IsNullOrEmpty(model.Search.Value))
            {
                models = models.Where(m => m.Code.Contains(model.Search.Value));
            }
            
            if (model.IsSearchValueExist("Form_question_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Form_question_id"));
                models = models.Where(m => m.Form_question_id == tmpValue);
            }
            if (model.IsSearchValueExist("Derived_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Derived_id"));
                models = models.Where(m => m.Derived_id == tmpValue);
            }
            if (model.IsSearchValueExist("Code"))
            {
                string tmpValue = model.GetSearchValue("Code");
                models = models.Where(m => m.Code.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Title"))
            {
                string tmpValue = model.GetSearchValue("Title");
                models = models.Where(m => m.Title.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Hint"))
            {
                string tmpValue = model.GetSearchValue("Hint");
                models = models.Where(m => m.Hint.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Description"))
            {
                string tmpValue = model.GetSearchValue("Description");
                models = models.Where(m => m.Description.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Version"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Version"));
                models = models.Where(m => m.Version == tmpValue);
            }
            if (model.IsSearchValueExist("Image_path"))
            {
                string tmpValue = model.GetSearchValue("Image_path");
                models = models.Where(m => m.Image_path.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Device_image_path"))
            {
                string tmpValue = model.GetSearchValue("Device_image_path");
                models = models.Where(m => m.Device_image_path.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Estimated_time"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Estimated_time"));
                models = models.Where(m => m.Estimated_time == tmpValue);
            }
            if (model.IsSearchValueExist("Video_path"))
            {
                string tmpValue = model.GetSearchValue("Video_path");
                models = models.Where(m => m.Video_path.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Device_video_path"))
            {
                string tmpValue = model.GetSearchValue("Device_video_path");
                models = models.Where(m => m.Device_video_path.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Method"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Method"));
                models = models.Where(m => m.Method == tmpValue);
            }
            if (model.IsSearchValueExist("Display_order"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Display_order"));
                models = models.Where(m => m.Display_order == tmpValue);
            }
            if (model.IsSearchValueExist("Period"))
            {
                string tmpValue = model.GetSearchValue("Period");
                models = models.Where(m => m.Period.Contains(tmpValue));
            }
            if (model.IsSearchValueExist("Active"))
            {
                bool tmpValue = bool.Parse(model.GetSearchValue("Active"));
                models = models.Where(m => m.Active == tmpValue);
            }
            if (model.IsSearchValueExist("Deleted"))
            {
                bool tmpValue = bool.Parse(model.GetSearchValue("Deleted"));
                models = models.Where(m => m.Deleted == tmpValue);
            }
            
            DTOrder order = null;
            if (model.Order != null && model.Order.Count() > 0)
            {
                order = model.Order.First();
                var pi = typeof(FormQuestionOption).GetProperty(model.Columns.ToList()[order.Column].Data);
                if(order.Dir == "asc")
                    models = models.OrderBy(x => pi.GetValue(x, null));
                else
                    models = models.OrderByDescending(x => pi.GetValue(x, null));
                
            }

            var totalRecords = models.Count();
            
            models = models.OrderBy(e => e.Id).Skip(model.Length * ((int)(Math.Ceiling((double)model.Start / model.Length)) + 1 - 1)).Take(model.Length);

            JsonResult jsonRes = Json(new
            {
                sEcho = model.Draw,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                //aaData = result
                aaData = models.ToList().Select(x => { var m = x.ToModel(); m.Derived_id_Text = x.QUESTION_OPTION_DERIVED == null ? "" : x.QUESTION_OPTION_DERIVED.Code; m.Form_question_id_Text = x.QUESTION_OPTION_QUESTION == null ? "" : x.QUESTION_OPTION_QUESTION.Code;  return m;})
            }, JsonRequestBehavior.AllowGet);
            return jsonRes;
        }
        
        [AresAuthorize(ResourceKey = "Admin.FormQuestionOption.Create")]
        public ActionResult CreateFormQuestionOption(int FormQuestionModelId)
        {
            FormQuestionOptionModel model = new FormQuestionOptionModel();
            model.Form_question_id = FormQuestionModelId;
            
            
            FillDropDownFormQuestionOption(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormQuestionOption.Create")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult CreateFormQuestionOption( FormQuestionOptionModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {                
                var record = model.ToEntity();
                
                
                record = _iFormQuestionOptionsvc.Insert(record);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormQuestionOption.Created"));
                
                if (continueEditing)
                    return RedirectToAction("EditFormQuestionOption", new { id = record.Id });

                return RedirectToAction("Edit", new  { id = model.Form_question_id, SelectedTab = "3" });
            }
            else
            {
                FillDropDownFormQuestionOption(model);
                return View(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }
        
        public ActionResult EditFormQuestionOption(int id, string SelectedTab)
        {
            FormQuestionOption record = _iFormQuestionOptionsvc.GetById(id);
            var model = record.ToModel();
            model.SelectedTab = SelectedTab;
            
            
            
            FillDropDownFormQuestionOption(model);

            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormQuestionOption.Update")]
        [System.Web.Mvc.HttpPost,  ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult EditFormQuestionOption( FormQuestionOptionModel model, bool continueEditing)
        {
            var item = _iFormQuestionOptionsvc.GetById(model.Id);
            FormQuestionOption originalItem = (FormQuestionOption)item.Clone();
            if (item == null)
                //No country found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                item = model.ToEntity(item);
                item.Object_id = originalItem.Object_id;
                item.Insert_user_id = originalItem.Insert_user_id;
                item.Insert_datetime = originalItem.Insert_datetime;


                _iFormQuestionOptionsvc.Update(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormQuestionOption.Updated"));
                
                if(continueEditing)
                    return RedirectToAction("EditFormQuestionOption", new { id = item.Id });
                
                return RedirectToAction("Edit", new  { id = model.Form_question_id, SelectedTab = "3" });
            }

            //If we got this far, something failed, redisplay form
            FillDropDownFormQuestionOption(model);
            
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.FormQuestionOption.Delete")]
        public ActionResult DeleteFormQuestionOption(int id)
        {

            var item = _iFormQuestionOptionsvc.GetById(id);
            if (item == null)
                return RedirectToAction("List");

            try
            {
                
                _iFormQuestionOptionsvc.Delete(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.FormQuestionOption.Deleted"));
                
                return RedirectToAction("Edit", new  { id = item.Form_question_id, SelectedTab = "3" });
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("EditFormQuestionOption", new { id = item.Id });
            }
        }
        #endregion



        #region  EqThreshold methods

        public void FillDropDownEqThreshold(EqThresholdModel model)
        {

            model.Warning_type_ParameterItem_SelectList = new SelectList(_iParameterItemsvc.GetAllFromCache().Where(p => p.Group.System_name == "EQ_WARNING_WARNING_TYPE").ToList().Select(p => { return new { Id = p.Id, Title = p.GetLocalized(l => l.Title) }; }), "Id", "Title");

            model.Eq_channel_id_EqChannel_SelectList = new SelectList(_iEqChannelsvc.GetAllFromCache().ToList(), "Id", "Name");
        }

        public ActionResult CreateOrUpdateEqThreshold(string itemId, string imageUrl, string mimeType, string uploadFilename)
        {
            EqThresholdModel response = null;


            int nItemId = -1;
            int.TryParse(itemId, out nItemId);

            if (nItemId != -1)
            {
                EqThreshold record = _iEqThresholdsvc.GetById(nItemId);
                response = record.ToModel();
                if (record != null) // update
                {



                    //response.RoleListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem { Text = "Administrators" }, new Ext.Net.ListItem { Text = "Guests" } };
                    //response.PermissionListItems = permListItems.ToArray();

                }
                else // insert mode (after image upload popup close)
                {
                    response = new EqThresholdModel();
                    //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() { Text = "" } };
                }
            }
            else // insert
            {
                response = new EqThresholdModel();
                //response.PermissionListItems = new Ext.Net.ListItem[] { new Ext.Net.ListItem() {Text = "" }  };

            }


            ViewBag.itemId = nItemId;

            return View(response);
        }

        [AresAuthorize(ResourceKey = "Admin.EqThreshold.Read")]
        public ActionResult ListEqThresholdDatatable(Ares.Web.Razor.Extensions.Datatable.Model.DTParameterModel model, int? EqChannelModelId)
        {
            var models = _iEqThresholdsvc.GetAll();
            if (!_workContext.CurrentUser.IsAdmin())
            {
                List<int> relatedLocationIds = _workContext.CurrentUser.RelatedLocations().Select(r => r.Id).ToList();
                models = models.Where(e => relatedLocationIds.Contains(e.threshold_eq_channel.channel_equipment.Location.Id));
            }

            models = models.Where(m => m.Eq_channel_id == EqChannelModelId);
            if (model.Search != null && !string.IsNullOrEmpty(model.Search.Value))
            {
                models = models.Where(m => m.Object_id.Contains(model.Search.Value));
            }

            if (model.IsSearchValueExist("Eq_channel_id"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Eq_channel_id"));
                models = models.Where(m => m.Eq_channel_id == tmpValue);
            }
            if (model.IsSearchValueExist("Lower_limit"))
            {
            }
            if (model.IsSearchValueExist("Upper_limit"))
            {
            }
            if (model.IsSearchValueExist("Warning_type"))
            {
                int tmpValue = int.Parse(model.GetSearchValue("Warning_type"));
                models = models.Where(m => m.Warning_type == tmpValue);
            }
            if (model.IsSearchValueExist("Active"))
            {
                bool tmpValue = bool.Parse(model.GetSearchValue("Active"));
                models = models.Where(m => m.Active == tmpValue);
            }

            DTOrder order = null;
            if (model.Order != null && model.Order.Count() > 0)
            {
                order = model.Order.First();
                var pi = typeof(EqThreshold).GetProperty(model.Columns.ToList()[order.Column].Data);
                if (order.Dir == "asc")
                    models = models.OrderBy(x => pi.GetValue(x, null));
                else
                    models = models.OrderByDescending(x => pi.GetValue(x, null));

            }

            var totalRecords = models.Count();

            models = models.OrderBy(e => e.Id).Skip(model.Length * ((int)(Math.Ceiling((double)model.Start / model.Length)) + 1 - 1)).Take(model.Length);

            JsonResult jsonRes = Json(new
            {
                sEcho = model.Draw,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                //aaData = result
                aaData = models.ToList().Select(x => { var m = x.ToModel(); m.Warning_type_Text = x.THRESHOLD_WARNING_TYPE_PARAMITEMS == null ? "" : x.THRESHOLD_WARNING_TYPE_PARAMITEMS.GetLocalized(t => t.Title); m.Eq_channel_id_Text = x.threshold_eq_channel == null ? "" : x.threshold_eq_channel.Object_id; m.Upper_limit_Text = m.threshold_eq_channel.Sensor_type == (int)EnumParameterItem.PARAM_ITEM_GORSEL_KONTROL ? ((int)m.Upper_limit.GetValueOrDefault() == (int)EnumParameterItem.PARAM_ITEM_OPTION_YES ? "Evet" : "Hayır") : m.Upper_limit.ToString(); return m; })
            }, JsonRequestBehavior.AllowGet);
            return jsonRes;
        }

        [AresAuthorize(ResourceKey = "Admin.EqThreshold.Create")]
        public ActionResult CreateEqThreshold(int EqChannelModelId)
        {
            EqThresholdModel model = new EqThresholdModel();
            model.Eq_channel_id = EqChannelModelId;


            FillDropDownEqThreshold(model);

            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.EqThreshold.Create")]
        [System.Web.Mvc.HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult CreateEqThreshold(EqThresholdModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {
                var record = model.ToEntity();

                record.Object_id = Guid.NewGuid().ToString();
                record.Insert_user_id = _workContext.CurrentUser.Id.ToString();
                record.Insert_datetime = StringHelper.ConvertDateTimeToString(DateTime.Now);


                record = _iEqThresholdsvc.Insert(record);

                SuccessNotification(_localizationSvc.GetResource("Notifications.EqThreshold.Created"));

                if (continueEditing)
                    return RedirectToAction("EditEqThreshold", new { id = record.Id });

                return RedirectToAction("Edit", new { id = model.Eq_channel_id, SelectedTab = "2" });
            }
            else
            {
                FillDropDownEqThreshold(model);
                return View(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.EqThreshold.Update")]
        [EqThresholdAuthorize]
        public ActionResult EditEqThreshold(int id, string SelectedTab)
        {
            EqThreshold record = _iEqThresholdsvc.GetById(id);
            var model = record.ToModel();
            model.SelectedTab = SelectedTab;



            FillDropDownEqThreshold(model);

            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.EqThreshold.Update")]
        [EqThresholdAuthorize]
        [System.Web.Mvc.HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult EditEqThreshold(EqThresholdModel model, bool continueEditing)
        {
            var item = _iEqThresholdsvc.GetById(model.Id);
            EqThreshold originalItem = (EqThreshold)item.Clone();
            if (item == null)
                //No country found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                item = model.ToEntity(item);
                item.Object_id = originalItem.Object_id;
                item.Insert_user_id = originalItem.Insert_user_id;
                item.Insert_datetime = originalItem.Insert_datetime;



                item.Update_user_id = _workContext.CurrentUser.Id.ToString();
                item.Update_datetime = StringHelper.ConvertDateTimeToString(DateTime.Now);

                _iEqThresholdsvc.Update(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.EqThreshold.Updated"));

                if (continueEditing)
                    return RedirectToAction("EditEqThreshold", new { id = item.Id });

                return RedirectToAction("Edit", new { id = model.Eq_channel_id, SelectedTab = "2" });
            }

            //If we got this far, something failed, redisplay form
            FillDropDownEqThreshold(model);

            return View(model);
        }

        [AresAuthorize(ResourceKey = "Admin.EqThreshold.Delete")]
        [EqThresholdAuthorize]
        public ActionResult DeleteEqThreshold(int id)
        {

            var item = _iEqThresholdsvc.GetById(id);
            if (item == null)
                return RedirectToAction("List");

            try
            {

                _iEqThresholdsvc.Delete(item);

                SuccessNotification(_localizationSvc.GetResource("Notifications.EqThreshold.Deleted"));

                return RedirectToAction("Edit", new { id = item.Eq_channel_id, SelectedTab = "2" });
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("EditEqThreshold", new { id = item.Id });
            }
        }
        #endregion





        public ActionResult BatchQuestionOpPopup(int? id)
        {
            BatchQuestionOperationModel model = new BatchQuestionOperationModel();
            model.CustomProperties.Add("id", id);
            model.Id = id.GetValueOrDefault();


            var models = _iFormsvc.GetAll();
            var questionModels = _iFormQuestionsvc.GetAll();

            if (!_workContext.CurrentUser.IsAdmin())
            {
                List<int> relatedLocationIds = _workContext.CurrentUser.RelatedLocations().Select(r => r.Id).ToList();
                models = models.Where(e => relatedLocationIds.Contains(e.EQUIPMENT.Location.Id));
                questionModels = questionModels.Where(e => relatedLocationIds.Contains(e.QUESTION_FORM.EQUIPMENT.Location.Id));
            }

            model.Form_id_Form_SelectList = new SelectList(models.ToList().Select(u => new { Id = u.Id, Title = u.Title }), "Id", "Title");
            model.FormList = new List<string>().ToArray();

            model.FormQuestion_id_FormQuestion_SelectList = new SelectList(Enumerable.Empty<SelectListItem>());
            model.FormQuestion_id_FormQuestion_SelectList = new SelectList(questionModels.ToList().Select(u => new { Id = u.Id, Title = (string.IsNullOrEmpty( u.Code) ? "": u.Code + " - ") + u.Title }), "Id", "Title"); 

            model.FormQuestionList = new List<string>().ToArray();



            model.Period_ParameterItem_SelectList = new SelectList(_iParameterItemsvc.GetAllFromCache().Where(p => p.Group.System_name == "FORMQUESTIONPERIOD").ToList().Select(p => { return new { Id = p.Id, Title = p.GetLocalized(l => l.Title) }; }), "Id", "Title");

            //model.Type_id_ParameterItem_SelectList = new SelectList(_iParameterItemsvc.GetAll().Where(p => p.Group.System_name == "FORMQUESTIONTYPE").ToList().Select(p => { return new { Id = p.Id, Title = p.GetLocalized(l => l.Title) }; }), "Id", "Title");

            List<SelectListItem> yesNoSelectList = new List<SelectListItem>();
            yesNoSelectList.Add(new SelectListItem() { Text = _localizationSvc.GetResource("select") });
            yesNoSelectList.Add(new SelectListItem() { Value = "1", Text = _localizationSvc.GetResource("Admin.Common.Yes") });
            yesNoSelectList.Add(new SelectListItem() { Value = "2", Text = _localizationSvc.GetResource("Admin.Common.No") });

            model.YesNo_SelectList = new SelectList(yesNoSelectList, "Value", "Text");


            return View("../Synchronization/BatchQuestionOpPopup", model);
        }


        public ActionResult BatchQuestionUpdate(BatchQuestionOperationModel model)
        {
            IFormQuestionSvc iFormQuestionSvc = FrameworkCtx.Current.Resolve<IFormQuestionSvc>();

            List<FormQuestion> formQuestions = iFormQuestionSvc.SqlQuery<FormQuestion>(string.Format(HardCodedSQL.FORM_QUESTION_GET_IDS_FOR_BATCH_UPDATE, string.Join<string>(", ", model.FormQuestionList) )).ToList();

            foreach (FormQuestion question in formQuestions)
            {
                if (model.Active != null)
                    question.Active = model.Active.GetValueOrDefault() == 1 ? true : false ;
                if (model.Display_order != null)
                    question.Display_order = model.Display_order.GetValueOrDefault();
                if (model.Period != null)
                    question.Period = model.Period.GetValueOrDefault();

                question.Update_datetime = StringHelper.ConvertDateTimeToString(DateTime.Now);


                try
                {
                    question.Update_user_id = _workContext.CurrentUser.Id.ToString();
                }
                catch (Exception exp)
                {


                }

                iFormQuestionSvc.Update(question);
            }


            return Redirect(Request.UrlReferrer.ToString());
        }






    }
}

