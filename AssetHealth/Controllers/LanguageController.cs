using Ares.Core.Caching;
using Ares.Core.Helpers;
using Ares.UMS.DTO.Localization;
using Ares.UMS.DTO.Media;
using Ares.UMS.DTO.Users;
using Ares.UMS.Svc.Localization;
using Ares.UMS.Svc.Media;
using Ares.UMS.Svc.Users;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Ares.Web.Helpers;

using Ares.UMS.Svc.Configuration;

using Ares.Web.Razor.Extensions.Datatable.Model;
using Ares.Web.Controllers;
using Ares.UMS.Svc;
using AssetHealth.Models.Common;
using AssetHealth.Models.Language;
using AssetHealth.Extensions;

namespace AssetHealth.Controllers
{
    public class LanguageController : BaseAresController
    {
        private readonly IWorkContext _workContext;
        private readonly IPictureSvc _pictureSvc;
        private readonly IUserSvc _userSvc;
        private readonly ILanguageSvc _languageSvc;
        private readonly ILocalizationSvc _localizationSvc;
        private readonly ISettingSvc _settingSvc;
        private readonly IMenuSvc _menuSvc;
        private readonly IWebHelper _webHelper;
        private readonly LocalizationSettings _localizationSettings;
        private readonly MediaSettings _mediaSettings;
        private readonly ICacheManager _cacheManager;
        private readonly ISiteContext _siteContext;

        public LanguageController(IWorkContext workContext, IUserSvc userSvc, IPictureSvc pictureSvc, ILanguageSvc languageSvc, IWebHelper webHelper, 
            LocalizationSettings localizationSettings, MediaSettings mediaSettings, ICacheManager cacheManager, ISiteContext siteContext, IMenuSvc menuSvc, ISettingSvc settingSvc, ILocalizationSvc localizationSvc)
        {
            this._workContext = workContext;
            this._pictureSvc = pictureSvc;
            this._userSvc = userSvc;
            this._languageSvc = languageSvc;
            this._webHelper = webHelper;
            this._localizationSettings = localizationSettings;
            this._mediaSettings = mediaSettings;
            this._cacheManager = cacheManager;
            this._siteContext = siteContext;
            this._menuSvc = menuSvc;
            this._settingSvc = settingSvc;
            this._localizationSvc = localizationSvc;

        }

        public ActionResult Index()
        {
            ImageUploadModel model = new ImageUploadModel();
           

            return View(model);
        }

        public ActionResult LocaleResource()
        {
            LocaleResourceModel model = new LocaleResourceModel();


            return View(model);
        }

        public ActionResult LocaleResourceDatatable(Ares.Web.Razor.Extensions.Datatable.Model.DTParameterModel model)
        {
            var currentLangId = _workContext.WorkingLanguage.Id;
            var models = _localizationSvc.GetAllResources(currentLangId).Where(l => l.Id > 0);

            if (model.Search != null && !string.IsNullOrEmpty(model.Search.Value))
            {
                models = models.Where(m => m.ResourceName.Contains(model.Search.Value));
            }

            DTOrder order = null;
            if (model.Order != null && model.Order.Count() > 0)
            {
                order = model.Order.First();
                var pi = typeof(LocaleResource).GetProperty(model.Columns.ToList()[order.Column].Data);
                if(order.Dir == "asc")
                    models = models.OrderBy(x => pi.GetValue(x, null));
                else
                    models = models.OrderByDescending(x => pi.GetValue(x, null));
                
            }

            var totalRecords = models.Count();
            models = models.OrderBy(e => e.Id).Skip(model.Length * ((int)(Math.Ceiling((double)model.Start / model.Length)) + 1 - 1)).Take(model.Length);

            JsonResult jsonRes = Json(new
            {
                sEcho = model.Draw,
                iTotalRecords = totalRecords,
                iTotalDisplayRecords = totalRecords,
                //aaData = result
                aaData = models.ToList().Select(x => x.ToModel())
            }, JsonRequestBehavior.AllowGet);
            return jsonRes;
        }

        public ActionResult CreateOrUpdateLocaleResource(int id)
        {
            LocaleResourceModel model = _localizationSvc.GetLocaleResourceById(id).ToModel();


            return View(model);
        }

        
        [System.Web.Mvc.HttpPost]
        public ActionResult EditLocaleResource(LocaleResourceModel model)
        {

            var record = _localizationSvc.GetLocaleResourceById(model.Id);


            //record = model.ToEntity(record);
            record.ResourceName = model.ResourceName;
            record.ResourceValue = model.ResourceValue;


            _localizationSvc.UpdateLocaleResource(record);

            SuccessNotification(_localizationSvc.GetResource("Notifications.LocaleResource.Updated"));

            return RedirectToAction("LocaleResource");
        }

        public ActionResult DeleteLocaleResource(int id)
        {
            if (id != -1)
            {
                var record = _localizationSvc.GetLocaleResourceById(id);

                _localizationSvc.DeleteLocaleResource(record);

                SuccessNotification(_localizationSvc.GetResource("Notifications.LocaleResource.Deleted"));
            }
            else
            {

            }


            return RedirectToAction("LocaleResource");
        }






    }
}
