﻿

var FormInputMask = function () {
    
    var handleInputMasks = function () {

        
            $("#Number_of_channels").inputmask("integer");
            $("#Read_order").inputmask("integer");


    }



    return {
        //main function to initiate the module
        init: function () {
            handleInputMasks();
            
        }
    };

}();


var FormValidation = function () {

    // basic validation
    var handleValidation1 = function () {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

        var form1 = $('form');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);


        var validator = form1.validate();



        
        //form1.validate();



        var settngs = $.data($('form')[0], 'validator').settings;
        
        var oldErrorFunction = settngs.errorPlacement;
        var oldSucessFunction = settngs.success;

        var dangerContent = $(error1).find('#dangerContent');
        var submitted;

        settngs.errorPlacement = function (error, inputElement) {
            //var errorDanger = $(inputElement).parent().parent().parent().find('.alert-danger');
            //error1.show();
            /*

            var summary = "You have the following errors: <br/>";
            $.each(validator.errorList, function () { summary += " * " + this.message + "<br/>"; });

            dangerContent.html(summary);
            */
            oldErrorFunction(error, inputElement);

        };
        
    }


    return {
        //main function to initiate the module
        init: function () {


            handleValidation1();


        }

    };

}();

if (App.isAngularJsApp() === false) { 
    jQuery(document).ready(function() {
        FormInputMask.init(); // init metronic core componets
        FormValidation.init();
    });
}