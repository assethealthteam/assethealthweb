var Dashboard = function() {

    return {

        initJQVMAP: function() {
            if (!jQuery().vectorMap) {
                return;
            }

            var showMap = function(name) {
                jQuery('.vmaps').hide();
                jQuery('#vmap_' + name).show();
            }

            var setMap = function(name) {
                var map = jQuery('#vmap_' + name);
                
                if (map.size() !== 1) {
                    return;
                }

                var data = {
                    map: 'world_en',
                    backgroundColor: null,
                    borderColor: '#333333',
                    borderOpacity: 0.5,
                    borderWidth: 1,
                    color: '#c6c6c6',
                    enableZoom: true,
                    hoverColor: '#c9dfaf',
                    hoverOpacity: null,
                    values: sample_data,
                    normalizeFunction: 'linear',
                    scaleColors: ['#b6da93', '#909cae'],
                    selectedColor: '#c9dfaf',
                    selectedRegion: null,
                    showTooltip: true,
                    onLabelShow: function(event, label, code) {

                    },
                    onRegionOver: function(event, code) {
                        if (code == 'ca') {
                            event.preventDefault();
                        }
                    },
                    onRegionClick: function(element, code, region) {
                        var message = 'You clicked "' + region + '" which has the code: ' + code.toUpperCase();
                        alert(message);
                    }
                };

                data.map = name + '_en';
              
                map.width(map.parent().parent().width());
                map.show();
                map.vectorMap(data);
                map.hide();
            }

            setMap("world");
            setMap("usa");
            setMap("europe");
            setMap("russia");
            setMap("germany");
            showMap("world");

            jQuery('#regional_stat_world').click(function() {
                showMap("world");
            });

            jQuery('#regional_stat_usa').click(function() {
                showMap("usa");
            });

            jQuery('#regional_stat_europe').click(function() {
                showMap("europe");
            });
            jQuery('#regional_stat_russia').click(function() {
                showMap("russia");
            });
            jQuery('#regional_stat_germany').click(function() {
                showMap("germany");
            });

            $('#region_statistics_loading').hide();
            $('#region_statistics_content').show();

            App.addResizeHandler(function() {
                jQuery('.vmaps').each(function() {
                    var map = jQuery(this);
                    map.width(map.parent().width());
                });
            });
        },

        initCalendar: function() {
            if (!jQuery().fullCalendar) {
                return;
            }

            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();

            var h = {};

            if ($('#calendar').width() <= 400) {
                $('#calendar').addClass("mobile");
                h = {
                    left: 'title, prev, next',
                    center: '',
                    right: 'today,month,agendaWeek,agendaDay'
                };
            } else {
                $('#calendar').removeClass("mobile");
                if (App.isRTL()) {
                    h = {
                        right: 'title',
                        center: '',
                        left: 'prev,next,today,month,agendaWeek,agendaDay'
                    };
                } else {
                    h = {
                        left: 'title',
                        center: '',
                        right: 'prev,next,today,month,agendaWeek,agendaDay'
                    };
                }
            }

            var events = [{
                title: 'All Day',
                start: new Date(y, m, 1),
                backgroundColor: App.getBrandColor('yellow')
            }, {
                title: 'Long Event',
                start: new Date(y, m, d - 5),
                end: new Date(y, m, d - 2),
                backgroundColor: App.getBrandColor('blue')
            }, {
                title: 'Repeating Event',
                start: new Date(y, m, d - 3, 16, 0),
                allDay: false,
                backgroundColor: App.getBrandColor('red')
            }, {
                title: 'Repeating Event',
                start: new Date(y, m, d + 6, 16, 0),
                allDay: false,
                backgroundColor: App.getBrandColor('green')
            }, {
                title: 'Meeting',
                start: new Date(y, m, d + 9, 10, 30),
                allDay: false
            }, {
                title: 'Lunch',
                start: new Date(y, m, d, 14, 0),
                end: new Date(y, m, d, 14, 0),
                backgroundColor: App.getBrandColor('grey'),
                allDay: false
            }, {
                title: 'Birthday',
                start: new Date(y, m, d + 1, 19, 0),
                end: new Date(y, m, d + 1, 22, 30),
                backgroundColor: App.getBrandColor('purple'),
                allDay: false
            }, {
                title: 'Click for Google',
                start: new Date(y, m, 28),
                end: new Date(y, m, 29),
                backgroundColor: App.getBrandColor('yellow'),
                url: 'http://google.com/'
            }];


            $('#calendar').fullCalendar('destroy'); // destroy the calendar
            $('#calendar').fullCalendar({ //re-initialize the calendar
                disableDragging: false,
                header: h,
                firstDay: 1,
                editable: true,
                events: function (start, end, timezone, callback) {
                    $.ajax({
                        url: '/Home/GetCalendarData',
                        type: "GET",
                        dataType: "JSON",

                        success: function (result) {
                            var events = [];
                            
                            $.each(result, function (i, data) {
                                console.log(moment(data.end, 'YYYYMMDDHHmmss').format('YYYY-MM-DD'));

                                events.push(
                               {
                                   
                                   title: data.Title,
                                   description: data.Desc,
                                   start: moment(data.start,'YYYYMMDDHHmmss').format('YYYY-MM-DD'),
                                   end: moment(data.end, 'YYYYMMDDHHmmss').format('YYYY-MM-DD'),
                                   //backgroundColor: "#9501fc",
                                   backgroundColor: App.getBrandColor(data.backgroundColor),
                                   dow: data.dow,
                                   allDay : data.allDay,
                                   borderColor: "#fc0101"
                               });
                            });

                            callback(events);
                        }
                    });
                },
                eventRender: function (event, element) {
                    element.qtip(
                    {
                        content: event.description,
                        position: {
                            my: 'top center',
                            at: 'bottom left'
                        }
                    });
                },

            });
        },

        initCharts: function() {
            if (!jQuery.plot) {
                return;
            }

            function showChartTooltip(x, y, xValue, yValue) {
                $('<div id="tooltip" class="chart-tooltip">' + yValue + '<\/div>').css({
                    position: 'absolute',
                    display: 'none',
                    top: y - 40,
                    left: x - 40,
                    border: '0px solid #ccc',
                    padding: '2px 6px',
                    'background-color': '#fff'
                }).appendTo("body").fadeIn(200);
            }

            var data = [];
            var totalPoints = 250;

            // random data generator for plot charts

            function getRandomData() {
                if (data.length > 0) data = data.slice(1);
                // do a random walk
                while (data.length < totalPoints) {
                    var prev = data.length > 0 ? data[data.length - 1] : 50;
                    var y = prev + Math.random() * 10 - 5;
                    if (y < 0) y = 0;
                    if (y > 100) y = 100;
                    data.push(y);
                }
                // zip the generated y values with the x values
                var res = [];
                for (var i = 0; i < data.length; ++i) res.push([i, data[i]])
                return res;
            }

            function randValue() {
                return (Math.floor(Math.random() * (1 + 50 - 20))) + 10;
            }

            var visitors = [
                ['02/2013', 1500],
                ['03/2013', 2500],
                ['04/2013', 1700],
                ['05/2013', 800],
                ['06/2013', 1500],
                ['07/2013', 2350],
                ['08/2013', 1500],
                ['09/2013', 1300],
                ['10/2013', 4600]
            ];

            visitors = assetHealthPerformance;


            if ($('#site_statistics').size() != 0) {

                $('#site_statistics_loading').hide();
                $('#site_statistics_content').show();

                var plot_statistics = $.plot($("#site_statistics"), [{
                    data: visitors,
                    lines: {
                        fill: 0.6,
                        lineWidth: 0
                    },
                    color: ['#f89f9f']
                }, {
                    data: visitors,
                    points: {
                        show: true,
                        fill: true,
                        radius: 5,
                        fillColor: "#f89f9f",
                        lineWidth: 3
                    },
                    color: '#fff',
                    shadowSize: 0
                }],

                    {
                        xaxis: {
                            tickLength: 0,
                            tickDecimals: 0,
                            mode: "categories",
                            min: 0,
                            font: {
                                lineHeight: 14,
                                style: "normal",
                                variant: "small-caps",
                                color: "#6F7B8A"
                            }
                        },
                        yaxis: {
                            ticks: 5,
                            tickDecimals: 0,
                            tickColor: "#eee",
                            font: {
                                lineHeight: 14,
                                style: "normal",
                                variant: "small-caps",
                                color: "#6F7B8A"
                            }
                        },
                        grid: {
                            hoverable: true,
                            clickable: true,
                            tickColor: "#eee",
                            borderColor: "#eee",
                            borderWidth: 1
                        }
                    });

                var previousPoint = null;
                $("#site_statistics").bind("plothover", function(event, pos, item) {
                    $("#x").text(pos.x.toFixed(2));
                    $("#y").text(pos.y.toFixed(2));
                    if (item) {
                        if (previousPoint != item.dataIndex) {
                            previousPoint = item.dataIndex;

                            $("#tooltip").remove();
                            var x = item.datapoint[0].toFixed(2),
                                y = item.datapoint[1].toFixed(2);


                            showChartTooltip(item.pageX, item.pageY, item.datapoint[0], visitors[item.datapoint[0]][0] + ' - % ' + item.datapoint[1]);
                        }
                    } else {
                        $("#tooltip").remove();
                        previousPoint = null;
                    }
                });
            }


            if ($('#site_activities').size() != 0) {
                //site activities
                var previousPoint2 = null;
                $('#site_activities_loading').hide();
                $('#site_activities_content').show();

                var data1 = [
                    ['DEC', 300],
                    ['JAN', 600],
                    ['FEB', 1100],
                    ['MAR', 1200],
                    ['APR', 860],
                    ['MAY', 1200],
                    ['JUN', 1450],
                    ['JUL', 1800],
                    ['AUG', 1200],
                    ['SEP', 600]
                ];


                var plot_statistics = $.plot($("#site_activities"),

                    [{
                        data: data1,
                        lines: {
                            fill: 0.2,
                            lineWidth: 0,
                        },
                        color: ['#BAD9F5']
                    }, {
                        data: data1,
                        points: {
                            show: true,
                            fill: true,
                            radius: 4,
                            fillColor: "#9ACAE6",
                            lineWidth: 2
                        },
                        color: '#9ACAE6',
                        shadowSize: 1
                    }, {
                        data: data1,
                        lines: {
                            show: true,
                            fill: false,
                            lineWidth: 3
                        },
                        color: '#9ACAE6',
                        shadowSize: 0
                    }],

                    {

                        xaxis: {
                            tickLength: 0,
                            tickDecimals: 0,
                            mode: "categories",
                            min: 0,
                            font: {
                                lineHeight: 18,
                                style: "normal",
                                variant: "small-caps",
                                color: "#6F7B8A"
                            }
                        },
                        yaxis: {
                            ticks: 5,
                            tickDecimals: 0,
                            tickColor: "#eee",
                            font: {
                                lineHeight: 14,
                                style: "normal",
                                variant: "small-caps",
                                color: "#6F7B8A"
                            }
                        },
                        grid: {
                            hoverable: true,
                            clickable: true,
                            tickColor: "#eee",
                            borderColor: "#eee",
                            borderWidth: 1
                        }
                    });

                $("#site_activities").bind("plothover", function(event, pos, item) {
                    $("#x").text(pos.x.toFixed(2));
                    $("#y").text(pos.y.toFixed(2));
                    if (item) {
                        if (previousPoint2 != item.dataIndex) {
                            previousPoint2 = item.dataIndex;
                            $("#tooltip").remove();
                            var x = item.datapoint[0].toFixed(2),
                                y = item.datapoint[1].toFixed(2);
                            showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1] + 'M$');
                        }
                    }
                });

                $('#site_activities').bind("mouseleave", function() {
                    $("#tooltip").remove();
                });
            }
        },

        initEasyPieCharts: function() {
            if (!jQuery().easyPieChart) {
                return;
            }

            $('.easy-pie-chart .number.transactions').easyPieChart({
                animate: 1000,
                size: 75,
                lineWidth: 3,
                barColor: App.getBrandColor('yellow')
            });

            $('.easy-pie-chart .number.visits').easyPieChart({
                animate: 1000,
                size: 75,
                lineWidth: 3,
                barColor: App.getBrandColor('green')
            });

            $('.easy-pie-chart .number.bounce').easyPieChart({
                animate: 1000,
                size: 75,
                lineWidth: 3,
                barColor: App.getBrandColor('red')
            });

            $('.easy-pie-chart-reload').click(function() {
                $('.easy-pie-chart .number').each(function() {
                    var newValue = Math.floor(100 * Math.random());
                    $(this).data('easyPieChart').update(newValue);
                    $('span', this).text(newValue);
                });
            });
        },

        initSparklineCharts: function() {
            if (!jQuery().sparkline) {
                return;
            }
            $("#sparkline_bar").sparkline([8, 9, 10, 11, 10, 10, 12, 10, 10, 11, 9, 12, 11, 10, 9, 11, 13, 13, 12], {
                type: 'bar',
                width: '100',
                barWidth: 5,
                height: '55',
                barColor: '#f36a5b',
                negBarColor: '#e02222'
            });

            $("#sparkline_bar2").sparkline([9, 11, 12, 13, 12, 13, 10, 14, 13, 11, 11, 12, 11, 11, 10, 12, 11, 10], {
                type: 'bar',
                width: '100',
                barWidth: 5,
                height: '55',
                barColor: '#5c9bd1',
                negBarColor: '#e02222'
            });

            $("#sparkline_bar5").sparkline([8, 9, 10, 11, 10, 10, 12, 10, 10, 11, 9, 12, 11, 10, 9, 11, 13, 13, 12], {
                type: 'bar',
                width: '100',
                barWidth: 5,
                height: '55',
                barColor: '#35aa47',
                negBarColor: '#e02222'
            });

            $("#sparkline_bar6").sparkline([9, 11, 12, 13, 12, 13, 10, 14, 13, 11, 11, 12, 11, 11, 10, 12, 11, 10], {
                type: 'bar',
                width: '100',
                barWidth: 5,
                height: '55',
                barColor: '#ffb848',
                negBarColor: '#e02222'
            });

            $("#sparkline_line").sparkline([9, 10, 9, 10, 10, 11, 12, 10, 10, 11, 11, 12, 11, 10, 12, 11, 10, 12], {
                type: 'line',
                width: '100',
                height: '55',
                lineColor: '#ffb848'
            });
        },

        initMorisCharts: function() {
            if (Morris.EventEmitter && $('#sales_statistics').size() > 0) {
                // Use Morris.Area instead of Morris.Line
                dashboardMainChart = Morris.Area({
                    element: 'sales_statistics',
                    padding: 0,
                    behaveLikeLine: false,
                    gridEnabled: false,
                    gridLineColor: false,
                    axes: false,
                    fillOpacity: 1,
                    data: [{
                        period: '2011 Q1',
                        sales: 1400,
                        profit: 400
                    }, {
                        period: '2011 Q2',
                        sales: 1100,
                        profit: 600
                    }, {
                        period: '2011 Q3',
                        sales: 1600,
                        profit: 500
                    }, {
                        period: '2011 Q4',
                        sales: 1200,
                        profit: 400
                    }, {
                        period: '2012 Q1',
                        sales: 1550,
                        profit: 800
                    }],
                    lineColors: ['#399a8c', '#92e9dc'],
                    xkey: 'period',
                    ykeys: ['sales', 'profit'],
                    labels: ['Sales', 'Profit'],
                    pointSize: 0,
                    lineWidth: 0,
                    hideHover: 'auto',
                    resize: true
                });

            }
        },

        initChat: function() {
            var cont = $('#chats');
            var list = $('.chats', cont);
            var form = $('.chat-form', cont);
            var input = $('input', form);
            var btn = $('.btn', form);

            var handleClick = function(e) {
                e.preventDefault();

                var text = input.val();
                if (text.length == 0) {
                    return;
                }

                var time = new Date();
                var time_str = (time.getHours() + ':' + time.getMinutes());
                var tpl = '';
                tpl += '<li class="out">';
                tpl += '<img class="avatar" alt="" src="' + Layout.getLayoutImgPath() + 'avatar1.jpg"/>';
                tpl += '<div class="message">';
                tpl += '<span class="arrow"></span>';
                tpl += '<a href="#" class="name">Bob Nilson</a>&nbsp;';
                tpl += '<span class="datetime">at ' + time_str + '</span>';
                tpl += '<span class="body">';
                tpl += text;
                tpl += '</span>';
                tpl += '</div>';
                tpl += '</li>';

                var msg = list.append(tpl);
                input.val("");

                var getLastPostPos = function() {
                    var height = 0;
                    cont.find("li.out, li.in").each(function() {
                        height = height + $(this).outerHeight();
                    });

                    return height;
                }

                cont.find('.scroller').slimScroll({
                    scrollTo: getLastPostPos()
                });
            }

            $('body').on('click', '.message .name', function(e) {
                e.preventDefault(); // prevent click event

                var name = $(this).text(); // get clicked user's full name
                input.val('@' + name + ':'); // set it into the input field
                App.scrollTo(input); // scroll to input if needed
            });

            btn.click(handleClick);

            input.keypress(function(e) {
                if (e.which == 13) {
                    handleClick(e);
                    return false; //<---- Add this line
                }
            });
        },

        initDashboardDaterange: function(id) {
            if (!jQuery().daterangepicker) {
                return;
            }
            var today = _("%Today");
            //console.log(today);
            var yesterday = _("%Yesterday");
            var last7Days = _('%Last_7_Days');
            var last30Days = _('%Last_30_Days');
            var This_Month = _('%This_Month');
            var Last_Month = _('%Last_Month');
            //var last30Days = TAPi18n.('last30Days');
            

            $('#' + id).daterangepicker({
                "ranges": {
                    [today] : [moment(), moment()],
                    [yesterday]: [moment().subtract('days', 1), moment().subtract('days', 1)],
                    [last7Days] : [moment().subtract('days', 6), moment()],
                    [last30Days]: [moment().subtract('days', 29), moment()],
                    [This_Month]: [moment().startOf('month'), moment().endOf('month')],
                    [Last_Month]: [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                },
                "locale": {
                    "format": "DD-MM-YYYY",
                    "applyLabel": _("%Apply"),
                    "cancelLabel": _("%Cancel"),
                    "customRangeLabel": _("%CustomRange"),
                },
                /*
                "locale": {
                    //"format": "MM/DD/YYYY",
                    //"format": "DD-MM-YYYY",
                    "separator": " - ",
                    "applyLabel": "Apply",
                    "cancelLabel": "Cancel",
                    "fromLabel": "From",
                    "toLabel": "To",
                    "customRangeLabel": "Custom",
                    /*
                    "daysOfWeek": [
                        "Su",
                        "Mo",
                        "Tu",
                        "We",
                        "Th",
                        "Fr",
                        "Sa"
                    ],
                    
                    "monthNames": [
                        "January",
                        "February",
                        "March",
                        "April",
                        "May",
                        "June",
                        "July",
                        "August",
                        "September",
                        "October",
                        "November",
                        "December"
                    ],
                    "firstDay": 1
                },*/
                //"startDate": "11/08/2015",
                //"endDate": "11/14/2015",
                opens: (App.isRTL() ? 'right' : 'left'),
            }, function (start, end, label) { // After an item is selected from dropdown (not for apply button)
                console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));

                if ($('#' + id).attr('data-display-range') != '0') {
                    $('#' + id  + ' span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }
            });
            if ($('#' + id).attr('data-display-range') != '0') {
                $('#' + id + ' span').html(moment().subtract('days', 29).format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
            }
            $('#' + id).show();


        },
        
        initAmChart1: function() {
            if (typeof(AmCharts) === 'undefined' || $('#dashboard_amchart_1').size() === 0) {
                return;
            }

            var chartData = [{
                "date": "2012-01-05",
                "distance": 480,
                "townName": "Miami",
                "townName2": "Miami",
                "townSize": 10,
                "latitude": 25.83,
                "duration": 501
            }, {
                "date": "2012-01-06",
                "distance": 386,
                "townName": "Tallahassee",
                "townSize": 7,
                "latitude": 30.46,
                "duration": 443
            }, {
                "date": "2012-01-07",
                "distance": 348,
                "townName": "New Orleans",
                "townSize": 10,
                "latitude": 29.94,
                "duration": 405
            }, {
                "date": "2012-01-08",
                "distance": 238,
                "townName": "Houston",
                "townName2": "Houston",
                "townSize": 16,
                "latitude": 29.76,
                "duration": 309
            }, {
                "date": "2012-01-09",
                "distance": 218,
                "townName": "Dalas",
                "townSize": 17,
                "latitude": 32.8,
                "duration": 287
            }, {
                "date": "2012-01-10",
                "distance": 349,
                "townName": "Oklahoma City",
                "townSize": 11,
                "latitude": 35.49,
                "duration": 485
            }, {
                "date": "2012-01-11",
                "distance": 603,
                "townName": "Kansas City",
                "townSize": 10,
                "latitude": 39.1,
                "duration": 890
            }, {
                "date": "2012-01-12",
                "distance": 534,
                "townName": "Denver",
                "townName2": "Denver",
                "townSize": 18,
                "latitude": 39.74,
                "duration": 810
            }, {
                "date": "2012-01-13",
                "townName": "Salt Lake City",
                "townSize": 12,
                "distance": 425,
                "duration": 670,
                "latitude": 40.75,
                "alpha": 0.4
            }, {
                "date": "2012-01-14",
                "latitude": 36.1,
                "duration": 470,
                "townName": "Las Vegas",
                "townName2": "Las Vegas",
                "bulletClass": "lastBullet"
            }, {
                "date": "2012-01-15"
            }];
            var chart = AmCharts.makeChart("dashboard_amchart_1", {
                type: "serial",
                fontSize: 12,
                fontFamily: "Open Sans",
                dataDateFormat: "YYYY-MM-DD",
                dataProvider: chartData,

                addClassNames: true,
                startDuration: 1,
                color: "#6c7b88",
                marginLeft: 0,

                categoryField: "date",
                categoryAxis: {
                    parseDates: true,
                    minPeriod: "DD",
                    autoGridCount: false,
                    gridCount: 50,
                    gridAlpha: 0.1,
                    gridColor: "#FFFFFF",
                    axisColor: "#555555",
                    dateFormats: [{
                        period: 'DD',
                        format: 'DD'
                    }, {
                        period: 'WW',
                        format: 'MMM DD'
                    }, {
                        period: 'MM',
                        format: 'MMM'
                    }, {
                        period: 'YYYY',
                        format: 'YYYY'
                    }]
                },

                valueAxes: [{
                    id: "a1",
                    title: "distance",
                    gridAlpha: 0,
                    axisAlpha: 0
                }, {
                    id: "a2",
                    position: "right",
                    gridAlpha: 0,
                    axisAlpha: 0,
                    labelsEnabled: false
                }, {
                    id: "a3",
                    title: "duration",
                    position: "right",
                    gridAlpha: 0,
                    axisAlpha: 0,
                    inside: true,
                    duration: "mm",
                    durationUnits: {
                        DD: "d. ",
                        hh: "h ",
                        mm: "min",
                        ss: ""
                    }
                }],
                graphs: [{
                    id: "g1",
                    valueField: "distance",
                    title: "distance",
                    type: "column",
                    fillAlphas: 0.7,
                    valueAxis: "a1",
                    balloonText: "[[value]] miles",
                    legendValueText: "[[value]] mi",
                    legendPeriodValueText: "total: [[value.sum]] mi",
                    lineColor: "#08a3cc",
                    alphaField: "alpha",
                }, {
                    id: "g2",
                    valueField: "latitude",
                    classNameField: "bulletClass",
                    title: "latitude/city",
                    type: "line",
                    valueAxis: "a2",
                    lineColor: "#786c56",
                    lineThickness: 1,
                    legendValueText: "[[description]]/[[value]]",
                    descriptionField: "townName",
                    bullet: "round",
                    bulletSizeField: "townSize",
                    bulletBorderColor: "#02617a",
                    bulletBorderAlpha: 1,
                    bulletBorderThickness: 2,
                    bulletColor: "#89c4f4",
                    labelText: "[[townName2]]",
                    labelPosition: "right",
                    balloonText: "latitude:[[value]]",
                    showBalloon: true,
                    animationPlayed: true,
                }, {
                    id: "g3",
                    title: "duration",
                    valueField: "duration",
                    type: "line",
                    valueAxis: "a3",
                    lineAlpha: 0.8,
                    lineColor: "#e26a6a",
                    balloonText: "[[value]]",
                    lineThickness: 1,
                    legendValueText: "[[value]]",
                    bullet: "square",
                    bulletBorderColor: "#e26a6a",
                    bulletBorderThickness: 1,
                    bulletBorderAlpha: 0.8,
                    dashLengthField: "dashLength",
                    animationPlayed: true
                }],

                chartCursor: {
                    zoomable: false,
                    categoryBalloonDateFormat: "DD",
                    cursorAlpha: 0,
                    categoryBalloonColor: "#e26a6a",
                    categoryBalloonAlpha: 0.8,
                    valueBalloonsEnabled: false
                },
                legend: {
                    bulletType: "round",
                    equalWidths: false,
                    valueWidth: 120,
                    useGraphSettings: true,
                    color: "#6c7b88"
                }
            });
        },

        initAmChart2: function() {
            if (typeof(AmCharts) === 'undefined' || $('#dashboard_amchart_2').size() === 0) {
                return;
            }

            // svg path for target icon
            var targetSVG = "M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z";
            // svg path for plane icon
            var planeSVG = "M19.671,8.11l-2.777,2.777l-3.837-0.861c0.362-0.505,0.916-1.683,0.464-2.135c-0.518-0.517-1.979,0.278-2.305,0.604l-0.913,0.913L7.614,8.804l-2.021,2.021l2.232,1.061l-0.082,0.082l1.701,1.701l0.688-0.687l3.164,1.504L9.571,18.21H6.413l-1.137,1.138l3.6,0.948l1.83,1.83l0.947,3.598l1.137-1.137V21.43l3.725-3.725l1.504,3.164l-0.687,0.687l1.702,1.701l0.081-0.081l1.062,2.231l2.02-2.02l-0.604-2.689l0.912-0.912c0.326-0.326,1.121-1.789,0.604-2.306c-0.452-0.452-1.63,0.101-2.135,0.464l-0.861-3.838l2.777-2.777c0.947-0.947,3.599-4.862,2.62-5.839C24.533,4.512,20.618,7.163,19.671,8.11z";

            var map = AmCharts.makeChart("dashboard_amchart_2", {
                type: "map",
                "theme": "light",
                pathToImages: "../assets/global/plugins/amcharts/ammap/images/",

                dataProvider: {
                    map: "worldLow",
                    linkToObject: "london",
                    images: [{
                        id: "london",
                        color: "#009dc7",
                        svgPath: targetSVG,
                        title: "London",
                        latitude: 51.5002,
                        longitude: -0.1262,
                        scale: 1.5,
                        zoomLevel: 2.74,
                        zoomLongitude: -20.1341,
                        zoomLatitude: 49.1712,

                        lines: [{
                            latitudes: [51.5002, 50.4422],
                            longitudes: [-0.1262, 30.5367]
                        }, {
                            latitudes: [51.5002, 46.9480],
                            longitudes: [-0.1262, 7.4481]
                        }, {
                            latitudes: [51.5002, 59.3328],
                            longitudes: [-0.1262, 18.0645]
                        }, {
                            latitudes: [51.5002, 40.4167],
                            longitudes: [-0.1262, -3.7033]
                        }, {
                            latitudes: [51.5002, 46.0514],
                            longitudes: [-0.1262, 14.5060]
                        }, {
                            latitudes: [51.5002, 48.2116],
                            longitudes: [-0.1262, 17.1547]
                        }, {
                            latitudes: [51.5002, 44.8048],
                            longitudes: [-0.1262, 20.4781]
                        }, {
                            latitudes: [51.5002, 55.7558],
                            longitudes: [-0.1262, 37.6176]
                        }, {
                            latitudes: [51.5002, 38.7072],
                            longitudes: [-0.1262, -9.1355]
                        }, {
                            latitudes: [51.5002, 54.6896],
                            longitudes: [-0.1262, 25.2799]
                        }, {
                            latitudes: [51.5002, 64.1353],
                            longitudes: [-0.1262, -21.8952]
                        }, {
                            latitudes: [51.5002, 40.4300],
                            longitudes: [-0.1262, -74.0000]
                        }],

                        images: [{
                            label: "Flights from London",
                            svgPath: planeSVG,
                            left: 100,
                            top: 45,
                            labelShiftY: 5,
                            color: "#d93d5e",
                            labelColor: "#d93d5e",
                            labelRollOverColor: "#d93d5e",
                            labelFontSize: 20
                        }, {
                            label: "show flights from Vilnius",
                            left: 106,
                            top: 70,
                            labelColor: "#6c7b88",
                            labelRollOverColor: "#d93d5e",
                            labelFontSize: 11,
                            linkToObject: "vilnius"
                        }]
                    },

                        {
                            id: "vilnius",
                            color: "#009dc7",
                            svgPath: targetSVG,
                            title: "Vilnius",
                            latitude: 54.6896,
                            longitude: 25.2799,
                            scale: 1.5,
                            zoomLevel: 4.92,
                            zoomLongitude: 15.4492,
                            zoomLatitude: 50.2631,

                            lines: [{
                                latitudes: [54.6896, 50.8371],
                                longitudes: [25.2799, 4.3676]
                            }, {
                                latitudes: [54.6896, 59.9138],
                                longitudes: [25.2799, 10.7387]
                            }, {
                                latitudes: [54.6896, 40.4167],
                                longitudes: [25.2799, -3.7033]
                            }, {
                                latitudes: [54.6896, 50.0878],
                                longitudes: [25.2799, 14.4205]
                            }, {
                                latitudes: [54.6896, 48.2116],
                                longitudes: [25.2799, 17.1547]
                            }, {
                                latitudes: [54.6896, 44.8048],
                                longitudes: [25.2799, 20.4781]
                            }, {
                                latitudes: [54.6896, 55.7558],
                                longitudes: [25.2799, 37.6176]
                            }, {
                                latitudes: [54.6896, 37.9792],
                                longitudes: [25.2799, 23.7166]
                            }, {
                                latitudes: [54.6896, 54.6896],
                                longitudes: [25.2799, 25.2799]
                            }, {
                                latitudes: [54.6896, 51.5002],
                                longitudes: [25.2799, -0.1262]
                            }, {
                                latitudes: [54.6896, 53.3441],
                                longitudes: [25.2799, -6.2675]
                            }],

                            images: [{
                                label: "Flights from Vilnius",
                                svgPath: planeSVG,
                                left: 100,
                                top: 45,
                                labelShiftY: 5,
                                color: "#d93d5e",
                                labelColor: "#d93d5e",
                                labelRollOverColor: "#d93d5e",
                                labelFontSize: 20
                            }, {
                                label: "show flights from London",
                                left: 106,
                                top: 70,
                                labelColor: "#009dc7",
                                labelRollOverColor: "#d93d5e",
                                labelFontSize: 11,
                                linkToObject: "london"
                            }]
                        }, {
                            svgPath: targetSVG,
                            title: "Brussels",
                            latitude: 50.8371,
                            longitude: 4.3676
                        }, {
                            svgPath: targetSVG,
                            title: "Prague",
                            latitude: 50.0878,
                            longitude: 14.4205
                        }, {
                            svgPath: targetSVG,
                            title: "Athens",
                            latitude: 37.9792,
                            longitude: 23.7166
                        }, {
                            svgPath: targetSVG,
                            title: "Reykjavik",
                            latitude: 64.1353,
                            longitude: -21.8952
                        }, {
                            svgPath: targetSVG,
                            title: "Dublin",
                            latitude: 53.3441,
                            longitude: -6.2675
                        }, {
                            svgPath: targetSVG,
                            title: "Oslo",
                            latitude: 59.9138,
                            longitude: 10.7387
                        }, {
                            svgPath: targetSVG,
                            title: "Lisbon",
                            latitude: 38.7072,
                            longitude: -9.1355
                        }, {
                            svgPath: targetSVG,
                            title: "Moscow",
                            latitude: 55.7558,
                            longitude: 37.6176
                        }, {
                            svgPath: targetSVG,
                            title: "Belgrade",
                            latitude: 44.8048,
                            longitude: 20.4781
                        }, {
                            svgPath: targetSVG,
                            title: "Bratislava",
                            latitude: 48.2116,
                            longitude: 17.1547
                        }, {
                            svgPath: targetSVG,
                            title: "Ljubljana",
                            latitude: 46.0514,
                            longitude: 14.5060
                        }, {
                            svgPath: targetSVG,
                            title: "Madrid",
                            latitude: 40.4167,
                            longitude: -3.7033
                        }, {
                            svgPath: targetSVG,
                            title: "Stockholm",
                            latitude: 59.3328,
                            longitude: 18.0645
                        }, {
                            svgPath: targetSVG,
                            title: "Bern",
                            latitude: 46.9480,
                            longitude: 7.4481
                        }, {
                            svgPath: targetSVG,
                            title: "Kiev",
                            latitude: 50.4422,
                            longitude: 30.5367
                        }, {
                            svgPath: targetSVG,
                            title: "Paris",
                            latitude: 48.8567,
                            longitude: 2.3510
                        }, {
                            svgPath: targetSVG,
                            title: "New York",
                            latitude: 40.43,
                            longitude: -74
                        }
                    ]
                },

                zoomControl: {
                    buttonFillColor: "#dddddd"
                },

                areasSettings: {
                    unlistedAreasColor: "#15A892"
                },

                imagesSettings: {
                    color: "#d93d5e",
                    rollOverColor: "#d93d5e",
                    selectedColor: "#009dc7"
                },

                linesSettings: {
                    color: "#d93d5e",
                    alpha: 0.4
                },


                backgroundZoomsToTop: true,
                linesAboveImages: true,

                "export": {
                    "enabled": true,
                    "libs": {
                        "path": "http://www.amcharts.com/lib/3/plugins/export/libs/"
                    }
                }
            });
        },

        initAmChart3: function() {
            if (typeof(AmCharts) === 'undefined' || $('#dashboard_amchart_3').size() === 0) {
                return;
            }

            var chart = AmCharts.makeChart("dashboard_amchart_3", {
                "type": "serial",
                "addClassNames": true,
                "theme": "light",
                "path": "../assets/global/plugins/amcharts/ammap/images/",
                "autoMargins": false,
                "marginLeft": 30,
                "marginRight": 8,
                "marginTop": 10,
                "marginBottom": 26,
                "balloon": {
                    "adjustBorderColor": false,
                    "horizontalPadding": 10,
                    "verticalPadding": 8,
                    "color": "#ffffff"
                },

                "dataProvider": [{
                    "year": 2009,
                    "income": 23.5,
                    "expenses": 21.1
                }, {
                    "year": 2010,
                    "income": 26.2,
                    "expenses": 30.5
                }, {
                    "year": 2011,
                    "income": 30.1,
                    "expenses": 34.9
                }, {
                    "year": 2012,
                    "income": 29.5,
                    "expenses": 31.1
                }, {
                    "year": 2013,
                    "income": 30.6,
                    "expenses": 28.2,
                }, {
                    "year": 2014,
                    "income": 34.1,
                    "expenses": 32.9,
                    "dashLengthColumn": 5,
                    "alpha": 0.2,
                    "additional": "(projection)"
                }],
                "valueAxes": [{
                    "axisAlpha": 0,
                    "position": "left"
                }],
                "startDuration": 1,
                "graphs": [{
                    "alphaField": "alpha",
                    "balloonText": "<span style='font-size:12px;'>[[title]] in [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
                    "fillAlphas": 1,
                    "title": "Income",
                    "type": "column",
                    "valueField": "income",
                    "dashLengthField": "dashLengthColumn"
                }, {
                    "id": "graph2",
                    "balloonText": "<span style='font-size:12px;'>[[title]] in [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
                    "bullet": "round",
                    "lineThickness": 3,
                    "bulletSize": 7,
                    "bulletBorderAlpha": 1,
                    "bulletColor": "#FFFFFF",
                    "useLineColorForBulletBorder": true,
                    "bulletBorderThickness": 3,
                    "fillAlphas": 0,
                    "lineAlpha": 1,
                    "title": "Expenses",
                    "valueField": "expenses"
                }],
                "categoryField": "year",
                "categoryAxis": {
                    "gridPosition": "start",
                    "axisAlpha": 0,
                    "tickLength": 0
                },
                "export": {
                    "enabled": true
                }
            });
        },

        initAmChart4: function() {
            if (typeof(AmCharts) === 'undefined' || $('#dashboard_amchart_4').size() === 0) {
                return;
            }

            var chart = AmCharts.makeChart("dashboard_amchart_4", {
                "type": "pie",
                "theme": "light",
                "path": "../assets/global/plugins/amcharts/ammap/images/",
                "dataProvider": [{
                    "country": "Lithuania",
                    "value": 260
                }, {
                    "country": "Ireland",
                    "value": 201
                }, {
                    "country": "Germany",
                    "value": 65
                }, {
                    "country": "Australia",
                    "value": 39
                }, {
                    "country": "UK",
                    "value": 19
                }, {
                    "country": "Latvia",
                    "value": 10
                }],
                "valueField": "value",
                "titleField": "country",
                "outlineAlpha": 0.4,
                "depth3D": 15,
                "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
                "angle": 30,
                "export": {
                    "enabled": true
                }
            });
            jQuery('.chart-input').off().on('input change', function() {
                var property = jQuery(this).data('property');
                var target = chart;
                var value = Number(this.value);
                chart.startDuration = 0;

                if (property == 'innerRadius') {
                    value += "%";
                }

                target[property] = value;
                chart.validateNow();
            });
        },
        
        initAmChartYesNo: function() {
            if (typeof (AmCharts) === 'undefined' || $('#yesno_statistics').size() === 0) {
                return;
            }

            var data = [{
                "Hafta": "18",
                "Hay�r": -5,
                "Evet": 95
            }, {
                "Hafta": "17",
                "Hay�r": -10,
                "Evet": 90
            }, {
                "Hafta": "16",
                "Hay�r": -10,
                "Evet": 90
            }, {
                "Hafta": "15",
                "Hay�r": -10,
                "Evet": 90
            }, {
                "Hafta": "14",
                "Hay�r": -15,
                "Evet": 85
            }, {
                "Hafta": "13",
                "Hay�r": -5,
                "Evet": 95
            }, {
                "Hafta": "12",
                "Hay�r": -10,
                "Evet": 90
            }, {
                "Hafta": "11",
                "Hay�r": 0,
                "Evet": 100
            }, {
                "Hafta": "10",
                "Hay�r": -5,
                "Evet": 95
            }, {
                "Hafta": "9",
                "Hay�r": -90,
                "Evet": 10
            }, {
                "Hafta": "8",
                "Hay�r": -60,
                "Evet": 40
            }, {
                "Hafta": "7",
                "Hay�r": -38,
                "Evet": 62
            }, {
                "Hafta": "6",
                "Hay�r": -5,
                "Evet": 95
            }, {
                "Hafta": "5",
                "Hay�r": 0,
                "Evet": 100
            }, {
                "Hafta": "4",
                "Hay�r": -30,
                "Evet": 70
            }, {
                "Hafta": "3",
                "Hay�r": -12,
                "Evet": 88
            }, {
                "Hafta": "2",
                "Hay�r": -10,
                "Evet": 90
            }, {
                "Hafta": "1",
                "Hay�r": -15,
                "Evet": 85
            }];

            data = EquipmentHealthPerformance;

            var chart = AmCharts.makeChart("yesno_statistics", {
                "type": "serial",
                //"path": "/Content/assets/global/plugins/amcharts/",
                //"pathToImages": "https://www.amcharts.com/lib/images/",
                "theme": "light",
                "rotate": true,
                "marginBottom": 50,
                "dataProvider": data,
                "startDuration": 1,
                "graphs": [ {
                    "fillAlphas": 0.8,
                    "lineAlpha": 0.2,
                    "fillColors": "#ff0000", // bar color
                    "type": "column",
                    "valueField": "UNHEALTHY",
                    "title": "Hay�r",
                    "color": "#000000",  // text color on the row
                    "labelText": "[[value]]",
                    "clustered": false,
                    "labelFunction": function (item) {
                        return Math.abs(item.values.value);
                    },
                    "balloonFunction": function (item) {
                        return item.category + ": " + Math.abs(item.values.value) + " ";
                    }
                },{
                    "fillAlphas": 0.8,
                    "lineAlpha": 0.2,
                    "fillColors": "#008800", // bar color
                    "type": "column",
                    "valueField": "HEALTHY",
                    "title": "Evet",
                    "labelText": "[[value]]",
                    "clustered": false,
                    "labelFunction": function (item) {
                        return Math.abs(item.values.value);
                    },
                    "balloonFunction": function (item) {
                        return item.category + ": " + Math.abs(item.values.value) + " ";
                    }
                }],
                "categoryField": "EQUIPMENT_NAME",
                "categoryAxis": {
                    "gridPosition": "start",
                    "gridAlpha": 0.2,
                    "axisAlpha": 0
                },
                "valueAxes": [{
                    "gridAlpha": 0,
                    "ignoreAxisWidth": true,
                    "labelFunction": function (value) {
                        return Math.abs(value) + ' ';
                    },
                    "guides": [{
                        "value": 0,
                        "lineAlpha": 0.2
                    }]
                }],
                "balloon": {
                    "fixedPosition": true
                },
                "chartCursor": {
                    "valueBalloonsEnabled": false,
                    "cursorAlpha": 0.05,
                    "fullWidth": true
                },
                "allLabels": [ {
                    "text": "Anormal",
                    "x": "25%",
                    "y": "97%",
                    "bold": true,
                    "align": "middle"
                },{
                    "text": "Normal",
                    "x": "75%",
                    "y": "97%",
                    "bold": true,
                    "align": "middle",
                    
                }],
                svgIcons : false,
                "export": {
                    "enabled": true,
                    "fileName": "assethealth-graph",
                    "libs": { "path": "/Content/assets/global/plugins/amcharts/amcharts/plugins/export/libs/" }
                },

            });

            // add click listener
            chart.addListener("init", function () {
                for (var x in chart.panels) {
                    chart.panels[x].addListener("clickGraphItem", function (e) {
                        alert("index: " + e.item.index + "\nvalue: " + e.item.values["value"]);
                    });
                }
            });
        },

        initWorldMapStats: function() {
            if ($('#mapplic').size() === 0) {
                return;
            }

            $('#mapplic').mapplic({
                source: '../assets/global/plugins/mapplic/world.json',
                height: 265,
                animate: false,
                sidebar: false,
                minimap: false,
                locations: true,
                deeplinking: true,
                fullscreen: false,
                hovertip: true,
                zoombuttons: false,
                clearbutton: false,
                developer: false,
                maxscale: 2,
                skin: 'mapplic-dark',
                zoom: true
            });

            $("#widget_sparkline_bar").sparkline([8, 7, 9, 8.5, 8, 8.2, 8, 8.5, 9, 8, 9], {
                type: 'bar',
                width: '100',
                barWidth: 5,
                height: '30',
                barColor: '#4db3a4',
                negBarColor: '#e02222'
            });

            $("#widget_sparkline_bar2").sparkline([8, 7, 9, 8.5, 8, 8.2, 8, 8.5, 9, 8, 9], {
                type: 'bar',
                width: '100',
                barWidth: 5,
                height: '30',
                barColor: '#f36a5a',
                negBarColor: '#e02222'
            });

            $("#widget_sparkline_bar3").sparkline([8, 7, 9, 8.5, 8, 8.2, 8, 8.5, 9, 8, 9], {
                type: 'bar',
                width: '100',
                barWidth: 5,
                height: '30',
                barColor: '#5b9bd1',
                negBarColor: '#e02222'
            });

            $("#widget_sparkline_bar4").sparkline([8, 7, 9, 8.5, 8, 8.2, 8, 8.5, 9, 8, 9], {
                type: 'bar',
                width: '100',
                barWidth: 5,
                height: '30',
                barColor: '#9a7caf',
                negBarColor: '#e02222'
            });
        },

        initTable2 : function () {
            
            if (!jQuery().dataTable) {
                return;
            }
            /*
            console.log('CurrentWarnings');
            console.log(CurrentWarnings);
            */
            var currentAlarmsAndWarnings = {};
            if (typeof CurrentWarnings !== 'undefined')
                currentAlarmsAndWarnings = CurrentWarnings
            
            var table = $('#warning_datatable');
            table.DataTable().destroy();

            table.DataTable({

                "autoWidth": true,
                data: currentAlarmsAndWarnings,
                "initComplete": function (settings, json) {
                    //$.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
                },
                "buttons" :  DataTablePrintButtonAttribute ,
                // Internationalisation. For more info refer to http://datatables.net/manual/i18n
                "language": {
                    "aria": {
                        "sortAscending": ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    },
                    "emptyTable": "No data available in table",
                    "info": "Showing _START_ to _END_ of _TOTAL_ records",
                    "infoEmpty": "No records found",
                    "infoFiltered": "(filtered1 from _MAX_ total records)",
                    "lengthMenu": "Show _MENU_",
                    "search": "Search:",
                    "zeroRecords": "No matching records found",
                    "paginate": {
                        "previous":"Prev",
                        "next": "Next",
                        "last": "Last",
                        "first": "First"
                    }
                },

                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
                // So when dropdowns used the scrollable div should be removed. 
                //"dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
                "pagingType": "bootstrap_extended",

                "lengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"] // change per page values here
                ],
                // set the initial value
                "pageLength": 5,
                "ScrollX": "100%",
                /*
                "columnDefs": [{  // set default column settings
                    'orderable': false,
                    'targets': [0],
                    "width": "40%"
                }, {
                    "searchable": false,
                    "targets": [0]
                }],
                "order": [
                    [1, "asc"]
                ] // set first column as a default sort by asc
                */
            });

            /*
            var tableWrapper = jQuery('#question_datatable_wrapper');

            table.find('.group-checkable').change(function () {
                var set = jQuery(this).attr("data-set");
                var checked = jQuery(this).is(":checked");
                jQuery(set).each(function () {
                    if (checked) {
                        $(this).prop("checked", true);
                    } else {
                        $(this).prop("checked", false);
                    }
                });
            });
            */
        },


        initAmChartPieChart: function() {
            if (typeof (AmCharts) === 'undefined' || $('#piechart_statistics').size() === 0) {
                return;
            }
            var data = [{
                "country": "DAP",
                "value": 260
            }, {
                "country": "NPK",
                "value": 201
            }, {
                "country": "Yardimci Isletmeler",
                "value": 65
            }, {
                "country": "Vinc",
                "value": 39
            }, {
                "country": "SA",
                "value": 19
            }, {
                "country": "FA",
                "value": 10
            }, {
                "country": "Diger",
                "value": 30
            }];
            
            //data = LocationHealthPerformance;

            var chart = AmCharts.makeChart("piechart_statistics", {
                    "type": "pie",
                    "theme": "light",

                    "fontFamily": 'Open Sans',

                    "color": '#888',

                    "dataProvider": data,
                //"valueField": "NAME",
                    "valueField": "value",
                    "titleField": "country",
                    "outlineAlpha": 0.4,
                    "depth3D": 15,
                    "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
                    "angle": 30,
                    "export": {
                        "enabled": true,
                        "fileName": "assethealth-graph",
                        "menu": [ {
                            "class": "export-main",
                            "menu": [ "PNG" ]
                        } ],
                        "libs": { "path": "/Content/assets/global/plugins/amcharts/amcharts/plugins/export/libs/" }
                    },
                    /*
                    "exportConfig": {
                        menuItems: [{
                            icon: '/lib/3/images/export.png',
                            format: 'png'
                        }],
                    }
                    */
                });
            /*
                jQuery('.chart_7_chart_input').off().on('input change', function () {
                    var property = jQuery(this).data('property');
                    var target = chart;
                    var value = Number(this.value);
                    chart.startDuration = 0;

                    if (property == 'innerRadius') {
                        value += "%";
                    }

                    target[property] = value;
                    chart.validateNow();
                });

                $('#chart_7').closest('.portlet').find('.fullscreen').click(function () {
                    chart.invalidateSize();
                });
            */
        },

        initQuestionDataTable: function () {

            if (!jQuery().dataTable) {
                return;
            }

            var table = $('#question_datatable');
            table.DataTable().destroy();

            var jsonVar = {};
            if (typeof QuestionGrid !== 'undefined')
                jsonVar = QuestionGrid;

            table.DataTable({
                'columns': [{ 'data': 'LOCATION_NAME', 'name': 'LOCATION_NAME' }
                            , { 'data': 'EQUIPMENT_NAME', 'name': 'EQUIPMENT_NAME' }
                            , { 'data': 'TITLE', 'name': 'TITLE' }
                ],

                "autoWidth": true,
                data: jsonVar,
                "initComplete": function (settings, json) {
                    //$.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
                },

                // Internationalisation. For more info refer to http://datatables.net/manual/i18n
                "language": {
                    "aria": {
                        "sortAscending": ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    },
                    "emptyTable": "No data available in table",
                    "info": "Showing _START_ to _END_ of _TOTAL_ records",
                    "infoEmpty": "No records found",
                    "infoFiltered": "(filtered1 from _MAX_ total records)",
                    "lengthMenu": "Show _MENU_",
                    "search": "Search:",
                    "zeroRecords": "No matching records found",
                    "paginate": {
                        "previous": "Prev",
                        "next": "Next",
                        "last": "Last",
                        "first": "First"
                    }
                },

                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
                // So when dropdowns used the scrollable div should be removed. 
                //"dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
                "pagingType": "bootstrap_extended",

                "lengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"] // change per page values here
                ],
                // set the initial value
                "pageLength": 5,
                "ScrollX": "100%",
                /*
                "columnDefs": [{  // set default column settings
                    'orderable': false,
                    'targets': [0],
                    "width": "40%"
                }, {
                    "searchable": false,
                    "targets": [0]
                }],
                "order": [
                    [1, "asc"]
                ] // set first column as a default sort by asc
                */
            });

            /*
            var tableWrapper = jQuery('#question_datatable_wrapper');

            table.find('.group-checkable').change(function () {
                var set = jQuery(this).attr("data-set");
                var checked = jQuery(this).is(":checked");
                jQuery(set).each(function () {
                    if (checked) {
                        $(this).prop("checked", true);
                    } else {
                        $(this).prop("checked", false);
                    }
                });
            });
            */
        },
        initQuestionAnswerChart: function (divName, questionId, OPTION_GROUP_ID, id, dateFormat) {
            //console.log('Denem');

            var chartData = [];

            var chartDataSample = [{
                "date": "2012-07-27",
                "value": 13
            }, {
                "date": "2012-07-28",
                "value": 11
            }, {
                "date": "2012-07-29",
                "value": 15
            }, {
                "date": "2012-07-30",
                "value": 16
            }];

            //var startDate = $("#dashboard-report-range").data('daterangepicker').startDate._d;
            //console.log($('#dashboard-report-range'));

            var startDate = $("#" + id).data('daterangepicker').startDate.format(dateFormat);
            var endDate = $("#" + id).data('daterangepicker').endDate.format(dateFormat);
            //console.log(questionId + ' ' + startDate);

            $.ajax({
                url: '/Home/GetFormAnswerValuesForChart',
                type: "GET",
                dataType: "JSON",
                data: { 
                    questionId: questionId,
                    StartDate: startDate,
                    EndDate: endDate
                },

                success: function (result) {
                    

                    $.each(result, function (i, data) {

                        chartData.push(
                       {

                           date: data.Date,
                           value: data.Value,
                           label : data.Label
                          // start: moment(data.start).format('YYYY-MM-DD'),
                       });

                    });

                    if (OPTION_GROUP_ID == 12)
                        Dashboard.drawQuestionAnswerChartYesNo(divName, chartData);
                    else
                        Dashboard.drawQuestionAnswerChartVariables(divName, chartData);
                }
            });

            
            /*
    chart.addListener("rendered", zoomChart);

    zoomChart();
    function zoomChart() {
        chart.zoomToIndexes(chart.dataProvider.length - 40, chart.dataProvider.length - 1);
    }
    */
        },
        drawQuestionAnswerChartVariables: function (divName, chartData) {
            var chart = AmCharts.makeChart(divName, {
                "type": "serial",
                "theme": "light",
                "marginRight": 40,
                "marginLeft": 80,
                "autoMarginOffset": 20,
                "mouseWheelZoomEnabled": true,
                "dataDateFormat": "YYYY-MM-DD",
                "valueAxes": [{
                    "id": "v1",
                    "axisAlpha": 0,
                    "position": "left",
                    "ignoreAxisWidth": true,
                    /*
                    "labelFunction": function (value, valueString, axis) {
                        console.log(value );
                        return value;
                    },*/
                    
                }],
                "balloon": {
                    "borderThickness": 1,
                    "shadowAlpha": 0
                },
                "graphs": [{
                    "id": "g1",
                    "balloon": {
                        "drop": true,
                        "adjustBorderColor": false,
                        "color": "#ffffff"
                    },
                    //"type": "step",
                    "bullet": "round",
                    "bulletBorderAlpha": 1,
                    "bulletColor": "#FFFFFF",
                    "bulletSize": 5,
                    "hideBulletsCount": 50,
                    "lineThickness": 2,
                    "title": "red line",
                    "useLineColorForBulletBorder": true,
                    "valueField": "value",
                    "balloonText": "<span style='font-size:18px;'>[[value]]</span>",

                }],
                "chartScrollbar": {
                    "graph": "g1",
                    "oppositeAxis": false,
                    "offset": 30,
                    "scrollbarHeight": 80,
                    "backgroundAlpha": 0,
                    "selectedBackgroundAlpha": 0.1,
                    "selectedBackgroundColor": "#888888",
                    "graphFillAlpha": 0,
                    "graphLineAlpha": 0.5,
                    "selectedGraphFillAlpha": 0,
                    "selectedGraphLineAlpha": 1,
                    "autoGridCount": true,
                    "color": "#AAAAAA"
                },
                "chartCursor": {
                    "pan": true,
                    "valueLineEnabled": true,
                    "valueLineBalloonEnabled": true,
                    "cursorAlpha": 1,
                    "cursorColor": "#258cbb",
                    "limitToGraph": "g1",
                    "valueLineAlpha": 0.2,
                    "valueZoomable": true
                },
                "valueScrollbar": {
                    "oppositeAxis": false,
                    "offset": 50,
                    "scrollbarHeight": 10
                },
                "categoryField": "date",
                "categoryAxis": {
                    "parseDates": true,
                    "dashLength": 1,
                    "minorGridEnabled": true
                },
                "export": {
                    "enabled": true,
                    "fileName": "assethealth-graph",
                    "libs": { "path": "/Content/assets/global/plugins/amcharts/amcharts/plugins/export/libs/" },
                },
                "dataProvider": chartData
            });

        },

        drawQuestionAnswerChartYesNo: function (divName, chartData) {
            var chart = AmCharts.makeChart(divName, {
                "type": "serial",
                "theme": "light",
                "marginRight": 40,
                "marginLeft": 80,
                "autoMarginOffset": 20,
                "mouseWheelZoomEnabled": true,
                "dataDateFormat": "YYYY-MM-DD",
                "valueAxes": [{
                    "id": "v1",
                    
                    "position": "left",
                    "ignoreAxisWidth": true,
                    /*
                    "labelFunction": function (value, valueString, axis) {
                        console.log(value );
                        return value;
                    },*/
                    //disable the axis' labels, gridAlpha and tickLength so you can
                    //draw them using guides
                    labelsEnabled: false,
                    gridAlpha: 0,
                    tickLength: 0,
                    guides: [{
                        value: 63,
                        tickLength: 5,
                        lineAlpha: .15,
                        label: "Evet"
                    }, {
                        value: 64,
                        tickLength: 5,
                        lineAlpha: .15,
                        label: "Hayir"
                    }, {
                        value: 65,
                        tickLength: 5,
                        lineAlpha: .15,
                        label: "Duzeltildi"
                    }],
                    minimum : 63,
                    maximum : 65
                }],
                "balloon": {
                    "borderThickness": 1,
                    "shadowAlpha": 0
                },
                "graphs": [{
                    "id": "g1",
                    "balloon": {
                        "drop": true,
                        "adjustBorderColor": false,
                        "color": "#ffffff"
                    },
                    "type": "step",
                    "bullet": "round",
                    "bulletBorderAlpha": 1,
                    "bulletColor": "#FFFFFF",
                    "bulletSize": 5,
                    "hideBulletsCount": 50,
                    "lineThickness": 2,
                    "title": "red line",
                    "useLineColorForBulletBorder": true,
                    "valueField": "value",
                    //"balloonText": "<span style='font-size:18px;'>[[value]]</span>",
                    "balloonFunction": function(graphDataItem, graph) {
                        var value = graphDataItem.values.value;
                        if (value == 63 ) {
                            return "Evet";
                        } else if (value == 64 ){
                            return "Hayir";
                        }else if (value == 65 ){
                            return "Duzeltildi";
                        }
                    },

                }],
                "chartScrollbar": {
                    "graph": "g1",
                    "oppositeAxis": false,
                    "offset": 30,
                    "scrollbarHeight": 80,
                    "backgroundAlpha": 0,
                    "selectedBackgroundAlpha": 0.1,
                    "selectedBackgroundColor": "#888888",
                    "graphFillAlpha": 0,
                    "graphLineAlpha": 0.5,
                    "selectedGraphFillAlpha": 0,
                    "selectedGraphLineAlpha": 1,
                    "autoGridCount": true,
                    "color": "#AAAAAA"
                },
                "chartCursor": {
                    "pan": true,
                    "valueLineEnabled": true,
                    "valueLineBalloonEnabled": true,
                    "cursorAlpha": 1,
                    "cursorColor": "#258cbb",
                    "limitToGraph": "g1",
                    "valueLineAlpha": 0.2,
                    "valueZoomable": true
                },
                "valueScrollbar": {
                    "oppositeAxis": false,
                    "offset": 50,
                    "scrollbarHeight": 10
                },
                "categoryField": "date",
                "categoryAxis": {
                    "parseDates": true,
                    "dashLength": 1,
                    "minorGridEnabled": true
                },
                "export": {
                    "enabled": true,
                    "fileName": "assethealth-graph",
                    "libs": { "path": "/Content/assets/global/plugins/amcharts/amcharts/plugins/export/libs/" },
                },
                "dataProvider": chartData
            });

        },

        initInstitutionLayout : function() {
            $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();

            if (typeof $.fn.maphilight === "undefined")
                return;

            $.fn.maphilight.defaults = {
                fill: true,
                fillColor: '000000',
                fillOpacity: 0.5,
                stroke: true,
                strokeColor: 'ff0000',
                strokeOpacity: 1,
                strokeWidth: 1,
                fade: true,
                alwaysOn: false,
                neverOn: false,
                groupBy: false,
                wrapClass: true,
                shadow: false,
                shadowX: 0,
                shadowY: 20,
                shadowRadius: 6,
                shadowColor: '000000',
                shadowOpacity: 0.8,
                shadowPosition: 'outside',
                shadowFrom: false
            }


            $('.map').maphilight({ fillOpacity: 1 });


            qTipWarning('', -1);
            maphilightWarning('', -1);

            var audioElement = document.createElement('audio');
            audioElement.setAttribute('src', '/Content/audio/2-ALARM.mp3');
            audioElement.setAttribute('loop', 'loop');

            var simulatiorActive = false;


            // ================================== Sim�lat�r Ekran�
            $('#btnAlarmSimulation').bind("click", function () {

                simulatiorActive = true;

                qTipWarning('', PARAM_ITEM_WARNING_TYPE_ALARM, 1);
                maphilightWarning(this.LocationName, 2);

                audioElement.play();

                $('#btnAlarmSimulation').hide();
                $('#btnAlarmSimulationStop').show();

            });


            $('#btnAlarmSimulationStop').bind("click", function () {

                qTipWarning('', -1);
                maphilightWarning('', -1);

                if (audioElement != undefined)
                {
                    audioElement.pause();
                    audioElement.currentTime = 0;
                }
                

                $('#btnAlarmSimulation').show();
                $('#btnAlarmSimulationStop').hide();

                setTimeout(function () { poll() }, 30000)

            });


            // ================================== Popup Ekran�
            $.ajaxSetup({ cache: false });

            $("area").each(function () {

                $('area[alt="' + $(this).attr('alt') + '"]').bind("click", function (e) {
                    e.preventDefault();

                    var altText = $(this).attr('alt');
                    var res = altText.split("#");

                    var startDate = dashboard_range.dashboard_range_sdate;
                    var endDate = dashboard_range.dashboard_range_edate;

                    showJqueryUIPopup("/Home/MapPopup?locationName=" + encodeURIComponent(res[0]) + "&StartDate=" + startDate + "&EndDate=" + endDate);

                }); //end of bind. "click"
            }); // end of "area".each

            


            (function poll() {
                LOADING_ENABLED_ON_AJAX = false;

                qTipWarning('', -1);
                maphilightWarning('', -1);

                /*
                var startDate = $("#dashboard-report-range").data('daterangepicker').startDate.format("MM/DD/YYYY");
                var endDate = $("#dashboard-report-range").data('daterangepicker').endDate.format("MM/DD/YYYY");
                */
                var startDate = dashboard_range.dashboard_range_sdate;
                var endDate = dashboard_range.dashboard_range_edate;

                $.ajax({
                    url: "/Home/MapPoll",
                    type: "post",
                    data: { 
                        StartDate: startDate,
                        EndDate: endDate
                    },
                    dataType: "json",
                    success: function (data) {
                        var sonuc = "";

                        //console.log(data.length);
                        if (!$.isArray(data) || data.length <= 0) {
                            qTipWarning('', -1);
                            maphilightWarning('', -1);

                            if (audioElement != undefined) {
                                audioElement.pause();
                                audioElement.currentTime = 0;
                            }
                        }

                        $.each(data, function () {
                            //console.log(this.LocationName + " " + this.Warning_type + " " + this.SensorType);

                            if (this.Warning_type == PARAM_ITEM_WARNING_TYPE_ALARM) // Alarm
                            {
                                qTipWarning(this.LocationName, this.Warning_type, this.SensorType);
                                maphilightWarning(this.LocationName, PARAM_ITEM_WARNING_TYPE_ALARM);

                                //audioElement.play();
                            }
                            else if (this.Warning_type == PARAM_ITEM_WARNING_TYPE_WARNING) // Uyar�
                            {
                                qTipWarning(this.LocationName, this.Warning_type, this.SensorType);
                                maphilightWarning(this.LocationName, PARAM_ITEM_WARNING_TYPE_WARNING);

                            }


                        });

                    },
                    error: function (msg) {
                        //$('#pollResult').html("Sunucu ile ileti�im kurulamad� : " + msg);
                    },
                    complete: setTimeout(function () { poll() }, 30000),
                    timeout: 10000
                })
            })();
        },

        initFeedList : function () {

            function EventListViewModel() {
                var self = this;

                self.IsLoading = ko.observable(true);

                self.lookupCollection = ko.observableArray();
                self.GeneralStatModel = ko.observableArray();
                self.UserActivityModel = ko.observableArray();

                self.Provider = ko.observable();
                self.Files = ko.observable();
                self.IsInDb = ko.observable();


                self.Files.Title = ko.observable("");

                var Provider =
                {
                    Title: self.Files.Title,
                    IsInDb: self.IsInDb
                };

                self.deleteItem = function (Provider) {
                    if (confirm('Are you sure you want to delete this row?')) {
                        var title = Provider.Files.Title();

                        self.IsLoading(true);
                        $.ajax({
                            url: "/RemoteAdmin/DeleteAmazonS3File",
                            cache: false,
                            type: 'POST',
                            contentType: 'application/json; charset=utf-8',
                            data: ko.toJSON({ fileName: title }),
                            success: function (data) {
                                self.lookupCollection.remove(Provider);
                                //   alert("Record Deleted Successfully");

                                self.IsLoading(false);
                            }
                        }).fail(
                             function (xhr, textStatus, err) {
                                 alert(err);
                             });
                    }
                };  //Delete Provider


            }

            this.viewModel = new EventListViewModel();

            ko.applyBindings(this.viewModel);

        },
        viewModel: {},

        init: function() {
            this.initCalendar(); // Feeds Calendar - Kontrol Plan�  - fullcalendar
            this.initCharts();  // Normal Chart - Asset Health Performance   -- flot plugin
            this.initEasyPieCharts(); // General Stats -  Genel �statistik
            this.initSparklineCharts(); // Member Activity - AKT�V�TE 
            this.initAmChartYesNo();  // Ekipman Sa�l�k Performans
            this.initTable2();  //  G�ncel Anormallikler
            this.initAmChartPieChart();  // Lokasyon Bazl� Kontrol Performans�
            this.initQuestionDataTable();
            
            //this.initQuestionAnswerChart();  // Bu methodu GridBoxQuestion'dan �a��r�yoruz

            this.initDashboardDaterange("dashboard-report-range");
            this.initInstitutionLayout();
            //this.initFeedList();

            /*this.initJQVMAP();
            this.initCalendar();
            this.initCharts();
            this.initEasyPieCharts();
            this.initSparklineCharts();
            this.initChat();
            
            this.initMorisCharts();

            this.initAmChart1();
            this.initAmChart2();
            this.initAmChart3();
            this.initAmChart4();

            this.initWorldMapStats();
            */
        }
    };

}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function() {
        //Dashboard.init(); // init metronic core componets
    });
}

