var FormFileUpload = function () {
    return {
        //main function to initiate the module
        init: function () {

            var multipleFiles = false;

            sessionStorage.setItem('number_of_files', 0);
            sessionStorage.setItem('multipleFiles', multipleFiles);

            var fileUploadGuid = $('#File_guid');
            var fileId = $('#Id');
            
            var uploading;

            var baseUrl = $("#BaseUrl").data("baseurl");

            var fileList = [{ "group": null, "name": "Requirement document_ GU_v2.1.docx", "type": null, "size": 1022067, "progress": "1.0", "url": "/api/Upload?f=Requirement document_ GU_v2.1.docx", "thumbnailUrl": null, "deleteUrl": "/api/Upload?f=Requirement document_ GU_v2.1.docx", "deleteType": "DELETE", "error": null }, { "group": null, "name": "Requirement document_ IPTA_v2.1.docx", "type": null, "size": 79140, "progress": "1.0", "url": "/api/Upload?f=Requirement document_ IPTA_v2.1.docx", "thumbnailUrl": null, "deleteUrl": "/api/Upload?f=Requirement document_ IPTA_v2.1.docx", "deleteType": "DELETE", "error": null }, { "group": null, "name": "Requirement document_ QACU_v2.1.docx", "type": null, "size": 352817, "progress": "1.0", "url": "/api/Upload?f=Requirement document_ QACU_v2.1.docx", "thumbnailUrl": null, "deleteUrl": "/api/Upload?f=Requirement document_ QACU_v2.1.docx", "deleteType": "DELETE", "error": null }];
             // Initialize the jQuery File Upload widget:
            var fUpload = $('#fileupload').fileupload({
                disableImageResize: true,
                autoUpload: !multipleFiles,
                //disableImageResize: /Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent),
                //maxNumberOfFiles: 1,
                //maxFileSize: 5000000,
                acceptFileTypes: /(\.|\/)(gif|jpe?g|png|doc|docx|xls|xlsx|pdf)$/i,
                //url: window.location.protocol + '//' + window.location.host + (window.location.port ? ':' + window.location.port : '') + '/' + 'api/Upload/',
                url: baseUrl + '/' + 'api/Upload/',
                formData: { fileUploadGuid: fileUploadGuid.val(), fileId: fileId.val() },
                //add: fileuploadaddFunction
                /*
                    add: function (e, data) {
                        if (!uploading) {
                            uploading = 1;
                            console.log('add');
                            data.submit();
                        } else {
                            alert('Single file is allowed');
                            console.log('cancel');
                        }
                    },
            always: function() {
                uploading = 0;
            },
            destroy: function (e, data) {
                var destroyFn = $('#fileupload').fileupload('option', 'destroy')
                console.log(destroyFn)
                destroyFn(e, data);
            }
            */
                // Uncomment the following to send cross-domain cookies:
                //xhrFields: {withCredentials: true},    

            });

            
            var deleteMouseDownFired = false;
            var globalDeleteConfirmed = false;

            $("#btnMainDelete").mouseup(
                function () {
                    deleteMouseDownFired = true;
                    globalDeleteConfirmed = confirm(_("%Delete.selected.files"));
                }
            );

            $("#btnMainDelete").click(
                function () {
                    deleteMouseDownFired = false;
                    globalDeleteConfirmed = false;
                }
            );

            $('#fileupload').bind('fileuploaddestroy',
                function (e, data) {
                    if (deleteMouseDownFired) {
                        if (globalDeleteConfirmed) {
                            return true;
                        }
                        return false;
                    }
                    else {

                        if (!multipleFiles)
                        {
                            number_of_files = 0;
                            sessionStorage.setItem('number_of_files', number_of_files);
                        }

                        return confirm(_("%Delete.selected.file"));
                    }
                }
            );


            var counter = 0;
            var number_of_files = 0; // or sessionStorage.getItem('number_of_files')

            //The added callback is the equivalent to the add callback and is triggered after the upload template has been rendered and the transition effects have completed.
            var fileuploadaddFunction = function (e, data) {
                console.log('fileuploadadd');


                
                if (multipleFiles)
                {
                    $.each(data.files, function (index, file) {
                        console.log('Added file in queue: ' + file.name);
                        number_of_files = number_of_files + 1;
                        sessionStorage.setItem('number_of_files', number_of_files);
                    });
                }
                else
                {
                    if (sessionStorage.getItem('number_of_files') <= 0)
                    {
                        var that = this;
                        $.blueimp.fileupload.prototype.options.add.call(that, e, data);

                        number_of_files = number_of_files + 1;
                        sessionStorage.setItem('number_of_files', number_of_files);
                        console.log(data.files);
                        data.submit();
                        $(this).fileupload('option', 'done').call(this, null, null);
                        //$(this).fileupload('option', 'done').call(this, $.Event('done'), { result: { files: data.files } });
                        //doneFunction(e, data);
                    }
                    else {
                        alert(_("%you.can.select.single.file"));
                        
                        //$(this).fileupload('option', 'done').call(this, $.Event('done'), null);
                        $(this).fileupload('option', 'done').call(this, null, null);
                        return;
                    }
                        
                    
                }
                    
            };
            $('#fileupload').bind('fileuploadadd', fileuploadaddFunction);
            

            //Callback for successful upload requests. This callback is the equivalent to the success callback provided by jQuery ajax() and will also be called if the server returns a JSON response with an error property.
            var doneFunction = function (e, data) {
                console.log('fileuploaddone')
                $.each(data.files, function (index, file) {
                    console.log('Added file in queue: ' + file.name);
                    if (multipleFiles) {
                        number_of_files = 0;
                        sessionStorage.setItem('number_of_files', number_of_files);
                    }
                });
            };

            $('#fileupload').bind('fileuploaddone', doneFunction);

            // The completed callback is the equivalent to the done callback and is triggered after successful uploads after the download template has been rendered and the transition effects have completed.
            $('#fileupload').bind('fileuploadcompleted',
            function (e, data) {
                console.log('fileuploadcompleted')
                if ($('#btnMainDelete').length > 0 && !$('#btnMainDelete').is(":visible"))
                {
                    //makeFormReadOnly();
                }
            });
            

            /*
            // Mevcut destroy metodunu extend ederek �a��rma y�ntemi
            // https://groups.google.com/forum/#!topic/jquery-fileupload/9SpfNYCmgcE
            //invocation context for old handler
            //var thisobj = $('#fileupload')[0];
            var thisobj = fUpload;

            var oldDestroy = $('#fileupload').fileupload('option', 'destroy');

            var newDestroy = function (e, data) {
                console.log(" greetings from the new destroy handler!");
                //more awesome code goes here...

                oldDestroy.call(thisobj, e, data);
                
                //if the delete worked on the server's end...
                //copy data, but with blank url (don't wipe out data.url or it's gone for good)
                var _data = $.extend({}, data, { url: null });
                //oldDestroy.call(thisobj, e, _data);
                
            };

            //replace the old handler with our handler (which may, in fact, call the old handler)
            $('#fileupload').fileupload('option', 'destroy', newDestroy);
            */
            


            // Enable iframe cross-domain access via redirect option:
            $('#fileupload').fileupload(
                'option',
                'redirect',
                window.location.href.replace(
                    /\/[^\/]*$/,
                    '/cors/result.html?%s'
                )
            );

            /*
            // Upload server status check for browsers with CORS support:
            if ($.support.cors) {
                $.ajax({
                    type: 'HEAD'
                }).fail(function () {
                    $('<div class="alert alert-danger"/>')
                        .text('Upload server currently unavailable - ' + new Date())
                        .appendTo('#fileupload');
                });
            }
            */


            // Load & display existing files:
            $('#fileupload').addClass('fileupload-processing');
            $.ajax({
                // Uncomment the following to send cross-domain cookies:
                //xhrFields: {withCredentials: true},
                url: baseUrl + '/' + 'api/Upload?fileUploadGuid=' + fileUploadGuid.val() + "&fileId=" + fileId.val(),
                //url: $('#fileupload').fileupload('option', 'url'),
                dataType: 'json',
                context: $('#fileupload')[0]
            }).always(function () {
                $(this).removeClass('fileupload-processing');
            }).done(function (result) {
                fileList = JSON.parse(result).files;
                sessionStorage.setItem('number_of_files', fileList.length);
                console.log('done ' + fileList.length);
                $(this).fileupload('option', 'done').call(this, $.Event('done'), { result: { files: fileList } });

                /*
                $('#fileupload').fileupload('add', {
                    files: fileList,
                    fileInput: $(this)
                });
                */
            });

            
            

        }

    };

}();

jQuery(document).ready(function() {
    FormFileUpload.init();
});