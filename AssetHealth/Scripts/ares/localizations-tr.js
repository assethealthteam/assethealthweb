﻿String.toLocaleString({
	"en": {
	    "%Delete.selected.file": "Are you sure to delete the file ?",
	    "%Delete.selected.files": "Are you sure to delete the files ?",
	    "%info": "You are viewing an English localization of this page.",
	    "%you.can.select.single.file": "You can only add single file",

	    "admin.common.edit": "Edit",
	    "admin.common.delete": "Delete",
	    "admin.common.assigntask": "Task",

	    "%Today": "Today",
	    "%Yesterday": "Yesterday",
	    '%Last_7_Days': "Last 7 Days",
	    "%Last_30_Days": "Last 30 Days",
	    "%This_Month": "This Month",
	    "%Last_Month": "Last Month",

	    "%Apply": "Apply",
	    "%Cancel": "Cancel",
	    "%CustomRange": "Custom Range",
	},
		"en-US": {
			"%title": "American English - l10n.js demo",
			"%info": "You are viewing an American English localization of this page."
		},
		"en-GB": {
			"%title": "British English - l10n.js demo",
			"%info": "You are viewing a British English localisation of this page."
		},


	"tr": {
	    "%Delete.selected.file": "Dosyayı silmek istediğinizden emin misiniz ?",
	    "%Delete.selected.files": "Dosyaları silmek istediğinizden emin misiniz ?",
	    "admin.common.edit": "Düzenle",
	    "admin.common.delete": "Sil",
	    "admin.common.assigntask": "Görev Kartı",
	    "%you.can.select.single.file": "Çoklu dosya ekleyemezsiniz",

	    "%Today": "Bugün",
	    "%Yesterday": "Dün",
	    '%Last_7_Days': "Son 7 Gün",
	    "%Last_30_Days": "Son 30 Gün",
	    "%This_Month": "Bu Ay",
	    "%Last_Month": "Geçen Ay",

	    "%Apply": "Uygula",
	    "%Cancel": "Vazgeç",

	    "%CustomRange": "Tarih Aralığı",
	    
	}
});