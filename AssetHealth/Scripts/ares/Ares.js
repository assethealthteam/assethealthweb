

jQuery(document).ready(function () {

    $(document).tooltip();

    $.validator.setDefaults({
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
            $(element).closest('.input-icon').find('.fa-warning').removeClass('hide');
           
            
            /*
            //var error1 = $('.alert-danger').show();
            var error1 = $(element).parent().parent().parent().find('.alert-danger').show();
            //console.log(error1);
            //App.scrollTo(error1, -200);
            error1.append('');

            */
            
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
            $(element).closest('.input-icon').find('.fa-warning').addClass('hide');
        },
        /*
        invalidHandler: function (event, validator) {
            // set up for showing one error message above form
            errors = validator.numberOfInvalids();
            if (errors) {
                
            } 
        },
        
        showErrors: function(errorMap, errorList) {
            alert( this.numberOfInvalids() + " errors");
        },
        */
        
        onfocusout: function (element) {
            var $el = $(element);
            /*
            if (!$el.is('select') && element.value === '' && element.defaultValue === '') {
                // for untouched text fields, don't validate on blur
                return;
            }
            */
            //refreshDangerContent(formValidate($(element).closest("form")), '')
            
            $el.valid();
            refreshDangerContent($(element).closest("form"), '')
            
        },

        
        //onkeyup: function (element) { $(element).valid(); refreshDangerContent($(element).closest("form"), '') }
        /*

    errorElement: 'span',
    errorClass: 'help-block',
        //errorClass: 'alert-danger',
    errorPlacement: function (error, element) {
        console.log('errorPlacement');
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    }
    */


    });

    /*
    $.validator.addMethod('date', function (value, element) {
        if (this.optional(element)) {
            return true;
        }
        var valid = true;
        try {
            $.datepicker.parseDate('dd-mm-yyyy', value);
        }
        catch (err) {
            valid = false;
        }
        return valid;
    });
    */

    moment.locale('tr'); // returns the new locale, in this case 'tr'

    // jQuery datepicker
    $.datepicker.setDefaults({
        dateFormat: 'dd-mm-yyyy',
        minDate: new Date(1900, 1 - 1, 1),
        yearRange: '1900:2020',
        showButtonPanel: true,
        closeText: 'Clear', // Text to show for "close" button
        
    });

    //bootstrap datepicker
    $('.date-picker').datepicker({
        format: "dd-mm-yyyy",
        maxViewMode: 3,
        todayBtn: "linked",
        clearBtn: true,
        keyboardNavigation: false,
        daysOfWeekHighlighted: "0,1,2,3,4",
        autoclose: true,
        todayHighlight: true,
    });
    
    
    $.validator.methods.date = function (value, element) {
        var isValidDate = true;
        if (value) {
            var rgexp = /(^(((0[1-9]|1[0-9]|2[0-8])[-](0[1-9]|1[012]))|((29|30|31)[-](0[13578]|1[02]))|((29|30)[-](0[4,6,9]|11)))[-](19|[2-9][0-9])\d\d$)|(^29[-]02[-](19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)$)/;
            isValidDate = rgexp.test(value);
        }

        return isValidDate;
        //return this.optional(element) || (value.length == 10 && $.datepicker.parseDate("dd-mm-yyyy", value));
    };

    // if web.config uiCulture is TR , numeric validation must be according to seperator of ","
    jQuery.extend(jQuery.validator.methods, {
        number: function (value, element) {
            return this.optional(element) || /^-?(?:\d+|\d{1,3}(?:\.\d{3})+)(?:[,.]\d+)?$/.test(value);
        }
    });



    Inputmask.extendAliases({
        'decimal': {
            "repeat": 10,
            "greedy": false,
            rightAlignNumerics: true,
            'groupSeparator': ',',
            'radixPoint': '.',
            //'autoGroup': true,
            'digits': 2,
            'digitsOptional': false,


        }
    });


    $('.fa').each(function () {
        $(this).qtip({
            content: {
                //text: $(this).attr("data-original-title"),
                //attr: "data-original-title",

                title: 'Error',
                text: function ( api) {
                    var content = $(this).attr("data-original-title");
                    console.log(content === '');

                    return (api != null && (content === undefined || content === '')) ? api.destroy() : content;
                }
                
                
            }, style: {
                classes: 'qtip-bootstrap',
                width: 500, // Overrides width set by CSS (but no max-width!)
                height: 100 // Overrides height set by CSS (but no max-height!)
            }
        });
    });


    jQuery('body').on('click', '.portlet > .portlet-title > .caption', function (e) {
        jQuery(this).parent().find('.tools > .collapse, .tools > .expand').each(function () {
            var el = jQuery(this).closest(".portlet").children(".portlet-body");
            if (jQuery(this).hasClass("collapse")) {
                jQuery(this).removeClass("collapse").addClass("expand");
                el.slideUp(200);
            } else {
                jQuery(this).removeClass("expand").addClass("collapse");
                el.slideDown(200);
            }
        });
    });



    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        /*
        https://datatables.net/forums/discussion/35868/scrollx-and-width-of-headers-problem
        https://datatables.net/examples/api/tabs_and_scrolling.html
        */
        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
    });



    /**
 * Trigger a callback when 'this' image is loaded:
 * @param {Function} callback
 */
    (function ($) {
        $.fn.imgLoad = function (callback) {
            return this.each(function () {
                if (callback) {
                    if (this.complete || /*for IE 10-*/ $(this).height() > 0) {
                        callback.apply(this);
                    }
                    else {
                        $(this).on('load', function () {
                            callback.apply(this);
                        });
                    }
                }
            });
        };

        $.fn.positionMe = function () {
            var oH = $(window).height();
            var oW = $(window).width();
            var iH = this.outerHeight();
            var iW = this.outerWidth();

            // When the image is too small so, do nothing
            if (iW > oW && iH > oH) {

                // Otherwise, proportionate the image relative 
                // to its container
                if (oH / iH > oW / iW) {
                    this.css("width", oW);
                    this.css("height", iH * (oW / iW))
                } else {
                    this.css("height", oH);
                    this.css("width", iW * (oH / iH));
                }

            }
            return this;
        };

    })(jQuery);




    $('#blueimp-gallery').on('slide', function (event, index, slide) {
        var firstImg = $(slide).find('img:first');
        $(firstImg[0]).hide();
        

        
        $(firstImg[0]).imgLoad(function () {
            
            var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);

            if (isChrome)
            {

                //$(firstImg[0]).parent().removeClass('slide');
                $(firstImg[0]).parent().addClass('slide-loading');

                /*
                var src = firstImg[0].getAttribute("src") + "?cb=" + new Date().getTime();
                
                window.loadImage(src, function (img) {
                    if (img.type === "error") {
                        console.log("couldn't load image:", img);
                    } else {
                        window.EXIF.getData(img, function () {
                            console.log(img);
                            var orientation = window.EXIF.getTag(this, "Orientation");
                            var canvas = window.loadImage.scale(img, { orientation: orientation || 0, canvas: true });
                            var ctx = canvas.getContext("2d");
                            console.log(canvas.width + " / " + canvas.height);

                            $(firstImg[0]).attr('src', canvas.toDataURL());
                            
                            firstImg[0].onload = function () {
                                //ctx.drawImage(firstImg[0], 10, 10);
                            };

                            //document.getElementById("container").appendChild(canvas);
                            // or using jquery $("#container").append(canvas);
                            
                            console.log(canvas);
                            $(firstImg[0]).attr('crossOrigin', 'anonymous');
                            $(firstImg[0]).attr('src', canvas.toDataURL("image/png"));
                            //$(".slide").append(canvas)
                            

                            $(firstImg[0]).show();
                            ctx.drawImage(firstImg[0], 10, 10);
                            
                        });
                    }
                });
                */
                

                fixExifOrientation(firstImg[0]);

                //$(".slide").css({ "overflow": "hidden" });                
            }
            else
                $(firstImg[0]).show();


            
        });


        //console.log('slide ' + index + firstImg[0].src);
    });


    $("body").on("click", "a.imgGallery", function () {
        //console.log('click');
        //console.log($(this).data('imggallery'));

        $(this).lightGallery($(this).data('imggallery'));
    });

}); // jquery onready finished



function fixExifOrientation(img) {
    /*
 var b64 = "data:image/jpeg;base64,/9j/4AAQSkZJRgABA......";
 var bin = atob(b64.split(',')[1]);
 var exif = EXIF.readFromBinaryFile(new BinaryFile(bin));
 alert(exif.Orientation);
 */

    /*
var exifArray = $($('.files').find('img:first')[0]).data('exif')
console.log(exifArray);

var longitude = $.grep(exifArray, function (obj) { return obj.Description === 'GPS Longitude'; })[0];
var orientation = $.grep(exifArray, function (obj) { return obj.Description === 'Orientation'; })[0];

var href = $('.slide[data-index=' + index + '] .slide-content');
*/

    //$img.on('load', function () {
        EXIF.getData(img, function () {
            //console.log('Exif=', EXIF.getTag(this, "Orientation"));
            //var make = EXIF.getTag(this, "Make");
            //var model = EXIF.getTag(this, "Model");

            switch (parseInt(EXIF.getTag(this, "Orientation"))) {
                case 2:
                    $(this).addClass('flip'); break;
                case 3:
                    $(this).addClass('rotate-180'); break;
                case 4:
                    $(this).addClass('flip-and-rotate-180'); break;
                case 5:
                    $(this).addClass('flip-and-rotate-270'); break;
                case 6:
                    $(this).addClass('rotate-90'); break;
                case 7:
                    $(this).addClass('flip-and-rotate-90'); break;
                case 8:
                    $(this).addClass('rotate-270'); break;
            }

            
            $(img).parent().addClass('slide');
            //$(".slide").css({ "overflow": "hidden" });
            $(img).addClass('center-fit');
            $(img).removeClass('slide-content');
            $(img).show();
        });
    //});
}


function formValidate(form) {

    var validator = form.validate();

    return validator;
}

function refreshDangerContent(form, summary)
{
    //var form1 = $('form');
    //var form1 = validator.form();
    var form1 = form;
    form1.valid();
    var validator = form1.validate();

    var error1 = $('.alert-danger', form1);
    var dangerContent = $(error1).find('#dangerContent');

    error1.removeClass('display-hide');
    error1.removeAttr("style");
    error1.show();
    /*
    form1.bind('invalid-form.validate', function (form, validator) {
        $.each(validator.errorList, function () { summary += " * " + this.message + "<br/>"; });
        dangerContent.html(summary);
    });
    */
    

    $("i[id*='fa']").data("original-title", "");
    $("i[id*='fa']").attr("data-original-title", "");

    $.each(validator.errorList, function () { summary += " * " + this.message + "<br/>"; });
    for (var i = 0; i < validator.errorList.length; i++) {

        $("#fa" + validator.errorList[i].element.name).data("original-title", validator.errorList[i].message);
        $("#fa" + validator.errorList[i].element.name).attr("data-original-title", validator.errorList[i].message);
    }

    
    dangerContent.html(summary);
    if (summary === '') {
        dangerContent.empty();        
        error1.hide();
    }
}


if (!String.locale)
{
    String.locale = "tr";
    console.log('tr set edildi');
}
    

// For Localization
var _ = function (string) {
    return string.toLocaleString();
};


function getJSONDate(jsonDate)
{
    if (jsonDate === null) return "";
    var pattern = /Date\(([^)]+)\)/;
    var results = pattern.exec(jsonDate);
    if (results != null)
        return new Date(parseFloat(results[1]));
    
    return null;
}

function formatJSONDate(jsonDate)
{
    if (jsonDate === null) return "";
    var pattern = /Date\(([^)]+)\)/; 
    var results = pattern.exec(jsonDate);
    var dt = new Date(parseFloat(results[1]));
    return formatDate(dt);
}

function formatJSONDateTime(jsonDate) {
    if (jsonDate === null) return "";
    var pattern = /Date\(([^)]+)\)/;
    var results = pattern.exec(jsonDate);
    var dt = new Date(parseFloat(results[1]));
    return formatDateTime(dt);
}

function formatDate(dt)
{
    var hour = dt.getHours(),
            minute = dt.getMinutes(),
            second = dt.getSeconds(),
            hourFormatted = hour < 10 ? "0" + hour : hour, // hour returned in 24 hour format
            minuteFormatted = minute < 10 ? "0" + minute : minute,
            morning = hour < 12 ? "am" : "pm";

    return (dt.getDate() < 10 ? "0" + dt.getDate() : dt.getDate()) + "." + (dt.getMonth() + 1 < 10 ? "0" + (dt.getMonth() + 1) : dt.getMonth() + 1) + "." + dt.getFullYear();
}

function formatDateTime(dt) {
    var hour = dt.getHours(),
            minute = dt.getMinutes(),
            second = dt.getSeconds(),
            hourFormatted = hour < 10 ? "0" + hour : hour, // hour returned in 24 hour format
            minuteFormatted = minute < 10 ? "0" + minute : minute,
            secondFormatted = second < 10 ? "0" + second : second,
            morning = hour < 12 ? "am" : "pm";

    return (dt.getDate() < 10 ? "0" + dt.getDate() : dt.getDate()) + "." + (dt.getMonth() + 1 < 10 ? "0" + (dt.getMonth() + 1) : dt.getMonth() + 1) + "." + dt.getFullYear() + " " + hourFormatted + ":" + minuteFormatted + ":" + secondFormatted;
}

function disableBootstrapDatePicker(id)
{
    $("#" + id).prop('disabled', true);
    $("#" + id + " ~ span:first").hide();
}

Object.defineProperty(Date.prototype, 'YYYYMMDDHHMMSS', {
    value: function () {
        function pad2(n) {  // always returns a string
            return (n < 10 ? '0' : '') + n;
        }

        return this.getFullYear() +
               pad2(this.getMonth() + 1) +
               pad2(this.getDate()) +
               pad2(this.getHours()) +
               pad2(this.getMinutes()) +
               pad2(this.getSeconds());
    }
});


function getDatatableEditDeleteButtonString(url, id, editVisible, deleteVisible, actionname) {

    var splitted = url.split("/");
    var urlPrefix;  // holds if in an iis application and controller name such as if url : /admin/Files/Edit/3 => urlPrefix : /admin/Files/
    
    if (splitted.length >= 3)
        urlPrefix = url.substring(0, url.indexOf(splitted[splitted.length - 2]));


    var buttonString = "";
    if (editVisible)
        buttonString = "&nbsp;&nbsp;<a href='" + url + "' ><i class='lnkEdit fa fa-pencil' style='font-size: 22px;' title='" + _("admin.common.edit") + "'></i></a>";

    if (deleteVisible)
    {
        var deleteUrl = urlPrefix + 'Delete' + actionname + '/' + id;
        buttonString += "&nbsp;&nbsp;<a id='lnk_delete_" + id + "'  data-toggle='modal' data-deleteurl='" + deleteUrl + "' data-id='" + id + "' data-urlPrefix='" + urlPrefix + "' data-actionname='Delete" + actionname + "' href='#modalDeleteConfirm' title='" + _("admin.common.delete") + "'><i class='lnkDelete fa fa-trash-o' style='font-size: 22px;' data-original-title='Delete'></i></a>";
    }
    

    return buttonString;
}

function getDatatableValueWithTooltip(fieldValue, tooltip)
{
    return (fieldValue === null ? "" : fieldValue) + (tooltip === null || tooltip === '' ? "" : "&nbsp;<i id='faTooltip' style='font-size: 20px;' class='fa fa-tags' title='" + tooltip + "'></i>");
}

function getDatatableTaskButtonString(url, id) {

    buttonString = "&nbsp;&nbsp;<a id='btnAssignTask' class='lnkTask' href='" + url + "' data-id='" + id + "' title='" + _("admin.common.assigntask") + "'><i class='fa fa-check-square-o' style='font-size: 22px;' ></i></a>";

    return buttonString;
}


function getDatatableImageGalleryString(text, imgGallery) {
    
    var result = text == null ? "" : text;
    if (imgGallery != null) {
        if (imgGallery.hasImage)
            result = result + " <a class='imgGallery' data-imggallery='" + JSON.stringify(imgGallery) + "' ><i class='fa fa-camera' style='font-size: 22px;' title='" + "'></i></a>";
        if (imgGallery.hasVideo)
        {
            result = result + "&nbsp;&nbsp;<a class='imgGallery' data-imggallery='" + JSON.stringify(imgGallery) + "'><i class='fa fa-video-camera' style='font-size: 22px;' title='" + "'></i></a>";
            //result = result + "&nbsp;&nbsp;<a class='imgGallery' data-imggallery='" + JSON.stringify(imgGallery).replace(/&#39;/g, '\\\"') + "'><i class='fa fa-video-camera' style='font-size: 22px;' title='" + "'></i></a>";
            //console.log(result);
        }
        if (imgGallery.hasDocument && imgGallery.documents != null) {
            for (i = 0; i < imgGallery.documents.length; i++)
            {
                result = result + "&nbsp;&nbsp;<a class='' target='_blank' href='" + imgGallery.documents[i].src + "'><i class='fa " + (imgGallery.documents[i].src.indexOf("pdf") !== -1 ? "fa-file-pdf-o" : imgGallery.documents[i].src.indexOf("xls") !== -1 ? "fa-file-excel-o" : imgGallery.documents[i].src.indexOf("doc") !== -1 ? "fa-file-word-o" : "fa-file") + "' style='font-size: 22px;' title='" + "'></i></a>";
            }
            
        }
            
    }


    return result;
}

/*
$('#imgGallery').on('click', function (e) {

    $(this).lightGallery({
        dynamic: true,
        thumbnail: false,
        dynamicEl: [{
            "src": 'https://assethealth.s3.amazonaws.com/demo/20180731193937.jpg',
            'thumb': '../static/img/thumb-1.jpg',
            'subHtml': '<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>'
        }, {

            'thumb': '../static/img/thumb-2.jpg',
            'html': '<video class="lg-video-object lg-html5" controls preload="none"><source src="https://assethealth.s3.amazonaws.com/demo/20180731194025.mp4" type="video/mp4">Your browser does not support HTML5 video</video>',
            'subHtml': "<h4>Bowness Bay</h4><p>A beautiful Sunrise this morning taken En-route to Keswick not one as planned but I'm extremely happy I was passing the right place at the right time....</p>"
        }]
    });

});
*/

function makeFormReadOnly() {
    $('form :input').not(':button,:hidden').attr('readonly', 'readonly');
    $("select").attr("disabled", true);
    $("form :input[type='checkbox']").attr("disabled", true);
    $("input[name*='save']").hide();
    $("button[name*='save']").hide();
    $("button[name*='btnDelete']").hide();
    $("button").not('.close').not('#btnCloseModal').not('#btnCancel').not('#deneme').not('#btnExamineURL').not('#btnTaskURL').not('#btnAssignTask').hide();
    $("#spanFileInput").hide();
    $("a[id*='btnDelete']").hide();
    $("i[class*='fa fa-trash-o']").closest('a').hide();
    $("i[class*='fa fa-plus']").closest('a').hide();

}

function displayValidationErrors(errors) {
    var $ul = $('div.validation-summary-valid > ul');

    $ul.empty();
    $ul.append('<li>' + errors + '</li>');
    /*
    $.each(errors, function (idx, errorMessage) {
        $ul.append('<li>' + errorMessage + '</li>');
    });
    */
}



function clearForm(myFormElement, excludedIds) {

    var elements = myFormElement.elements;

    //myFormElement.reset();

    for (i = 0; i < elements.length; i++) {

        if (excludedIds.indexOf(elements[i].id) != -1) continue;

        field_type = elements[i].type.toLowerCase();

        switch (field_type) {

            case "text":
            case "password":
            case "textarea":
            case "hidden":

                elements[i].value = "";
                break;

            case "radio":
            case "checkbox":
                if (elements[i].checked) {
                    elements[i].checked = false;
                }
                break;

            case "select-one":
            case "select-multi":
                //elements[i].selectedIndex = -1;
                elements[i].selectedIndex = 0;
                break;

            default:
                break;
        }
    }
}

function dataTableDeselectAll(tableId)
{
    $('#' + tableId +' tbody tr').removeClass('selected');
}

function dataTableGetSelectedRow(tableId) {
    return $('#' + tableId).DataTable().rows('.selected');
}

function dataTableGetSelectedRowData(tableId) {
    return $('#' + tableId).DataTable().rows('.selected').data()[0];
}

function getDataTableRowIdxById(tableId, id) {
    var indexes = $('#' + tableId).DataTable().rows().eq(0).filter(function (rowIdx) {
        return $('#' + tableId).DataTable().cell(rowIdx, 0).data() === id ? true : false;
    });
    return indexes;
}

function getDataTableSelectedRowIdx(tableId) {
    var id = dataTableGetSelectedRowData(tableId);

    return getDataTableRowIdxById(tableId, id);
}

function resetDatatableSearch(tableId) {
    $('#' + tableId).DataTable().search('').columns().search('').draw();;
    
}


var DataTablePrintButtonAttribute = (function () {
    return [{
        extend: 'collection', text: 'Export', buttons: ['copy', { extend: 'excel', filename: (function () { var d = new Date(); var n = d.YYYYMMDDHHMMSS(); return 'Exp-' + n; }()) }, 'csv', {
            extend: 'pdf', title: 'Data export',
            pageSize : 'a3',
            filename: (function () { var d = new Date(); var n = d.YYYYMMDDHHMMSS(); return 'Exp-' + n; }()),
            customize: function (doc, config) {
                var tableNode;
                for (i = 0; i < doc.content.length; ++i) {
                    if (doc.content[i].table !== undefined) {
                        tableNode = doc.content[i];
                        break;
                    }
                }

                var rowIndex = 0;
                var tableColumnCount = tableNode.table.body[rowIndex].length;

                if (tableColumnCount > 5) {
                    doc.pageOrientation = 'landscape';
                }
            }
        }, {
            text: 'Expand',
            action: function (e, dt, node, config) {
                
                reCreateDataTable('#dataTable', window.dataTableOptions);
                               
                
            }
        }, {
            extend: 'colvis',
            collectionLayout: 'fixed two-column'
        }]
    }];

}());

function reCreateDataTable(datatableSelector, dataTableOptions)
{
    /*
    $.ajax({
        url: '/Form/List',
        type: 'GET',
        cache: false,
        data: {  }
    }).done(function(result) {
        $('#divList').html(result);
    });
    */


//    var dtOptions = dt.settings()[0];
    //var params = dt.ajax.params();

    if (dataTableOptions.responsive === undefined || !dataTableOptions.responsive)
        dataTableOptions.responsive = DataTableResponsiveAttribute;
    else
        dataTableOptions.responsive = false;

    var dt = $(datatableSelector).DataTable();
    dt.destroy();

    $(datatableSelector).removeClass('dtr-inline collapsed');

    dataTable = $(datatableSelector).DataTable(dataTableOptions);
    
    
}

var DataTableResponsiveAttribute = (function () {
    return {
        details: {
            display: $.fn.dataTable.Responsive.display.modal({
                header: function (row) {
                    var data = row.data();
                    return 'Details for ' + data[0] + ' ' + data[1];
                }
            }),
            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                tableClass: 'table'
            })
        }
    };

}());


function datatableActionClick(elem, buttonIdx)
{
    $(elem).parents('.portlet').find('table').DataTable().button('0-' + buttonIdx).trigger();
}




function makeSelectReadOnly(id)
{
    var $dropDown = $('#' + id),
    name = $dropDown.prop('name'),
    $form = $dropDown.closest('form');

    $dropDown.data('original-name', name);  //store the name in the data attribute 

    //disable it 
    var $hiddenInput = $('<input/>', { type: 'hidden', name: name, value: $dropDown.val() });
    $form.append($hiddenInput);  //append the hidden field with same name and value from the dropdown field 
    $dropDown.addClass('disabled').prop({ 'name': name + "_1", disabled: true }); //change name and disbale 
}



function getNullSafeDecimal(val) {
    if (isNaN(val) || val == '') {
        return 0;
    }
    return parseFloat(val);
}


function HtmlEncode(s) {
    var el = document.createElement("div");
    el.innerText = el.textContent = s;
    s = el.innerHTML;
    return s;
}

function cleanHtml(s) {
    var el = s.replace(/<\/?[^>]+(>|$)/g, "");
    el = el.replace(/<br\s*[\/]?>/gi, "");
    el = el.replace(/<\s*br[^>]?>/, "");
    el = el.replace("<br>", "");
    el = el.replace(/[*****]/g, "");
    el = el.replace(/[\n\r]/g, '');
    return el;
}

function CleanPastedHTML(input) {
    // 1. remove line breaks / Mso classes
    var stringStripper = /(\n|\r| class=(")?Mso[a-zA-Z]+(")?)/g;
    var output = input.replace(stringStripper, ' ');
    // 2. strip Word generated HTML comments
    var commentSripper = new RegExp('<!--(.*?)-->', 'g');
    var output = output.replace(commentSripper, '');
    var tagStripper = new RegExp('<(/)*(meta|link|span|\\?xml:|st1:|o:|font)(.*?)>', 'gi');
    // 3. remove tags leave content if any
    output = output.replace(tagStripper, '');
    // 4. Remove everything in between and including tags '<style(.)style(.)>'
    var badTags = ['style', 'script', 'applet', 'embed', 'noframes', 'noscript'];

    for (var i = 0; i < badTags.length; i++) {
        tagStripper = new RegExp('<' + badTags[i] + '.*?' + badTags[i] + '(.*?)>', 'gi');
        output = output.replace(tagStripper, '');
    }
    // 5. remove attributes ' style="..."'
    var badAttributes = ['style', 'start'];
    for (var i = 0; i < badAttributes.length; i++) {
        var attributeStripper = new RegExp(' ' + badAttributes[i] + '="(.*?)"', 'gi');
        output = output.replace(attributeStripper, '');
    }
    return output;
}

function parseWordPaste(desc) {
    var cleaned;
    var that = this;
    if (/<!--StartFragment-->([^]*)<!--EndFragment-->/.test(desc)) {
        // Pasted from word
        // Strip all normal <p> tags, replace with divs
        var result = desc.replace(/<p class="MsoNormal">([\s\S]*?)<\/p>/g, "<div>$1</div>\n");
        // Fix titles
        result = result.replace(/<p class="MsoTitle">([\s\S]*?)<\/p>/g, "## $1");

        var $desc = $('<div>' + result + '</div>');
        $desc.contents().each(function () {
            if (this.nodeType == 8) { // nodeType 8 = comments
                $(this).remove();
            }
        });
        var firstItems = $desc.find('p').filter(function () {
            return /MsoList.*?First/g.test(this.className);
        });

        var lists = [];

        firstItems.each(function () {
            lists.push($(this).nextUntil('.MsoListParagraphCxSpLast').addBack().next().addBack());
        });

        // Add lists with one item
        lists.push($desc.find(".MsoListParagraph"));

        // Change between ordered and un-ordered lists
        if (lists.length != 0) {
            lists.forEach(function (list) {
                if (list.length > 0) {
                    if (/[\s\S]*?(Symbol|Wingdings)[\s\S]*?/.test(list.html()))
                        var unordered = true;
                    else if (/[^0-9]/.test(list.text()[0]))
                        var unordered = true;

                    list.each(function () {
                        if (/[\s\S]*?level[2-9][\s\S]*/.test(this.outerHTML))
                            var nested = true;

                        var $this = $(this);
                        if (unordered)
                            var newText = $this.text().replace(/[^0-9]([\s\S]*)/, "$1");
                        else {
                            var newText = $this.text().replace(/[0-9](\.|\))([\s\S]*)/, "$2");
                        }

                        $this.html(newText);
                        if (nested) {
                            if (unordered)
                                $this.wrapInner('<ul><li>');
                            else
                                $this.wrapInner('<ol><li>');
                        }

                    });
                    list.wrapInner('<li>');
                    if (unordered)
                        list.wrapAll('<ul>');
                    else
                        list.wrapAll('<ol>');
                    // Filter to make sure that we don't unwrap nested lists
                    list.find('li').filter(function () {
                        return this.parentNode.tagName == 'P'
                    }).unwrap();
                }
            });
        }
        cleaned = $desc.html();
    } else cleaned = desc;


    cleaned = cleaned.replace(/&(lt|gt|quot);/g, function (m, p) {
        return (p == "lt") ? "<" : (p == "gt") ? ">" : "";
    });

  

    //cleaned = cleaned.replace(/<w:LsdException[^>]*?>[\s\S]*?<\/w:LsdException>/gi, function (m, p) {
    cleaned = cleaned.replace(/<w:LsdException[^>]*?>[\s\S]*?<\/w:LsdException>/gi, function (m, p) {
        return "";
    });

    cleaned = cleaned.replace(/(\r\n|\n|\r)/gm, function (m, p) {
        return "";
    });

   

    

    return cleaned;
}


function dropDownOnChangeFillAnother(sourceDDLId, targetDDLId, fillURL)
{
    $("#" + sourceDDLId).change(function () {
        var itemId = $('#' + sourceDDLId).val();

        $("#" + sourceDDLId).prop("disabled", true);

        $("#" + sourceDDLId).after("<div id='ajaxInputLoading'>&nbsp;&nbsp;&nbsp;<img id='pic1' src='#' width='22' height='22' /></div>")
        $("#pic1").attr("src", $("#BaseUrl").data("baseurl") + "/Content/loading/spinner.gif");

        $.ajax({
            url: fillURL,
            type: "GET",
            dataType: "JSON",
            data: { itemId: itemId },
            success: function (items) {
                $("#" + targetDDLId).html(""); // clear before appending new list
                $.each(items, function (i, item) {
                    $("#" + targetDDLId).append(
                        $('<option></option>').val(item.Id).html(item.ListItem));
                });

                $("#" + sourceDDLId).prop("disabled", false);
                $("#ajaxInputLoading").html('');
                $("#ajaxInputLoading").remove();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {

                $("#" + sourceDDLId).prop("disabled", false);
                $("#ajaxInputLoading").html('');
                $("#ajaxInputLoading").remove();
            }
        });

    });
}


function showGlobalModal(title, content)
{
    $('#globalModal #modalTitle').html(title);
    $('#globalModal #modalBody').html(content);
    $('#globalModal').modal('show');
}


function prepareSummerNotes()
{
    //var defaultContent = "<p><font face='Arial'></font><br></p>";
    var defaultContent = "<p style='font-family:Arial;font-size:10pt'><br></p>";
    var clean = "<p><br></p>";
    //$('textarea').each(function (i, obj) {
    $('.summernote').each(function (i, obj) {
        prepareSummerNote(obj);

        //$('.summernote').summernote('code', "<p><font face='Arial'>aas</font><br></p>");
        //var get_code = $('.summernote').summernote('code');
        console.log($(obj).summernote('code'));
        if ($(obj).summernote('code') == clean)
            $(obj).summernote('code', defaultContent);

        $(obj).css('font-family', 'Arial');
        $(obj).css('font-size', '10pt');

    });

}

function prepareSummerNote(obj)
{
    $(obj).summernote({
        height: 500,
        width: 500,
        //fontNames: ['Arial'],
        defaultFontName: 'Arial',
        fontNamesIgnoreCheck: ['Arial'],
        fontSize: 10,
        callbacks: {
            onInit: function () {
                /*
                console.log('Summernote is launched');
                console.log($($(obj).summernote('code'))[0].outerHTML);
                $("div.note-btn-group.btn-group.note-fontname > div > div > li:nth-child(1) > a").trigger('click');
                */
            },
            onKeyup: function (e) {

                var limit = 150;

                var num = e.target.innerText.length;
                /*
                console.log($($(obj).summernote('code')).text());
                console.log($($(obj).summernote('code')).text().replace(/(<([^>]+)>)/ig, "").length);

                if (num < limit) {
                    console.log('Characters : ' + num + ' of ' + limit);
                } else {
                    console.log('Characters : ' + num + ' of ' + limit);
                    
                }
                */
            },

            onPaste: function (e) {


                var thisNote = $(obj);



                var updatePastedText = function (someNote) {
                    var original = someNote.summernote('code');
                    //console.log(original);
                    var cleaned = parseWordPaste(original); //this is where to call whatever clean function you want. I have mine in a different file, called CleanPastedHTML.
                    //var cleaned = CleanPastedHTML(original); //this is where to call whatever clean function you want. I have mine in a different file, called CleanPastedHTML.
                    //console.log(cleaned);

                    //someNote.summernote('code', '').html(cleaned); //this sets the displayed content editor to the cleaned pasted code.
                    someNote.summernote('code', cleaned); //this sets the displayed content editor to the cleaned pasted code.
                };
                setTimeout(function () {
                    //this kinda sucks, but if you don't do a setTimeout, 
                    //the function is called before the text is really pasted.
                    updatePastedText(thisNote);
                }, 10);


            }


        },


        toolbar: [

                ['font', ['bold', 'underline']],
                ['fontname', ['fontname']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen' /*, 'codeview', 'help'*/]]
              ],

        //modules: $.extend($.summernote.options.modules, { "autolink": AutoLink })



    });
}


function qTipWarning(locationName, warningType, sensorType) {
    var areaName = !isBlank(locationName) ? ('area[alt^="' + locationName + '"]') : 'area';

    //console.log(locationName + " " + areaName);

    //var qTipShow = { when: { event: 'mouseover' }, ready: true };
    var qTipShow = { when: { event: 'mouseover' } };
    var qTipHide = { when: { event: 'mouseout' } };

    if (warningType != -1) {

        qTipShow = {
            when: false, // Don't specify a show event
            ready: true // Show the tooltip when ready
        };

        qTipHide = false;
    }


    /* http://qtip2.com/options#position.basics */

    $(areaName).each(function () {
        var altText = $(this).attr('alt');
        var res = altText.split("#");
        //console.log(res);

        var qTipText = (warningType == -1 ? res[0] : "<div class='panel-left' style='" + "text-align:center;color:" + (warningType == 5 ? "orange" : (warningType == 3 ? "white" : ((warningType == 0 || warningType == 4) ? "black" : ((warningType == PARAM_ITEM_WARNING_TYPE_ALARM) ? "red" : "yellow")))) + "'><i class='" + (warningType == 3 ? "wi wi-snowflake-cold wi-2x" : (warningType == PARAM_ITEM_WARNING_TYPE_WARNING ? "fa fa-exclamation-triangle" : "fa fa-bell")) + "' style='font-size:24px;' ></i></div>");  // fa fa-bell fa-2x

        //console.log($(areaName));
        if ($(this) != undefined && $(this).length == 1)
        {
            $(this).qtip({
                content: {
                    text: qTipText
                },
                style: {
                    classes: 'qtip-bootstrap'
                    /*,height: 35,
                    margin: 0,
                    padding:0*/
                },

                show: qTipShow,
                hide: qTipHide,

                position: {
                    /*
                    my: 'right center',  // position of the triangle that is on the tip
                    at: 'left center'  // tip's position according to the polygon */
                    my: res[1],
                    at: res[2]
                }
            });
        }
        

    });
}

function maphilightWarning(locationName, warningType) {
    var areaName = !isBlank(locationName) ? ('area[alt^="' + locationName + '"]') : 'area';

    var data = {};
    data.alwaysOn = (warningType != -1);
    data.fillColor = '000000';
    data.fillOpacity = '0.5';
    //$('area[alt="156"]').data('maphilight', data).trigger('alwaysOn.maphilight');
    //console.log($(areaName).length > 0);
    if ($(areaName) != undefined && $(areaName).length > 0)
        $(areaName).data('maphilight', data).trigger('alwaysOn.maphilight');

}

function isBlank(str) {
    return (!str || /^\s*$/.test(str));
}


function showJqueryUIPopup(link)
{
    var w = $(window).width() * 0.9;
    var h = $(window).height() * 0.9;

    var $dialog2 = $("<div style='text-align: center;height: 100%;'> <img src='/Content/loading/spinner.gif' width='30'></div>")
    .addClass("dialog")
    .attr("id", $(this)
    .attr("data-dialog-id"))
    .appendTo("body")
    .dialog({
        //title: $(this).attr("data-dialog-title"),
        title: "Mahal No : " ,
        //open: function (event, ui) { setTimeout(function () { $(this).dialog("close"); }, 1000) },
        close: function () { $(this).remove(); },
        modal: true,
        height: h,
        //maxHeight: window.innerHeight - 15,
        overflow: 'scroll',
        width: w,
        left: 0

    })
    .load(link);
}

// Constants 

var PARAM_ITEM_WARNING_TYPE_WARNING = 61;
var PARAM_ITEM_WARNING_TYPE_ALARM = 62;

var LOADING_ENABLED_ON_AJAX = true;

//var momentDateFormat = "MM/DD/YYYY";
//var momentDateFormat = "DD.MM.YYYY";
var momentDateFormat = "DD-MM-YYYY";
var AmCharts_path = "/Content/assets/global/plugins/amcharts/amcharts/";

