/// <reference path="../knockout-2.2.0.debug.js" />
/// <reference path="../jquery-1.8.3.intellisense.js" />
/// <reference path="../jquery.signalR-1.0.0-rc1.js" />

(function () {

    function Message(from, msg, datetime, isPrivate) {
        this.from = ko.observable(from);
        this.message = ko.observable(msg);
        this.datetime = ko.observable(datetime);
        this.isPrivate = ko.observable(isPrivate);
    }

    function User(name) {

        var self = this;

        self.name = ko.observable(name);
        self.isPrivateChatUser = ko.observable(false);
        self.setAsPrivateChat = function (user) {
            viewModel.privateChatUser(user.name());
            viewModel.isInPrivateChat(true);
            $.each(viewModel.users(), function (i, user) {
                user.isPrivateChatUser(false);
            });
            self.isPrivateChatUser(true);
        };
    }

    var viewModel = {
        messages: ko.observableArray([]),
        logs: ko.observableArray([]),
        users: ko.observableArray([]),
        isInPrivateChat: ko.observable(false),
        privateChatUser: ko.observable(),
        exitFromPrivateChat: function () {

            viewModel.isInPrivateChat(false);
            viewModel.privateChatUser(null);
            $.each(viewModel.users(), function (i, user) {
                user.isPrivateChatUser(false);
            });
        }
    };

    $(function () {

        var chatHub = $.connection.chatHub,
            loginHub = $.connection.login,
            $sendBtn = $('#btnSend'),
            $btnDownloadApk = $('#btnDownloadApk'),
            $btnInstallApk = $('#btnInstallApk'),
            $btnUnInstallApk = $('#btnUnInstallApk'),
            $btnInstallFromMarket = $('#btnInstallFromMarket'),
            $btnCloseApplication = $('#btnCloseApplication'),
            $btnRestartAssetHealth = $('#btnRestartAssetHealth'),
            $btnTakeScreenshot = $('#btnTakeScreenshot'),
            $btnUploadDB = $('#btnUploadDB'),
            $btnUploadLogAndDatabase = $('#btnUploadLogAndDatabase'),
            $btnDownloadDB = $('#btnDownloadDB'),
            $btnSendLinuxCommand = $('#btnSendLinuxCommand'),
            $btnDeleteFile = $('#btnDeleteFile'),
            $btnDeleteFilesLogs = $('#btnDeleteFilesLogs'),
            $btnDeleteFilesApks = $('#btnDeleteFilesApks'),
            $btnCopyFile = $('#btnCopyFile'),
            $btnCopyDBFromNoxShare = $('#btnCopyDBFromNoxShare'),
            $btnZipFiles = $('#btnZipFiles'),

            $btnGetAppPath = $('#btnGetAppPath'),
            $btnGetUDID = $('#btnGetUDID'),
            $btnListFiles = $('#btnListFiles'),
            $btnListFilesFilter = $('#btnListFilesFilter'),
            $btnGetLogCat = $('#btnGetLogCat'),

            $btnStartLogCat = $('#btnStartLogCat'),
            $btnStopLogCat = $('#btnStopLogCat'),
            
            $btnClearMessages = $('#btnClearMessages'),
            $btnClearLogs = $('#btnClearLogs'),
            
            
            $msgTxt = $('#txtMsg');

        // turn the logging on for demo purposes
        $.connection.hub.logging = true;

        chatHub.client.received = function (message) {
            if (message.message.startsWith("#LOG#")) {
                viewModel.logs.push(new Message(message.sender, message.message, message.datetime, message.isPrivate));

                $('#logs').scrollTop($('#logs')[0].scrollHeight);
            }
            else {
                viewModel.messages.push(new Message(message.sender, message.message, message.datetime, message.isPrivate));
                $('#messages').scrollTop($('#messages')[0].scrollHeight);
            }
            
        };

        chatHub.client.userConnected = function (username) {
            viewModel.users.push(new User(username));
        };

        chatHub.client.userDisconnected = function (username) {
            viewModel.users.pop(new User(username));
            if (viewModel.isInPrivateChat() && viewModel.privateChatUser() === username) {
                viewModel.isInPrivateChat(false);
                viewModel.privateChatUser(null);
            }
        };

        // $.connection.hub.starting(callback)
        // there is also $.connection.hub.(received|stateChanged|error|disconnected|connectionSlow|reconnected)

        // $($.connection.hub).bind($.signalR.events.onStart, callback)

        // $.connection.hub.error(function () {
        //     console.log("foo");
        // });

        startConnection();
        ko.applyBindings(viewModel);

        function startConnection() {

            $.connection.hub.start().done(function () {

                toggleInputs(false);
                bindClickEvents();

                $msgTxt.focus();

                chatHub.server.getConnectedUsers().done(function (users) {
                    $.each(users, function (i, username) {
                        viewModel.users.push(new User(username));
                    });
                });

            }).fail(function (err) {

                console.log(err);
            });
        }

        function bindClickEvents() {

            $msgTxt.keypress(function (e) {
                var code = (e.keyCode ? e.keyCode : e.which);
                if (code === 13) {
                    sendMessage();
                }
            });

            $sendBtn.click(function (e) {

                sendMessage();
                e.preventDefault();
            });

            $btnDownloadApk.click(function (e) {

                $msgTxt.val('#CMD#DWN#com.improvative.assethealth-1000001.apk');
                e.preventDefault();
            });
            
            $btnInstallApk.click(function (e) {

                $msgTxt.val('#CMD#INS#com.improvative.assethealth-1000001.apk');
                e.preventDefault();
            });
            $btnUnInstallApk.click(function (e) {

                $msgTxt.val('#CMD#UNS#com.improvative.assethealth');
                e.preventDefault();
            });
            $btnInstallFromMarket.click(function (e) {

                $msgTxt.val('#CMD#INM#com.teamviewer.quicksupport.market');
                e.preventDefault();
            });
            $btnCloseApplication.click(function (e) {

                $msgTxt.val('#CMD#CLS#com.improvative.assethealth');
                e.preventDefault();
            });
            $btnRestartAssetHealth.click(function (e) {

                $msgTxt.val('#CMD#RST#com.improvative.assethealth');
                e.preventDefault();
            });
            $btnTakeScreenshot.click(function (e) {

                $msgTxt.val('#CMD#USC#');
                e.preventDefault();
            });
            $btnUploadDB.click(function (e) {

                $msgTxt.val('#CMD#UFL#assethealth-db');
                e.preventDefault();
            });
            $btnUploadLogAndDatabase.click(function (e) {

                $msgTxt.val('#CMD#ULD#');
                e.preventDefault();
            });
            
            $btnDownloadDB.click(function (e) {

                $msgTxt.val('#CMD#DWN#assethealth-db');
                e.preventDefault();
            });
            $btnSendLinuxCommand.click(function (e) {

                $msgTxt.val('#CMD#PRC#');
                e.preventDefault();
            });
            

            $btnCopyFile.click(function (e) {

                $msgTxt.val('#CMD#CPY#/storage/emulated/0/Download/assethealth/assethealth-db#/mnt/shared/Other/assethealth-db');
                e.preventDefault();
            });
            
            $btnCopyDBFromNoxShare.click(function (e) {

                $msgTxt.val('#CMD#CPY#/mnt/shared/Other/assethealth-db_test#/storage/emulated/0/Download/assethealth/assethealth-db');
                e.preventDefault();
            });

            $btnDeleteFilesLogs.click(function (e) {
                $msgTxt.val('#CMD#DLT#^.*\\.log.*$');
                e.preventDefault();
            });
            $btnDeleteFilesApks.click(function (e) {
                $msgTxt.val('#CMD#DLT#^.*\\.apk.*$');
                e.preventDefault();
            });
            
            $btnDeleteFile.click(function (e) {

                $msgTxt.val('#CMD#DLT#com.improvative.assethealth-1000001.apk');
                e.preventDefault();
            });
            
            $btnGetAppPath.click(function (e) {

                $msgTxt.val('#CMD#IAP#');
                e.preventDefault();
            });
            $btnGetUDID.click(function (e) {

                $msgTxt.val('#CMD#IID#');
                e.preventDefault();
            });
            $btnListFiles.click(function (e) {

                $msgTxt.val('#CMD#LFD#^.*$');
                e.preventDefault();
            });
            $btnListFilesFilter.click(function (e) {

                $msgTxt.val('#CMD#LFD#^.*\\.log.*$');
                e.preventDefault();
            });
            $btnGetLogCat.click(function (e) {

                $msgTxt.val('#CMD#LOG#');
                e.preventDefault();
            });
            $btnZipFiles.click(function (e) {

                $msgTxt.val('#CMD#ZIP#^.*\\.log.*$#logs.zip');
                e.preventDefault();
            });
            
            
            
            $btnClearMessages.click(function (e) {

                document.getElementById("messages").innerHTML = "";
                e.preventDefault();
            });


            /////////

            $btnStartLogCat.click(function (e) {

                sendMessage('#CMD#PRC#logcat');
                e.preventDefault();
            });

            $btnStopLogCat.click(function (e) {

                sendMessage('#CMD#STL#');
                e.preventDefault();
            });

            $btnClearLogs.click(function (e) {

                document.getElementById("logs").innerHTML = "";
                e.preventDefault();
            });
            
            
            
            
            
        }

        function sendMessage(msg) {

            var msgValue = "";

            if (msg === undefined)
                msgValue = $msgTxt.val();
            else
                msgValue = msg;

            if (msgValue !== null && msgValue.length > 0) {

                if (viewModel.isInPrivateChat()) {

                    chatHub.server.send(msgValue, viewModel.privateChatUser()).fail(function (err) {
                        console.log('Send method failed: ' + err);
                    });
                }
                else {
                    chatHub.server.send(msgValue).fail(function (err) {
                        console.log('Send method failed: ' + err);
                    });
                }
            }

            $msgTxt.val(null);
            $msgTxt.focus();
        }

        function toggleInputs(status) {

            $sendBtn.prop('disabled', status);
            $msgTxt.prop('disabled', status);
        }
    });
}());