﻿using Ares.Core.Caching;
using Ares.Core.Framework;
using Ares.Sync.Svc;
using Ares.UMS.Svc.Configuration;
using Ares.UMS.Svc.Messages;
using Ares.UMS.Svc.Security;
using Ares.UMS.Svc.Tasks;
using Ares.UMS.Svc.Users;
using AssetHealth.Common.Svc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetHealth.BackgroundTask
{
	public class CacheRefreshTask : ITask
	{
		/// <summary>
		/// Executes a task
		/// </summary>
        public void Execute()
		{
			var cacheManager = FrameworkCtx.Current.Resolve<ICacheManager>();
			cacheManager.Clear();

			IRoleSvc roleSvc = FrameworkCtx.Current.Resolve<IRoleSvc>();
			roleSvc.GetAllFromCache();

			IPermissionSvc permissionSvc = FrameworkCtx.Current.Resolve<IPermissionSvc>();
			permissionSvc.GetAllFromCache();

			IDeviceSvc deviceSvc = FrameworkCtx.Current.Resolve<IDeviceSvc>();
			deviceSvc.GetAllFromCache();

			IEqLocationSvc eqLocationSvc = FrameworkCtx.Current.Resolve<IEqLocationSvc>();
			eqLocationSvc.GetAllFromCache();

			IEquipmentSvc equipmentSvc = FrameworkCtx.Current.Resolve<IEquipmentSvc>();
			equipmentSvc.GetAllFromCache();

			IEqChannelSvc eqChannelSvc = FrameworkCtx.Current.Resolve<IEqChannelSvc>();
			eqChannelSvc.GetAllFromCache();

			IEqThresholdSvc eqThresholdSvc = FrameworkCtx.Current.Resolve<IEqThresholdSvc>();
			eqThresholdSvc.GetAllFromCache();

			IFormSvc formSvc = FrameworkCtx.Current.Resolve<IFormSvc>();
			formSvc.GetAllFromCache();

			IFormQuestionSvc formQuestionSvc = FrameworkCtx.Current.Resolve<IFormQuestionSvc>();
			formQuestionSvc.GetAllFromCache();

			IMenuSvc menuSvc = FrameworkCtx.Current.Resolve<IMenuSvc>();
			menuSvc.GetAllFromCache();

			IMessageTemplateSvc meessageTemplateSvc = FrameworkCtx.Current.Resolve<IMessageTemplateSvc>();
			meessageTemplateSvc.GetAllFromCache();

			IParameterGroupSvc parameterGroupSvc = FrameworkCtx.Current.Resolve<IParameterGroupSvc>();
			parameterGroupSvc.GetAllFromCache();

			IParameterItemSvc parameterItemSvc = FrameworkCtx.Current.Resolve<IParameterItemSvc>();
			parameterItemSvc.GetAllFromCache();
			


        }
    }
}