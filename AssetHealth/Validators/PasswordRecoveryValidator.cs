﻿using Ares.UMS.Svc.Localization;
using Ares.Web.Validators;
using AssetHealth.Models.User;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetHealth.Validators
{
    public class PasswordRecoveryValidator : BaseAresValidator<PasswordRecoveryModel>
    {
        public PasswordRecoveryValidator(ILocalizationSvc localizationService)
        {
            RuleFor(x => x.Email).NotEmpty().WithMessage(localizationService.GetResource("User.PasswordRecovery.Email.Required"));
            RuleFor(x => x.Email).EmailAddress().WithMessage(localizationService.GetResource("Admin.Common.WrongEmail"));
        }
    }
}
