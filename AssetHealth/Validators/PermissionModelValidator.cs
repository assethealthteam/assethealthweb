﻿using FluentValidation;
using AssetHealth.Models.Configuration;
using AssetHealth.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.Web.Validators;
using Ares.UMS.Svc.Localization;

namespace AssetHealth.Validators
{
    public class PermissionModelValidator : BaseAresValidator<PermissionModel>
    {
        public PermissionModelValidator(ILocalizationSvc localizationService)
        {
            RuleFor(x => x.PermissionName).NotNull(); 
            RuleFor(x => x.Systemname).NotNull();
            RuleFor(x => x.Category).NotNull();

            //RuleFor(y => y.ValDouble1).InclusiveBetween(40.0, 50.0);

            //RuleFor(customer => customer.ApplicationSubmitted).NotEmpty().When(customer => customer.SelectedApplicationStatus > 0);
        }
    }
}