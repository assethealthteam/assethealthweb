﻿using FluentValidation;
using AssetHealth.Models.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetHealth.Validators
{
    public class MenuModelValidator : AbstractValidator<MenuModel>
    {
        public MenuModelValidator()
        {
            //RuleFor(x => x.Menuname).NotNull().WithMessage("").Length(2,50).WithMessage("");
            RuleFor(x => x.Menuname).NotNull().Length(2, 50);
            RuleFor(x => x.Title).NotNull(); 
             
            //RuleFor(y => y.ValDouble1).InclusiveBetween(40.0, 50.0);

            //RuleFor(customer => customer.ApplicationSubmitted).NotEmpty().When(customer => customer.SelectedApplicationStatus > 0);
        }
    }
}