﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using System.Web.Mvc;
using FluentValidation;
using AssetHealth.Models.Form_question;
using Ares.UMS.Svc.Localization;
using Ares.Web.Validators;


namespace AssetHealth.Validators
{
    public partial class FormQuestionModelValidator : BaseAresValidator<FormQuestionModel>
    {
        public FormQuestionModelValidator(ILocalizationSvc _localizationSvc)
        {
		
            RuleFor(x => x.Form_id).NotNull().WithMessage(_localizationSvc.GetResource("FormQuestionModelValidator.Form_id.NotNull"));
            RuleFor(x => x.Title).NotNull().WithMessage(_localizationSvc.GetResource("FormQuestionModelValidator.Title.NotNull")).Length(1, 3000).WithMessage(_localizationSvc.GetResource("FormQuestionModelValidator.Title.NotNull"));
            RuleFor(x => x.Hint).Length(1, 1000).WithMessage(_localizationSvc.GetResource("FormQuestionModelValidator.Hint.Validator"));
            RuleFor(x => x.Description).Length(1, 1000).WithMessage(_localizationSvc.GetResource("FormQuestionModelValidator.Description.Validator"));
            RuleFor(x => x.Image_path).Length(1, 1000).WithMessage(_localizationSvc.GetResource("FormQuestionModelValidator.Image_path.Validator"));
            RuleFor(x => x.Device_image_path).Length(1, 1000).WithMessage(_localizationSvc.GetResource("FormQuestionModelValidator.Device_image_path.Validator"));
            RuleFor(x => x.V_email).EmailAddress().Length(1, 100).WithMessage(_localizationSvc.GetResource("FormQuestionModelValidator.V_email.Validator"));
            RuleFor(x => x.V_regex).Length(1, 2000).WithMessage(_localizationSvc.GetResource("FormQuestionModelValidator.V_regex.Validator"));
            RuleFor(x => x.Active).NotNull().WithMessage(_localizationSvc.GetResource("FormQuestionModelValidator.Active.NotNull"));
            RuleFor(x => x.Deleted).NotNull().WithMessage(_localizationSvc.GetResource("FormQuestionModelValidator.Deleted.NotNull"));
        }

        
    }


}