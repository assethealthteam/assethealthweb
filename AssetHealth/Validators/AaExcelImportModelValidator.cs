﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using System.Web.Mvc;
using FluentValidation;
using AssetHealth.Models.Aa_excel_import;
using Ares.UMS.Svc.Localization;
using Ares.Web.Validators;


namespace AssetHealth.Validators
{
    public partial class AaExcelImportModelValidator : BaseAresValidator<AaExcelImportModel>
    {
        public AaExcelImportModelValidator(ILocalizationSvc _localizationSvc)
        {
		
            RuleFor(x => x.Location_name).Length(1, 300).WithMessage(_localizationSvc.GetResource("AaExcelImportModelValidator.Location_name.Validator"));
            RuleFor(x => x.Sublocation_name).Length(1, 300).WithMessage(_localizationSvc.GetResource("AaExcelImportModelValidator.Sublocation_name.Validator"));
            RuleFor(x => x.Equipment_name).Length(1, 300).WithMessage(_localizationSvc.GetResource("AaExcelImportModelValidator.Equipment_name.Validator"));
            RuleFor(x => x.Period).Length(1, 300).WithMessage(_localizationSvc.GetResource("AaExcelImportModelValidator.Period.Validator"));
            RuleFor(x => x.Category).Length(1, 300).WithMessage(_localizationSvc.GetResource("AaExcelImportModelValidator.Category.Validator"));
            RuleFor(x => x.Question_type).Length(1, 300).WithMessage(_localizationSvc.GetResource("AaExcelImportModelValidator.Question_type.Validator"));
            RuleFor(x => x.Code).Length(1, 300).WithMessage(_localizationSvc.GetResource("AaExcelImportModelValidator.Code.Validator"));
            RuleFor(x => x.Title).Length(1, 3000).WithMessage(_localizationSvc.GetResource("AaExcelImportModelValidator.Title.Validator"));
            RuleFor(x => x.Hint).Length(1, 300).WithMessage(_localizationSvc.GetResource("AaExcelImportModelValidator.Hint.Validator"));
            RuleFor(x => x.Display_order).Length(1, 300).WithMessage(_localizationSvc.GetResource("AaExcelImportModelValidator.Display_order.Validator"));
            RuleFor(x => x.Unhealthy_cause).Length(1, 300).WithMessage(_localizationSvc.GetResource("AaExcelImportModelValidator.Unhealthy_cause.Validator"));
            RuleFor(x => x.Threshold_repetition_count).Length(1, 300).WithMessage(_localizationSvc.GetResource("AaExcelImportModelValidator.Threshold_repetition_count.Validator"));
            RuleFor(x => x.Answer_deadline_day_count).Length(1, 300).WithMessage(_localizationSvc.GetResource("AaExcelImportModelValidator.Answer_deadline_day_count.Validator"));
            RuleFor(x => x.Answer_deadline_hour_count).Length(1, 300).WithMessage(_localizationSvc.GetResource("AaExcelImportModelValidator.Answer_deadline_hour_count.Validator"));
            RuleFor(x => x.Criticality).Length(1, 300).WithMessage(_localizationSvc.GetResource("AaExcelImportModelValidator.Criticality.Validator"));
            RuleFor(x => x.Is_deleted).Length(1, 300).WithMessage(_localizationSvc.GetResource("AaExcelImportModelValidator.Is_deleted.Validator"));
        }

        
    }


}