﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using System.Web.Mvc;
using FluentValidation;
using AssetHealth.Models.Form_answer;
using Ares.UMS.Svc.Localization;
using Ares.Web.Validators;


namespace AssetHealth.Validators
{
    public partial class FormAnswerModelValidator : BaseAresValidator<FormAnswerModel>
    {
        public FormAnswerModelValidator(ILocalizationSvc _localizationSvc)
        {
		
            RuleFor(x => x.Form_question_id).NotNull().WithMessage(_localizationSvc.GetResource("FormAnswerModelValidator.Form_question_id.NotNull"));
            RuleFor(x => x.Username).Length(1, 250).WithMessage(_localizationSvc.GetResource("FormAnswerModelValidator.Username.Validator"));
            RuleFor(x => x.Answer).NotNull().WithMessage(_localizationSvc.GetResource("FormAnswerModelValidator.Answer.NotNull")).Length(1, 3000).WithMessage(_localizationSvc.GetResource("FormAnswerModelValidator.Answer.NotNull"));
            RuleFor(x => x.Converted_answer).Length(1, 1000).WithMessage(_localizationSvc.GetResource("FormAnswerModelValidator.Converted_answer.Validator"));
            RuleFor(x => x.Description).Length(1, 2000).WithMessage(_localizationSvc.GetResource("FormAnswerModelValidator.Description.Validator"));
            RuleFor(x => x.Udid).Length(1, 250).WithMessage(_localizationSvc.GetResource("FormAnswerModelValidator.Udid.Validator"));
        }

        
    }


}