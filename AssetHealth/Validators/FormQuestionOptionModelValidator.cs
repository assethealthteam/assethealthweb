﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using System.Web.Mvc;
using FluentValidation;
using AssetHealth.Models.Form_question_option;
using Ares.UMS.Svc.Localization;
using Ares.Web.Validators;


namespace AssetHealth.Validators
{
    public partial class FormQuestionOptionModelValidator : BaseAresValidator<FormQuestionOptionModel>
    {
        public FormQuestionOptionModelValidator(ILocalizationSvc _localizationSvc)
        {
		
            RuleFor(x => x.Form_question_id).NotNull().WithMessage(_localizationSvc.GetResource("FormQuestionOptionModelValidator.Form_question_id.NotNull"));
            RuleFor(x => x.Code).NotNull().WithMessage(_localizationSvc.GetResource("FormQuestionOptionModelValidator.Code.NotNull")).Length(1, 30).WithMessage(_localizationSvc.GetResource("FormQuestionOptionModelValidator.Code.NotNull"));
            RuleFor(x => x.Title).NotNull().WithMessage(_localizationSvc.GetResource("FormQuestionOptionModelValidator.Title.NotNull")).Length(1, 3000).WithMessage(_localizationSvc.GetResource("FormQuestionOptionModelValidator.Title.NotNull"));
            RuleFor(x => x.Hint).Length(1, 1000).WithMessage(_localizationSvc.GetResource("FormQuestionOptionModelValidator.Hint.Validator"));
            RuleFor(x => x.Description).Length(1, 1000).WithMessage(_localizationSvc.GetResource("FormQuestionOptionModelValidator.Description.Validator"));
            RuleFor(x => x.Image_path).Length(1, 1000).WithMessage(_localizationSvc.GetResource("FormQuestionOptionModelValidator.Image_path.Validator"));
            RuleFor(x => x.Device_image_path).Length(1, 1000).WithMessage(_localizationSvc.GetResource("FormQuestionOptionModelValidator.Device_image_path.Validator"));
            RuleFor(x => x.Video_path).Length(1, 1000).WithMessage(_localizationSvc.GetResource("FormQuestionOptionModelValidator.Video_path.Validator"));
            RuleFor(x => x.Device_video_path).Length(1, 1000).WithMessage(_localizationSvc.GetResource("FormQuestionOptionModelValidator.Device_video_path.Validator"));
            RuleFor(x => x.Period).Length(1, 500).WithMessage(_localizationSvc.GetResource("FormQuestionOptionModelValidator.Period.Validator"));
            RuleFor(x => x.Active).NotNull().WithMessage(_localizationSvc.GetResource("FormQuestionOptionModelValidator.Active.NotNull"));
            RuleFor(x => x.Deleted).NotNull().WithMessage(_localizationSvc.GetResource("FormQuestionOptionModelValidator.Deleted.NotNull"));
        }

        
    }


}