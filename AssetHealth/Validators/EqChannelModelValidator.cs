﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using System.Web.Mvc;
using FluentValidation;
using AssetHealth.Models.Eq_channel;
using Ares.UMS.Svc.Localization;
using Ares.Web.Validators;


namespace AssetHealth.Validators
{
    public partial class EqChannelModelValidator : BaseAresValidator<EqChannelModel>
    {
        public EqChannelModelValidator(ILocalizationSvc _localizationSvc)
        {
		
            RuleFor(x => x.Equipment_id).NotNull().WithMessage(_localizationSvc.GetResource("EqChannelModelValidator.Equipment_id.NotNull"));
            RuleFor(x => x.Cid).Length(1, 150).WithMessage(_localizationSvc.GetResource("EqChannelModelValidator.Cid.Validator"));
        }

        
    }


}