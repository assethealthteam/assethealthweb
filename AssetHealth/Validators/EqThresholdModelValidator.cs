﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using System.Web.Mvc;
using FluentValidation;
using AssetHealth.Models.Eq_threshold;
using Ares.UMS.Svc.Localization;
using Ares.Web.Validators;


namespace AssetHealth.Validators
{
    public partial class EqThresholdModelValidator : BaseAresValidator<EqThresholdModel>
    {
        public EqThresholdModelValidator(ILocalizationSvc _localizationSvc)
        {
		
            RuleFor(x => x.Eq_channel_id).NotNull().WithMessage(_localizationSvc.GetResource("EqThresholdModelValidator.Eq_channel_id.NotNull"));
        }

        
    }


}