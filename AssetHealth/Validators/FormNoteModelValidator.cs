﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using System.Web.Mvc;
using FluentValidation;
using AssetHealth.Models.Form_notes;
using Ares.UMS.Svc.Localization;
using Ares.Web.Validators;


namespace AssetHealth.Validators
{
    public partial class FormNoteModelValidator : BaseAresValidator<FormNoteModel>
    {
        public FormNoteModelValidator(ILocalizationSvc _localizationSvc)
        {
		
            RuleFor(x => x.Form_answer_round_id).NotNull().WithMessage(_localizationSvc.GetResource("FormNoteModelValidator.Form_answer_round_id.NotNull"));
            RuleFor(x => x.Username).Length(1, 250).WithMessage(_localizationSvc.GetResource("FormNoteModelValidator.Username.Validator"));
            RuleFor(x => x.Title).NotNull().WithMessage(_localizationSvc.GetResource("FormNoteModelValidator.Title.NotNull")).Length(1, 250).WithMessage(_localizationSvc.GetResource("FormNoteModelValidator.Title.NotNull"));
            RuleFor(x => x.Description).Length(1, 2000).WithMessage(_localizationSvc.GetResource("FormNoteModelValidator.Description.Validator"));
        }

        
    }


}