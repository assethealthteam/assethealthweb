﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using System.Web.Mvc;
using FluentValidation;
using AssetHealth.Models.Eq_warning;
using Ares.UMS.Svc.Localization;
using Ares.Web.Validators;


namespace AssetHealth.Validators
{
    public partial class EqWarningModelValidator : BaseAresValidator<EqWarningModel>
    {
        public EqWarningModelValidator(ILocalizationSvc _localizationSvc)
        {
		
            RuleFor(x => x.Last_stop_datetime).Length(1, 150).WithMessage(_localizationSvc.GetResource("EqWarningModelValidator.Last_stop_datetime.Validator"));
        }

        
    }


}