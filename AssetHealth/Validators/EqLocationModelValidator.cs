﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using System.Web.Mvc;
using FluentValidation;
using AssetHealth.Models.Eq_location;
using Ares.UMS.Svc.Localization;
using Ares.Web.Validators;


namespace AssetHealth.Validators
{
    public partial class EqLocationModelValidator : BaseAresValidator<EqLocationModel>
    {
        public EqLocationModelValidator(ILocalizationSvc _localizationSvc)
        {
		
            RuleFor(x => x.Location_name).Length(1, 250).WithMessage(_localizationSvc.GetResource("EqLocationModelValidator.Location_name.Validator"));
            RuleFor(x => x.Cid).Length(1, 150).WithMessage(_localizationSvc.GetResource("EqLocationModelValidator.Cid.Validator"));
            RuleFor(x => x.Schema_path).Length(1, 250).WithMessage(_localizationSvc.GetResource("EqLocationModelValidator.Schema_path.Validator"));
            RuleFor(x => x.Latitude).Length(1, 75).WithMessage(_localizationSvc.GetResource("EqLocationModelValidator.Latitude.Validator"));
            RuleFor(x => x.Longitude).Length(1, 75).WithMessage(_localizationSvc.GetResource("EqLocationModelValidator.Longitude.Validator"));
        }

        
    }


}