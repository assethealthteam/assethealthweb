﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using System.Web.Mvc;
using FluentValidation;
using AssetHealth.Models.Form;
using Ares.UMS.Svc.Localization;
using Ares.Web.Validators;


namespace AssetHealth.Validators
{
    public partial class FormModelValidator : BaseAresValidator<FormModel>
    {
        public FormModelValidator(ILocalizationSvc _localizationSvc)
        {
		
            RuleFor(x => x.Code).Length(1, 150).WithMessage(_localizationSvc.GetResource("FormModelValidator.Code.Validator"));
            RuleFor(x => x.Title).NotNull().WithMessage(_localizationSvc.GetResource("FormModelValidator.Title.NotNull")).Length(1, 3000).WithMessage(_localizationSvc.GetResource("FormModelValidator.Title.NotNull"));
            RuleFor(x => x.Description).Length(1, 3000).WithMessage(_localizationSvc.GetResource("FormModelValidator.Description.Validator"));
            RuleFor(x => x.Image_path).Length(1, 3000).WithMessage(_localizationSvc.GetResource("FormModelValidator.Image_path.Validator"));
        }

        
    }


}