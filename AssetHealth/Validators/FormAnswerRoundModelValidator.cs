﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using System.Web.Mvc;
using FluentValidation;
using AssetHealth.Models.Form_answer_round;
using Ares.UMS.Svc.Localization;
using Ares.Web.Validators;


namespace AssetHealth.Validators
{
    public partial class FormAnswerRoundModelValidator : BaseAresValidator<FormAnswerRoundModel>
    {
        public FormAnswerRoundModelValidator(ILocalizationSvc _localizationSvc)
        {
		
            RuleFor(x => x.Object_id).NotNull().WithMessage(_localizationSvc.GetResource("FormAnswerRoundModelValidator.Object_id.NotNull")).Length(1, 150).WithMessage(_localizationSvc.GetResource("FormAnswerRoundModelValidator.Object_id.NotNull"));
            RuleFor(x => x.Username).Length(1, 250).WithMessage(_localizationSvc.GetResource("FormAnswerRoundModelValidator.Username.Validator"));
            RuleFor(x => x.Description).Length(1, 2000).WithMessage(_localizationSvc.GetResource("FormAnswerRoundModelValidator.Description.Validator"));
        }

        
    }


}