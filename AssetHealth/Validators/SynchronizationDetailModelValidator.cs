﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using System.Web.Mvc;
using FluentValidation;
using AssetHealth.Models.Synchronization_detail;
using Ares.UMS.Svc.Localization;
using Ares.Web.Validators;


namespace AssetHealth.Validators
{
    public partial class SynchronizationDetailModelValidator : BaseAresValidator<SynchronizationDetailModel>
    {
        public SynchronizationDetailModelValidator(ILocalizationSvc _localizationSvc)
        {
		
            RuleFor(x => x.Synchronization_id).NotNull().WithMessage(_localizationSvc.GetResource("SynchronizationDetailModelValidator.Synchronization_id.NotNull"));
            RuleFor(x => x.Table_name).NotNull().WithMessage(_localizationSvc.GetResource("SynchronizationDetailModelValidator.Table_name.NotNull")).Length(1, 150).WithMessage(_localizationSvc.GetResource("SynchronizationDetailModelValidator.Table_name.NotNull"));
            RuleFor(x => x.Active).NotNull().WithMessage(_localizationSvc.GetResource("SynchronizationDetailModelValidator.Active.NotNull"));
        }

        
    }


}