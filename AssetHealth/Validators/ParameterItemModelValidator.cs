﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using System.Web.Mvc;
using FluentValidation;
using Ares.UMS.Svc.Localization;
using Ares.Web.Validators;
using Ares.Web.Models.Parameter_item;

namespace AssetHealth.Validators
{
    public partial class ParameterItemModelValidator : BaseAresValidator<ParameterItemModel>
    {
        public ParameterItemModelValidator(ILocalizationSvc _localizationSvc)
        {
		
            RuleFor(x => x.Parametergroup_id).NotNull().WithMessage(_localizationSvc.GetResource("ParameterItemModelValidator.Parametergroup_id.NotNull"));
            RuleFor(x => x.Code).NotNull().WithMessage(_localizationSvc.GetResource("ParameterItemModelValidator.Code.NotNull")).Length(1, 30).WithMessage(_localizationSvc.GetResource("ParameterItemModelValidator.Code.NotNull"));
            RuleFor(x => x.Title).NotNull().WithMessage(_localizationSvc.GetResource("ParameterItemModelValidator.Title.NotNull")).Length(1, 3000).WithMessage(_localizationSvc.GetResource("ParameterItemModelValidator.Title.NotNull"));
            RuleFor(x => x.Description).Length(1, 1000).WithMessage(_localizationSvc.GetResource("ParameterItemModelValidator.Description.Validator"));
            RuleFor(x => x.Active).NotNull().WithMessage(_localizationSvc.GetResource("ParameterItemModelValidator.Active.NotNull"));
        }

        
    }


}