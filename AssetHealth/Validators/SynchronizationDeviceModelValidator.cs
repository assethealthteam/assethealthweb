﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using System.Web.Mvc;
using FluentValidation;
using AssetHealth.Models.Synchronization_device;
using Ares.UMS.Svc.Localization;
using Ares.Web.Validators;


namespace AssetHealth.Validators
{
    public partial class SynchronizationDeviceModelValidator : BaseAresValidator<SynchronizationDeviceModel>
    {
        public SynchronizationDeviceModelValidator(ILocalizationSvc _localizationSvc)
        {
		
            RuleFor(x => x.Synchronization_id).NotNull().WithMessage(_localizationSvc.GetResource("SynchronizationDeviceModelValidator.Synchronization_id.NotNull"));
            RuleFor(x => x.Device_id).NotNull().WithMessage(_localizationSvc.GetResource("SynchronizationDeviceModelValidator.Device_id.NotNull"));
            RuleFor(x => x.Udid).Length(1, 250).WithMessage(_localizationSvc.GetResource("SynchronizationDeviceModelValidator.Udid.Validator"));
            RuleFor(x => x.Status).NotNull().WithMessage(_localizationSvc.GetResource("SynchronizationDeviceModelValidator.Status.NotNull"));
            RuleFor(x => x.Description).Length(1, 1000).WithMessage(_localizationSvc.GetResource("SynchronizationDeviceModelValidator.Description.Validator"));
        }

        
    }


}