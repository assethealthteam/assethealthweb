﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using System.Web.Mvc;
using FluentValidation;
using AssetHealth.Models.Device;
using Ares.UMS.Svc.Localization;
using Ares.Web.Validators;


namespace AssetHealth.Validators
{
    public partial class DeviceModelValidator : BaseAresValidator<DeviceModel>
    {
        public DeviceModelValidator(ILocalizationSvc _localizationSvc)
        {
		
            RuleFor(x => x.Udid)/*.NotNull().WithMessage(_localizationSvc.GetResource("DeviceModelValidator.Udid.NotNull"))*/.Length(1, 750).WithMessage(_localizationSvc.GetResource("DeviceModelValidator.Udid.NotNull"));
            RuleFor(x => x.Device_name).Length(1, 50).WithMessage(_localizationSvc.GetResource("DeviceModelValidator.Device_name.Validator"));
            RuleFor(x => x.Device_model).Length(1, 100).WithMessage(_localizationSvc.GetResource("DeviceModelValidator.Device_model.Validator"));
            RuleFor(x => x.Platform_type).NotNull().WithMessage(_localizationSvc.GetResource("DeviceModelValidator.Platform_type.NotNull"));
            RuleFor(x => x.Os_version).Length(1, 50).WithMessage(_localizationSvc.GetResource("DeviceModelValidator.Os_version.Validator"));
            RuleFor(x => x.Current_carrier_network).Length(1, 100).WithMessage(_localizationSvc.GetResource("DeviceModelValidator.Current_carrier_network.Validator"));
            RuleFor(x => x.Battery_level).Length(1, 24).WithMessage(_localizationSvc.GetResource("DeviceModelValidator.Battery_level.Validator"));
            RuleFor(x => x.Username).Length(1, 100).WithMessage(_localizationSvc.GetResource("DeviceModelValidator.Username.Validator"));
            RuleFor(x => x.Mobile_number).Length(1, 15).WithMessage(_localizationSvc.GetResource("DeviceModelValidator.Mobile_number.Validator"));
            RuleFor(x => x.Image_path).Length(1, 1000).WithMessage(_localizationSvc.GetResource("DeviceModelValidator.Image_path.Validator"));
        }

        
    }


}