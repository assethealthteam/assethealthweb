﻿using FluentValidation;
using AssetHealth.Models.Configuration;
using AssetHealth.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.Web.Validators;
using Ares.UMS.Svc.Localization;
using Ares.Core;
using Ares.UMS.DTO.Users;
using Ares.UMS.Svc.Users;
using System.Text.RegularExpressions;

namespace AssetHealth.Validators
{
    public class UserModelValidator : BaseAresValidator<UserModel>
    {

        public UserModelValidator(ILocalizationSvc localizationService)
        {


            RuleFor(x => x.Email).NotEmpty().WithMessage(localizationService.GetResource("UserModel.Email.Required")).EmailAddress().WithMessage(localizationService.GetResource("UserModel.Email.NotEmail"));
            RuleFor(x => x.Username).NotEmpty().WithMessage(localizationService.GetResource("UserModel.Username.Required"));
            RuleFor(x => x.IdentityNumber).NotEmpty().WithMessage(localizationService.GetResource("UserModel.IdentityNumber.Required"));
            RuleFor(x => x.WorkingLanguage_Id).NotNull().WithMessage(localizationService.GetResource("UserModel.WorkingLanguage_Id.Required"));
            RuleFor(x => x.Nationality_id).NotNull().WithMessage(localizationService.GetResource("UserModel.Nationality_id.Required"));



            //RuleFor(y => y.ValDouble1).InclusiveBetween(40.0, 50.0);

            //RuleFor(customer => customer.ApplicationSubmitted).NotEmpty().When(customer => customer.SelectedApplicationStatus > 0);
        }


    }
}