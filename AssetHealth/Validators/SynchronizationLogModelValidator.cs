﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using System.Web.Mvc;
using FluentValidation;
using AssetHealth.Models.Synchronization_log;
using Ares.UMS.Svc.Localization;
using Ares.Web.Validators;


namespace AssetHealth.Validators
{
    public partial class SynchronizationLogModelValidator : BaseAresValidator<SynchronizationLogModel>
    {
        public SynchronizationLogModelValidator(ILocalizationSvc _localizationSvc)
        {
		
            RuleFor(x => x.Synchronization_detail_id).NotNull().WithMessage(_localizationSvc.GetResource("SynchronizationLogModelValidator.Synchronization_detail_id.NotNull"));
            RuleFor(x => x.Device_id).NotNull().WithMessage(_localizationSvc.GetResource("SynchronizationLogModelValidator.Device_id.NotNull"));
        }

        
    }


}