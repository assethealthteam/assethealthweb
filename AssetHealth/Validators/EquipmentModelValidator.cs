﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using System.Web.Mvc;
using FluentValidation;
using AssetHealth.Models.Equipment;
using Ares.UMS.Svc.Localization;
using Ares.Web.Validators;


namespace AssetHealth.Validators
{
    public partial class EquipmentModelValidator : BaseAresValidator<EquipmentModel>
    {
        public EquipmentModelValidator(ILocalizationSvc _localizationSvc)
        {
		
            RuleFor(x => x.Eq_location_id).NotNull().WithMessage(_localizationSvc.GetResource("EquipmentModelValidator.Eq_location_id.NotNull"));
        }

        
    }


}