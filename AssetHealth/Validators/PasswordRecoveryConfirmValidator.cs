﻿using Ares.UMS.DTO.Users;
using Ares.UMS.Svc.Localization;
using Ares.Web.Validators;
using AssetHealth.Models.User;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetHealth.Validators
{
    public class PasswordRecoveryConfirmValidator : BaseAresValidator<PasswordRecoveryConfirmModel>
    {
        public PasswordRecoveryConfirmValidator(ILocalizationSvc localizationService, UserSettings userSettings)
        {
            RuleFor(x => x.NewPassword).NotEmpty().WithMessage(localizationService.GetResource("User.PasswordRecovery.NewPassword.Required"));
            RuleFor(x => x.NewPassword).Length(userSettings.PasswordMinLength, 999).WithMessage(string.Format(localizationService.GetResource("User.PasswordRecovery.NewPassword.LengthValidation"), userSettings.PasswordMinLength));
            RuleFor(x => x.ConfirmNewPassword).NotEmpty().WithMessage(localizationService.GetResource("User.PasswordRecovery.ConfirmNewPassword.Required"));
            RuleFor(x => x.ConfirmNewPassword).Equal(x => x.NewPassword).WithMessage(localizationService.GetResource("User.PasswordRecovery.NewPassword.EnteredPasswordsDoNotMatch"));
        }
    }
}
