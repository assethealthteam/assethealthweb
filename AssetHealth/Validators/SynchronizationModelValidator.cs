﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using System.Web.Mvc;
using FluentValidation;
using AssetHealth.Models.Synchronization;
using Ares.UMS.Svc.Localization;
using Ares.Web.Validators;


namespace AssetHealth.Validators
{
    public partial class SynchronizationModelValidator : BaseAresValidator<SynchronizationModel>
    {
        public SynchronizationModelValidator(ILocalizationSvc _localizationSvc)
        {
		
            RuleFor(x => x.Udid).Length(1, 250).WithMessage(_localizationSvc.GetResource("SynchronizationModelValidator.Udid.Validator"));
            RuleFor(x => x.Username).Length(1, 250).WithMessage(_localizationSvc.GetResource("SynchronizationModelValidator.Username.Validator"));
            RuleFor(x => x.Sync_type).NotNull().WithMessage(_localizationSvc.GetResource("SynchronizationModelValidator.Sync_type.NotNull"));
            RuleFor(x => x.Status).NotNull().WithMessage(_localizationSvc.GetResource("SynchronizationModelValidator.Status.NotNull"));
            RuleFor(x => x.Start_datetime).NotNull().WithMessage(_localizationSvc.GetResource("SynchronizationModelValidator.Start_datetime.NotNull"));
            RuleFor(x => x.Client_version).Length(1, 300).WithMessage(_localizationSvc.GetResource("SynchronizationModelValidator.Client_version.Validator"));
            RuleFor(x => x.Server_version).Length(1, 300).WithMessage(_localizationSvc.GetResource("SynchronizationModelValidator.Server_version.Validator"));
            RuleFor(x => x.Description).Length(1, 1000).WithMessage(_localizationSvc.GetResource("SynchronizationModelValidator.Description.Validator"));
        }

        
    }


}