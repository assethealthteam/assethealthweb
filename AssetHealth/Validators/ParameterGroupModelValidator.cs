﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using Ares.Web.Models;
using Ares.Web.Razor;
using System.Web.Mvc;
using FluentValidation;
using Ares.UMS.Svc.Localization;
using Ares.Web.Validators;
using Ares.Web.Models.Parameter_group;

namespace AssetHealth.Validators
{
    public partial class ParameterGroupModelValidator : BaseAresValidator<ParameterGroupModel>
    {
        public ParameterGroupModelValidator(ILocalizationSvc _localizationSvc)
        {
		
            RuleFor(x => x.Name).NotNull().WithMessage(_localizationSvc.GetResource("ParameterGroupModelValidator.Name.NotNull")).Length(1, 1500).WithMessage(_localizationSvc.GetResource("ParameterGroupModelValidator.Name.NotNull"));
            RuleFor(x => x.System_name).NotNull().WithMessage(_localizationSvc.GetResource("ParameterGroupModelValidator.System_name.NotNull")).Length(1, 150).WithMessage(_localizationSvc.GetResource("ParameterGroupModelValidator.System_name.NotNull"));
            RuleFor(x => x.Description).Length(1, 1000).WithMessage(_localizationSvc.GetResource("ParameterGroupModelValidator.Description.Validator"));
        }

        
    }


}