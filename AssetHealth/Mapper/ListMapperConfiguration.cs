﻿using Ares.Core.Framework.Mapper;
using AutoMapper;
using System;
using AssetHealth.Models.Configuration;
using AssetHealth.Models.Device;
using AssetHealth.Common.DTO;
using AssetHealth.Models.Form;
using AssetHealth.Models.Form_answer;
using AssetHealth.Models.Form_question;
using AssetHealth.Models.Form_question_option;
using AssetHealth.Models.Synchronization;
using AssetHealth.Models.Synchronization_detail;
using AssetHealth.Models.Synchronization_log;
using Ares.UMS.DTO.Localization;
using AssetHealth.Models.Language;
using AssetHealth.Models.Eq_channel;
using AssetHealth.Models.Eq_location;
using AssetHealth.Models.Eq_threshold;
using AssetHealth.Models.Eq_warning;
using AssetHealth.Models.Equipment;
using Ares.Web.Models.Parameter_group;
using Ares.Web.Models.Parameter_item;
using Ares.Sync.DTO;
using Ares.UMS.DTO.Configuration;
using AssetHealth.Models.Form_answer_round;
using AssetHealth.Models.Form_notes;
using AssetHealth.Models.User;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using AssetHealth.Models.Synchronization_device;

namespace AssetHealth.Mapper
{
    public class ListMapperConfiguration : IMapperConfiguration
    {

        public const string CONFIGURATION_NAME = "ListMapperConfiguration";

        public string ConfigurationName
        {
            get { return CONFIGURATION_NAME; }
        }


        /// <summary>
        /// Order of this mapper implementation
        /// </summary>
        public int Order
        {
            get { return 1; }
        }

        /// <summary>
        /// Get configuration
        /// </summary>
        /// <returns>Mapper configuration action</returns>
        public Action<IMapperConfigurationExpression> GetConfiguration()
        {

            Action<IMapperConfigurationExpression> action = cfg =>
            {
                cfg.CreateMap<User, Ares.Web.Models.User.UserModel>();
                cfg.CreateMap<Ares.Web.Models.User.UserModel, User>().ForMember(x => x.Password, opt => opt.Ignore()).ForMember(x => x.PasswordFormatId, opt => opt.Ignore()).ForMember(x => x.PasswordSalt, opt => opt.Ignore()).ForMember(x => x.INSERT_USER_ID, opt => opt.Ignore()).ForMember(x => x.INSERT_DATETIME, opt => opt.Ignore()).ForMember(x => x.UPDATE_DATETIME, opt => opt.Ignore()).ForMember(x => x.UPDATE_USER_ID, opt => opt.Ignore()).ForMember(x => x.FailedPasswordAttemptCount, opt => opt.Ignore()).ForMember(x => x.LastActivityDateTime, opt => opt.Ignore()).ForMember(x => x.LastIpAddress, opt => opt.Ignore()).ForMember(x => x.LastLoginDateTime, opt => opt.Ignore()).ForMember(x => x.LastPasswordChangedDateTime, opt => opt.Ignore());

                cfg.CreateMap<User, Models.User.UserModel>();
                cfg.CreateMap<Models.User.UserModel, User>().ForMember(x => x.Password, opt => opt.Ignore()).ForMember(x => x.PasswordFormatId, opt => opt.Ignore()).ForMember(x => x.PasswordSalt, opt => opt.Ignore()).ForMember(x => x.INSERT_USER_ID, opt => opt.Ignore()).ForMember(x => x.INSERT_DATETIME, opt => opt.Ignore()).ForMember(x => x.UPDATE_DATETIME, opt => opt.Ignore()).ForMember(x => x.UPDATE_USER_ID, opt => opt.Ignore()).ForMember(x => x.FailedPasswordAttemptCount, opt => opt.Ignore()).ForMember(x => x.LastActivityDateTime, opt => opt.Ignore()).ForMember(x => x.LastIpAddress, opt => opt.Ignore()).ForMember(x => x.LastLoginDateTime, opt => opt.Ignore()).ForMember(x => x.LastPasswordChangedDateTime, opt => opt.Ignore());

                cfg.CreateMap<Role, RoleModel>();
                cfg.CreateMap<RoleModel, Role>();

                cfg.CreateMap<Permission, PermissionModel>().ForMember(x => x.Menus, opt => opt.Ignore());
                cfg.CreateMap<PermissionModel, Permission>();

                cfg.CreateMap<Menu, MenuModel>().ForMember(x => x.Permission, opt => opt.Ignore()).ForMember(x => x.Parent, opt => opt.Ignore()).ForMember(d => d.SubMenus, o => o.Ignore()).ForMember(d => d.TopParent, o => o.Ignore());
                cfg.CreateMap<MenuModel, Menu>().ForMember(x => x.SubMenus, opt => opt.Ignore())/*.ForMember(d => d.SubMenus, o => o.MapFrom(s => s.SubMenus))*/;

                cfg.CreateMap<LocaleResource, LocaleResourceModel>();
                cfg.CreateMap<LocaleResourceModel, LocaleResource>();

                cfg.CreateMap<Device, DeviceModel>();
                cfg.CreateMap<DeviceModel, Device>();

                cfg.CreateMap<Form, FormModel>().ForMember(x => x.DERIVED, opt => opt.Ignore()).ForMember(x => x.PARAMETER_ITEMS, opt => opt.Ignore()).ForMember(x => x.PERIOD_PARAMITEMS, opt => opt.Ignore()).ForMember(x => x.ROLE, opt => opt.Ignore()).ForMember(x => x.EQUIPMENT, opt => opt.Ignore()).ForMember(x => x.LANGUAGE, opt => opt.Ignore());
                cfg.CreateMap<FormModel, Form>().ForMember(x => x.DERIVED, opt => opt.Ignore()).ForMember(x => x.PARAMETER_ITEMS, opt => opt.Ignore()).ForMember(x => x.PERIOD_PARAMITEMS, opt => opt.Ignore()).ForMember(x => x.ROLE, opt => opt.Ignore()).ForMember(x => x.EQUIPMENT, opt => opt.Ignore()).ForMember(x => x.LANGUAGE, opt => opt.Ignore());

                // aşağdaıki şekilde bir mapping olduğunda form answer liste sayfasında yandaki tablolara her bir form_answer için gidiyor.100 kayıt getir dediğinde inanılmaz bir hacim çıkıyor form_question where id = 1245 eq_channel parameter_item parameter_group equipment eq_location form role form_answer_round
                //cfg.CreateMap<FormAnswer, FormAnswerModel>();
                cfg.CreateMap<FormAnswer, FormAnswerModel>().ForMember(x => x.ANSWER_PREVIOUS_ANSWER, opt => opt.Ignore()).ForMember(x => x.ANSWER_QUESTION, opt => opt.Ignore()).ForMember(x => x.ANSWER_ROUND, opt => opt.Ignore()).ForMember(x => x.ANSWER_WARNING_PARAMITEMS, opt => opt.Ignore());
                cfg.CreateMap<FormAnswerModel, FormAnswer>().ForMember(x => x.ANSWER_PREVIOUS_ANSWER, opt => opt.Ignore()).ForMember(x => x.ANSWER_QUESTION, opt => opt.Ignore()).ForMember(x => x.ANSWER_ROUND, opt => opt.Ignore()).ForMember(x => x.ANSWER_WARNING_PARAMITEMS, opt => opt.Ignore());

                cfg.CreateMap<FormAnswerRound, FormAnswerRoundModel>().ForMember(x => x.ANSWER_ROUND_PERIOD_PARAMITEMS, opt => opt.Ignore()).ForMember(x => x.ANSWER_ROUND_WARNING_PARAMITEMS, opt => opt.Ignore());
                cfg.CreateMap<FormAnswerRoundModel, FormAnswerRound>().ForMember(x => x.ANSWER_ROUND_PERIOD_PARAMITEMS, opt => opt.Ignore()).ForMember(x => x.ANSWER_ROUND_WARNING_PARAMITEMS, opt => opt.Ignore());
                cfg.CreateMap<FormQuestion, FormQuestionModel>().ForMember(x => x.QUESTION_CATEGORY_PARAMITEMS, opt => opt.Ignore()).ForMember(x => x.QUESTION_DERIVED, opt => opt.Ignore()).ForMember(x => x.QUESTION_FORM, opt => opt.Ignore()).ForMember(x => x.QUESTION_PERIOD_CATEGORY_PARAMITEMS, opt => opt.Ignore()).ForMember(x => x.QUESTION_TYPE_CATEGORY_PARAMITEMS, opt => opt.Ignore()).ForMember(x => x.EQ_CHANNEL, opt => opt.Ignore()).ForMember(x => x.QUESTION_OPTION_PARAM_GROUP, opt => opt.Ignore());
                cfg.CreateMap<FormQuestionModel, FormQuestion>().ForMember(x => x.QUESTION_CATEGORY_PARAMITEMS, opt => opt.Ignore()).ForMember(x => x.QUESTION_DERIVED, opt => opt.Ignore()).ForMember(x => x.QUESTION_FORM, opt => opt.Ignore()).ForMember(x => x.QUESTION_PERIOD_CATEGORY_PARAMITEMS, opt => opt.Ignore()).ForMember(x => x.QUESTION_TYPE_CATEGORY_PARAMITEMS, opt => opt.Ignore()).ForMember(x => x.EQ_CHANNEL, opt => opt.Ignore()).ForMember(x => x.QUESTION_OPTION_PARAM_GROUP, opt => opt.Ignore());
                cfg.CreateMap<FormQuestionOption, FormQuestionOptionModel>();
                cfg.CreateMap<FormQuestionOptionModel, FormQuestionOption>().ForMember(x => x.QUESTION_OPTION_DERIVED, opt => opt.Ignore()).ForMember(x => x.QUESTION_OPTION_QUESTION, opt => opt.Ignore());

                cfg.CreateMap<ParameterGroup, ParameterGroupModel>();
                cfg.CreateMap<ParameterGroupModel, ParameterGroup>();
                cfg.CreateMap<ParameterItem, ParameterItemModel>();
                cfg.CreateMap<ParameterItemModel, ParameterItem>().ForMember(x => x.Group, opt => opt.Ignore());

                cfg.CreateMap<Synchronization, SynchronizationModel>();
                cfg.CreateMap<SynchronizationModel, Synchronization>().ForMember(x => x.SYNCHRONIZATION_DEVICE, opt => opt.Ignore());
                cfg.CreateMap<SynchronizationDetail, SynchronizationDetailModel>();
                cfg.CreateMap<SynchronizationDetailModel, SynchronizationDetail>().ForMember(x => x.SYNCHRONIZATION_DETAIL, opt => opt.Ignore());
                cfg.CreateMap<SynchronizationDevice, SynchronizationDeviceModel>();
                cfg.CreateMap<SynchronizationDeviceModel, SynchronizationDevice>().ForMember(x => x.SYNCHRONIZATION_DEVICE_DEVICE, opt => opt.Ignore()).ForMember(x => x.SYNCHRONIZATION_DEVICE_SYNCHRONIZATION, opt => opt.Ignore());
                cfg.CreateMap<SynchronizationLog, SynchronizationLogModel>();
                cfg.CreateMap<SynchronizationLogModel, SynchronizationLog>().ForMember(x => x.SYNCHRONIZATION_LOG_DETAIL, opt => opt.Ignore()).ForMember(x => x.SYNCHRONIZATION_LOG_DEVICE, opt => opt.Ignore());

                cfg.CreateMap<EqChannel, EqChannelModel>().ForMember(x => x.CHANNEL_SENSOR_TYPE_PARAMITEMS, opt => opt.Ignore()).ForMember(x => x.channel_equipment, opt => opt.Ignore());
                cfg.CreateMap<EqChannelModel, EqChannel>().ForMember(x => x.CHANNEL_SENSOR_TYPE_PARAMITEMS, opt => opt.Ignore()).ForMember(x => x.channel_equipment, opt => opt.Ignore());
                cfg.CreateMap<EqLocation, EqLocationModel>();
                cfg.CreateMap<EqLocationModel, EqLocation>().ForMember(x => x.ParentLocation, opt => opt.Ignore());
                cfg.CreateMap<EqThreshold, EqThresholdModel>();
                cfg.CreateMap<EqThresholdModel, EqThreshold>().ForMember(x => x.THRESHOLD_WARNING_TYPE_PARAMITEMS, opt => opt.Ignore()).ForMember(x => x.threshold_eq_channel, opt => opt.Ignore());
                cfg.CreateMap<EqWarning, EqWarningModel>();
                cfg.CreateMap<EqWarningModel, EqWarning>().ForMember(x => x.WARNING_WARNING_PARAMITEMS, opt => opt.Ignore()).ForMember(x => x.Warning_EQUIPMENT, opt => opt.Ignore()).ForMember(x => x.Warning_EQ_CHANNEL, opt => opt.Ignore());
                cfg.CreateMap<Equipment, EquipmentModel>().ForMember(x => x.EQUIPMENT_TYPE_PARAMITEMS, opt => opt.Ignore()).ForMember(x => x.Location, opt => opt.Ignore());
                cfg.CreateMap<EquipmentModel, Equipment>().ForMember(x => x.EQUIPMENT_TYPE_PARAMITEMS, opt => opt.Ignore()).ForMember(x => x.Location, opt => opt.Ignore());


                cfg.CreateMap<FormNote, FormNoteModel>();
                cfg.CreateMap<FormNoteModel, FormNote>()/*.ForMember(x => x.NOTES_DEVICE, opt => opt.Ignore())*/.ForMember(x => x.NOTES_ROUND, opt => opt.Ignore()).ForMember(x => x.NOTES_WARNING_PARAMITEMS, opt => opt.Ignore());


            };

            return action;
        }
    }
}