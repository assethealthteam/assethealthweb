using Ares.Web.Localization;
using Ares.Web.Mvc.Routes;
using Ares.Web.Seo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace AssetHealth.Infrastructure
{
    public class GenericUrlRouteProvider : IRouteProvider
    {
        public void RegisterRoutes(RouteCollection routes)
        {

            //generic URLs
            routes.MapGenericPathRoute("GenericUrl",
                                       "{generic_se_name}",
                                       new { controller = "Common", action = "GenericUrl" },
                                       new[] { "AssetHealth.Controllers" });

            //define this routes to use in UI views (in case if you want to customize some of them later)
            routes.MapLocalizedRoute("Foundation",
                                     "{SeName}",
                                     new { controller = "Home", action = "Detail" },
                                     new[] { "AssetHealth.Controllers" });


            routes.MapLocalizedRoute("AccountActivation",
                            "user/activation",
                            new { controller = "User", action = "AccountActivation" },
                            new[] { "AssetHealth.Controllers" });


            routes.MapLocalizedRoute("ChangeLanguage",
                "changelanguage/{langid}",
                new { controller = "Common", action = "ChangeLanguage" },
                new { langid = @"\d+" },
                new[] { "AssetHealth.Controllers" });


            routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );


            routes.MapRoute(
                name: "HomePage",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "AssetHealth.Controllers" }

            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "AssetHealth.Controllers" }
            );


            //the last route. it's used when none of registered routes could be used for the current request
            //but it this case we cannot process non-registered routes (/controller/action)
            //routes.MapLocalizedRoute(
            //    "PageNotFound-Wildchar",
            //    "{*url}",
            //    new { controller = "Common", action = "PageNotFound" },
            //    new[] { "Nop.Web.Controllers" });
        }

        public int Priority
        {
            get
            {
                //it should be the last route
                return -int.MaxValue;
            }
        }
    }
}