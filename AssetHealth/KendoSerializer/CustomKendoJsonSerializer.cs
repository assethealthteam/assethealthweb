﻿using Ares.Web.JSON;
using AssetHealth.Models.Aa_excel_import;
using Kendo.Mvc.Infrastructure;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetHealth.KendoSerializer
{
    public class CustomKendoJsonSerializer : IJavaScriptSerializer
    {
        public string Serialize(object value)
        {
            var jsonResolver = new PropertyRenameAndIgnoreSerializerContractResolver();
            jsonResolver.IgnoreProperty(typeof(AaExcelImportModel), "Threshold_repetition_count");
            //jsonResolver.RenameProperty(typeof(AaExcelImportModel), "FirstName", "firstName");

            var serializerSettings = new JsonSerializerSettings();
            serializerSettings.ContractResolver = jsonResolver;

            return JsonConvert.SerializeObject(value, serializerSettings); //use the serializer you want  here
        }
    }
}
