﻿using Kendo.Mvc.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetHealth.KendoSerializer
{
    public class CustomKendoInitializer : JavaScriptInitializer
    {

        public override IJavaScriptSerializer CreateSerializer()

        {

            return new CustomKendoJsonSerializer();

        }

    }
}
