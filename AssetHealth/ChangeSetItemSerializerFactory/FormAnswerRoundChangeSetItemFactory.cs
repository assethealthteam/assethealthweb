﻿using Ares.Core.Framework;
using Ares.Sync.ChangeManagement;
using Ares.Sync.DTO;
using Ares.Sync.DTO.Enums;
using Ares.Sync.Entities.Custom;
using Ares.Sync.Svc;
using Ares.UMS.DTO.Users;
using Ares.UMS.Svc.Users;
using Ares.Web.JSON;
using Ares.Web.Mvc.Entities;
using AssetHealth.Common.DTO;
using AssetHealth.Common.Svc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetHealth.ChangeSetItemSerializerFactory
{
    public class FormAnswerRoundChangeSetItemFactory : ChangeSetItemFactory<FormAnswerRound, FormAnswerRound>
    {
        public FormAnswerRoundChangeSetItemFactory(int cihazId, SynchronizationDetail detail)
            : base(cihazId, detail)
        {

        }

        protected override ApiTransferObject Validate(FormAnswerRound item)
        {
            ApiTransferObject result = new ApiTransferObject()
            {
                Success = true
            };
            return result;
        }

        protected override FormAnswerRound PrepareForSynchronization(int itemId)
        {
            FormAnswerRound result = null;
            /*
            using (NumaratajContext context = new NumaratajContext())
            using (var tr = context.Database.BeginTransaction(IsolationLevel.ReadUncommitted))
            {
                yapi = context.YAPI.FirstOrDefault(t => t.OBJECTID == itemId);
                yapi.Geometry = yapi.SHAPE.AsText();
                yapi.ID = yapi.OBJECTID;
            }
            */
            return result;
        }

        public override int SaveChangeSetItem(SynchronizationDetail item)
        {
            IUserSvc userSvc = FrameworkCtx.Current.Resolve<IUserSvc>();
            IFormAnswerRoundSvc svc = FrameworkCtx.Current.Resolve<IFormAnswerRoundSvc>();
            IFormQuestionSvc questionSvc = FrameworkCtx.Current.Resolve<IFormQuestionSvc>();
            FormAnswerRound comingFormAnswerRound = JsonConvert.DeserializeObject<FormAnswerRound>(item.Item, new AresJsonSerializerSettings());
            comingFormAnswerRound.Device_id = _CihazId;
            comingFormAnswerRound.Id = 0;

            // Trying to find form_id (server id)
            ISynchronizationLogSvc syncLogSvc = FrameworkCtx.Current.Resolve<ISynchronizationLogSvc>();
            List<SynchronizationLog> logs = syncLogSvc.GetAll().Where(sl =>
            sl.Client_id == comingFormAnswerRound.Form_id
            && sl.Device_id == _CihazId
            && sl.Status == EnumSynchronizationLogStatus.Finished
            && sl.SYNCHRONIZATION_LOG_DETAIL.Table_name == "form"
            //&& sl.SYNCHRONIZATION_LOG_DETAIL.Status == Ares.Sync.Common.DTO.Enums.EnumSynchronizationDetailStatus.Finished
            ).ToList();

            if (logs != null && logs.Count > 0)
            {
                comingFormAnswerRound.Form_id = logs[0].SYNCHRONIZATION_LOG_DETAIL.Server_id.GetValueOrDefault();
            }

            if (_detail.Server_id.GetValueOrDefault() != 0)
            {
                if (svc.GetById(_detail.Server_id) == null)
                {
                    return _detail.Server_id.GetValueOrDefault();
                }
            }
            if (item.Operation_type == EnumSynchronizationDetailOperation_type.Created)
            {
                if (!string.IsNullOrEmpty(comingFormAnswerRound.Username))
                {
                    User user = userSvc.GetUserByUsername(comingFormAnswerRound.Username);
                    if(user != null)
                        comingFormAnswerRound.Insert_user_id = user.Id + "";
                }
                

                return svc.Insert(comingFormAnswerRound).Id;
            }
            else if (item.Operation_type == EnumSynchronizationDetailOperation_type.Deleted)
            {
                FormAnswerRound formAnswer = svc.GetById(_detail.Server_id.GetValueOrDefault());
                svc.Delete(formAnswer);

                return _detail.Server_id.GetValueOrDefault();
            }
            else
            {
                FormAnswerRound formAnswerRound = svc.GetById(_detail.Server_id.GetValueOrDefault());
                formAnswerRound.End_read_datetime = comingFormAnswerRound.End_read_datetime;
                formAnswerRound.Last_read_datetime = comingFormAnswerRound.Last_read_datetime;
                formAnswerRound.Warning_type = comingFormAnswerRound.Warning_type;
                formAnswerRound.Start_count = comingFormAnswerRound.Start_count;
                formAnswerRound.File_count = comingFormAnswerRound.File_count;

                if (!string.IsNullOrEmpty(comingFormAnswerRound.Username))
                {
                    User user = userSvc.GetUserByUsername(comingFormAnswerRound.Username);
                    if (user != null)
                        comingFormAnswerRound.Update_user_id = user.Id + "";
                }

                svc.Update(formAnswerRound);

                return formAnswerRound.Id;
            }
            
        }

        public override void SaveMedia(int medyaId)
        {
            throw new NotImplementedException();
        }

        public override List<int> GetNesneMedya()
        {
            throw new NotImplementedException();
        }

        public override void NotKaydet(NotData not)
        {
            throw new NotImplementedException();
        }

        public override string GetCentroid(SynchronizationDetail item)
        {
            throw new NotImplementedException();
        }
    }
}