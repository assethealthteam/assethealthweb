﻿using Ares.Core.Framework;
using Ares.Sync.ChangeManagement;
using Ares.Sync.DTO;
using Ares.Sync.DTO.Enums;
using Ares.Sync.Entities.Custom;
using Ares.Sync.Svc;
using Ares.UMS.DTO.Configuration;
using Ares.UMS.Svc;
using Ares.UMS.Svc.Configuration;
using Ares.Web.JSON;
using Ares.Web.Mvc.Entities;
using AssetHealth.Common.DAL;
using AssetHealth.Common.DTO;
using AssetHealth.Common.Svc;
using AssetHealth.Common.Svc.Analyzer;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetHealth.ChangeSetItemSerializerFactory
{
    public class FormAnswerChangeSetItemFactory : ChangeSetItemFactory<FormAnswer, FormAnswer>
    {
        public FormAnswerChangeSetItemFactory(int cihazId, SynchronizationDetail detail)
            : base(cihazId, detail)
        {

        }

        protected override ApiTransferObject Validate(FormAnswer item)
        {
            ApiTransferObject result = new ApiTransferObject()
            {
                Success = true
            };
            return result;
        }

        protected override FormAnswer PrepareForSynchronization(int itemId)
        {
            FormAnswer result = null;
            /*
            using (NumaratajContext context = new NumaratajContext())
            using (var tr = context.Database.BeginTransaction(IsolationLevel.ReadUncommitted))
            {
                yapi = context.YAPI.FirstOrDefault(t => t.OBJECTID == itemId);
                yapi.Geometry = yapi.SHAPE.AsText();
                yapi.ID = yapi.OBJECTID;
            }
            */
            return result;
        }

        public override int SaveChangeSetItem(SynchronizationDetail item)
        {
            IFormAnswerSvc svc = FrameworkCtx.Current.Resolve<IFormAnswerSvc>();
            IFormQuestionSvc questionSvc = FrameworkCtx.Current.Resolve<IFormQuestionSvc>();
            FormAnswer comingFormAnswer = JsonConvert.DeserializeObject<FormAnswer>(item.Item, new AresJsonSerializerSettings());
            comingFormAnswer.Device_id = _CihazId;
            comingFormAnswer.Id = 0;
            comingFormAnswer.Converted_answer = comingFormAnswer.Answer;

            if (!string.IsNullOrEmpty(comingFormAnswer.Answer) && comingFormAnswer.Answer == ".")
                return -1;

            // Trying to find form_question_server_id
            ISynchronizationLogSvc syncLogSvc = FrameworkCtx.Current.Resolve<ISynchronizationLogSvc>();
            List<SynchronizationLog> logs = syncLogSvc.GetAll().Where(sl => 
            sl.Client_id == comingFormAnswer.Form_question_id 
            && sl.Device_id == _CihazId
            && sl.Status == EnumSynchronizationLogStatus.Finished
            && sl.SYNCHRONIZATION_LOG_DETAIL.Table_name == "form_question"
            //&& sl.SYNCHRONIZATION_LOG_DETAIL.Status == Ares.Sync.Common.DTO.Enums.EnumSynchronizationDetailStatus.Finished
            ).ToList();

            if (logs != null && logs.Count > 0)
            {
                comingFormAnswer.Form_question_id = logs[0].SYNCHRONIZATION_LOG_DETAIL.Server_id.GetValueOrDefault();
            }

            IAnswerAnalyzer answerAnalyzer = FrameworkCtx.Current.Resolve<IAnswerAnalyzer>();

            // Trying to find form_answer_round_id
            ISynchronizationDetailSvc syncDetailSvc = FrameworkCtx.Current.Resolve<ISynchronizationDetailSvc>();
            List<SynchronizationDetail> detailRound = syncDetailSvc.GetAll().Where(sd =>
            sd.Client_id == comingFormAnswer.Form_answer_round_id
            && sd.SYNCHRONIZATION_DETAIL.Device_id == _CihazId
            && sd.Status == Ares.Sync.Common.DTO.Enums.EnumSynchronizationDetailStatus.Finished
            && sd.Table_name == "formanswerround"
            ).OrderByDescending(r => r.Id).ToList();

            if (detailRound != null && detailRound.Count > 0)
            {
                comingFormAnswer.Form_answer_round_id = detailRound[0].Server_id.GetValueOrDefault();
            }


            if (_detail.Server_id.GetValueOrDefault() != 0)
            {
                if (svc.GetById(_detail.Server_id) == null)
                {
                    return _detail.Server_id.GetValueOrDefault();
                }
            }

            FormQuestion question = questionSvc.GetById(comingFormAnswer.Form_question_id);

            if (item.Operation_type == EnumSynchronizationDetailOperation_type.Created)
            {
                /*
                if (question.FORM_QUESTIONForm_question_option != null && question.FORM_QUESTIONForm_question_option.Count > 0)
                {
                    FormQuestionOption option = question.FORM_QUESTIONForm_question_option.FirstOrDefault(o => o.Id == nAnswer);
                    if (option != null)
                    {
                        comingFormAnswer.Converted_answer = option.Title;
                    }
                }
                */
                if (question.Type_id == 13) // spinner
                {
                    int nAnswer = -1;
                    int.TryParse(comingFormAnswer.Answer, out nAnswer);

                    IParameterItemSvc _parameterItemSvc = FrameworkCtx.Current.Resolve<IParameterItemSvc>();
                    ParameterItem option = _parameterItemSvc.GetById(nAnswer);
                    if (option != null)
                    {
                        comingFormAnswer.Converted_answer = option.Title;
                    }
                    
                }
                
                int newId = svc.Insert(comingFormAnswer).Id;

                /*
                FormAnswer answerToAnalyze = svc.GetById(newId); 
                answerAnalyzer.AnalyzeAnswer(answerToAnalyze);
                */
                IWorkContext workContext = FrameworkCtx.Current.Resolve<IWorkContext>();
                svc.ExecuteSqlCommand(string.Format(HardCodedSQL.ANSWER_ANALYZE_SP, newId, "20190101000000", "20190101000000", workContext.CurrentUser.Id, workContext.CurrentUser.WorkingLanguage_Id));

                return newId;
            }
            else if (item.Operation_type == EnumSynchronizationDetailOperation_type.Deleted)
            {
                FormAnswer formAnswer = svc.GetById(_detail.Server_id.GetValueOrDefault());
                svc.Delete(formAnswer);

                return _detail.Server_id.GetValueOrDefault();
            }
            else
            {
                FormAnswer formAnswer = svc.GetById(item.Server_id.GetValueOrDefault());
                if (formAnswer != null && (formAnswer.Answer != comingFormAnswer.Answer || formAnswer.Description != comingFormAnswer.Description))
                {
                    formAnswer.Answer = comingFormAnswer.Answer;
                    formAnswer.Long_answer = comingFormAnswer.Long_answer;
                    formAnswer.Description = comingFormAnswer.Description;
                    //formAnswer.Warning_type = comingFormAnswer.Warning_type;

                    /*
                    if (question.FORM_QUESTIONForm_question_option != null && question.FORM_QUESTIONForm_question_option.Count > 0)
                    {
                        int nAnswer = int.Parse(comingFormAnswer.Answer);
                        FormQuestionOption option = question.FORM_QUESTIONForm_question_option.FirstOrDefault(o => o.Id == nAnswer);
                        if (option != null)
                        {
                            formAnswer.Converted_answer = option.Title;
                        }
                    }
                    */
                    if (question.Type_id == 13) // spinner
                    {
                        int nAnswer = int.Parse(formAnswer.Answer);
                        IParameterItemSvc _parameterItemSvc = FrameworkCtx.Current.Resolve<IParameterItemSvc>();
                        ParameterItem option = _parameterItemSvc.GetById(nAnswer);
                        if (option != null)
                        {
                            formAnswer.Converted_answer = option.Title;
                        }

                    }

                    svc.Update(formAnswer);
                }
                

                return formAnswer.Id;
            }
            
        }

        public override void SaveMedia(int medyaId)
        {
            throw new NotImplementedException();
        }

        public override List<int> GetNesneMedya()
        {
            throw new NotImplementedException();
        }

        public override void NotKaydet(NotData not)
        {
            throw new NotImplementedException();
        }

        public override string GetCentroid(SynchronizationDetail item)
        {
            throw new NotImplementedException();
        }
    }
}