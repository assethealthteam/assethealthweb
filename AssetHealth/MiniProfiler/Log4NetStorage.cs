﻿using Ares.Core.Framework;
using Ares.Core.Framework.Logging;
using StackExchange.Profiling.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetHealth.MiniProfiler
{
    /// <summary>
    ///     User Log4Net as storage.
    /// </summary>
    public class Log4NetStorage : IStorage
    {
        /// <summary>
        ///     The logger by Log4Net.
        /// </summary>
        //private static readonly ILog Log4NetLogger;
        private ILogger _logger;

        List<Guid> result;

        /// <summary>
        ///     Initializes static members of the <see cref="Log4NetStorage" /> class.
        /// </summary>
        public Log4NetStorage(ILogger logger)
        {
            this._logger = logger;
            result = new List<Guid>();

        }

        /// <summary>
        /// Returns a list of <see cref="P:StackExchange.Profiling.MiniProfiler.Id"/>s that haven't been seen by
        ///     <paramref name="user"/>.
        /// </summary>
        /// <param name="user">
        /// User identified by the current <c>MiniProfiler.Settings.UserProvider</c>
        /// </param>
        /// <returns>
        /// the list of key values.
        /// </returns>
        public List<Guid> GetUnviewedIds(string user)
        {
            //throw new NotSupportedException("This method should never run");
            result.Add(Guid.NewGuid());

            return result;
        }


        /// <summary>
        /// list the result keys.
        /// </summary>
        /// <param name="maxResults">
        /// The max results.
        /// </param>
        /// <param name="start">
        /// The start.
        /// </param>
        /// <param name="finish">
        /// The finish.
        /// </param>
        /// <param name="orderBy">
        /// order by.
        /// </param>
        /// <returns>
        /// the list of keys in the result.
        /// </returns>
        public IEnumerable<Guid> List(int maxResults, DateTime? start = null, DateTime? finish = null,
            ListResultsOrder orderBy = ListResultsOrder.Descending)
        {
            //throw new NotSupportedException("This method should never run");
            return null;
        }


        /// <summary>
        /// Returns a <see cref="T:StackExchange.Profiling.MiniProfiler"/> from storage based on <paramref name="id"/>, which
        ///     should map to <see cref="P:StackExchange.Profiling.MiniProfiler.Id"/>.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <remarks>
        /// Should also update that the resulting profiler has been marked as viewed by its profiling
        ///     <see cref="P:StackExchange.Profiling.MiniProfiler.User"/>.
        /// </remarks>
        /// <returns>
        /// The <see cref="T:StackExchange.Profiling.MiniProfiler"/>.
        /// </returns>
        public StackExchange.Profiling.MiniProfiler Load(Guid id)
        {
            //throw new NotSupportedException("This method should never run");
            //return null;
            return StackExchange.Profiling.MiniProfiler.Current;
        }


        /// <summary>
        /// Stores <paramref name="profiler"/> under its <see cref="P:StackExchange.Profiling.MiniProfiler.Id"/>.
        /// </summary>
        /// <param name="profiler">
        /// The results of a profiling session.
        /// </param>
        /// <remarks>
        /// Should also ensure the profiler is stored as being un-viewed by its profiling
        ///     <see cref="P:StackExchange.Profiling.MiniProfiler.User"/>.
        /// </remarks>
        public void Save(StackExchange.Profiling.MiniProfiler profiler)
        {
            //ILogger logger = FrameworkCtx.Current.Resolve<ILogger>();
            if (profiler == null || _logger == null) return;

            _logger.Log(string.Format("MiniProfiler User<{0}>: {1}", profiler.User, profiler));

            // Write step times
            if (profiler.Root.HasChildren)
            {
                var children = profiler.Root.Children;
                foreach (var child in children)
                {
                    //sb.AppendLine(child.Name + " " + child.DurationMilliseconds);
                    _logger.Log(string.Format("MiniProfiler User<{0}>: {1} {2}", profiler.User, child.Name, child.DurationMilliseconds));
                }
            }

            // Write overall request time
            //sb.AppendLine(string.Format("{0} {1}\n",  miniProfiler.DurationMilliseconds,  miniProfiler.Root));
        }


        /// <summary>
        /// Sets a particular profiler session so it is considered "un-viewed"
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="id">
        /// The id.
        /// </param>
        public void SetUnviewed(string user, Guid id)
        {
            // do nothing
        }


        /// <summary>
        /// Sets a particular profiler session to "viewed"
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="id">
        /// The id.
        /// </param>
        public void SetViewed(string user, Guid id)
        {
            //throw new NotSupportedException("This method should never run");
        }
    }
}