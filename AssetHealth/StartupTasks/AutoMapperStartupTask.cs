using Ares.Core.Framework;
using Ares.UMS.DTO.Configuration;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AssetHealth.Models.Configuration;
using AssetHealth.Models.Device;
using AssetHealth.Common.DTO;
using AssetHealth.Models.Form;
using AssetHealth.Models.Form_answer;
using AssetHealth.Models.Form_question;
using AssetHealth.Models.Form_question_option;
using AssetHealth.Models.Synchronization;
using AssetHealth.Models.Synchronization_detail;
using AssetHealth.Models.Synchronization_log;
using Ares.UMS.DTO.Localization;
using AssetHealth.Models.Language;
using AssetHealth.Models.Eq_channel;
using AssetHealth.Models.Eq_location;
using AssetHealth.Models.Eq_threshold;
using AssetHealth.Models.Eq_warning;
using AssetHealth.Models.Equipment;
using Ares.Web.Models.Parameter_group;
using Ares.Web.Models.Parameter_item;
using AssetHealth.Models.Synchronization_device;
using Ares.Sync.DTO;
using AssetHealth.Models.Form_answer_round;
using AssetHealth.Models.Form_notes;
using Ares.Web.Models.User;
using AssetHealth.Models.User;

namespace AssetHealth.StartupTasks
{
    /*
        public class AutoMapperStartupTask : IStartupTask
        {

            public void Execute()
            {
                Mapper.CreateMap<User, Ares.Web.Models.User.UserModel>();
                Mapper.CreateMap<Ares.Web.Models.User.UserModel, User>().ForMember(x => x.Password, opt => opt.Ignore()).ForMember(x => x.PasswordFormatId, opt => opt.Ignore()).ForMember(x => x.PasswordSalt, opt => opt.Ignore()).ForMember(x => x.INSERT_USER_ID, opt => opt.Ignore()).ForMember(x => x.INSERT_DATETIME, opt => opt.Ignore()).ForMember(x => x.UPDATE_DATETIME, opt => opt.Ignore()).ForMember(x => x.UPDATE_USER_ID, opt => opt.Ignore()).ForMember(x => x.FailedPasswordAttemptCount, opt => opt.Ignore()).ForMember(x => x.LastActivityDateTime, opt => opt.Ignore()).ForMember(x => x.LastIpAddress, opt => opt.Ignore()).ForMember(x => x.LastLoginDateTime, opt => opt.Ignore()).ForMember(x => x.LastPasswordChangedDateTime, opt => opt.Ignore());

                Mapper.CreateMap<User, Models.User.UserModel>();
                Mapper.CreateMap<Models.User.UserModel, User>().ForMember(x => x.Password, opt => opt.Ignore()).ForMember(x => x.PasswordFormatId, opt => opt.Ignore()).ForMember(x => x.PasswordSalt, opt => opt.Ignore()).ForMember(x => x.INSERT_USER_ID, opt => opt.Ignore()).ForMember(x => x.INSERT_DATETIME, opt => opt.Ignore()).ForMember(x => x.UPDATE_DATETIME, opt => opt.Ignore()).ForMember(x => x.UPDATE_USER_ID, opt => opt.Ignore()).ForMember(x => x.FailedPasswordAttemptCount, opt => opt.Ignore()).ForMember(x => x.LastActivityDateTime, opt => opt.Ignore()).ForMember(x => x.LastIpAddress, opt => opt.Ignore()).ForMember(x => x.LastLoginDateTime, opt => opt.Ignore()).ForMember(x => x.LastPasswordChangedDateTime, opt => opt.Ignore());

                Mapper.CreateMap<Role, RoleModel>();
                Mapper.CreateMap<RoleModel, Role>();

                Mapper.CreateMap<Permission, PermissionModel>();
                Mapper.CreateMap<PermissionModel, Permission>();

                Mapper.CreateMap<Menu, MenuModel>();
                Mapper.CreateMap<MenuModel, Menu>().ForMember(x => x.SubMenus, opt => opt.Ignore()); //.ForMember(d => d.SubMenus, o => o.MapFrom(s => s.SubMenus))

                Mapper.CreateMap<LocaleResource, LocaleResourceModel>();
                Mapper.CreateMap<LocaleResourceModel, LocaleResource>();

                Mapper.CreateMap<Device, DeviceModel>();
                Mapper.CreateMap<DeviceModel, Device>();

                Mapper.CreateMap<Form, FormModel>();
                Mapper.CreateMap<FormModel, Form>().ForMember(x => x.DERIVED, opt => opt.Ignore()).ForMember(x => x.PARAMETER_ITEMS, opt => opt.Ignore()).ForMember(x => x.PERIOD_PARAMITEMS, opt => opt.Ignore()).ForMember(x => x.ROLE, opt => opt.Ignore()).ForMember(x => x.EQUIPMENT_ID, opt => opt.Ignore()).ForMember(x => x.LANGUAGE, opt => opt.Ignore());

                // a�a�da�ki �ekilde bir mapping oldu�unda form answer liste sayfas�nda yandaki tablolara her bir form_answer i�in gidiyor.100 kay�t getir dedi�inde inan�lmaz bir hacim ��k�yor form_question where id = 1245 eq_channel parameter_item parameter_group equipment eq_location form role form_answer_round
                Mapper.CreateMap<FormAnswer, FormAnswerModel>(); 
                //Mapper.CreateMap<FormAnswer, FormAnswerModel>().ForMember(x => x.ANSWER_PREVIOUS_ANSWER, opt => opt.Ignore()).ForMember(x => x.ANSWER_QUESTION, opt => opt.Ignore()).ForMember(x => x.ANSWER_ROUND, opt => opt.Ignore()).ForMember(x => x.ANSWER_WARNING_PARAMITEMS, opt => opt.Ignore());
                Mapper.CreateMap<FormAnswerModel, FormAnswer>().ForMember(x => x.ANSWER_PREVIOUS_ANSWER, opt => opt.Ignore()).ForMember(x => x.ANSWER_QUESTION, opt => opt.Ignore()).ForMember(x => x.ANSWER_ROUND, opt => opt.Ignore()).ForMember(x => x.ANSWER_WARNING_PARAMITEMS, opt => opt.Ignore());

                Mapper.CreateMap<FormAnswerRound, FormAnswerRoundModel>();
                Mapper.CreateMap<FormAnswerRoundModel, FormAnswerRound>().ForMember(x => x.ANSWER_ROUND_PERIOD_PARAMITEMS, opt => opt.Ignore()).ForMember(x => x.ANSWER_ROUND_WARNING_PARAMITEMS, opt => opt.Ignore());
                Mapper.CreateMap<FormQuestion, FormQuestionModel>();
                Mapper.CreateMap<FormQuestionModel, FormQuestion>().ForMember(x => x.QUESTION_CATEGORY_PARAMITEMS, opt => opt.Ignore()).ForMember(x => x.QUESTION_DERIVED, opt => opt.Ignore()).ForMember(x => x.QUESTION_FORM, opt => opt.Ignore()).ForMember(x => x.QUESTION_PERIOD_CATEGORY_PARAMITEMS, opt => opt.Ignore()).ForMember(x => x.QUESTION_TYPE_CATEGORY_PARAMITEMS, opt => opt.Ignore()).ForMember(x => x.EQ_CHANNEL, opt => opt.Ignore()).ForMember(x => x.QUESTION_OPTION_PARAM_GROUP, opt => opt.Ignore());
                Mapper.CreateMap<FormQuestionOption, FormQuestionOptionModel>();
                Mapper.CreateMap<FormQuestionOptionModel, FormQuestionOption>().ForMember(x => x.QUESTION_OPTION_DERIVED, opt => opt.Ignore()).ForMember(x => x.QUESTION_OPTION_QUESTION, opt => opt.Ignore());

                Mapper.CreateMap<ParameterGroup, ParameterGroupModel>();
                Mapper.CreateMap<ParameterGroupModel, ParameterGroup>();
                Mapper.CreateMap<ParameterItem, ParameterItemModel>();
                Mapper.CreateMap<ParameterItemModel, ParameterItem>().ForMember(x => x.Group, opt => opt.Ignore());

                Mapper.CreateMap<Synchronization, SynchronizationModel>();
                Mapper.CreateMap<SynchronizationModel, Synchronization>().ForMember(x => x.SYNCHRONIZATION_DEVICE, opt => opt.Ignore());
                Mapper.CreateMap<SynchronizationDetail, SynchronizationDetailModel>();
                Mapper.CreateMap<SynchronizationDetailModel, SynchronizationDetail>().ForMember(x => x.SYNCHRONIZATION_DETAIL, opt => opt.Ignore());
                Mapper.CreateMap<SynchronizationDevice, SynchronizationDeviceModel>();
                Mapper.CreateMap<SynchronizationDeviceModel, SynchronizationDevice>().ForMember(x => x.SYNCHRONIZATION_DEVICE_DEVICE, opt => opt.Ignore()).ForMember(x => x.SYNCHRONIZATION_DEVICE_SYNCHRONIZATION, opt => opt.Ignore());
                Mapper.CreateMap<SynchronizationLog, SynchronizationLogModel>();
                Mapper.CreateMap<SynchronizationLogModel, SynchronizationLog>().ForMember(x => x.SYNCHRONIZATION_LOG_DETAIL, opt => opt.Ignore()).ForMember(x => x.SYNCHRONIZATION_LOG_DEVICE, opt => opt.Ignore());

                Mapper.CreateMap<EqChannel, EqChannelModel>();
                Mapper.CreateMap<EqChannelModel, EqChannel>().ForMember(x => x.CHANNEL_SENSOR_TYPE_PARAMITEMS, opt => opt.Ignore()).ForMember(x => x.channel_equipment, opt => opt.Ignore());
                Mapper.CreateMap<EqLocation, EqLocationModel>();
                Mapper.CreateMap<EqLocationModel, EqLocation>().ForMember(x => x.ParentLocation, opt => opt.Ignore());
                Mapper.CreateMap<EqThreshold, EqThresholdModel>();
                Mapper.CreateMap<EqThresholdModel, EqThreshold>().ForMember(x => x.THRESHOLD_WARNING_TYPE_PARAMITEMS, opt => opt.Ignore()).ForMember(x => x.threshold_eq_channel, opt => opt.Ignore());
                Mapper.CreateMap<EqWarning, EqWarningModel>();
                Mapper.CreateMap<EqWarningModel, EqWarning>().ForMember(x => x.WARNING_WARNING_PARAMITEMS, opt => opt.Ignore()).ForMember(x => x.Warning_EQUIPMENT, opt => opt.Ignore()).ForMember(x => x.Warning_EQ_CHANNEL, opt => opt.Ignore());
                Mapper.CreateMap<Equipment, EquipmentModel>();
                Mapper.CreateMap<EquipmentModel, Equipment>().ForMember(x => x.EQUIPMENT_TYPE_PARAMITEMS, opt => opt.Ignore()).ForMember(x => x.Location, opt => opt.Ignore());


                Mapper.CreateMap<FormNote, FormNoteModel>();
                Mapper.CreateMap<FormNoteModel, FormNote>().ForMember(x => x.NOTES_ROUND, opt => opt.Ignore()).ForMember(x => x.NOTES_WARNING_PARAMITEMS, opt => opt.Ignore()); //.ForMember(x => x.NOTES_DEVICE, opt => opt.Ignore())

        }

        public int Order
        {
            get { return 1; }
        }
    }
*/

}