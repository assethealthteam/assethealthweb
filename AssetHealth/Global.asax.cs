using Ares.Core.Framework;
using Ares.Core.Framework.Logging;
using Ares.Core.Framework.Logging.Business;
using Ares.UMS.DTO.Sites;
using Ares.UMS.DTO.Users;
using Ares.UMS.Svc;
using Ares.Web.Helpers;
using EF6.BulkInsert;
using EF6.BulkInsert.Providers;
using log4net;
using log4net.Core;
using StackExchange.Profiling;
using StackExchange.Profiling.EntityFramework6;
using StackExchange.Profiling.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace AssetHealth
{
    public class WebApiApplication : System.Web.HttpApplication
    {

        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(WebApiApplication));
        protected void Application_Start()
        {

            AreaRegistration.RegisterAllAreas();
            //GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            //RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            Log.Debug("Application_Start");

            ProviderFactory.Register<MySqlBulkInsertProvider>("MySql.Data.MySqlClient.MySqlConnection");

            /*
            //miniprofiler
            if (FrameworkCtx.Current.Resolve<SiteInformationSettings>().DisplayMiniProfilerInPublicSite)
            {
                GlobalFilters.Filters.Add(new ProfilingActionFilter());
                //MiniProfilerEF6.Initialize();
            }
            */

        }

        protected void Application_End()
        {

            HttpRuntime runtime = (HttpRuntime)typeof(System.Web.HttpRuntime).InvokeMember("_theRuntime", BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.GetField, null, null, null);

            if (runtime == null)
                return;

            string shutDownMessage = (string)runtime.GetType().InvokeMember("_shutDownMessage", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.GetField, null, runtime, null);

            string shutDownStack = (string)runtime.GetType().InvokeMember("_shutDownStack", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.GetField, null, runtime, null);


            Log.Debug("Application_End");

            Log.Debug(String.Format("\r\n\r\n_shutDownMessage={0}\r\n\r\n_shutDownStack={1}", shutDownMessage, shutDownStack));

        }


        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            //ignore static resources
            var webHelper = FrameworkCtx.Current.Resolve<IWebHelper>();
            if (webHelper.IsStaticResource(this.Request))
                return;

            //keep alive page requested (we ignore it to prevent creating a guest customer records)
            string keepAliveUrl = string.Format("{0}keepalive/index", webHelper.GetSiteLocation());
            if (webHelper.GetThisPageUrl(false).StartsWith(keepAliveUrl, StringComparison.InvariantCultureIgnoreCase))
                return;

            //miniprofiler
            if (FrameworkCtx.Current.Resolve<SiteInformationSettings>().DisplayMiniProfilerInPublicSite)
            {
                StackExchange.Profiling.MiniProfiler.Start();
                //store a value indicating whether profiler was started
                HttpContext.Current.Items["ares.MiniProfilerStarted"] = true;
            }
        }


        protected void Application_EndRequest(object sender, EventArgs e)
        {
            //miniprofiler
            var miniProfilerStarted = HttpContext.Current.Items.Contains("ares.MiniProfilerStarted") && Convert.ToBoolean(HttpContext.Current.Items["ares.MiniProfilerStarted"]);
            if (miniProfilerStarted)
            {
                StackExchange.Profiling.MiniProfiler.Stop();
            }
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            var _workContext = FrameworkCtx.Current.Resolve<IWorkContext>();
            var _userSettings = FrameworkCtx.Current.Resolve<UserSettings>();
            var businessLog = FrameworkCtx.Current.Resolve<IBusinessLogger>();

            if (_workContext.CurrentUser != null)
                businessLog.Log(new LogItem() { Category = "Deneme", LogType = 107, Message = (_userSettings.UsernamesEnabled ? _workContext.CurrentUser.Username : _workContext.CurrentUser.Email) + " User Started a new session" });

            /*
            var businessLog = LogManager.GetLogger("BusinessLogger").Logger;
            
            if(_workContext.CurrentUser != null)
                businessLog.Log(typeof(WebApiApplication), Level.Info, (_userSettings.UsernamesEnabled ? _workContext.CurrentUser.Username : _workContext.CurrentUser.Email) + " User Started a new session" , null);
                */
        }


    }
}
