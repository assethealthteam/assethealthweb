using Ares.Core.Configuration;
using Ares.Core.Framework;
using Ares.Core.Framework.DI;
using Ares.Sync.ChangeManagement;
using Ares.Sync.ChangeManagement.Interfaces;
using Ares.Sync.DTO;
using Ares.Sync.Services;
using Ares.Sync.Services.Interfaces;
using Ares.UMS.Svc.Configuration;
using AssetHealth.ChangeSetItemSerializerFactory;
using AssetHealth.Common.Svc;
using AssetHealth.Common.Svc.Analyzer;
using Autofac;
using Autofac.Builder;
using Autofac.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace AssetHealth.DI
{
    public class DependencyRegistrar : IDependencyRegistrar
    {


        public virtual void Register(ContainerBuilder builder, ContainerManager containerManager, ITypeFinder typeFinder, AresConfig config)
        {
            
            builder.RegisterType<FormSvc>().As<IFormSvc>().InstancePerLifetimeScope();
            builder.RegisterType<FormAnswerSvc>().As<IFormAnswerSvc>().InstancePerLifetimeScope();
            builder.RegisterType<FormAnswerRoundSvc>().As<IFormAnswerRoundSvc>().InstancePerLifetimeScope();
            builder.RegisterType<FormQuestionSvc>().As<IFormQuestionSvc>().InstancePerLifetimeScope();
            builder.RegisterType<FormQuestionOptionSvc>().As<IFormQuestionOptionSvc>().InstancePerLifetimeScope();
            builder.RegisterType<ParameterGroupSvc>().As<IParameterGroupSvc>().InstancePerLifetimeScope();
            builder.RegisterType<ParameterItemSvc>().As<IParameterItemSvc>().InstancePerLifetimeScope();
            builder.RegisterType<FormNoteSvc>().As<IFormNoteSvc>().InstancePerLifetimeScope();


            builder.RegisterType<EqChannelSvc>().As<IEqChannelSvc>().InstancePerLifetimeScope();
            builder.RegisterType<EqLocationSvc>().As<IEqLocationSvc>().InstancePerLifetimeScope();
            builder.RegisterType<EqThresholdSvc>().As<IEqThresholdSvc>().InstancePerLifetimeScope();
            builder.RegisterType<EqWarningSvc>().As<IEqWarningSvc>().InstancePerLifetimeScope();
            builder.RegisterType<EquipmentSvc>().As<IEquipmentSvc>().InstancePerLifetimeScope();

            builder.RegisterType<AaExcelImportSvc>().As<IAaExcelImportSvc>().InstancePerLifetimeScope();
            builder.RegisterType<AnswerAnalyzer>().As<IAnswerAnalyzer>().InstancePerLifetimeScope();

            builder.RegisterType<ChangeSetService>().As<IChangeSetService>().InstancePerLifetimeScope();
            builder.RegisterType<BaseChangeSetItemFactoryStrategy>().As<IChangeSetItemSerializerStrategy>().InstancePerLifetimeScope();


            builder.Register((c, p) => new FormAnswerRoundChangeSetItemFactory(p.Named<int>("cihazId"), p.Named<SynchronizationDetail>("detail"))).Keyed<IChangeSetItemSerializerFactory>("formanswerround").PropertiesAutowired().InstancePerLifetimeScope();
            builder.Register((c, p) => new FormAnswerChangeSetItemFactory(p.Named<int>("cihazId"), p.Named<SynchronizationDetail>("detail"))).Keyed<IChangeSetItemSerializerFactory>("formanswer").PropertiesAutowired().InstancePerLifetimeScope();

            //builder.Register<LoggingPipelineModule>(b => new LoggingPipelineModule()).PropertiesAutowired().InstancePerLifetimeScope();

            builder.RegisterSource(new SettingsSource());

        }


        public class SettingsSource : IRegistrationSource
        {
            static readonly MethodInfo BuildMethod = typeof(SettingsSource).GetMethod(
                "BuildRegistration",
                BindingFlags.Static | BindingFlags.NonPublic);

            public IEnumerable<IComponentRegistration> RegistrationsFor(
                    Service service,
                    Func<Service, IEnumerable<IComponentRegistration>> registrations)
            {
                var ts = service as TypedService;
                if (ts != null && typeof(ISettings).IsAssignableFrom(ts.ServiceType))
                {
                    var buildMethod = BuildMethod.MakeGenericMethod(ts.ServiceType);
                    yield return (IComponentRegistration)buildMethod.Invoke(null, null);
                }
            }

            [global::System.Reflection.Obfuscation(Exclude = true, Feature = "renaming")]
            static IComponentRegistration BuildRegistration<TSettings>() where TSettings : ISettings, new()
            {
                return RegistrationBuilder
                    .ForDelegate((c, p) =>
                    {
                        return c.Resolve<ISettingSvc>().LoadSetting<TSettings>();
                    })
                    .InstancePerLifetimeScope()
                    .CreateRegistration();
            }

            public bool IsAdapterForIndividualComponents { get { return false; } }
        }

        public int Order
        {
            get { return 100; }
        }

    }
}