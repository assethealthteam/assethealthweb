using Ares.UMS.DTO.Configuration;
using Ares.UMS.DTO.Security;
using Ares.UMS.DTO.Users;
using AutoMapper;
using AssetHealth.Models.User;
using AssetHealth.Common.DTO;
using AssetHealth.Models.Synchronization_log;
using AssetHealth.Models.Synchronization_detail;
using AssetHealth.Models.Synchronization;
using AssetHealth.Models.Form_question_option;
using AssetHealth.Models.Form_question;
using AssetHealth.Models.Form_answer;
using AssetHealth.Models.Device;
using AssetHealth.Models.Form;
using Ares.UMS.DTO.Localization;
using AssetHealth.Models.Language;
using AssetHealth.Models.Eq_channel;
using AssetHealth.Models.Eq_warning;
using AssetHealth.Models.Eq_threshold;
using AssetHealth.Models.Equipment;
using AssetHealth.Models.Eq_location;
using Ares.Web.Models.Parameter_group;
using Ares.Web.Models.Parameter_item;
using Ares.Sync.DTO;
using AssetHealth.Models.Form_answer_round;
using AssetHealth.Models.Form_notes;
using Ares.Web.Extensions;
using Ares.Core.Framework.Mapper;
using AssetHealth.Models.Synchronization_device;

namespace AssetHealth.Extensions
{
    public static class MappingExtensions
    {

        public static UserModel ToModel(this User entity)
        {
            if (entity == null)
                return null;
           
            var model = entity.MapTo<User, Models.User.UserModel>();
            model.First_name = entity.FirstName;
            model.Last_name = entity.LastName;
            model.User_guid = entity.UserGuid;
            //JobTitle = entity.JobTitle;
            model.Birth_date = entity.BirthDate;
            model.Username = entity.Username;
            model.IdentityNumber = entity.IdentityNumber;
            model.Email = entity.Email;
            model.OrgUnit_id = entity.OrgUnit_Id;
            model.Jobtitle_id = entity.JobTitle_Id;
            model.Active = entity.Active.GetValueOrDefault();
            model.Notes = entity.Notes;

            //Password = entity.Password,
            model.UserPicture = new Ares.Web.Models.Media.PictureModel();

            return model;
        }
        

        public static User ToEntity(this UserModel model)
        {
            return model.MapTo<UserModel, User>();
        }

        public static User ToEntity(this UserModel model, User destination)
        {
            User result = model.MapTo(destination);


            return result;
        }
        

        public static Role ToEntity(this Models.User.RoleModel model)
        {
            return model.MapTo<Models.User.RoleModel, Role>();
        }

        public static Role ToEntity(this Models.User.RoleModel model, Role destination)
        {
            return model.MapTo(destination);
        }

        public static Models.User.RoleModel ToModel(this Role entity)
        {
            return entity.MapTo<Role, Models.User.RoleModel>();
        }


        public static Permission ToEntity(this Models.User.PermissionModel model, Permission destination)
        {
            return model.MapTo(destination);
        }

        public static Permission ToEntity(this Models.User.PermissionModel model)
        {
            return model.MapTo<Models.User.PermissionModel, Permission>();
        }

        public static Models.User.PermissionModel ToModel(this Permission entity)
        {
            return entity.MapTo<Permission, Models.User.PermissionModel>();
        }


        public static Menu ToEntity(this Models.Configuration.MenuModel model, Menu destination)
        {
            return model.MapTo(destination);
        }

        public static Models.Configuration.MenuModel ToModel(this Menu entity, string configurationName = AutoMapperConfiguration.DEFAULT_CONFIGURATION_NAME)
        {
            return entity.MapTo<Menu, Models.Configuration.MenuModel>(configurationName);
        }

        public static Menu ToEntity(this Models.Configuration.MenuModel model)
        {
            return model.MapTo<Models.Configuration.MenuModel, Menu>();
        }



        public static Device ToEntity(this DeviceModel model, string configurationName = AutoMapperConfiguration.DEFAULT_CONFIGURATION_NAME)
        {
            return model.MapTo<DeviceModel, Device>(configurationName);
        }

        public static Device ToEntity(this DeviceModel model, Device destination, string configurationName = AutoMapperConfiguration.DEFAULT_CONFIGURATION_NAME)
        {
            return model.MapTo(destination, configurationName);
        }

        public static DeviceModel ToModel(this Device entity, string configurationName = AutoMapperConfiguration.DEFAULT_CONFIGURATION_NAME)
        {
            return entity.MapTo<Device, DeviceModel>(configurationName);
        }
        public static Form ToEntity(this FormModel model, string configurationName = AutoMapperConfiguration.DEFAULT_CONFIGURATION_NAME)
        {
            return model.MapTo<FormModel, Form>(configurationName);
        }

        public static Form ToEntity(this FormModel model, Form destination, string configurationName = AutoMapperConfiguration.DEFAULT_CONFIGURATION_NAME)
        {
            return model.MapTo(destination, configurationName);
        }

        public static FormModel ToModel(this Form entity, string configurationName = AutoMapperConfiguration.DEFAULT_CONFIGURATION_NAME)
        {
            return entity.MapTo<Form, FormModel>(configurationName);
        }
        public static FormAnswer ToEntity(this FormAnswerModel model, string configurationName = AutoMapperConfiguration.DEFAULT_CONFIGURATION_NAME)
        {
            return model.MapTo<FormAnswerModel, FormAnswer>(configurationName);
        }

        public static FormAnswer ToEntity(this FormAnswerModel model, FormAnswer destination, string configurationName = AutoMapperConfiguration.DEFAULT_CONFIGURATION_NAME)
        {
            return model.MapTo(destination, configurationName);
        }

        public static FormAnswerModel ToModel(this FormAnswer entity, string configurationName = AutoMapperConfiguration.DEFAULT_CONFIGURATION_NAME)
        {
            return entity.MapTo<FormAnswer, FormAnswerModel>(configurationName);
        }

        public static FormAnswerRound ToEntity(this FormAnswerRoundModel model)
        {
            return model.MapTo<FormAnswerRoundModel, FormAnswerRound>();
        }

        public static FormAnswerRound ToEntity(this FormAnswerRoundModel model, FormAnswerRound destination)
        {
            return model.MapTo(destination);
        }

        public static FormAnswerRoundModel ToModel(this FormAnswerRound entity)
        {
            return entity.MapTo<FormAnswerRound, FormAnswerRoundModel>();
        }

        public static FormQuestion ToEntity(this FormQuestionModel model, string configurationName = AutoMapperConfiguration.DEFAULT_CONFIGURATION_NAME)
        {
            return model.MapTo<FormQuestionModel, FormQuestion>(configurationName);
        }

        public static FormQuestion ToEntity(this FormQuestionModel model, FormQuestion destination, string configurationName = AutoMapperConfiguration.DEFAULT_CONFIGURATION_NAME)
        {
            return model.MapTo(destination, configurationName);
        }

        public static FormQuestionModel ToModel(this FormQuestion entity, string configurationName = AutoMapperConfiguration.DEFAULT_CONFIGURATION_NAME)
        {
            return entity.MapTo<FormQuestion, FormQuestionModel>(configurationName);
        }

        public static FormQuestionOption ToEntity(this FormQuestionOptionModel model)
        {
            return model.MapTo<FormQuestionOptionModel, FormQuestionOption>();
        }

        public static FormQuestionOption ToEntity(this FormQuestionOptionModel model, FormQuestionOption destination)
        {
            return model.MapTo(destination);
        }

        public static FormQuestionOptionModel ToModel(this FormQuestionOption entity)
        {
            return entity.MapTo<FormQuestionOption, FormQuestionOptionModel>();
        }
        public static ParameterGroup ToEntity(this ParameterGroupModel model)
        {
            return model.MapTo<ParameterGroupModel, ParameterGroup>();
        }

        public static ParameterGroup ToEntity(this ParameterGroupModel model, ParameterGroup destination)
        {
            return model.MapTo(destination);
        }

        public static ParameterGroupModel ToModel(this ParameterGroup entity)
        {
            return entity.MapTo<ParameterGroup, ParameterGroupModel>();
        }
        public static ParameterItem ToEntity(this ParameterItemModel model)
        {
            return model.MapTo<ParameterItemModel, ParameterItem>();
        }

        public static ParameterItem ToEntity(this ParameterItemModel model, ParameterItem destination)
        {
            return model.MapTo(destination);
        }

        public static ParameterItemModel ToModel(this ParameterItem entity)
        {
            return entity.MapTo<ParameterItem, ParameterItemModel>();
        }
        public static Synchronization ToEntity(this SynchronizationModel model, string configurationName = AutoMapperConfiguration.DEFAULT_CONFIGURATION_NAME)
        {
            return model.MapTo<SynchronizationModel, Synchronization>();
        }

        public static Synchronization ToEntity(this SynchronizationModel model, Synchronization destination, string configurationName = AutoMapperConfiguration.DEFAULT_CONFIGURATION_NAME)
        {
            return model.MapTo(destination);
        }

        public static SynchronizationModel ToModel(this Synchronization entity, string configurationName = AutoMapperConfiguration.DEFAULT_CONFIGURATION_NAME)
        {
            return entity.MapTo<Synchronization, SynchronizationModel>();
        }
        public static SynchronizationDetail ToEntity(this SynchronizationDetailModel model, string configurationName = AutoMapperConfiguration.DEFAULT_CONFIGURATION_NAME)
        {
            return model.MapTo<SynchronizationDetailModel, SynchronizationDetail>();
        }

        public static SynchronizationDetail ToEntity(this SynchronizationDetailModel model, SynchronizationDetail destination, string configurationName = AutoMapperConfiguration.DEFAULT_CONFIGURATION_NAME)
        {
            return model.MapTo(destination);
        }

        public static SynchronizationDetailModel ToModel(this SynchronizationDetail entity, string configurationName = AutoMapperConfiguration.DEFAULT_CONFIGURATION_NAME)
        {
            return entity.MapTo<SynchronizationDetail, SynchronizationDetailModel>();
        }
		public static SynchronizationDevice  ToEntity(this SynchronizationDeviceModel model, string configurationName = AutoMapperConfiguration.DEFAULT_CONFIGURATION_NAME)
        {
            return model.MapTo<SynchronizationDeviceModel, SynchronizationDevice>();
        }

        public static SynchronizationDevice ToEntity(this SynchronizationDeviceModel model, SynchronizationDevice destination, string configurationName = AutoMapperConfiguration.DEFAULT_CONFIGURATION_NAME)
        {
            return model.MapTo(destination);
        }

        public static SynchronizationDeviceModel ToModel(this SynchronizationDevice entity, string configurationName = AutoMapperConfiguration.DEFAULT_CONFIGURATION_NAME)
        {
            return entity.MapTo<SynchronizationDevice, SynchronizationDeviceModel>();
        }
        public static SynchronizationLog ToEntity(this SynchronizationLogModel model, string configurationName = AutoMapperConfiguration.DEFAULT_CONFIGURATION_NAME)
        {
            return model.MapTo<SynchronizationLogModel, SynchronizationLog>();
        }

        public static SynchronizationLog ToEntity(this SynchronizationLogModel model, SynchronizationLog destination, string configurationName = AutoMapperConfiguration.DEFAULT_CONFIGURATION_NAME)
        {
            return model.MapTo(destination);
        }

        public static SynchronizationLogModel ToModel(this SynchronizationLog entity, string configurationName = AutoMapperConfiguration.DEFAULT_CONFIGURATION_NAME)
        {
            return entity.MapTo<SynchronizationLog, SynchronizationLogModel>();
        }



        public static LocaleResource ToEntity(this LocaleResourceModel model)
        {
            return model.MapTo<LocaleResourceModel, LocaleResource>();
        }

        public static LocaleResource ToEntity(this LocaleResourceModel model, LocaleResource destination)
        {
            return model.MapTo(destination);
        }

        public static LocaleResourceModel ToModel(this LocaleResource entity)
        {
            return entity.MapTo<LocaleResource, LocaleResourceModel>();
        }

        public static EqChannel ToEntity(this EqChannelModel model, string configurationName = AutoMapperConfiguration.DEFAULT_CONFIGURATION_NAME)
        {
            return model.MapTo<EqChannelModel, EqChannel>(configurationName);
        }

        public static EqChannel ToEntity(this EqChannelModel model, EqChannel destination, string configurationName = AutoMapperConfiguration.DEFAULT_CONFIGURATION_NAME)
        {
            return model.MapTo(destination, configurationName);
        }

        public static EqChannelModel ToModel(this EqChannel entity, string configurationName = AutoMapperConfiguration.DEFAULT_CONFIGURATION_NAME)
        {
            return entity.MapTo<EqChannel, EqChannelModel>(configurationName);
        }
        public static EqLocation ToEntity(this EqLocationModel model, string configurationName = AutoMapperConfiguration.DEFAULT_CONFIGURATION_NAME)
        {
            return model.MapTo<EqLocationModel, EqLocation>(configurationName);
        }

        public static EqLocation ToEntity(this EqLocationModel model, EqLocation destination, string configurationName = AutoMapperConfiguration.DEFAULT_CONFIGURATION_NAME)
        {
            return model.MapTo(destination, configurationName);
        }

        public static EqLocationModel ToModel(this EqLocation entity, string configurationName = AutoMapperConfiguration.DEFAULT_CONFIGURATION_NAME)
        {
            return entity.MapTo<EqLocation, EqLocationModel>(configurationName);
        }

        public static EqThreshold ToEntity(this EqThresholdModel model)
        {
            return model.MapTo<EqThresholdModel, EqThreshold>();
        }

        public static EqThreshold ToEntity(this EqThresholdModel model, EqThreshold destination)
        {
            return model.MapTo(destination);
        }

        public static EqThresholdModel ToModel(this EqThreshold entity)
        {
            return entity.MapTo<EqThreshold, EqThresholdModel>();
        }
        public static EqWarning ToEntity(this EqWarningModel model)
        {
            return model.MapTo<EqWarningModel, EqWarning>();
        }

        public static EqWarning ToEntity(this EqWarningModel model, EqWarning destination)
        {
            return model.MapTo(destination);
        }

        public static EqWarningModel ToModel(this EqWarning entity)
        {
            return entity.MapTo<EqWarning, EqWarningModel>();
        }
        public static Equipment ToEntity(this EquipmentModel model)
        {
            return model.MapTo<EquipmentModel, Equipment>();
        }

        public static Equipment ToEntity(this EquipmentModel model, Equipment destination)
        {
            return model.MapTo(destination);
        }

        public static EquipmentModel ToModel(this Equipment entity)
        {
            return entity.MapTo<Equipment, EquipmentModel>();
        }
        public static FormNote ToEntity(this FormNoteModel model)
        {
            return model.MapTo<FormNoteModel, FormNote>();
        }

        public static FormNote ToEntity(this FormNoteModel model, FormNote destination)
        {
            return model.MapTo(destination);
        }

        public static FormNoteModel ToModel(this FormNote entity)
        {
            return entity.MapTo<FormNote, FormNoteModel>();
        }


    }
}