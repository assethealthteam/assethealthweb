using Ares.Core.Framework;
using System.Web.Mvc;
using System.Web.Routing;
using Ares.Web.Logging.Business;
using FluentValidation.Mvc;
using Ares.Web.Validators;
using System.Web;
using Ares.UMS.Svc.Tasks;
using Ares.Web.Helpers;
using Ares.UMS.DTO.Sites;
using StackExchange.Profiling.EntityFramework6;
using StackExchange.Profiling.Mvc;
using AssetHealth.MiniProfiler;
using StackExchange.Profiling.Storage;
using System;
using Ares.Core.Framework.Logging;
using Ares.Web.ModelBinder;
using AssetHealth.KendoSerializer;

[assembly: WebActivatorEx.PostApplicationStartMethod(typeof(AssetHealth.AresStarter), "Initialize")]
[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(AssetHealth.AresStarter), "PreApplicationStart")]
[assembly: WebActivatorEx.ApplicationShutdownMethod(typeof(AssetHealth.AresStarter), "End")]

namespace AssetHealth 
{
    public static class AresStarter
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(AresStarter));

        private static bool _startWasCalled;

        public static void Start()
        {

            AresStarter.Initialize();
        }

        public static void End()
        {
            TaskManager.Instance.Stop();
        }
        public static void PreApplicationStart()
        {
            //RijndaelHelper.SetConnectionString("connectionString", "DefaultConnection", "System.Data.SqlClient");
            log4net.Config.XmlConfigurator.Configure();
        }
        public static void Initialize()
        {
            if (_startWasCalled) return;

            _startWasCalled = true;


            FrameworkCtx.Initialize(false);
            //DynamicModuleUtility.RegisterModule(typeof(Ares.Web.DI.AutofacRequestLifetimeHttpModule));

            //fluent validation
            //FluentValidationModelValidatorProvider.Configure();

            /*
            FluentValidationModelValidationFactory validationFactory = (metadata, context, rule, validator) => new RemoteFluentValidationPropertyValidator(metadata, context, rule, validator);
            FluentValidationModelValidatorProvider.Configure(x => x.Add(typeof(RemoteValidator), validationFactory));
            var validationProvider = new FluentValidationModelValidatorProvider(new AresValidatorFactory());
            validationProvider.Add(typeof(RemoteValidator), validationFactory);
            ModelValidatorProviders.Providers.Add(validationProvider);
            DataAnnotationsModelValidatorProvider.AddImplicitRequiredAttributeForValueTypes = false;
            */
            DataAnnotationsModelValidatorProvider.AddImplicitRequiredAttributeForValueTypes = false;
            ModelValidatorProviders.Providers.Add(new FluentValidationModelValidatorProvider(new AresValidatorFactory()));


            /*
            ModelBinders.Binders.Add(typeof(DateTime), new DateTimeModelBinder());
            ModelBinders.Binders.Add(typeof(DateTime?), new DateTimeModelBinder());
            */

            ModelBinders.Binders.Add(typeof(DateTime), new DateTimeModelBinder());
            ModelBinders.Binders.Add(typeof(DateTime?), new DateTimeModelBinder());

            if (HttpContext.Current != null && HttpContext.Current.Application["TaskManagerStarted"] == null)
            {
                HttpContext.Current.Application["TaskManagerStarted"] = 1;
                TaskManager.Instance.Initialize();
            }

            if (FrameworkCtx.Current.Resolve<SiteInformationSettings>().DisplayMiniProfilerInPublicSite)
            {
                GlobalFilters.Filters.Add(new ProfilingActionFilter());
                //IStorage currentStorage = StackExchange.Profiling.MiniProfiler.Settings.Storage;
                StackExchange.Profiling.MiniProfiler.Settings.Storage = new MultiStorageProvider(new StackExchange.Profiling.Storage.HttpRuntimeCacheStorage(TimeSpan.FromDays(1)), new Log4NetStorage(FrameworkCtx.Current.Resolve<ILogger>()));
                //StackExchange.Profiling.MiniProfiler.Settings.Storage = new Log4NetStorage(FrameworkCtx.Current.Resolve<ILogger>());

                //MiniProfilerEF6.Initialize();
            }

            //Kendo.Mvc.Infrastructure.DI.Current.Register<Kendo.Mvc.Infrastructure.IJavaScriptInitializer>(() => new CustomKendoInitializer());

            Log.Debug("AresStarter Initialized");


        }

    }
}