﻿using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AssetHealth;
using AssetHealth.Controllers;
using Ares.Core.Framework;
using Ares.Core.Framework.Logging.Business;
using Ares.Core.Framework.Logging;
using Ares.UMS.Svc.Users;
using Ares.UMS.DTO.Users;

namespace AssetHealth.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest : IoCSupportedTest
    {


        [TestMethod]
        public void Index()
        {
            // Arrange
            HomeController controller = FrameworkCtx.Current.Resolve<HomeController>();
            

            // Act
            ViewResult result = controller.Index(null) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("Home Page", result.ViewBag.Title);
        }


        [TestMethod]
        public void TestLog()
        {
            AresStarter.Initialize();
            //IBusinessLogger log = FrameworkCtx.Current.Resolve<IBusinessLogger>();
            ILogger log = FrameworkCtx.Current.Resolve<ILogger>();
            log.Log("test");

            IUserSvc userSvc = FrameworkCtx.Current.Resolve<IUserSvc>();
            Ares.UMS.DTO.Users.User user = userSvc.GetById(1);

            log.Log(user.Username);

        }
    }
}
