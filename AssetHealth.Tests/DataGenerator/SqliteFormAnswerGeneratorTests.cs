﻿using Ares.Core.Framework;
using Ares.Core.Helpers;
using Ares.Sync.ChangeManagement;
using Ares.Sync.DTO;
using Ares.Sync.DTO.Enums;
using Ares.Sync.Entities.Client;
using Ares.Sync.Enums;
using Ares.Sync.Services.Interfaces;
using Ares.Sync.Svc;
using Ares.UMS.Svc.Configuration;
using Ares.Web.JSON;
using Ares.Web.Mvc.Entities;
using AssetHealth.Common.DTO;
using AssetHealth.Common.DTO.Enums;
using AssetHealth.Common.Svc;
using AssetHealth.Common.Svc.Analyzer;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace AssetHealth.Tests.DataGenerator
{
    [TestClass]
    public class SqliteFormAnswerGeneratorTests : IoCSupportedTest
    {
        IChangeSetService _ChangeSetService;

        private string DatabasePath = "../../assethealth-db";

        private readonly JavaScriptSerializer _js = new JavaScriptSerializer {  MaxJsonLength = 41943040 };
        

        [TestInitialize]
        public void InitializeTest()
        {
            //this._ChangeSetService = FrameworkCtx.Current.Resolve<IChangeSetService>();

        }

        [TestMethod]
        public void EncryptSqlite()
        {
            SqliteDAL dal = new SqliteDAL();
            dal.EncryptSqlite(DatabasePath, "As!He%@lth2018");
            //dal.EncryptSqlite(DatabasePath, "2018");
        }

        [TestMethod]
        public void DecryptSqlite()
        {
            SqliteDAL dal = new SqliteDAL();
            dal.DecryptSqlite(DatabasePath, "As!He%@lth2018");
            //dal.DecryptSqlite(DatabasePath, "2018");
        }

        [TestMethod]
        public void SqliteSelectForm()
        {
            SqliteDAL dal = new SqliteDAL();
            string result = dal.SelectForm(DatabasePath, "As!He%@lth2018");
            Trace.WriteLine("Result " + result);
            //Assert.IsTrue(true);
        }

        [TestMethod]
        public void PrepareSqliteYearlyData()
        {
            // Çalıştırmadan Önce Aşağıdaki değişkenleri kontrol et
            int deviceID = 2; // Nox
            int locId = 12;
            int roleId = 8; // Otonom bakım
            int subtractStartDay = -1; // Geriye doğru 3 gün
            string username = "npk";

            SqliteDAL dal = new SqliteDAL();

            IEqLocationSvc iEqLocationSvc = FrameworkCtx.Current.Resolve<IEqLocationSvc>();
            ISynchronizationSvc iSynchronizationSvc = FrameworkCtx.Current.Resolve<ISynchronizationSvc>();
            IParameterItemSvc iParameterItemSvc = FrameworkCtx.Current.Resolve<IParameterItemSvc>();
            IEqChannelSvc iEqChannelSvc = FrameworkCtx.Current.Resolve<IEqChannelSvc>();
            IFormSvc formSvc = FrameworkCtx.Current.Resolve<IFormSvc>();
            IFormAnswerRoundSvc formAnswerRoundSvc = FrameworkCtx.Current.Resolve<IFormAnswerRoundSvc>();
            IDeviceSvc deviceSvc = FrameworkCtx.Current.Resolve<IDeviceSvc>();

            Device device = deviceSvc.GetAll().FirstOrDefault();
            //DateTime starTime = new DateTime(2017, 1, 1, 14, 05, 00);
            //DateTime starTime = DateTime.Now.AddDays(-1);
            DateTime starTime = DateTime.Now.AddDays(subtractStartDay);
            //DateTime starTime = DateTime.Now.AddYears(-1);
            DateTime endime = DateTime.Now;

            
            EqLocation location = iEqLocationSvc.GetById(locId);
            List<object> syncItems = new List<object>();

            
            for (DateTime tmpDateTime = starTime; tmpDateTime <= endime; tmpDateTime = tmpDateTime.AddDays(1))
            {
                long readNo = StringHelper.ConvertDateTimeToLong(tmpDateTime);
                FormAnswerRound round = dal.CreateAnswerRound(DatabasePath, roleId, deviceID, readNo, username);
                syncItems.Add(round);

                foreach (Equipment equipment in location.EQ_LOCATIONEquipment)
                {
                    foreach (Form form in dal.GetFormsByEquipment(DatabasePath, equipment.Id, roleId))
                    {
                        //Form form = formSvc.GetById(formId); // Tüprag için data oluştur

                        /*
                        if (form.FORMForm_question == null || form.FORMForm_question.Count <= 0)
                            break;
                            */



                        foreach (FormQuestion question in dal.GetFormQuestionsByForm(DatabasePath, form.Id))
                        {
                            string answer = "";
                            EqChannel channel = iEqChannelSvc.GetById(question.Eq_channel_id);
                            if (channel != null)
                            {
                                if (channel.Sensor_type == 70) // Sıcaklık
                                {
                                    answer = GetRandomNumberInt(0, 120).ToString();
                                }
                                else if (channel.Sensor_type == 81) // Görsel kontrol
                                {
                                    answer = GetRandomNumberInt(63, 66).ToString();
                                    //answer.Converted_answer = iParameterItemSvc.GetById(int.Parse(answer.Answer)).Title;
                                }
                                else if (channel.Sensor_type == 72) // Vibrasyon
                                {
                                    answer = GetRandomNumberInt(0, 10).ToString();
                                    //answer.Converted_answer = answer.Answer;
                                }


                            }

                            FormAnswer answerEntity = dal.CreateAnswer(DatabasePath, round.Id, deviceID, readNo, question.Id, answer, username);
                            syncItems.Add(answerEntity);
                        }


                    }
                } // end of loc
            }
            long syncId = dal.CreateSynchronization(DatabasePath);
            dal.CreateSynchronizationDetail(DatabasePath, syncId, syncItems);

        }



        public double GetRandomNumberDouble(double minimum, double maximum)
        {
            Random random = new Random();
            return random.NextDouble() * (maximum - minimum) + minimum;
        }

        public int GetRandomNumberInt(int minimum, int maximum)
        {
            Random random = new Random();
            return random.Next(minimum, maximum);
        }


        public static long ToFileTimeUtc(DateTime dateTime)
        {
            return dateTime.ToFileTimeUtc();
        }

        public static DateTime FromFileTimeUtc(long fileTimeUtc)
        {
            return DateTime.FromFileTimeUtc(fileTimeUtc);
        }

        [TestMethod]
        public void TestSqliteDateTime()
        {
            TimeSpan time = TimeSpan.FromMilliseconds(1534344691830);
            DateTime startdate = new DateTime(1970, 1, 1) + time;

            var utcDate = DateTime.SpecifyKind(startdate, DateTimeKind.Utc);
            DateTime localTime = utcDate.ToLocalTime();

            int year = startdate.Year;
        }


    }
}