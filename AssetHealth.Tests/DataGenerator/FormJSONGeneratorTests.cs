﻿using Ares.Core.Framework;
using Ares.Core.Helpers;
using Ares.Sync.ChangeManagement;
using Ares.Sync.DTO;
using Ares.Sync.DTO.Enums;
using Ares.Sync.Entities.Client;
using Ares.Sync.Enums;
using Ares.Sync.Services.Interfaces;
using Ares.Sync.Svc;
using Ares.UMS.Svc.Configuration;
using Ares.Web.JSON;
using Ares.Web.Mvc.Entities;
using AssetHealth.Common.DTO;
using AssetHealth.Common.DTO.Enums;
using AssetHealth.Common.Svc;
using AssetHealth.Common.Svc.Analyzer;
using AssetHealth.Common.Svc.JSONGenerator;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace AssetHealth.Tests.DataGenerator
{
    [TestClass]
    public class FormJSONGeneratorTests : IoCSupportedTest
    {
        IChangeSetService _ChangeSetService;

        private readonly JavaScriptSerializer _js = new JavaScriptSerializer {  MaxJsonLength = 41943040 };
        

        [TestInitialize]
        public void InitializeTest()
        {
            //this._ChangeSetService = FrameworkCtx.Current.Resolve<IChangeSetService>();

        }




        [TestMethod]
        public void FormJSONGenerator()
        {
            IFormQuestionSvc _iFormQuestionSvc = FrameworkCtx.Current.Resolve<IFormQuestionSvc>();
            _iFormQuestionSvc.fillEmptyJSONValues();

        }

        [TestMethod]
        public void FormQuestionJSONGenerator()
        {

            IFormQuestionSvc _iFormQuestionsvc = FrameworkCtx.Current.Resolve<IFormQuestionSvc>();
            List<FormQuestion> questions =_iFormQuestionsvc.GetAll().Where(q => q.Json == null || q.Json == "").ToList();

            JsonFormWizard jsonWizard = new JsonFormWizard();

            foreach (FormQuestion question in questions)
            {
                question.Json = jsonWizard.GenerateJSONFromDTOFormQuestion(question);
                _iFormQuestionsvc.Update(question);
            }

            
            

        }



    }
}