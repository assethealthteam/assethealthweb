﻿using Ares.Core.Framework;
using Ares.Core.Helpers;
using Ares.Sync.ChangeManagement;
using Ares.Sync.DTO;
using Ares.Sync.DTO.Enums;
using Ares.Sync.Entities.Client;
using Ares.Sync.Enums;
using Ares.Sync.Services.Interfaces;
using Ares.Sync.Svc;
using Ares.UMS.Svc.Configuration;
using Ares.Web.JSON;
using Ares.Web.Mvc.Entities;
using AssetHealth.Common.DTO;
using AssetHealth.Common.DTO.Enums;
using AssetHealth.Common.Svc;
using AssetHealth.Common.Svc.Analyzer;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace AssetHealth.Tests.DataGenerator
{
    [TestClass]
    public class FormAnswerGeneratorTests : IoCSupportedTest
    {
        IChangeSetService _ChangeSetService;

        private readonly JavaScriptSerializer _js = new JavaScriptSerializer {  MaxJsonLength = 41943040 };
        

        [TestInitialize]
        public void InitializeTest()
        {
            //this._ChangeSetService = FrameworkCtx.Current.Resolve<IChangeSetService>();

            GC.Collect();
            GC.WaitForPendingFinalizers();

        }


        [TestMethod]
        public void PrepareYearlyData()
        {
            // Çalıştırmadan Önce Aşağıdaki değişkenleri kontrol et
            int deviceID = 2; // Nox
            //int[] formIds = new int[] { 48, 49, 50 }; // Erkunt
            //int[] formIds = new int[] { 51 }; // Doğu Pres
            int[] formIds = new int[] { 52 }; // Tüprag
            //DateTime endDate = DateTime.Now;
            DateTime endDate = new DateTime(2018, 10, 6);
            int subtractStartDay = -90; // Geriye doğru 3 ay
            DateTime starTime = endDate.AddDays(subtractStartDay);
            string username = "tuprag_5s";



            ISynchronizationSvc iSynchronizationSvc = FrameworkCtx.Current.Resolve<ISynchronizationSvc>();
            IParameterItemSvc iParameterItemSvc = FrameworkCtx.Current.Resolve<IParameterItemSvc>();
            IEqChannelSvc iEqChannelSvc = FrameworkCtx.Current.Resolve<IEqChannelSvc>();
            IFormSvc formSvc = FrameworkCtx.Current.Resolve<IFormSvc>();
            IFormAnswerRoundSvc formAnswerRoundSvc = FrameworkCtx.Current.Resolve<IFormAnswerRoundSvc>();
            IDeviceSvc deviceSvc = FrameworkCtx.Current.Resolve<IDeviceSvc>();

            Device device = deviceSvc.GetAll().FirstOrDefault();

            //DateTime starTime = new DateTime(2017, 1, 1, 14, 05, 00);
            //DateTime starTime = DateTime.Now.AddDays(-1);
            
            //DateTime starTime = DateTime.Now.AddYears(-1);
            

            for (DateTime tmpDateTime = starTime; tmpDateTime <= endDate; tmpDateTime = tmpDateTime.AddDays(1))
            {
                
                long readNo = StringHelper.ConvertDateTimeToLong(tmpDateTime);
                FormAnswerRound round = new FormAnswerRound();
                round.Object_id = Guid.NewGuid().ToString();
                //round.Form_id = form.Id;
                round.Start_read_datetime = tmpDateTime;
                round.Read_no = readNo;
                round.Device_id = deviceID;
                round.Start_count = 1;
                round.FORM_ANSWER_ROUNDForm_answer = new List<FormAnswer>();
                round.Insert_datetime = StringHelper.ConvertDateTimeToString(DateTime.Now);

                foreach (Form form in formSvc.GetAll().Where(f => formIds.Contains(f.Id)).ToList())
                {
                    /*
                    round.Warning_type
                    */
                    foreach (FormQuestion question in form.FORMForm_question)
                    {
                        FormAnswer answer = new FormAnswer();
                        answer.Object_id = Guid.NewGuid().ToString();
                        answer.Form_question_id = question.Id;
                        answer.Read_no = readNo;
                        answer.Device_id = deviceID;
                        answer.Answer = ""; // null olamaz kontrolü var
                        answer.Insert_datetime = StringHelper.ConvertDateTimeToString(tmpDateTime);
                        answer.Actual_read_datetime = tmpDateTime;
                        answer.Username = username;

                        EqChannel channel = iEqChannelSvc.GetAll().Where(c => c.EQ_CHANNELForm_question.ToList().Any(v => v.Id == question.Id)).FirstOrDefault();
                        if (channel != null)
                        {
                            if (channel.Sensor_type == 70) // Sıcaklık
                            {
                                answer.Answer = GetRandomNumberInt(0, 120).ToString();
                                answer.Converted_answer = answer.Answer;
                            }
                            else if (channel.Sensor_type == 81) // Görsel kontrol
                            {
                                answer.Answer = GetRandomNumberInt(63, 66).ToString();
                                answer.Converted_answer = iParameterItemSvc.GetById(int.Parse(answer.Answer)).Title;
                            }
                            else if (channel.Sensor_type == 72) // Vibrasyon
                            {
                                answer.Answer = GetRandomNumberInt(0, 10).ToString();
                                answer.Converted_answer = answer.Answer;
                            }
                            else if (channel.Sensor_type == 77) // Aşınma
                            {
                                answer.Answer = GetRandomNumberInt(0, 40).ToString();
                                answer.Converted_answer = answer.Answer;
                            }


                        }

                        //answer.

                        round.FORM_ANSWER_ROUNDForm_answer.Add(answer);
                    }
                    
                }
                formAnswerRoundSvc.Insert(round);
            }

        }


        public double GetRandomNumberDouble(double minimum, double maximum)
        {
            Random random = new Random();
            return random.NextDouble() * (maximum - minimum) + minimum;
        }

        public int GetRandomNumberInt(int minimum, int maximum)
        {
            Random random = new Random();
            return random.Next(minimum, maximum);
        }


        [TestMethod]
        public void AnalyzeAnswers()
        {
            // Çalıştırmadan önce aşağıdaki değişkenleri kontrol et
            //int form_answer_round_start_id = 553; // analiz edilecek başlangıç form answer round id'si Erkunt
            //int form_answer_round_start_id = 735; // analiz edilecek başlangıç form answer round id'si Tüprag
            //int form_answer_round_start_id = 458; // analiz edilecek başlangıç form answer round id'si Toros
            int form_answer_round_start_id = 482; // analiz edilecek başlangıç form answer round id'si Toros
            int subtractDays = -90;  // analize 90 gün önceden itibaren başla
            DateTime endDate = DateTime.Now;
            //DateTime endDate = new DateTime(2018, 10, 6);
            DateTime startDate = endDate.AddDays(subtractDays);


            IFormAnswerRoundSvc formAnswerRoundSvc = FrameworkCtx.Current.Resolve<IFormAnswerRoundSvc>();
            IFormAnswerSvc formAnswerSvc = FrameworkCtx.Current.Resolve<IFormAnswerSvc>();

            IAnswerAnalyzer analyzer = FrameworkCtx.Current.Resolve<IAnswerAnalyzer>();

            
            //DateTime startDate = new DateTime(2018, 5, 1);
            long startDateLong = StringHelper.ConvertDateTimeToLong(startDate);

            new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;
 
                Console.WriteLine("Hello, world");
            }).Start();

            int lastRoundId = formAnswerRoundSvc.GetAll().OrderByDescending(r => r.Id).FirstOrDefault().Id;

            for (int tmpRoundId = form_answer_round_start_id; tmpRoundId <= lastRoundId; tmpRoundId++)
            {
                FormAnswerRound round = formAnswerRoundSvc.GetAll().Where(r => r.Id == tmpRoundId && r.Read_no >= startDateLong).FirstOrDefault();

                foreach (FormAnswer answer in round.FORM_ANSWER_ROUNDForm_answer)
                {
                    analyzer.AnalyzeAnswer(answer);
                }
                
            }
            
            /*
            Parallel.ForEach(
            formAnswerRoundSvc.GetAll().ToList(),
            round =>
            {
                foreach (FormAnswer answer in round.FORM_ANSWER_ROUNDForm_answer)
                {
                    analyzer.AnalyzeAnswer(answer);
                }
            });
            */
        }

        [TestMethod]
        public void TestSqliteDateTime()
        {
            TimeSpan time = TimeSpan.FromMilliseconds(1534344691830);
            DateTime startdate = new DateTime(1970, 1, 1) + time;

            var utcDate = DateTime.SpecifyKind(startdate, DateTimeKind.Utc);
            DateTime localTime = utcDate.ToLocalTime();

            int year = startdate.Year;
        }


    }
}