﻿using Ares.Core.Helpers;
using Ares.DTO;
using Ares.UMS.DTO.Media;
using AssetHealth.Common.DTO;
using AssetHealth.Common.Svc.JSONGenerator;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetHealth.Tests.DataGenerator
{
    public class SqliteDAL
    {
        public void EncryptSqlite(string DatabasePath, string password)
        {
            using (var connection = new SQLiteConnection("Data Source=" + DatabasePath))
            {
                try
                {
                    connection.Open();
                    connection.ChangePassword(password);

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    connection.Close();
                }


                
            }
        }

        public void DecryptSqlite(string DatabasePath, string password)
        {
            using (var connection = new SQLiteConnection("Data Source=" + DatabasePath + ";Password=" + password))
            {
                try
                {
                    connection.Open();
                    connection.ChangePassword(String.Empty);
                    //connection.ChangePassword((String)null);
                    connection.ChangePassword(default(byte[]));

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    connection.Close();
                }



            }
        }

        public String SelectForm(string DatabasePath, string password)
        {
            string result = "";
            using (var connection = new SQLiteConnection("Data Source=" + DatabasePath + ";Password=" + password))
            {
                try
                {
                    connection.Open();
                    SQLiteCommand cmd = new SQLiteCommand("SELECT * FROM FORM;", connection);
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            result = reader.GetString(reader.GetOrdinal("TITLE"));
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return result;
        }

        public List<Form> GetFormsByEquipment(string DatabasePath, int equipmentId, int roleId)
        {
            List<Form> result = new List<Form>();
            using (var connection = new SQLiteConnection("Data Source=" + DatabasePath ))
            {
                try
                {
                    connection.Open();
                    SQLiteCommand cmd = new SQLiteCommand("SELECT * FROM FORM WHERE EQUIPMENT_ID = " + equipmentId + " AND ROLE_ID = " + roleId, connection);
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Form tmpForm = new Form();
                            tmpForm.Id = reader.GetInt32(reader.GetOrdinal("_id"));
                            tmpForm.Object_id = reader.GetString(reader.GetOrdinal("OBJECT_ID"));
                            tmpForm.Category_id = reader.GetInt32(reader.GetOrdinal("CATEGORY_Id"));
                            tmpForm.Equipment_id = reader.GetInt32(reader.GetOrdinal("EQUIPMENT_ID"));
                            tmpForm.Role_id = reader.GetInt32(reader.GetOrdinal("ROLE_ID"));
                            tmpForm.Title = reader.GetString(reader.GetOrdinal("TITLE"));

                            result.Add(tmpForm);
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return result;
        }

        public List<FormQuestion> GetFormQuestionsByForm(string DatabasePath, int formId)
        {
            List<FormQuestion> result = new List<FormQuestion>();
            using (var connection = new SQLiteConnection("Data Source=" + DatabasePath))
            {
                try
                {
                    connection.Open();
                    SQLiteCommand cmd = new SQLiteCommand("SELECT * FROM FORM_QUESTION WHERE ACTIVE = 1 AND FORM_ID = " + formId + " ORDER BY DISPLAY_ORDER", connection);
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            FormQuestion tmpForm = new FormQuestion();
                            tmpForm.Id = reader.GetInt32(reader.GetOrdinal("_id"));
                            tmpForm.Object_id = reader.GetString(reader.GetOrdinal("OBJECT_ID"));
                            tmpForm.Category_id = reader.GetInt32(reader.GetOrdinal("CATEGORY_Id"));
                            tmpForm.Type_id = reader.GetInt32(reader.GetOrdinal("TYPE_Id"));
                            tmpForm.Form_id = reader.GetInt32(reader.GetOrdinal("FORM_Id"));
                            tmpForm.Eq_channel_id = reader.GetInt32(reader.GetOrdinal("EQ_CHANNEL_Id")); 
                            tmpForm.Code = reader.GetString(reader.GetOrdinal("CODE"));
                            tmpForm.Title = reader.GetString(reader.GetOrdinal("TITLE"));
                            tmpForm.Display_order = reader.GetInt32(reader.GetOrdinal("DISPLAY_ORDER"));
                            tmpForm.Active = reader.GetInt32(reader.GetOrdinal("ACTIVE")) == 1;
                            

                            result.Add(tmpForm);
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return result;
        }


        public long CreateForm(string DatabasePath, Form entity)
        {
            using (var connection = new SQLiteConnection("Data Source=" + DatabasePath))
            {
                try
                {
                    connection.Open();

                    try
                    {
                        SQLiteTransaction transaction = null;
                        transaction = connection.BeginTransaction();

                        SQLiteCommand cmd = new SQLiteCommand("INSERT INTO 'form' ( 'OBJECT_ID', 'CATEGORY_Id', 'DERIVED_Id', 'LANGUAGE_ID', 'PERIOD', 'EQUIPMENT_ID', 'ROLE_ID', 'SERVER_ID', 'CODE', 'TITLE', 'DESCRIPTION', 'NOTES', 'VERSION', 'IMAGE_PATH', 'FORM_JSON', 'FORM_JSON2', 'ACTIVE', 'DELETED', 'DISPLAY_ORDER', 'INSERT_USER_ID', 'UPDATE_USER_ID', 'INSERT_DATETIME', 'UPDATE_DATETIME') VALUES ( @OBJECT_ID, @CATEGORY_Id, @DERIVED_Id, @LANGUAGE_ID, @PERIOD, @EQUIPMENT_ID, @ROLE_ID, @SERVER_ID, @CODE, @TITLE, @DESCRIPTION, @NOTES, @VERSION, @IMAGE_PATH, @FORM_JSON, @FORM_JSON2, @ACTIVE, @DELETED, @DISPLAY_ORDER, @INSERT_USER_ID, @UPDATE_USER_ID, @INSERT_DATETIME, @UPDATE_DATETIME)", connection);
                        cmd.Parameters.Add(new SQLiteParameter("OBJECT_ID", entity.Object_id));
                        cmd.Parameters.Add(new SQLiteParameter("CATEGORY_Id", entity.Category_id));
                        cmd.Parameters.Add(new SQLiteParameter("DERIVED_Id", entity.Derived_id));
                        cmd.Parameters.Add(new SQLiteParameter("LANGUAGE_ID", entity.Language_id));
                        cmd.Parameters.Add(new SQLiteParameter("PERIOD", entity.Period));
                        cmd.Parameters.Add(new SQLiteParameter("EQUIPMENT_ID", entity.Equipment_id));
                        cmd.Parameters.Add(new SQLiteParameter("ROLE_ID", entity.Role_id));
                        cmd.Parameters.Add(new SQLiteParameter("SERVER_ID", entity.Server_id));
                        cmd.Parameters.Add(new SQLiteParameter("CODE", entity.Code));
                        cmd.Parameters.Add(new SQLiteParameter("TITLE", entity.Title));
                        cmd.Parameters.Add(new SQLiteParameter("DESCRIPTION", entity.Description));
                        cmd.Parameters.Add(new SQLiteParameter("NOTES", entity.Notes));
                        cmd.Parameters.Add(new SQLiteParameter("VERSION", entity.Version));
                        cmd.Parameters.Add(new SQLiteParameter("IMAGE_PATH", entity.Image_path));
                        cmd.Parameters.Add(new SQLiteParameter("FORM_JSON", entity.Form_json));
                        cmd.Parameters.Add(new SQLiteParameter("FORM_JSON2", entity.Form_json2));
                        cmd.Parameters.Add(new SQLiteParameter("ACTIVE", entity.Active));
                        cmd.Parameters.Add(new SQLiteParameter("DELETED", entity.Deleted));
                        cmd.Parameters.Add(new SQLiteParameter("DISPLAY_ORDER", entity.Display_order));
                        cmd.Parameters.Add(new SQLiteParameter("INSERT_USER_ID", entity.Insert_user_id));
                        cmd.Parameters.Add(new SQLiteParameter("UPDATE_USER_ID", entity.Update_user_id));
                        cmd.Parameters.Add(new SQLiteParameter("INSERT_DATETIME", entity.Insert_datetime));
                        cmd.Parameters.Add(new SQLiteParameter("UPDATE_DATETIME", entity.Update_datetime));

                        cmd.ExecuteNonQuery();
                        /*
                        SQLiteCommand sql_cmd= new SQLiteCommand("select seq from sqlite_sequence where name='form_answer_round'; ");
                        int newId = Convert.ToInt32(sql_cmd.ExecuteScalar());
                        */
                        long newId = connection.LastInsertRowId;

                        transaction.Commit();

                        return newId;

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
                catch (Exception e)
                {
                    /*
                    Application.Current.Dispatcher.Invoke((Action)(() =>
                    {
                        Hatalar.Add(e.Message);
                    }));
                    */
                }
                finally
                {
                    connection.Close();
                }
            }

            return -1;
        }

        public long CreateFormQuestion(string DatabasePath, FormQuestion entity)
        {
            using (var connection = new SQLiteConnection("Data Source=" + DatabasePath))
            {
                try
                {
                    connection.Open();

                    try
                    {
                        SQLiteTransaction transaction = null;
                        transaction = connection.BeginTransaction();

                        SQLiteCommand cmd = new SQLiteCommand("INSERT INTO 'form_question' ( 'OBJECT_ID', 'CATEGORY_Id', 'TYPE_Id', 'FORM_Id', 'DERIVED_Id', 'EQ_CHANNEL_Id', 'CODE', 'TITLE', 'HINT', 'LAST_ANSWER_ID', 'LAST_ANSWER_DATETIME', 'SERVER_ID', 'DESCRIPTION', 'VERSION', 'IMAGE_PATH', 'DEVICE_IMAGE_PATH', 'ESTIMATED_TIME', 'METHOD', 'HELP_TEXT', 'JSON', 'DISPLAY_ORDER', 'STEP', 'PERIOD', 'OPTION_GROUP_ID', 'V_REQUIRED', 'V_EMAIL', 'V_MIN_LENGTH', 'V_MAX_LENGTH', 'V_REGEX', 'ACTIVE', 'DELETED', 'INSERT_USER_ID', 'UPDATE_USER_ID', 'INSERT_DATETIME', 'UPDATE_DATETIME') VALUES ( @OBJECT_ID, @CATEGORY_Id, @TYPE_Id, @FORM_Id, @DERIVED_Id, @EQ_CHANNEL_Id, @CODE, @TITLE, @HINT, @LAST_ANSWER_ID, @LAST_ANSWER_DATETIME, @SERVER_ID, @DESCRIPTION, @VERSION, @IMAGE_PATH, @DEVICE_IMAGE_PATH, @ESTIMATED_TIME, @METHOD, @HELP_TEXT, @JSON, @DISPLAY_ORDER, @STEP, @PERIOD, @OPTION_GROUP_ID, @V_REQUIRED, @V_EMAIL, @V_MIN_LENGTH, @V_MAX_LENGTH, @V_REGEX, @ACTIVE, @DELETED, @INSERT_USER_ID, @UPDATE_USER_ID, @INSERT_DATETIME, @UPDATE_DATETIME)", connection);
                        cmd.Parameters.Add(new SQLiteParameter("OBJECT_ID", entity.Object_id));
                        cmd.Parameters.Add(new SQLiteParameter("CATEGORY_Id", entity.Category_id));
                        cmd.Parameters.Add(new SQLiteParameter("TYPE_Id", entity.Type_id));
                        cmd.Parameters.Add(new SQLiteParameter("FORM_Id", entity.Form_id));
                        cmd.Parameters.Add(new SQLiteParameter("DERIVED_Id", entity.Derived_id));
                        cmd.Parameters.Add(new SQLiteParameter("EQ_CHANNEL_Id", entity.Eq_channel_id));
                        cmd.Parameters.Add(new SQLiteParameter("CODE", entity.Code));
                        cmd.Parameters.Add(new SQLiteParameter("TITLE", entity.Title));
                        cmd.Parameters.Add(new SQLiteParameter("HINT", entity.Hint));
                        cmd.Parameters.Add(new SQLiteParameter("LAST_ANSWER_ID", entity.Last_answer_id));
                        cmd.Parameters.Add(new SQLiteParameter("LAST_ANSWER_DATETIME", entity.Last_answer_datetime));
                        cmd.Parameters.Add(new SQLiteParameter("SERVER_ID", entity.Server_id));
                        cmd.Parameters.Add(new SQLiteParameter("DESCRIPTION", entity.Description));
                        cmd.Parameters.Add(new SQLiteParameter("VERSION", entity.Version));
                        cmd.Parameters.Add(new SQLiteParameter("IMAGE_PATH", entity.Image_path));
                        cmd.Parameters.Add(new SQLiteParameter("DEVICE_IMAGE_PATH", entity.Device_image_path));
                        cmd.Parameters.Add(new SQLiteParameter("ESTIMATED_TIME", entity.Estimated_time));
                        cmd.Parameters.Add(new SQLiteParameter("METHOD", entity.Method));
                        cmd.Parameters.Add(new SQLiteParameter("HELP_TEXT", entity.Help_text));
                        cmd.Parameters.Add(new SQLiteParameter("JSON", entity.Json));
                        cmd.Parameters.Add(new SQLiteParameter("DISPLAY_ORDER", entity.Display_order));
                        cmd.Parameters.Add(new SQLiteParameter("STEP", entity.Step));
                        cmd.Parameters.Add(new SQLiteParameter("PERIOD", entity.Period));
                        cmd.Parameters.Add(new SQLiteParameter("OPTION_GROUP_ID", entity.Option_group_id));
                        cmd.Parameters.Add(new SQLiteParameter("V_REQUIRED", entity.V_required));
                        cmd.Parameters.Add(new SQLiteParameter("V_EMAIL", entity.V_email));
                        cmd.Parameters.Add(new SQLiteParameter("V_MIN_LENGTH", entity.V_min_length));
                        cmd.Parameters.Add(new SQLiteParameter("V_MAX_LENGTH", entity.V_max_length));
                        cmd.Parameters.Add(new SQLiteParameter("V_REGEX", entity.V_regex));
                        cmd.Parameters.Add(new SQLiteParameter("ACTIVE", entity.Active));
                        cmd.Parameters.Add(new SQLiteParameter("DELETED", entity.Deleted));
                        cmd.Parameters.Add(new SQLiteParameter("INSERT_USER_ID", entity.Insert_user_id));
                        cmd.Parameters.Add(new SQLiteParameter("UPDATE_USER_ID", entity.Update_user_id));
                        cmd.Parameters.Add(new SQLiteParameter("INSERT_DATETIME", entity.Insert_datetime));
                        cmd.Parameters.Add(new SQLiteParameter("UPDATE_DATETIME", entity.Update_datetime));

                        cmd.ExecuteNonQuery();
                        /*
                        SQLiteCommand sql_cmd= new SQLiteCommand("select seq from sqlite_sequence where name='form_answer_round'; ");
                        int newId = Convert.ToInt32(sql_cmd.ExecuteScalar());
                        */
                        long newId = connection.LastInsertRowId;

                        transaction.Commit();

                        return newId;

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }

                }
                catch (Exception e)
                {
                    /*
                    Application.Current.Dispatcher.Invoke((Action)(() =>
                    {
                        Hatalar.Add(e.Message);
                    }));
                    */
                }
                finally
                {
                    connection.Close();
                }
            }

            return -1;
        }

        public long CreateFiles(string DatabasePath, Files entity)
        {
            using (var connection = new SQLiteConnection("Data Source=" + DatabasePath))
            {
                try
                {
                    connection.Open();

                    try
                    {
                        SQLiteTransaction transaction = null;
                        transaction = connection.BeginTransaction();

                        SQLiteCommand cmd = new SQLiteCommand("INSERT INTO 'files' ( 'APP_CODE', 'FILE_MANAGER_ID', 'STATUS', 'TITLE', 'ENTITY_NAME', 'ENTITY_PROPERTY', 'ENTITY_ID', 'CATEGORY_ID', 'FILE_TYPE_ID', 'FILE_DATA', 'ORIGINAL_FILENAME', 'FILE_REFERENCE', 'FILE_REFERENCE_DEVICE', 'FILE_GUID', 'FILE_EXTENSION', 'CONTENT_TYPE', 'SIZE', 'DISPLAY_ORDER', 'THUMBNAIL', 'THUMBNAIL_DEVICE', 'THUMBNAIL_DATA', 'EXIF', 'OP_MESSAGE', 'NOTES', 'DESCRIPTION', 'VERSION', 'TOP_PARENT_ID', 'PARENT_ID', 'IS_ACTIVE', 'DELETED', 'INSERT_USER_ID', 'UPDATE_USER_ID', 'INSERT_DATETIME', 'UPDATE_DATETIME') VALUES ( @APP_CODE, @FILE_MANAGER_ID, @STATUS, @TITLE, @ENTITY_NAME, @ENTITY_PROPERTY, @ENTITY_ID, @CATEGORY_ID, @FILE_TYPE_ID, @FILE_DATA, @ORIGINAL_FILENAME, @FILE_REFERENCE, @FILE_REFERENCE_DEVICE, @FILE_GUID, @FILE_EXTENSION, @CONTENT_TYPE, @SIZE, @DISPLAY_ORDER, @THUMBNAIL, @THUMBNAIL_DEVICE, @THUMBNAIL_DATA, @EXIF, @OP_MESSAGE, @NOTES, @DESCRIPTION, @VERSION, @TOP_PARENT_ID, @PARENT_ID, @IS_ACTIVE, @DELETED, @INSERT_USER_ID, @UPDATE_USER_ID, @INSERT_DATETIME, @UPDATE_DATETIME)", connection);
                        
                        cmd.Parameters.Add(new SQLiteParameter("APP_CODE", entity.App_code));
                        cmd.Parameters.Add(new SQLiteParameter("FILE_MANAGER_ID", entity.File_manager_id));
                        cmd.Parameters.Add(new SQLiteParameter("STATUS", entity.Status));
                        cmd.Parameters.Add(new SQLiteParameter("TITLE", entity.Title));
                        cmd.Parameters.Add(new SQLiteParameter("ENTITY_NAME", entity.Entity_name));
                        cmd.Parameters.Add(new SQLiteParameter("ENTITY_PROPERTY", entity.Entity_property));
                        cmd.Parameters.Add(new SQLiteParameter("ENTITY_ID", entity.Entity_id));
                        cmd.Parameters.Add(new SQLiteParameter("CATEGORY_ID", entity.Category_id));
                        cmd.Parameters.Add(new SQLiteParameter("FILE_TYPE_ID", entity.File_type_id));
                        cmd.Parameters.Add(new SQLiteParameter("FILE_DATA", entity.File_data));
                        cmd.Parameters.Add(new SQLiteParameter("ORIGINAL_FILENAME", entity.Original_filename));
                        cmd.Parameters.Add(new SQLiteParameter("FILE_REFERENCE", entity.File_reference));
                        cmd.Parameters.Add(new SQLiteParameter("FILE_REFERENCE_DEVICE", entity.File_reference_device));
                        cmd.Parameters.Add(new SQLiteParameter("FILE_GUID", entity.File_guid));
                        cmd.Parameters.Add(new SQLiteParameter("FILE_EXTENSION", entity.File_extension));
                        cmd.Parameters.Add(new SQLiteParameter("CONTENT_TYPE", entity.Content_type));
                        cmd.Parameters.Add(new SQLiteParameter("SIZE", entity.Size));
                        cmd.Parameters.Add(new SQLiteParameter("DISPLAY_ORDER", entity.Display_order));
                        cmd.Parameters.Add(new SQLiteParameter("THUMBNAIL", entity.Thumbnail));
                        cmd.Parameters.Add(new SQLiteParameter("THUMBNAIL_DEVICE", entity.Thumbnail_device));
                        cmd.Parameters.Add(new SQLiteParameter("THUMBNAIL_DATA", entity.Thumbnail_data));
                        cmd.Parameters.Add(new SQLiteParameter("EXIF", entity.Exif));
                        cmd.Parameters.Add(new SQLiteParameter("OP_MESSAGE", entity.Op_message));
                        cmd.Parameters.Add(new SQLiteParameter("NOTES", entity.Notes));
                        cmd.Parameters.Add(new SQLiteParameter("DESCRIPTION", entity.Description));
                        cmd.Parameters.Add(new SQLiteParameter("VERSION", entity.Version));
                        cmd.Parameters.Add(new SQLiteParameter("TOP_PARENT_ID", entity.Top_parent_id));
                        cmd.Parameters.Add(new SQLiteParameter("PARENT_ID", entity.Parent_id));
                        cmd.Parameters.Add(new SQLiteParameter("IS_ACTIVE", entity.Is_active));
                        cmd.Parameters.Add(new SQLiteParameter("DELETED", entity.Deleted));
                        cmd.Parameters.Add(new SQLiteParameter("INSERT_USER_ID", entity.Insert_user_id));
                        cmd.Parameters.Add(new SQLiteParameter("UPDATE_USER_ID", entity.Update_user_id));
                        cmd.Parameters.Add(new SQLiteParameter("INSERT_DATETIME", entity.Insert_datetime));
                        cmd.Parameters.Add(new SQLiteParameter("UPDATE_DATETIME", entity.Update_datetime));

                        cmd.ExecuteNonQuery();
                        /*
                        SQLiteCommand sql_cmd= new SQLiteCommand("select seq from sqlite_sequence where name='form_answer_round'; ");
                        int newId = Convert.ToInt32(sql_cmd.ExecuteScalar());
                        */
                        long newId = connection.LastInsertRowId;

                        transaction.Commit();

                        return newId;

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }


                }
                catch (Exception e)
                {
                    /*
                    Application.Current.Dispatcher.Invoke((Action)(() =>
                    {
                        Hatalar.Add(e.Message);
                    }));
                    */
                }
                finally
                {
                    connection.Close();
                }
            }

            return -1;
        }



        public FormAnswerRound GetFormAnswerRound(string DatabasePath, long id)
        {
            FormAnswerRound result = new FormAnswerRound();
            using (var connection = new SQLiteConnection("Data Source=" + DatabasePath))
            {
                try
                {
                    connection.Open();
                    SQLiteCommand cmd = new SQLiteCommand("SELECT * FROM FORM_ANSWER_ROUND WHERE _id = " + id, connection);
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {

                            result.Id = reader.GetInt32(reader.GetOrdinal("_id"));
                            result.Object_id = reader.GetString(reader.GetOrdinal("OBJECT_ID"));
                            result.Device_id = (int?)reader.SafeGet<long?>(reader.GetOrdinal("Device_id"));
                            result.Form_id = (int?)reader.SafeGet<long?>(reader.GetOrdinal("Form_id"));
                            result.Location_id = (int?)reader.SafeGet<long?>(reader.GetOrdinal("Location_id"));
                            result.Equipment_id = (int?)reader.SafeGet<long?>(reader.GetOrdinal("EQUIPMENT_ID")); 
                            result.Role_id = (int?)reader.SafeGet<long?>(reader.GetOrdinal("Role_id"));
                            result.Username = reader.GetString(reader.GetOrdinal("Username"));
                            result.Read_no = reader.SafeGet<long?>(reader.GetOrdinal("Read_no"));
                            result.Start_read_datetime = FromFileTimeUtc(reader.SafeGet<long>(reader.GetOrdinal("Start_read_datetime"))); 
                            result.Start_count = (int?)reader.SafeGet<long?>(reader.GetOrdinal("Start_count"));
                            result.Description = reader.SafeGet<string>(reader.GetOrdinal("Description")); 
                            result.Insert_user_id = reader.SafeGet<string>(reader.GetOrdinal("Insert_user_id")); 
                            result.Insert_datetime = reader.GetString(reader.GetOrdinal("Insert_datetime"));

                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return result;
        }

        public FormAnswerRound CreateAnswerRound(string DatabasePath, int roleId, int deviceId, long readNo, string Username)
        {
            using (var connection = new SQLiteConnection("Data Source=" + DatabasePath))
            {
                try
                {
                    connection.Open();

                    try
                    {
                        SQLiteTransaction transaction = null;
                        transaction = connection.BeginTransaction();

                        SQLiteCommand cmd = new SQLiteCommand("INSERT INTO form_answer_round ('Object_id','Role_id','Start_read_datetime','Read_no', 'Device_id', 'Start_count', 'Username', 'Insert_datetime') VALUES (@Object_id, @Role_id, @Start_read_datetime, @Read_no, @Device_id, @Start_count, @Username, @Insert_datetime)", connection);
                        cmd.Parameters.Add(new SQLiteParameter("Object_id", Guid.NewGuid().ToString()));
                        //cmd.Parameters.Add(new SQLiteParameter("Form_id", formId));
                        cmd.Parameters.Add(new SQLiteParameter("Role_id", roleId));
                        cmd.Parameters.Add(new SQLiteParameter("Start_read_datetime", ToFileTimeUtc(StringHelper.ConvertLongToDateTime(readNo))));
                        cmd.Parameters.Add(new SQLiteParameter("Read_no", readNo));
                        cmd.Parameters.Add(new SQLiteParameter("Device_id", deviceId));
                        cmd.Parameters.Add(new SQLiteParameter("Start_count", 1));
                        cmd.Parameters.Add(new SQLiteParameter("Username", Username));
                        cmd.Parameters.Add(new SQLiteParameter("Insert_datetime", StringHelper.ConvertDateTimeToString(DateTime.Now)));
                        cmd.ExecuteNonQuery();
                        /*
                        SQLiteCommand sql_cmd= new SQLiteCommand("select seq from sqlite_sequence where name='form_answer_round'; ");
                        int newId = Convert.ToInt32(sql_cmd.ExecuteScalar());
                        */
                        long newId = connection.LastInsertRowId;

                        transaction.Commit();

                        return GetFormAnswerRound(DatabasePath, newId);

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }

                    /*
                    SQLiteCommand spatialiteCommand = new SQLiteCommand("SELECT load_extension(\"mod_spatialite\")", connection);
                    spatialiteCommand.ExecuteNonQuery();
                    
                    Parallel.ForEach(yapilar, (yapi) =>
                    {
                        try
                        {
                            SQLiteCommand cmd = new SQLiteCommand("INSERT INTO form_answer_round ('Object_id','Form_id','Start_read_datetime','Read_no', 'Device_id', 'Start_count') VALUES (@Object_id, @Form_id, @Start_read_datetime, @Read_no, @Device_id, @Start_count)", connection);
                            cmd.Parameters.Add(new SQLiteParameter("Object_id", Guid.NewGuid().ToString()));
                            cmd.Parameters.Add(new SQLiteParameter("Form_id", formId));
                            cmd.Parameters.Add(new SQLiteParameter("Start_read_datetime", StringHelper.ConvertLongToDateTime(readNo)));
                            cmd.Parameters.Add(new SQLiteParameter("Read_no", readNo));
                            cmd.Parameters.Add(new SQLiteParameter("Device_id", deviceId));
                            cmd.Parameters.Add(new SQLiteParameter("Start_count", 1));
                            cmd.ExecuteNonQuery();
                        }
                        catch (Exception e)
                        {
                            Application.Current.Dispatcher.Invoke((Action)(() =>
                            {
                                Hatalar.Add(e.Message);
                            }));
                        }
                    });
                    SQLiteCommand countQuery = new SQLiteCommand("select count(1) from YAPI", connection);
                    /*
                    Application.Current.Dispatcher.Invoke((Action)(() =>
                    {
                        Progress = string.Format("{0}/{1} adet Yapı", countQuery.ExecuteScalar(), count);
                    }));
                    */
                }
                catch (Exception e)
                {
                    /*
                    Application.Current.Dispatcher.Invoke((Action)(() =>
                    {
                        Hatalar.Add(e.Message);
                    }));
                    */
                }
                finally
                {
                    connection.Close();
                }
            }

            return null;
        }


        public FormAnswer GetFormAnswer(string DatabasePath, long id)
        {
            FormAnswer result = new FormAnswer();
            using (var connection = new SQLiteConnection("Data Source=" + DatabasePath))
            {
                try
                {
                    connection.Open();
                    SQLiteCommand cmd = new SQLiteCommand("SELECT * FROM FORM_ANSWER WHERE _id = " + id, connection);
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            result.Id = reader.GetInt32(reader.GetOrdinal("_id"));
                            result.Object_id = reader.GetString(reader.GetOrdinal("OBJECT_ID"));
                            result.Form_answer_round_id = (int)reader.SafeGet<long>(reader.GetOrdinal("Form_answer_round_id")); 
                            result.Form_question_id = (int)reader.SafeGet<long>(reader.GetOrdinal("Form_question_id")); 
                            result.Read_no = reader.SafeGet<long?>(reader.GetOrdinal("Read_no")); 
                            result.Device_id = (int?)reader.SafeGet<long?>(reader.GetOrdinal("Device_id")); 
                            result.Answer = reader.GetString(reader.GetOrdinal("Answer"));
                            result.Actual_read_datetime = FromFileTimeUtc(reader.SafeGet<long>(reader.GetOrdinal("Actual_read_datetime"))); 
                            //result.Actual_read_datetime = reader.SafeGet<DateTime>(reader.GetOrdinal("Actual_read_datetime"));
                            result.Username = reader.GetString(reader.GetOrdinal("Username"));
                            result.Description = reader.SafeGet<string>(reader.GetOrdinal("Description")); 
                            result.Insert_datetime = reader.GetString(reader.GetOrdinal("Insert_datetime"));

                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return result;
        }

        public FormAnswer CreateAnswer(string DatabasePath, long Form_answer_round_id, int deviceId, long readNo, int formQuestionId, string answer, string Username)
        {
            using (var connection = new SQLiteConnection("Data Source=" + DatabasePath))
            {
                try
                {
                    connection.Open();

                    try
                    {
                        SQLiteCommand cmd = new SQLiteCommand("INSERT INTO form_answer ('Object_id', 'Form_answer_round_id', 'Form_question_id', 'Read_no', 'Device_id', 'Answer', 'Insert_datetime', 'Actual_read_datetime', 'Username') VALUES (@Object_id, @Form_answer_round_id, @Form_question_id, @Read_no, @Device_id, @Answer, @Insert_datetime, @Actual_read_datetime, @Username )", connection);
                        cmd.Parameters.Add(new SQLiteParameter("Object_id", Guid.NewGuid().ToString()));
                        cmd.Parameters.Add(new SQLiteParameter("Form_answer_round_id", Form_answer_round_id));
                        cmd.Parameters.Add(new SQLiteParameter("Form_question_id", formQuestionId));
                        cmd.Parameters.Add(new SQLiteParameter("Read_no", readNo));
                        cmd.Parameters.Add(new SQLiteParameter("Device_id", deviceId));
                        cmd.Parameters.Add(new SQLiteParameter("Answer", answer));
                        cmd.Parameters.Add(new SQLiteParameter("Actual_read_datetime", ToFileTimeUtc(DateTime.Now)));
                        cmd.Parameters.Add(new SQLiteParameter("Username", Username));
                        cmd.Parameters.Add(new SQLiteParameter("Insert_datetime", StringHelper.ConvertDateTimeToString(DateTime.Now)));
                        cmd.ExecuteNonQuery();

                        long newId = connection.LastInsertRowId;
                        /*
                        SQLiteCommand sql_cmd = new SQLiteCommand("select seq from sqlite_sequence where name='form_answer'; ");
                        Convert.ToInt32(sql_cmd.ExecuteScalar());
                        */
                        return GetFormAnswer(DatabasePath, newId);

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }

                    /*
                    SQLiteCommand spatialiteCommand = new SQLiteCommand("SELECT load_extension(\"mod_spatialite\")", connection);
                    spatialiteCommand.ExecuteNonQuery();
                    
                    Parallel.ForEach(yapilar, (yapi) =>
                    {
                        try
                        {
                            SQLiteCommand cmd = new SQLiteCommand("INSERT INTO form_answer_round ('Object_id','Form_id','Start_read_datetime','Read_no', 'Device_id', 'Start_count') VALUES (@Object_id, @Form_id, @Start_read_datetime, @Read_no, @Device_id, @Start_count)", connection);
                            cmd.Parameters.Add(new SQLiteParameter("Object_id", Guid.NewGuid().ToString()));
                            cmd.Parameters.Add(new SQLiteParameter("Form_id", formId));
                            cmd.Parameters.Add(new SQLiteParameter("Start_read_datetime", StringHelper.ConvertLongToDateTime(readNo)));
                            cmd.Parameters.Add(new SQLiteParameter("Read_no", readNo));
                            cmd.Parameters.Add(new SQLiteParameter("Device_id", deviceId));
                            cmd.Parameters.Add(new SQLiteParameter("Start_count", 1));
                            cmd.ExecuteNonQuery();
                        }
                        catch (Exception e)
                        {
                            Application.Current.Dispatcher.Invoke((Action)(() =>
                            {
                                Hatalar.Add(e.Message);
                            }));
                        }
                    });
                    SQLiteCommand countQuery = new SQLiteCommand("select count(1) from YAPI", connection);
                    /*
                    Application.Current.Dispatcher.Invoke((Action)(() =>
                    {
                        Progress = string.Format("{0}/{1} adet Yapı", countQuery.ExecuteScalar(), count);
                    }));
                    */
                }
                catch (Exception e)
                {
                    /*
                    Application.Current.Dispatcher.Invoke((Action)(() =>
                    {
                        Hatalar.Add(e.Message);
                    }));
                    */
                }
                finally
                {
                    connection.Close();
                }
            }

            return null;
        }



        public long CreateSynchronization(string DatabasePath)
        {
            using (var connection = new SQLiteConnection("Data Source=" + DatabasePath))
            {
                try
                {
                    connection.Open();

                    try
                    {
                        SQLiteTransaction transaction = null;
                        transaction = connection.BeginTransaction();
                        // INSERT INTO 'synchronization' ('Id', 'TENANT_ID', 'OBJECT_ID', 'DEVICE_ID', 'UDID', 'USERNAME', 'SYNC_TYPE', 'STATUS', 'START_DATETIME', 'FINISH_DATETIME', 'SERVER_ID', 'IP_ADDRESS', 'CLIENT_VERSION', 'SERVER_VERSION', 'DESCRIPTION', 'INSERT_USER_ID', 'UPDATE_USER_ID', 'INSERT_DATETIME', 'UPDATE_DATETIME') VALUES (@Id, @TENANT_ID, @OBJECT_ID, @DEVICE_ID, @UDID, @USERNAME, @SYNC_TYPE, @STATUS, @START_DATETIME, @FINISH_DATETIME, @SERVER_ID, @IP_ADDRESS, @CLIENT_VERSION, @SERVER_VERSION, @DESCRIPTION, @INSERT_USER_ID, @UPDATE_USER_ID, @INSERT_DATETIME, @UPDATE_DATETIME);
                        SQLiteCommand cmd = new SQLiteCommand("INSERT INTO 'SYNCHRONIZATION' ('SYNC_TYPE', 'STATUS', 'Insert_datetime') VALUES (@SYNC_TYPE, @STATUS, @Insert_datetime);", connection);
                        cmd.Parameters.Add(new SQLiteParameter("SYNC_TYPE", "0"));
                        cmd.Parameters.Add(new SQLiteParameter("STATUS", "0"));
                        cmd.Parameters.Add(new SQLiteParameter("Insert_datetime", StringHelper.ConvertDateTimeToString(DateTime.Now)));
                        cmd.ExecuteNonQuery();
                        /*
                        SQLiteCommand sql_cmd= new SQLiteCommand("select seq from sqlite_sequence where name='form_answer_round'; ");
                        int newId = Convert.ToInt32(sql_cmd.ExecuteScalar());
                        */
                        long newId = connection.LastInsertRowId;

                        transaction.Commit();

                        return newId;

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }

                    /*
                    SQLiteCommand spatialiteCommand = new SQLiteCommand("SELECT load_extension(\"mod_spatialite\")", connection);
                    spatialiteCommand.ExecuteNonQuery();
                    
                    Parallel.ForEach(yapilar, (yapi) =>
                    {
                        try
                        {
                            SQLiteCommand cmd = new SQLiteCommand("INSERT INTO form_answer_round ('Object_id','Form_id','Start_read_datetime','Read_no', 'Device_id', 'Start_count') VALUES (@Object_id, @Form_id, @Start_read_datetime, @Read_no, @Device_id, @Start_count)", connection);
                            cmd.Parameters.Add(new SQLiteParameter("Object_id", Guid.NewGuid().ToString()));
                            cmd.Parameters.Add(new SQLiteParameter("Form_id", formId));
                            cmd.Parameters.Add(new SQLiteParameter("Start_read_datetime", StringHelper.ConvertLongToDateTime(readNo)));
                            cmd.Parameters.Add(new SQLiteParameter("Read_no", readNo));
                            cmd.Parameters.Add(new SQLiteParameter("Device_id", deviceId));
                            cmd.Parameters.Add(new SQLiteParameter("Start_count", 1));
                            cmd.ExecuteNonQuery();
                        }
                        catch (Exception e)
                        {
                            Application.Current.Dispatcher.Invoke((Action)(() =>
                            {
                                Hatalar.Add(e.Message);
                            }));
                        }
                    });
                    SQLiteCommand countQuery = new SQLiteCommand("select count(1) from YAPI", connection);
                    /*
                    Application.Current.Dispatcher.Invoke((Action)(() =>
                    {
                        Progress = string.Format("{0}/{1} adet Yapı", countQuery.ExecuteScalar(), count);
                    }));
                    */
                }
                catch (Exception e)
                {
                    /*
                    Application.Current.Dispatcher.Invoke((Action)(() =>
                    {
                        Hatalar.Add(e.Message);
                    }));
                    */
                }
                finally
                {
                    connection.Close();
                }
            }

            return -1;
        }

        public long CreateSynchronizationDetail(string DatabasePath, long syncId, List<object> items)
        {
            using (var connection = new SQLiteConnection("Data Source=" + DatabasePath))
            {
                JsonFormWizard jsonWizard = new JsonFormWizard();
                try
                {
                    connection.Open();

                    foreach (object obj in items)
                    {
                        Ares.DTO.Entity<int> entity = (Ares.DTO.Entity<int>)obj;
                        try
                        {
                            SQLiteTransaction transaction = null;
                            transaction = connection.BeginTransaction();

                            string json = jsonWizard.convertToJSON(entity);
                            json = json.Replace("ı", "i");

                            // INSERT INTO 'synchronization_detail' ('Id', 'TENANT_ID', 'OBJECT_ID', 'SYNCHRONIZATION_ID', 'STATUS', 'TABLE_NAME', 'SERVER_ID', 'CLIENT_ID', 'TRANSACTION_ID', 'OPERATION_TYPE', 'RESULT', 'MESSAGE', 'ITEM', 'SIZE', 'ORDER', 'START_DATETIME', 'END_DATETIME', 'LAST_EXEC_DATETIME', 'RETRY_COUNT', 'PAGE_INDEX', 'PAGE_SIZE', 'PAGE_COUNT', 'TOTAL_ITEM_COUNT', 'SIZE_LIMIT', 'DESCRIPTION', 'SYNC_DETAIL_SERVER_ID', 'NOTES', 'ACTIVE', 'INSERT_USER_ID', 'UPDATE_USER_ID', 'INSERT_DATETIME', 'UPDATE_DATETIME') VALUES (@Id, @TENANT_ID, @OBJECT_ID, @SYNCHRONIZATION_ID, @STATUS, @TABLE_NAME, @SERVER_ID, @CLIENT_ID, @TRANSACTION_ID, @OPERATION_TYPE, @RESULT, @MESSAGE, @ITEM, @SIZE, @ORDER, @START_DATETIME, @END_DATETIME, @LAST_EXEC_DATETIME, @RETRY_COUNT, @PAGE_INDEX, @PAGE_SIZE, @PAGE_COUNT, @TOTAL_ITEM_COUNT, @SIZE_LIMIT, @DESCRIPTION, @SYNC_DETAIL_SERVER_ID, @NOTES, @ACTIVE, @INSERT_USER_ID, @UPDATE_USER_ID, @INSERT_DATETIME, @UPDATE_DATETIME);
                            SQLiteCommand cmd = new SQLiteCommand("INSERT INTO 'SYNCHRONIZATION_DETAIL' ('SYNCHRONIZATION_ID', 'STATUS', 'TABLE_NAME', 'CLIENT_ID', 'OPERATION_TYPE', 'ITEM', 'ACTIVE', 'Insert_datetime') VALUES (@SYNCHRONIZATION_ID, @STATUS, @TABLE_NAME, @CLIENT_ID, @OPERATION_TYPE, @ITEM, @ACTIVE, @Insert_datetime);", connection);
                            cmd.Parameters.Add(new SQLiteParameter("SYNCHRONIZATION_ID", syncId));
                            cmd.Parameters.Add(new SQLiteParameter("STATUS", "0"));
                            cmd.Parameters.Add(new SQLiteParameter("TABLE_NAME", obj is FormAnswerRound ? "formanswerround" : "formanswer"));
                            cmd.Parameters.Add(new SQLiteParameter("CLIENT_ID", entity.Id));
                            cmd.Parameters.Add(new SQLiteParameter("OPERATION_TYPE", "0")); // insert
                            cmd.Parameters.Add(new SQLiteParameter("ITEM", json));
                            cmd.Parameters.Add(new SQLiteParameter("ACTIVE", 1));
                            cmd.Parameters.Add(new SQLiteParameter("Insert_datetime", StringHelper.ConvertDateTimeToString(DateTime.Now)));
                            cmd.ExecuteNonQuery();
                            /*
                            SQLiteCommand sql_cmd= new SQLiteCommand("select seq from sqlite_sequence where name='form_answer_round'; ");
                            int newId = Convert.ToInt32(sql_cmd.ExecuteScalar());
                            */
                            long newId = connection.LastInsertRowId;

                            transaction.Commit();

                            //return newId;

                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                    }
                    /*
                    SQLiteCommand spatialiteCommand = new SQLiteCommand("SELECT load_extension(\"mod_spatialite\")", connection);
                    spatialiteCommand.ExecuteNonQuery();
                    
                    Parallel.ForEach(yapilar, (yapi) =>
                    {
                        try
                        {
                            SQLiteCommand cmd = new SQLiteCommand("INSERT INTO form_answer_round ('Object_id','Form_id','Start_read_datetime','Read_no', 'Device_id', 'Start_count') VALUES (@Object_id, @Form_id, @Start_read_datetime, @Read_no, @Device_id, @Start_count)", connection);
                            cmd.Parameters.Add(new SQLiteParameter("Object_id", Guid.NewGuid().ToString()));
                            cmd.Parameters.Add(new SQLiteParameter("Form_id", formId));
                            cmd.Parameters.Add(new SQLiteParameter("Start_read_datetime", StringHelper.ConvertLongToDateTime(readNo)));
                            cmd.Parameters.Add(new SQLiteParameter("Read_no", readNo));
                            cmd.Parameters.Add(new SQLiteParameter("Device_id", deviceId));
                            cmd.Parameters.Add(new SQLiteParameter("Start_count", 1));
                            cmd.ExecuteNonQuery();
                        }
                        catch (Exception e)
                        {
                            Application.Current.Dispatcher.Invoke((Action)(() =>
                            {
                                Hatalar.Add(e.Message);
                            }));
                        }
                    });
                    SQLiteCommand countQuery = new SQLiteCommand("select count(1) from YAPI", connection);
                    /*
                    Application.Current.Dispatcher.Invoke((Action)(() =>
                    {
                        Progress = string.Format("{0}/{1} adet Yapı", countQuery.ExecuteScalar(), count);
                    }));
                    */
                }
                catch (Exception e)
                {
                    /*
                    Application.Current.Dispatcher.Invoke((Action)(() =>
                    {
                        Hatalar.Add(e.Message);
                    }));
                    */
                }
                finally
                {
                    connection.Close();
                }
            }

            return -1;
        }

        public static long ToFileTimeUtc(DateTime dateTime)
        {
            return dateTime.ToFileTimeUtc();
        }

        public static DateTime FromFileTimeUtc(long fileTimeUtc)
        {
            return DateTime.FromFileTimeUtc(fileTimeUtc);
        }


    }

    public static class SQLiteExtensions
    {
        public static T SafeGet<T>(this SQLiteDataReader reader, int col)
        {
            return reader.IsDBNull(col) ? default(T) : reader.GetFieldValue<T>(col);
        }
    }
}
