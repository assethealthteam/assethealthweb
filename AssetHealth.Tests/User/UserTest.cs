﻿using Ares.Core.Framework;
using Ares.UMS.Svc.Users;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetHealth.Tests.User
{
    [TestClass]
    public class UserTest : IoCSupportedTest
    {
        private IUserSvc _iUserSvc;
        [TestInitialize]
        public void InitializeTest()
        {
            this._iUserSvc = FrameworkCtx.Current.Resolve<IUserSvc>();

        }

        [TestMethod]
        public void EncryptUserPasswordsInDb()
        {
            foreach(Ares.UMS.DTO.Users.User user in _iUserSvc.GetAll().ToList())
            {
                if (user.PasswordFormatId == 0 && !string.IsNullOrEmpty(user.Password))
                {
                    _iUserSvc.PasswordChange(user.Username, user.Password);
                }
            }

        }



    }
}
