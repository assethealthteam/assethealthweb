﻿using Ares.Core.Configuration;
using Ares.Core.Framework;
using Ares.Core.Framework.DI;
using Ares.UMS.Svc.Configuration;
using Autofac;
using Autofac.Builder;
using Autofac.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AssetHealth.Tests.DI
{
    public class DependencyRegistrar : IDependencyRegistrar
    {


        public virtual void Register(ContainerBuilder builder, ContainerManager containerManager, ITypeFinder typeFinder, AresConfig config)
        {
            //            builder.RegisterType<DeviceSvc>().As<IDeviceSvc>().InstancePerLifetimeScope();


            //builder.Register<LoggingPipelineModule>(b => new LoggingPipelineModule()).PropertiesAutowired().InstancePerLifetimeScope();

            //builder.RegisterSource(new SettingsSource());

        }


        public class SettingsSource : IRegistrationSource
        {
            static readonly MethodInfo BuildMethod = typeof(SettingsSource).GetMethod(
                "BuildRegistration",
                BindingFlags.Static | BindingFlags.NonPublic);

            public IEnumerable<IComponentRegistration> RegistrationsFor(
                    Service service,
                    Func<Service, IEnumerable<IComponentRegistration>> registrations)
            {
                var ts = service as TypedService;
                if (ts != null && typeof(ISettings).IsAssignableFrom(ts.ServiceType))
                {
                    var buildMethod = BuildMethod.MakeGenericMethod(ts.ServiceType);
                    yield return (IComponentRegistration)buildMethod.Invoke(null, null);
                }
            }

            [global::System.Reflection.Obfuscation(Exclude = true, Feature = "renaming")]
            static IComponentRegistration BuildRegistration<TSettings>() where TSettings : ISettings, new()
            {
                return RegistrationBuilder
                    .ForDelegate((c, p) =>
                    {
                        return c.Resolve<ISettingSvc>().LoadSetting<TSettings>();
                    })
                    .InstancePerLifetimeScope()
                    .CreateRegistration();
            }

            public bool IsAdapterForIndividualComponents { get { return false; } }
        }

        public int Order
        {
            get { return 100; }
        }

    }
}
