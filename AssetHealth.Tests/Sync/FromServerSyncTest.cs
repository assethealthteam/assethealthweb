﻿using Ares.Core.Framework;
using Ares.Core.Helpers;
using Ares.Sync.ChangeManagement;
using Ares.Sync.Controllers;
using Ares.Sync.DTO;
using Ares.Sync.DTO.Enums;
using Ares.Sync.Entities.Client;
using Ares.Sync.Enums;
using Ares.Sync.Models.Synchronization_detail;
using Ares.Sync.Services.Interfaces;
using Ares.Sync.Svc;
using Ares.UMS.Svc;
using Ares.UMS.Svc.Users;
using Ares.Web.JSON;
using Ares.Web.Mvc.Entities;
using AssetHealth.Common.DTO;
using AssetHealth.Common.Svc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace AssetHealth.Tests.Sync
{
    [TestClass]
    public class FromServerSyncTest : IoCSupportedTest
    {
        IChangeSetService _iChangeSetService;
        ISynchronizationSvc _iSynchronizationSvc;
        IFormSvc _iFormSvc;
        IFormQuestionSvc _iFormQuestionSvc;
        IDeviceSvc _iDeviceSvc;
        IWorkContext _iWorkContext;
        IUserSvc _iUserSvc;

        [TestInitialize]
        public void InitializeTest()
        {
            //this._ChangeSetService = FrameworkCtx.Current.Resolve<IChangeSetService>();

            _iSynchronizationSvc = FrameworkCtx.Current.Resolve<ISynchronizationSvc>();
            _iChangeSetService = FrameworkCtx.Current.Resolve<IChangeSetService>();
            _iFormSvc = FrameworkCtx.Current.Resolve<IFormSvc>();
            _iFormQuestionSvc = FrameworkCtx.Current.Resolve<IFormQuestionSvc>();
            _iDeviceSvc = FrameworkCtx.Current.Resolve<IDeviceSvc>();
            _iUserSvc = FrameworkCtx.Current.Resolve<IUserSvc>();
            _iWorkContext = FrameworkCtx.Current.Resolve<IWorkContext>();

            _iWorkContext.CurrentUser = _iUserSvc.GetById(1);

        }


        [TestMethod]
        public void InsertNewSynchronizaion()
        {
            Form entity = _iFormSvc.GetById(11);
            Device device = _iDeviceSvc.GetById(6);

            Synchronization sync = new Synchronization();
            sync.Object_id = Guid.NewGuid().ToString();
            sync.Sync_type = EnumSynchronizationSync_type.From_Server;
            sync.Status = EnumSynchronizationStatus.Started;
            sync.Start_datetime = DateTime.Now;
            sync.Insert_datetime = StringHelper.ConvertDateTimeToString(DateTime.Now);
            //sync.Insert_user_id = _iWorkContext.CurrentUser.Id.ToString();

            sync.SYNCHRONIZATIONSynchronization_detail = new List<SynchronizationDetail>();
            sync.SYNCHRONIZATIONSynchronization_device = new List<SynchronizationDevice>();
            sync.SYNCHRONIZATIONSynchronization_device.Add(new SynchronizationDevice() { Device_id = device.Id, Status = EnumSynchronizationDeviceStatus.None });


            SynchronizationDetail detailToBeAdded = new SynchronizationDetail()
            {
                Object_id = Guid.NewGuid().ToString(),
                Status = Ares.Sync.Common.DTO.Enums.EnumSynchronizationDetailStatus.Started,
                Table_name = "form",
                Server_id = entity.Id,
                Active = true,
                Operation_type = EnumSynchronizationDetailOperation_type.Created,
                Item = JsonConvert.SerializeObject(entity, new AresJsonSerializerSettings()),
                Insert_datetime = StringHelper.ConvertDateTimeToString(DateTime.Now),
                Insert_user_id = _iWorkContext.CurrentUser.Id.ToString(),

            };

            sync.SYNCHRONIZATIONSynchronization_detail.Add(detailToBeAdded);
        }



        [TestMethod]
        public void UpdateFormQuestion()
        {
            FormQuestion question = _iFormQuestionSvc.GetById(1001);

            question.Display_order = 3000;
            _iFormQuestionSvc.Update(question);
        }



    }
}