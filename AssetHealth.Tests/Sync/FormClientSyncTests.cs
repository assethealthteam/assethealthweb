﻿using Ares.Core.Framework;
using Ares.Sync.ChangeManagement;
using Ares.Sync.DTO;
using Ares.Sync.DTO.Enums;
using Ares.Sync.Entities.Client;
using Ares.Sync.Enums;
using Ares.Sync.Services.Interfaces;
using Ares.Sync.Svc;
using Ares.Web.JSON;
using Ares.Web.Mvc.Entities;
using AssetHealth.Common.DTO;
using AssetHealth.Common.DTO.Enums;
using AssetHealth.Common.Svc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Script.Serialization;

namespace AssetHealth.Tests.Sync
{
    [TestClass]
    public class FormClientSyncTests : IoCSupportedTest
    {
        IChangeSetService _ChangeSetService;

        private readonly JavaScriptSerializer _js = new JavaScriptSerializer {  MaxJsonLength = 41943040 };
        

        [TestInitialize]
        public void InitializeTest()
        {
            //this._ChangeSetService = FrameworkCtx.Current.Resolve<IChangeSetService>();

        }

        [TestMethod]
        public void ReTryFailedSynchronizationDetails()
        {
            ISynchronizationSvc iSynchronizationSvc = FrameworkCtx.Current.Resolve<ISynchronizationSvc>();
            IChangeSetService iChangeSetService = FrameworkCtx.Current.Resolve<IChangeSetService>();

            List<Synchronization> syncs = iSynchronizationSvc.GetAll().Where(s => s.Status == EnumSynchronizationStatus.Failed).ToList();

            foreach (Synchronization sync in syncs)
            {
                ChangeSet set = new ChangeSet();
                List<SynchronizationDetail> failedDetails = sync.SYNCHRONIZATIONSynchronization_detail.Where(sd => sd.Status == Ares.Sync.Common.DTO.Enums.EnumSynchronizationDetailStatus.Failed).ToList();
                set.Items = failedDetails;
                iChangeSetService.SyncSendClientChanges(sync.SYNCHRONIZATION_DEVICE.Udid, sync.Id, set);
                iChangeSetService.SyncFinish(sync.Id);
            }
        }

        [TestMethod]
        public void PrepareServerChangeChangeSet()
        {
            ISynchronizationSvc iSynchronizationSvc = FrameworkCtx.Current.Resolve<ISynchronizationSvc>();
            IFormSvc formSvc = FrameworkCtx.Current.Resolve<IFormSvc>();
            IDeviceSvc deviceSvc = FrameworkCtx.Current.Resolve<IDeviceSvc>();

            Device device = deviceSvc.GetAll().FirstOrDefault();
            Form form = formSvc.GetById(4);

            Synchronization sync = new Synchronization();
            sync.Object_id = Guid.NewGuid().ToString();
            sync.Sync_type = EnumSynchronizationSync_type.From_Server;
            sync.Status = EnumSynchronizationStatus.Started;
            sync.Start_datetime = DateTime.Now;

            List<SynchronizationDetail> details = new List<SynchronizationDetail>();
            details.Add(new SynchronizationDetail() { Table_name = "form", Server_id = form.Id, Status = Ares.Sync.Common.DTO.Enums.EnumSynchronizationDetailStatus.Started, Active = true, Item = JsonConvert.SerializeObject(form, new AresJsonSerializerSettings()) });

            foreach (FormQuestion question in form.FORMForm_question)
            {
                details.Add(new SynchronizationDetail() { Table_name = "form_question", Server_id = question.Id, Status = Ares.Sync.Common.DTO.Enums.EnumSynchronizationDetailStatus.Started, Active = true, Item = JsonConvert.SerializeObject(question, new AresJsonSerializerSettings()) });
            }
            sync.SYNCHRONIZATIONSynchronization_detail = details;
            sync.SYNCHRONIZATIONSynchronization_device = new List<SynchronizationDevice>();

            sync.SYNCHRONIZATIONSynchronization_device.Add(new SynchronizationDevice() { Device_id  = device .Id, Status =  EnumSynchronizationDeviceStatus.None });

            sync = iSynchronizationSvc.Insert(sync);


        }


    }
}