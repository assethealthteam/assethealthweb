﻿using Ares.Core.Framework;
using Ares.Sync.ChangeManagement;
using Ares.Sync.Controllers;
using Ares.Sync.DTO;
using Ares.Sync.DTO.Enums;
using Ares.Sync.Entities.Client;
using Ares.Sync.Enums;
using Ares.Sync.Models.Synchronization_detail;
using Ares.Sync.Services.Interfaces;
using Ares.Web.Mvc.Entities;
using AssetHealth.Common.DTO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace AssetHealth.Tests.Sync
{
    [TestClass]
    public class ClientSyncTests : IoCSupportedTest
    {
        IChangeSetService _ChangeSetService;

        [TestInitialize]
        public void InitializeTest()
        {
            //this._ChangeSetService = FrameworkCtx.Current.Resolve<IChangeSetService>();



        }


        [TestMethod]
        public void InsertNew()
        {
            _ChangeSetService = FrameworkCtx.Current.Resolve<IChangeSetService>();
            string androidId = "abdc35ba4940238e";
            IEnumerable<SynchronizationDetailModel> clientSyncList = new SynchronizationDetailModel[]{
                new SynchronizationDetailModel(){
                    Client_id=1,
                    Operation_type= EnumSynchronizationDetailOperation_type.Created,
                    Table_name="YAPI"
                }
             };
            ApiTransferObject<int> result = _ChangeSetService.SyncStart(androidId, clientSyncList);
            int senkronId = result.Data;
            ChangeSet set = new ChangeSet();
            /*
            YAPI yapi = new YAPI()
            {
                AD = "Test Binası Yeni Kayıt",
                Geometry = "POLYGON((29.986188937048748 39.41990191498951,29.98645715795051 39.42014641545386,29.986261356691916 39.420227224740664,29.98600922904469 39.41995578804628,29.986188937048748 39.41990191498951))"
            };
            ChangeSetItem item = new ChangeSetItem()
            {
                Alias = "YAPI",
                ClientId = 1,
                Item = JsonConvert.SerializeObject(yapi),
                ItemState = SenkronizasyonOperasyonTipi.Yeni
            };
            set.Items.Add(item);

            var syncResult = _ChangeSetService.SyncClientChangeSet(androidId, senkronId, set);
            */
            _ChangeSetService.SyncFinish(senkronId);
        }

        [TestMethod]
        public void ResimTest()
        {
            _ChangeSetService = FrameworkCtx.Current.Resolve<IChangeSetService>();
            string androidId = "178a3655d8ad16c0";
            IEnumerable<SynchronizationDetailModel> clientSyncList = new SynchronizationDetailModel[]{
                new SynchronizationDetailModel(){
                    Client_id = 1,
                    Operation_type = EnumSynchronizationDetailOperation_type.Created,
                    Table_name = "MEDYA"
                }
             };
            ApiTransferObject<int> result = _ChangeSetService.SyncStart(androidId, clientSyncList);
            int senkronId = result.Data;
            ChangeSet set = new ChangeSet();
            byte[] file = File.ReadAllBytes(@"X:\Test.jpg");

            ClientMedya medya = new ClientMedya()
            {
                ClientReferenceId = 1,
                Data = Convert.ToBase64String(file),
                Exif = null,
                MedyaTip = 1,
                TabloAdi = "YAPI",
                LokasyonGeometry = "POINT (32.775123855926815 39.914055727867627)"
            };
            SynchronizationDetail item = new SynchronizationDetail()
            {
                Table_name = "MEDYA",
                Client_id = 1,
                Item = JsonConvert.SerializeObject(medya),
                Operation_type = EnumSynchronizationDetailOperation_type.Created
            };
            set.Items.Add(item);

            var syncResult = _ChangeSetService.SyncSendClientChanges(androidId, senkronId, set);
            _ChangeSetService.SyncFinish(senkronId);
        }


        [TestMethod]
        public void YapiMedyaKayit()
        {
            /*
            using (var ctx = new NumaratajContext())
            {
                ctx.YAPI_MEDYA.Add(new YAPI_MEDYA()
                {
                    YAPIID = 566,
                    MEDYAID = 13
                });
                ctx.SaveChanges();
            }
            */
        }

        [TestMethod]
        public void Update()
        {
            _ChangeSetService = FrameworkCtx.Current.Resolve<IChangeSetService>();
            string androidId = "12345678";
            IEnumerable<SynchronizationDetailModel> clientSyncList = new SynchronizationDetailModel[]{
                new SynchronizationDetailModel(){
                    Client_id=1,
                    Operation_type= EnumSynchronizationDetailOperation_type.Updated,
                    Table_name="YAPI"
                }
             };
            ApiTransferObject<int> result = _ChangeSetService.SyncStart(androidId, clientSyncList);
            int senkronId = result.Data;
            ChangeSet set = new ChangeSet();
            /*
            YAPI yapi = new YAPI()
            {
                AD = "Test Binası Güncelleme",
                Geometry = "POLYGON ((32.775123855926815 39.914055727867627, 32.77576324500005 39.912911413406235, 32.776642404975746 39.913187276677085, 32.776229466199283 39.913963774733396, 32.776336031044821 39.914045510858813, 32.776162863170825 39.914382671345059, 32.775123855926815 39.914055727867627))"
            };
            ChangeSetItem item = new ChangeSetItem()
            {
                Alias = "YAPI",
                ClientId = 1,
                Item = JsonConvert.SerializeObject(yapi),
                ItemState = SenkronizasyonOperasyonTipi.Degismis
            };
            set.Items.Add(item);
            */
            var syncResult = _ChangeSetService.SyncSendClientChanges(androidId, senkronId, set);
            _ChangeSetService.SyncFinish(senkronId);
        }

        [TestMethod]
        public void SyncServer()
        {
            _ChangeSetService = FrameworkCtx.Current.Resolve<IChangeSetService>();
            string androidId = "12345678";
            IEnumerable<SenkronizasyonDetayClient> clientSyncList = new SenkronizasyonDetayClient[]{
                new SenkronizasyonDetayClient(){
                    ClientId=2,
                    ServerId=531,
                    OperasyonTipi=SenkronizasyonOperasyonTipi.Yeni,
                    TabloAdi="YAPI",
                    IsSuccess=true,
                    //TranId=1
                }
             };
            var changes = _ChangeSetService.SyncGetServerChanges(androidId, 0, 10);
            //if (changes.IsSuccess && changes.Data.Items.Count > 0)
            //    _ChangeSetService.SetChangeSetResult(androidId, changes.Data.SenkronizasyonId, clientSyncList);
        }
        [TestMethod]
        public void RaporTest()
        {
            _ChangeSetService = FrameworkCtx.Current.Resolve<IChangeSetService>();
            string androidId = "abdc35ba4940238e";
            var changes = _ChangeSetService.GetSenkronizasyonRapor(androidId);
        }

        [TestMethod]
        public void ClientTest()
        {
            /*
            YOL yol = new YOL();
            YAPI y = new YAPI();
            NUMARATAJ N = new NUMARATAJ();
            */
            //string startSyncJson = @"{'Data':[{'tabloAdi':'YAPI','isSuccess':false,'operasyonTipi':0,'senkronizasyonDetayId':0,'senkronizasyonId':0,'serverId':0,'clientId':2,'tranId':0},{'tabloAdi':'YOLORTAHAT','isSuccess':false,'operasyonTipi':0,'senkronizasyonDetayId':0,'senkronizasyonId':0,'serverId':0,'clientId':1,'tranId':0},{'tabloAdi':'NUMARATAJ','isSuccess':false,'operasyonTipi':0,'senkronizasyonDetayId':0,'senkronizasyonId':0,'serverId':0,'clientId':1,'tranId':0},],'AndroidId':'4766ce689a0e14e3','PageSize':0,'IsSuccess':false,'PageIndex':0,'SenkronizasyonId':0}";
            //string syncClientChangeSetJson = @"{'Data':{'items':[{'alias':'YAPI','item':'{\'AD\':\'Ali Paşa Mah. Muhtarlığı\',\'Geometry\':\'POLYGON((29.984236 39.419895, 29.984293 39.41977, 29.984067 39.419729, 29.983984 39.419837, 29.984236 39.419895))\',\'SITEVEYAKOOPERATIFADI\':\'null\',\'POSTAKODU\':\'null\',\'OLCEK\':0,\'PARSELID\':0,\'PARSELKIMLIKNO\':0,\'KIMLIKNO\':0,\'KAYNAK\':0,\'TIP\':0,\'ZEMINALTIKATSAYISI\':0,\'ZEMINUSTUKATSAYISI\':0,\'SRID\':3857,\'ACIKLAMA\':\'null\',\'DEGISTIRMETARIHI\':\'20160428110043\',\'OLUSTURMATARIHI\':\'2016-04-28 11:20:00\',\'LAST_SYNC_DATETIME\':\'20160416115207\',\'IS_SYNCHED\':0,\'ID\':2,\'LAST_SYNC_ID\':1509,\'OLUSTURAN\':0,\'DURUM\':1,\'OLUSUMYONTEMI\':0,\'DEGISTIREN\':0,\'OPERATIONNUMBER\':0}','clientId':2,'itemState':0,'senkronizasyonDetayId':0,'senkronizasyonId':0,'serverId':0},{'alias':'YOLORTAHAT','item':'{\'AD\':\'Sümer\',\'Geometry\':\'LINESTRING(29.98429 39.419296, 29.984022 39.419565, 29.983766 39.419816, 29.983533 39.420013)\',\'KIMLIKNO\':0,\'OLCEK\':0,\'TIP\':1,\'YERLESIMACISI\':0.0,\'YOLID\':0,\'SRID\':3857,\'DEGISTIRMETARIHI\':\'20160428105932\',\'OLUSTURMATARIHI\':\'2016-04-28 11:20:00\',\'LAST_SYNC_DATETIME\':\'20160416115208\',\'IS_SYNCHED\':0,\'ID\':1,\'LAST_SYNC_ID\':1509,\'OLUSTURAN\':0,\'DURUM\':1,\'OLUSUMYONTEMI\':0,\'DEGISTIREN\':0,\'OPERATIONNUMBER\':0}','clientId':1,'itemState':0,'senkronizasyonDetayId':0,'senkronizasyonId':0,'serverId':0},{'alias':'NUMARATAJ','item':'{\'AD\':\'numarataj\',\'KAPINO\':\'1\',\'Geometry\':\'LINESTRING(29.983852 39.419711, 29.98404 39.419764)\',\'DIGERYAPIID\':0,\'BINAKODU\':0,\'BAGLIOLDUGUNUMARATAJID\':0,\'KIMLIKNO\':0,\'PARSELID\':0,\'PARSELKIMLIKNO\':0,\'TIP\':0,\'YAPIID\':2,\'YERLESIMACISI\':0.0,\'YOLORTAHATYONID\':1,\'SRID\':3857,\'OLUSTURMATARIHI\':\'2016-04-28 11:20:00\',\'IS_SYNCHED\':0,\'ID\':1,\'LAST_SYNC_ID\':0,\'OLUSTURAN\':0,\'DURUM\':0,\'OLUSUMYONTEMI\':0,\'DEGISTIREN\':0,\'OPERATIONNUMBER\':0}','clientId':1,'itemState':0,'senkronizasyonDetayId':0,'senkronizasyonId':0,'serverId':0}],'pageCount':0,'totalItemCount':0},'AndroidId':'4766ce689a0e14e3','PageSize':0,'IsSuccess':false,'PageIndex':0,'SenkronizasyonId':1543}";
            //_ChangeSetService = ServiceManager.GetService<IChangeSetService>();
            //StartSyncInput sync = JsonConvert.DeserializeObject<StartSyncInput>(startSyncJson);
            //SyncClientChangeSetInput syncClient = JsonConvert.DeserializeObject<SyncClientChangeSetInput>(syncClientChangeSetJson);

            //ApiTransferObject<int> result = _ChangeSetService.StartSync(sync.AndroidId, sync.Data);
            //_ChangeSetService.SyncClientChangeSet(syncClient.AndroidId, result.Data, syncClient.Data);


            string startSyncJson = @"{'Data':[{'tabloAdi':'YAPI','isSuccess':false,'operasyonTipi':0,'senkronizasyonDetayId':0,'senkronizasyonId':0,'serverId':0,'clientId':1,'tranId':0},{'tabloAdi':'YOL','isSuccess':false,'operasyonTipi':0,'senkronizasyonDetayId':0,'senkronizasyonId':0,'serverId':0,'clientId':1,'tranId':0},{'tabloAdi':'NUMARATAJ','isSuccess':false,'operasyonTipi':0,'senkronizasyonDetayId':0,'senkronizasyonId':0,'serverId':0,'clientId':1,'tranId':0}],'AndroidId':'4766ce689a0e14e3','PageSize':0,'IsSuccess':false,'PageIndex':0,'SenkronizasyonId':0}";
            string syncClientChangeSetJson = @"{'Data':{'items':[{'alias':'YAPI','item':'{\'AD\':\'bina\',\'Geometry\':\'POLYGON((29.983581 39.41795, 29.984606 39.418082, 29.984552 39.416797, 29.983774 39.416777, 29.983581 39.41795))\',\'OLCEK\':0,\'PARSELID\':0,\'PARSELKIMLIKNO\':0,\'KIMLIKNO\':0,\'KAYNAK\':0,\'TIP\':0,\'ZEMINALTIKATSAYISI\':0,\'ZEMINUSTUKATSAYISI\':0,\'SRID\':3857,\'OLUSTURMATARIHI\':\'2016-04-30 14:34:22\',\'IS_SYNCHED\':0,\'ID\':1,\'LAST_SYNC_ID\':0,\'OLUSTURAN\':0,\'DURUM\':0,\'OLUSUMYONTEMI\':0,\'DEGISTIREN\':0,\'OPERATIONNUMBER\':0}','clientId':1,'itemState':0,'senkronizasyonDetayId':0,'senkronizasyonId':0,'serverId':0},{'alias':'YOL','item':'{\'AD\':\'yol\',\'Geometry\':\'LINESTRING(29.9835 39.418875, 29.986193 39.418086)\',\'CIFTBITISNO\':0,\'CIFTBASLANGICNO\':0,\'KIMLIKNO\':0,\'TEKBASLANGICNO\':0,\'TEKBITISNO\':0,\'TIP\':0,\'SRID\':3857,\'OLUSTURMATARIHI\':\'2016-04-30 14:34:22\',\'IS_SYNCHED\':0,\'ID\':1,\'LAST_SYNC_ID\':0,\'OLUSTURAN\':0,\'DURUM\':0,\'OLUSUMYONTEMI\':0,\'DEGISTIREN\':0,\'OPERATIONNUMBER\':0}','clientId':1,'itemState':0,'senkronizasyonDetayId':0,'senkronizasyonId':0,'serverId':0},{'alias':'NUMARATAJ','item':'{\'AD\':\'numarataj\',\'Geometry\':\'LINESTRING(29.984425 39.418604, 29.984227 39.418033)\',\'DIGERYAPIID\':0,\'BINAKODU\':0,\'BAGLIOLDUGUNUMARATAJID\':0,\'KIMLIKNO\':0,\'PARSELID\':0,\'PARSELKIMLIKNO\':0,\'TIP\':0,\'YAPIID\':1,\'YERLESIMACISI\':0.0,\'YOLORTAHATYONID\':1,\'SRID\':3857,\'OLUSTURMATARIHI\':\'2016-04-30 14:34:22\',\'IS_SYNCHED\':0,\'ID\':1,\'LAST_SYNC_ID\':0,\'OLUSTURAN\':0,\'DURUM\':0,\'OLUSUMYONTEMI\':0,\'DEGISTIREN\':0,\'OPERATIONNUMBER\':0}','clientId':1,'itemState':0,'senkronizasyonDetayId':0,'senkronizasyonId':0,'serverId':0}],'pageCount':0,'totalItemCount':0},'AndroidId':'4766ce689a0e14e3','PageSize':0,'IsSuccess':false,'PageIndex':0,'SenkronizasyonId':1678}";
            _ChangeSetService = FrameworkCtx.Current.Resolve<IChangeSetService>();
            StartSyncInput sync = JsonConvert.DeserializeObject<StartSyncInput>(startSyncJson);
            SyncClientChangeSetInput syncClient = JsonConvert.DeserializeObject<SyncClientChangeSetInput>(syncClientChangeSetJson);

            ApiTransferObject<int> result = _ChangeSetService.SyncStart(sync.AndroidId, sync.Data);
            _ChangeSetService.SyncSendClientChanges(syncClient.AndroidId, result.Data, syncClient.Data);
        }
    }
}