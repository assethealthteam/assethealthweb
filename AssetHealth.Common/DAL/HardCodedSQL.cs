﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetHealth.Common.DAL
{
    public class HardCodedSQL
    {

        //public const string DASHBOARD_SP_STATISTICS = "CALL _SP_STATISTICS(@startDate, @endDate, @groupBy, @userId, @locId);";
        public const string DASHBOARD_SP_STATISTICS = "CALL _SP_STATISTICS(@exec_mode, @tenant_id, @locId, @roleId, @startDate, @endDate, @groupBy);";
        public const string DASHBOARD_SP_STATISTICS_CALCULATE = "CALL _SP_STATISTICS_CALCULATE(@exec_mode, @tenant_id, @locId, @roleId, @startDate, @endDate, @groupBy);";
        public const string DASHBOARD_VCURRENT_WARNINGS = "SELECT * FROM VCURRENT_WARNINGS WHERE READ_NO between @startDate AND @endDate AND LOC_ID IN ({0}) AND LANGUAGE_ID = {1}";
        public const string DASHBOARD_VQUESTION_GRID = "SELECT * FROM VQUESTION_GRID WHERE Location_Id IN({0}) ORDER BY Location_Id, DISPLAY_ORDER";
        //public const string DASHBOARD_USER_STATISTICS = "CALL _USER_STATISTICS(@startDate, @endDate, @groupBy, @userId, @languageId, @locId);";
        public const string DASHBOARD_USER_STATISTICS = "CALL _USER_STATISTICS(@exec_mode, @tenant_id, @locId, @roleId, @startDate, @endDate, @groupBy, @languageId);";
        public const string DASHBOARD_SP_MAP_POLL = "CALL _SP_MAP_POLL(@startDate, @endDate, @userId);";

        public const string REMOTE_ADMIN_GET_FILES = "SELECT * FROM FILES";
        public const string DEVICE_UPDATE_TENANT_ID = "UPDATE DEVICE SET TENANT_ID = '{0}' WHERE Id = {1}";
        public const string DEVICE_SQLECT_ALL_USERS = "SELECT `Id`, `TENANT_ID` as tenant_id, `USER_GUID` as UserGuid, `USERNAME` as Username, `PASSWORD` as Password,`PASSWORD_FORMAT_ID` PasswordFormatId, `PASSWORD_SALT` PasswordSalt, `EMAIL` Email, `IDENTITY_NUMBER` IdentityNumber, `SYSTEM_NAME` SystemName, `FIRST_NAME` FirstName, `LAST_NAME` LastName, `BIRTH_DATE` BirthDate, `HIRE_DATE` HireDate, `EXTENSION`, `MOBILE_PHONE` MobilePhone, `NOTES`, `DESCRIPTION`, `ACTIVE`, `DELETED`, `IS_SYSTEM_ACCOUNT` IsSystemAccount, `LAST_IP_ADDRESS` LastIpAddress, `LAST_LOGIN_DATETIME` LastLoginDateTime, `LAST_PWD_CHANGED_DATETIME` LastPasswordChangedDateTime, `LAST_ACTIVITY_DATETIME` LastActivityDateTime, `FAILED_PASSWORD_ATTEMPT_COUNT` FailedPasswordAttemptCount, `ORGUNIT_ID`, `JOBTITLE_ID`, `LANGUAGE_ID` WorkingLanguage_Id, `INSERT_USER_ID`, `UPDATE_USER_ID`, `INSERT_DATETIME`, `UPDATE_DATETIME`, `MANAGER_ID` FROM user";
        public const string FORM_QUESTION_GET_IDS_FOR_BATCH_UPDATE = "SELECT * FROM FORM_QUESTION WHERE Id in({0})";
        public const string SYNCHRONIZATION_GET_FILES_OF_QUESTIONS = "SELECT * FROM FILES f WHERE f.Entity_name = 'Formquestion' AND f.Entity_id in({0})";

        public const string ANSWER_ANALYZE_SP = "CALL _SP_ANALYZE({0}, {1}, {2}, {3}, {4} );"; // answerId, startDate, endDate, userId, languageId 

        public const string CHANNEL_GET_AUTHORIZED_ONES = "SELECT * from eq_channel c, equipment e WHERE c.EQUIPMENT_ID = e.Id AND  _FUserAuthorized(e.EQ_LOCATION_ID , @user_id) = 1";
        public const string EQ_LOCATION_GET_AUTHORIZED_ONES = "SELECT * from eq_location e WHERE Parent_Id is not null AND _FUserAuthorized(Id ,@user_id) = 1";
        public const string EQUIPMENT_GET_AUTHORIZED_ONES = "SELECT * from equipment e WHERE _FUserAuthorized(EQ_LOCATION_ID ,@user_id) = 1";

        public const string HEADER_ACTION_UPDATE_TENANT_ID = "UPDATE user SET TENANT_ID = '{0}' WHERE Id = {1};\r\n";
        public const string HEADER_ACTION_INSERT_LOC_USER_MAP = "INSERT INTO eq_location_user_mapping(EQ_LOCATION_ID, USER_ID) SELECT MIN(Id), {0} FROM eq_location l WHERE l.PARENT_ID is null AND l.TENANT_ID = '{1}';";

        public const string USER_EXT_GET_USER_RELATED_LOCS = "CALL _GetUserRelatedLocations(@user_id);";
        public const string USER_EXT_USER_AUTHORIZED = "CALL _UserAuthorized(@location_id , @user_id, @isAuthorized);";
    }
}
