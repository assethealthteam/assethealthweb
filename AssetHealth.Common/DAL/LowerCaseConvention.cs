﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetHealth.Common.DAL
{
    public class LowerCaseConvention : IStoreModelConvention<EntitySet>, IStoreModelConvention<EdmProperty>
    {
        public void Apply(EntitySet item, DbModel model)
        {
            item.Table = MakeLowerCase(item.Table);
        }

        public void Apply(EdmProperty item, DbModel model)
        {
            item.Name = MakeLowerCase(item.Name);
        }

        protected virtual string MakeLowerCase(string s)
        {
            return s.ToLowerInvariant();
        }
    }
    
}
