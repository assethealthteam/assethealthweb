﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.DAL.Repository;
using AssetHealth.Common.DTO;



namespace AssetHealth.Common.DAL.Repository
{
	
	/// <summary>
	/// A Repository component to manage aa_excel_import.
	/// </summary>
	public interface IAaExcelImportRepository   : IRepository<AaExcelImport>   
	{
		
		
	}

}