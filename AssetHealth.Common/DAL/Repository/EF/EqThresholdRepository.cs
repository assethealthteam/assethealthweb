﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ares.Core.Framework.Logging;
using Ares.DAL.Repository.EF;
using Autofac.Extras.Attributed;
using AssetHealth.Common.DTO;



namespace AssetHealth.Common.DAL.Repository.EF
{
	
    /// <summary>
    /// An EF Repository component to manage eq_threshold.
    /// </summary>
    public class EqThresholdRepository   : EFRepositoryBase<EqThreshold  , int>, IEqThresholdRepository 
    {
        public EqThresholdRepository([WithKey("DefaultConnection")] IDbContext dbContext, ILogger logger) : base(dbContext, logger)
        {

        }
		
    }

}