﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssetHealth.Common.DTO;
using Ares.DAL.Repository.EF.Mapping;

namespace AssetHealth.Common.DAL.Mapping.EF
{
	
	/// <summary>
	/// An EF Entity Type Configuration component to manage form_answer.
	/// </summary>
	public class FormAnswerEntityTypeConfiguration   : AresEntityTypeConfiguration<FormAnswer>   
	{
         public FormAnswerEntityTypeConfiguration()
        {
		//key and properties
            this.ToTable("form_answer" , "");
            this.HasKey(ba => ba.Id);
            this.Property(ba => ba.Object_id).HasColumnName("OBJECT_ID");
            this.Property(ba => ba.Form_question_id).HasColumnName("FORM_QUESTION_Id");
            this.Property(ba => ba.Device_id).HasColumnName("DEVICE_ID").IsOptional();
            this.Property(ba => ba.Form_answer_round_id).HasColumnName("FORM_ANSWER_ROUND_Id");
            this.Property(ba => ba.Previous_answer).HasColumnName("PREVIOUS_ANSWER").IsOptional();
            this.Property(ba => ba.Username).HasColumnName("USERNAME").IsOptional();
            this.Property(ba => ba.Read_no).HasColumnName("READ_NO").IsOptional();
            this.Property(ba => ba.Warning_type).HasColumnName("WARNING_TYPE").IsOptional();
            this.Property(ba => ba.Answer).HasColumnName("ANSWER");
            this.Property(ba => ba.Long_answer).HasColumnName("LONG_ANSWER").IsOptional();
            this.Property(ba => ba.Converted_answer).HasColumnName("CONVERTED_ANSWER").IsOptional();
            this.Property(ba => ba.Json).HasColumnName("JSON").IsOptional();
            this.Property(ba => ba.Required_read_datetime).HasColumnName("REQUIRED_READ_DATETIME").IsOptional();
            this.Property(ba => ba.Actual_read_datetime).HasColumnName("ACTUAL_READ_DATETIME").IsOptional();
            this.Property(ba => ba.Description).HasColumnName("DESCRIPTION").IsOptional();
            this.Property(ba => ba.Notes).HasColumnName("NOTES").IsOptional();
            this.Property(ba => ba.Udid).HasColumnName("UDID").IsOptional();
            this.Property(ba => ba.Insert_user_id).HasColumnName("INSERT_USER_ID").IsOptional();
            this.Property(ba => ba.Update_user_id).HasColumnName("UPDATE_USER_ID").IsOptional();
            this.Property(ba => ba.Insert_datetime).HasColumnName("INSERT_DATETIME").IsOptional();
            this.Property(ba => ba.Update_datetime).HasColumnName("UPDATE_DATETIME").IsOptional();
            this.Ignore(ba => ba.HasMedia);


//this.HasOptional(p => p.ANSWER_DEVICE).WithMany(s => s.DEVICEForm_answer).HasForeignKey(f => f.Device_id);

            this.HasOptional(p => p.ANSWER_PREVIOUS_ANSWER).WithMany(s => s.PREVIOUS_ANSWERForm_answer).HasForeignKey(f => f.Previous_answer);

this.HasRequired(p => p.ANSWER_QUESTION).WithMany(s => s.FORM_QUESTIONForm_answer).HasForeignKey(f => f.Form_question_id);

this.HasRequired(p => p.ANSWER_ROUND).WithMany(s => s.FORM_ANSWER_ROUNDForm_answer).HasForeignKey(f => f.Form_answer_round_id);

this.HasOptional(p => p.ANSWER_WARNING_PARAMITEMS).WithMany().HasForeignKey(f => f.Warning_type);
        }
        
        
	}

}


