﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssetHealth.Common.DTO;
using Ares.DAL.Repository.EF.Mapping;

namespace AssetHealth.Common.DAL.Mapping.EF
{
	
	/// <summary>
	/// An EF Entity Type Configuration component to manage form.
	/// </summary>
	public class FormEntityTypeConfiguration   : AresEntityTypeConfiguration<Form>   
	{
         public FormEntityTypeConfiguration()
        {
		//key and properties
            this.ToTable("form" , "");
            this.HasKey(ba => ba.Id);
            this.Property(ba => ba.Object_id).HasColumnName("OBJECT_ID");
            this.Property(ba => ba.Category_id).HasColumnName("CATEGORY_Id").IsOptional();
            this.Property(ba => ba.Derived_id).HasColumnName("DERIVED_Id").IsOptional();
            this.Property(ba => ba.Language_id).HasColumnName("LANGUAGE_ID").IsOptional();
            this.Property(ba => ba.Period).HasColumnName("PERIOD").IsOptional();
            this.Property(ba => ba.Equipment_id).HasColumnName("EQUIPMENT_ID").IsOptional();
            this.Property(ba => ba.Role_id).HasColumnName("ROLE_ID").IsOptional();
            this.Property(ba => ba.Server_id).HasColumnName("SERVER_ID").IsOptional();
            this.Property(ba => ba.Code).HasColumnName("CODE").IsOptional();
            this.Property(ba => ba.Title).HasColumnName("TITLE");
            this.Property(ba => ba.Description).HasColumnName("DESCRIPTION").IsOptional();
            this.Property(ba => ba.Notes).HasColumnName("NOTES").IsOptional();
            this.Property(ba => ba.Version).HasColumnName("VERSION").IsOptional();
            this.Property(ba => ba.Image_path).HasColumnName("IMAGE_PATH").IsOptional();
            this.Property(ba => ba.Form_json).HasColumnName("FORM_JSON").IsOptional();
            this.Property(ba => ba.Form_json2).HasColumnName("FORM_JSON2").IsOptional();
            this.Property(ba => ba.Active).HasColumnName("ACTIVE").IsOptional();
            this.Property(ba => ba.Deleted).HasColumnName("DELETED").IsOptional();
            this.Property(ba => ba.Display_order).HasColumnName("DISPLAY_ORDER").IsOptional();
            this.Property(ba => ba.Insert_user_id).HasColumnName("INSERT_USER_ID").IsOptional();
            this.Property(ba => ba.Update_user_id).HasColumnName("UPDATE_USER_ID").IsOptional();
            this.Property(ba => ba.Insert_datetime).HasColumnName("INSERT_DATETIME").IsOptional();
            this.Property(ba => ba.Update_datetime).HasColumnName("UPDATE_DATETIME").IsOptional();
        


this.HasOptional(p => p.DERIVED).WithMany(s => s.DERIVEDForm).HasForeignKey(f => f.Derived_id);

this.HasOptional(p => p.EQUIPMENT).WithMany().HasForeignKey(f => f.Equipment_id);

this.HasOptional(p => p.LANGUAGE).WithMany().HasForeignKey(f => f.Language_id);

this.HasOptional(p => p.PARAMETER_ITEMS).WithMany().HasForeignKey(f => f.Category_id);

this.HasOptional(p => p.PERIOD_PARAMITEMS).WithMany().HasForeignKey(f => f.Period);

this.HasOptional(p => p.ROLE).WithMany().HasForeignKey(f => f.Role_id);
        }
        
        
	}

}


