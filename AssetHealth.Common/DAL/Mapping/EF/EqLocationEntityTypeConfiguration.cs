﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssetHealth.Common.DTO;
using Ares.UMS.DTO.Users;

namespace AssetHealth.Common.DAL.Mapping.EF
{
	
	/// <summary>
	/// An EF Entity Type Configuration component to manage eq_location.
	/// </summary>
	public class EqLocationEntityTypeConfiguration   : EntityTypeConfiguration<EqLocation>   
	{
         public EqLocationEntityTypeConfiguration()
        {
		//key and properties
            this.ToTable("eq_location" , "");
            this.HasKey(ba => ba.Id);
            this.Property(ba => ba.Object_id).HasColumnName("OBJECT_ID");
            this.Property(ba => ba.Location_name).HasColumnName("LOCATION_NAME").IsOptional();
            this.Property(ba => ba.Cid).HasColumnName("CID").IsOptional();
            this.Property(ba => ba.Description).HasColumnName("DESCRIPTION").IsOptional();
            this.Property(ba => ba.Display_order).HasColumnName("DISPLAY_ORDER").IsOptional();
            this.Property(ba => ba.Schema_path).HasColumnName("SCHEMA_PATH").IsOptional();
            this.Property(ba => ba.Latitude).HasColumnName("LATITUDE").IsOptional();
            this.Property(ba => ba.Longitude).HasColumnName("LONGITUDE").IsOptional();
            this.Property(ba => ba.Parent_id).HasColumnName("PARENT_ID").IsOptional();
            this.Property(ba => ba.Insert_user_id).HasColumnName("INSERT_USER_ID").IsOptional();
            this.Property(ba => ba.Update_user_id).HasColumnName("UPDATE_USER_ID").IsOptional();
            this.Property(ba => ba.Insert_datetime).HasColumnName("INSERT_DATETIME").IsOptional();
            this.Property(ba => ba.Update_datetime).HasColumnName("UPDATE_DATETIME").IsOptional();
        

this.HasOptional(p => p.ParentLocation).WithMany(s => s.PARENTEq_location).HasForeignKey(f => f.Parent_id);

            this.HasMany<User>(pr => pr.User_From_EqLocationUserMapping)
                .WithMany()
                .Map(m =>
                {
                    m.ToTable("eq_location_user_mapping", "");
                    m.MapLeftKey("EQ_LOCATION_ID");
                    m.MapRightKey("USER_ID");
                });
        }
        
        
	}

}


