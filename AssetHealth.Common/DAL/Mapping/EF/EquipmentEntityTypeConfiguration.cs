﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ares.DAL.Repository.EF.Mapping;
using Ares.UMS.DTO.Users;
using AssetHealth.Common.DTO;



namespace AssetHealth.Common.DAL.Mapping.EF
{
	
	/// <summary>
	/// An EF Entity Type Configuration component to manage equipment.
	/// </summary>
	public class EquipmentEntityTypeConfiguration   : AresEntityTypeConfiguration<Equipment>   
	{
         public EquipmentEntityTypeConfiguration()
        {
		//key and properties
            this.ToTable("equipment" , "");
            this.HasKey(ba => ba.Id);
            this.Property(ba => ba.Object_id).HasColumnName("OBJECT_ID");
            this.Property(ba => ba.Eq_location_id).HasColumnName("EQ_LOCATION_ID");
            this.Property(ba => ba.Equipment_type).HasColumnName("EQUIPMENT_TYPE").IsOptional();
            this.Property(ba => ba.Ip_address).HasColumnName("IP_ADDRESS").IsOptional();
            this.Property(ba => ba.Name).HasColumnName("NAME").IsOptional();
            this.Property(ba => ba.Number_of_channels).HasColumnName("NUMBER_OF_CHANNELS").IsOptional();
            this.Property(ba => ba.Cid).HasColumnName("CID").IsOptional();
            this.Property(ba => ba.Description).HasColumnName("DESCRIPTION").IsOptional();
            this.Property(ba => ba.Read_order).HasColumnName("READ_ORDER").IsOptional();
            this.Property(ba => ba.Active).HasColumnName("ACTIVE").IsOptional();
            this.Property(ba => ba.Insert_user_id).HasColumnName("INSERT_USER_ID").IsOptional();
            this.Property(ba => ba.Update_user_id).HasColumnName("UPDATE_USER_ID").IsOptional();
            this.Property(ba => ba.Insert_datetime).HasColumnName("INSERT_DATETIME").IsOptional();
            this.Property(ba => ba.Update_datetime).HasColumnName("UPDATE_DATETIME").IsOptional();
        

this.HasOptional(p => p.EQUIPMENT_TYPE_PARAMITEMS).WithMany().HasForeignKey(f => f.Equipment_type);

this.HasRequired(p => p.Location).WithMany(s => s.EQ_LOCATIONEquipment).HasForeignKey(f => f.Eq_location_id);
        }
        
        
	}

}


