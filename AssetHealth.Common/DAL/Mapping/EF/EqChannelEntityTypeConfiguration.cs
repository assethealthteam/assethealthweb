﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ares.DAL.Repository.EF.Mapping;
using Ares.UMS.DTO.Users;
using AssetHealth.Common.DTO;



namespace AssetHealth.Common.DAL.Mapping.EF
{
	
	/// <summary>
	/// An EF Entity Type Configuration component to manage eq_channel.
	/// </summary>
	public class EqChannelEntityTypeConfiguration   : AresEntityTypeConfiguration<EqChannel>   
	{
         public EqChannelEntityTypeConfiguration()
        {
		//key and properties
            this.ToTable("eq_channel" , "");
            this.HasKey(ba => ba.Id);
            this.Property(ba => ba.Object_id).HasColumnName("OBJECT_ID");
            this.Property(ba => ba.Equipment_id).HasColumnName("EQUIPMENT_ID");
            this.Property(ba => ba.Sensor_type).HasColumnName("SENSOR_TYPE").IsOptional();
            this.Property(ba => ba.Name).HasColumnName("NAME").IsOptional();
            this.Property(ba => ba.Channel_number).HasColumnName("CHANNEL_NUMBER").IsOptional();
            this.Property(ba => ba.Active).HasColumnName("ACTIVE").IsOptional();
            this.Property(ba => ba.Cid).HasColumnName("CID").IsOptional();
            this.Property(ba => ba.Description).HasColumnName("DESCRIPTION").IsOptional();
            this.Property(ba => ba.Insert_user_id).HasColumnName("INSERT_USER_ID").IsOptional();
            this.Property(ba => ba.Update_user_id).HasColumnName("UPDATE_USER_ID").IsOptional();
            this.Property(ba => ba.Insert_datetime).HasColumnName("INSERT_DATETIME").IsOptional();
            this.Property(ba => ba.Update_datetime).HasColumnName("UPDATE_DATETIME").IsOptional();
        

this.HasOptional(p => p.CHANNEL_SENSOR_TYPE_PARAMITEMS).WithMany().HasForeignKey(f => f.Sensor_type);

this.HasRequired(p => p.channel_equipment).WithMany(s => s.EQUIPMENTEq_channel).HasForeignKey(f => f.Equipment_id);
        }
        
        
	}

}


