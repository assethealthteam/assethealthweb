﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ares.DAL.Repository.EF.Mapping;
using Ares.UMS.DTO.Users;
using AssetHealth.Common.DTO;



namespace AssetHealth.Common.DAL.Mapping.EF
{
	
	/// <summary>
	/// An EF Entity Type Configuration component to manage form_notes.
	/// </summary>
	public class FormNoteEntityTypeConfiguration   : AresEntityTypeConfiguration<FormNote>   
	{
         public FormNoteEntityTypeConfiguration()
        {
		//key and properties
            this.ToTable("form_notes" , "");
            this.HasKey(ba => ba.Id);
            this.Property(ba => ba.Object_id).HasColumnName("OBJECT_ID");
            this.Property(ba => ba.Device_id).HasColumnName("DEVICE_ID").IsOptional();
            this.Property(ba => ba.Form_answer_round_id).HasColumnName("FORM_ANSWER_ROUND_Id");
            this.Property(ba => ba.Username).HasColumnName("USERNAME").IsOptional();
            this.Property(ba => ba.Read_no).HasColumnName("READ_NO").IsOptional();
            this.Property(ba => ba.Warning_type).HasColumnName("WARNING_TYPE").IsOptional();
            this.Property(ba => ba.Server_id).HasColumnName("SERVER_ID");
            this.Property(ba => ba.Title).HasColumnName("TITLE");
            this.Property(ba => ba.Description).HasColumnName("DESCRIPTION").IsOptional();
            this.Property(ba => ba.Notes).HasColumnName("NOTES").IsOptional();
            this.Property(ba => ba.Actual_read_datetime).HasColumnName("ACTUAL_READ_DATETIME").IsOptional();
            this.Property(ba => ba.Insert_user_id).HasColumnName("INSERT_USER_ID").IsOptional();
            this.Property(ba => ba.Update_user_id).HasColumnName("UPDATE_USER_ID").IsOptional();
            this.Property(ba => ba.Insert_datetime).HasColumnName("INSERT_DATETIME").IsOptional();
            this.Property(ba => ba.Update_datetime).HasColumnName("UPDATE_DATETIME").IsOptional();
        

//this.HasOptional(p => p.NOTES_DEVICE).WithMany(s => s.DEVICEForm_notes).HasForeignKey(f => f.Device_id);

this.HasRequired(p => p.NOTES_ROUND).WithMany(s => s.FORM_ANSWER_ROUNDForm_notes).HasForeignKey(f => f.Form_answer_round_id);

this.HasOptional(p => p.NOTES_WARNING_PARAMITEMS).WithMany().HasForeignKey(f => f.Warning_type);
        }
        
        
	}

}


