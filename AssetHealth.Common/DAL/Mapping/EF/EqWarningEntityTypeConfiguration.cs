﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssetHealth.Common.DTO;



namespace AssetHealth.Common.DAL.Mapping.EF
{
	
	/// <summary>
	/// An EF Entity Type Configuration component to manage eq_warning.
	/// </summary>
	public class EqWarningEntityTypeConfiguration   : EntityTypeConfiguration<EqWarning>   
	{
         public EqWarningEntityTypeConfiguration()
        {
		//key and properties
            this.ToTable("eq_warning" , "");
            this.HasKey(ba => ba.Id);
            this.Property(ba => ba.Object_id).HasColumnName("OBJECT_ID");
            this.Property(ba => ba.Read_no).HasColumnName("READ_NO").IsOptional();
            this.Property(ba => ba.Equipment_id).HasColumnName("EQUIPMENT_ID").IsOptional();
            this.Property(ba => ba.Eq_channel_id).HasColumnName("EQ_CHANNEL_ID").IsOptional();
            this.Property(ba => ba.Real_value).HasColumnName("REAL_VALUE").IsOptional();
            this.Property(ba => ba.Io_real_value).HasColumnName("IO_REAL_VALUE").IsOptional();
            this.Property(ba => ba.Io_input_value).HasColumnName("IO_INPUT_VALUE").IsOptional();
            this.Property(ba => ba.Lower_limit).HasColumnName("LOWER_LIMIT").IsOptional();
            this.Property(ba => ba.Upper_limit).HasColumnName("UPPER_LIMIT").IsOptional();
            this.Property(ba => ba.Level_increase_repeat_times).HasColumnName("LEVEL_INCREASE_REPEAT_TIMES").IsOptional();
            this.Property(ba => ba.Level_decrease_repeat_times).HasColumnName("LEVEL_DECREASE_REPEAT_TIMES").IsOptional();
            this.Property(ba => ba.Strategy).HasColumnName("STRATEGY").IsOptional();
            this.Property(ba => ba.Repeat_count).HasColumnName("REPEAT_COUNT").IsOptional();
            this.Property(ba => ba.Pre_warning_type).HasColumnName("PRE_WARNING_TYPE").IsOptional();
            this.Property(ba => ba.Warning_type).HasColumnName("WARNING_TYPE").IsOptional();
            this.Property(ba => ba.Active).HasColumnName("ACTIVE").IsOptional();
            this.Property(ba => ba.Description).HasColumnName("DESCRIPTION").IsOptional();
            this.Property(ba => ba.Last_stop_datetime).HasColumnName("LAST_STOP_DATETIME").IsOptional();
            this.Property(ba => ba.Insert_user_id).HasColumnName("INSERT_USER_ID").IsOptional();
            this.Property(ba => ba.Update_user_id).HasColumnName("UPDATE_USER_ID").IsOptional();
            this.Property(ba => ba.Insert_datetime).HasColumnName("INSERT_DATETIME").IsOptional();
            this.Property(ba => ba.Update_datetime).HasColumnName("UPDATE_DATETIME").IsOptional();
        
this.HasOptional(p => p.WARNING_PRE_WARNING_PARAMITEMS).WithMany().HasForeignKey(f => f.Pre_warning_type);

this.HasOptional(p => p.WARNING_STRATEGY_PARAMITEMS).WithMany().HasForeignKey(f => f.Strategy);

this.HasOptional(p => p.WARNING_WARNING_PARAMITEMS).WithMany().HasForeignKey(f => f.Warning_type);

this.HasOptional(p => p.Warning_EQUIPMENT).WithMany(s => s.EQUIPMENTEq_warning).HasForeignKey(f => f.Equipment_id);

this.HasOptional(p => p.Warning_EQ_CHANNEL).WithMany(s => s.EQ_CHANNELEq_warning).HasForeignKey(f => f.Eq_channel_id);
        }
        
        
	}

}


