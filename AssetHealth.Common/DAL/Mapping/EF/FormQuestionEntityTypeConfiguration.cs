﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssetHealth.Common.DTO;
using Ares.DAL.Repository.EF.Mapping;

namespace AssetHealth.Common.DAL.Mapping.EF
{
	
	/// <summary>
	/// An EF Entity Type Configuration component to manage form_question.
	/// </summary>
	public class FormQuestionEntityTypeConfiguration   : AresEntityTypeConfiguration<FormQuestion>   
	{
         public FormQuestionEntityTypeConfiguration()
        {
		//key and properties
            this.ToTable("form_question" , "");
            this.HasKey(ba => ba.Id);
            this.Property(ba => ba.Object_id).HasColumnName("OBJECT_ID");
            this.Property(ba => ba.Category_id).HasColumnName("CATEGORY_Id").IsOptional();
            this.Property(ba => ba.Type_id).HasColumnName("TYPE_Id").IsOptional();
            this.Property(ba => ba.Form_id).HasColumnName("FORM_Id");
            this.Property(ba => ba.Derived_id).HasColumnName("DERIVED_Id").IsOptional();
            this.Property(ba => ba.Eq_channel_id).HasColumnName("EQ_CHANNEL_Id").IsOptional();
            this.Property(ba => ba.Code).HasColumnName("CODE").IsOptional();
            this.Property(ba => ba.Title).HasColumnName("TITLE");
            this.Property(ba => ba.Sublocation_name).HasColumnName("SUBLOCATION_NAME").IsOptional();
            this.Property(ba => ba.Criticality).HasColumnName("CRITICALITY").IsOptional();
            this.Property(ba => ba.Hint).HasColumnName("HINT").IsOptional();
            this.Property(ba => ba.Unhealthy_cause).HasColumnName("UNHEALTHY_CAUSE").IsOptional();
            this.Property(ba => ba.Last_answer_id).HasColumnName("LAST_ANSWER_ID").IsOptional();
            this.Property(ba => ba.Last_answer_datetime).HasColumnName("LAST_ANSWER_DATETIME").IsOptional();
            this.Property(ba => ba.Server_id).HasColumnName("SERVER_ID").IsOptional();
            this.Property(ba => ba.Description).HasColumnName("DESCRIPTION").IsOptional();
            this.Property(ba => ba.Version).HasColumnName("VERSION").IsOptional();
            this.Property(ba => ba.Image_path).HasColumnName("IMAGE_PATH").IsOptional();
            this.Property(ba => ba.Device_image_path).HasColumnName("DEVICE_IMAGE_PATH").IsOptional();
            this.Property(ba => ba.Estimated_time).HasColumnName("ESTIMATED_TIME").IsOptional();
            this.Property(ba => ba.Method).HasColumnName("METHOD").IsOptional();
            this.Property(ba => ba.Help_text).HasColumnName("HELP_TEXT").IsOptional();
            this.Property(ba => ba.Json).HasColumnName("JSON").IsOptional();
            this.Property(ba => ba.Display_order).HasColumnName("DISPLAY_ORDER").IsOptional();
            this.Property(ba => ba.Step).HasColumnName("STEP").IsOptional();
            this.Property(ba => ba.Period).HasColumnName("PERIOD").IsOptional();
            this.Property(ba => ba.Option_group_id).HasColumnName("OPTION_GROUP_ID").IsOptional();
            this.Property(ba => ba.V_required).HasColumnName("V_REQUIRED").IsOptional();
            this.Property(ba => ba.V_email).HasColumnName("V_EMAIL").IsOptional();
            this.Property(ba => ba.V_min_length).HasColumnName("V_MIN_LENGTH").IsOptional();
            this.Property(ba => ba.V_max_length).HasColumnName("V_MAX_LENGTH").IsOptional();
            this.Property(ba => ba.V_regex).HasColumnName("V_REGEX").IsOptional();
            this.Property(ba => ba.Active).HasColumnName("ACTIVE");
            this.Property(ba => ba.Deleted).HasColumnName("DELETED");
            this.Property(ba => ba.Insert_user_id).HasColumnName("INSERT_USER_ID").IsOptional();
            this.Property(ba => ba.Update_user_id).HasColumnName("UPDATE_USER_ID").IsOptional();
            this.Property(ba => ba.Insert_datetime).HasColumnName("INSERT_DATETIME").IsOptional();
            this.Property(ba => ba.Update_datetime).HasColumnName("UPDATE_DATETIME").IsOptional();
        

this.HasOptional(p => p.EQ_CHANNEL).WithMany(s => s.EQ_CHANNELForm_question).HasForeignKey(f => f.Eq_channel_id);

this.HasOptional(p => p.QUESTION_CATEGORY_PARAMITEMS).WithMany().HasForeignKey(f => f.Category_id);

this.HasOptional(p => p.QUESTION_DERIVED).WithMany(s => s.DERIVEDForm_question).HasForeignKey(f => f.Derived_id);

this.HasRequired(p => p.QUESTION_FORM).WithMany(s => s.FORMForm_question).HasForeignKey(f => f.Form_id);

this.HasOptional(p => p.QUESTION_OPTION_PARAM_GROUP).WithMany().HasForeignKey(f => f.Option_group_id);

this.HasOptional(p => p.QUESTION_PERIOD_CATEGORY_PARAMITEMS).WithMany().HasForeignKey(f => f.Period);

this.HasOptional(p => p.QUESTION_TYPE_CATEGORY_PARAMITEMS).WithMany().HasForeignKey(f => f.Type_id);
        }
        
        
	}

}


