﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssetHealth.Common.DTO;



namespace AssetHealth.Common.DAL.Mapping.EF
{
	
	/// <summary>
	/// An EF Entity Type Configuration component to manage form_question_option.
	/// </summary>
	public class FormQuestionOptionEntityTypeConfiguration   : EntityTypeConfiguration<FormQuestionOption>   
	{
         public FormQuestionOptionEntityTypeConfiguration()
        {
		//key and properties
            this.ToTable("form_question_option" , "");
            this.HasKey(ba => ba.Id);
            this.Property(ba => ba.Object_id).HasColumnName("OBJECT_ID");
            this.Property(ba => ba.Form_question_id).HasColumnName("FORM_QUESTION_Id");
            this.Property(ba => ba.Derived_id).HasColumnName("DERIVED_Id").IsOptional();
            this.Property(ba => ba.Code).HasColumnName("CODE");
            this.Property(ba => ba.Title).HasColumnName("TITLE");
            this.Property(ba => ba.Hint).HasColumnName("HINT").IsOptional();
            this.Property(ba => ba.Description).HasColumnName("DESCRIPTION").IsOptional();
            this.Property(ba => ba.Version).HasColumnName("VERSION").IsOptional();
            this.Property(ba => ba.Image_path).HasColumnName("IMAGE_PATH").IsOptional();
            this.Property(ba => ba.Device_image_path).HasColumnName("DEVICE_IMAGE_PATH").IsOptional();
            this.Property(ba => ba.Estimated_time).HasColumnName("ESTIMATED_TIME").IsOptional();
            this.Property(ba => ba.Video_path).HasColumnName("VIDEO_PATH").IsOptional();
            this.Property(ba => ba.Device_video_path).HasColumnName("DEVICE_VIDEO_PATH").IsOptional();
            this.Property(ba => ba.Method).HasColumnName("METHOD").IsOptional();
            this.Property(ba => ba.Display_order).HasColumnName("DISPLAY_ORDER").IsOptional();
            this.Property(ba => ba.Period).HasColumnName("PERIOD").IsOptional();
            this.Property(ba => ba.Active).HasColumnName("ACTIVE");
            this.Property(ba => ba.Deleted).HasColumnName("DELETED");
            this.Property(ba => ba.Insert_user_id).HasColumnName("INSERT_USER_ID").IsOptional();
            this.Property(ba => ba.Update_user_id).HasColumnName("UPDATE_USER_ID").IsOptional();
            this.Property(ba => ba.Insert_datetime).HasColumnName("INSERT_DATETIME").IsOptional();
            this.Property(ba => ba.Update_datetime).HasColumnName("UPDATE_DATETIME").IsOptional();
        

this.HasOptional(p => p.QUESTION_OPTION_DERIVED).WithMany(s => s.DERIVEDForm_question_option).HasForeignKey(f => f.Derived_id);

this.HasRequired(p => p.QUESTION_OPTION_QUESTION).WithMany(s => s.FORM_QUESTIONForm_question_option).HasForeignKey(f => f.Form_question_id);
        }
        
        
	}

}


