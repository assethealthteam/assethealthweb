﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ares.DAL.Repository.EF.Mapping;
using Ares.UMS.DTO.Users;
using AssetHealth.Common.DTO;



namespace AssetHealth.Common.DAL.Mapping.EF
{
	
	/// <summary>
	/// An EF Entity Type Configuration component to manage eq_threshold.
	/// </summary>
	public class EqThresholdEntityTypeConfiguration   : AresEntityTypeConfiguration<EqThreshold>   
	{
         public EqThresholdEntityTypeConfiguration()
        {
		//key and properties
            this.ToTable("eq_threshold" , "");
            this.HasKey(ba => ba.Id);
            this.Property(ba => ba.Object_id).HasColumnName("OBJECT_ID");
            this.Property(ba => ba.Eq_channel_id).HasColumnName("EQ_CHANNEL_ID");
            this.Property(ba => ba.Name).HasColumnName("NAME").IsOptional();
            this.Property(ba => ba.Description).HasColumnName("DESCRIPTION").IsOptional();
            this.Property(ba => ba.Lower_limit).HasColumnName("LOWER_LIMIT").IsOptional();
            this.Property(ba => ba.Upper_limit).HasColumnName("UPPER_LIMIT").IsOptional();
            this.Property(ba => ba.Warning_type).HasColumnName("WARNING_TYPE").IsOptional();
            this.Property(ba => ba.Strategy).HasColumnName("STRATEGY").IsOptional();
            this.Property(ba => ba.Level_increase_percentage).HasColumnName("LEVEL_INCREASE_PERCENTAGE").IsOptional();
            this.Property(ba => ba.Level_decrease_percentage).HasColumnName("LEVEL_DECREASE_PERCENTAGE").IsOptional();
            this.Property(ba => ba.Level_increase_repeat_times).HasColumnName("LEVEL_INCREASE_REPEAT_TIMES").IsOptional();
            this.Property(ba => ba.Level_decrease_repeat_times).HasColumnName("LEVEL_DECREASE_REPEAT_TIMES").IsOptional();
            this.Property(ba => ba.Active).HasColumnName("ACTIVE").IsOptional();
            this.Property(ba => ba.Insert_user_id).HasColumnName("INSERT_USER_ID").IsOptional();
            this.Property(ba => ba.Update_user_id).HasColumnName("UPDATE_USER_ID").IsOptional();
            this.Property(ba => ba.Insert_datetime).HasColumnName("INSERT_DATETIME").IsOptional();
            this.Property(ba => ba.Update_datetime).HasColumnName("UPDATE_DATETIME").IsOptional();
        

this.HasOptional(p => p.THRESHOLD_STRATEGY_PARAMITEMS).WithMany().HasForeignKey(f => f.Strategy);
this.HasOptional(p => p.THRESHOLD_WARNING_TYPE_PARAMITEMS).WithMany().HasForeignKey(f => f.Warning_type);

this.HasRequired(p => p.threshold_eq_channel).WithMany(s => s.EQ_CHANNELEq_threshold).HasForeignKey(f => f.Eq_channel_id);
        }
        
        
	}

}


