﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ares.DAL.Repository.EF.Mapping;
using Ares.UMS.DTO.Users;
using AssetHealth.Common.DTO;



namespace AssetHealth.Common.DAL.Mapping.EF
{
	
	/// <summary>
	/// An EF Entity Type Configuration component to manage aa_excel_import.
	/// </summary>
	public class AaExcelImportEntityTypeConfiguration   : AresEntityTypeConfiguration<AaExcelImport>   
	{
         public AaExcelImportEntityTypeConfiguration()
        {
		//key and properties
            this.ToTable("aa_excel_import" , "");
            this.HasKey(ba => ba.Id);
            this.Property(ba => ba.Location_name).HasColumnName("LOCATION_NAME").IsOptional();
            this.Property(ba => ba.Sublocation_name).HasColumnName("SUBLOCATION_NAME").IsOptional();
            this.Property(ba => ba.Equipment_name).HasColumnName("EQUIPMENT_NAME").IsOptional();
            this.Property(ba => ba.Object_id).HasColumnName("OBJECT_ID").IsOptional();
            this.Property(ba => ba.Period).HasColumnName("PERIOD").IsOptional();
            this.Property(ba => ba.Category).HasColumnName("CATEGORY").IsOptional();
            this.Property(ba => ba.Question_type).HasColumnName("QUESTION_TYPE").IsOptional();
            this.Property(ba => ba.Code).HasColumnName("CODE").IsOptional();
            this.Property(ba => ba.Title).HasColumnName("TITLE").IsOptional();
            this.Property(ba => ba.Hint).HasColumnName("HINT").IsOptional();
            this.Property(ba => ba.Display_order).HasColumnName("DISPLAY_ORDER").IsOptional();
            this.Property(ba => ba.Unhealthy_cause).HasColumnName("UNHEALTHY_CAUSE").IsOptional();
            this.Property(ba => ba.Threshold_repetition_count).HasColumnName("THRESHOLD_REPETITION_COUNT").IsOptional();
            this.Property(ba => ba.Answer_deadline_day_count).HasColumnName("ANSWER_DEADLINE_DAY_COUNT").IsOptional();
            this.Property(ba => ba.Answer_deadline_hour_count).HasColumnName("ANSWER_DEADLINE_HOUR_COUNT").IsOptional();
            this.Property(ba => ba.Criticality).HasColumnName("CRITICALITY").IsOptional();
            this.Property(ba => ba.Is_deleted).HasColumnName("IS_DELETED").IsOptional();
            this.Property(ba => ba.Warning_level).HasColumnName("WARNING_LEVEL").IsOptional();
            this.Property(ba => ba.Alarm_level).HasColumnName("ALARM_LEVEL").IsOptional();
            this.Property(ba => ba.Location_id).HasColumnName("Location_Id").IsOptional();
            this.Property(ba => ba.Equipment_id).HasColumnName("Equipment_Id").IsOptional();
            this.Property(ba => ba.Period_id).HasColumnName("Period_Id").IsOptional();
            this.Property(ba => ba.Category_id).HasColumnName("Category_Id").IsOptional();
            this.Property(ba => ba.Questiontype_id).HasColumnName("QuestionType_Id").IsOptional();
            this.Property(ba => ba.Form_id).HasColumnName("Form_Id").IsOptional();
            this.Property(ba => ba.Formquestion_id).HasColumnName("FormQuestion_Id").IsOptional();
            this.Property(ba => ba.Eq_channel_id).HasColumnName("Eq_Channel_Id").IsOptional();
        
        }
        
        
	}

}

