﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ares.DAL.Repository.EF.Mapping;
using Ares.UMS.DTO.Users;
using AssetHealth.Common.DTO;

namespace AssetHealth.Common.DAL.Mapping.EF
{
	
	/// <summary>
	/// An EF Entity Type Configuration component to manage form_answer_round.
	/// </summary>
	public class FormAnswerRoundEntityTypeConfiguration   : AresEntityTypeConfiguration<FormAnswerRound>   
	{
         public FormAnswerRoundEntityTypeConfiguration()
        {
		//key and properties
            this.ToTable("form_answer_round" , "");
            this.HasKey(ba => ba.Id);
            this.Property(ba => ba.Object_id).HasColumnName("OBJECT_ID");
            this.Property(ba => ba.Device_id).HasColumnName("DEVICE_ID").IsOptional();
            this.Property(ba => ba.Form_id).HasColumnName("FORM_ID").IsOptional();
            this.Property(ba => ba.Location_id).HasColumnName("LOCATION_ID").IsOptional();
            this.Property(ba => ba.Equipment_id).HasColumnName("EQUIPMENT_ID").IsOptional();
            this.Property(ba => ba.Role_id).HasColumnName("ROLE_ID").IsOptional();
            this.Property(ba => ba.Username).HasColumnName("USERNAME").IsOptional();
            this.Property(ba => ba.Read_no).HasColumnName("READ_NO").IsOptional();
            this.Property(ba => ba.Start_read_datetime).HasColumnName("START_READ_DATETIME").IsOptional();
            this.Property(ba => ba.End_read_datetime).HasColumnName("END_READ_DATETIME").IsOptional();
            this.Property(ba => ba.Last_read_datetime).HasColumnName("LAST_READ_DATETIME").IsOptional();
            this.Property(ba => ba.Warning_type).HasColumnName("WARNING_TYPE").IsOptional();
            this.Property(ba => ba.Period).HasColumnName("PERIOD").IsOptional();
            this.Property(ba => ba.Json).HasColumnName("JSON").IsOptional();
            this.Property(ba => ba.Start_count).HasColumnName("START_COUNT").IsOptional();
            this.Property(ba => ba.File_count).HasColumnName("FILE_COUNT").IsOptional();
            this.Property(ba => ba.Required_read_datetime).HasColumnName("REQUIRED_READ_DATETIME").IsOptional();
            this.Property(ba => ba.Description).HasColumnName("DESCRIPTION").IsOptional();
            this.Property(ba => ba.Notes).HasColumnName("NOTES").IsOptional();
            this.Property(ba => ba.Insert_user_id).HasColumnName("INSERT_USER_ID").IsOptional();
            this.Property(ba => ba.Update_user_id).HasColumnName("UPDATE_USER_ID").IsOptional();
            this.Property(ba => ba.Insert_datetime).HasColumnName("INSERT_DATETIME").IsOptional();
            this.Property(ba => ba.Update_datetime).HasColumnName("UPDATE_DATETIME").IsOptional();
        

//this.HasOptional(p => p.ANSWER_ROUND_DEVICE).WithMany(s => s.DEVICEForm_answer_round).HasForeignKey(f => f.Device_id);
this.HasOptional(p => p.ANSWER_ROUND_ROLE).WithMany().HasForeignKey(f => f.Role_id);

this.HasOptional(p => p.ANSWER_ROUND_FORM).WithMany(s => s.FORMForm_answer_round).HasForeignKey(f => f.Form_id);

this.HasOptional(p => p.ANSWER_ROUND_PERIOD_PARAMITEMS).WithMany().HasForeignKey(f => f.Period);

this.HasOptional(p => p.ANSWER_ROUND_WARNING_PARAMITEMS).WithMany().HasForeignKey(f => f.Warning_type);
        }
        
        
	}

}


