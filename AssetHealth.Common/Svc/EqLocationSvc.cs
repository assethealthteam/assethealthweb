﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ares.Svc;
using AssetHealth.Common.DAL.Repository;
using AssetHealth.Common.DTO;
using MySql.Data.MySqlClient;
using System.Data;
using AssetHealth.Common.DAL;
using Ares.Core.Caching;

namespace AssetHealth.Common.Svc
{
    public class EqLocationSvc : EntitySvc<EqLocation>, IEqLocationSvc
    {
        IEqLocationRepository _repository;
        ICacheManager _iCacheManager;

        private string ENTITY_BY_USERID_KEY = "Ares.Entity." + typeof(EqLocation).Name + ".userId-{0}";

        public EqLocationSvc(IEqLocationRepository repository, ICacheManager iCacheManager) : base(repository)
        {
            this._repository = repository;
            this._iCacheManager = iCacheManager;

        }


        public List<EqLocation> GetAuthorizedLocations(int userId)
        {
            if (userId == 0)
                return null;

            string key = string.Format(ENTITY_BY_USERID_KEY, userId);

            return _iCacheManager.Get(key, () => {
                MySqlParameter[] sqlParams = new MySqlParameter[]
                {
                    new MySqlParameter("user_id", userId) { Direction = ParameterDirection.Input },
                };

                var result = _repository.SqlQuery<EqLocation>(HardCodedSQL.EQ_LOCATION_GET_AUTHORIZED_ONES, sqlParams).ToList();

                return result;
            });


            

        }
    }
 
}
