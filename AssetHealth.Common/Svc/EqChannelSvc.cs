﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ares.Svc;
using AssetHealth.Common.DAL.Repository;
using AssetHealth.Common.DTO;
using MySql.Data.MySqlClient;
using System.Data;
using AssetHealth.Common.DAL;
using Ares.Core.Caching;

namespace AssetHealth.Common.Svc
{
    public class EqChannelSvc : EntitySvc<EqChannel>, IEqChannelSvc
    {
        IEqChannelRepository _repository;
        ICacheManager _iCacheManager;

        private string ENTITY_BY_USERID_KEY = "Ares.Entity." + typeof(EqChannel).Name + ".userId-{0}";

        public EqChannelSvc(IEqChannelRepository repository, ICacheManager iCacheManager) : base(repository)
        {
            this._repository = repository;
            this._iCacheManager = iCacheManager;

        }

        public List<EqChannel> GetAuthorizedChannels(int userId)
        {

            if (userId == 0)
                return null;

            string key = string.Format(ENTITY_BY_USERID_KEY, userId);

            return _iCacheManager.Get(key, () => {
                MySqlParameter[] sqlParams = new MySqlParameter[]
                {
                    new MySqlParameter("user_id", userId) { Direction = ParameterDirection.Input },
                };

                var result = _repository.SqlQuery<EqChannel>(HardCodedSQL.CHANNEL_GET_AUTHORIZED_ONES, sqlParams).ToList();

                return result;
            });



        }
    }
 
}
