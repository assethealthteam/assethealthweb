﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ares.Svc;
using AssetHealth.Common.DAL.Repository;
using AssetHealth.Common.DTO;
using AssetHealth.Common.Svc;

namespace AssetHealth.Common.Svc
{
    public class FormAnswerRoundSvc : EntitySvc<FormAnswerRound>, IFormAnswerRoundSvc
    {
        IFormAnswerRoundRepository _repository;

        public FormAnswerRoundSvc(IFormAnswerRoundRepository repository) : base(repository)
        {
            this._repository = repository;

        }
    }
 
}
