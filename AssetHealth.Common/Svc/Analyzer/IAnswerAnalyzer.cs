﻿using AssetHealth.Common.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetHealth.Common.Svc.Analyzer
{
    public interface IAnswerAnalyzer
    {
        AnswerAnalyzeResult checkIfAnswerOutOfThreshold(FormAnswer answer);
        void increaseWarningLevel(FormAnswer answer, AnswerAnalyzeResult result);
        void decreaseWarningLevel(FormAnswer answer, AnswerAnalyzeResult result);
        List<EqWarning> getPreviousWarnings(FormAnswer answer, int lastDaysCount, bool includePassiveOnes, int lastNRecords);

        void AnalyzeAnswer(FormAnswer answer);

        



    }
}

