﻿using Ares.Core.Helpers;
using Ares.UMS.DTO.Log;
using Ares.UMS.Svc.Log;
using AssetHealth.Common.DTO;
using AssetHealth.Common.DTO.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetHealth.Common.Svc.Analyzer
{
    public class AnswerAnalyzer : IAnswerAnalyzer
    {
        private readonly IEqWarningSvc _iEqWarningSvc;
        private readonly IEqChannelSvc _iEqChannelSvc;
        private readonly IFormAnswerSvc _iFormAnswerSvc;
        private readonly INotificationSvc _iNotificationSvc;



        public AnswerAnalyzer(IFormAnswerSvc iFormAnswerSvc, IEqChannelSvc iEqChannelSvc, IEqWarningSvc iEqWarningSvc, INotificationSvc iNotificationSvc)
        {
            this._iEqChannelSvc = iEqChannelSvc;
            this._iEqWarningSvc = iEqWarningSvc;
            this._iFormAnswerSvc = iFormAnswerSvc;
            this._iNotificationSvc = iNotificationSvc;
        }

        public AnswerAnalyzeResult checkIfAnswerOutOfThreshold(FormAnswer answer)
        {
            EqChannel channel = _iEqChannelSvc.GetAll().Where(c => c.EQ_CHANNELForm_question.ToList().Any(v => v.Id == answer.Form_question_id)).FirstOrDefault();

            int nAnswerValue = -1;
            int.TryParse(answer.Answer, out nAnswerValue);

            if (channel.EQ_CHANNELEq_threshold != null)
            {
                EqThreshold thresholdWarning = channel.EQ_CHANNELEq_threshold.Where(t => t.Warning_type == (int)EnumParameterItem.PARAM_ITEM_WARNING).FirstOrDefault(); //Warning
                EqThreshold thresholdAlarm = channel.EQ_CHANNELEq_threshold.Where(t => t.Warning_type == (int)EnumParameterItem.PARAM_ITEM_ALARM).FirstOrDefault(); //Alarm

                // decrease level yapılmaz, önce warning olmak zorunda, ardından alarma geçebilir ve nowarning'ten direkt olarak alarma geçiş yok
                if (channel.Sensor_type == (int)EnumParameterItem.PARAM_ITEM_GORSEL_KONTROL)
                {
                    if (thresholdWarning != null && thresholdAlarm == null) // warning seviyesinde
                    {
                        int warningRepeatTimes = thresholdWarning.Level_increase_repeat_times.GetValueOrDefault(3);
                        warningRepeatTimes = warningRepeatTimes > 0 ? warningRepeatTimes - 1 : warningRepeatTimes; // önceki kaydedilen warning adedi - 1 limit olacak.Çünkü şu anda da warning seviyesinde
                        //List<EqWarning> warnings = getPreviousWarnings(answer, 0, true, warningRepeatTimes);
                        List<EqWarning> warnings = getPreviousWarnings(answer, 0, false, warningRepeatTimes);


                        if (warnings != null && warnings.Any(w => w.Active.HasValue && w.Active.Value && w.Warning_type == (int)EnumParameterItem.PARAM_ITEM_ALARM) && thresholdWarning.Upper_limit == nAnswerValue)
                        {
                            // Warning -> Nochange  warning seviyesine geçiş (önceden aktif bir warning kaydı var ve mevcut durum da warning)
                            return new AnswerAnalyzeResult() { StateChange = StateChange.NoChange, ActiveWarning = warnings.FirstOrDefault(w => w.Active.HasValue && w.Active.Value), EqThreshold = thresholdWarning };
                        }
                        // Son 3 kaydı warning seviyesinde olan ve en son aktif bir warning seviyesinde kaydı olan warning kaydı seviyesi alarma yükseltilmelidir
                        else if (thresholdWarning.Upper_limit == nAnswerValue &&
                            warnings != null &&
                            //warnings.Count >= warningRepeatTimes &&
                            warnings.Count >= 1 &&
                            warnings[0].Repeat_count >= warningRepeatTimes &&
                            warnings.All(w => w.Warning_type == (int)EnumParameterItem.PARAM_ITEM_WARNING) &&
                            warnings[0].Active.GetValueOrDefault(false)
                            )
                        {

                            // Warning - > Alarma geçiş   
                            return new AnswerAnalyzeResult() { StateChange = StateChange.IncreaseLevel, ActiveWarning = warnings.FirstOrDefault(w => w.Active.HasValue && w.Active.Value), EqThreshold = thresholdWarning };
                        }
                        else if (warnings != null && warnings.Any(w => w.Active.HasValue && w.Active.Value) && thresholdWarning.Upper_limit != nAnswerValue)
                        {
                            // Normal duruma geçiş (mevcut aktif warning (tipi alarm veya warning farketmeksizin) disabled yapılmalı)
                            // Warning -> NoWarning veya Alarm -> NoWarning
                            return new AnswerAnalyzeResult() { StateChange = StateChange.NoWarning, ActiveWarning = warnings.FirstOrDefault(w => w.Active.HasValue && w.Active.Value), EqThreshold = thresholdWarning }; 
                        }
                        else if ((warnings == null || warnings.Count <= 0) && thresholdWarning.Upper_limit == nAnswerValue)
                        {
                            // NoWarning -> Warning  warning seviyesine geçiş (önceden aktif bir warning kaydı yok )
                            return new AnswerAnalyzeResult() { StateChange = StateChange.IncreaseLevel, ActiveWarning = null, EqThreshold = thresholdWarning };
                        }
                        else if (warnings != null && warnings.Any(w => w.Active.HasValue && w.Active.Value && w.Warning_type == (int)EnumParameterItem.PARAM_ITEM_WARNING) && thresholdWarning.Upper_limit == nAnswerValue)
                        {
                            // Warning -> Nochange  warning seviyesine geçiş (önceden aktif bir warning kaydı var ve mevcut durum da warning)
                            return new AnswerAnalyzeResult() { StateChange = StateChange.NoChange, ActiveWarning = warnings.FirstOrDefault(w => w.Active.HasValue && w.Active.Value), EqThreshold = thresholdWarning };
                        }

                        /*
                        else if (warnings != null && warnings.Any(w => w.Active.HasValue && !w.Active.Value) && thresholdWarning.Upper_limit == nAnswerValue)
                        {
                            // NoWarning -> Warning warning seviyesine geçiş (önceden pasif bir warning kaydı var ve mevcut durum da warning)
                            return new AnswerAnalyzeResult() { StateChange = StateChange.IncreaseLevel, ActiveWarning = null, EqThreshold = thresholdWarning };
                        }
                        */

                    }
                }
                else if (channel.Sensor_type == (int)EnumParameterItem.PARAM_ITEM_GORSEL_SICAKLIK || channel.Sensor_type == (int)EnumParameterItem.PARAM_ITEM_GORSEL_VIBRASYON 
                    || channel.Sensor_type == (int)EnumParameterItem.PARAM_ITEM_GORSEL_ASINMA_KALINLIGI || channel.Sensor_type == (int)EnumParameterItem.PARAM_ITEM_GORSEL_PINYON_BASLIGI_OLCUMU
                    || channel.Sensor_type == (int)EnumParameterItem.PARAM_ITEM_GORSEL_KAPLIN_LINE_AYARI || channel.Sensor_type == (int)EnumParameterItem.PARAM_ITEM_GORSEL_KALINLIK_OLCUMU
                    )
                {
                    double dAnswer = -1;
                    double.TryParse(answer.Answer.Replace(",", "."), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.NumberFormatInfo.InvariantInfo, out dAnswer);

                    bool isInWarningLevel = dAnswer >= thresholdWarning.Lower_limit  && dAnswer <= thresholdWarning.Upper_limit;
                    bool isInAlarmLevel = dAnswer >= thresholdAlarm.Lower_limit  && dAnswer <= thresholdAlarm.Upper_limit;

                    // Aktif warning var mı ?
                    List<EqWarning> warnings = getPreviousWarnings(answer, 0, false, thresholdWarning.Level_increase_repeat_times.GetValueOrDefault(1));
                    if (warnings != null && warnings.Count > 0)
                    {
                        if (isInAlarmLevel && warnings[0].Warning_type == (int)EnumParameterItem.PARAM_ITEM_WARNING) // Warning -> Alarm
                            return new AnswerAnalyzeResult() { StateChange = StateChange.IncreaseLevel, ActiveWarning = warnings[0], EqThreshold = thresholdAlarm};
                        else if (isInAlarmLevel && warnings[0].Warning_type == (int)EnumParameterItem.PARAM_ITEM_ALARM) 
                            return new AnswerAnalyzeResult() { StateChange = StateChange.NoChange, ActiveWarning = warnings[0] };
                        else if (isInWarningLevel && warnings[0].Warning_type == (int)EnumParameterItem.PARAM_ITEM_WARNING)
                            return new AnswerAnalyzeResult() { StateChange = StateChange.NoChange, ActiveWarning = warnings[0] };
                        else if (isInWarningLevel && warnings[0].Warning_type == (int)EnumParameterItem.PARAM_ITEM_ALARM) // Alarm -> Warning
                            return new AnswerAnalyzeResult() { StateChange = StateChange.DecreaseLevel, ActiveWarning = warnings[0], EqThreshold = thresholdWarning };
                        else if (!isInWarningLevel && !isInAlarmLevel /*&& warnings[0].Warning_type == PARAM_ITEM_WARNING*/) // Warning -> Normal veya Alarm -> Normal
                            return new AnswerAnalyzeResult() { StateChange = StateChange.NoWarning, ActiveWarning = warnings[0], EqThreshold = thresholdWarning };
                    }
                    else
                    {
                        if (!isInAlarmLevel && !isInWarningLevel)
                            return new AnswerAnalyzeResult() { StateChange = StateChange.NoWarning, ActiveWarning = null };
                        else if(isInWarningLevel)
                            return new AnswerAnalyzeResult() { StateChange = StateChange.IncreaseLevel, ActiveWarning = null, EqThreshold = thresholdWarning };
                        else if(isInAlarmLevel)
                            return new AnswerAnalyzeResult() { StateChange = StateChange.IncreaseToAlarm, ActiveWarning = null, EqThreshold = thresholdAlarm};


                    }




                }

                
            }

            return new AnswerAnalyzeResult() { StateChange = StateChange.NoChange, ActiveWarning = null };

        }

        private EqWarning createNewWarning(FormAnswer answer, EqThreshold threshold)
        {
            EqChannel channel = _iEqChannelSvc.GetAll().Where(c => c.EQ_CHANNELForm_question.ToList().Any(v => v.Id == answer.Form_question_id)).FirstOrDefault();

            EqWarning warning = new EqWarning()
            {
                 Active = true,
                 Object_id = Guid.NewGuid().ToString(),
                 Eq_channel_id = channel.Id,
                 Equipment_id = channel.Equipment_id,
                 Insert_datetime = StringHelper.ConvertDateTimeToString(DateTime.Now),
                 Read_no = answer.Read_no,
                 Lower_limit = threshold.Lower_limit,
                 Upper_limit = threshold.Upper_limit,
                 Repeat_count = 1,
                 Real_value = answer.Answer,
                 Strategy = threshold.Strategy,
                 Level_decrease_repeat_times = threshold.Level_decrease_repeat_times,
                 Level_increase_repeat_times = threshold.Level_increase_repeat_times
            };

            return warning;
        }

        public void increaseWarningLevel(FormAnswer answer, AnswerAnalyzeResult result)
        {
            addNotification(answer, result);

            if (result.StateChange == StateChange.IncreaseToAlarm)
            {

                EqWarning newWarning = createNewWarning(answer, result.EqThreshold);
                newWarning.Warning_type = (int)EnumParameterItem.PARAM_ITEM_ALARM;
                newWarning.Pre_warning_type = null;

                newWarning = _iEqWarningSvc.Insert(newWarning);

                FormAnswer tmpAnswer = _iFormAnswerSvc.GetById(answer.Id);
                tmpAnswer.Warning_type = (int)EnumParameterItem.PARAM_ITEM_ALARM;
                _iFormAnswerSvc.Update(tmpAnswer);

            }
            else if (result.ActiveWarning != null && result.ActiveWarning.Warning_type == (int)EnumParameterItem.PARAM_ITEM_WARNING)
            {
                EqWarning newWarning = createNewWarning(answer, result.EqThreshold);
                newWarning.Pre_warning_type = newWarning.Warning_type;
                newWarning.Warning_type = (int)EnumParameterItem.PARAM_ITEM_ALARM;

                result.ActiveWarning.Active = false;
                _iEqWarningSvc.Update(result.ActiveWarning);

                _iEqWarningSvc.Insert(newWarning);

                FormAnswer tmpAnswer = _iFormAnswerSvc.GetById(answer.Id);
                tmpAnswer.Warning_type = (int)EnumParameterItem.PARAM_ITEM_ALARM;
                _iFormAnswerSvc.Update(tmpAnswer);

            }
            else if (result.ActiveWarning == null)
            {
                EqWarning newWarning = createNewWarning(answer, result.EqThreshold);
                newWarning.Warning_type = (int)EnumParameterItem.PARAM_ITEM_WARNING;

                _iEqWarningSvc.Insert(newWarning);

                FormAnswer tmpAnswer = _iFormAnswerSvc.GetById(answer.Id);
                tmpAnswer.Warning_type = (int)EnumParameterItem.PARAM_ITEM_WARNING;
                _iFormAnswerSvc.Update(tmpAnswer);
                
            }

        }
        public void decreaseWarningLevel(FormAnswer answer, AnswerAnalyzeResult result)
        {
            addNotification(answer, result);

            if (result.ActiveWarning != null && result.ActiveWarning.Warning_type == (int)EnumParameterItem.PARAM_ITEM_WARNING)
            {
                result.ActiveWarning.Active = false;
                _iEqWarningSvc.Update(result.ActiveWarning);

                FormAnswer tmpAnswer = _iFormAnswerSvc.GetById(answer.Id);
                tmpAnswer.Warning_type = null;
                _iFormAnswerSvc.Update(tmpAnswer);
                
            }
            else if (result.ActiveWarning != null && result.StateChange == StateChange.NoWarning)
            {
                result.ActiveWarning.Active = false;
                _iEqWarningSvc.Update(result.ActiveWarning);

                FormAnswer tmpAnswer = _iFormAnswerSvc.GetById(answer.Id);
                tmpAnswer.Warning_type = null;
                _iFormAnswerSvc.Update(tmpAnswer);

            }
            else if (result.ActiveWarning != null && result.ActiveWarning.Warning_type == (int)EnumParameterItem.PARAM_ITEM_ALARM)
            {
                EqWarning newWarning = createNewWarning(answer, result.EqThreshold);
                newWarning.Pre_warning_type = newWarning.Warning_type;
                newWarning.Warning_type = (int)EnumParameterItem.PARAM_ITEM_WARNING;

                result.ActiveWarning.Active = false;
                _iEqWarningSvc.Update(result.ActiveWarning);

                _iEqWarningSvc.Insert(newWarning);

                FormAnswer tmpAnswer = _iFormAnswerSvc.GetById(answer.Id);
                tmpAnswer.Warning_type = (int)EnumParameterItem.PARAM_ITEM_WARNING;
                _iFormAnswerSvc.Update(tmpAnswer);

            }
        }

        public void noChangeUpdate(FormAnswer answer, AnswerAnalyzeResult result)
        {
            if (result.ActiveWarning != null)
            {
                result.ActiveWarning.Update_datetime = StringHelper.ConvertDateTimeToString(DateTime.Now);
                result.ActiveWarning.Repeat_count = result.ActiveWarning.Repeat_count + 1;
                _iEqWarningSvc.Update(result.ActiveWarning);

                FormAnswer tmpAnswer = _iFormAnswerSvc.GetById(answer.Id);
                tmpAnswer.Warning_type = result.ActiveWarning.Warning_type;
                tmpAnswer.Update_datetime = StringHelper.ConvertDateTimeToString(DateTime.Now);
                _iFormAnswerSvc.Update(tmpAnswer);
            }
            
        }

        public void addNotification(FormAnswer answer, AnswerAnalyzeResult result)
        {
            //int locID = answer.ANSWER_QUESTION.QUESTION_FORM.EQUIPMENT_ID.Location.Parent_id == null ? answer.ANSWER_QUESTION.QUESTION_FORM.EQUIPMENT_ID.Location.Id : answer.ANSWER_QUESTION.QUESTION_FORM.EQUIPMENT_ID.Location.Parent_id.GetValueOrDefault();
            int locID = answer.ANSWER_QUESTION.QUESTION_FORM.EQUIPMENT.Eq_location_id;
            Notification notification = new Notification()
            {
                Insert_datetime = StringHelper.ConvertDateTimeToString(DateTime.Now),
                //Start_datetime = answer.Actual_read_datetime == null ? answer.Read_no.ToString() : StringHelper.ConvertDateTimeToString(answer.Actual_read_datetime.GetValueOrDefault()),
                Start_datetime = StringHelper.ConvertDateTimeToLong(answer.Actual_read_datetime.GetValueOrDefault()),
                Description = answer.Answer,
                Notified_entity_name = "eqlocation",
                Notified_entity_id = locID,
                Notification_entity_id = answer.Id,
                Notification_entity_name = "formanswer"
            };

            string state = "";
            if (result.StateChange == StateChange.IncreaseToAlarm)
            {
                state = "direkt alarm seviyesine yükseldi";
                notification.Type_id = (int)EnumParameterItem.PARAM_ITEM_NOTIFICATION_CRITIC;
            }
            else if (result.StateChange == StateChange.IncreaseLevel && result.ActiveWarning != null && result.ActiveWarning.Warning_type == (int)EnumParameterItem.PARAM_ITEM_WARNING)
            {
                state = "uyarı seviyesinden alarm seviyesine yükseldi";
                notification.Type_id = (int)EnumParameterItem.PARAM_ITEM_NOTIFICATION_CRITIC;
            }
            else if (result.StateChange == StateChange.IncreaseLevel && result.ActiveWarning == null)
            {
                state = "uyarı seviyesine yükseldi";
                notification.Type_id = (int)EnumParameterItem.PARAM_ITEM_NOTIFICATION_WARNING;
            }
            else if ((result.StateChange == StateChange.DecreaseLevel || result.StateChange == StateChange.NoWarning) && result.ActiveWarning != null && result.ActiveWarning.Warning_type == (int)EnumParameterItem.PARAM_ITEM_WARNING)
            {
                state = "sağlıklı hale geçti";
                notification.Type_id = (int)EnumParameterItem.PARAM_ITEM_NOTIFICATION_SUCCESS;
            }
            else if ((result.StateChange == StateChange.DecreaseLevel || result.StateChange == StateChange.NoWarning) && result.ActiveWarning != null && result.StateChange == StateChange.NoWarning)
            {
                state = "sağlıklı hale geçti";
                notification.Type_id = (int)EnumParameterItem.PARAM_ITEM_NOTIFICATION_SUCCESS;
            }
            else if (result.StateChange == StateChange.DecreaseLevel && result.ActiveWarning != null && result.ActiveWarning.Warning_type == (int)EnumParameterItem.PARAM_ITEM_ALARM)
            {
                state = "alarm seviyesinden uyarı seviyesine düştü";
                notification.Type_id = (int)EnumParameterItem.PARAM_ITEM_NOTIFICATION_WARNING;
            }
            else
            {
                state = "";
            }

            notification.Title = "\"" + (!string.IsNullOrEmpty(answer.ANSWER_QUESTION.Code) ? answer.ANSWER_QUESTION.Code + " " : "" ) +  answer.ANSWER_QUESTION.Title + "\" durumu " + state;


            _iNotificationSvc.Insert(notification);
        }

        public List<EqWarning> getPreviousWarnings(FormAnswer answer, int lastDaysCount, bool includePassiveOnes, int lastNRecords)
        {
            EqChannel channel = _iEqChannelSvc.GetAll().Where(c => c.EQ_CHANNELForm_question.ToList().Any(v => v.Id == answer.Form_question_id)).FirstOrDefault();

            IQueryable<EqWarning> query = _iEqWarningSvc.GetAll().Where(w => w.Eq_channel_id == channel.Id);
            if (!includePassiveOnes)
                query = query.Where(w => w.Active.HasValue && w.Active.Value);

            
            if(lastDaysCount > 0)
            {
                DateTime startDate = DateTime.Today.AddDays(-1 * lastDaysCount);
                long lStartDate = StringHelper.ConvertDateTimeToLong(startDate);
                query = query.Where(w => w.Read_no >= lStartDate);
            }

            query = query.OrderByDescending(w => w.Id);
            if(lastNRecords > 0)
                query = query.Take(lastNRecords);

            List<EqWarning> warnings = query.ToList();

            return warnings;
        }


        public void AnalyzeAnswer(FormAnswer answer)
        {

            AnswerAnalyzeResult result = checkIfAnswerOutOfThreshold(answer);
            if (result.StateChange == StateChange.IncreaseLevel || result.StateChange == StateChange.IncreaseToAlarm)
                increaseWarningLevel(answer, result);
            else if (result.StateChange == StateChange.DecreaseLevel || (result.StateChange == StateChange.NoWarning && result.ActiveWarning != null))
                decreaseWarningLevel(answer, result);
            else if (result.StateChange == StateChange.NoChange && result.ActiveWarning != null)
                noChangeUpdate(answer, result);

        }

    }

    public class AnswerAnalyzeResult
    {
        public StateChange StateChange { get;set;}
        public EqWarning ActiveWarning { get; set; }
        public EqThreshold EqThreshold { get; set; }
    }

    public enum StateChange
    {
        NoChange = 0,
        IncreaseLevel,
        DecreaseLevel,
        IncreaseToAlarm,
        NoWarning
    }
}
