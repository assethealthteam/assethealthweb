﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ares.Svc;
using AssetHealth.Common.DAL.Repository;
using AssetHealth.Common.DTO;
using Ares.Core.Helpers;
using AssetHealth.Common.DTO.Enums;
using Ares.Core.Framework;
using Ares.UMS.Svc;
using AssetHealth.Common.Svc.JSONGenerator;

namespace AssetHealth.Common.Svc
{
    public class FormQuestionSvc : EntitySvc<FormQuestion>, IFormQuestionSvc
    {
        IFormQuestionRepository _repository;

        public FormQuestionSvc(IFormQuestionRepository repository) : base(repository)
        {
            this._repository = repository;

        }

        public void insertOrUpdateChannelAndThreshold(FormQuestion record)
        {
            IEqChannelSvc _iEqChannelsvc = FrameworkCtx.Current.Resolve<IEqChannelSvc>();
            IFormQuestionSvc _iFormQuestionsvc = FrameworkCtx.Current.Resolve<IFormQuestionSvc>();
            IWorkContext _workContext = FrameworkCtx.Current.Resolve<IWorkContext>();


            if (record.Eq_channel_id == null || record.Eq_channel_id <= 0)
            {
                EqChannel channel = new EqChannel();
                channel.Active = true;
                channel.Equipment_id = record.QUESTION_FORM.Equipment_id.GetValueOrDefault();
                channel.Name = record.Title;
                channel.Object_id = Guid.NewGuid().ToString();
                channel.Sensor_type = (int)EnumParameterItem.PARAM_ITEM_GORSEL_KONTROL;
                channel.Insert_datetime = StringHelper.ConvertDateTimeToString(DateTime.Now);
                channel.Insert_user_id = _workContext.CurrentUser.Id.ToString();



                if (!string.IsNullOrEmpty(record.Unhealthy_cause))
                {
                    EqThreshold threshold = new EqThreshold();
                    threshold.Active = true;
                    threshold.Object_id = Guid.NewGuid().ToString();
                    threshold.Level_increase_repeat_times = 3;
                    threshold.Level_decrease_repeat_times = 1;
                    threshold.Upper_limit = record.Unhealthy_cause.Trim().ToLowerInvariant() == "evet" ? (int)EnumParameterItem.PARAM_ITEM_OPTION_YES : (int)EnumParameterItem.PARAM_ITEM_OPTION_NO;
                    threshold.Warning_type = (int)EnumParameterItem.PARAM_ITEM_WARNING;

                    threshold.Insert_datetime = StringHelper.ConvertDateTimeToString(DateTime.Now);
                    threshold.Insert_user_id = _workContext.CurrentUser.Id.ToString();

                    channel.EQ_CHANNELEq_threshold = new List<EqThreshold>();
                    channel.EQ_CHANNELEq_threshold.Add(threshold);
                }
                channel = _iEqChannelsvc.Insert(channel);

                record.Eq_channel_id = channel.Id;
                _iFormQuestionsvc.Update(record);
            }
            else
            {
                EqChannel channel = _iEqChannelsvc.GetById(record.Eq_channel_id);
                bool isUpdate = false;

                if (channel.Name != record.Title)
                {
                    isUpdate = true;
                    channel.Name = record.Title;
                }
                    

                foreach (EqThreshold threshold in channel.EQ_CHANNELEq_threshold)
                {
                    if (channel.Sensor_type == (int)EnumParameterItem.PARAM_ITEM_GORSEL_KONTROL && !string.IsNullOrEmpty(record.Unhealthy_cause))
                    {
                        if (record.Unhealthy_cause.Trim().ToLowerInvariant() == "evet" && threshold.Upper_limit == (int)EnumParameterItem.PARAM_ITEM_OPTION_NO)
                        {
                            isUpdate = true;
                            threshold.Upper_limit = (int)EnumParameterItem.PARAM_ITEM_OPTION_YES;
                        }
                        else if (record.Unhealthy_cause.Trim().ToLowerInvariant() != "evet" && threshold.Upper_limit == (int)EnumParameterItem.PARAM_ITEM_OPTION_YES)
                        {
                            isUpdate = true;
                            threshold.Upper_limit = (int)EnumParameterItem.PARAM_ITEM_OPTION_NO;
                        }
                    }
                }

                if(isUpdate)
                    _iEqChannelsvc.Update(channel);


            }
            
        }



        public void fillEmptyJSONValues()
        {
            IFormSvc _iFormsvc = FrameworkCtx.Current.Resolve<IFormSvc>();
            IFormAnswerRoundSvc formAnswerRoundSvc = FrameworkCtx.Current.Resolve<IFormAnswerRoundSvc>();
            IFormAnswerSvc formAnswerSvc = FrameworkCtx.Current.Resolve<IFormAnswerSvc>();

            var items = _iFormsvc.GetAll().Where(f => f.Form_json == null || f.Form_json == "");

            JsonFormWizard jsonWizard = new JsonFormWizard();
            foreach (Form item in items.ToList())
            {
                item.Form_json = jsonWizard.GenerateJSONFromDTOForm(item);
                foreach (FormQuestion question in item.FORMForm_question.Where(q => q.Json == null || q.Json == ""))
                {
                    question.Json = jsonWizard.GenerateJSONFromDTOFormQuestion(question);
                }

                _iFormsvc.Update(item);
            }
        }


    }
 
}
