﻿using Ares.UMS.DTO.Configuration;
using AssetHealth.Common.DTO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetHealth.Common.Svc.JSONGenerator
{
    public class JsonFormWizard
    {
        public string generateForm()
        {
            JsonForm form = new JsonForm();
            JsonFormStep step1 = new JsonFormStep();
            step1.Title = "Step 1 of 3";
            step1.Next = "step2";

            JsonFormField field1 = new JsonFormField();
            field1.Type = JsonFieldType.edit_text;
            field1.Key = "name";
            field1.Hint = "Enter Your Name";
            field1.V_min_length = new JsonFormFieldValidator() { Value = "3", Err = "Min length should be at least 3" };
            field1.V_max_length = new JsonFormFieldValidator() { Value = "10", Err = "Max length can be at most 10." };

            step1.Fields.Add(field1);

            JsonFormField field2 = new JsonFormField();
            field2.Type = JsonFieldType.edit_text;
            field2.Key = "email";
            field2.Hint = "Enter Your Email";
            field2.V_email = new JsonFormFieldValidator() { Value = "true", Err = "Not an email." };

            step1.Fields.Add(field2);

            JsonFormField field3 = new JsonFormField();
            field3.Type = JsonFieldType.label;
            field3.Key = "labelBackgroundImage";
            field3.Text = "Choose Background Image";

            step1.Fields.Add(field3);

            form.Steps.Add("step1", step1);

            return convertToJSON(form);
        }


        public string GenerateJSONFromDTOForm(Form form)
        {
            JsonForm jsonForm = new JsonForm();
            JsonFormStep step1 = new JsonFormStep();

            //step1.Title = "Step 1 of 3";
            step1.Title = form.Title;
            //step1.Next = "step2";

            foreach (FormQuestion question in form.FORMForm_question.OrderBy(q => q.Display_order))
            {
                step1.Fields.Add(GetJsonFormFieldFromQuestion(question));
            }


            jsonForm.Steps.Add("step1", step1);

            return convertToJSON(jsonForm);
        }

        public string GenerateJSONFromDTOFormQuestion(FormQuestion formQuestion)
        {
            return convertToJSON(GetJsonFormFieldFromQuestion(formQuestion));
        }

        public JsonFormField GetJsonFormFieldFromQuestion(FormQuestion question)
        {
            JsonFormField field1 = new JsonFormField();
            //field1.Key = question.Id.ToString();
            field1.Key = question.Object_id;
            field1.Hint = (question.Code != null ? question.Code + " " : "") + question.Hint;
            field1.Label = question.Title;
            field1.Type = GetJsonFieldType(question, field1);
            if (field1.Type == JsonFieldType.spinner)
            {
                field1.Options = new List<JsonFormFieldOptions>();
                /*
                if(question.FORM_QUESTIONForm_question_option != null)
                {
                    foreach (FormQuestionOption option in question.FORM_QUESTIONForm_question_option)
                    {
                        field1.Options.Add(new JsonFormFieldOptions() { Key = option.Id.ToString(), Text = option.Title });
                    }
                }
                */
                if (question.QUESTION_OPTION_PARAM_GROUP != null)
                {
                    foreach (ParameterItem option in question.QUESTION_OPTION_PARAM_GROUP.PARAMETERGROUPParameter_item)
                    {
                        field1.Options.Add(new JsonFormFieldOptions() { Key = option.Id.ToString(), Text = option.Title });
                    }
                }

            }

            if (string.IsNullOrEmpty(question.Hint))
            {
                field1.Hint = (question.Code != null ? question.Code + " " : "") + question.Title;
            }

            if (!string.IsNullOrEmpty(question.V_email))
            {
                field1.V_email = new JsonFormFieldValidator() { Value = "true", Err = question.V_email };
            }
            if (question.V_max_length.HasValue && question.V_max_length > 0)
            {
                field1.V_min_length = new JsonFormFieldValidator() { Value = question.V_min_length.Value.ToString(), Err = "Min length should be at least 3" };
                field1.V_max_length = new JsonFormFieldValidator() { Value = question.V_max_length.Value.ToString(), Err = "Max length can be at most 10." };
            }
            if (!string.IsNullOrEmpty(question.V_regex))
            {
                field1.V_regex = new JsonFormFieldValidator() { Value = question.V_regex, Err = "Not valid regex" };
            }

            if (question.V_required.HasValue && question.V_required.Value)
            {
                field1.V_required = new JsonFormFieldValidator() { Value = question.V_required.Value.ToString(), Err = "Required Field" };
            }

            return field1;
        }

        public string convertToJSON(object obj)
        {
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            };

            settings.ContractResolver = new LowercaseContractResolver();

            string result = Newtonsoft.Json.JsonConvert.SerializeObject(obj, Newtonsoft.Json.Formatting.Indented, settings);

            return result;
        }

        public JsonFieldType GetJsonFieldType(FormQuestion question, JsonFormField field1)
        {
            switch (question.Type_id)
            {
                case 8: return JsonFieldType.edit_text;
                case 9: field1.Label = ""; field1.Options = new List<JsonFormFieldOptions>(); field1.Options.Add(new JsonFormFieldOptions() { Key = question.Id.ToString(), Text = question.Title }); return JsonFieldType.check_box;
                case 10: return JsonFieldType.radio;
                case 11: return JsonFieldType.label;
                case 12: return JsonFieldType.choose_image;
                case 13: return JsonFieldType.spinner;
            }
            return JsonFieldType.edit_text;
        }

    }

    public class LowercaseContractResolver : DefaultContractResolver
    {
        protected override string ResolvePropertyName(string propertyName)
        {
            return propertyName.ToLower();
        }
    }

    [JsonConverter(typeof(JsonFormConverter))]
    public class JsonForm
    {
        public JsonForm()
        {
            //Steps = new List<JsonFormStep>();
            Steps = new Dictionary<string, JsonFormStep>();
        }

        public string Key { get; set; }
        public int Count { get { return Steps.Count; } }
        //public List<JsonFormStep> Steps { get; set; }
        public Dictionary<string, JsonFormStep> Steps { get; set; }
    }

    /// <summary>
    /// This converter supplies us to serialize "steps" property without property name
    /// </summary>
    public class JsonFormConverter : JsonConverter
    {
        public override object ReadJson(JsonReader jsonReader, Type type, object obj, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override void WriteJson(JsonWriter jsonWriter, object obj, JsonSerializer serializer)
        {
            jsonWriter.WriteStartObject();

            // Write properties.
            var propertyInfos = obj.GetType().GetProperties();
            foreach (var propertyInfo in propertyInfos)
            {
                // Skip the Steps property.
                var propertyValue = propertyInfo.GetValue(obj);
                if (propertyInfo.Name == "Steps" || propertyValue == null || propertyValue.ToString() == "null")
                    continue;

                jsonWriter.WritePropertyName(propertyInfo.Name.ToLowerInvariant());
                serializer.Serialize(jsonWriter, propertyValue);
            }

            // Write dictionary key-value pairs.
            var test = (JsonForm)obj;
            foreach (var kvp in test.Steps)
            {
                jsonWriter.WritePropertyName(kvp.Key);
                serializer.Serialize(jsonWriter, kvp.Value);
            }
            jsonWriter.WriteEndObject();

            /*
            var response = (JsonForm)obj;
            foreach (var kvp in response.Steps)
            {
                jsonWriter.WritePropertyName(kvp.Key);
                serializer.Serialize(jsonWriter, kvp.Value);
                //jsonWriter.WriteValue(kvp.Value);
            }
            */
        }
        public override bool CanConvert(Type t)
        {
            return t == typeof(JsonForm);
        }
    }




    public class JsonFormStep
    {
        public JsonFormStep()
        {
            Fields = new List<JsonFormField>();
        }

        public string Key { get; set; }
        public string Title { get; set; }
        public string Next { get; set; }
        public List<JsonFormField> Fields { get; set; }
    }



    public class JsonFormField
    {
        public string Key { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public JsonFieldType Type { get; set; }
        public string Label { get; set; }
        public string Hint { get; set; }
        public JsonFormFieldValidator V_required { get; set; }
        public JsonFormFieldValidator V_email { get; set; }
        public JsonFormFieldValidator V_min_length { get; set; }
        public JsonFormFieldValidator V_max_length { get; set; }
        public JsonFormFieldValidator V_regex { get; set; }
        public string Text { get; set; }
        public string UploadButtonText { get; set; }
        public string Values { get; set; }
        public List<JsonFormFieldOptions> Options { get; set; }
        public string Value { get; set; }


    }

    public class JsonFormFieldOptions
    {
        public string Key { get; set; }
        public string Text { get; set; }
        public string Value { get; set; }
    }

    public enum JsonFieldType
    {
        edit_text,
        check_box,
        radio,
        label,
        choose_image,
        spinner

    }


    public class JsonFormFieldValidator
    {
        public string Value { get; set; }
        public string Err { get; set; }
    }
}
