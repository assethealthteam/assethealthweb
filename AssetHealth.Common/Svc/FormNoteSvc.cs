﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ares.Svc;
using AssetHealth.Common.DAL.Repository;
using AssetHealth.Common.DTO;

namespace AssetHealth.Common.Svc
{
    public class FormNoteSvc : EntitySvc<FormNote>, IFormNoteSvc
    {
        IFormNoteRepository _repository;

        public FormNoteSvc(IFormNoteRepository repository) : base(repository)
        {
            this._repository = repository;

        }
    }
 
}
