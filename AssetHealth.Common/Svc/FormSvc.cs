﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ares.Svc;
using AssetHealth.Common.DAL.Repository;
using AssetHealth.Common.DTO;

namespace AssetHealth.Common.Svc
{
    public class FormSvc : EntitySvc<Form>, IFormSvc
    {
        IFormRepository _repository;

        public FormSvc(IFormRepository repository) : base(repository)
        {
            this._repository = repository;

        }
    }
 
}
