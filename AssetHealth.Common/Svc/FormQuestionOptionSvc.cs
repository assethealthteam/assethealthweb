﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ares.Svc;
using AssetHealth.Common.DAL.Repository;
using AssetHealth.Common.DTO;

namespace AssetHealth.Common.Svc
{
    public class FormQuestionOptionSvc : EntitySvc<FormQuestionOption>, IFormQuestionOptionSvc
    {
        IFormQuestionOptionRepository _repository;

        public FormQuestionOptionSvc(IFormQuestionOptionRepository repository) : base(repository)
        {
            this._repository = repository;

        }
    }
 
}
