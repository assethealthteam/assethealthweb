﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ares.Svc;
using AssetHealth.Common.DAL.Repository;
using AssetHealth.Common.DTO;
using MySql.Data.MySqlClient;
using System.Data;
using AssetHealth.Common.DAL;
using Ares.Core.Caching;

namespace AssetHealth.Common.Svc
{
    public class EquipmentSvc : EntitySvc<Equipment>, IEquipmentSvc
    {
        IEquipmentRepository _repository;
        ICacheManager _iCacheManager;

        private string ENTITY_BY_USERID_KEY = "Ares.Entity." + typeof(Equipment).Name + ".userId-{0}";

        public EquipmentSvc(IEquipmentRepository repository, ICacheManager iCacheManager) : base(repository)
        {
            this._repository = repository;
            this._iCacheManager = iCacheManager;

        }

        public List<Equipment> GetAuthorizedEquipments(int userId)
        {

            if (userId == 0)
                return null;

            string key = string.Format(ENTITY_BY_USERID_KEY, userId);

            return _iCacheManager.Get(key, () => {
                MySqlParameter[] sqlParams = new MySqlParameter[]
                {
                    new MySqlParameter("user_id", userId) { Direction = ParameterDirection.Input },
                };

                var result = _repository.SqlQuery<Equipment>(HardCodedSQL.EQUIPMENT_GET_AUTHORIZED_ONES, sqlParams).ToList();

                return result;
            });





        }

    }
 
}
