﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ares.Svc;
using AssetHealth.Common.DTO;

namespace AssetHealth.Common.Svc
{
    public interface IFormQuestionSvc : IEntitySvc<FormQuestion>
    {
        void insertOrUpdateChannelAndThreshold(FormQuestion record);
        void fillEmptyJSONValues();
    }
 
}