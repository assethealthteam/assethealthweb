﻿using Ares.Core.Framework;
using Ares.UMS.DTO.Users;
using AssetHealth.Common.DAL;
using AssetHealth.Common.DAL.Repository;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetHealth.Common.DTO.Extensions
{
    public static class UserExtensions
    {
        public static List<EqLocation> Location(this User user)
        {
            if (user == null || user.Id < 0)
                return null;

            IEqLocationRepository lr = FrameworkCtx.Current.Resolve<IEqLocationRepository>();

            var relatedLocation = lr.Table.Where(s => s.User_From_EqLocationUserMapping.Any(u => u.Id == user.Id)).ToList();

            return relatedLocation;
        }

        public static IQueryable<EqLocation> RelatedLocations(this User user)
        {
            if (user == null || user.Id < 0)
                return null;

            IEqLocationRepository lr = FrameworkCtx.Current.Resolve<IEqLocationRepository>();

            MySqlParameter[] sqlParams = new MySqlParameter[]
            {
              new MySqlParameter("user_id", user.Id) { Direction = ParameterDirection.Input },
            };

            var result = lr.SqlQuery<EqLocation>(HardCodedSQL.USER_EXT_GET_USER_RELATED_LOCS, sqlParams);

            return result;
        }

        public static bool IsAuthorizedInLocation(this User user, int locId)
        {
            if (user == null || user.Id < 0)
                return false;

            IEqLocationRepository lr = FrameworkCtx.Current.Resolve<IEqLocationRepository>();

            MySqlParameter[] sqlParams = new MySqlParameter[]
            {
              new MySqlParameter("location_id", locId) { Direction = ParameterDirection.Input },
              new MySqlParameter("user_id", user.Id) { Direction = ParameterDirection.Input },
              new MySqlParameter("isAuthorized", MySqlDbType.Int32) { Direction = ParameterDirection.Output },
            };

            var isAuthorized = lr.SqlQuery<int>(HardCodedSQL.USER_EXT_USER_AUTHORIZED, sqlParams).ToList();
            if (isAuthorized != null && isAuthorized.Count > 0)
                return isAuthorized[0] == 1;

            return false;
        }


    }
}
