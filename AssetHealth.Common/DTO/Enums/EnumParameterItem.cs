﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetHealth.Common.DTO.Enums
{
    public enum EnumParameterItem
    {
        PARAM_ITEM_UPLOAD_MANAGER_SERVER_FILE = 1,
        PARAM_ITEM_UPLOAD_MANAGER_SERVER_DATABASE = 2,
        PARAM_ITEM_UPLOAD_MANAGER_AMAZON = 105,

        PARAM_ITEM_FILE_TYPE_VIDEO = 6,
        PARAM_ITEM_FILE_TYPE_JPG = 3,
        PARAM_ITEM_FILE_TYPE_PNG = 4,

        
        PARAM_ITEM_GORSEL_SICAKLIK = 70,
        PARAM_ITEM_GORSEL_VIBRASYON = 72,
        PARAM_ITEM_GORSEL_KONTROL = 81,
        PARAM_ITEM_GORSEL_ASINMA_KALINLIGI = 77,
        PARAM_ITEM_GORSEL_PINYON_BASLIGI_OLCUMU = 78,
        PARAM_ITEM_GORSEL_KAPLIN_LINE_AYARI = 79,
        PARAM_ITEM_GORSEL_KALINLIK_OLCUMU = 80,
        PARAM_ITEM_WARNING = 61,
        PARAM_ITEM_ALARM = 62,

        PARAM_ITEM_OPTION_YES = 63,
        PARAM_ITEM_OPTION_NO = 64,
        PARAM_ITEM_OPTION_CORRECTED = 65,

        PARAM_ITEM_NOTIFICATION_SUCCESS = 101,
        PARAM_ITEM_NOTIFICATION_WARNING = 102,
        PARAM_ITEM_NOTIFICATION_CRITIC = 103,
        
    }

    public enum EnumParameterGroup
    {
        GROUP_YES_NO_CORRECTED = 12
    }
}
