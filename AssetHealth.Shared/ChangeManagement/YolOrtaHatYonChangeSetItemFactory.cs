﻿using OdakGIS.Framework.Core.Entities;
using OdakGIS.Numarataj.Data;
using OdakGIS.Numarataj.Data.Entities;
using OdakGIS.Sync.ChangeManagement;
using OdakGIS.Sync.Entities.Custom;
using OdakGIS.Sync.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace OdakGIS.Numarataj.Shared.ChangeManagement
{
    public class YolOrtaHatYonChangeSetItemFactory : ChangeSetItemFactory<YOLORTAHATYON, YOLORTAHATYON>
    {
        public YolOrtaHatYonChangeSetItemFactory(int itemId, int cihazId, SenkronizasyonOperasyonTipi itemState)
            : base(itemId, cihazId, itemState)
        {

        }
        protected override OperationResult Validate(YOLORTAHATYON item)
        {
            return new OperationResult() { IsSuccess = true };
        }

        protected override YOLORTAHATYON PrepareForSynchronization(int itemId)
        {
            YOLORTAHATYON yon = null;
            using (var ctx = new NumaratajContext())
            using (var tr = ctx.Database.BeginTransaction(IsolationLevel.ReadUncommitted))
            {
                yon = ctx.YOLORTAHATYON.FirstOrDefault(t => t.OBJECTID == itemId);
                yon.ID = yon.OBJECTID;
                //using (var sCtx = new SyncEntities())
                //    yon.MAHALLEID = GetClientId(sCtx, "YOLORTAHATYON", itemId);
                tr.Commit();
            }
            return yon;
        }

        public override int SaveChangeSetItem(ChangeSetItem item)
        {//sadece sunucu
            throw new NotImplementedException();
        }

        public override void SaveMedia(int medyaId)
        {
            throw new NotImplementedException();
        }

        public override List<int> GetNesneMedya()
        {
            throw new NotImplementedException();
        }

        public override void NotKaydet(NotData not)
        {
            throw new NotImplementedException();
        }

        public override string GetCentroid(ChangeSetItem item)
        {
            return null;
        }
    }
}