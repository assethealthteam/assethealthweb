﻿using OdakGIS.Numarataj.Shared.UavtChangeManagement;
using OdakGIS.Sync.ChangeManagement;
using OdakGIS.Sync.ChangeManagement.Interfaces;
using OdakGIS.Sync.Enums;
using System;

namespace OdakGIS.Numarataj.Shared.ChangeManagement
{
    public class NumaratajChangeSetItemFactoryService : BaseChangeSetItemFactoryStrategy
    {
        public override IChangeSetItemSerializerFactory GetSerializerForChangeSetItemType(string alias, int itemId, int cihazId, SenkronizasyonOperasyonTipi itemState)
        {
            IChangeSetItemSerializerFactory factory = null;
            switch (alias)
            {
                case "MAHALLE":
                    factory = new MahalleChangeSetItemFactory(itemId, cihazId, itemState);
                    break;
                case "YAPI":
                    factory = new BinaChangeSetItemFactory(itemId, cihazId, itemState);
                    break;
                case "BAGIMSIZBOLUM":
                    factory = new BagimsizBolumChangeSetItemFactory(itemId, cihazId, itemState);
                    break;
                case "YOLORTAHAT":
                    factory = new YolOrtaHatChangeSetItemFactory(itemId, cihazId, itemState);
                    break;
                case "YOLORTAHATYON":
                    factory = new YolOrtaHatYonChangeSetItemFactory(itemId, cihazId, itemState);
                    break;
                case "YOL":
                    factory = new CsbmChangeSetItemFactory(itemId, cihazId, itemState);
                    break;
                case "NUMARATAJ":
                    factory = new NumaratajChangeSetItemFactory(itemId, cihazId, itemState);
                    break;
                case "PARSEL":
                    factory = new ParselChangeSetItemFactory(itemId, cihazId, itemState);
                    break;
                case "YOLTABELA":
                    factory = new YolTabelaChangeSetItemFactory(itemId, cihazId, itemState);
                    break;

                //uavt
                case "UAVT_IL":
                    factory = new UavtIlChangeSetItemFactory(itemId, cihazId, itemState);
                    break;
                case "UAVT_ILCE":
                    factory = new UavtIlceChangeSetItemFactory(itemId, cihazId, itemState);
                    break;
                case "UAVT_MAHALLE":
                    factory = new UavtMahalleChangeSetItemFactory(itemId, cihazId, itemState);
                    break;
                case "UAVT_CSBM":
                    factory = new UavtCsbmChangeSetItemFactory(itemId, cihazId, itemState);
                    break;
                case "UAVT_BINA":
                    factory = new UavtBinaChangeSetItemFactory(itemId, cihazId, itemState);
                    break;
                case "UAVT_BAGIMSIZ_BOLUM":
                    factory = new UavtBagimsizBolumChangeSetItemFactory(itemId, cihazId, itemState);
                    break;
                default:
                    factory = base.GetSerializerForChangeSetItemType(alias, itemId, cihazId, itemState);
                    break;
            }
            if (factory == null)
                throw new InvalidOperationException("tanimlanmamis tablo aliasi");
            return factory;
        }
    }
}