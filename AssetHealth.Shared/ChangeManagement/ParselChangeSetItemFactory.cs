﻿using OdakGIS.Framework.Core.Entities;
using OdakGIS.Numarataj.Data;
using OdakGIS.Numarataj.Data.Entities;
using OdakGIS.Sync.ChangeManagement;
using OdakGIS.Sync.Entities.Custom;
using OdakGIS.Sync.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace OdakGIS.Numarataj.Shared.ChangeManagement
{
    public class ParselChangeSetItemFactory : ChangeSetItemFactory<PARSEL, PARSEL>
    {
        public ParselChangeSetItemFactory(int id, int cihazId, SenkronizasyonOperasyonTipi itemState)
            : base(id, cihazId, itemState)
        {

        }
        protected override OperationResult Validate(PARSEL item)
        {
            return new OperationResult()
            {
                IsSuccess = true
            };
        }

        protected override PARSEL PrepareForSynchronization(int itemId)
        {
            PARSEL parsel = null;
            using (var ctx = new NumaratajContext())
            using (var tr = ctx.Database.BeginTransaction(IsolationLevel.ReadUncommitted))
            {
                parsel = ctx.PARSEL.FirstOrDefault(t => t.ID == itemId);
                parsel.Geometry = parsel.SHAPE.AsText();
                tr.Commit();
            }
            return parsel;
        }

        public override int SaveChangeSetItem(ChangeSetItem item)
        {
            throw new NotImplementedException();
        }

        public override void SaveMedia(int medyaId)
        {
            throw new NotImplementedException();
        }

        public override List<int> GetNesneMedya()
        {
            throw new NotImplementedException();
        }

        public override void NotKaydet(NotData not)
        {
            throw new NotImplementedException();
        }

        public override string GetCentroid(ChangeSetItem item)
        {
            return null;
        }
    }
}