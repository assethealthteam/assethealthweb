﻿using Microsoft.SqlServer.Types;
using Newtonsoft.Json;
using OdakGIS.Framework.Core.Entities;
using OdakGIS.Framework.Core.Managers;
using OdakGIS.Numarataj.Data;
using OdakGIS.Numarataj.Data.Entities;
using OdakGIS.Numarataj.Shared.Entities;
using OdakGIS.Numarataj.Shared.Interfaces;
using OdakGIS.Sync.ChangeManagement;
using OdakGIS.Sync.Entities.Custom;
using OdakGIS.Sync.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Spatial;
using System.Data.SqlTypes;
using System.Globalization;
using System.Linq;

namespace OdakGIS.Numarataj.Shared.ChangeManagement
{
    public class CsbmChangeSetItemFactory : ChangeSetItemFactory<YOL, YOL>
    {
        public CsbmChangeSetItemFactory(int id, int cihazId, SenkronizasyonOperasyonTipi itemState)
            : base(id, cihazId, itemState)
        {

        }

        protected override OperationResult Validate(YOL item)
        {
            OperationResult result = new OperationResult() { IsSuccess = true };
            return result;
        }

        protected override YOL PrepareForSynchronization(int itemId)
        {
            YOL result = null;
            using (var ctx = new NumaratajContext())
            using (var tr = ctx.Database.BeginTransaction(IsolationLevel.ReadUncommitted))
            {
                result = ctx.YOL.FirstOrDefault(t => t.OBJECTID == itemId);
                result.ID = result.OBJECTID;
                result.Geometry = result.SHAPE.AsText();
                tr.Commit();
            }
            return result;
        }

        public override int SaveChangeSetItem(ChangeSetItem item)
        {
            OperationResult<int> result = null;
            YOL gelenYol = JsonConvert.DeserializeObject<YOL>(item.Item);
            gelenYol.OBJECTID = _ItemId;
            ICsbmService service = ServiceManager.GetService<ICsbmService>();

            using (var ctx = new NumaratajContext())
            {
                if (_ItemId != 0)
                {
                    if (ctx.YOL.FirstOrDefault(t => t.OBJECTID == _ItemId) == null)
                    {
                        return _ItemId;
                    }
                }
                if (_ItemState == SenkronizasyonOperasyonTipi.Yeni)
                {
                    CsbmData data = new CsbmData()
                    {
                        Id = 0,
                        Adi = gelenYol.AD,
                        UavtNo = gelenYol.UAVTNO,
                        UavtEslesmiyor = gelenYol.UAVTESLESMIYOR,
                        Genislik = gelenYol.GENISLIK,
                        Uzunluk = gelenYol.UZUNLUK,
                        CsbmWkt = gelenYol.Geometry,
                        Tip = gelenYol.TIP == 0 ? null : new int?(gelenYol.TIP.Value),
                        Aciklama = gelenYol.ACIKLAMA,
                        AydinlatmaVarMi = gelenYol.AYDINLATMAVARMI,
                        BaglantiNiteligi = gelenYol.BAGLANTINITELIGI,
                        CaddeSokakLevhaVarMi = gelenYol.CADDESOKAKLEVHAVARMI,
                        Cinsi = gelenYol.CINSI,
                        Durum = gelenYol.DURUM,
                        Egimi = gelenYol.EGIMI,
                        HizLimiti = gelenYol.HIZLIMITI,
                        Islevi = gelenYol.ISLEVI,
                        KaldirimVarMi = gelenYol.KALDIRIMVARMI,
                        KaplamaCinsi = gelenYol.KAPLAMACINSI,
                        KimlikNo = gelenYol.KIMLIKNO,
                        MeclisKararNo = gelenYol.MECLISKARARNO,
                        MeclisKararTarih = gelenYol.MECLISKARARTARIH,
                        ParklanmaVarMi = gelenYol.PARKLANMAVARMI,
                        RefujVarMi = gelenYol.REFUJVARMI,
                        SeritCizgisiVarMi = gelenYol.SERITCIZGISIVARMI,
                        SeritSayisi = gelenYol.SERITSAYISI,
                        Sinyalizasyon = gelenYol.SINYALIZASYON,
                        TabelaAdi = gelenYol.TABELAADI,
                        YolcuDuragiVarMi = gelenYol.YOLCUDURAGIVARMI,
                        YolunDevamliligi = gelenYol.YOLUNDEVAMLILIGI,
                        YonDurumu = gelenYol.YONDURUMU,
                        ZeminNiteligi = gelenYol.ZEMINNITELIGI,
                        CiftBaslangicNo = gelenYol.CIFTBASLANGICNO,
                        CiftBitisNo = gelenYol.CIFTBITISNO,
                        TekBaslangicNo = gelenYol.TEKBASLANGICNO,
                        TekBitisNo = gelenYol.TEKBITISNO,
                        DegistirmeTarihi = gelenYol.DEGISTIRMETARIHISTRING,
                        OlusturmaTarihi = gelenYol.OLUSTURMATARIHISTRING
                    };
                    result = service.CsbmKaydetUi(data, true);
                    if (!result.IsSuccess)
                        throw new Exception(result.OperationMessage);
                    return result.Data;
                }
                else if (item.ItemState == SenkronizasyonOperasyonTipi.Silinmis)
                {
                    var yol = ctx.YOL.FirstOrDefault(t => t.OBJECTID == _ItemId);
                    yol.AKTIF = false;
                    ctx.SaveChanges();
                    return _ItemId;
                }
                else
                {
                    // NOT SALAR: Tablleten eğerki yolun geometry si değiştirilmişse onu eski haline çek çunku tablet yolun geometrisini değiştirmemesi lazım dediler(Kuntay bey ve Burcu Çelikbaş) 
                    YOL eskiYol = ctx.YOL
                                        .Where(l => l.OBJECTID == _ItemId)
                                        .FirstOrDefault();
                    gelenYol.Geometry = eskiYol.SHAPE.AsText();

                    CsbmData data = new CsbmData()
                    {
                        Id = _ItemId,
                        Adi = gelenYol.AD,
                        UavtNo = gelenYol.UAVTNO,
                        UavtEslesmiyor = gelenYol.UAVTESLESMIYOR,
                        Genislik = gelenYol.GENISLIK,
                        Uzunluk = gelenYol.UZUNLUK,
                        CsbmWkt = gelenYol.Geometry,
                        Tip = gelenYol.TIP,
                        Aciklama = gelenYol.ACIKLAMA,
                        AydinlatmaVarMi = gelenYol.AYDINLATMAVARMI,
                        BaglantiNiteligi = gelenYol.BAGLANTINITELIGI,
                        CaddeSokakLevhaVarMi = gelenYol.CADDESOKAKLEVHAVARMI,
                        Cinsi = gelenYol.CINSI,
                        Durum = gelenYol.DURUM,
                        Egimi = gelenYol.EGIMI,
                        HizLimiti = gelenYol.HIZLIMITI,
                        Islevi = gelenYol.ISLEVI,
                        KaldirimVarMi = gelenYol.KALDIRIMVARMI,
                        KaplamaCinsi = gelenYol.KAPLAMACINSI,
                        KimlikNo = gelenYol.KIMLIKNO,
                        MeclisKararNo = gelenYol.MECLISKARARNO,
                        MeclisKararTarih = gelenYol.MECLISKARARTARIH,
                        ParklanmaVarMi = gelenYol.PARKLANMAVARMI,
                        RefujVarMi = gelenYol.REFUJVARMI,
                        SeritCizgisiVarMi = gelenYol.SERITCIZGISIVARMI,
                        SeritSayisi = gelenYol.SERITSAYISI,
                        Sinyalizasyon = gelenYol.SINYALIZASYON,
                        TabelaAdi = gelenYol.TABELAADI,
                        YolcuDuragiVarMi = gelenYol.YOLCUDURAGIVARMI,
                        YolunDevamliligi = gelenYol.YOLUNDEVAMLILIGI,
                        YonDurumu = gelenYol.YONDURUMU,
                        ZeminNiteligi = gelenYol.ZEMINNITELIGI,
                        CiftBaslangicNo = gelenYol.CIFTBASLANGICNO,
                        CiftBitisNo = gelenYol.CIFTBITISNO,
                        TekBaslangicNo = gelenYol.TEKBASLANGICNO,
                        TekBitisNo = gelenYol.TEKBITISNO,
                        DegistirmeTarihi = gelenYol.DEGISTIRMETARIHISTRING,
                        OlusturmaTarihi = gelenYol.OLUSTURMATARIHISTRING
                    };
                    result = service.CsbmKaydetUi(data, true);
                    if (!result.IsSuccess)
                    {
                        throw new Exception(result.OperationMessage);
                    }
                    return _ItemId;

                    //int id = service.YolOrtaHatGuncelle(_ItemId, gelenYol.Geometry).Data;
                    //try
                    //{
                    //    //int? yolOrtaHatClientId = null;
                    //    using (var context = SenkronizasyonDbContextFactory.CreateSynchronizableContext<NumaratajContext>())
                    //    using (var tr = context.Database.BeginTransaction(IsolationLevel.ReadUncommitted))
                    //    {
                    //        var yolOrtaHat = context.YOLORTAHAT.Include("YOLORTAHATYON").FirstOrDefault(t => t.OBJECTID == _ItemId);

                    //        var mahalleler = ctx.MAHALLE.Where(t => t.SHAPE.Intersects(yolOrtaHat.SHAPE)).ToList();
                    //        var yonler = service.YolOrtaHatYonHesapla(yolOrtaHat.SHAPE, mahalleler);
                    //        using (var syncCtx = new SyncEntities())
                    //        {
                    //            if (context.YOLORTAHATYON.Local.Count > 0)
                    //            {
                    //                for (int i = 0; i < context.YOLORTAHATYON.Local.Count; i++)
                    //                {
                    //                    var yon = context.YOLORTAHATYON.Local[i];
                    //                    yon.MAHALLEID = yonler[i].MAHALLEID;
                    //                }
                    //            }
                    //            else
                    //            {
                    //                context.YOLORTAHATYON.AddRange(yonler);
                    //            }
                    //        }
                    //        yolOrtaHat.DEGISTIRMETARIHI = DateTime.Now.AddSeconds(1D);
                    //        var yol = context.YOL.Include("YOLORTAHAT").Where(t => t.OBJECTID == yolOrtaHat.YOLID).FirstOrDefault();
                    //        //SqlGeometryBuilder builder = new SqlGeometryBuilder();
                    //        var points = yol.YOLORTAHAT.Select(t=> t.SHAPE).ToList();
                    //        yol.SHAPE = Util.BuildLineStringFromGeometries(points);
                    //        context.SaveChanges();
                    //        tr.Commit();
                    //    }
                    //}
                    //catch (Exception e)
                    //{
                    //    LogHelper.LogException(e,"Yol Orta Hat Güncelleme");
                    //    throw e;
                    //}
                    //return id;
                }
            }
        }

        public override void SaveMedia(int medyaId)
        {
            using (var ctx = new NumaratajContext())
            {
                if (!ctx.YOL_MEDYA.Where(t => t.MEDYAID == medyaId).Any())
                {
                    ctx.YOL_MEDYA.Add(new YOL_MEDYA()
                    {
                        YOLID = _ItemId,
                        MEDYAID = medyaId
                    });
                    ctx.SaveChanges();
                }
            }
        }

        public override List<int> GetNesneMedya()
        {
            List<int> result = null;
            using (NumaratajContext ctx = new NumaratajContext())
            {
                result = ctx.YOL_MEDYA.Where(t => t.YOLID == _ItemId).Select(a => a.MEDYAID).ToList();
            }
            return result;
        }

        public override void NotKaydet(NotData not)
        {
            if (not.ServerId != null)
            {
                using (var ctx = new NumaratajContext())
                using (var tr = ctx.Database.BeginTransaction(IsolationLevel.ReadUncommitted))
                {
                    ctx.CSBM_NOT.Add(new CSBM_NOT()
                    {
                        CSBMID = not.ServerId.Value,
                        NOTID = not.NotId
                    });
                    ctx.SaveChanges();
                    tr.Commit();
                }
            }
        }

        public override string GetCentroid(ChangeSetItem item)
        {
            YOL gelenYOL = JsonConvert.DeserializeObject<YOL>(item.Item);
            DbGeometry nesneGeometry;
            if (!string.IsNullOrWhiteSpace(gelenYOL.Geometry))
            {
                try
                {
                    nesneGeometry = DbGeometry.FromText(gelenYOL.Geometry, 4326);
                    if (!nesneGeometry.IsValid)
                    {
                        nesneGeometry = DbGeometry.FromText(SqlGeometry.STGeomFromText(new SqlChars(nesneGeometry.AsText()), 4326).MakeValid().STAsText().ToSqlString().ToString(), 4326);
                    }
                }
                catch (Exception ex)
                {
                    return null;
                }

                DbGeometry hataGeometry = null;
                if (nesneGeometry.SpatialTypeName.ToLower() == "polygon")
                {
                    hataGeometry = nesneGeometry.Centroid;
                }
                else if (nesneGeometry.SpatialTypeName.ToLower() == "linestring")
                {
                    double startX, startY, endX, endY, middeleX, middeleY;
                    int pointCount = nesneGeometry.PointCount.Value;
                    if (pointCount % 2 == 0)
                    {
                        startX = nesneGeometry.PointAt(pointCount / 2).XCoordinate.Value;
                        startY = nesneGeometry.PointAt(pointCount / 2).YCoordinate.Value;
                        endX = nesneGeometry.PointAt((pointCount / 2) + 1).XCoordinate.Value;
                        endY = nesneGeometry.PointAt((pointCount / 2) + 1).YCoordinate.Value;
                        middeleX = (startX + endX) / 2;
                        middeleY = (startY + endY) / 2;

                        hataGeometry = DbGeometry.PointFromText(string.Format("POINT({0} {1})", middeleX.ToString(CultureInfo.InvariantCulture), middeleY.ToString(CultureInfo.InvariantCulture)), 4326);
                    }
                    else
                    {
                        int middlePointIndex = Convert.ToInt32(Math.Ceiling((decimal)(pointCount / 2)));
                        hataGeometry = nesneGeometry.PointAt(middlePointIndex);
                    }
                }
                else if (nesneGeometry.SpatialTypeName.ToLower() == "point")
                {
                    hataGeometry = nesneGeometry;
                }
                else
                {
                    return null;
                }
                return hataGeometry.AsText();
            }
            else
            {
                return null;
            }
        }
    }
}
