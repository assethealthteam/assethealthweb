﻿using Newtonsoft.Json;
using OdakGIS.Framework.Core.Entities;
using OdakGIS.Numarataj.Data;
using OdakGIS.Numarataj.Data.Entities;
using OdakGIS.Numarataj.Shared.Enums;
using OdakGIS.Sync;
using OdakGIS.Sync.ChangeManagement;
using OdakGIS.Sync.Entities;
using OdakGIS.Sync.Entities.Custom;
using OdakGIS.Sync.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Text;

namespace OdakGIS.Numarataj.Shared.ChangeManagement
{
    public class BagimsizBolumChangeSetItemFactory : ChangeSetItemFactory<BAGIMSIZBOLUM, BAGIMSIZBOLUM>
    {
        public BagimsizBolumChangeSetItemFactory(int itemId, int cihazId, SenkronizasyonOperasyonTipi itemState)
            : base(itemId, cihazId, itemState)
        {

        }
        protected override OperationResult Validate(BAGIMSIZBOLUM item)
        {
            OperationResult result = new OperationResult()
            {
                IsSuccess = true
            };
            return result;
        }

        protected override BAGIMSIZBOLUM PrepareForSynchronization(int itemId)
        {
            BAGIMSIZBOLUM bagimsizBolum = null;
            using (NumaratajContext context = new NumaratajContext())
            using (var tr = context.Database.BeginTransaction(IsolationLevel.ReadUncommitted))
            {
                bagimsizBolum = context.BAGIMSIZBOLUM.FirstOrDefault(t => t.OBJECTID == itemId);
                bagimsizBolum.OBJECTID = bagimsizBolum.OBJECTID;
            }
            return bagimsizBolum;
        }

        public override string GetCentroid(ChangeSetItem item)
        {
            BAGIMSIZBOLUM gelenBAGIMSIZBOLUM = JsonConvert.DeserializeObject<BAGIMSIZBOLUM>(item.Item);
            if (gelenBAGIMSIZBOLUM.YAPIID > 0)
            {
                int? yapiServerId = null;
                using (var ctx = new SyncEntities())
                using (var tr = ctx.Database.BeginTransaction(IsolationLevel.ReadUncommitted))
                {
                    yapiServerId = GetServerId(ctx, gelenBAGIMSIZBOLUM.YAPIID, "YAPI");
                }
                if (yapiServerId.HasValue)
                {
                    using (var ctxNumarataj = new NumaratajContext())
                    using (var tr = ctxNumarataj.Database.BeginTransaction(IsolationLevel.ReadUncommitted))
                    {
                        DbGeometry geom = ctxNumarataj.YAPI
                                             .Where(l => l.OBJECTID == yapiServerId)
                                             .Select(l => l.SHAPE)
                                             .FirstOrDefault();

                        if (geom != null)
                        {
                            return geom.Centroid.AsText();
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
                else
                {
                    return null;
                }
            }
            else
                return null;
        }

        public override int SaveChangeSetItem(ChangeSetItem item)
        {
            BAGIMSIZBOLUM gelenBAGIMSIZBOLUM = JsonConvert.DeserializeObject<BAGIMSIZBOLUM>(item.Item);
            gelenBAGIMSIZBOLUM.OLUSTURMATARIHI = !string.IsNullOrWhiteSpace(gelenBAGIMSIZBOLUM.OLUSTURMATARIHISTRING) ? (DateTime?)DateTime.ParseExact(gelenBAGIMSIZBOLUM.OLUSTURMATARIHISTRING, "yyyyMMddHHmmss", null) : null;
            gelenBAGIMSIZBOLUM.DEGISTIRMETARIHI = !string.IsNullOrWhiteSpace(gelenBAGIMSIZBOLUM.DEGISTIRMETARIHISTRING) ? (DateTime?)DateTime.ParseExact(gelenBAGIMSIZBOLUM.DEGISTIRMETARIHISTRING, "yyyyMMddHHmmss", null) : null;
            int? yapiServerId = null;
            int? numaratajServerId = null;

            using (var ctx = new SyncEntities())
            {
                if (gelenBAGIMSIZBOLUM.YAPIID > 0)
                {
                    yapiServerId = GetServerId(ctx, gelenBAGIMSIZBOLUM.YAPIID, "YAPI");
                }
                if (gelenBAGIMSIZBOLUM.NUMARATAJID > 0)
                {
                    numaratajServerId = GetServerId(ctx, gelenBAGIMSIZBOLUM.NUMARATAJID.Value, "NUMARATAJ");
                }
            }
            if (!(yapiServerId > 0))
            {
                throw new Exception("Bu bağımsız bölumun binası sistemde bulunamadı");
            }
            gelenBAGIMSIZBOLUM.YAPIID = yapiServerId.Value;

            using (var ctxNumarataj = SenkronizasyonDbContextFactory.CreateSynchronizableContext<NumaratajContext>())
            using (var tr = ctxNumarataj.Database.BeginTransaction(IsolationLevel.ReadUncommitted))
            {
                if (!(numaratajServerId > 0))
                {
                    DbGeometry yapiGeom = ctxNumarataj.YAPI
                                                        .Where(l => l.OBJECTID == gelenBAGIMSIZBOLUM.YAPIID)
                                                        .Select(l => l.SHAPE)
                                                        .FirstOrDefault();
                    List<int> ustUsteYapilar = ctxNumarataj.YAPI
                                                             .Where(l => !l.SHAPE.Touches(yapiGeom) && l.SHAPE.Intersects(yapiGeom))
                                                             .Select(l => l.OBJECTID)
                                                             .ToList();
                    List<NUMARATAJ> ustUsteYapilarNumaratajList = ctxNumarataj.NUMARATAJ
                                                                                .Where(l => ustUsteYapilar.Contains(l.YAPIID.Value)).ToList();
                    List<NUMARATAJ> numaratajList = ctxNumarataj.NUMARATAJ
                                                                     .Where(l => l.YAPIID == gelenBAGIMSIZBOLUM.YAPIID)
                                                                     .ToList();

                    if ((numaratajList == null || numaratajList.Count == 0) && (ustUsteYapilarNumaratajList == null || ustUsteYapilarNumaratajList.Count == 0))
                    {
                        throw new Exception("Bu bağımsız bölumun binasınına ayit her hangi bir Numarataj bulunamadı");
                    }
                    else
                    {
                        if (numaratajList != null && numaratajList.Count > 0 && !string.IsNullOrWhiteSpace(gelenBAGIMSIZBOLUM.DISKAPINO))
                        {
                            numaratajServerId = numaratajList
                                                .Where(l => !string.IsNullOrWhiteSpace(l.KAPINO) &&
                                                            RemoveSpecialCharacters(l.KAPINO.ToLower()) == RemoveSpecialCharacters(gelenBAGIMSIZBOLUM.DISKAPINO.ToLower()))
                                                .Select(l => l.OBJECTID)
                                                .FirstOrDefault();
                        }
                        if (!(numaratajServerId > 0))
                        {
                            if (numaratajList != null && numaratajList.Count > 0 && (!string.IsNullOrWhiteSpace(gelenBAGIMSIZBOLUM.BAGIMSIZBOLUMNO)) && (!IsAllDigits(gelenBAGIMSIZBOLUM.BAGIMSIZBOLUMNO)))
                            {
                                numaratajServerId = numaratajList
                                           .Where(l => !string.IsNullOrWhiteSpace(l.KAPINO) &&
                                                       RemoveSpecialCharacters(l.KAPINO.ToLower()) == RemoveSpecialCharacters(gelenBAGIMSIZBOLUM.BAGIMSIZBOLUMNO.ToLower()))
                                           .Select(l => l.OBJECTID)
                                           .FirstOrDefault();
                            }
                            if (!(numaratajServerId > 0))
                            {
                                if (numaratajList != null && numaratajList.Count > 0 && (!string.IsNullOrWhiteSpace(gelenBAGIMSIZBOLUM.BAGIMSIZBOLUMNO)) && (IsAllDigits(gelenBAGIMSIZBOLUM.BAGIMSIZBOLUMNO)))
                                {
                                    numaratajServerId = numaratajList
                                                            .Where(l => l.TIP == Convert.ToInt32(NumaratajTip.AnaGiris))
                                                            .Select(l => l.OBJECTID)
                                                            .FirstOrDefault();
                                }
                            }
                            if (!(numaratajServerId > 0))
                            {
                                if (ustUsteYapilarNumaratajList != null && ustUsteYapilarNumaratajList.Count > 0)
                                {
                                    if (!string.IsNullOrWhiteSpace(gelenBAGIMSIZBOLUM.DISKAPINO))
                                    {
                                        numaratajServerId = ustUsteYapilarNumaratajList
                                                            .Where(l => !string.IsNullOrWhiteSpace(l.KAPINO) &&
                                                                        RemoveSpecialCharacters(l.KAPINO.ToLower()) == RemoveSpecialCharacters(gelenBAGIMSIZBOLUM.DISKAPINO.ToLower()))
                                                            .Select(l => l.OBJECTID)
                                                            .FirstOrDefault();
                                    }
                                    if (!(numaratajServerId > 0))
                                    {
                                        if ((!string.IsNullOrWhiteSpace(gelenBAGIMSIZBOLUM.BAGIMSIZBOLUMNO)) && (!IsAllDigits(gelenBAGIMSIZBOLUM.BAGIMSIZBOLUMNO)))
                                        {
                                            numaratajServerId = ustUsteYapilarNumaratajList
                                                       .Where(l => !string.IsNullOrWhiteSpace(l.KAPINO) &&
                                                                   RemoveSpecialCharacters(l.KAPINO.ToLower()) == RemoveSpecialCharacters(gelenBAGIMSIZBOLUM.BAGIMSIZBOLUMNO.ToLower()))
                                                       .Select(l => l.OBJECTID)
                                                       .FirstOrDefault();
                                        }
                                        if (!(numaratajServerId > 0))
                                        {
                                            if ((!string.IsNullOrWhiteSpace(gelenBAGIMSIZBOLUM.BAGIMSIZBOLUMNO)) && (IsAllDigits(gelenBAGIMSIZBOLUM.BAGIMSIZBOLUMNO)))
                                            {
                                                numaratajServerId = ustUsteYapilarNumaratajList
                                                                            .Where(l => l.TIP == Convert.ToInt32(NumaratajTip.AnaGiris))
                                                                            .Select(l => l.OBJECTID)
                                                                            .FirstOrDefault();
                                            }
                                        }
                                    }
                                }
                                if (!(numaratajServerId > 0))
                                {
                                    throw new Exception("Lütfen Bağımsız Bölümün Numarataj'ı seçiniz.");
                                }
                            }
                        }
                    }
                }

                gelenBAGIMSIZBOLUM.NUMARATAJID = numaratajServerId;

                if (_ItemState == SenkronizasyonOperasyonTipi.Yeni)
                {
                    ctxNumarataj.BAGIMSIZBOLUM.Add(gelenBAGIMSIZBOLUM);
                }
                else
                {
                    var varolanBAGIMSIZBOLUM = ctxNumarataj.BAGIMSIZBOLUM.FirstOrDefault(t => t.OBJECTID == _ItemId);
                    if (_ItemState == SenkronizasyonOperasyonTipi.Degismis)
                    {
                        if (varolanBAGIMSIZBOLUM == null)
                        {
                            ctxNumarataj.BAGIMSIZBOLUM.Add(gelenBAGIMSIZBOLUM);
                        }
                        else
                        {
                            ctxNumarataj.Entry<BAGIMSIZBOLUM>(varolanBAGIMSIZBOLUM).CurrentValues.SetValues(gelenBAGIMSIZBOLUM);
                        }
                    }
                    else if (_ItemState == SenkronizasyonOperasyonTipi.Silinmis)
                    {
                        ctxNumarataj.BAGIMSIZBOLUM.Remove(varolanBAGIMSIZBOLUM);
                    }
                }
                ctxNumarataj.SaveChanges();
                tr.Commit();
            }
            return _ItemId == 0 ? gelenBAGIMSIZBOLUM.OBJECTID : _ItemId;
        }

        public override void SaveMedia(int medyaId)
        {

            throw new NotImplementedException();
        }

        public override List<int> GetNesneMedya()
        {
            throw new NotImplementedException();
        }

        public override void NotKaydet(NotData not)
        {
            throw new NotImplementedException();
        }


        public static string RemoveSpecialCharacters(string str)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'))
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }

        bool IsAllDigits(string s)
        {
            return s.All(char.IsDigit);
        }
    }
}
