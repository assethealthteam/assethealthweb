﻿using Microsoft.SqlServer.Types;
using Newtonsoft.Json;
using OdakGIS.Framework.Core.Entities;
using OdakGIS.Numarataj.Data;
using OdakGIS.Numarataj.Data.Entities;
using OdakGIS.Sync;
using OdakGIS.Sync.ChangeManagement;
using OdakGIS.Sync.Entities;
using OdakGIS.Sync.Entities.Custom;
using OdakGIS.Sync.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Spatial;
using System.Data.SqlTypes;
using System.Globalization;
using System.Linq;

namespace OdakGIS.Numarataj.Shared.ChangeManagement
{
    public class YolTabelaChangeSetItemFactory : ChangeSetItemFactory<TABELA, TABELA>
    {
        public YolTabelaChangeSetItemFactory(int itemId, int cihazId, SenkronizasyonOperasyonTipi itemState)
            : base(itemId, cihazId, itemState)
        {

        }
        protected override OperationResult Validate(TABELA item)
        {
            OperationResult result = new OperationResult()
            {
                IsSuccess = true
            };
            return result;
        }

        protected override TABELA PrepareForSynchronization(int itemId)
        {//sadece tabletten
            throw new NotImplementedException();
        }

        public override int SaveChangeSetItem(ChangeSetItem item)
        {
            //int result = 0;
            TABELA gelenTabela = JsonConvert.DeserializeObject<TABELA>(item.Item);
            gelenTabela.SHAPE = DbGeometry.FromText(gelenTabela.Geometry, 4326);
            int? yolServerId = null;
            using (var ctx = new SyncEntities())
                yolServerId = GetServerId(ctx, gelenTabela.YOLID, "YOL");
            using (var ctxNumarataj = SenkronizasyonDbContextFactory.CreateSynchronizableContext<NumaratajContext>())
            using (var tr = ctxNumarataj.Database.BeginTransaction(IsolationLevel.ReadUncommitted))
            {
                if (yolServerId == null)
                {
                    throw new Exception("Bu tabelanın yolu sistemde bulunamadı");
                }
                gelenTabela.YOLID = yolServerId.Value;
                if (_ItemState == SenkronizasyonOperasyonTipi.Yeni)
                {
                    ctxNumarataj.TABELA.Add(gelenTabela);
                }
                else
                {
                    var varolanTabela = ctxNumarataj.TABELA.FirstOrDefault(t => t.ID == _ItemId);
                    if (_ItemState == SenkronizasyonOperasyonTipi.Degismis)
                    {
                        if (varolanTabela == null)
                        {
                            ctxNumarataj.TABELA.Add(gelenTabela);
                        }
                        else
                        {
                            ctxNumarataj.Entry<TABELA>(varolanTabela).CurrentValues.SetValues(gelenTabela);
                        }
                    }
                    else if (_ItemState == SenkronizasyonOperasyonTipi.Silinmis)
                    {
                        ctxNumarataj.TABELA.Remove(varolanTabela);
                    }
                }
                ctxNumarataj.SaveChanges();
                tr.Commit();
            }
            return _ItemId == 0 ? gelenTabela.ID : _ItemId;
        }

        public override void SaveMedia(int medyaId)
        {

            using (var ctxNumarataj = new NumaratajContext())
            using (var tr = ctxNumarataj.Database.BeginTransaction(IsolationLevel.ReadUncommitted))
            {
                ctxNumarataj.TABELA_MEDYA.Add(new TABELA_MEDYA()
                {
                    TABELAID = _ItemId,
                    MEDYAID = medyaId
                });
                ctxNumarataj.SaveChanges();
                tr.Commit();
            }
        }

        public override List<int> GetNesneMedya()
        {
            List<int> result = null;
            using (var ctxNumarataj = new NumaratajContext())
            using (var tr = ctxNumarataj.Database.BeginTransaction(IsolationLevel.ReadUncommitted))
            {
                result = ctxNumarataj.TABELA_MEDYA.Where(t => t.TABELAID == _ItemId).Select(t => t.MEDYAID).ToList();
                tr.Commit();
            }
            return result;
        }

        public override void NotKaydet(NotData not)
        {
            throw new NotImplementedException();
        }

        public override string GetCentroid(ChangeSetItem item)
        {
            TABELA gelenTABELA = JsonConvert.DeserializeObject<TABELA>(item.Item);
            DbGeometry nesneGeometry;
            if (!string.IsNullOrWhiteSpace(gelenTABELA.Geometry))
            {
                try
                {
                    nesneGeometry = DbGeometry.FromText(gelenTABELA.Geometry, 4326);
                    if (!nesneGeometry.IsValid)
                    {
                        nesneGeometry = DbGeometry.FromText(SqlGeometry.STGeomFromText(new SqlChars(nesneGeometry.AsText()), 4326).MakeValid().STAsText().ToSqlString().ToString(), 4326);
                    }
                }
                catch (Exception ex)
                {
                    return null;
                }

                DbGeometry hataGeometry = null;
                if (nesneGeometry.SpatialTypeName.ToLower() == "polygon")
                {
                    hataGeometry = nesneGeometry.Centroid;
                }
                else if (nesneGeometry.SpatialTypeName.ToLower() == "linestring")
                {
                    double startX, startY, endX, endY, middeleX, middeleY;
                    int pointCount = nesneGeometry.PointCount.Value;
                    if (pointCount % 2 == 0)
                    {
                        startX = nesneGeometry.PointAt(pointCount / 2).XCoordinate.Value;
                        startY = nesneGeometry.PointAt(pointCount / 2).YCoordinate.Value;
                        endX = nesneGeometry.PointAt((pointCount / 2) + 1).XCoordinate.Value;
                        endY = nesneGeometry.PointAt((pointCount / 2) + 1).YCoordinate.Value;
                        middeleX = (startX + endX) / 2;
                        middeleY = (startY + endY) / 2;

                        hataGeometry = DbGeometry.PointFromText(string.Format("POINT({0} {1})", middeleX.ToString(CultureInfo.InvariantCulture), middeleY.ToString(CultureInfo.InvariantCulture)), 4326);
                    }
                    else
                    {
                        int middlePointIndex = Convert.ToInt32(Math.Ceiling((decimal)(pointCount / 2)));
                        hataGeometry = nesneGeometry.PointAt(middlePointIndex);
                    }
                }
                else if (nesneGeometry.SpatialTypeName.ToLower() == "point")
                {
                    hataGeometry = nesneGeometry;
                }
                else
                {
                    return null;
                }
                return hataGeometry.AsText();
            }
            else
            {
                return null;
            }
        }
    }
}