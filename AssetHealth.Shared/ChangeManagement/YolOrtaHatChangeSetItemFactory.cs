﻿using OdakGIS.Framework.Core.Entities;
using OdakGIS.Numarataj.Data;
using OdakGIS.Numarataj.Data.Entities;
using OdakGIS.Sync.ChangeManagement;
using OdakGIS.Sync.Entities.Custom;
using OdakGIS.Sync.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace OdakGIS.Numarataj.Shared.ChangeManagement
{
    public class YolOrtaHatChangeSetItemFactory : ChangeSetItemFactory<YOLORTAHAT, YOLORTAHAT>
    {
        public YolOrtaHatChangeSetItemFactory(int id, int cihazId, SenkronizasyonOperasyonTipi itemState)
            : base(id, cihazId, itemState)
        {

        }
        protected override OperationResult Validate(YOLORTAHAT item)
        {
            return new OperationResult() { IsSuccess = true };
        }

        protected override YOLORTAHAT PrepareForSynchronization(int itemId)
        {
            YOLORTAHAT ortaHat = null;
            using (var ctx = new NumaratajContext())
            using (var tr = ctx.Database.BeginTransaction(IsolationLevel.ReadUncommitted))
            {
                ortaHat = ctx.YOLORTAHAT.FirstOrDefault(t => t.OBJECTID == itemId);
                ortaHat.ID = ortaHat.OBJECTID;
                //if (ortaHat.YOLID != null)
                //    ortaHat.YOLID = GetClientId(new SyncEntities(), "YOL", ortaHat.YOLID.Value);
                ortaHat.Geometry = ortaHat.SHAPE.AsText();
                tr.Commit();
            }
            return ortaHat;
        }

        public override int SaveChangeSetItem(ChangeSetItem item)
        {
            throw new NotImplementedException();
        }

        public override void SaveMedia(int medyaId)
        {
            throw new NotImplementedException();
        }

        public override List<int> GetNesneMedya()
        {
            throw new NotImplementedException();
        }

        public override void NotKaydet(NotData not)
        {
            throw new NotImplementedException();
        }

        public override string GetCentroid(ChangeSetItem item)
        {
            return null;
        }
    }
}