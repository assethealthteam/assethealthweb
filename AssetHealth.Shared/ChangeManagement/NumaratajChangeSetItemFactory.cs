﻿using Microsoft.SqlServer.Types;
using Newtonsoft.Json;
using OdakGIS.Framework.Core.Entities;
using OdakGIS.Framework.Core.Managers;
using OdakGIS.Numarataj.Data;
using OdakGIS.Numarataj.Data.Entities;
using OdakGIS.Numarataj.Shared.Entities;
using OdakGIS.Numarataj.Shared.Interfaces;
using OdakGIS.Sync.ChangeManagement;
using OdakGIS.Sync.Entities;
using OdakGIS.Sync.Entities.Custom;
using OdakGIS.Sync.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Spatial;
using System.Data.SqlTypes;
using System.Globalization;
using System.Linq;

namespace OdakGIS.Numarataj.Shared.ChangeManagement
{
    public class NumaratajChangeSetItemFactory : ChangeSetItemFactory<NUMARATAJ, NUMARATAJ>
    {
        public NumaratajChangeSetItemFactory(int itemId, int cihazId, SenkronizasyonOperasyonTipi itemState)
            : base(itemId, cihazId, itemState)
        {

        }
        protected override OperationResult Validate(NUMARATAJ item)
        {
            return new OperationResult()
            {
                IsSuccess = true
            };
        }

        protected override NUMARATAJ PrepareForSynchronization(int itemId)
        {
            NUMARATAJ numarataj = null;
            using (NumaratajContext context = new NumaratajContext())
            using (var tr = context.Database.BeginTransaction(IsolationLevel.ReadUncommitted))
            {
                numarataj = context.NUMARATAJ.FirstOrDefault(t => t.OBJECTID == itemId);
                numarataj.Geometry = numarataj.SHAPE.AsText();
                numarataj.ID = numarataj.OBJECTID;
                using (var syncContext = new SyncEntities())
                {
                    if (numarataj.YAPIID != null)
                    {
                        numarataj.YAPIID = GetClientId(syncContext, "YAPI", numarataj.YAPIID.Value);
                    }

                    if (numarataj.YOLORTAHATYONID != null)
                    {
                        var yolOrtaHatYon = context.YOLORTAHATYON.Include("YOLORTAHAT").FirstOrDefault(t => t.OBJECTID == numarataj.YOLORTAHATYONID);
                        if (yolOrtaHatYon != null)
                        {
                            numarataj.YOLORTAHATID = GetClientId(syncContext, "YOLORTAHAT", yolOrtaHatYon.YOLORTAHAT.OBJECTID);
                        }
                    }
                }
                //tr.Commit();
            }
            return numarataj;
        }

        public override int SaveChangeSetItem(ChangeSetItem item)
        {
            //double buffer = 0.000005D;
            //YAPI yapi = null;
            OperationResult<int> result = null;
            NUMARATAJ gelenNumarataj = JsonConvert.DeserializeObject<NUMARATAJ>(item.Item);
            gelenNumarataj.SHAPE = DbGeometry.FromText(gelenNumarataj.Geometry, 4326);
            if (!gelenNumarataj.SHAPE.IsValid)
            {
                throw new Exception("Numaratajın kapı linki geometrisi geçersizdir");
            }
            gelenNumarataj.OBJECTID = _ItemId;
            INumaratajService service = ServiceManager.GetService<INumaratajService>();
            using (NumaratajContext context = new NumaratajContext())
            {
                if (_ItemId != 0)
                {
                    if (context.NUMARATAJ.FirstOrDefault(t => t.OBJECTID == _ItemId) == null)
                    {
                        return _ItemId;
                    }
                }

                if (_ItemState == SenkronizasyonOperasyonTipi.Yeni)
                {
                    NumaratajData data = new NumaratajData()
                    {
                        Id = 0,
                        KapiNo = gelenNumarataj.KAPINO,
                        Geometry = gelenNumarataj.Geometry,
                        BagliOlduguNumaratajId = gelenNumarataj.BAGLIOLDUGUNUMARATAJID,
                        Tip = gelenNumarataj.TIP != null ? (Enums.NumaratajTip?)gelenNumarataj.TIP : null,
                        UavtNo = gelenNumarataj.UAVTNO,
                        MontajYapilmamaSebebi = gelenNumarataj.MONTAJYAPILMAMASEBEBI,
                        Aciklama = gelenNumarataj.ACIKLAMA,
                        Ad = gelenNumarataj.AD,
                        Binakodu = gelenNumarataj.BINAKODU,
                        DigerYapiId = gelenNumarataj.DIGERYAPIID,
                        KimlikNo = gelenNumarataj.KIMLIKNO,
                        ParselId = gelenNumarataj.PARSELID,
                        ParselKimlikNo = gelenNumarataj.PARSELKIMLIKNO,
                        YapiId = gelenNumarataj.YAPIID,
                        YerlesimAcisi = gelenNumarataj.YERLESIMACISI,
                        YolOrtaHatId = gelenNumarataj.YOLORTAHATID,
                        YolOrtaHatYonId = gelenNumarataj.YOLORTAHATYONID,
                        MevcutDurum = gelenNumarataj.MEVCUTDURUM,
                        DegistirmeTarihi = gelenNumarataj.DEGISTIRMETARIHISTRING,
                        OlusturmaTarihi = gelenNumarataj.OLUSTURMATARIHISTRING
                    };
                    result = service.NumaratajKaydet(data, true);
                    if (!result.IsSuccess)
                        throw new Exception(result.OperationMessage);
                    return result.Data;
                }
                else if (item.ItemState == SenkronizasyonOperasyonTipi.Silinmis)
                {
                    NUMARATAJ numarataj = context.NUMARATAJ.FirstOrDefault(t => t.OBJECTID == _ItemId);
                    List<BAGIMSIZBOLUM> bbList = context.BAGIMSIZBOLUM
                                                            .Where(l => l.NUMARATAJID == numarataj.OBJECTID)
                                                            .ToList();

                    context.BAGIMSIZBOLUM.RemoveRange(bbList);
                    context.NUMARATAJ.Remove(numarataj);
                    context.SaveChanges();
                    return _ItemId;
                }
                else
                {
                    NumaratajData data = new NumaratajData()
                    {
                        Id = _ItemId,
                        KapiNo = gelenNumarataj.KAPINO,
                        Geometry = gelenNumarataj.Geometry,
                        BagliOlduguNumaratajId = gelenNumarataj.BAGLIOLDUGUNUMARATAJID,
                        Tip = gelenNumarataj.TIP != null ? (Enums.NumaratajTip?)gelenNumarataj.TIP : null,
                        UavtNo = gelenNumarataj.UAVTNO,
                        MontajYapilmamaSebebi = gelenNumarataj.MONTAJYAPILMAMASEBEBI,
                        Aciklama = gelenNumarataj.ACIKLAMA,
                        Ad = gelenNumarataj.AD,
                        Binakodu = gelenNumarataj.BINAKODU,
                        DigerYapiId = gelenNumarataj.DIGERYAPIID,
                        KimlikNo = gelenNumarataj.KIMLIKNO,
                        ParselId = gelenNumarataj.PARSELID,
                        ParselKimlikNo = gelenNumarataj.PARSELKIMLIKNO,
                        YapiId = gelenNumarataj.YAPIID,
                        YerlesimAcisi = gelenNumarataj.YERLESIMACISI,
                        YolOrtaHatId = gelenNumarataj.YOLORTAHATID,
                        YolOrtaHatYonId = gelenNumarataj.YOLORTAHATYONID,
                        MevcutDurum = gelenNumarataj.MEVCUTDURUM,
                        DegistirmeTarihi = gelenNumarataj.DEGISTIRMETARIHISTRING,
                        OlusturmaTarihi = gelenNumarataj.OLUSTURMATARIHISTRING
                    };
                    result = service.NumaratajKaydet(data, true);
                    if (!result.IsSuccess)
                    {
                        throw new Exception(result.OperationMessage);
                    }
                    return _ItemId;
                }
            }
        }

        public override void SaveMedia(int medyaId)
        {
            using (var ctx = new NumaratajContext())
            {
                if (!ctx.NUMARATAJ_MEDYA.Where(t => t.MEDYAID == medyaId).Any())
                {
                    ctx.Database.ExecuteSqlCommand(string.Format("insert into NUMARATAJ_MEDYA select {0},{1}", medyaId, _ItemId));
                    //ctx.YAPI_MEDYA.Add(new YAPI_MEDYA()
                    //{
                    //    YAPIID = _ItemId,
                    //    MEDYAID = medyaId
                    //});
                    ctx.SaveChanges();
                }
            }
        }

        public override List<int> GetNesneMedya()
        {
            List<int> result = null;
            using (NumaratajContext ctx = new NumaratajContext())
            {
                result = ctx.NUMARATAJ_MEDYA.Where(t => t.NUMARATAJID == _ItemId).Select(a => a.MEDYAID).ToList();
            }
            return result;
        }

        public override void NotKaydet(NotData not)
        {
            if (not.ServerId != null)
            {
                using (var ctx = new NumaratajContext())
                using (var tr = ctx.Database.BeginTransaction(IsolationLevel.ReadUncommitted))
                {
                    ctx.NUMARATAJ_NOT.Add(new NUMARATAJ_NOT()
                    {
                        NUMARATAJID = not.ServerId.Value,
                        NOTID = not.NotId
                    });
                    ctx.SaveChanges();
                    tr.Commit();
                }
            }
        }

        public override string GetCentroid(ChangeSetItem item)
        {
            NUMARATAJ gelenNUMARATAJ = JsonConvert.DeserializeObject<NUMARATAJ>(item.Item);
            DbGeometry nesneGeometry;
            if (!string.IsNullOrWhiteSpace(gelenNUMARATAJ.Geometry))
            {
                try
                {
                    nesneGeometry = DbGeometry.FromText(gelenNUMARATAJ.Geometry, 4326);
                    if (!nesneGeometry.IsValid)
                    {
                        nesneGeometry = DbGeometry.FromText(SqlGeometry.STGeomFromText(new SqlChars(nesneGeometry.AsText()), 4326).MakeValid().STAsText().ToSqlString().ToString(), 4326);
                    }
                }
                catch (Exception ex)
                {
                    return null;
                }

                DbGeometry hataGeometry = null;
                if (nesneGeometry.SpatialTypeName.ToLower() == "polygon")
                {
                    hataGeometry = nesneGeometry.Centroid;
                }
                else if (nesneGeometry.SpatialTypeName.ToLower() == "linestring")
                {
                    double startX, startY, endX, endY, middeleX, middeleY;
                    int pointCount = nesneGeometry.PointCount.Value;
                    if (pointCount % 2 == 0)
                    {
                        startX = nesneGeometry.PointAt(pointCount / 2).XCoordinate.Value;
                        startY = nesneGeometry.PointAt(pointCount / 2).YCoordinate.Value;
                        endX = nesneGeometry.PointAt((pointCount / 2) + 1).XCoordinate.Value;
                        endY = nesneGeometry.PointAt((pointCount / 2) + 1).YCoordinate.Value;
                        middeleX = (startX + endX) / 2;
                        middeleY = (startY + endY) / 2;

                        hataGeometry = DbGeometry.PointFromText(string.Format("POINT({0} {1})", middeleX.ToString(CultureInfo.InvariantCulture), middeleY.ToString(CultureInfo.InvariantCulture)), 4326);
                    }
                    else
                    {
                        int middlePointIndex = Convert.ToInt32(Math.Ceiling((decimal)(pointCount / 2)));
                        hataGeometry = nesneGeometry.PointAt(middlePointIndex);
                    }
                }
                else if (nesneGeometry.SpatialTypeName.ToLower() == "point")
                {
                    hataGeometry = nesneGeometry;
                }
                else
                {
                    return null;
                }
                return hataGeometry.AsText();
            }
            else
            {
                return null;
            }
        }
    }
}