﻿using Microsoft.SqlServer.Types;
using Newtonsoft.Json;
using OdakGIS.Framework.Core.Entities;
using OdakGIS.Framework.Core.Managers;
using OdakGIS.Numarataj.Data;
using OdakGIS.Numarataj.Data.Entities;
using OdakGIS.Numarataj.Shared.Entities;
using OdakGIS.Numarataj.Shared.Interfaces;
using OdakGIS.Sync.ChangeManagement;
using OdakGIS.Sync.Entities.Custom;
using OdakGIS.Sync.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Spatial;
using System.Data.SqlTypes;
using System.Globalization;
using System.Linq;

namespace OdakGIS.Numarataj.Shared.ChangeManagement
{
    public class BinaChangeSetItemFactory : ChangeSetItemFactory<YAPI, YAPI>
    {
        public BinaChangeSetItemFactory(int itemId, int cihazId, SenkronizasyonOperasyonTipi itemState)
            : base(itemId, cihazId, itemState)
        {

        }

        protected override OperationResult Validate(YAPI item)
        {
            OperationResult result = new OperationResult()
            {
                IsSuccess = true
            };
            return result;
        }

        protected override YAPI PrepareForSynchronization(int itemId)
        {
            YAPI yapi = null;
            using (NumaratajContext context = new NumaratajContext())
            using (var tr = context.Database.BeginTransaction(IsolationLevel.ReadUncommitted))
            {
                yapi = context.YAPI.FirstOrDefault(t => t.OBJECTID == itemId);
                yapi.Geometry = yapi.SHAPE.AsText();
                yapi.ID = yapi.OBJECTID;
            }
            return yapi;
        }

        public override int SaveChangeSetItem(ChangeSetItem item)
        {
            OperationResult<int> result = null;
            YAPI gelenYapi = JsonConvert.DeserializeObject<YAPI>(item.Item);
            gelenYapi.OBJECTID = _ItemId;
            IYapiService service = ServiceManager.GetService<IYapiService>();
            using (NumaratajContext context = new NumaratajContext())
            {
                if (_ItemId != 0)
                {
                    if (context.YAPI.FirstOrDefault(t => t.OBJECTID == _ItemId) == null)
                    {
                        return _ItemId;
                    }
                }
                if (_ItemState == SenkronizasyonOperasyonTipi.Yeni)
                {
                    YapiData data = new YapiData()
                    {
                        Id = 0,
                        Adi = gelenYapi.AD,
                        Durum = gelenYapi.YAPIDURUM,
                        Geometry = gelenYapi.Geometry,
                        Parsel = gelenYapi.PARSEL != null ? gelenYapi.PARSEL.ADANO.ToString() + " " + gelenYapi.PARSEL.PARSELNO : "",
                        PostaKodu = gelenYapi.POSTAKODU,
                        SiteVeyaKooperatifAdi = gelenYapi.SITEVEYAKOOPERATIFADI,
                        Tip = gelenYapi.TIP,
                        ZeminAltiKatSayi = gelenYapi.ZEMINALTIKATSAYISI,
                        ZeminUstuKatSayi = gelenYapi.ZEMINUSTUKATSAYISI,
                        UavtNo = gelenYapi.UAVTNO,
                        UavtEslesmiyor = gelenYapi.UAVTESLESMIYOR,
                        Aciklama = gelenYapi.ACIKLAMA,
                        AcilAsansorSayisi = gelenYapi.ACILASANSORSAYISI,
                        AcilDurumAsansoruVarMi = gelenYapi.ACILDURUMASANSORUVARMI,
                        AcilDurumPlaniVarMi = gelenYapi.ACILDURUMPLANIVARMI,
                        AnonsSistemi = gelenYapi.ANONSSISTEMI,
                        AsansorSayisi = gelenYapi.ASANSORSAYISI,
                        AsansorVarMi = gelenYapi.ASANSORVARMI,
                        BbSayisi = gelenYapi.BBSAYISI,
                        BinaYapimYili = gelenYapi.BINAYAPIMYILI,
                        BinaYasi = gelenYapi.BINAYASI,
                        BodrumSayisi = gelenYapi.BODRUMSAYISI,
                        CatiDurumu = gelenYapi.CATIDURUMU,
                        DigerMi = gelenYapi.DIGERMI,
                        DiniMi = gelenYapi.DINIMI,
                        DogalGazAbone = gelenYapi.DOGALGAZABONE,
                        EgitimMi = gelenYapi.EGITIMMI,
                        ElektrikAbone = gelenYapi.ELEKTRIKABONE,
                        EngelliGirisiVarMi = gelenYapi.ENGELLIGIRISIVARMI,
                        EskiKapiNo = gelenYapi.ESKIKAPINO,
                        FizikselDurum = gelenYapi.FIZIKSELDURUM,
                        InsaatTipi = gelenYapi.INSAATTIPI,
                        IsinmaBilgileri = gelenYapi.ISINMABILGILERI,
                        IsinmaBilgileriMadde = gelenYapi.ISINMABILGILERIMADDE,
                        JeneratorVarMi = gelenYapi.JENERATORVARMI,
                        KatKrokisiVarMi = gelenYapi.KATKROKISIVARMI,
                        KatSayisi = gelenYapi.KATSAYISI,
                        Kaynak = gelenYapi.KAYNAK,
                        KazanDairesiVarMi = gelenYapi.KAZANDAIRESIVARMI,
                        KimlikNo = gelenYapi.KIMLIKNO,
                        KonutVarMi = gelenYapi.KONUTVARMI,
                        KulturelMi = gelenYapi.KULTURELMI,
                        Olcek = gelenYapi.OLCEK,
                        OtoparkSayi = gelenYapi.OTOPARKSAYI,
                        OtoparkTip = gelenYapi.OTOPARKTIP,
                        OtoparkVarMi = gelenYapi.OTOPARKVARMI,
                        ParselId = gelenYapi.PARSELID,
                        ParselKimlikNo = gelenYapi.PARSELKIMLIKNO,
                        ResmiDaireMi = gelenYapi.RESMIDAIREMI,
                        SaglikMi = gelenYapi.SAGLIKMI,
                        SanayiMi = gelenYapi.SANAYIMI,
                        SiginakVarMi = gelenYapi.SIGINAKVARMI,
                        SporMu = gelenYapi.SPORMU,
                        SuAboneSicilNo = gelenYapi.SUABONESICILNO,
                        SuDeposuVarMi = gelenYapi.SUDEPOSUVARMI,
                        SuSaatiNo = gelenYapi.SUSAATINO,
                        TahliyePlani = gelenYapi.TAHLIYEPLANI,
                        TahsisNo = gelenYapi.TAHSISNO,
                        TaliGirisNo = gelenYapi.TALIGIRISNO,
                        TaliSokakAdi = gelenYapi.TALISOKAKADI,
                        TicariMi = gelenYapi.TICARIMI,
                        YanginAlgilamaVarMi = gelenYapi.YANGINALGILAMAVARMI,
                        YanginMerdiveniVarMi = gelenYapi.YANGINMERDIVENIVARMI,
                        YanginSondurmeVarMi = gelenYapi.YANGINSONDURMEVARMI,
                        YerlesimPlaniVarMi = gelenYapi.YERLESIMPLANIVARMI,
                        YuruyenMerdivenVarMi = gelenYapi.YURUYENMERDIVENVARMI,
                        TahsisSokak = gelenYapi.TAHSISSOKAK,
                        BinaNotu = gelenYapi.BINANOTU,
                        YapiTipiAciklama = gelenYapi.YAPITIPIACIKLAMA,
                        MahalleId = gelenYapi.MAHALLEID,
                        DegistirmeTarihi = gelenYapi.DEGISTIRMETARIHISTRING,
                        OlusturmaTarihi = gelenYapi.OLUSTURMATARIHISTRING
                    };
                    result = service.YapiKaydet(data, true);
                    if (!result.IsSuccess)
                        throw new Exception(result.OperationMessage);
                    return result.Data;
                }
                else if (item.ItemState == SenkronizasyonOperasyonTipi.Silinmis)
                {
                    YAPI yapi = context.YAPI.FirstOrDefault(t => t.OBJECTID == _ItemId);

                    List<NUMARATAJ> numaratajList = context.NUMARATAJ
                                                            .Where(l => l.YAPIID == yapi.OBJECTID)
                                                            .ToList();

                    List<int> numaratajIdList = numaratajList
                                                    .Select(l => l.OBJECTID)
                                                    .ToList();

                    List<BAGIMSIZBOLUM> bbList = context.BAGIMSIZBOLUM
                                                            .Where(l => l.YAPIID == yapi.OBJECTID ||
                                                                        numaratajIdList.Contains(l.NUMARATAJID ?? 0))
                                                            .ToList();

                    context.BAGIMSIZBOLUM.RemoveRange(bbList);
                    context.NUMARATAJ.RemoveRange(numaratajList);
                    context.YAPI.Remove(yapi);
                    context.SaveChanges();
                    return _ItemId;
                }
                else
                {
                    YapiData data = new YapiData()
                    {
                        Id = _ItemId,
                        Adi = gelenYapi.AD,
                        Durum = gelenYapi.YAPIDURUM,
                        Geometry = gelenYapi.Geometry,
                        Parsel = gelenYapi.PARSEL != null ? gelenYapi.PARSEL.ADANO.ToString() + " " + gelenYapi.PARSEL.PARSELNO : "",
                        PostaKodu = gelenYapi.POSTAKODU,
                        SiteVeyaKooperatifAdi = gelenYapi.SITEVEYAKOOPERATIFADI,
                        Tip = gelenYapi.TIP,
                        ZeminAltiKatSayi = gelenYapi.ZEMINALTIKATSAYISI,
                        ZeminUstuKatSayi = gelenYapi.ZEMINUSTUKATSAYISI,
                        UavtNo = gelenYapi.UAVTNO,
                        UavtEslesmiyor = gelenYapi.UAVTESLESMIYOR,
                        Aciklama = gelenYapi.ACIKLAMA,
                        AcilAsansorSayisi = gelenYapi.ACILASANSORSAYISI,
                        AcilDurumAsansoruVarMi = gelenYapi.ACILDURUMASANSORUVARMI,
                        AcilDurumPlaniVarMi = gelenYapi.ACILDURUMPLANIVARMI,
                        AnonsSistemi = gelenYapi.ANONSSISTEMI,
                        AsansorSayisi = gelenYapi.ASANSORSAYISI,
                        AsansorVarMi = gelenYapi.ASANSORVARMI,
                        BbSayisi = gelenYapi.BBSAYISI,
                        BinaYapimYili = gelenYapi.BINAYAPIMYILI,
                        BinaYasi = gelenYapi.BINAYASI,
                        BodrumSayisi = gelenYapi.BODRUMSAYISI,
                        CatiDurumu = gelenYapi.CATIDURUMU,
                        DigerMi = gelenYapi.DIGERMI,
                        DiniMi = gelenYapi.DINIMI,
                        DogalGazAbone = gelenYapi.DOGALGAZABONE,
                        EgitimMi = gelenYapi.EGITIMMI,
                        ElektrikAbone = gelenYapi.ELEKTRIKABONE,
                        EngelliGirisiVarMi = gelenYapi.ENGELLIGIRISIVARMI,
                        EskiKapiNo = gelenYapi.ESKIKAPINO,
                        FizikselDurum = gelenYapi.FIZIKSELDURUM,
                        InsaatTipi = gelenYapi.INSAATTIPI,
                        IsinmaBilgileri = gelenYapi.ISINMABILGILERI,
                        IsinmaBilgileriMadde = gelenYapi.ISINMABILGILERIMADDE,
                        JeneratorVarMi = gelenYapi.JENERATORVARMI,
                        KatKrokisiVarMi = gelenYapi.KATKROKISIVARMI,
                        KatSayisi = gelenYapi.KATSAYISI,
                        Kaynak = gelenYapi.KAYNAK,
                        KazanDairesiVarMi = gelenYapi.KAZANDAIRESIVARMI,
                        KimlikNo = gelenYapi.KIMLIKNO,
                        KonutVarMi = gelenYapi.KONUTVARMI,
                        KulturelMi = gelenYapi.KULTURELMI,
                        Olcek = gelenYapi.OLCEK,
                        OtoparkSayi = gelenYapi.OTOPARKSAYI,
                        OtoparkTip = gelenYapi.OTOPARKTIP,
                        OtoparkVarMi = gelenYapi.OTOPARKVARMI,
                        ParselId = gelenYapi.PARSELID,
                        ParselKimlikNo = gelenYapi.PARSELKIMLIKNO,
                        ResmiDaireMi = gelenYapi.RESMIDAIREMI,
                        SaglikMi = gelenYapi.SAGLIKMI,
                        SanayiMi = gelenYapi.SANAYIMI,
                        SiginakVarMi = gelenYapi.SIGINAKVARMI,
                        SporMu = gelenYapi.SPORMU,
                        SuAboneSicilNo = gelenYapi.SUABONESICILNO,
                        SuDeposuVarMi = gelenYapi.SUDEPOSUVARMI,
                        SuSaatiNo = gelenYapi.SUSAATINO,
                        TahliyePlani = gelenYapi.TAHLIYEPLANI,
                        TahsisNo = gelenYapi.TAHSISNO,
                        TaliGirisNo = gelenYapi.TALIGIRISNO,
                        TaliSokakAdi = gelenYapi.TALISOKAKADI,
                        TicariMi = gelenYapi.TICARIMI,
                        YanginAlgilamaVarMi = gelenYapi.YANGINALGILAMAVARMI,
                        YanginMerdiveniVarMi = gelenYapi.YANGINMERDIVENIVARMI,
                        YanginSondurmeVarMi = gelenYapi.YANGINSONDURMEVARMI,
                        YerlesimPlaniVarMi = gelenYapi.YERLESIMPLANIVARMI,
                        YuruyenMerdivenVarMi = gelenYapi.YURUYENMERDIVENVARMI,
                        TahsisSokak = gelenYapi.TAHSISSOKAK,
                        BinaNotu = gelenYapi.BINANOTU,
                        YapiTipiAciklama = gelenYapi.YAPITIPIACIKLAMA,
                        MahalleId = gelenYapi.MAHALLEID,
                        DegistirmeTarihi = gelenYapi.DEGISTIRMETARIHISTRING,
                        OlusturmaTarihi = gelenYapi.OLUSTURMATARIHISTRING
                    };
                    result = service.YapiKaydet(data, true);
                    if (!result.IsSuccess)
                    {
                        throw new Exception(result.OperationMessage);
                    }
                    return _ItemId;
                }
            }
        }

        public override void SaveMedia(int medyaId)
        {
            using (var ctx = new NumaratajContext())
            using (var tr = ctx.Database.BeginTransaction(IsolationLevel.ReadUncommitted))
            {
                if (!ctx.YAPI_MEDYA.Where(t => t.MEDYAID == medyaId).Any())
                {
                    ctx.Database.ExecuteSqlCommand(string.Format("insert into YAPI_MEDYA select {0},{1}", medyaId, _ItemId));
                    //ctx.YAPI_MEDYA.Add(new YAPI_MEDYA()
                    //{
                    //    YAPIID = _ItemId,
                    //    MEDYAID = medyaId
                    //});
                }
                tr.Commit();
            }
        }

        public override List<int> GetNesneMedya()
        {
            List<int> result = null;
            using (NumaratajContext ctx = new NumaratajContext())
            {
                result = ctx.YAPI_MEDYA.Where(t => t.YAPIID == _ItemId).Select(a => a.MEDYAID).ToList();
            }
            return result;
        }

        public override void NotKaydet(NotData not)
        {
            if (not.ServerId != null)
            {
                using (var ctx = new NumaratajContext())
                using (var tr = ctx.Database.BeginTransaction(IsolationLevel.ReadUncommitted))
                {
                    if (!ctx.YAPI_NOT.Where(t => t.NOTID == not.NotId).Any())
                    {
                        ctx.Database.ExecuteSqlCommand(string.Format("insert into YAPI_NOT (YAPIID,NOTID) VALUES ({0},{1})", not.ServerId.Value, not.NotId));
                        //ctx.YAPI_MEDYA.Add(new YAPI_MEDYA()
                        //{
                        //    YAPIID = _ItemId,
                        //    MEDYAID = medyaId
                        //});
                        ctx.SaveChanges();
                    }

                    //ctx.YAPI_NOT.Add(new YAPI_NOT()
                    //{
                    //    YAPIID = not.ServerId.Value,
                    //    NOTID = not.NotId
                    //});
                    //ctx.SaveChanges();
                    tr.Commit();
                }
            }
        }

        public override string GetCentroid(ChangeSetItem item)
        {
            YAPI gelenYAPI = JsonConvert.DeserializeObject<YAPI>(item.Item);
            DbGeometry nesneGeometry;
            if (!string.IsNullOrWhiteSpace(gelenYAPI.Geometry))
            {
                try
                {
                    nesneGeometry = DbGeometry.FromText(gelenYAPI.Geometry, 4326);
                    if (!nesneGeometry.IsValid)
                    {
                        nesneGeometry = DbGeometry.FromText(SqlGeometry.STGeomFromText(new SqlChars(nesneGeometry.AsText()), 4326).MakeValid().STAsText().ToSqlString().ToString(), 4326);
                    }
                }
                catch (Exception ex)
                {
                    return null;
                }

                DbGeometry hataGeometry = null;
                if (nesneGeometry.SpatialTypeName.ToLower() == "polygon")
                {
                    hataGeometry = nesneGeometry.Centroid;
                }
                else if (nesneGeometry.SpatialTypeName.ToLower() == "linestring")
                {
                    double startX, startY, endX, endY, middeleX, middeleY;
                    int pointCount = nesneGeometry.PointCount.Value;
                    if (pointCount % 2 == 0)
                    {
                        startX = nesneGeometry.PointAt(pointCount / 2).XCoordinate.Value;
                        startY = nesneGeometry.PointAt(pointCount / 2).YCoordinate.Value;
                        endX = nesneGeometry.PointAt((pointCount / 2) + 1).XCoordinate.Value;
                        endY = nesneGeometry.PointAt((pointCount / 2) + 1).YCoordinate.Value;
                        middeleX = (startX + endX) / 2;
                        middeleY = (startY + endY) / 2;

                        hataGeometry = DbGeometry.PointFromText(string.Format("POINT({0} {1})", middeleX.ToString(CultureInfo.InvariantCulture), middeleY.ToString(CultureInfo.InvariantCulture)), 4326);
                    }
                    else
                    {
                        int middlePointIndex = Convert.ToInt32(Math.Ceiling((decimal)(pointCount / 2)));
                        hataGeometry = nesneGeometry.PointAt(middlePointIndex);
                    }
                }
                else if (nesneGeometry.SpatialTypeName.ToLower() == "point")
                {
                    hataGeometry = nesneGeometry;
                }
                else
                {
                    return null;
                }
                return hataGeometry.AsText();
            }
            else
            {
                return null;
            }
        }
    }
}