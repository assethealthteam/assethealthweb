﻿using Newtonsoft.Json;
using OdakGIS.Framework.Core.Entities;
using OdakGIS.Numarataj.Data;
using OdakGIS.Numarataj.Data.Entities;
using OdakGIS.Sync.ChangeManagement;
using OdakGIS.Sync.Entities.Custom;
using OdakGIS.Sync.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Spatial;
using System.Linq;

namespace OdakGIS.Numarataj.Shared.ChangeManagement
{
    public class MahalleChangeSetItemFactory : ChangeSetItemFactory<MAHALLE, MAHALLE>
    {
        public MahalleChangeSetItemFactory(int itemId, int cihazId, SenkronizasyonOperasyonTipi itemState)
            : base(itemId, cihazId, itemState)
        {

        }
        protected override OperationResult Validate(MAHALLE item)
        {
            return new OperationResult()
            {
                IsSuccess = true
            };
        }

        protected override MAHALLE PrepareForSynchronization(int itemId)
        {
            MAHALLE mahalle = null;
            using (NumaratajContext context = new NumaratajContext())
            using (var tr = context.Database.BeginTransaction(IsolationLevel.ReadUncommitted))
            {
                mahalle = context.MAHALLE.FirstOrDefault(t => t.OBJECTID == itemId);
                mahalle.Geometry = mahalle.SHAPE.AsText();
                mahalle.ID = mahalle.OBJECTID;
            }
            return mahalle;
        }

        public override int SaveChangeSetItem(ChangeSetItem item)
        {
            int result = 0;
            NUMARATAJ gelenNumarataj = JsonConvert.DeserializeObject<NUMARATAJ>(item.Item);
            NUMARATAJ sistemdekiNumarataj = null, yeniNumarataj = null;
            using (NumaratajContext context = new NumaratajContext())
            {
                if (_ItemId != 0)
                {
                    result = _ItemId;
                    sistemdekiNumarataj = context.NUMARATAJ.FirstOrDefault(t => t.OBJECTID == _ItemId);
                    if (item.ItemState == SenkronizasyonOperasyonTipi.Degismis)
                    {
                        sistemdekiNumarataj.SHAPE = DbGeometry.FromText(gelenNumarataj.Geometry, 4326);
                        sistemdekiNumarataj.KAPINO = gelenNumarataj.KAPINO;
                        sistemdekiNumarataj.AD = gelenNumarataj.AD;
                    }
                    //else if (item.ItemState == SenkronizasyonOperasyonTipi.Silinmis)
                    //{
                    //    context.YAPI.Remove(sistemdekiYapi);
                    //}
                    //sistemdekiYapi.OBJECTID = gelenYapi.ID;
                }
                else //if (item.ItemState == SenkronizasyonOperasyonTipi.Yeni)
                {
                    yeniNumarataj = new NUMARATAJ()
                    {
                        KAPINO = gelenNumarataj.KAPINO,
                        AD = gelenNumarataj.AD,
                        SHAPE = DbGeometry.FromText(gelenNumarataj.Geometry, 4326)
                    };
                    context.NUMARATAJ.Add(yeniNumarataj);
                }
                context.SaveChanges();
                if (yeniNumarataj != null)
                    result = yeniNumarataj.OBJECTID;
            }
            return result;
        }

        public override void SaveMedia(int medyaId)
        {
            throw new NotImplementedException();
        }

        public override List<int> GetNesneMedya()
        {
            throw new NotImplementedException();
        }
        public override void NotKaydet(NotData not)
        {
        }

        public override string GetCentroid(ChangeSetItem item)
        {
            return null;
        }
    }
}
