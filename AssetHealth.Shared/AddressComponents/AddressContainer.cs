﻿using OdakGIS.Numarataj.Shared.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace OdakGIS.Numarataj.Shared.AddressComponents
{
    [DataContract]
    public class AddressContainer
    {
        public AddressContainer()
        {
        }

        public AddressContainer Clone()
        {
            if (Object.ReferenceEquals(this, null))
            {
                return new AddressContainer();
            }

            AddressContainer oClone = new AddressContainer();

            DataContractSerializer dcs = new DataContractSerializer(this.GetType());
            using (MemoryStream ms = new MemoryStream())
            {
                dcs.WriteObject(ms, this);
                ms.Position = 0;
                oClone = (AddressContainer)dcs.ReadObject(ms);

            }

            return oClone;
        }

        private List<BagimsizBolum> bagimsizBolumList;
        [DataMember]
        public List<BagimsizBolum> BagimsizBolumList
        {
            get
            {
                if (bagimsizBolumList == null)
                    bagimsizBolumList = new List<BagimsizBolum>();
                return bagimsizBolumList;
            }
            set { bagimsizBolumList = value; }
        }

        private Bina bina;
        [DataMember]
        public Bina Bina
        {
            get
            {
                if (bina == null)
                    bina = new Bina();
                return bina;
            }
            set { bina = value; }
        }

        private Numarataj numarataj;
        [DataMember]
        public Numarataj Numarataj
        {
            get
            {
                if (numarataj == null)
                    numarataj = new Numarataj();
                return numarataj;
            }
            set { numarataj = value; }
        }

        private Csbm csbm;
        [DataMember]
        public Csbm Csbm
        {
            get
            {
                if (csbm == null)
                    csbm = new Csbm();
                return csbm;
            }
            set { csbm = value; }
        }

        private Mahalle mahalle;
        [DataMember]
        public Mahalle Mahalle
        {
            get
            {
                if (mahalle == null)
                    mahalle = new Mahalle();
                return mahalle;
            }
            set { mahalle = value; }
        }

        private Koy koy;
        [DataMember]
        public Koy Koy
        {
            get
            {
                if (koy == null)
                    koy = new Koy();
                return koy;
            }
            set { koy = value; }
        }

        private Bucak bucak;
        [DataMember]
        public Bucak Bucak
        {
            get
            {
                if (bucak == null)
                    bucak = new Bucak();
                return bucak;
            }
            set { bucak = value; }
        }

        private Ilce ilce;
        [DataMember]
        public Ilce Ilce
        {
            get
            {
                if (ilce == null)
                    ilce = new Ilce();
                return ilce;
            }
            set { ilce = value; }
        }

        private Il il;
        [DataMember]
        public Il Il
        {
            get
            {
                if (il == null)
                    il = new Il();
                return il;
            }
            set { il = value; }
        }

        private YetkiliIdare yetkiliIdare;
        [DataMember]
        public YetkiliIdare YetkiliIdare
        {
            get
            {
                if (yetkiliIdare == null)
                    yetkiliIdare = new YetkiliIdare();
                return yetkiliIdare;
            }
            set { yetkiliIdare = value; }
        }

        private Belediye belediye;
        [DataMember]
        public Belediye Belediye
        {
            get
            {
                if (belediye == null)
                    belediye = new Belediye();
                return belediye;
            }
            set { belediye = value; }
        }

        private string acikAdres;
        [DataMember]
        public string AcikAdres
        {
            get
            {
                if (string.IsNullOrWhiteSpace(acikAdres) == false)
                    return acikAdres;

                if (BindEdildi == false)
                    return null;

                acikAdres = AcikAdresUret();

                if (string.IsNullOrWhiteSpace(acikAdres))
                    return null;
                else
                    return acikAdres;
            }
            set
            {
                acikAdres = value;
            }
        }

        public string AcikAdresUret()
        {
            StringBuilder stringBuilder = new StringBuilder();

            if (Bucak.Id.HasValue && Bucak.IsMerkez != true)
                stringBuilder.Append(" ").Append(Bucak.Ad).Append(" BUCAĞI");

            if (Koy.Id.HasValue && Koy.IsMerkez != true)
                stringBuilder.Append(" ").Append(Koy.Ad).Append(" KÖYÜ");

            if (Mahalle.Id.HasValue)
                stringBuilder.Append(" ").Append(Mahalle.UzunAd);

            if (Csbm.Id.HasValue)
                stringBuilder.Append(" ").Append(Csbm.Ad).Append(" ").Append(Csbm.TipAdKisa);

            if (Bina.Id.HasValue)
                stringBuilder.Append(" Bina No:").Append(Bina.Ad);
            else if (Numarataj.Id.HasValue)
                stringBuilder.Append(" Bina No:").Append(Numarataj.Ad);

            string bbler = "";
            foreach (BagimsizBolum bb in BagimsizBolumList)
            {
                if (bb.IsSelected)
                    if (string.IsNullOrEmpty(bbler))
                        bbler = bb.Ad;
                    else
                        bbler = string.Concat(bbler, ",", bb.Ad);
            }
            if (string.IsNullOrWhiteSpace(bbler) == false)
                stringBuilder.Append(" İç Kapı No:").Append(bbler);

            if (Ilce.Id.HasValue)
                stringBuilder.Append(" ").Append(Ilce.Ad);

            if (Il.Id.HasValue)
                stringBuilder.Append("/").Append(Il.Ad);

            string cevap = stringBuilder.ToString().Trim();

            if (string.IsNullOrWhiteSpace(cevap))
                return null;
            else
                return cevap;
        }

        public string AcikAdresUret2()
        {
            StringBuilder stringBuilder = new StringBuilder();

            if (string.IsNullOrWhiteSpace(Bucak.Ad) == false && Bucak.IsMerkez == false)
                stringBuilder.Append(" ").Append(Bucak.Ad).Append(" BUCAĞI");

            if (string.IsNullOrEmpty(Koy.Ad) == false && Koy.IsMerkez != true)
                stringBuilder.Append(" ").Append(Koy.Ad).Append(" KÖYÜ");

            if (string.IsNullOrEmpty(Mahalle.Ad) == false)
                stringBuilder.Append(" ").Append(Mahalle.Ad).Append(" MAH.");

            if (string.IsNullOrEmpty(Csbm.Ad) == false)
                stringBuilder.Append(" ").Append(Csbm.Ad).Append(" ").Append(Csbm.TipAdKisa);

            if (Bina.Id.HasValue)
                stringBuilder.Append(" No:").Append(Bina.Ad);
            else if (Numarataj.Id.HasValue)
                stringBuilder.Append(" No:").Append(Numarataj.Ad);

            string bbler = "";
            foreach (BagimsizBolum bb in BagimsizBolumList)
            {
                if (bb.IsSelected)
                    if (string.IsNullOrEmpty(bbler))
                        bbler = bb.Ad;
                    else
                        bbler = string.Concat(bbler, ",", bb.Ad);
            }
            if (string.IsNullOrEmpty(bbler) == false)
                stringBuilder.Append(" ").Append(bbler);

            if (string.IsNullOrEmpty(Ilce.Ad) == false)
                stringBuilder.Append(" ").Append(Ilce.Ad);

            if (string.IsNullOrEmpty(Il.Ad) == false)
                stringBuilder.Append("/").Append(Il.Ad);

            string cevap = stringBuilder.ToString().Trim();

            if (string.IsNullOrWhiteSpace(cevap))
                return null;
            else
                return cevap;
        }

        public string AcikAdresUretAdaParsel()
        {
            StringBuilder stringBuilder = new StringBuilder();

            if (Bucak.Id.HasValue && Bucak.IsMerkez == false)
                stringBuilder.Append(" ").Append(Bucak.Ad).Append(" BUCAĞI");

            if (Koy.Id.HasValue && Koy.IsMerkez == false)
                stringBuilder.Append(" ").Append(Koy.Ad).Append(" KÖYÜ");

            if (Mahalle.Id.HasValue)
                stringBuilder.Append(" ").Append(Mahalle.Ad).Append(" MAH.");

            if (Csbm.Id.HasValue)
                stringBuilder.Append(" ").Append(Csbm.Ad).Append(" ").Append(Csbm.TipAdKisa);

            if (String.IsNullOrWhiteSpace(Numarataj.Ad) == false)
                stringBuilder.Append(" No: ").Append(Numarataj.Ad);
            else if (Bina.Id.HasValue && String.IsNullOrWhiteSpace(Bina.Ad) == false)
                stringBuilder.Append(" No: ").Append(Bina.Ad);

            string bbler = "";
            foreach (BagimsizBolum bb in BagimsizBolumList)
            {
                if (bb.IsSelected)
                    if (string.IsNullOrEmpty(bbler))
                        bbler = bb.Ad;
                    else
                        bbler = string.Concat(bbler, ",", bb.Ad);
            }
            if (string.IsNullOrEmpty(bbler) == false)
                stringBuilder.Append(" ").Append(bbler);

            stringBuilder.Append(" Ada: ").Append(Bina.Ada).Append(" Parsel: ").Append(Bina.Parsel);

            if (Ilce.Id.HasValue)
                stringBuilder.Append(" ").Append(Ilce.Ad);

            if (Il.Id.HasValue)
                stringBuilder.Append("/").Append(Il.Ad);

            string cevap = stringBuilder.ToString().Trim();

            if (string.IsNullOrWhiteSpace(cevap))
                return null;
            else
                return cevap;
        }

        private bool bilesenlerOkundu;
        [DataMember]
        public bool BilesenlerOkundu
        {
            get { return bilesenlerOkundu; }
            set { bilesenlerOkundu = value; }
        }

        private bool bindEdildi;
        [DataMember]
        public bool BindEdildi
        {
            get { return bindEdildi; }
            set { bindEdildi = value; }
        }

        public bool IsNull
        {
            get
            {
                return (BagimsizBolumList == null || (bagimsizBolumList != null && BagimsizBolumList.Count() == 0))
                    && Bina.Id.HasValue == false
                    && Csbm.Id.HasValue == false
                    && Mahalle.Id.HasValue == false
                    && Ilce.Id.HasValue == false
                    && Il.Id.HasValue == false;
            }
        }

        /// <summary>
        /// Verilen seviye ve üstü bileşenler için dolu olup olmadıklarını kontrol eder
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public string IsFull(AddressComponentTypes level)
        {
            string result = null;

            if (level >= AddressComponentTypes.Il)
            {
                if (string.IsNullOrWhiteSpace(Il.Ad) || Il.Id == null || Il.Id == 0)
                    result = "il";
            }

            if (level >= AddressComponentTypes.Ilce)
            {
                if (string.IsNullOrWhiteSpace(Ilce.Ad) || Ilce.Id == null || Ilce.Id == 0)
                    result = string.Join(" ,", result, "ilçe");
            }

            if (level >= AddressComponentTypes.Mahalle)
            {
                if ((string.IsNullOrWhiteSpace(Mahalle.Ad) || Mahalle.Id == null || Mahalle.Id == 0)
                    && (string.IsNullOrWhiteSpace(Koy.Ad) || Koy.Id == null || Koy.Id == 0))
                    result = string.Join(" ,", result, "mahalle ya da köy");



                //todo: servis getirince alttaki comment kaldırılacak Onur
                //if (Mahalle.Tipi == null)
                //    result = string.Join(" ,", result, "mahalle tipi");
            }

            if (level >= AddressComponentTypes.CSBM)
            {
                if (string.IsNullOrWhiteSpace(Csbm.Ad) || Csbm.Id == null || Csbm.Id == 0)
                    result = string.Join(" ,", result, "CSBM");

                //todo: servis getirince alttaki comment kaldırılacak Onur
                //if (Csbm.Tipi == null)
                //    result = string.Join(" ,", result, "CSBM tipi");
            }

            if (level >= AddressComponentTypes.Bina)
            {
                if (Bina.Id == null || Bina.Id == 0)
                    result = string.Join(" ,", result, "yapı");
            }

            if (level >= AddressComponentTypes.Numarataj)
            {
                if (string.IsNullOrWhiteSpace(Numarataj.Ad) || Numarataj.Id == null || Numarataj.Id == 0)
                    result = string.Join(" ,", result, "numarataj");
            }

            if (level >= AddressComponentTypes.BagimsizBolum)
            {
                if (BagimsizBolumList == null || BagimsizBolumList.Count == 0)
                {
                    result = string.Join(" ,", "bağımsız bölüm");
                }
                else
                {
                    foreach (BagimsizBolum bb in BagimsizBolumList)
                    {
                        if (bb.Id == null || bb.Id == 0)
                            result = string.Join(" ,", "bağımsız bölüm");
                    }
                }
            }

            return result;
        }
    }
}
