﻿using OdakGIS.Numarataj.Shared.Enums;

namespace OdakGIS.Numarataj.Shared.AddressComponents
{
    public class YolOrtaHat : BaseAddressComponent
    {
        #region Properties

        private AddressComponentTypes addressComponentType;

        public override AddressComponentTypes AddressComponentType
        {
            get { return AddressComponentTypes.YolOrtaHat; }
            set { addressComponentType = value; }
        }

        public long? YolKimlikNo { get; set; }

        #endregion

        #region Methods

        #endregion

    }
}
