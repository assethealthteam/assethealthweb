﻿using OdakGIS.Numarataj.Shared.Enums;
using OdakGIS.Numarataj.Shared.Enums.Maks;
using System.Runtime.Serialization;

namespace OdakGIS.Numarataj.Shared.AddressComponents
{
    public class Mahalle : BaseAddressComponent
    {
        #region Properties

        [DataMember]
        public int? SabitTanitimNumarasi { get; set; }

        [DataMember]
        public Parametre Tipi { get; set; }

        [DataMember]
        public long? KoyKod { get; set; }

        [DataMember]
        public int? YetkiliIdareKodu { get; set; }

        public string UzunAd
        {
            get
            {
                if (Tipi == null)
                    return string.Concat(Ad, " MAH.");

                EnmMahalleTip mahalleTip = (EnmMahalleTip)Tipi.Kod;
                string sonuc = "";
                switch (mahalleTip)
                {
                    case EnmMahalleTip.NA:
                        break;
                    case EnmMahalleTip.BelediyeMahallesi:
                        sonuc = string.Concat(Ad, " MAH.");
                        break;
                    case EnmMahalleTip.Mezra:
                        sonuc = string.Concat(Ad, " MEZRASI");
                        break;
                    case EnmMahalleTip.Mevkii:
                        sonuc = string.Concat(Ad, " MEVKİİ");
                        break;
                    case EnmMahalleTip.YaylaEvleri:
                        sonuc = string.Concat(Ad, " YAYLA EVLERİ");
                        break;
                    case EnmMahalleTip.Diger:
                        break;
                    default:
                        break;
                }

                return sonuc;
            }
        }

        private AddressComponentTypes addressComponentType;

        public override AddressComponentTypes AddressComponentType
        {
            get { return AddressComponentTypes.Mahalle; }
            set { addressComponentType = value; }
        }

        #endregion

        #region Methods

        #endregion
    }
}
