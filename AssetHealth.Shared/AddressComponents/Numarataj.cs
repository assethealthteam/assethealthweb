﻿using OdakGIS.Numarataj.Shared.Enums;

namespace OdakGIS.Numarataj.Shared.AddressComponents
{
    public class Numarataj : BaseAddressComponent
    {
        #region Properties

        private AddressComponentTypes addressComponentType;

        public override AddressComponentTypes AddressComponentType
        {
            get { return AddressComponentTypes.Numarataj; }
            set { addressComponentType = value; }
        }

        public long? YolOrtaHatYonKimlikNo { get; set; }

        #endregion

        #region Methods

        #endregion

    }
}
