﻿using OdakGIS.Numarataj.Shared.Enums;
using System;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;

namespace OdakGIS.Numarataj.Shared.AddressComponents
{
    public class Bina : BaseAddressComponent
    {
        #region Properties

        private AddressComponentTypes addressComponentType;

        public override AddressComponentTypes AddressComponentType
        {
            get { return AddressComponentTypes.Bina; }
            set { addressComponentType = value; }
        }

        [DataMember]
        public string Ada { get; set; }

        [DataMember]
        public long? CsbmKod { get; set; }

        [DataMember]
        public string SiteAd { get; set; }
        [DataMember]
        public string BlokAd { get; set; }
        [DataMember]
        public long? EsBinaKod { get; set; }
        [DataMember]
        public int? EsBinaKimlikNo { get; set; }
        public Parametre Nitelik { get; set; }
        public Parametre BinaDurum { get; set; }
        [DataMember]
        public string Pafta { get; set; }
        [DataMember]
        public string Parsel { get; set; }
        [DataMember]
        public string PostaKod { get; set; }
        [DataMember]
        public int? YolKotuAltKatSayi { get; set; }
        [DataMember]
        public int? YolKotuUstuKatSayi { get; set; }
        [DataMember]
        public Parametre BinaYapiTipi { get; set; }
        [DataMember]
        public Parametre BinaNumaratajTipi { get; set; }
        public bool? IsDigerYapi { get; set; }

        [DataMember]
        public string DisKapiNo
        {
            get
            {
                return string.IsNullOrEmpty(Ad) ? "" : FixKapiNo(Ad);
            }
        }

        [DataMember]
        public long? Kod { get; set; }
        //public int? YolAltiKatSayisi { get; set; }
        //public int? YolUstuKatSayisi { get; set; }

        #endregion

        #region Methods
        private static readonly Regex regex = new Regex("^(\\d*)([ ]*)([/-]?)(\\d*)(.*)");

        protected static string FixKapiNo(string disKapiNo)
        {
            disKapiNo = disKapiNo.Replace(Environment.NewLine, string.Empty);

            StringBuilder builder = new StringBuilder();

            Match match = regex.Match(disKapiNo);

            builder.Append(match.Groups[1].Value);

            bool hasSeperator = false;
            if (string.IsNullOrEmpty(match.Groups[3].Value) == false)
            {
                builder.Append(match.Groups[3].Value);

                hasSeperator = true;
            }

            if (string.IsNullOrEmpty(match.Groups[4].Value) == false)
            {
                if (hasSeperator == false)
                {
                    builder.Append(" ");
                }

                builder.Append(match.Groups[4].Value);
            }

            if (string.IsNullOrEmpty(match.Groups[5].Value) == false)
            {
                builder.Append(match.Groups[5].Value);
            }

            return builder.ToString();
        }
        #endregion
    }
}
