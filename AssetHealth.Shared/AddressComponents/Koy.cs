﻿using OdakGIS.Numarataj.Shared.Enums;

namespace OdakGIS.Numarataj.Shared.AddressComponents
{
    public class Koy : BaseAddressComponent
    {
        #region Properties

        private AddressComponentTypes addressComponentType;

        public override AddressComponentTypes AddressComponentType
        {
            get { return AddressComponentTypes.Koy; }
            set { addressComponentType = value; }
        }

        public long? BucakKod { get; set; }

        public bool? IsMerkez { get; set; }

        #endregion

        #region Methods

        #endregion

    }
}
