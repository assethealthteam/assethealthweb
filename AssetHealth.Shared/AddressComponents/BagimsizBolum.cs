﻿using OdakGIS.Numarataj.Shared.Enums;
using OdakGIS.Numarataj.Shared.Enums.Maks;

namespace OdakGIS.Numarataj.Shared.AddressComponents
{
    public class BagimsizBolum : BaseAddressComponent
    {
        #region Properties

        private AddressComponentTypes addressComponentType;

        public override AddressComponentTypes AddressComponentType
        {
            get { return AddressComponentTypes.BagimsizBolum; }
            set { addressComponentType = value; }
        }

        public long? BinaKimlikNo { get; set; }

        public long? AnaBinaKimlikNo { get; set; }

        public Parametre Nitelik { get; set; }

        public string KatNo { get; set; }

        public EnumBagimsizBolumDurum? BBDurum { get; set; }

        public EnumBagimsizBolumTip? BBTip { get; set; }

        public int? YapiKullanimAmac { get; set; }

        #endregion
    }
}
