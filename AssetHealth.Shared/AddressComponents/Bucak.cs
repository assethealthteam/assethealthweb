﻿using OdakGIS.Numarataj.Shared.Enums;

namespace OdakGIS.Numarataj.Shared.AddressComponents
{
    public class Bucak : BaseAddressComponent
    {
        #region Properties

        public long? IlceKod { get; set; }

        private AddressComponentTypes addressComponentType;

        public override AddressComponentTypes AddressComponentType
        {
            get { return AddressComponentTypes.Bucak; }
            set { addressComponentType = value; }
        }

        public bool? IsMerkez { get; set; }

        #endregion

        #region Methods

        #endregion

    }
}
