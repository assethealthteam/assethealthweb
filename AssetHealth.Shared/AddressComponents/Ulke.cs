﻿using OdakGIS.Numarataj.Shared.Enums;
using System.Runtime.Serialization;

namespace OdakGIS.Numarataj.Shared.AddressComponents
{
    [DataContract]
    public class Ulke : BaseAddressComponent
    {
        #region Properties

        private AddressComponentTypes addressComponentType;

        [DataMember]
        public override AddressComponentTypes AddressComponentType
        {
            get { return AddressComponentTypes.Ulke; }
            set { addressComponentType = value; }
        }

        #endregion

        #region Methods

        #endregion

    }
}
