﻿using OdakGIS.Numarataj.Shared.Enums;
using System.Runtime.Serialization;

namespace OdakGIS.Numarataj.Shared.AddressComponents
{
    [DataContract]
    public class Ilce : BaseAddressComponent
    {
        #region Properties

        [DataMember]
        public int? IlKod { get; set; }

        #endregion

        #region Overrides

        private AddressComponentTypes addressComponentType;

        [DataMember]
        public override AddressComponentTypes AddressComponentType
        {
            get { return AddressComponentTypes.Ilce; }
            set { addressComponentType = value; }
        }

        #endregion

        #region Methods

        #endregion

    }
}
