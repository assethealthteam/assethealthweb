﻿using OdakGIS.Numarataj.Shared.Enums;

namespace OdakGIS.Numarataj.Shared.AddressComponents
{
    public class Belediye : BaseAddressComponent
    {
        public override AddressComponentTypes AddressComponentType
        {
            get
            {
                return AddressComponentTypes.Belediye;
            }
            set { }
        }

        private Ilce ilce;
        public Ilce Ilce
        {
            get
            {
                if (ilce == null)
                    ilce = new Ilce();
                return ilce;
            }
            set { ilce = value; }
        }

        private Il il;
        public Il Il
        {
            get
            {
                if (il == null)
                    il = new Il();
                return il;
            }
            set { il = value; }
        }

        private Koy koy;
        public Koy Koy
        {
            get
            {
                if (koy == null)
                    koy = new Koy();

                return koy;
            }
            set { koy = value; }
        }

    }
}
