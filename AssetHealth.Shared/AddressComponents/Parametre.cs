﻿using System;
using System.Runtime.Serialization;

namespace OdakGIS.Numarataj.Shared.AddressComponents
{
    [DataContract]
#if !SILVERLIGHT
    [Serializable]
#endif

    public class Parametre
    {
        [DataMember]
        public int? Kod { get; set; }

        [DataMember]
        public string Aciklama { get; set; }

        public static Parametre LoadFromAksParametre(object aksParametre)
        {
            try
            {
                if (aksParametre != null)
                {
                    Parametre parametre = new Parametre();

                    //parametre.Kod = (int?)aksParametre.GetType().InvokeMember("Kod", BindingFlags.Instance | BindingFlags.GetProperty | BindingFlags.NonPublic, null, aksParametre, null);
                    //parametre.Aciklama = (string)aksParametre.GetType().InvokeMember("Aciklama", BindingFlags.Instance | BindingFlags.GetProperty | BindingFlags.NonPublic, null, aksParametre, null);

                    parametre.Kod = (int?)aksParametre.GetType().GetProperty("Kod").GetValue(aksParametre, null);
                    parametre.Aciklama = (string)aksParametre.GetType().GetProperty("Aciklama").GetValue(aksParametre, null);
                    return parametre;
                }
                else
                {
                    return new Parametre();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
