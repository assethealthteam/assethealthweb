﻿using OdakGIS.Numarataj.Shared.Enums;

namespace OdakGIS.Numarataj.Shared.AddressComponents
{
    public class Il : BaseAddressComponent
    {
        #region Properties

        private AddressComponentTypes addressComponentType;

        public override AddressComponentTypes AddressComponentType
        {
            get { return AddressComponentTypes.Il; }
            set { addressComponentType = value; }
        }

        #endregion

        #region Methods

        #endregion

    }
}