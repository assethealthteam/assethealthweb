﻿using OdakGIS.Numarataj.Shared.Enums;
using System.Runtime.Serialization;

namespace OdakGIS.Numarataj.Shared.AddressComponents
{
    [DataContract]
    public class DisTemsilcilikComponent : BaseAddressComponent
    {
        #region Properties

        [DataMember]
        public int? UlkeKod { get; set; }

        private AddressComponentTypes addressComponentType;

        [DataMember]
        public override AddressComponentTypes AddressComponentType
        {
            get { return AddressComponentTypes.DisTemsilcilik; }
            set { addressComponentType = value; }
        }

        #endregion

        #region Methods

        #endregion

    }
}
