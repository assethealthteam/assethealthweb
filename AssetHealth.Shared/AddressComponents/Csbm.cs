﻿using OdakGIS.Numarataj.Shared.Enums;
using System.Runtime.Serialization;

namespace OdakGIS.Numarataj.Shared.AddressComponents
{
    public class Csbm : BaseAddressComponent
    {
        #region Properties

        private AddressComponentTypes addressComponentType;

        public override AddressComponentTypes AddressComponentType
        {
            get { return AddressComponentTypes.CSBM; }
            set { addressComponentType = value; }
        }

        #endregion

        [DataMember]
        public long? MahalleKod { get; set; }

        [DataMember]
        public decimal? SabitTanitimNumarasi { get; set; }

        [DataMember]
        public Parametre Tipi { get; set; }

        //0 KÖY SOKAĞI
        //1	MEYDAN
        //2	BULVAR
        //3	CADDE
        //4	SOKAK
        //5	KÜME EVLER
        [DataMember]
        public string TipAdKisa
        {
            get
            {
                if (Tipi == null)
                    return null;

                switch (Tipi.Kod)
                {
                    case 3:
                        return "CAD.";
                    case 4:
                        return "SOK.";
                    default:
                        return Tipi.Aciklama;
                }
            }
        }

        //Default_DoNotUse = 0,
        //Sokak = 1,
        //Cadde = 2,
        //Bulvar = 3,
        //Meydan = 4,
        //KumeEvler = 5,
        //Karayolu = 6,
        //Otoyol = 7,
        //KoyBaglantiYolu = 8,
        [DataMember]
        public string TipAdKisGIS
        {
            get
            {
                if (Tipi == null)
                    return null;

                switch (Tipi.Kod)
                {
                    case 0:
                        return "";
                    case 1:
                        return "SOK.";
                    case 2:
                        return "CAD.";
                    case 3:
                        return "BUL.";
                    case 4:
                        return "MEY.";
                    case 5:
                        return "KÜME EVLERİ";
                    case 6:
                        return "KARAYOLU";
                    case 7:
                        return "OTOYOLU";
                    case 8:
                        return "KÖY BAĞLANTI YOLU";
                    default:
                        return "";
                }
            }
        }

        [DataMember]
        public string MahalleCsbmAd
        {
            get
            {
                if (Parent != null && Parent.Id.HasValue)
                    return string.Concat(Parent.Ad, " MAH. ", Ad, " ", TipAdKisa);
                else
                    return Ad;
            }
        }

        #region Methods

        #endregion

    }
}
