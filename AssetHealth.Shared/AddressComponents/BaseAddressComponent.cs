﻿using OdakGIS.Numarataj.Shared.Enums;
using OdakGIS.Numarataj.Shared.Interfaces.Maks;
using System.Runtime.Serialization;

namespace OdakGIS.Numarataj.Shared.AddressComponents
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(Il))]
    [KnownType(typeof(Ilce))]
    [KnownType(typeof(Bucak))]
    [KnownType(typeof(Koy))]
    [KnownType(typeof(Mahalle))]
    [KnownType(typeof(Csbm))]
    [KnownType(typeof(Bina))]
    [KnownType(typeof(BagimsizBolum))]
    [KnownType(typeof(Ulke))]
    [KnownType(typeof(DisTemsilcilikComponent))]
    public abstract class BaseAddressComponent : IAddressComponent
    {
        #region Fields

        #endregion

        #region Properties

        [DataMember]
        public long? Id { get; set; }

        [DataMember]
        public string Ad { get; set; }

        public IAddressComponent Parent { get; set; }


        [DataMember]
        public bool IsSelected { get; set; }

        #endregion

        #region Abstraction

        [DataMember]
        public abstract AddressComponentTypes AddressComponentType { get; set; }

        #endregion

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            try
            {
                return ((BaseAddressComponent)obj).Id == Id;
            }
            catch
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return string.Format("{0}_{1}", AddressComponentType, Id).GetHashCode();
        }
    }
}