﻿using OdakGIS.Numarataj.Shared.Enums;

namespace OdakGIS.Numarataj.Shared.AddressComponents
{
    public class YetkiliIdare : BaseAddressComponent
    {
        public override AddressComponentTypes AddressComponentType
        {
            get
            {
                return AddressComponentTypes.YetkiliIdare;
            }
            set { }
        }

        public Parametre Tur { get; set; }

        public string KysKurumKod { get; set; }
    }
}
