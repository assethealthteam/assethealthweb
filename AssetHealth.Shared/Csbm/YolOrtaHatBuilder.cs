﻿using Microsoft.SqlServer.Types;
using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdakGIS.Numarataj.Shared.Csbm
{
    public class YolOrtaHatBuilder
    {
        List<YolOrtaHat> _OrtaHatlar;
        SqlGeometry _YolGeometry;

        public YolOrtaHatBuilder(string geom)
        {
            _YolGeometry = SqlGeometry.Parse(new SqlString(geom));
            _YolGeometry.STSrid = 4326;
            _OrtaHatlar = new List<YolOrtaHat>();
        }

    }
}