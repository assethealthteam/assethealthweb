﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdakGIS.Numarataj.Shared.Csbm
{
    public class SinirYolOrtaHat : YolOrtaHat
    {
        public int SolBolgeId { get; set; }
        public CsbmBolgeTip SolBolgeTipi { get; set; }
        public int SagBolgeId { get; set; }
        public CsbmBolgeTip SagBolgeTipi { get; set; }
    }
}