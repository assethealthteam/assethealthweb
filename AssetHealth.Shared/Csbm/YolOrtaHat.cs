﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdakGIS.Numarataj.Shared.Csbm
{
    public class YolOrtaHat
    {
        public List<DbGeometry> Points { get; set; }
    }
}
