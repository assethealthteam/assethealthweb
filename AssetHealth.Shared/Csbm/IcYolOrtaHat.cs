﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdakGIS.Numarataj.Shared.Csbm
{
    public class IcYolOrtaHat : YolOrtaHat
    {
        public int BolgeId { get; set; }

        public CsbmBolgeTip BolgeTipi { get; set; }
    }
}