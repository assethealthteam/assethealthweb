﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdakGIS.Numarataj.Shared.Csbm
{
    public enum CsbmBolgeTip
    {
        Mahalle,
        Koy,
        KoyBaglisi
    }
}
