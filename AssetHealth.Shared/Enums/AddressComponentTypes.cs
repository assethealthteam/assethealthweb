﻿using System;

namespace OdakGIS.Numarataj.Shared.Enums
{
    [Flags]
    public enum AddressComponentTypes
    {
        Il = 0x1,
        Ilce = 0x2,
        Bucak = 0x4,
        Koy = 0x8,
        Belediye = 0x10,
        YetkiliIdare = 0x20,
        Mahalle = 0x40,
        CSBM = 0x80,
        YolOrtaHat = 0x100,
        YolOrtaHatYon = 0x200,
        Bina = 0x400,
        Numarataj = 0x800,
        BagimsizBolum = 0x1000,
        Ulke = 0x2000,
        DisTemsilcilik = 0x4000
    }
}
