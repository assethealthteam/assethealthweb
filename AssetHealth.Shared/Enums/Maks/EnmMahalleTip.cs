﻿namespace OdakGIS.Numarataj.Shared.Enums.Maks
{
    public enum EnmMahalleTip
    {
        NA = 0,
        BelediyeMahallesi = 1,
        Mezra = 3,
        Mevkii = 4,
        YaylaEvleri = 5,
        Diger = 6
    }
}
