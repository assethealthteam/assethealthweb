﻿namespace OdakGIS.Numarataj.Shared.Enums.Maks
{
    public enum CstAdresTipleri
    {
        YerlesimYeri = 1,
        Diger = 2,
    }
}