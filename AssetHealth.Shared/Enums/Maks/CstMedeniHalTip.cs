﻿namespace OdakGIS.Numarataj.Shared.Enums.Maks
{
    public enum CstMedeniHalTip
    {
        Bilinmeyen = 0,
        Bekar = 1,
        Evli = 2,
        Bosanmis = 3,
        Dul = 4,
        EvliliginFeshi = 5,
        EvliliginIptali = 6,
    }
}
