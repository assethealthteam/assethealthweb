﻿namespace OdakGIS.Numarataj.Shared.Enums.Maks
{
    public enum EnumBagimsizBolumDurum
    {
        Bilinmeyen = 1,
        Proje = 2,
        Insaat = 3,
        Iskan = 4,
        Ruhsatsiz = 5,
        KismiIskan = 6,
        YananYikilan = 7
    }
}