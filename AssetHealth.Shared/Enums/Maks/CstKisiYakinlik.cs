﻿namespace OdakGIS.Numarataj.Shared.Enums.Maks
{
    public enum CstKisiYakinlik
    {
        Kendisi = 1,
        Babasi = 2,
        Annesi = 3,
        Kardesi = 4,
        Esi = 5,
        EskiEsi = 6,
        Oglu = 7,
        Kizi = 9
    }
}