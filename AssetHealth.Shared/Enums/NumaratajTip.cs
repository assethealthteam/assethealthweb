﻿using System;

namespace OdakGIS.Numarataj.Shared.Enums
{
    [Serializable]
    public enum NumaratajTip
    {
        Arsa = 1,
        BagimsizAnaGiris = 2,
        BagimsizTaliGiris = 3,
        AnaGiris = 4,
        TaliGiris = 5,
        DigerNumarataj = 6,
        DigerYapiGiris = 7,
        TahsisliArsa = 8,
        SiteGirisi = 9,
        KorCephe = 10
    }
}

/*
1	Bina Ana Giriş
2	Bina Tali Giriş
3	Bağımsız Ana Giriş
4	Bağımsız Tali Giriş
5	Arsa
6	Diğer
7	Tahsisli Arsa
8	Numaratajlı Arsa
9	Kör Cephe
10	Site Girişi


TABLET
Seçiniz  -> Boş ise 0 
Arsa	-> 1	
Bağımsız Ana Giriş ->2
Bağımsız Tali Giriş ->3
Bina Ana Giriş  ->4
Bina Tali Giriş ->5
Diğer Numarataj  ->6
Diğer Yapı Giriş ->7
Tahsisli Arsa    ->8
Site Giriş      ->9
Kör Cephe  ->10
*/
