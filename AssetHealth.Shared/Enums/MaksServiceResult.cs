﻿namespace OdakGIS.Numarataj.Shared.Enums
{
    public class MaksServiceResult
    {
        public string ReturnCode { get; set; }
        public string ReturnMessage { get; set; }

        public MaksServiceResult()
        {
            ReturnCode = "0000";
            ReturnMessage = "İşlem başarıyla gerçekleştirildi";
        }

        public MaksServiceResult SetStatus(MaksServiceResultStatus status)
        {
            switch (status)
            {
                case MaksServiceResultStatus.Basarili:
                    ReturnCode = "0000";
                    ReturnMessage = "İşlem başarıyla gerçekleştirildi";
                    break;
                case MaksServiceResultStatus.IcHata:
                    ReturnCode = "0001";
                    ReturnMessage = "İç hata nedeni ile işleminiz gerçekleştirilemedi";
                    break;
                case MaksServiceResultStatus.ServisBulunamadi:
                    ReturnCode = "0002";
                    ReturnMessage = "Erişmek istenilen servis bulunamadı, lütfen kontrol ediniz";
                    break;
                case MaksServiceResultStatus.ParametreHatasi:
                    ReturnCode = "0003";
                    ReturnMessage = "Servise gönderilen parametreler hatalı, lütfen kontrol ediniz";
                    break;
                case MaksServiceResultStatus.YetkiHatasi:
                    ReturnCode = "0004";
                    ReturnMessage = "Bu servise erişim yetkiniz bulunmamaktadır";
                    break;
                case MaksServiceResultStatus.GeometrikAlanBulunamadi:
                    ReturnCode = "0005";
                    ReturnMessage = "Geometrik alan bulunamadı";
                    break;
                case MaksServiceResultStatus.AdresBilesenSeviye:
                    ReturnCode = "0006";
                    ReturnMessage = "Adres Bileşeni belirlenen seviye için verilemez!";
                    break;
                case MaksServiceResultStatus.ServisCevapYok:
                    ReturnCode = "0007";
                    ReturnMessage = "Servisten cevap alınamıyor";
                    break;
                case MaksServiceResultStatus.JsonParseHatasi:
                    ReturnCode = "0008";
                    ReturnMessage = "Json Parse Hatası";
                    break;
                case MaksServiceResultStatus.ServisHatasi:
                    ReturnCode = "0009";
                    ReturnMessage = "Web Client Servis Hatası";
                    break;
            }

            return this;
        }

        public static MaksServiceResult Create(MaksServiceResultStatus status)
        {
            MaksServiceResult result = new MaksServiceResult();

            result.SetStatus(status);

            return result;
        }
    }

    public class MaksServiceResult<T> : MaksServiceResult
    {
        public T Data { get; set; }
    }
}
