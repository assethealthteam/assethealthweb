﻿namespace OdakGIS.Numarataj.Shared.Enums
{
    public enum NumaratajDisVeriTipi
    {
        Ilce = 0,
        Mahalle = 1,
        Parsel = 2,
        Yapi = 3,
        Csbm = 4,
        Numarataj = 5
    }
}