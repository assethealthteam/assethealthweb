﻿using System.Configuration;

namespace OdakGIS.Numarataj.Shared.Enums
{
    public static class MaksServiceConfs
    {
        public static readonly string ServisDurum = ConfigurationManager.AppSettings["MaksService.ServisDurum"];
        public static readonly string IlSorgula = ConfigurationManager.AppSettings["MaksService.IlSorgula"];
        public static readonly string IlceSorgula = ConfigurationManager.AppSettings["MaksService.IlceSorgula"];
        public static readonly string MahalleKoySorgula = ConfigurationManager.AppSettings["MaksService.MahalleKoySorgula"];
        public static readonly string CsbmSorgulaAdIle = ConfigurationManager.AppSettings["MaksService.CsbmSorgulaAdIle"];
        public static readonly string CsbmSorgula = ConfigurationManager.AppSettings["MaksService.CsbmSorgula"];
        public static readonly string BinaSorgula = ConfigurationManager.AppSettings["MaksService.BinaSorgula"];
        public static readonly string BagimsizBolumList = ConfigurationManager.AppSettings["MaksService.GetBagimsizBolumList"];
        public static readonly string TcIleAdresSorgula = ConfigurationManager.AppSettings["MaksService.TcIleAdresSorgula"];

        public static readonly string IlId = ConfigurationManager.AppSettings["MaksService.IlId"];
        public static readonly string IlceId = ConfigurationManager.AppSettings["MaksService.IlceId"];

        public static readonly string LocalIlId = ConfigurationManager.AppSettings["LocalVt.IlId"];
        public static readonly string LocalIlceId = ConfigurationManager.AppSettings["LocalVt.IlceId"];

    }
}
