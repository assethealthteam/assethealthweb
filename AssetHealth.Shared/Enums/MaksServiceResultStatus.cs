﻿namespace OdakGIS.Numarataj.Shared.Enums
{
    public enum MaksServiceResultStatus
    {
        Basarili = 0,
        IcHata = 1,
        ServisBulunamadi = 2,
        ParametreHatasi = 3,
        YetkiHatasi = 4,
        GeometrikAlanBulunamadi = 5,
        AdresBilesenSeviye = 6,
        ServisCevapYok = 7,
        JsonParseHatasi = 8,
        ServisHatasi = 9
    }
}
