﻿using System.Web;

namespace OdakGIS.Numarataj.Shared.Constant
{
    public static class SessionData
    {
        /// <summary>
        /// Uygulama içinden session'a yapılacak tüm erişimler bu sabit class üzerinden yapılmalıdır...
        /// </summary>
        public static int NumaratajSorgulamaKapiId
        {
            get { return ReadSessionData<int>("tempNumaratajSorgulamaKapiId"); }
            set { HttpContext.Current.Session["tempNumaratajSorgulamaKapiId"] = value; }
        }

        public static string BinaAdi
        {
            get { return ReadSessionData<string>("tempBinaAdi"); }
            set { HttpContext.Current.Session["tempBinaAdi"] = value; }
        }

        public static string CsbmAdi
        {
            get { return ReadSessionData<string>("tempCsbmAdi"); }
            set { HttpContext.Current.Session["tempCsbmAdi"] = value; }
        }

        public static int CsbmSorgulamaYolId
        {
            get { return ReadSessionData<int>("CsbmSorgulamaKapiId"); }
            set { HttpContext.Current.Session["CsbmSorgulamaKapiId"] = value; }
        }

        public static void Clear()
        {
            HttpContext.Current.Session.Clear();
        }

        private static T ReadSessionData<T>(string key)
        {
            T result = default(T);
            if (HttpContext.Current != null && HttpContext.Current.Session != null && HttpContext.Current.Session[key] is T)
            {
                result = (T)HttpContext.Current.Session[key];
            }


            return result;
        }
    }
}
