﻿using System;

namespace OdakGIS.Numarataj.Shared.Helper
{
    public static class DateParser
    {

        public static DateTime? ParseDate(string date)
        {
            if (string.IsNullOrEmpty(date))
            {
                return null;
            }
            var val = date.Split('.')[0];
            try
            {
                return DateTime.ParseExact(val, "yyyy-MM-dd HH:mm:ss",
                    System.Globalization.CultureInfo.InvariantCulture);
            }
            catch
            {
                return null;
            }
        }

    }
}