﻿using System;
using System.Globalization;
using System.Text;

namespace OdakGIS.Numarataj.Shared.Helper
{
    public class StringHelper
    {
        public static string ConvertANSITOUTF8(string ansiContent)
        {
            Encoding ANSI = Encoding.GetEncoding(1252);

            //Encoding iso = Encoding.GetEncoding("ISO-8859-1");
            Encoding utf8 = Encoding.UTF8;
            byte[] utfBytes = utf8.GetBytes(ansiContent);
            byte[] isoBytes = Encoding.Convert(utf8, ANSI, utfBytes);
            string msg = ANSI.GetString(isoBytes);

            return msg;
        }

        // Date Conversions 
        public static string ConvertDateToString(DateTime input)
        {
            return input.ToString(Constants.DATE_FORMAT);
        }

        public static DateTime ConvertStringToDate(string input)
        {
            return DateTime.ParseExact(input, Constants.DATE_FORMAT, CultureInfo.InvariantCulture);
        }

        public static string ConvertStringDateToFormattedString(string input)
        {
            return DateTime.ParseExact(input, Constants.DATE_FORMAT, CultureInfo.InvariantCulture).ToString(Constants.DATE_FORMAT_DISPLAY);
        }

        // DateTime Conversions 

        public static string ConvertDateTimeToString(DateTime input)
        {
            return input.ToString(Constants.DATETIME_FORMAT);
        }

        public static DateTime ConvertStringToDateTime(string input)
        {
            return DateTime.ParseExact(input, Constants.DATETIME_FORMAT, CultureInfo.InvariantCulture);
        }

        public static string ConvertStringDateTimeToFormattedString(string input)
        {
            return DateTime.ParseExact(input, Constants.DATETIME_FORMAT, CultureInfo.InvariantCulture).ToString(Constants.DATETIME_FORMAT_DISPLAY);
        }
        public static string ConvertStringDateTimeToFormattedDateString(string input)
        {
            return DateTime.ParseExact(input, Constants.DATETIME_FORMAT, CultureInfo.InvariantCulture).ToString(Constants.DATE_FORMAT_DISPLAY);
        }

        public static string ConvertStringDateTimeToFormattedTimeString(string input)
        {
            return DateTime.ParseExact(input, Constants.DATETIME_FORMAT, CultureInfo.InvariantCulture).ToString(Constants.TIME_FORMAT_DISPLAY);
        }

        public static long ConvertDateTimeToLong(DateTime input)
        {
            long result;
            long.TryParse(input.ToString(Constants.DATETIME_FORMAT), out result);

            return result;
        }


        public static DateTime ConvertLongToDateTime(long input)
        {
            if (input > 0)
                return ConvertStringToDateTime(input.ToString());
            else
                return DateTime.Now;
        }


        public static string ConvertDoubleStringToFormattedString(string doubleInput, string format)
        {
            double dValue;
            double.TryParse(doubleInput, out dValue);

            return string.Format(format, dValue);

        }


        public static decimal ConvertStringToDecimal(string input)
        {
            decimal dValue;
            //Decimal.TryParse(input.Trim().Replace(',', '.'), NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out dValue);
            Decimal.TryParse(input.Trim(), out dValue);

            return dValue;
        }

    }
}
