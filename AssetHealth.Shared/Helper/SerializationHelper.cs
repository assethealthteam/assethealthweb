﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using OdakGIS.Framework.Core.Entities;
using OdakGIS.Numarataj.Shared.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace OdakGIS.Numarataj.Shared.Helper
{
    public static class SerializationHelper
    {
        public static T FromJson<T>(string jsonContent)
        {
            return JsonConvert.DeserializeObject<T>(jsonContent);
        }

        public static void LogException(Exception e,string data)
        {
            Guid guid = Guid.NewGuid();
            String hata = "Exception:{0}{1}InnerException:{2}{3}StackTrace:{4}{5}Data:{6}";
            string hataString = string.Format(hata,
                e.Message,
                Environment.NewLine,
                e.InnerException != null ? e.InnerException.Message : "",
                Environment.NewLine,
                e.StackTrace,
                Environment.NewLine,
                data ?? ""
                );
            File.WriteAllText(string.Format(@"C:\NumaratajTablet\{0}.txt", guid),
                hataString);
        }

        public static OperationResult<T> FromJsonOperationResult<T>(string jsonContent)
        {
            var result = new OperationResult<T>();
            try
            {

                result.Data = JsonConvert.DeserializeObject<T>(jsonContent, new IsoDateTimeConverter { DateTimeFormat = Constants.DATETIME_FORMAT });
                result.SetInformation("Başarılı");
            }
            catch (Exception exp)
            {
                result.SetError(String.Format("{0} : {1}", "Json Parse Hatası", exp.Message));
                LogException(exp, jsonContent);
            }
            return result;
        }

        public static T FromJsonRequest<T>(string urlFormat, params object[] args)
        {
            var responseString = string.Empty;

            var request = WebRequest.Create(string.Format(urlFormat, args));

            using (var response = request.GetResponse())
            {
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    responseString = reader.ReadToEnd();
                }
            }

            return FromJson<T>(responseString);
        }

        public static T FromXml<T>(string xmlContent)
        {
            var srl = new XmlSerializer(typeof(T));

            T obj;

            using (var rd = new StringReader(xmlContent))
            {
                using (var xrd = new XmlTextReader(rd))
                {
                    obj = (T)srl.Deserialize(xrd);
                }
            }

            return obj;
        }

        public static T FromXmlRequest<T>(string urlFormat, params object[] args)
        {
            var request = WebRequest.Create(string.Format(urlFormat, args));

            var response = request.GetResponse();

            var serializer = new XmlSerializer(typeof(T));


            return (T)serializer.Deserialize(response.GetResponseStream());
        }

        public static string ToXml<T>(T data)
        {
            var result = string.Empty;

            var serializer = new XmlSerializer(typeof(T));

            using (var writer = new StringWriter())
            {
                serializer.Serialize(writer, data);

                result = writer.ToString();

                writer.Flush();
            }

            return result;
        }

        public static string ToJson(object data)
        {
            return JsonConvert.SerializeObject(data);
        }
    }
}