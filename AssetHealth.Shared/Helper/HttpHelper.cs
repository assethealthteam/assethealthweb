﻿using OdakGIS.Framework.Core.Entities;
using OdakGIS.Framework.Core.Helpers;
using OdakGIS.Numarataj.Shared.Enums;
using System;
using System.Net;
using System.Text;

namespace OdakGIS.Numarataj.Shared.Helper
{
    public static class HttpHelper
    {

        public static MaksServiceResult<T> GetHttp<T>(string url)
        {
            MaksServiceResult<T> result = new MaksServiceResult<T>();
            try
            {
                using (WebClient client = new WebClient())
                {
                    client.Headers.Add("Content-Type", "application/json");
                    client.Encoding = Encoding.UTF8;
                    string jsonString = client.DownloadString(url);
                    OperationResult<MaksServiceResult<T>> data = SerializationHelper.FromJsonOperationResult<MaksServiceResult<T>>(jsonString);
                    if (data.IsSuccess)
                    {
                        result = data.Data;
                    }
                    else
                    {
                        result.SetStatus(MaksServiceResultStatus.JsonParseHatasi);
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
                result.SetStatus(MaksServiceResultStatus.ServisHatasi);
            }
            return result;
        }

        public static MaksServiceResult GetHttp(string url)
        {
            MaksServiceResult result = new MaksServiceResult();
            try
            {
                using (WebClient client = new WebClient())
                {
                    client.Headers.Add("Content-Type", "application/json");
                    client.Encoding = Encoding.UTF8;
                    string jsonString = client.DownloadString(url);
                    OperationResult<MaksServiceResult> data = SerializationHelper.FromJsonOperationResult<MaksServiceResult>(jsonString);
                    if (data.IsSuccess)
                    {
                        result = data.Data;
                    }
                    else
                    {
                        result.SetStatus(MaksServiceResultStatus.JsonParseHatasi);
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
                result.SetStatus(MaksServiceResultStatus.ServisHatasi);
            }
            return result;
        }

    }
}
