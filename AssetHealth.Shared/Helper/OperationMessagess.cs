﻿namespace OdakGIS.Numarataj.Shared.Helper
{
    public static class OperationMessages
    {
        public const string BirimBulunamadi = "İşlem yapmak istediğiniz birim bulunamadı.";
        public const string DbError = "Veritabanı hatası nedeni ile işlem gerçekleştirilemedi";
        public const string DbItemNotFound = "Güncellenecek kayıt bilgisi bulunamadığından işlem gerçekleştirilemedi";
        public const string DuplicateRecord = "Bu kayıt daha önceden eklendiğinden tekrar eklenemez.";
        public const string GeneralError = "Hata nedeniyle işleminiz gerçekleştirilemedi";
        public const string ModelStateNotValid = "Lütfen girilen bilgileri kontrol edip tekrar deneyiniz";
        public const string NewPasswordMismatch = "Yeni şifrenizi Hatalı girdiniz.";
        public const string PasswordMismatch = "Mevcut Şifrenizi hatalı girdiniz.";
        public const string PasswordNewPasswordMismatch = "Şifre ve Şifre tekrar aynı değil.";
        public const string Success = "İşleminiz başarıyla gerçekleştirildi";
    }
}
