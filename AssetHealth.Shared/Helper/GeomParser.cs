﻿using System.Data.Entity.Spatial;

namespace OdakGIS.Numarataj.Shared.Helper
{
    public static class GeomParser
    {

        public static DbGeometry GetGeom(string data)
        {
            DbGeometry geometry = DbGeometry.FromBinary(GetBytes(data));
            return geometry;
        }


        public static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
    }
}
