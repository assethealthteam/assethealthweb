﻿using System;

namespace OdakGIS.Numarataj.Shared.Entities
{
    [Serializable]
    public class YapiData
    {
        public int Id { get; set; }
        public string Adi { get; set; }
        public string Geometry { get; set; }
        public string Parsel { get; set; }
        public string PostaKodu { get; set; }
        public int? ZeminUstuKatSayi { get; set; }
        public int? ZeminAltiKatSayi { get; set; }
        public string SiteVeyaKooperatifAdi { get; set; }
        public int? Tip { get; set; }
        public int? Durum { get; set; }
        public long? UavtNo { get; set; }
        public int? UavtEslesmiyor { get; set; }
        public string Aciklama { get; set; }
        public int? AcilAsansorSayisi { get; set; }
        public short? AcilDurumAsansoruVarMi { get; set; }
        public short? AcilDurumPlaniVarMi { get; set; }
        public int? AnonsSistemi { get; set; }
        public int? AsansorSayisi { get; set; }
        public short? AsansorVarMi { get; set; }
        public int? BbSayisi { get; set; }
        public int? BinaYapimYili { get; set; }
        public int? BinaYasi { get; set; }
        public int? BodrumSayisi { get; set; }
        public int? CatiDurumu { get; set; }
        public short? DigerMi { get; set; }
        public short? DiniMi { get; set; }
        public string DogalGazAbone { get; set; }
        public short? EgitimMi { get; set; }
        public string ElektrikAbone { get; set; }
        public short? EngelliGirisiVarMi { get; set; }
        public string EskiKapiNo { get; set; }
        public int? FizikselDurum { get; set; }
        public int? InsaatTipi { get; set; }
        public int? IsinmaBilgileri { get; set; }
        public int? IsinmaBilgileriMadde { get; set; }
        public short? JeneratorVarMi { get; set; }
        public short? KatKrokisiVarMi { get; set; }
        public int? KatSayisi { get; set; }
        public short? Kaynak { get; set; }
        public short? KazanDairesiVarMi { get; set; }
        public int? KimlikNo { get; set; }
        public short? KonutVarMi { get; set; }
        public short? KulturelMi { get; set; }
        public short? Olcek { get; set; }
        public int? OtoparkSayi { get; set; }
        public int? OtoparkTip { get; set; }
        public short? OtoparkVarMi { get; set; }
        public int? ParselId { get; set; }
        public int? ParselKimlikNo { get; set; }
        public short? ResmiDaireMi { get; set; }
        public short? SaglikMi { get; set; }
        public short? SanayiMi { get; set; }
        public short? SiginakVarMi { get; set; }
        public short? SporMu { get; set; }
        public string SuAboneSicilNo { get; set; }
        public short? SuDeposuVarMi { get; set; }
        public string SuSaatiNo { get; set; }
        public int? TahliyePlani { get; set; }
        public string TahsisNo { get; set; }
        public string TaliGirisNo { get; set; }
        public string TaliSokakAdi { get; set; }
        public short? TicariMi { get; set; }
        public short? YanginAlgilamaVarMi { get; set; }
        public short? YanginMerdiveniVarMi { get; set; }
        public short? YanginSondurmeVarMi { get; set; }
        public short? YerlesimPlaniVarMi { get; set; }
        public short? YuruyenMerdivenVarMi { get; set; }
        public string TahsisSokak { get; set; }
        public string BinaNotu { get; set; }
        public string YapiTipiAciklama { get; set; }
        public int? MahalleId { get; set; }
        public string DegistirmeTarihi { get; set; }
        public string OlusturmaTarihi { get; set; }
    }
}