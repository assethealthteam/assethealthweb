﻿using System;
using System.Collections.Generic;

namespace OdakGIS.Numarataj.Shared.Entities
{
    [Serializable]
    public class InfoYapiData
    {
        public YapiData yapi { get; set; }
        public List<NumaratajData> kapiList { get; set; }
        public List<int> bagimsizBolumIdList { get; set; }
    }
}
