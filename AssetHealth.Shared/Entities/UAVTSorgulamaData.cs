﻿using System;

namespace OdakGIS.Numarataj.Shared.Entities
{
    [Serializable]
    public class UAVTSorgulamaData
    {
        public long Id { get; set; }
        public string Il { get; set; }
        public int? IlId { get; set; }
        public string Ilce { get; set; }
        public int? IlceId { get; set; }
        public string Mahalle { get; set; }
        public int? MahalleId { get; set; }
        public string CSBMAdi { get; set; }
        public string CSBMTurAdi { get; set; }
        public int? CSBMId { get; set; }
        public int? YolTipId { get; set; }
        public string YolTip { get; set; }
        public string AdaParsel { get; set; }
        public int? AdaParselId { get; set; }
        public string Ada { get; set; }
        public string Parsel { get; set; }
        public string ApartmanBlokAdi { get; set; }
        public int? ApartmanBlokId { get; set; }
        public string GirisYonu { get; set; }
        public int? GirisYonuId { get; set; }
        public string DisKapiNo { get; set; }
        public string YapiTipi { get; set; }
        public string Not { get; set; }
        public string UAVTBinaNo { get; set; }
        public string UAVTAdresNo { get; set; }
        public string Uzunluk { get; set; }
        public string KullanimDurumu { get; set; }
    }
}
