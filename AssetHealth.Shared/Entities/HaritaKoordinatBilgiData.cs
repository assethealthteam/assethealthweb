﻿namespace OdakGIS.Numarataj.Shared.Entities
{
    public class HaritaKoordinatBilgiData
    {
        public string Projeksiyon { get; set; }
        public string X { get; set; }
        public string Y { get; set; }
    }
}
