﻿namespace OdakGIS.Numarataj.Shared.Entities
{
    public class TcKisiAdresData
    {
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public string BabaAd { get; set; }
        public string AnneAd { get; set; }
        public string TcNo { get; set; }
        public string Adres { get; set; }
        public string Il { get; set; }
        public string Ilce { get; set; }
        public string Mahalle { get; set; }
        public string Csbm { get; set; }
        public string DisKapiNo { get; set; }
        public string IcKapiNo { get; set; }
        public string UavtBinaNo { get; set; }
        public string UavtAdresNo { get; set; }
    }
}
