﻿using System.ComponentModel;

namespace OdakGIS.Numarataj.Shared.Entities
{
    public class CsbmSorgulamaCriteria
    {
        public int? MahalleId { get; set; }
        public int? CsbmTurId { get; set; }

        [DisplayName("CSBM Adı")]
        public string Adi { get; set; }
    }
}
