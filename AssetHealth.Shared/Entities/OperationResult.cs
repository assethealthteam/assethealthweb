﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdakGIS.Numarataj.Shared.Entities
{
    public class OperationResult<T> : OperationResult
    {

        public T Data { get; set; }
    }

    public class OperationResult
    {

        public bool IsSuccess { get; set; }
        public string OperationMessage { get; set; }

        public string AndroidId { get; set; }

        public int SenkronizasyonId { get; set; }

        public OperationResult()
        {

        }


        public OperationResult(bool isSuccess, string operationMessage)
        {
            OperationMessage = operationMessage;
            IsSuccess = isSuccess;
        }


        public void SetError(string errorMessage)
        {
            OperationMessage = errorMessage;
            IsSuccess = false;
        }
        public void SetInformation(string informationMessage)
        {
            OperationMessage = informationMessage;
            IsSuccess = true;
        }
    }

}
