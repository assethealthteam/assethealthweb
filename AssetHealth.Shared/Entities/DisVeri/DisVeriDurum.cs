﻿namespace OdakGIS.Numarataj.Shared.Entities.DisVeri
{
    public class DisVeriDurum
    {
        public string Adi { get; set; }
        public string Mesaj { get; set; }
    }
}