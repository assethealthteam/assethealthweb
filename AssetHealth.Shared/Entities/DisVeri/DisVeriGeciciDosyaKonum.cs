﻿using OdakGIS.Numarataj.Shared.Enums;
using System.Collections.Generic;

namespace OdakGIS.Numarataj.Shared.Entities.DisVeri
{
    public class DisVeriGeciciDosyaKonum
    {
        public NumaratajDisVeriTipi Tip { get; set; }
        public int Islenmis { get; set; }
        public string TempFileLocation { get; set; }
        public IEnumerable<DisVeriAlan> Alanlar { get; set; }
    }
}