﻿namespace OdakGIS.Numarataj.Shared.Entities.DisVeri
{
    public class DisVeriAlan
    {
        public string Aciklama { get; set; }
        public string MerkezAlanAdi { get; set; }
        public string DosyaAlanAdi { get; set; }
        public bool Zorunlu { get; set; }
    }
}