﻿using System.Collections.Generic;
using System.Threading;

namespace OdakGIS.Numarataj.Shared.Entities.DisVeri
{
    public class DisVeriIlerleme
    {
        public int Islenen { get; set; }
        public int Toplam { get; set; }
        public IList<DisVeriDurum> Hatalar { get; set; }
        public CancellationTokenSource Token { get; set; }
        public bool Cancel { get; set; }
    }
}
