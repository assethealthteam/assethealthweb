﻿using System.Collections.Generic;

namespace OdakGIS.Numarataj.Shared.Entities
{
    public class ResultData
    {

        public List<int> ErrorList { get; set; }
        public int SuccessCount { get; set; }
    }
}
