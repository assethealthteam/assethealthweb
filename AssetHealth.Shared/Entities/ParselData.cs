﻿namespace OdakGIS.Numarataj.Shared.Entities
{
    public class ParselData
    {
        public int Id { get; set; }
        public string Adi { get; set; }
        public string ParselNo { get; set; }
        public string PaftaNo { get; set; }
        public int AdaNo { get; set; }
        public int ParselTip { get; set; }
        public string Mahalle { get; set; }
        public int MahalleId { get; set; }
        public string Geometry { get; set; }
    }
}