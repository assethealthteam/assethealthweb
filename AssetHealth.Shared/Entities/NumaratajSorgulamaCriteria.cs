﻿using System.ComponentModel;

namespace OdakGIS.Numarataj.Shared.Entities
{
    public class NumaratajSorgulamaCriteria
    {
        public string YolAdi { get; set; }
        public int? MahalleId { get; set; }
        public int? YolId { get; set; }

        [DisplayName("Dış Kapı No")]
        public string DisKapiNo { get; set; }

        [DisplayName("Ada/Parsel")]
        public int AdaParsel { get; set; }
    }
}