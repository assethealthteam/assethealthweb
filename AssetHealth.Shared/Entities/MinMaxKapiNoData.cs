﻿using System.Text;

namespace OdakGIS.Numarataj.Shared.Entities
{
    public class MinMaxKapiNoData
    {
        public int MinTek { get; set; }
        public int MaxTek { get; set; }
        public int MinCift { get; set; }
        public int MaxCift { get; set; }

        public string GetTekBinaNo()
        {
            StringBuilder builder = new StringBuilder();
            if (MinTek == 0 || MinTek == -1)
                builder.Append(" ");
            else
                builder.Append(MinTek);

            builder.Append("-");

            if (MaxTek == 0 || MaxTek == -1)
                builder.Append(" ");
            else
                builder.Append(MaxTek);

            return builder.ToString();
        }

        public string GetCiftBinaNo()
        {
            StringBuilder builder = new StringBuilder();
            if (MinCift == 0)
                builder.Append(" ");
            else
                builder.Append(MinCift);

            builder.Append("-");

            if (MaxCift == 0)
                builder.Append(" ");
            else
                builder.Append(MaxCift);
            return builder.ToString();
        }

    }
}