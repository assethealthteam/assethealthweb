﻿namespace OdakGIS.Numarataj.Shared.Entities
{
    public class MedyaGosterimData
    {
        public int Id { get; set; }
        public int? MedyaId { get; set; }
        public string Etiket { get; set; }

        /// <summary>
        ///     Medya'ya ait dosyanın, dosya sistemindeki adresidir.
        ///     Örn: d:\uploads\resim.jpg
        /// </summary>
        public string DosyaAdres { get; set; }
        public string OnIzlemeAdres { get; set; }

        /// <summary>
        ///     Medya'ya ait dosyanın, orjinal url adresidir.
        ///     Örn: http://www.site.com/images/resim.jpg
        /// </summary>
        public string UrlAdres { get; set; }

        public string BaseUrlAdres { get; set; }

        /// <summary>
        ///     Medya'ya ait dosyanın, küçük boyutlu önizleme url adresidir.
        ///     Örn: http://www.site.com/images/thumb_resim.jpg
        /// </summary>
        public string UrlAdresOnizleme { get; set; }


        //public string Tarih2 { get; set; }
        public string Tarih { get; set; }
        public double EnlemX { get; set; }
        public double EnlemY { get; set; }
        public LonLatData LonLat { get; set; }

        public string BinaDetayToString()
        {

            return UrlAdresOnizleme;
        }

    }
}
