﻿namespace OdakGIS.Numarataj.Shared.Entities
{
    public class TcData
    {
        public string TcKimlikNumarasi { get; set; }
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public string BabaAd { get; set; }
        public string DogumYeri { get; set; }
    }
}
