﻿namespace OdakGIS.Numarataj.Shared.Entities
{
    public class LonLatData
    {
        public string Longitude { get; set; }
        public string Latitude { get; set; }
    }
}
