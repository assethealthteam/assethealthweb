﻿using System.Data.Entity.Spatial;

namespace OdakGIS.Numarataj.Shared.Entities
{
    public class MahalleCsbmKesisim
    {
        public int Mahalle1 { get; set; }
        public int Mahalle2 { get; set; }
        public DbGeometry Geometry { get; set; }
    }
}
