﻿namespace OdakGIS.Numarataj.Shared.Entities
{
    public class CsbmYolOrtaHatData
    {
        public int Number { get; set; }
        public string Geometry { get; set; }
        public string OrtaHatYonGeometry { get; set; }

    }
}