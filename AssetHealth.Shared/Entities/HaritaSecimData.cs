﻿namespace OdakGIS.Numarataj.Shared.Entities
{
    public class HaritaSecimData
    {
        public int Id { get; set; }
        public string Adi { get; set; }
        public string[] Geom { get; set; }
    }
}
