﻿using System;

namespace OdakGIS.Numarataj.Shared.Entities
{
    public class CsbmData
    {
        public int Id { get; set; }
        public string Adi { get; set; }
        public string MahalleAdi { get; set; }
        public int? Tip { get; set; }
        public string TipAdi { get; set; }
        public int? KimlikNo { get; set; }
        public int? TekBaslangicNo { get; set; }
        public int? TekBitisNo { get; set; }
        public int? CiftBaslangicNo { get; set; }
        public int? CiftBitisNo { get; set; }
        public string CsbmWkt { get; set; }
        public long? UavtNo { get; set; }
        public int? UavtEslesmiyor { get; set; }
        public decimal? Genislik { get; set; }
        public decimal? Uzunluk { get; set; }
        public string Aciklama { get; set; }
        public int? AydinlatmaVarMi { get; set; }
        public int? BaglantiNiteligi { get; set; }
        public int? CaddeSokakLevhaVarMi { get; set; }
        public int? Cinsi { get; set; }
        public int? Durum { get; set; }
        public decimal? Egimi { get; set; }
        public int? HizLimiti { get; set; }
        public int? Islevi { get; set; }
        public int? KaldirimVarMi { get; set; }
        public string KaplamaCinsi { get; set; }
        public string MeclisKararNo { get; set; }
        public DateTime? MeclisKararTarih { get; set; }
        public int? ParklanmaVarMi { get; set; }
        public int? RefujVarMi { get; set; }
        public int? SeritCizgisiVarMi { get; set; }
        public int? SeritSayisi { get; set; }
        public int? Sinyalizasyon { get; set; }
        public string TabelaAdi { get; set; }
        public int? YolcuDuragiVarMi { get; set; }
        public int? YolunDevamliligi { get; set; }
        public int? YonDurumu { get; set; }
        public int? ZeminNiteligi { get; set; }
        public string DegistirmeTarihi { get; set; }
        public string OlusturmaTarihi { get; set; }

        public string PhotoPath { get; set; }
        public int ToplamBina { get; set; }
        public int ToplamKapi { get; set; }
        public int ToplamParsel { get; set; }
        public string IlAdi { get; set; }
        public string IlceAdi { get; set; }
        public string CiftBinalar { get; set; }
        public string TekBinalar { get; set; }

        public override string ToString()
        {
            if (string.IsNullOrEmpty(Adi) == false)
                return IlAdi + " ili, " + IlceAdi + " ilçesi, " + MahalleAdi + " mahallesi, " + Adi;
            return IlAdi + " ili, " + IlceAdi + " ilçesi," + MahalleAdi + " mahallesi";
        }
    }
}