namespace OdakGIS.Numarataj.Shared.Data
{
    using OdakGIS.Sync.Entities;
    using OdakGIS.Sync.Enums;
    using System;
    using System.Runtime.Serialization;

    public partial class SenkronizasyonDetayData
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string AndroidId { get; set; }

        [DataMember]
        public int SenkronizasyonId { get; set; }

        [DataMember]
        public string TabloAdi { get; set; }

        [DataMember]
        public int? TabloId { get; set; }

        [DataMember]
        public int? TransactionId { get; set; }

        [DataMember]
        public int? OperasyonTipi { get; set; }

        [DataMember]
        public bool? IsSent { get; set; }

        [DataMember]
        public int? Result { get; set; }

        [DataMember]
        public string Item { get; set; }

        [DataMember]
        public int? CihazId { get; set; }

        [DataMember]
        public int? ClientId { get; set; }

        [DataMember]
        public string Message { get; set; }

    }

    public static class SenkronizasyonDetayDataExtensions
    {

        public static SenkronizasyonDetay GetSenkronizasyonDetay(this SenkronizasyonDetayData s)
        {
            if (!(s is SenkronizasyonDetayData))
                throw new Exception("SenkronizasyonDetayData veri olmal�d�r.");

            SenkronizasyonDetay result = new SenkronizasyonDetay();
            result.SenkronizasyonId = s.SenkronizasyonId;
            result.OperasyonTipi = (SenkronizasyonOperasyonTipi)s.OperasyonTipi;
            result.TabloAdi = s.TabloAdi;
            result.ServerId = s.TabloId;
            //result.TransactionId = s.TransactionId;
            result.Result = (SenkronizasyonDetaySonuc)s.Result;

            return result;
        }
    }



}
