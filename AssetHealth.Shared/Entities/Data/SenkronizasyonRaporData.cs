namespace OdakGIS.Numarataj.Shared.Data
{
    using OdakGIS.Sync.Entities;
    using System.Collections.Generic;

    public partial class SenkronizasyonRaporData
    {
        public Senkronizasyon SenkronizasyonData { get; set; }
        public List<SenkronizasyonDetayData> SenkronizasyonDetayList { get; set; }


    }
}
