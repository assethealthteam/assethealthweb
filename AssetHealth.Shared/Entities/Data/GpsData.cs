using OdakGIS.Numarataj.Data;
using OdakGIS.Numarataj.Shared.Helper;
using System;
using System.Data.Entity.Spatial;
using System.Runtime.Serialization;

namespace OdakGIS.Numarataj.Shared.Data
{
    [DataContract]
    public class GpsData : Gps
    {
        [DataMember]
        public string Geometry { get; set; }

        [DataMember]
        public string KayitTarih { get; set; }

        private DateTime _kayitTarih;

        public DateTime KayitTarihDb
        {
            get
            {
                if ((_kayitTarih.Year == 1) && !string.IsNullOrEmpty(KayitTarih))
                    _kayitTarih = DateParser.ParseDate(KayitTarih) ?? DateTime.Now;
                return _kayitTarih;
            }
        }

        private DbGeometry _geometry;
        public DbGeometry GeometryDb
        {
            get
            {
                if (_geometry == null && !string.IsNullOrEmpty(Geometry))
                {
                    try
                    {
                        _geometry = DbGeometry.FromText(Geometry, 4326);
                    }
                    catch { }
                }
                return _geometry;
            }
        }


        [DataMember]
        public string GuncellemeTarih { get; set; }

        [DataMember]
        public string SistemTarih { get; set; }

        private DateTime _guncellemeTarih;

        private DateTime _sistemTarih;

        public DateTime? GuncellemeTarihDb
        {
            get
            {
                if ((_guncellemeTarih.Year != 1) || string.IsNullOrEmpty(GuncellemeTarih))
                    _guncellemeTarih = DateParser.ParseDate(GuncellemeTarih) ?? DateTime.Now;
                return _guncellemeTarih;
            }
        }

        public DateTime SistemTarihDb
        {
            get
            {
                if ((_sistemTarih.Year == 1) && !string.IsNullOrEmpty(SistemTarih))
                    _sistemTarih = DateParser.ParseDate(SistemTarih) ?? DateTime.Now;
                return _sistemTarih;
            }
        }
    }
}
