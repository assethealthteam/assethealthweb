﻿namespace OdakGIS.Numarataj.Shared.Entities
{
    public class HaritaVektorVeri
    {
        public int Id { get; set; }
        public string Tip { get; set; }
        public string Adi { get; set; }
        public string Geometry { get; set; }
    }
}