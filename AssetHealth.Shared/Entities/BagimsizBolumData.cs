﻿using System;

namespace OdakGIS.Numarataj.Shared.Entities
{
    [Serializable]
    public class BagimsizBolumData
    {
        public int Id { get; set; }
        public int? Durum { get; set; }
        public string Aciklama { get; set; }
        public int? KimlikNo { get; set; }
        public int? BagliOlduguBBId { get; set; }
        public int YapiId { get; set; }
        public int? NumaratajId { get; set; }
        public string Ad { get; set; }
        public string BagimsizBolumNo { get; set; }
        public int? KullanimTuru { get; set; }
        public int? KullanimAltTur { get; set; }
        public int? DigerYapiId { get; set; }
        public string TapuBagimsizBolumNo { get; set; }
        public short? Tip { get; set; }
        public short? KatNo { get; set; }
        public short? Kaynak { get; set; }
        public long? UavtNo { get; set; }
        public int? YasayanSayisi { get; set; }
        public int? CalisanSayisi { get; set; }
        public int? YakitTuru { get; set; }
        public string SuAbonesi { get; set; }
        public string TicariIsYeriAdi { get; set; }
        public string EvSahibi { get; set; }
        public string Kiraci { get; set; }
        public string OturanTc { get; set; }
        public string BilgiVerenKisi { get; set; }
        public short? Soru1 { get; set; }
        public short? Soru2 { get; set; }
        public string IsYeriAdi { get; set; }
        public string DisKapiNo { get; set; }
        public string EskiBBNo { get; set; }
        public string DegistirmeTarihi { get; set; }
        public string OlusturmaTarihi { get; set; }
        public string Cevap1 { get; set; }
        public string Cevap2 { get; set; }

        public string IlAdi { get; set; }
        public string IlceAdi { get; set; }
        public string SiteAdi { get; set; }
        public string YapiAdi { get; set; }
        public string CsbmAdi { get; set; }
        public string MahalleAdi { get; set; }
        public string AdaParsel { get; set; }
        public string KapiNo { get; set; }

        public string BinaDetayToString()
        {
            if (string.IsNullOrEmpty(YapiAdi) == false)
                return IlAdi + " ili, " + IlceAdi + " ilçesi, " + MahalleAdi + " mahallesi, " + YapiAdi + " binası ";
            else
                return IlAdi + " ili, " + IlceAdi + " ilçesi, " + MahalleAdi + " mahallesi";
        }
    }
}
