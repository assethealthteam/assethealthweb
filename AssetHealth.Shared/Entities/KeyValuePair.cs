﻿namespace OdakGIS.Numarataj.Shared.Entities
{
    public class KeyValuePair
    {
        public int Id { get; set; }
        public string Adi { get; set; }
        public string Value { get; set; }
    }
}