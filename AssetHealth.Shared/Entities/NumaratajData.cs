﻿using OdakGIS.Numarataj.Data.Enums;
using OdakGIS.Numarataj.Shared.Enums;
using System;

namespace OdakGIS.Numarataj.Shared.Entities
{
    [Serializable]
    public class NumaratajData
    {
        public int Id { get; set; }
        //public string DisKapiNo { get; set; }
        public string Geometry { get; set; }
        public string KapiNo { get; set; }
        public NumaratajTip? Tip { get; set; }
        public int? TipId { get; set; }
        public string TipAdi { get; set; }
        public int? BagliOlduguNumaratajId { get; set; }
        public string YapiAdi { get; set; }
        public string CsbmAdi { get; set; }
        public string MahalleAdi { get; set; }
        public SayisallastirmaYon? Yon { get; set; }
        public long? UavtNo { get; set; }
        public int? MontajYapilmamaSebebi { get; set; }
        public string Aciklama { get; set; }
        public string Ad { get; set; }
        public int? Binakodu { get; set; }
        public int? DigerYapiId { get; set; }
        public int? KimlikNo { get; set; }
        public int? ParselId { get; set; }
        public string AdaParsel { get; set; }
        public int? ParselKimlikNo { get; set; }
        public int? YapiId { get; set; }
        public string YerlesimAcisi { get; set; }
        public int? YolOrtaHatId { get; set; }
        public int? YolOrtaHatYonId { get; set; }
        public int? MevcutDurum { get; set; }
        public string DegistirmeTarihi { get; set; }
        public string OlusturmaTarihi { get; set; }

        public string PhotoPath { get; set; }
        public string IlAdi { get; set; }
        public string IlceAdi { get; set; }
        public string SiteAdi { get; set; }
        public int ToplamBagimsizBolum { get; set; }
        public int ToplamKapi { get; set; }
        public string YapiTipi { get; set; }
        public string YapiTuru { get; set; }
        public long UAVTNo { get; set; }
        public string KullanimDurumu { get; set; }
        public string PostaKodu { get; set; }
        public int ZeminUstuKatSayisi { get; set; }
        public int ZeminAltiKatSayisi { get; set; }

        public string BinaDetayToString()
        {
            if (string.IsNullOrEmpty(YapiAdi) == false)
                return IlAdi + " ili, " + IlceAdi + " ilçesi, " + MahalleAdi + " mahallesi, " + YapiAdi + " binası ";
            else
                return IlAdi + " ili, " + IlceAdi + " ilçesi, " + MahalleAdi + " mahallesi";
        }
    }
}