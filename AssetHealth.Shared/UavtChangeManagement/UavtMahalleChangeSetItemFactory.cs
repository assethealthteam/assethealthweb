﻿using OdakGIS.Framework.Core.Entities;
using OdakGIS.Numarataj.Data.UAVTEntities;
using OdakGIS.Sync.ChangeManagement;
using OdakGIS.Sync.Entities.Custom;
using OdakGIS.Sync.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OdakGIS.Numarataj.Shared.UavtChangeManagement
{
    public class UavtMahalleChangeSetItemFactory : ChangeSetItemFactory<Mahalle, Mahalle>
    {
        public UavtMahalleChangeSetItemFactory(int itemId, int cihazId, SenkronizasyonOperasyonTipi itemState)
            : base(itemId, cihazId, itemState)
        {

        }
        protected override OperationResult Validate(Mahalle item)
        {
            OperationResult result = new OperationResult()
            {
                IsSuccess = true
            };
            return result;
        }

        protected override Mahalle PrepareForSynchronization(int itemId)
        {
            Mahalle mahalle = null;
            using (var ctx = new UAVTContext())
                mahalle = ctx.Mahalle.FirstOrDefault(t => t.Id == itemId);

            return mahalle;
        }

        public override int SaveChangeSetItem(ChangeSetItem item)
        {
            throw new NotImplementedException();
        }

        public override void SaveMedia(int medyaId)
        {
            throw new NotImplementedException();
        }

        public override List<int> GetNesneMedya()
        {
            throw new NotImplementedException();
        }

        public override void NotKaydet(NotData not)
        {
        }

        public override string GetCentroid(ChangeSetItem item)
        {
            return null;
        }
    }
}
