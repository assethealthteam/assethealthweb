﻿using OdakGIS.Framework.Core.Entities;
using OdakGIS.Numarataj.Data.UAVTEntities;
using OdakGIS.Sync.ChangeManagement;
using OdakGIS.Sync.Entities.Custom;
using OdakGIS.Sync.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OdakGIS.Numarataj.Shared.UavtChangeManagement
{
    public class UavtBagimsizBolumChangeSetItemFactory : ChangeSetItemFactory<BagimsizBolum, BagimsizBolum>
    {
        public UavtBagimsizBolumChangeSetItemFactory(int itemId, int cihazId, SenkronizasyonOperasyonTipi itemState)
            : base(itemId, cihazId, itemState)
        {

        }

        protected override OperationResult Validate(BagimsizBolum item)
        {
            OperationResult result = new OperationResult()
            {
                IsSuccess = true
            };
            return result;
        }

        protected override BagimsizBolum PrepareForSynchronization(int itemId)
        {
            BagimsizBolum bagimsiz = null;
            using (var ctx = new UAVTContext())
                bagimsiz = ctx.BagimsizBolum.FirstOrDefault(t => t.Id == itemId);

            return bagimsiz;
        }

        public override int SaveChangeSetItem(ChangeSetItem item)
        {
            throw new NotImplementedException();
        }

        public override void SaveMedia(int medyaId)
        {
            throw new NotImplementedException();
        }

        public override List<int> GetNesneMedya()
        {
            throw new NotImplementedException();
        }
        public override void NotKaydet(NotData not)
        {
        }

        public override string GetCentroid(ChangeSetItem item)
        {
            return null;
        }
    }
}
