﻿using OdakGIS.Framework.Core.Entities;
using OdakGIS.Framework.Core.Interfaces;
using OdakGIS.Numarataj.Data.Entities;
using OdakGIS.Numarataj.Shared.Entities;
using OdakGIS.Numarataj.Shared.Enums;
using System.Collections.Generic;
using System.Data.Entity.Spatial;

namespace OdakGIS.Numarataj.Shared.Interfaces
{
    public interface INumaratajService : IProjectService
    {
        OperationResult<int> NumaratajKaydet(NumaratajData numarataj, bool sync);
        NumaratajData NumaratajGetir(int id);
        List<KeyValuePair> BagliNumaratajGetir(string geometry, NumaratajTip? tip);
        OperationResult<NumaratajData> NumaratajBilgiAl(string geom);
        OperationResult<YOLORTAHATYON> OrtaHatYonBelirle(IEnumerable<YOLORTAHATYON> ortaHatYonleri, DbGeometry startPoint);
        IList<NUMARATAJ> NumaratajSayfala(int pageIndex, int pageSize);
        int GetNumaratajSayisi();
        OperationResult<int?> GetNumaratajIdFromUavtAjax(string uavtNo);
    }
}