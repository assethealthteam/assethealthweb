﻿using OdakGIS.Framework.Core.Entities;
using OdakGIS.Framework.Core.Interfaces;
using OdakGIS.Numarataj.Data.Entities;
using OdakGIS.Numarataj.Shared.Entities;
using System.Collections.Generic;
using System.Data.Entity.Spatial;

namespace OdakGIS.Numarataj.Shared.Interfaces
{
    public interface IIlceService : IProjectService
    {
        OperationResult IlceKaydet(ILCE ilce);
        IList<KeyValuePair> IlceListele();
        IList<ILCE> IlceSayfala(int pageIndex, int pageSize);
        int? GetContainingIlce(DbGeometry geometry);
        int GetIlceSayisi();
    }
}
