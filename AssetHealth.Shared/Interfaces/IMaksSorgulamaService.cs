﻿using OdakGIS.Framework.Core.Entities;
using OdakGIS.Framework.Core.Interfaces;
using OdakGIS.Numarataj.Shared.Entities;

namespace OdakGIS.Numarataj.Shared.Interfaces
{
    public interface IMaksSorgulamaService : IProjectService
    {
        Paging<TcKisiAdresData> TcKisiBilgiPaging(Paging paging, TcData tcData);
    }
}
