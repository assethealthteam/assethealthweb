﻿using OdakGIS.Framework.Core.Entities;
using OdakGIS.Framework.Core.Interfaces;
using OdakGIS.Numarataj.Shared.Entities;
using System.Collections.Generic;

namespace OdakGIS.Numarataj.Shared.Interfaces
{
    public interface IUAVTSorgulamaService : IProjectService
    {
        List<KeyValuePair> MahalleListele(int ilceKod);

        List<KeyValuePair> CsbmListele(int mahalleKod);

        List<KeyValuePair> BinaListele(int csbmKod);

        Paging<UAVTSorgulamaData> SorgulamaPagingAjax(Paging pagingParameters, UAVTSorgulamaData sorgulamaData);
    }
}
