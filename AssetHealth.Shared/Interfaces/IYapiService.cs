﻿using OdakGIS.Framework.Core.Entities;
using OdakGIS.Framework.Core.Interfaces;
using OdakGIS.Numarataj.Data.Entities;
using OdakGIS.Numarataj.Shared.Entities;
using System.Collections.Generic;

namespace OdakGIS.Numarataj.Shared.Interfaces
{
    public interface IYapiService : IProjectService
    {
        OperationResult<int> YapiKaydet(YapiData yapi, bool sync);
        YapiData YapiGetir(int id);
        OperationResult YapiSil(int id);
        IList<YAPI> YapiSayfala(int pageIndex, int pageSize);
        int GetYapiSayisi();
        OperationResult<int> GetYapiIdFromUavtAjax(int UavtNo);
    }
}