﻿using OdakGIS.Framework.Core.Interfaces;
using OdakGIS.Numarataj.Shared.Entities;
using System.Collections.Generic;

namespace OdakGIS.Numarataj.Shared.Interfaces
{
    public interface ITanimService : IProjectService
    {
        List<KeyValuePair> YapiTipleriniGetir();
        List<KeyValuePair> YapiDurumlariniGetir();
        List<KeyValuePair> ParselTipleriniGetir();
        List<KeyValuePair> YolTipleriniGetir();

        List<KeyValuePair> NumaratajTipleriniGetir();
    }
}