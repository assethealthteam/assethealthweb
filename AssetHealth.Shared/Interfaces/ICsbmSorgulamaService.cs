﻿using OdakGIS.Framework.Core.Entities;
using OdakGIS.Framework.Core.Interfaces;
using OdakGIS.Numarataj.Shared.Entities;
using System.Collections.Generic;

namespace OdakGIS.Numarataj.Shared.Interfaces
{
    public interface ICsbmSorgulamaService : IProjectService
    {
        Paging<CsbmData> CsbmSorgula(Paging pagingParameters, CsbmSorgulamaCriteria cri);

        CsbmData getCSBMData(int yolId);

        List<MedyaGosterimData> GetMedyaGosterimData(int numaratajId);

        Paging<NumaratajData> BaglantiliBinaPaging(Paging pagingParameters, int yolId);
    }
}