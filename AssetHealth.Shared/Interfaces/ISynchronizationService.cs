﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdakGIS.Numarataj.Shared.Interfaces
{
    public interface ISynchronizationService
    {
        bool AllowSync { set; }
        DbTransaction SetReusableTransaction { set; }
    }
}