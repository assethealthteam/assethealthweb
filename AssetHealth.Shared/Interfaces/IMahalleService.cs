﻿using OdakGIS.Framework.Core.Entities;
using OdakGIS.Framework.Core.Interfaces;
using OdakGIS.Numarataj.Data.Entities;
using OdakGIS.Numarataj.Shared.Entities;
using System.Collections.Generic;
using System.Data.Entity.Spatial;

namespace OdakGIS.Numarataj.Shared.Interfaces
{
    public interface IMahalleService : IProjectService
    {
        OperationResult MahalleKaydet(MAHALLE mahalle);
        IList<KeyValuePair> MahalleListele();
        IList<MAHALLE> MahalleSayfala(int pageIndex, int pageSize);
        int? GetContainingMahalle(DbGeometry geometry);
        int GetMahalleSayisi();
    }
}