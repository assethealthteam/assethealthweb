﻿using OdakGIS.Framework.Core.Entities;
using OdakGIS.Framework.Core.Interfaces;
using OdakGIS.Numarataj.Shared.Entities.DisVeri;
using OdakGIS.Numarataj.Shared.Enums;
using System.Collections.Generic;

namespace OdakGIS.Numarataj.Shared.Interfaces
{
    public interface IDisVeriImportService : IProjectService
    {
        OperationResult LoadFile(NumaratajDisVeriTipi tip, int dosyaId, IEnumerable<DisVeriAlan> alanlar);
        OperationResult Ilerlet();
        List<DisVeriAlan> AlanlariGetir(NumaratajDisVeriTipi tip);
        bool Iptal();
        DisVeriIlerleme Ilerleme();
        void SiraDuzelt();
    }
}