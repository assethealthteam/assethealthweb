﻿using OdakGIS.Framework.Core.Interfaces;
using OdakGIS.Numarataj.Data;
using OdakGIS.Numarataj.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OdakGIS.Numarataj.Shared.Interfaces
{
    public interface IHierarsiService : IProjectService
    {
        void IlKaydet(NumaratajContext ctx, IL il);
        void IlceKaydet(NumaratajContext ctx, ILCE ilce);
        void MahalleKaydet(NumaratajContext ctx, MAHALLE mahalle);
        void KoyKaydet(NumaratajContext ctx, KOY koy);
        void ParselKaydet(NumaratajContext ctx, PARSEL parsel);
        void YapiKaydet(NumaratajContext ctx, YAPI yapi);
        void YolKaydet(NumaratajContext ctx, YOL yol);
        void NumaratajKaydet(NumaratajContext ctx, NUMARATAJ numarataj);
    }
}