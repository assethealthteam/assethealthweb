﻿using OdakGIS.Framework.Core.Entities;
using OdakGIS.Framework.Core.Interfaces;
using OdakGIS.Numarataj.Data.Entities;
using OdakGIS.Numarataj.Shared.Entities;
using System.Collections.Generic;
using System.Data.Entity.Spatial;

namespace OdakGIS.Numarataj.Shared.Interfaces
{
    public interface IParselService : IProjectService
    {
        OperationResult ParselKaydet(ParselData parsel);
        ParselData ParselGetir(int id);
        int? GetContainingParsel(DbGeometry geometry);
        IList<PARSEL> ParselSayfala(int pageIndex, int pageSize);
        int GetParselSayisi();
        void ParselMahalleIdDuzelt();
    }
}