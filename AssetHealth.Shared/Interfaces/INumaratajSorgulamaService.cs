﻿using OdakGIS.Framework.Core.Entities;
using OdakGIS.Framework.Core.Interfaces;
using OdakGIS.Numarataj.Shared.Entities;
using System.Collections.Generic;

namespace OdakGIS.Numarataj.Shared.Interfaces
{
    public interface INumaratajSorgulamaService : IProjectService
    {
        Paging<NumaratajData> NumaratajSorgula(Paging pagingParameters, NumaratajSorgulamaCriteria cri);

        List<SelectItem<int>> AdaParselAjaxList(int mahalleID);

        NumaratajData getNumaratajData(int Id);

        List<MedyaGosterimData> GetMedyaGosterimData(int kapiId);

        List<KeyValuePair> CsbmListele(int? mahalleId);
    }
}
