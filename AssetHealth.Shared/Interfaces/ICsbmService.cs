﻿using OdakGIS.Framework.Core.Entities;
using OdakGIS.Framework.Core.Interfaces;
using OdakGIS.Numarataj.Data.Entities;
using OdakGIS.Numarataj.Shared.Entities;
using System.Collections.Generic;
using System.Data.Entity.Spatial;

namespace OdakGIS.Numarataj.Shared.Interfaces
{
    public interface ICsbmService : IProjectService
    {
        OperationResult<int> CsbmKaydetUi(CsbmData yol, bool sync);
        OperationResult<int> YolOrtaHatGuncelle(int id, string geom);
        OperationResult<List<YOLORTAHAT>> YolOrtaHatHesapla(string geometry);
        OperationResult<List<YOLORTAHAT>> YolOrtaHatHesapla2(string geometry);
        CsbmData CsbmGetir(int id);
        OperationResult YolOrtaHatDogrula(string geom, int yolId);
        YOLORTAHATYON[] YolOrtaHatYonHesapla(DbGeometry yolOrtaHat, IList<MAHALLE> mahalleler);
        IList<KeyValuePair> CsbmListele(int? mahalleId);
        OperationResult<int> ClientYolOrtaHatGuncelle(int yolId, int cihazId, int clientId);
        IList<YOL> YolSayfala(int pageIndex, int pageSize);
        int GetYolSayisi();
    }
}