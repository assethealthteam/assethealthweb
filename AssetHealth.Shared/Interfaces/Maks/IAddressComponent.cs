﻿using OdakGIS.Numarataj.Shared.Enums;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace OdakGIS.Numarataj.Shared.Interfaces.Maks
{
    [ServiceContract]
    public interface IAddressComponent
    {
        [DataMember]
        long? Id { get; set; }

        [DataMember]
        string Ad { get; set; }

        [DataMember]
        AddressComponentTypes AddressComponentType { get; set; }

        [IgnoreDataMember]
        IAddressComponent Parent { get; set; }

        [DataMember]
        bool IsSelected { get; set; }
    }
}
