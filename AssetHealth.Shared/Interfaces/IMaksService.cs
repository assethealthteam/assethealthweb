﻿using OdakGIS.Framework.Core.Entities;
using OdakGIS.Framework.Core.Interfaces;

namespace OdakGIS.Numarataj.Shared.Interfaces
{
    public interface IMaksService : IProjectService
    {
        OperationResult<T> GetGenericData<T>(string url, params object[] queryData);
    }
}
