﻿using OdakGIS.Framework.Core.Entities;
using OdakGIS.Framework.Core.Interfaces;
using OdakGIS.Numarataj.Shared.Data;
using System.Collections.Generic;

namespace OdakGIS.Numarataj.Shared.Interfaces
{
    public interface ISenkronizasyonDuzeltmeService : IProjectService
    {
        OperationResult DataAktar();
        OperationResult DataAktar2();
        List<SenkronizasyonDetayData> GetYapiHataByMahalle(int mahalleId);
        List<SenkronizasyonDetayData> GetNumaratajHataByMahalle(int mahalleId);
        List<SenkronizasyonDetayData> GetBagimsizBolumHataByMahalle(int mahalleId);
    }
}
