﻿using OdakGIS.Framework.Core.Entities;
using OdakGIS.Framework.Core.Interfaces;
using OdakGIS.Numarataj.Data.Entities;
using OdakGIS.Numarataj.Shared.Entities;
using System.Collections.Generic;

namespace OdakGIS.Numarataj.Shared.Interfaces
{
    public interface IBagimsizBolumService : IProjectService
    {
        OperationResult BagimsizBolumKaydet(BagimsizBolumData bagimsizBolum, bool sync);
        BagimsizBolumData BagimsizBolumGetir(int id);
        OperationResult BagimsizBolumSil(int id);
        IList<BAGIMSIZBOLUM> BagimsizBolumSayfala(int pageIndex, int pageSize);
        int GetBagimsizBolumSayisi();
        Paging<BagimsizBolumData> BagimsizBolumPaging(Paging pagingParameters, int numaratajId);
    }
}
