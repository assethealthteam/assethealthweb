﻿using OdakGIS.Framework.Core.Entities;
using OdakGIS.Framework.Core.Interfaces;
using OdakGIS.Numarataj.Shared.Entities;
using System.Collections.Generic;

namespace OdakGIS.Numarataj.Shared.Interfaces
{
    public interface IHaritaService : IProjectService
    {
        OperationResult<HaritaSecimData> GetSelection(string geom, string tip);
        List<HaritaVektorVeri> GetVektorVeri(string bbox);
        OperationResult<List<InfoYapiData>> GetYapi(string geom);
        OperationResult<List<BagimsizBolumData>> GetBagimsizBolumByKapiId(int kapiId, int yapiId);
        string GetMapExtent();
    }
}
