﻿using Microsoft.SqlServer.Types;
using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Text;

namespace OdakGIS.Numarataj.Shared
{
    public class Util
    {
        public static DbGeometry BuildLineString(IList<DbGeometry> points)
        {
            DbGeometry result = null;
            StringBuilder builder = new StringBuilder("LINESTRING(");
            for (int i = 0; i < points.Count(); i++)
            {
                builder.AppendFormat("{0} {1}", points[i].XCoordinate, points[i].YCoordinate);
                if (i != points.Count() - 1)
                {
                    builder.Append(",");
                }
            }
            builder.Append(")");
            result = DbGeometry.FromText(builder.ToString(), 4326);
            return result;
        }
        public static DbGeometry ReverseLineString(DbGeometry geometry)
        {
            if (geometry.SpatialTypeName.ToLower() != "linestring")
            {
                throw new InvalidOperationException("geometry linestring degil");
            }
            DbGeometry result = null;
            List<DbGeometry> points = new List<DbGeometry>();
            for (int i = 1; i <= geometry.PointCount; i++)
            {
                points.Add(geometry.PointAt(i));
            }
            points.Reverse();
            result = BuildLineStringFromGeometries(points);
            return result;
        }

        public static DbGeometry BuildLineStringFromGeometries(IList<DbGeometry> pointArray)
        {
            DbGeometry firstPoint = null;
            SqlGeometryBuilder builder = null;
            if (pointArray.Count > 0)
            {
                firstPoint = pointArray.FirstOrDefault();
                pointArray.Remove(firstPoint);
            }
            else
            {
                return null;
            }
            builder = new SqlGeometryBuilder();
            builder.SetSrid(4326);
            builder.BeginGeometry(OpenGisGeometryType.LineString);
            builder.BeginFigure(firstPoint.XCoordinate.Value, firstPoint.YCoordinate.Value);
            foreach (var point in pointArray)
            {
                builder.AddLine(point.XCoordinate.Value, point.YCoordinate.Value);
            }
            builder.EndFigure();
            builder.EndGeometry();
            return DbGeometry.FromText(builder.ConstructedGeometry.MakeValid().ToString(), 4326);
        }

        public static SqlGeometry BuildLineStringFromGeometries(IList<SqlGeometry> pointArray)
        {
            //SqlGeometry firstPoint = null;
            SqlGeometryBuilder builder = null;
            if (pointArray.Count > 0)
            {
                //firstPoint = pointArray.FirstOrDefault();
                //pointArray.Remove(firstPoint);
            }
            else
            {
                return null;
            }
            builder = new SqlGeometryBuilder();
            builder.SetSrid(4326);
            builder.BeginGeometry(OpenGisGeometryType.LineString);
            builder.BeginFigure(pointArray[0].STX.Value, pointArray[0].STY.Value);
            for (int i = 1; i < pointArray.Count; i++)
            {
                builder.AddLine(pointArray[i].STX.Value, pointArray[i].STY.Value);
            }
            builder.EndFigure();
            builder.EndGeometry();
            return builder.ConstructedGeometry;
        }

        public static SqlGeometry FindNearestPointOnLine(SqlGeometry point, SqlGeometry geom)
        {
            //double distance = point.STDistance(geom).Value;
            //SqlGeometry pointBuffer = point.STBuffer(distance);
            var line = point.ShortestLineTo(geom);
            if (line.STEndPoint().ToString() != "Null")
                return line.STEndPoint();
            else
                return point;
        }

    }
}