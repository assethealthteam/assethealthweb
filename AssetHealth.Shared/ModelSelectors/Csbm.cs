﻿using OdakGIS.Numarataj.Data;
using OdakGIS.Numarataj.Data.Entities;
using OdakGIS.Numarataj.Shared.Entities;
using System.Linq;

namespace OdakGIS.Numarataj.Shared.ModelSelectors
{
    public static class Csbm
    {
        public static CsbmData ToCsbmData(this YOL yol)
        {
            //int i = 1;
            //string ortaHatYonStr = "LINESTRING({0} {1},{2} {3})";
            //CsbmYolOrtaHatData ortaHatData = null;
            if (yol == null)
            {
                return null;
            }
            CsbmData result = new CsbmData()
            {
                Adi = yol.AD,
                CiftBaslangicNo = yol.CIFTBASLANGICNO,
                CiftBitisNo = yol.CIFTBITISNO,
                Id = yol.OBJECTID,
                KimlikNo = yol.KIMLIKNO,
                TekBaslangicNo = yol.TEKBASLANGICNO,
                TekBitisNo = yol.TEKBITISNO,
                Tip = yol.TIP,
                UavtNo = yol.UAVTNO,
                UavtEslesmiyor = yol.UAVTESLESMIYOR,
                Genislik = yol.GENISLIK,
                Uzunluk = yol.UZUNLUK,
                CsbmWkt = yol.SHAPE.AsText(),
                Aciklama = yol.ACIKLAMA,
                AydinlatmaVarMi = yol.AYDINLATMAVARMI,
                BaglantiNiteligi = yol.BAGLANTINITELIGI,
                CaddeSokakLevhaVarMi = yol.CADDESOKAKLEVHAVARMI,
                Cinsi = yol.CINSI,
                Durum = yol.DURUM,
                Egimi = yol.EGIMI,
                HizLimiti = yol.HIZLIMITI,
                Islevi = yol.ISLEVI,
                KaldirimVarMi = yol.KALDIRIMVARMI,
                KaplamaCinsi = yol.KAPLAMACINSI,
                MeclisKararNo = yol.MECLISKARARNO,
                MeclisKararTarih = yol.MECLISKARARTARIH,
                ParklanmaVarMi = yol.PARKLANMAVARMI,
                RefujVarMi = yol.REFUJVARMI,
                SeritCizgisiVarMi = yol.SERITCIZGISIVARMI,
                SeritSayisi = yol.SERITSAYISI,
                Sinyalizasyon = yol.SINYALIZASYON,
                TabelaAdi = yol.TABELAADI,
                YolcuDuragiVarMi = yol.YOLCUDURAGIVARMI,
                YolunDevamliligi = yol.YOLUNDEVAMLILIGI,
                YonDurumu = yol.YONDURUMU,
                ZeminNiteligi = yol.ZEMINNITELIGI
            };
            return result;
        }

        public static IQueryable<CsbmData> ToCsbmData(this IQueryable<YOL> yol, NumaratajContext ctx)
        {
            return yol.Select(t => new CsbmData()
            {
                Id = t.OBJECTID,
                KimlikNo = t.KIMLIKNO,
                TipAdi = t.YOL_TIP.ADI,
                IlAdi = ctx.YOLORTAHATYON.Where(p => p.YOLORTAHAT.YOLID == t.OBJECTID).Select(p => p.MAHALLE).FirstOrDefault() != null ? ctx.YOLORTAHATYON.Where(p => p.YOLORTAHAT.YOLID == t.OBJECTID).Select(p => p.MAHALLE).FirstOrDefault().ILCE.IL.AD : "",
                IlceAdi = ctx.YOLORTAHATYON.Where(p => p.YOLORTAHAT.YOLID == t.OBJECTID).Select(p => p.MAHALLE).FirstOrDefault() != null ? ctx.YOLORTAHATYON.Where(p => p.YOLORTAHAT.YOLID == t.OBJECTID).Select(p => p.MAHALLE).FirstOrDefault().ILCE.AD : "",
                Adi = t.AD,
                MahalleAdi = t.YOLORTAHAT.SelectMany(a => a.YOLORTAHATYON).Select(s => s.MAHALLE.AD).FirstOrDefault(),
                UavtNo = t.UAVTNO,
                UavtEslesmiyor = t.UAVTESLESMIYOR,
                Genislik = t.GENISLIK,
                Uzunluk = t.UZUNLUK,
                Aciklama = t.ACIKLAMA,
                AydinlatmaVarMi = t.AYDINLATMAVARMI,
                BaglantiNiteligi = t.BAGLANTINITELIGI,
                CaddeSokakLevhaVarMi = t.CADDESOKAKLEVHAVARMI,
                Cinsi = t.CINSI,
                Durum = t.DURUM,
                Egimi = t.EGIMI,
                HizLimiti = t.HIZLIMITI,
                Islevi = t.ISLEVI,
                KaldirimVarMi = t.KALDIRIMVARMI,
                KaplamaCinsi = t.KAPLAMACINSI,
                MeclisKararNo = t.MECLISKARARNO,
                MeclisKararTarih = t.MECLISKARARTARIH,
                ParklanmaVarMi = t.PARKLANMAVARMI,
                RefujVarMi = t.REFUJVARMI,
                SeritCizgisiVarMi = t.SERITCIZGISIVARMI,
                SeritSayisi = t.SERITSAYISI,
                Sinyalizasyon = t.SINYALIZASYON,
                TabelaAdi = t.TABELAADI,
                YolcuDuragiVarMi = t.YOLCUDURAGIVARMI,
                YolunDevamliligi = t.YOLUNDEVAMLILIGI,
                YonDurumu = t.YONDURUMU,
                ZeminNiteligi = t.ZEMINNITELIGI,
                CiftBaslangicNo = t.CIFTBASLANGICNO,
                CiftBitisNo = t.CIFTBITISNO,
                TekBaslangicNo = t.TEKBASLANGICNO,
                TekBitisNo = t.TEKBITISNO,
                CsbmWkt = t.SHAPE.AsText(),
                ToplamBina = ctx.YAPI.Count(k => ctx.NUMARATAJ.Where(z => z.YOLORTAHATYON.YOLORTAHAT.YOLID == t.OBJECTID).Select(z => z.YAPIID).ToList().Contains(k.OBJECTID)),
                ToplamKapi = ctx.NUMARATAJ.Count(k => k.YOLORTAHATYON.YOLORTAHAT.YOLID == t.OBJECTID),
                ToplamParsel = ctx.PARSEL.Count(h => ctx.YAPI.Where(k => ctx.NUMARATAJ.Where(z => z.YOLORTAHATYON.YOLORTAHAT.YOLID == t.OBJECTID).Select(z => z.YAPIID).ToList().Contains(k.OBJECTID)).Select(k => k.PARSELID).ToList().Contains(h.ID)),
            });
        }
    }
}