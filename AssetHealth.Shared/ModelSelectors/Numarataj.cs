﻿using OdakGIS.Numarataj.Data.Entities;
using OdakGIS.Numarataj.Shared.Entities;
using OdakGIS.Numarataj.Shared.Enums;
using System;
using System.Linq;

namespace OdakGIS.Numarataj.Shared.ModelSelectors
{
    public static class Numarataj
    {
        public static NumaratajData ToNumaratajData(this NUMARATAJ numarataj)
        {
            NumaratajData numaratajData = null;
            if (numarataj != null)
            {
                numaratajData = new NumaratajData()
                {
                    Id = numarataj.OBJECTID,
                    BagliOlduguNumaratajId = numarataj.BAGLIOLDUGUNUMARATAJID,
                    CsbmAdi = numarataj.YOLORTAHATYON != null ? numarataj.YOLORTAHATYON.YOLORTAHAT.YOL.AD : String.Empty,
                    YapiAdi = numarataj.YAPI != null ? numarataj.YAPI.AD : String.Empty,
                    Geometry = numarataj.SHAPE.AsText(),
                    KapiNo = numarataj.KAPINO,
                    Tip = (NumaratajTip?)numarataj.TIP,
                    MontajYapilmamaSebebi = numarataj.MONTAJYAPILMAMASEBEBI,
                    UavtNo = numarataj.UAVTNO,
                    Yon = numarataj.YOLORTAHATYON != null ? numarataj.YOLORTAHATYON.SAYISALLASTIRMAYON : null,
                    Aciklama = numarataj.ACIKLAMA,
                    Ad = numarataj.AD,
                    Binakodu = numarataj.BINAKODU,
                    DigerYapiId = numarataj.DIGERYAPIID,
                    KimlikNo = numarataj.KIMLIKNO,
                    ParselId = numarataj.PARSELID,
                    ParselKimlikNo = numarataj.PARSELKIMLIKNO,
                    YapiId = numarataj.YAPIID,
                    YerlesimAcisi = numarataj.YERLESIMACISI,
                    YolOrtaHatId = numarataj.YOLORTAHATID,
                    YolOrtaHatYonId = numarataj.YOLORTAHATYONID
                };
            }
            return numaratajData;
        }
        public static IQueryable<NumaratajData> ToNumaratajData(this IQueryable<NUMARATAJ> numarataj)
        {
            return numarataj.Select(t => new NumaratajData()
            {
                Id = t.OBJECTID,
                IlAdi = t.YOLORTAHATYON != null ? t.YOLORTAHATYON.MAHALLE.ILCE.IL.AD : "",
                IlceAdi = t.YOLORTAHATYON != null ? t.YOLORTAHATYON.MAHALLE.ILCE.AD : "",
                CsbmAdi = t.YOLORTAHATYON != null ? t.YOLORTAHATYON.YOLORTAHAT.YOL.AD : "",
                MahalleAdi = t.YOLORTAHATYON != null ? t.YOLORTAHATYON.MAHALLE.AD : "",
                SiteAdi = t.YAPI != null ? t.YAPI.SITEVEYAKOOPERATIFADI : "",
                YapiAdi = t.YAPI != null ? t.YAPI.AD : "",
                KapiNo = t.KAPINO,
                TipAdi = t.NUMARATAJ_TIP.ADI,
                Yon = t.YOLORTAHATYON != null ? t.YOLORTAHATYON.SAYISALLASTIRMAYON : null,
                UavtNo = t.UAVTNO,
                MontajYapilmamaSebebi = t.MONTAJYAPILMAMASEBEBI,
                Aciklama = t.ACIKLAMA,
                Ad = t.AD,
                Binakodu = t.BINAKODU,
                DigerYapiId = t.DIGERYAPIID,
                KimlikNo = t.KIMLIKNO,
                ParselId = t.PARSELID,
                ParselKimlikNo = t.PARSELKIMLIKNO,
                AdaParsel = t.PARSEL.ADANO + "/" + t.PARSEL.PARSELNO,
                YapiId = t.YAPIID,
                YerlesimAcisi = t.YERLESIMACISI,
                YolOrtaHatYonId = t.YOLORTAHATYONID,
                BagliOlduguNumaratajId = t.BAGLIOLDUGUNUMARATAJID,
                YapiTipi = t.YAPI.YAPI_TIP.ADI,
                UAVTNo = t.UAVTNO.HasValue ? t.UAVTNO.Value : 0,
                KullanimDurumu = t.YAPI != null ? t.YAPI.YAPI_DURUM.ADI : "",
                PostaKodu = t.YAPI != null ? t.YAPI.POSTAKODU : "",
                ZeminUstuKatSayisi = (t.YAPI != null && t.YAPI.ZEMINUSTUKATSAYISI.HasValue) ? t.YAPI.ZEMINUSTUKATSAYISI.Value : 0,
                ZeminAltiKatSayisi = (t.YAPI != null && t.YAPI.ZEMINALTIKATSAYISI.HasValue) ? t.YAPI.ZEMINALTIKATSAYISI.Value : 0,
                ToplamBagimsizBolum = (t.YAPI != null && t.YAPI.BBSAYISI.HasValue) ? t.YAPI.BBSAYISI.Value : 0,
                ToplamKapi = t.YAPI.NUMARATAJ.Count
            });
        }
    }
}