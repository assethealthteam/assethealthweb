﻿using OdakGIS.Numarataj.Data.Entities;
using OdakGIS.Numarataj.Shared.Entities;
using System.Linq;

namespace OdakGIS.Numarataj.Shared.ModelSelectors
{
    public static class BagimsizBolum
    {
        public static IQueryable<BagimsizBolumData> ToBagimsizBolumData(this IQueryable<BAGIMSIZBOLUM> bagimsizBolum)
        {
            return bagimsizBolum.Select(t => new BagimsizBolumData()
            {
                Id = t.OBJECTID,
                IlAdi = t.NUMARATAJ.YOLORTAHATYON.MAHALLE.ILCE.IL.AD,
                IlceAdi = t.NUMARATAJ.YOLORTAHATYON.MAHALLE.ILCE.AD,
                CsbmAdi = t.NUMARATAJ.YOLORTAHATYON.YOLORTAHAT.YOL.AD,
                MahalleAdi = t.NUMARATAJ.YOLORTAHATYON.MAHALLE.AD,
                SiteAdi = t.NUMARATAJ.YAPI.SITEVEYAKOOPERATIFADI,
                YapiAdi = t.NUMARATAJ.YAPI != null ? t.NUMARATAJ.YAPI.AD : "",
                KapiNo = t.BAGIMSIZBOLUMNO,
                AdaParsel = t.NUMARATAJ.PARSEL.ADANO + "/" + t.NUMARATAJ.PARSEL.PARSELNO,
                Aciklama = t.ACIKLAMA,
                BagimsizBolumNo = t.BAGIMSIZBOLUMNO,
                BagliOlduguBBId = t.BAGLIOLDUGUBBID,
                DigerYapiId = t.DIGERYAPIID,
                Durum = t.DURUM,
                KatNo = t.KATNO,
                Kaynak = t.KAYNAK,
                KimlikNo = t.KIMLIKNO,
                KullanimAltTur = t.KULLANIMALTTUR,
                KullanimTuru = t.KULLANIMTURU,
                NumaratajId = t.NUMARATAJID,
                TapuBagimsizBolumNo = t.TAPUBAGIMSIZBOLUMNO,
                YapiId = t.YAPIID,
                UavtNo = t.UAVTNO,
                Ad = t.AD,
                BilgiVerenKisi = t.BILGIVERENKISI,
                CalisanSayisi = t.CALISANSAYISI,
                EvSahibi = t.EVSAHIBI,
                IsYeriAdi = t.ISYERIADI,
                Kiraci = t.KIRACI,
                OturanTc = t.OTURANTC,
                Soru1 = t.SORU1,
                Soru2 = t.SORU2,
                SuAbonesi = t.SUABONESI,
                TicariIsYeriAdi = t.TICARIISYERIADI,
                Tip = t.TIP,
                YakitTuru = t.YAKITTURU,
                YasayanSayisi = t.YASAYANSAYISI,
                DisKapiNo = t.DISKAPINO,
                EskiBBNo = t.ESKIBBNO
                //DegistirmeTarihi = t.DEGISTIRMETARIHISTRING,
                //OlusturmaTarihi = t.OLUSTURMATARIHISTRING
            });
        }
    }
}
