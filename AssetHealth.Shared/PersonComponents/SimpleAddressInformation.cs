﻿using OdakGIS.Numarataj.Shared.AddressComponents;
using OdakGIS.Numarataj.Shared.Enums.Maks;
using System;
using System.Runtime.Serialization;

namespace OdakGIS.Numarataj.Shared.PersonComponents
{
    [DataContract]
    [Serializable]

    public class SimpleAddressInformation
    {
        private long bagimsizBolumKimlikNo;
        private string acikAdres;
        private long ilKimlikNo;

        private Parametre adresDurum;
        private DateTime beyanTarihi;
        private CstAdresTipleri? adresTipi;
        private long? tcKimlikNo;

        [DataMember]
        public Il Il { get; set; }
        [DataMember]
        public Ilce Ilce { get; set; }
        [DataMember]
        public Mahalle Mahalle { get; set; }
        [DataMember]
        public Csbm Csbm { get; set; }
        [DataMember]
        public Bina Bina { get; set; }
        [DataMember]
        public BagimsizBolum BagimsizBolum { get; set; }

        [DataMember]
        public long BagimsizBolumKimlikNo
        {
            get { return bagimsizBolumKimlikNo; }
            set { bagimsizBolumKimlikNo = value; }
        }

        [DataMember]
        public string AcikAdres
        {
            get { return acikAdres; }
            set { acikAdres = value; }
        }

        [DataMember]
        public long IlKimlikNo
        {
            get { return ilKimlikNo; }
            set { ilKimlikNo = value; }
        }

        [DataMember]
        public Parametre AdresDurum
        {
            get { return adresDurum; }
            set { adresDurum = value; }
        }

        [DataMember]
        public DateTime BeyanTarihi
        {
            get { return beyanTarihi; }
            set { beyanTarihi = value; }
        }

        [DataMember]
        public CstAdresTipleri? AdresTipi
        {
            get { return adresTipi; }
            set { adresTipi = value; }
        }

        [DataMember]
        public long? TcKimlikNo
        {
            get { return tcKimlikNo; }
            set { tcKimlikNo = value; }
        }

        public SimpleAddressInformation(string acikAdres)
        {
            this.acikAdres = acikAdres;
        }

        public SimpleAddressInformation(long? bagimsizBolumKimlikNo, string acikAdres, long? ilKimlikNo)
        {
            AcikAdres = acikAdres;
            if (bagimsizBolumKimlikNo.HasValue)
                BagimsizBolumKimlikNo = bagimsizBolumKimlikNo.Value;
            if (ilKimlikNo.HasValue)
                IlKimlikNo = ilKimlikNo.Value;
        }

        public SimpleAddressInformation()
        {
        }
    }
}
