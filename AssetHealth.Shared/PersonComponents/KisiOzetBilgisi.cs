﻿using OdakGIS.Numarataj.Shared.Enums.Maks;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace OdakGIS.Numarataj.Shared.PersonComponents
{
    [DataContract]
    [Serializable]
    public class KisiOzetBilgisi
    {
        private long? tcKimlikNo;
        private string ad;
        private string soyad;
        private DateTime dogumTarihi;
        private CstMedeniHalTip? medeniHal;
        private List<SimpleAddressInformation> adres;
        private CstKisiYakinlik? yakinlik;
        private CstCinsiyet? cinsiyet;

        [DataMember]
        public string BabaAd { get; set; }
        [DataMember]
        public string AnneAd { get; set; }

        public KisiOzetBilgisi() { }


        private string tcKimlikNoGizli;

        [DataMember]
        public long? TcKimlikNo
        {
            get { return tcKimlikNo; }
            set { tcKimlikNo = value; }
        }

        [DataMember]
        public string TcKimlikNoGizli
        {
            get { return tcKimlikNoGizli; }
            set { tcKimlikNoGizli = value; }
        }

        [DataMember]
        public string Ad
        {
            get { return ad; }
            set { ad = value; }
        }

        [DataMember]
        public string Soyad
        {
            get { return soyad; }
            set { soyad = value; }
        }

        [DataMember]
        public DateTime DogumTarihi
        {
            get { return dogumTarihi; }
            set { dogumTarihi = value; }
        }

        [DataMember]
        public CstMedeniHalTip? MedeniHal
        {
            get { return medeniHal; }
            set { medeniHal = value; }
        }

        [DataMember]
        public List<SimpleAddressInformation> Adres
        {
            get
            {
                if (adres == null)
                    adres = new List<SimpleAddressInformation>();
                return adres;
            }
            set { adres = value; }
        }

        [DataMember]
        public CstKisiYakinlik? Yakinlik
        {
            get { return yakinlik; }
            set { yakinlik = value; }
        }

        [DataMember]
        public CstCinsiyet? Cinsiyet
        {
            get { return cinsiyet; }
            set { cinsiyet = value; }
        }

        public void SetTcKimlikNo(string p)
        {
            if (string.IsNullOrEmpty(p) == true || p.Contains("*"))
                this.TcKimlikNoGizli = p;
            else
                this.TcKimlikNo = Int64.Parse(p);
        }

        public static class PropNames
        {
            public const string TcKimlikNo = "TcKimlikNo";
            public const string Ad = "Ad";
            public const string Soyad = "Soyad";
        }
    }
}
