﻿namespace OdakGIS.Numarataj.Shared
{
    internal class Constants
    {
        public static string DATE_FORMAT = "yyyyMMdd";
        public static string DATE_FORMAT_DISPLAY = "dd.MM.yyyy";

        public static string DATETIME_FORMAT = "yyyyMMddHHmmss";
        public static string DATETIME_FORMAT_DISPLAY = "dd.MM.yyyy HH:mm:ss";

        public static string TIME_FORMAT = "HHmmss";
        public static string TIME_FORMAT_DISPLAY = "HH:mm:ss";


    }
}
